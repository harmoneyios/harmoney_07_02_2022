//
//  SooninSlideInHandler.swift
//  FitKet
//
//  Created by vaish navi on 10/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit
import UIImageView_Letters

class SooninSlideInHandler: NSObject , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
     var isCorporateUser : Bool = false
     var userLevel = ""
     var mobileNumber = ""

   
    var homeModel: HomeModel? {
        return HomeManager.sharedInstance.homeModel
    }


    
    override init() {
        super.init()
        
//       homeViewController = HomeViewController()
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(SettingCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView.register(UINib(nibName: SideMenuHeaderCollectionViewCell.sideMenuHeaderID, bundle: .main), forCellWithReuseIdentifier: SideMenuHeaderCollectionViewCell.sideMenuHeaderID)
        
         collectionView.register(UINib(nibName: SideMenuFooterCollectionViewCell.sideMenuFooterID, bundle: .main), forCellWithReuseIdentifier: SideMenuFooterCollectionViewCell.sideMenuFooterID)
//        HarmonySingleton.previousVC = .sideMenu
    }
    
    struct MenuLocation {
        var width : CGFloat = 0
        var height : CGFloat = 0
        var xTo : CGFloat = 0
        var yTo : CGFloat = 0
        var xFrom : CGFloat = 0
        var yFrom : CGFloat = 100
    }
    
    let blackView = UIView()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero , collectionViewLayout: layout)
        cv.backgroundColor = UIColor(red: 236.0/255.0, green: 242.0/255.0, blue: 252.0/255.0, alpha: 1.0)
        return cv
    }()
    
    var selectionDelegate : SelectionDelegate!
     
    var cellId = "cellId"
    var menuCellheight : CGFloat = 35
    var menuheaderCellheight : CGFloat = 130
    var menuCellWidth : CGFloat = 300
    var menuIconsSize : CGFloat = 32
    var menuCellsBackColor  = UIColor.clear
    var menuHighlightColor  = UIColor.darkGray
    var menuCellsTextColor  = UIColor.black
    var menuBackColor  = UIColor.white
//    rgba(236, 242, 252, 1)
    var menuBackSystem =  UIColor(red:  236.0/255.0, green:  242.0/255.0, blue:  252.0/255.0, alpha: 1.0)
    var mainMenuSize : CGFloat?
    var MainMenuSide : MenuLocationOption = .Left
    var isReloadData:Bool = false

    
    //    var settings:[Setting] = {
    //       return [Setting(name: "New", imageName: "addicon32x32"),
    //               Setting(name: "Help", imageName: "Helpicon32x32"),
    //               Setting(name: "Send", imageName: "Send32x32"),
    //                Setting(name: "Save", imageName: "save32x32"),
    //                Setting(name: "Settings", imageName: "Settings32x32"),
    //               Setting(name: "Lock", imageName: "Lock32x32"),
    //               Setting(name: "Cancel", imageName: "Cancel32x32")]
    //    }()
    
    
    func showSlideInMenu(menuSide: MenuLocationOption = .Left) {
        //mainMenuSize = menuSize
        MainMenuSide = menuSide
        //        let userProgress = UserProgressViewController.instantiateFrom(storyboard: .userProgress)
        //        self.navigationController.isNavigationBarHidden = true
        //        userProgress.viewModel = self.userProgressViewModel
        //        userProgress.mediaManager = self.mediaManager
        //        userProgress.accountManager = self.accountManager
        //        userProgress.gameManager = self.gameManager
        //        userProgress.hidesBottomBarWhenPushed = true
        //        self.navigationController.pushViewController(userProgress, animated: true)
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(blackView)
            window.addSubview(collectionView)
            blackView.frame = window.frame
            blackView.backgroundColor = UIColor.clear
            collectionView.layer.cornerRadius = 10
                              collectionView.layer.shadowColor = UIColor(red: 35/255, green: 136/255, blue: 243/255, alpha: 0.16).cgColor
                              collectionView.layer.shadowOpacity = 1.0
                              collectionView.layer.shadowOffset = .init(width: 1 , height: 10)
                              collectionView.layer.shadowRadius = 5
            
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDissmiss)))
          
            let directions: [UISwipeGestureRecognizer.Direction] = [.right, .left, .up, .down]
            for direction in directions {
                let gesture = UISwipeGestureRecognizer(target: self, action:  #selector(handleDissmiss))
                gesture.direction = direction
                blackView.addGestureRecognizer(gesture)
            }

            blackView.alpha = 0
            self.collectionView.alpha = 1
            self.collectionView.backgroundColor = UIColor(red: 236.0/255.0, green: 242.0/255.0, blue: 252.0/255.0, alpha: 1.0)
            
            print(menuSide)
            
            let menuLoc = slideMenuLocationCalculator(window: window)
            
            print(menuLoc.xFrom , menuLoc.xTo)
            collectionView.frame = CGRect(x: menuLoc.xFrom, y: menuLoc.yFrom + 30, width: menuLoc.width , height: menuLoc.height - 30)
            
            self.selectionDelegate.didTapSelect(menuSelection: "",value: menuLoc.width)

            UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .transitionFlipFromLeft, animations: {
                
                self.blackView.alpha = 1
                
                self.collectionView.frame = CGRect(x: menuLoc.xTo, y: menuLoc.yTo, width: menuLoc.width, height: menuLoc.height)
                
                
            }, completion: {finished in

            })
            
        }
    }
    
    
    func slideMenuLocationCalculator(window: UIView) -> MenuLocation {
        var menuLocation = MenuLocation()
        
        var menuHeight:CGFloat = (6 * menuCellheight) + (2 * menuheaderCellheight)
        
        if isCorporateUser {
            menuHeight = menuHeight + menuheaderCellheight
        }
        let menuWidth:CGFloat = menuCellWidth // + menuIconsSize + 32
        
        switch MainMenuSide {
        case .TOP :
            menuLocation.width = window.frame.width
            menuLocation.height = menuHeight // mainMenuSize!
            menuLocation.xTo = 0
            menuLocation.yTo = 0
            menuLocation.xFrom = 0
            menuLocation.yFrom = 0 - menuHeight // - mainMenuSize!
        case .Right :
            menuLocation.width = menuWidth //mainMenuSize!
            menuLocation.height = window.frame.height
            menuLocation.xTo = window.frame.width - menuLocation.width
            menuLocation.yTo = 0
            menuLocation.xFrom = window.frame.width * 0.80
            menuLocation.yFrom = 0
        case .Left :
            menuLocation.width = menuWidth //mainMenuSize!
            menuLocation.height = window.frame.height
            menuLocation.xTo = 0
            menuLocation.yTo = 0
            menuLocation.xFrom = 0 - menuWidth //mainMenuSize!
            menuLocation.yFrom = 0
        case .Bottom :
            menuLocation.width = window.frame.width
            menuLocation.height = menuHeight //mainMenuSize!
            menuLocation.xTo = 0
            menuLocation.yTo = window.frame.height - menuLocation.height
            menuLocation.xFrom = 0
            menuLocation.yFrom = window.frame.height
            break
            //        default :
            //            menuLocation.width = window.frame.width
            //            menuLocation.height = menuHeight //mainMenuSize!
            //            menuLocation.xTo = 0
            //            menuLocation.yTo = window.frame.height - menuLocation.height
            //            menuLocation.xFrom = 0
            //            menuLocation.yFrom = window.frame.height
            //            break
        }
        
        return menuLocation
    }
    
    @objc func handleDissmiss() {
        self.selectionDelegate.didTapSelect(menuSelection: "",value: 0)

        UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .transitionFlipFromRight, animations: {
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                let menuLoc = self.slideMenuLocationCalculator(window: window)
                self.collectionView.frame = CGRect(x: menuLoc.xFrom, y: menuLoc.yFrom,  width: menuLoc.width, height: menuLoc.height)
                
            }
            self.collectionView.alpha = 0
            //self.collectionView.frame = CGRect(x: xFrom, y: yFrom, width: width, height: height)
            
        }, completion: {finished in

        })
     
    }
    
   @objc func privacyPolicyBtnAction(_ sender: Any) {
          self.selectionDelegate.didTapSelect(menuSelection: "Privacy Policy", value: 0)
    menuHidden()


      }
   @objc func termsAndConditionBtnAction(_ sender: Any) {
          self.selectionDelegate.didTapSelect(menuSelection: "Terms of Use", value: 0)
    menuHidden()
  }
    
    @objc func corporateLoginBtnAction(_ sender: Any) {
        
    self.selectionDelegate.didTapSelect(menuSelection: "corporate Login", value: 0)
      menuHidden()
    }
    
    @objc func LogOutBtnAction(_ sender: Any) {
         
     self.selectionDelegate.didTapSelect(menuSelection: "LogOutBtnAction", value: 0)
       menuHidden()
     }
    @objc func corporateLogOutBtnAction(_ sender: Any) {
         
     self.selectionDelegate.didTapSelect(menuSelection: "corporateLogOutBtnAction", value: 0)
       menuHidden()
     }
    
    
    
    func menuHidden()  {
        
        UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .transitionFlipFromLeft, animations: {
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                let menuLoc = self.slideMenuLocationCalculator(window: window)
                self.collectionView.frame = CGRect(x: menuLoc.xFrom, y: menuLoc.yFrom,  width: menuLoc.width, height: menuLoc.height)
                
            }
            self.collectionView.alpha = 0
        }, completion: {finished in
            
        })
        
    }
    
    func setProfilePic(frstName: String?, lstName: String?, profileImageView: UIImageView){
    
        if let firstName = frstName, let lastName = lstName {
            profileImageView.setImageWith(firstName + " " + lastName, color: .lightGray)
        } else {
            if let dispName = frstName {
                profileImageView.setImageWith(dispName, color: .lightGray)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isReloadData{
//            if isCorporateUser {
//                return 9
//            }
//            return 6
            var rowCount:Int = 3
            if HarmonySingleton.shared.dashBoardData.data?.profileMenu?.about?.isActive == true{
              rowCount = rowCount + 1
            }
            if HarmonySingleton.shared.dashBoardData.data?.profileMenu?.charity?.isActive == true{
              rowCount = rowCount + 1
            }
            if HarmonySingleton.shared.dashBoardData.data?.profileMenu?.privacyPolicy?.isActive == true{
              rowCount = rowCount + 1
            }
            if HarmonySingleton.shared.dashBoardData.data?.profileMenu?.termsOfUse?.isActive == true{
              rowCount = rowCount + 1
            }
            if HarmonySingleton.shared.dashBoardData.data?.profileMenu?.faq?.isActive == true{
              rowCount = rowCount + 1
            }
            if HarmonySingleton.shared.dashBoardData.data?.profileMenu?.support?.isActive == true{
              rowCount = rowCount + 1
            }
            if HarmonySingleton.shared.dashBoardData.data?.profileMenu?.settings_permission?.isActive == true{
              rowCount = rowCount + 1
            }
            if HarmonySingleton.shared.dashBoardData.data?.profileMenu?.myorder?.isActive == true{
              rowCount = rowCount + 1
            }
            if HarmonySingleton.shared.dashBoardData.data?.profileMenu?.rewards?.isActive == true{
              rowCount = rowCount + 1
            }
            return rowCount
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        if ((indexPath.row  == 0) || (indexPath.row  == 7 && isCorporateUser)){
                if (indexPath.row  == 0){

                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SideMenuHeaderCollectionViewCell", for: indexPath) as! SideMenuHeaderCollectionViewCell
                    
                    if (indexPath.row  == 7 && isCorporateUser) {
                        cell.nameLbl.text = ""
                        cell.levelLbl.text = ""
        //                cell.accountLbl.text = self.corporateInfo?.email ?? ""
                        cell.accountLbl.text = ""

                        cell.logoutBtn.addTarget(self, action:#selector(corporateLogOutBtnAction), for:.touchUpInside)
                        cell.logoutBtn .setTitle("Logout Corporate/Institute Account", for: .normal)
                        cell.profileviewWidthConstraint.constant = 100
                        cell.topviewHeightConstraint.constant = 15
                        cell.dividerviewHeightConstraint.constant = 1

                    }
                    else {
                        let userName = homeModel?.data.name
                        
                        cell.nameLbl.text = userName?.capitalized
                        if homeModel?.data.profilePic == "" {
                            self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: cell.profileImg)
                        } else {
                            cell.profileImg.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
                        }
                        cell.levelLbl.text = userLevel
                        cell.accountLbl.text = mobileNumber
                        cell.profileImg.layer.borderWidth = 0
                        cell.profileImg.layer.masksToBounds = false
                        cell.profileImg.layer.cornerRadius = 25.0
                        cell.profileImg.clipsToBounds = true
                        cell.profileviewWidthConstraint.constant = 50
                        cell.bottomviewHeightConstraint.constant = 20


        //                if let profileURL = profile?.imageUrl {
        //                    cell.profileImg.sd_setImage(with: URL(string: profileURL))
        //                } else {
        //                    Util.shared.setProfilePic(frstName: profile?.name, lstName: nil, profileImageView: cell.profileImg)
        //                }
                        cell.logoutBtn.addTarget(self, action:#selector(LogOutBtnAction), for:.touchUpInside)
                    }
                    return cell
                    
                }

                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SettingCell
                if indexPath.row == 1{
                    cell.nameLabel.text = "Profile"
                    cell.iconImageView.image = UIImage(named: "profile_sidemenu")
                } else if indexPath.row == 2 {
                    cell.nameLabel.text =   HarmonySingleton.shared.dashBoardData.data?.profileMenu?.about?.menuName
                    cell.iconImageView.image = UIImage(named:  "harmoney_sidemenu")
                }else if indexPath.row == 3 {
                    cell.nameLabel.text =    HarmonySingleton.shared.dashBoardData.data?.profileMenu?.charity?.menuName
                    cell.iconImageView.image = UIImage(named: "charity")
                }else if indexPath.row == 4 {
                    cell.nameLabel.text =    HarmonySingleton.shared.dashBoardData.data?.profileMenu?.privacyPolicy?.menuName
                    cell.iconImageView.image = UIImage(named: "privacyPolicy_sidemenu")
                }else if indexPath.row == 5 {
                    cell.nameLabel.text =    HarmonySingleton.shared.dashBoardData.data?.profileMenu?.termsOfUse?.menuName
                    cell.iconImageView.image = UIImage(named: "terms_sidemenu")
                }
        //        else if indexPath.row == 4 {
        //            cell.nameLabel.text =    HarmonySingleton.shared.dashBoardData.data?.profileMenu?.faq?.menuName
        //            cell.iconImageView.image = UIImage(named: "terms_sidemenu")
        //        }else if indexPath.row == 4 {
        //            cell.nameLabel.text =    HarmonySingleton.shared.dashBoardData.data?.profileMenu?.support?.menuName
        //            cell.iconImageView.image = UIImage(named: "terms_sidemenu")
        //        }else if indexPath.row == 4 {
        //            cell.nameLabel.text =    HarmonySingleton.shared.dashBoardData.data?.profileMenu?.settings_permission?.menuName
        //            cell.iconImageView.image = UIImage(named: "terms_sidemenu")
        //        }else if indexPath.row == 4 {
        //            cell.nameLabel.text =    HarmonySingleton.shared.dashBoardData.data?.profileMenu?.myorder?.menuName
        //            cell.iconImageView.image = UIImage(named: "terms_sidemenu")
        //        }else if indexPath.row == 4 {
        //            cell.nameLabel.text =    HarmonySingleton.shared.dashBoardData.data?.profileMenu?.rewards?.menuName
        //            cell.iconImageView.image = UIImage(named: "terms_sidemenu")
        //        }
                
                else if indexPath.row == 6 {
                    cell.nameLabel.text =  "Tour"
                    cell.iconImageView.image = UIImage(named: "tour_sidemenu")
                }
                
                return cell
                
            }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0
        {
            return CGSize(width: collectionView.frame.width, height: menuheaderCellheight + 40)
            
        }
//        else if ((indexPath.row == 7 ) || (indexPath.row  == 8 && isCorporateUser)){
//
//            if (indexPath.row  == 7 && isCorporateUser){
//                return CGSize(width: collectionView.frame.width, height: 110)
//            }
//            return CGSize(width: collectionView.frame.width, height: menuheaderCellheight)
//
//        }
        
        return CGSize(width: collectionView.frame.width, height: menuCellheight)
        //        return CGSize(width: menuCellWidth , height: menuCellheight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 1{
            self.selectionDelegate.didTapSelect(menuSelection: "Profile", value: 0)
            menuHidden()
        } else if indexPath.row == 2 {
            self.selectionDelegate.didTapSelect(menuSelection: HarmonySingleton.shared.dashBoardData.data?.profileMenu?.about?.menuName ?? "", value: 0)
             menuHidden()
        }else if indexPath.row == 3 {
            self.selectionDelegate.didTapSelect(menuSelection: HarmonySingleton.shared.dashBoardData.data?.profileMenu?.charity?.menuName ?? "", value: 0)
            menuHidden()
        }else if indexPath.row == 4 {
            self.selectionDelegate.didTapSelect(menuSelection: HarmonySingleton.shared.dashBoardData.data?.profileMenu?.privacyPolicy?.menuName ?? "", value: 0)
            menuHidden()
        }else if indexPath.row == 5 {
            self.selectionDelegate.didTapSelect(menuSelection: HarmonySingleton.shared.dashBoardData.data?.profileMenu?.termsOfUse?.menuName ?? "", value: 0)
            menuHidden()
        }
//        else if indexPath.row == 5 {
//            self.selectionDelegate.didTapSelect(menuSelection: "Support", value: 0)
//            menuHidden()
//        }
        else if indexPath.row == 6 {
            UserDefaults.standard.set(false, forKey: "ShowFirstTime")
            UserDefaults.standard.synchronize()
            self.selectionDelegate.didTapSelect(menuSelection: "Tour", value: 0)
            menuHidden()
        }
        
        
        //        { (completed: Bool) in
        //            self.selectionDelegate.didTapSelect(menuSelection: setting.name, description: "Your Selection: \(setting.name)",value: 0)
        //        }
    }
    
}

protocol SelectionDelegate {
    func didTapSelect(menuSelection: String, value: CGFloat)
}



class SettingCell: BaseCell {
    
    var localslideInHandler = SooninSlideInHandler()
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ?  UIColor.darkGray : UIColor.clear
            nameLabel.textColor = isHighlighted ? UIColor.white : UIColor.black
            //iconImageView.tintColor = isHighlighted ? UIColor.white : UIColor.darkGray
            //print(isHighlighted)
        }
    }
    
 
    
    let nameLabel:UILabel = {
        let label = UILabel()
        label.text = "setting"
        label.font = ConstantString.textFieldFont
        label.textColor = UIColor(red: 19.0/255.0, green: 23.0/255.0, blue: 23.0/255.0, alpha: 1.0)
        return label
    }()
    
    let iconImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Settings32x32")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor =  localslideInHandler.menuCellsBackColor //UIColor.blue
        addSubview(nameLabel)
        addSubview(iconImageView)
        
        addConstraintsWithFormat("H:|-15-[v0(20)]-8-[v1]", views: iconImageView,nameLabel)
        addConstraintsWithFormat("V:|[v0]|", views: nameLabel)
        addConstraintsWithFormat("V:[v0(20)]", views: iconImageView)
        
        addConstraint(NSLayoutConstraint(item: iconImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        //        //build dinctionary of views
        //        let viewsDict = ["labelName": labelName,
        //                         "iconImage": iconImage];
        //
        //
        //        let menuHorizontal = NSLayoutConstraint.constraints(
        //            withVisualFormat: "H:|[labelName]|",
        //            options: [], metrics: nil, views: viewsDict)
        //        let menuVertical = NSLayoutConstraint.constraints(
        //            withVisualFormat: "V:|[labelName]|",
        //            options: [], metrics: nil, views: viewsDict)
        //        addConstraints(menuHorizontal)
        //        addConstraints(menuVertical)
        
        
    }
    
    
}


class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
    }
}


enum MenuLocationOption {
    case TOP,Bottom,Right,Left
}


extension UIView {
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: [], metrics: nil, views: viewsDictionary))
    }
}

