//
//  SideMenuFooterCollectionViewCell.swift
//  FitKet
//
//  Created by vaish navi on 10/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

class SideMenuFooterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var privacyPolicybtn: UIButton!
    @IBOutlet weak var termsAndConditionbtn: UIButton!
    @IBOutlet weak var corporateLoginbtn: UIButton!
    @IBOutlet weak var corporateLoginbtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topborderview: UIView!
    @IBOutlet weak var btnVersion: UIButton!


    static let sideMenuFooterID = "SideMenuFooterCollectionViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnVersion.setTitle("Version - \(Utilities.appVersionCode())", for: .normal)

    }

}
