//
//  SideMenuHeaderCollectionViewCell.swift
//  FitKet
//
//  Created by vaish navi on 10/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

class SideMenuHeaderCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet var backView: UIView!
    @IBOutlet var profileImg: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var levelLbl: UILabel!
    @IBOutlet var accountLbl: UILabel!
    @IBOutlet var logoutBtn: UIButton!
    @IBOutlet var profileviewWidthConstraint :NSLayoutConstraint!
    
    @IBOutlet weak var topviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dividerviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomviewHeightConstraint: NSLayoutConstraint!

     static let sideMenuHeaderID = "SideMenuHeaderCollectionViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        profileImg.layer.cornerRadius = profileImg.frame.height/2
//        profileImg.layer.masksToBounds  = true
        
    }
    
    
    @IBAction func logoutAct(_ sender: Any) {
        
        
    }
    
}
