//
//  GUID_Model.swift
//  Harmoney
//
//  Created by Mac on 05/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct FPL_GUID_Model:Codable {
    var intCode:String?
    var strMessage:String?
    var response:FPL_GUID_Response?
    
    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
      intCode <- map["intCode"]
      strMessage <- map["strMessage"]
      response <- map["response"]
    }
    
}

struct FPL_GUID_Response:Codable {
    var guid:String?
    var profile:FPL_GUID_Profile?
    
    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
      guid <- map["guid"]
      profile <- map["profile"]
    }
}


struct FPL_GUID_Profile:Codable {
    var strGender:String?
    var strDOB:String?
    var intHeight:String?
    var strWeight:String?
    var strDisplayName:String?
    var strFirstName:String?
    var strLastName:String?
    var strProfilePicLink:String?
    var leagueInfo:[FPL_GUID_League_Array]?
    var leagueArray:[FPL_GUID_League_Array]?
    var eventInfo:Corporate_Event_Info?
    var eventArray:[Corporate_Event_Array]?
    var corporates:[Corporates_list]?
    var userInfo:userInfo?


    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
      strGender <- map["strGender"]
         strDOB <- map["strDOB"]
         intHeight <- map["intHeight"]
         strWeight <- map["strWeight"]
         strDisplayName <- map["strDisplayName"]
         strFirstName <- map["strFirstName"]
         strLastName <- map["strLastName"]
         strProfilePicLink <- map["strProfilePicLink"]
         leagueInfo <- map["leagueInfo"]
        leagueArray <- map["leagueArray"]
         eventInfo <- map["eventInfo"]
         eventArray <- map["eventArray"]
         corporates <- map["corporates"]
         userInfo <- map["userInfo"]

    }
    
}


struct FPL_GUID_League_Info:Codable {
    
    var leagueStatus:String?
    var leagueId:String?
    var leagueName:String?
    var leagueLogo:String?
    var leagueSponsorLogo:String?
    var leagueRunningImageUrl:String?
    var teamId:String?
    var leagueStartInfo:String?
    var organizedText : String?
    var isSingleTeam : Bool?
    var isJoinNow : Bool?
    var isJoint: Bool?
    
    

    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
      leagueStatus <- map["leagueStatus"]
        leagueId <- map["leagueId"]
        leagueName <- map["leagueName"]
        leagueLogo <- map["leagueLogo"]
        leagueSponsorLogo <- map["leagueSponsorLogo"]
        leagueRunningImageUrl <- map["leagueRunningImageUrl"]
        teamId <- map["teamId"]
        leagueStartInfo <- map["leagueStartInfo"]
        organizedText <- map["organizedText"]
        isSingleTeam <- map["isSingleTeam"]
        isJoinNow <- map["isJoinNow"]
        isJoint <- map["isJoint"]
        
    }
    
    
}

struct userInfo:Codable {

    var strTeamId:String?
    var strTeamFlag:String?
    var strTeamName:String?
    var strSponsorFlag:String?
    var strNgoFlag:String?
    var intTotalRun:Int?
    var intTotalKarmaPoints:Int?

    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
      strTeamId <- map["strTeamId"]
        strTeamFlag <- map["strTeamFlag"]
        strTeamName <- map["strTeamName"]
        strSponsorFlag <- map["strSponsorFlag"]
        strNgoFlag <- map["strNgoFlag"]
        intTotalRun <- map["intTotalRun"]
        intTotalKarmaPoints <- map["intTotalKarmaPoints"]

    }
    
    
}


struct Corporate_Event_Info:Codable {
    
    var eventStatus:String?
    var eventId:String?
    var eventName:String?
    var eventLogo:String?
    var eventSponsorLogo:String?
    var eventDesc:String?
    var eventRules:String?
    var eventStateDate:String?
    var eventStateTime:String?
    var eventEndTime:String?
    var selectedEventTypes:Int?
    var selectedEventTypesText:String?
    var eventCurrentStatus:Int?
    var eventCompletedStatus:String?
    var eventTarget:Int?
    var eventTargetCheckMeter:Int?
    var eventTargetReached:String?
    var rank:Int?
    var rankInt:Int?
    var isCorporateEvent:Bool?
    var eventTypes:[eventTypes]?
    var raceType :String?

    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
      eventStatus <- map["eventStatus"]
        eventId <- map["eventId"]
        eventName <- map["eventName"]
        eventLogo <- map["eventLogo"]
        eventSponsorLogo <- map["eventSponsorLogo"]
        eventDesc <- map["eventDesc"]
        eventRules <- map["eventRules"]
        eventStateDate <- map["eventStateDate"]
        eventStateTime <- map["eventStateTime"]
        eventEndTime <- map["eventEndTime"]
        selectedEventTypes <- map["selectedEventTypes"]
        selectedEventTypesText <- map["selectedEventTypesText"]
        eventCurrentStatus <- map["eventCurrentStatus"]
        eventCompletedStatus <- map["eventCompletedStatus"]
        eventTarget <- map["eventTarget"]
        eventTargetCheckMeter <- map["eventTargetCheckMeter"]
        eventTargetReached <- map["eventTargetReached"]
        rank <- map["rank"]
        rankInt <- map["rank"]
        isCorporateEvent <- map["isCorporateEvent"]
        eventTypes <- map["eventTypes"]
        raceType <- map["raceType"]

    }
    
    
}

struct eventTypes:Codable {
    
    var text:String?
    var value:String?
    
    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
      text <- map["text"]
      value <- map["value"]
    }
}
struct FPL_GUID_League_Array:Codable {
    
    var leagueStatus:String?
    var leagueId:String?
    var leagueName:String?
    var leagueLogo:String?
    var leagueSponsorLogo:String?
    var leagueRunningImageUrl:String?
    var teamId:String?
    var leagueStartInfo:String?
    var organizedText : String?
    var isSingleTeam : Bool?
    var isJoinNow : Bool?
    var isJoint: Bool?
    var intTotalRun: String?
    var intTotalKarmaPoints: String?
    var strNgoFlag: String?
    var strTeamFlag: String?
    
    

    init?(map: Map) {}
    init(leagueStatus: String?,leagueId: String?,leagueName: String?,leagueLogo: String?,leagueSponsorLogo: String?,leagueRunningImageUrl: String?,teamId: String?,leagueStartInfo: String?,organizedText: String?,isSingleTeam: Bool?,isJoinNow: Bool?,isJoint: Bool?,intTotalRun: String?,intTotalKarmaPoints :String?,strNgoFlag: String?,strTeamFlag: String?){
          
          self.leagueStatus = leagueStatus
          self.leagueId = leagueId
          self.leagueName = leagueName
          self.leagueLogo = leagueLogo
          self.leagueSponsorLogo = leagueSponsorLogo
          self.leagueRunningImageUrl = leagueRunningImageUrl
          self.teamId = teamId
          self.leagueStartInfo = leagueStartInfo
          self.organizedText = organizedText
          self.isSingleTeam = isSingleTeam
          self.isJoinNow = isJoinNow
          self.isJoint = isJoint
          self.intTotalRun = intTotalRun
          self.intTotalKarmaPoints = intTotalKarmaPoints
          self.strNgoFlag = strNgoFlag
        self.strTeamFlag = strTeamFlag

      }
    
    mutating func mapping(map: Map) {
      leagueStatus <- map["leagueStatus"]
        leagueId <- map["leagueId"]
        leagueName <- map["leagueName"]
        leagueLogo <- map["leagueLogo"]
        leagueSponsorLogo <- map["leagueSponsorLogo"]
        leagueRunningImageUrl <- map["leagueRunningImageUrl"]
        teamId <- map["teamId"]
        leagueStartInfo <- map["leagueStartInfo"]
        organizedText <- map["organizedText"]
        isSingleTeam <- map["isSingleTeam"]
        isJoinNow <- map["isJoinNow"]
        isJoint <- map["isJoint"]
        intTotalRun <- map["intTotalRun"]
        intTotalKarmaPoints <- map["intTotalKarmaPoints"]
        strNgoFlag <- map["strNgoFlag"]
        strTeamFlag <- map["strTeamFlag"]
    }
    
    
}

struct Corporate_Event_Array:Codable {
    
    var eventStatus:String?
    var eventId:String?
    var eventName:String?
    var eventLogo:String?
    var eventSponsorLogo:String?
    var eventDesc:String?
    var eventRules:String?
    var eventStateDate:String?
    var eventStateTime:String?
    var eventEndTime:String?
    var selectedEventTypes:Int?
    var selectedEventTypesText:String?
    var eventCurrentStatus:Int?
    var eventCompletedStatus:String?
    var eventTarget:Int?
    var eventTargetCheckMeter:Int?
    var eventTargetReached:String?
    var rank:Int?
    var rankInt:Int?
    var eventTypes:[eventTypes]?
    var isCorporateEvent:Bool?
    var raceType :String?

    init?(map: Map) {}
    init(eventStatus: String?,eventId: String?,eventName: String?,eventLogo: String?,eventSponsorLogo: String?,eventDesc: String?,eventRules: String?,eventStateDate: String?,eventStateTime: String?,eventEndTime: String?,selectedEventTypes: Int?,selectedEventTypesText: String?,eventCurrentStatus: Int?,eventCompletedStatus: String?,eventTarget: Int?,eventTargetCheckMeter: Int?,eventTargetReached: String?,rank: Int?,rankInt: Int?,eventTypes: [eventTypes]?,isCorporateEvent: Bool?, raceType :String?){
        
        self.eventStatus = eventStatus
        self.eventId = eventId
        self.eventName = eventName
        self.eventLogo = eventLogo
        self.eventSponsorLogo = eventSponsorLogo
        self.eventDesc = eventDesc
        self.eventRules = eventRules
        self.eventStateDate = eventStateDate
        self.eventStateTime = eventStateTime
        self.eventEndTime = eventEndTime
        self.selectedEventTypes = selectedEventTypes
        self.selectedEventTypesText = selectedEventTypesText
        self.eventCurrentStatus = eventCurrentStatus
        self.eventCompletedStatus = eventCompletedStatus
        self.eventTarget = eventTarget
        self.eventTargetCheckMeter = eventTargetCheckMeter
        self.eventTargetReached = eventTargetReached
        self.rank = rank
        self.rankInt = rankInt
        self.eventTypes = eventTypes
        self.isCorporateEvent = isCorporateEvent
        self.raceType = raceType

    }

    mutating func mapping(map: Map) {
      eventStatus <- map["eventStatus"]
        eventId <- map["eventId"]
        eventName <- map["eventName"]
        eventLogo <- map["eventLogo"]
        eventSponsorLogo <- map["eventSponsorLogo"]
        eventDesc <- map["eventDesc"]
        eventRules <- map["eventRules"]
        eventStateDate <- map["eventStateDate"]
        eventStateTime <- map["eventStateTime"]
        eventEndTime <- map["eventEndTime"]
        selectedEventTypes <- map["selectedEventTypes"]
        selectedEventTypesText <- map["selectedEventTypesText"]
        eventCurrentStatus <- map["eventCurrentStatus"]
        eventCompletedStatus <- map["eventCompletedStatus"]
        eventTarget <- map["eventTarget"]
        eventTargetCheckMeter <- map["eventTargetCheckMeter"]
        eventTargetReached <- map["eventTargetReached"]
        rank <- map["rank"]
        rankInt <- map["rankInt"]
        eventTypes <- map["eventTypes"]
        isCorporateEvent <- map["isCorporateEvent"]
        raceType <- map["raceType"]

    }
    
    
}

struct Corporates_list:Codable {
    
    var leagueStatus:String?
    var leagueId:String?
    var leagueName:String?
    var leagueLogo:String?
    var leagueSponsorLogo:String?
    var isCorporateLeague:Bool?
    var leagueStartInfo:String?
    var organizedText:String?
    var isSingleTeam:Bool?
    var isJoinNow : Bool?
    var leagueRunningImageUrl:String?
    var isJoint : Bool?
    

    
    
    
    init?(map: Map) {}
    init(leagueStatus: String?,leagueId: String?,teamId: String?,leagueName: String?,leagueLogo: String?,leagueSponsorLogo: String?,isCorporateLeague: Bool?,leagueStartInfo: String?,organizedText:String?,isSingleTeam:Bool?,isJoinNow:Bool?,isJoint:Bool,leagueRunningImageUrl:String?){
        
        self.leagueStatus = leagueStatus
        self.leagueId = leagueId
        self.leagueName = leagueName
        self.leagueLogo = leagueLogo
        self.leagueSponsorLogo = leagueSponsorLogo
        self.isCorporateLeague = isCorporateLeague
        self.leagueStartInfo = leagueStartInfo
        self.organizedText = organizedText
        self.isSingleTeam = isSingleTeam
        self.isJoinNow = isJoinNow
        self.isJoint = isJoint
        self.leagueRunningImageUrl = leagueRunningImageUrl

    }
    
    mutating func mapping(map: Map) {
        leagueStatus <- map["leagueStatus"]
        leagueId <- map["leagueId"]
        leagueName <- map["leagueName"]
        leagueLogo <- map["leagueLogo"]
        leagueSponsorLogo <- map["leagueSponsorLogo"]
        isCorporateLeague <- map["isCorporateLeague"]
        leagueStartInfo <- map["leagueStartInfo"]
        organizedText <- map["organizedText"]
        isSingleTeam <- map["isSingleTeam"]
        isJoinNow <- map["isJoinNow"]
        isJoint <- map["isJoint"]
        leagueRunningImageUrl <- map["leagueRunningImageUrl"]

        
    }
}

struct eventType:Codable {
    
    var text:String?
    var value:String?
    
    init?(map: Map) {}
    init() {}
    
    mutating func mapping(map: Map) {
      text <- map["text"]
      value <- map["value"]
    }
}

