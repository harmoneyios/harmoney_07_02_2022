//
//  CMSModel.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 13/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

//guard let data = response.data else { return }
//let jsonObj = try? JSONDecoder().decode(CMSModel.self, from: data)


import UIKit

struct CMSModel: Decodable {
    let intCode: Int?
    let strMessage: String?
    let data : MetaData?
    
    enum CodingKeys:String,CodingKey {
        case intCode
        case strMessage
        case data
    }
}

struct MetaData: Decodable {
    let Login : Login?
    
    enum CodingKeys : String,CodingKey {
        case Login
    }
}



struct Login: Decodable {
    let title1 : String?
    let subTitle1 : String?
    let desc1 : String?
    let button1 : String?
    let footer1 : String?
    let footer2 : String?
    
    enum CodingKeys: String,CodingKey {
        case title1
        case subTitle1
        case desc1
        case button1
        case footer1
        case footer2
        
    }
}


