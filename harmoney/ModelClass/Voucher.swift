//
//  Voucher.swift
//  Harmoney
//
//  Created by Csongor Korosi on 8/21/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

// MARK: - Voucher
struct Voucher: Codable {
    var data: [VoucherData]
}

// MARK: - Datum
struct VoucherData: Codable {
    var vID: String?
    let vType: Int?
    let voucherFrom, voucherName: String?
    let voucherValue: Int?
    let voucherCode: String?
    let voucherImage: String?

    enum CodingKeys: String, CodingKey {
        case vID = "vId"
        case vType, voucherFrom, voucherName, voucherValue, voucherCode, voucherImage
    }
}
