//
//  checkChallengeStatus.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 06/07/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation

struct ChallengeStatusData: Codable {
    var response: ChallengeStatusList
}

struct ChallengeStatusList: Codable {
    var isTaskAlreadyRunning: Bool
    var taskType: Int
    var taskId: String?
    var taskTitle: String?
    var taskSubTitle: String?
    var taskImage: String?
    var displayMessage: String?
    
    enum CodingKeys: String, CodingKey {
        case isTaskAlreadyRunning = "isTaskAlreadyRunning"
        case taskType = "taskType"
        case taskId = "taskId"
        case taskTitle = "taskTitle"
        case taskSubTitle =  "taskSubTitle"
        case taskImage = "taskImage"
        case displayMessage = "displayMessage"
    }
}

