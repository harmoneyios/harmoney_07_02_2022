//
//  DailyChallenge.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/21/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

public enum DailyChallengeStatus: Int, Codable {
    case notStarted = 0
    case accepted = 1
    case completed = 2
    case rewards = 3
    case whatsNext = 4
    case none = 5
}

struct DailyChallengeList: Codable {
    var data: [DailyChallenge]
}

struct DailyChallenge: Codable {
    let id, challengeID: String
    let challengeCode, challengeType, challegeTaskType: Int
    let taskValue: String
    var taskCurrentValue: Int?
//    let taskDuration: String?
    let title, subTitle: String
    let image: String
    var xp, hBucks :Int
    var gem : String?
    var shareTags: String?
    let challengeStatus: Int
//    let challegePostType : Int?
    let createdDateTime: String
    var userCompletionStatus: DailyChallengeStatus
    let position: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case challengeID = "challengeId"
        case challengeCode, challengeType, challegeTaskType, taskValue, /*taskCurrentValue, taskDuration,*/ title, subTitle, image, xp, hBucks, gem, /*challegePostType,*/ shareTags, challengeStatus, createdDateTime, userCompletionStatus, position
    }
}
