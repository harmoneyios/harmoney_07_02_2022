//
//  DashBoardModel.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import ObjectMapper

public enum RegistrationType: String {
    case child = "1"
    case parent = "2"
    case adult = "3"
}

struct DashBoardModel: Mappable {
   var status : Int?
   var message : String?
   var data : DashBoardData?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        status    <- map["status"]
        message    <- map["message"]
        data    <- map["data"]
    }
}

struct DashBoardData : Mappable {
    var name : String?
    var email : String?
    var onboardingChallengeCompletionStatus : Bool?
    var userType : RegistrationType?
    var currentChallenge : CurrentChallenge?
    var challenges : [ChallengeData]?
    var notificationCount : Int?
    var newNotificationCount : Int?
    var notification : [NotificationDash]?
    var points : Points?
    var inviteFriendGroup : Int?
    var inviteFriendGroupAccept : Int?
    var collectMysteryBox : Int?
    var challengeShareStatus : Int?
    var invitePoints : [InvitePoints]?
    var levelCompletionPoints : Int?
    var personalFitnessSettings : [PersonalFitnessSettings]?
    var systemMessages : [SystemMessages]?
    var invite: InviteData?
    var bankAccount: String?
    var parentApproval : ParentApproval?
    var kidsRequest : Bool?
    var invitedRequest : [InvitedChildRequest]?
    var profileMenu : profileMenuData?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        name    <- map["name"]
        email    <- map["email"]
        onboardingChallengeCompletionStatus    <- map["onboardingChallengeCompletionStatus"]
        userType    <- map["userType"]
        challenges <- map["challenges"]
        notificationCount    <- map["notificationCount"]
        email    <- map["email"]
        userType    <- map["userType"]
        currentChallenge    <- map["currentChallenge"]
        challenges    <- map["challenges"]
        notificationCount    <- map["notificationCount"]
        notification    <- map["notification"]
        inviteFriendGroup    <- map["inviteFriendGroup"]
        inviteFriendGroupAccept    <- map["inviteFriendGroupAccept"]
        collectMysteryBox    <- map["collectMysteryBox"]
        challengeShareStatus    <- map["challengeShareStatus"]
        invitePoints    <- map["invitePoints"]
        levelCompletionPoints    <- map["levelCompletionPoints"]
        personalFitnessSettings    <- map["personalFitnessSettings"]
        systemMessages    <- map["systemMessages"]
        points    <- map["points"]
        bankAccount <- map["backAccount"]
        parentApproval <- map["parentApproval"]
        kidsRequest <- map["kidsRequest"]
        invitedRequest <- map["invitedRequest"]
        profileMenu <- map["profileMenu"]
        newNotificationCount <- map["newNotificationCount"]
    }
}
struct profileMenuData : Mappable {
    var about : MenuItem?
    var charity : MenuItem?
    var privacyPolicy : MenuItem?
    var termsOfUse: MenuItem?
    var faq: MenuItem?
    var support: MenuItem?
    var settings_permission : MenuItem?
    var myorder : MenuItem?
    var rewards : MenuItem?
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        about  <- map["about"]
        charity  <- map["charity"]
        privacyPolicy  <- map["privacyPolicy"]
        termsOfUse  <- map["termsOfUse"]
        faq <- map["faq"]
        support  <- map["support"]
        settings_permission  <- map["settings_permission"]
        myorder  <- map["myorder"]
        rewards  <- map["rewards"]
    }
}

struct MenuItem : Mappable {
    var isActive : Bool?
    var menuName : String?
    var link : String?
    var content: String?

    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        isActive  <- map["isActive"]
        menuName  <- map["menuName"]
        link  <- map["link"]
        content  <- map["content"]
    }
}


struct NotificationModel : Mappable{
    var status : Int?
    var message : String?
    var arrNotifications : [NotificationDash]?
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        status    <- map["status"]
        message    <- map["message"]
        arrNotifications    <- map["data"]
    }
    
}
struct NotificationDash : Mappable {
    var message : String?
    var type : Int?
    var createdDateTime : String?
    var request: InvitedRequest?
    var profilePictureURL: String?
    var emoji: Emoji?
    var status: Int?
    var id : String?
    var isRead: Int?
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id  <- map["_id"]
        message  <- map["message"]
        type  <- map["type"]
        createdDateTime  <- map["createdDateTime"]
        request  <- map["request"]
        profilePictureURL <- map["profilePic"]
        emoji  <- map["emoji"]
        status  <- map["status"]
        isRead  <- map["isRead"]
    }
}

struct Emoji: Mappable {
    var hexCode: String?
    var uniCode: String?
    
    init?(map: Map) {}
       
    mutating func mapping(map: Map) {
        hexCode  <- map["hexCode"]
        uniCode  <- map["uniCode"]
    }
}

struct InvitedRequest: Mappable {
    var type: String?
    var mobile: String?
    var requestUserGuid: String?
    var memberId: String?
    var taskId: String?
    var kidName: String?
    var status: Int?
    var taskFor: Int?
    var rewardName: String?
    var level: Int?
    var rewardId: String?
    
    init?(map: Map) {}
       
    mutating func mapping(map: Map) {
        type  <- map["type"]
        mobile  <- map["mobile"]
        requestUserGuid  <- map["requestUserGuid"]
        memberId  <- map["memberId"]
        taskId <- map["taskId"]
        kidName <- map["kidName"]
        status <- map["status"]
        taskFor <- map["taskFor"]
        rewardName <- map["rewardName"]
        level <- map["level"]
        rewardId <- map["rewardId"]
    }
}

struct PersonalFitnessSettings: Mappable {
    var taskyType : String?
    var dropdown : DropDownModel?
    var points : Points?

    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        taskyType  <- map["taskyType"]
        dropdown  <- map["dropdown"]
        points  <- map["points"]
    }
}

struct DropDownModel : Mappable {
    var MinCount : Int?
    var MaxCount : Int?
    var stepsCount : Int?
    var unit : String?

    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
         MinCount  <- map["MinCount"]
         MaxCount  <- map["MaxCount"]
         stepsCount  <- map["stepsCount"]
         unit  <- map["unit"]
    }
}

struct Points : Mappable {
    var xp : Int?
    var hbucks : Int?
    var jem : Int?
    var level : Int?
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
         xp  <- map["xp"]
         hbucks  <- map["hBucks"]
         jem  <- map["jem"]
         level <- map["level"]
    }
}

struct InvitePoints: Mappable {
    var cid : Int?
    var points : Int?
    var hbucks : Int?

    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
         cid  <- map["cid"]
         points  <- map["points"]
         hbucks  <- map["hbucks"]
    }
}

struct SystemMessages: Mappable {
    var code : Int?
    var msg : String?
    var module : String?
    var screen : String?
    var field : String?
    var type : String?

    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
         code  <- map["code"]
         msg  <- map["msg"]
         module  <- map["module"]
         screen  <- map["screen"]
         field  <- map["field"]
         type  <- map["type"]
    }
}

//struct EachChallenge : Mappable {
//       var cId : String?
//       var cName : String?
//       var cType : String?
//       var cStartDateTime : String?
//       var cEndDateTime : String?
//       var cDurationInHours : String?
//       var cStatus : String?
//       var cInfo : DashCInfo?
//       var points : Points?
//    
//    init?(map: Map) {}
//    
//    mutating func mapping(map: Map) {
//        cId    <- map["cId"]
//        cName    <- map["cName"]
//        cType    <- map["cType"]
//        cStartDateTime    <- map["cStartDateTime"]
//        cEndDateTime    <- map["cEndDateTime"]
//        cDurationInHours    <- map["cDurationInHours"]
//        cStatus    <- map["cStatus"]
//        cInfo    <- map["cInfo"]
//        points    <- map["points"]
//    }
//}

struct CurrentChallenge : Mappable {
    var currentChllangeId : String?
    var currentChllangeStatus : String?
    var currentChllangeAccptedDateTime : String?

     init?(map: Map) {}
       
       mutating func mapping(map: Map) {
            currentChllangeId  <- map["currentChllangeId"]
            currentChllangeStatus  <- map["currentChllangeStatus"]
            currentChllangeAccptedDateTime  <- map["currentChllangeAccptedDateTime"]
       }
}



struct DashCInfo : Mappable {
    var cCompletedCount : Int?
    var taskCount : String?
    var lastUpdateDateTime : String?
    var cUnit : String?
    var cType : String?

    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
         cCompletedCount  <- map["cCompletedCount"]
         taskCount  <- map["taskCount"]
         lastUpdateDateTime  <- map["lastUpdateDateTime"]
        cUnit  <- map["cUnit"]
        cType  <- map["cType"]
    }
}



public enum ParentApproval: Int {
    case notApproved = 0
    case approved = 1
        
}


struct InvitedChildRequest : Mappable {
    var memberId : String?
    var memberPhoto : String?
    
    var memberName_encrypted : String?
    var memberMobile_encrypted : String?
    var memberRelation_encrypted : String?
    var isPrimary : Int?
    var memberAccept : Int?
    var memberEmail_encrypted :String?
    var isAcceptReject :Bool?
    var fromGuid :String?

    var memberName : String?{
        return getDecryptedString(cipherText: self.memberName_encrypted)
    }
    var memberMobile : String?{
        return getDecryptedString(cipherText: self.memberMobile_encrypted)
    }
    var memberRelation : String?{
        return getDecryptedString(cipherText: self.memberRelation_encrypted)
    }
    var memberEmail :String?{
        return getDecryptedString(cipherText: self.memberEmail_encrypted)
    }

    init?(map: Map) {}
    
    mutating func mapping(map: Map) {        
        memberId    <- map["memberId"]
        memberPhoto  <- map["memberPhoto"]
        memberName_encrypted  <- map["memberName"]
        memberMobile_encrypted  <- map["memberMobile"]
        memberRelation_encrypted  <- map["memberRelation"]
        memberAccept  <- map["memberAccept"]
        isPrimary  <- map["isPrimary"]
        memberEmail_encrypted  <- map["memberEmail"]
        isAcceptReject  <- map["isAcceptReject"]
        fromGuid  <- map["fromGuid"]
    }
}


