//
//  ChoreSubcategoryModel.swift
//  Harmoney
//
//  Created by Mac on 20/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

//import Foundation
//import UIKit
//import ObjectMapper
//struct ChoresSubCategoryListModel: Codable {
//    let chorselist: [ChoreSubCategoryModel]
//}
//
//struct ChoreSubCategory: Codable {
//    var data: ChoreSubCategoryModel
//}
//struct ChoreSubCategoryModel : Codable {
//    var chorseType : Int?
//    var chorseTaskId : String?
//    var chorserName : String?
//    var chorserSubTitle : String?
//    var choserImage : String?
//
//    enum CodingKeys : String, CodingKey {
//        case chorseType = "chorseType"
//        case chorseTaskId = "chorseTaskId"
//        case chorserName = "chorserName"
//        case chorserSubTitle = "chorserSubTitle"
//        case choserImage = "choserImage"
//    }
//}

import Foundation
import ObjectMapper

class ChoreSubCategoryModel: Mappable {
    var data : ChoresData?
    

    required init?(map: Map){
    }

    func mapping(map: Map) {
        data <- map["data"]
    }
}

class ChoresData :  Mappable{
    var chorselist: [chorselist]?
    required init?(map: Map){
    }

    func mapping(map: Map) {
        chorselist <- map["chorselist"]
    }
}
class chorselist: Mappable {

    var chorseType : Int?
    var chorseTaskId : String?
    var chorserName : String?
    var chorserSubTitle : String?
    var choserImage : String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        chorseType <- map["chorseType"]
        chorseTaskId <- map["chorseTaskId"]
        chorserName <- map["chorserName"]
        chorserSubTitle <- map["chorserSubTitle"]
        choserImage <- map["choserImage"]
    }
}



