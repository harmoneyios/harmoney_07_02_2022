//
//  LeagueRegistrationInfo.swift
//  Harmoney
//
//  Created by Saravanan on 08/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper
struct LeagueRegistrationInfo : Codable,Mappable {
    var isActive : Bool?
    var labelName : String?
    var dropdownValues : [String]?
    var isMandatory : Bool?
    var isEditOrDropdown : Int?
    var inputType : Int?
    var formKey : String?

    enum CodingKeys: String, CodingKey {

        case isActive = "isActive"
        case labelName = "labelName"
        case dropdownValues = "dropdownValues"
        case isMandatory = "isMandatory"
        case isEditOrDropdown = "isEditOrDropdown"
        case inputType = "inputType"
        case formKey = "formKey"
    }
    init?(map: Map) {

    }
    mutating func mapping(map: Map) {
        isActive <- map["isActive"]
        labelName <- map["labelName"]
        dropdownValues <- map["dropdownValues"]
        isMandatory <- map["isMandatory"]
        isEditOrDropdown <- map["isEditOrDropdown"]
        inputType <- map["inputType"]
        formKey <- map["formKey"]
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        labelName = try values.decodeIfPresent(String.self, forKey: .labelName)
        dropdownValues = try values.decodeIfPresent([String].self, forKey: .dropdownValues)
        isMandatory = try values.decodeIfPresent(Bool.self, forKey: .isMandatory)
        isEditOrDropdown = try values.decodeIfPresent(Int.self, forKey: .isEditOrDropdown)
        inputType = try values.decodeIfPresent(Int.self, forKey: .inputType)
        formKey = try values.decodeIfPresent(String.self, forKey: .formKey)
    }

    init(isActive : Bool,labelName:String,dropdownValues:[String],isMandatory:Bool,isEditOrDropdown:Int,inputType:Int,formKey:String) {
        self.isActive = isActive
        self.labelName = labelName
        self.dropdownValues = dropdownValues
        self.isMandatory = isMandatory
        self.isEditOrDropdown = isEditOrDropdown
        self.inputType = inputType
        self.formKey = formKey
    }
    
}


