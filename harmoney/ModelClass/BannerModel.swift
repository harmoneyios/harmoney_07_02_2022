//
//  BannerModel.swift
//  Harmoney
//
//  Created by INQ Projects on 18/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper
// MARK: - BannerModel
struct BannerModel: Codable {
    let intCode: Int
    let strMessage: String
    let response: BannerResponse
}

// MARK: - Response
struct BannerResponse: Codable {
    let guid: String
    let profile: BannerProfile
}

// MARK: - Profile
struct BannerProfile: Codable {
    let leagueInfo: [BannerLeagueInfo]
}

// MARK: - LeagueInfo
struct BannerLeagueInfo: Codable {
    
    
    let leagueStatus: String
    let isJoint: Bool
    let name, strLeagueLink, fromDate, toDate: String
    let strSponsorFlag, strTeamID, strTeamFlag, strTeamName: String
    let strNgoFlag: String
    let intTotalRun: Int
    let intTotalKarmaPoints: Int
    let leagueID, teamID, leagueName, leagueLogo: String
    let leagueSponsorLogo: String
//    let pass_code_background: String
    let isJoinNow: Bool
    let leagueStateDate, leagueStartInfo, leagueEndDate, leagueStateTime,leagueStartDate: String
    let leagueEndTime, organizedText: String
    let isSingleTeam: Bool
    let leagueRunningImageURL,leagueBackgroundImage, startInformation: String
    let weburl: String
    let bg_images : Bg_images?
    let imgRegisterBackground : String?
    let footer_text : String?
    let isShowSteps : String?
    let isShowGoals : String?
    let isShowBtnLeaderboard : String?
    let isShowBtnCharity : String?
    let my_charity_link : String?
    let league_logo_navigation_link : String?
//    let sponsor_logo_navigation_link : String?
    let leagueRegistrationInfo: [LeagueRegistrationInfo]?
    var currentLeagueInfo : [CurrentLeagueInfo]?
    let intTotalSteps : Int?
    let intTotalKms : Float?
    let intTotalGoals : Int?
    let intTotalDonation : Int?
    let intTotalMember : Int?
    let isShowDonation : Bool?
    let isLeagueVerify : Int?
    let leagueBeforJoiningBgImage : String?
    let leagueAfterJoiningBgImage : String?
    var strSubTeamId : String?
    var strSubTeamName : String?
    var strSubTeamFlag : String?
    var add2KStepsConfirmation : Bool?
  
    
    enum CodingKeys: String, CodingKey {
        case leagueStatus, isJoint, name, strLeagueLink, fromDate, toDate, strSponsorFlag
        case strTeamID = "strTeamId"
        case strTeamFlag, strTeamName, strNgoFlag, intTotalRun, intTotalKarmaPoints
        case leagueID = "leagueId"
        case teamID = "teamId"
        case leagueName, leagueLogo, leagueSponsorLogo, isJoinNow, leagueStateDate, leagueStartInfo, leagueEndDate, leagueStateTime, leagueEndTime, organizedText, isSingleTeam, leagueStartDate
        case leagueRunningImageURL = "leagueRunningImageUrl"
        case startInformation,leagueBackgroundImage, weburl
        case leagueRegistrationInfo
       case currentLeagueInfo = "currentLeague"
        case isLeagueVerify,leagueBeforJoiningBgImage, leagueAfterJoiningBgImage
//        case pass_code_background = "pass_code_background"
        
        case intTotalSteps = "intTotalSteps"
        case intTotalKms = "intTotalKms"
        case intTotalGoals = "intTotalGoals"
        case intTotalDonation = "intTotalDonation"
        case intTotalMember = "intTotalMember"
        case isShowDonation = "isShowDonation"
        
        case strSubTeamId = "strSubTeamId"
        case strSubTeamName = "strSubTeamName"
        case strSubTeamFlag = "strSubTeamFlag"
        case add2KStepsConfirmation = "add2KStepsConfirmation"
        case bg_images = "bg_images"
        case imgRegisterBackground
        case my_charity_link
        case footer_text,isShowGoals,isShowSteps,isShowBtnLeaderboard,isShowBtnCharity
        case league_logo_navigation_link

//        case sponsor_logo_navigation_link,league_logo_navigation_link
    }
    
    
    
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        bg_images = try values.decodeIfPresent(Bg_images.self, forKey: .bg_images)
        leagueRegistrationInfo = try values.decodeIfPresent([LeagueRegistrationInfo].self, forKey: .leagueRegistrationInfo)
        leagueStatus = try values.decodeIfPresent(String.self, forKey: .leagueStatus) ?? ""
        isJoint = try values.decodeIfPresent(Bool.self, forKey: .isJoint) ?? false
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        strLeagueLink = try values.decodeIfPresent(String.self, forKey: .strLeagueLink) ?? ""
        fromDate = try values.decodeIfPresent(String.self, forKey: .fromDate) ?? ""
        toDate = try values.decodeIfPresent(String.self, forKey: .toDate) ?? ""
        strSponsorFlag = try values.decodeIfPresent(String.self, forKey: .strSponsorFlag) ?? ""
        strTeamID = try values.decodeIfPresent(String.self, forKey: .strTeamID) ?? ""
        strTeamFlag = try values.decodeIfPresent(String.self, forKey: .strTeamFlag) ?? ""
        strTeamName = try values.decodeIfPresent(String.self, forKey: .strTeamName) ?? ""
        strNgoFlag = try values.decodeIfPresent(String.self, forKey: .strNgoFlag) ?? ""
        intTotalRun = try values.decodeIfPresent(Int.self, forKey: .intTotalRun) ?? 0
        intTotalKarmaPoints = try values.decodeIfPresent(Int.self, forKey: .intTotalKarmaPoints) ?? 0
        leagueID = try values.decodeIfPresent(String.self, forKey: .leagueID) ?? ""
        teamID = try values.decodeIfPresent(String.self, forKey: .teamID) ?? ""
        leagueName = try values.decodeIfPresent(String.self, forKey: .leagueName) ?? ""
        leagueLogo = try values.decodeIfPresent(String.self, forKey: .leagueLogo) ?? ""
        leagueSponsorLogo = try values.decodeIfPresent(String.self, forKey: .leagueSponsorLogo) ?? ""
        isJoinNow = try values.decodeIfPresent(Bool.self, forKey: .isJoinNow) ?? false
        leagueStateDate = try values.decodeIfPresent(String.self, forKey: .leagueStateDate) ?? ""
        leagueEndDate = try values.decodeIfPresent(String.self, forKey: .leagueEndDate) ?? ""
        leagueStartInfo = try values.decodeIfPresent(String.self, forKey: .leagueStartInfo) ?? ""
        leagueStateTime = try values.decodeIfPresent(String.self, forKey: .leagueStateTime) ?? ""
        leagueEndTime = try values.decodeIfPresent(String.self, forKey: .leagueEndTime) ?? ""
        organizedText = try values.decodeIfPresent(String.self, forKey: .organizedText) ?? ""
        isSingleTeam = try values.decodeIfPresent(Bool.self, forKey: .isSingleTeam) ?? false
        leagueRunningImageURL = try values.decodeIfPresent(String.self, forKey: .leagueRunningImageURL) ?? ""
        leagueBackgroundImage = try values.decodeIfPresent(String.self, forKey: .leagueBackgroundImage) ?? ""
        startInformation = try values.decodeIfPresent(String.self, forKey: .startInformation) ?? ""
        weburl = try values.decodeIfPresent(String.self, forKey: .weburl) ?? ""
        currentLeagueInfo = try values.decodeIfPresent([CurrentLeagueInfo].self, forKey: .currentLeagueInfo)
        leagueStartDate = try values.decodeIfPresent(String.self, forKey: .leagueStartDate) ?? ""
        
        intTotalSteps = try values.decodeIfPresent(Int.self, forKey: .intTotalSteps)
        intTotalKms = try values.decodeIfPresent(Float.self, forKey: .intTotalKms)
        intTotalGoals = try values.decodeIfPresent(Int.self, forKey: .intTotalGoals)
        intTotalDonation = try values.decodeIfPresent(Int.self, forKey: .intTotalDonation)
        intTotalMember = try values.decodeIfPresent(Int.self, forKey: .intTotalMember)
        isShowDonation = try values.decodeIfPresent(Bool.self, forKey: .isShowDonation)
        
        isLeagueVerify = try values.decodeIfPresent(Int.self, forKey: .isLeagueVerify)
        leagueAfterJoiningBgImage = try values.decodeIfPresent(String.self, forKey: .leagueAfterJoiningBgImage)
        leagueBeforJoiningBgImage = try values.decodeIfPresent(String.self, forKey: .leagueBeforJoiningBgImage)
        
        strSubTeamId = try values.decodeIfPresent(String.self, forKey: .strSubTeamId) ?? ""
        strSubTeamName = try values.decodeIfPresent(String.self, forKey: .strSubTeamName) ?? ""
        strSubTeamFlag = try values.decodeIfPresent(String.self, forKey: .strSubTeamFlag) ?? ""
        
        add2KStepsConfirmation = try values.decodeIfPresent(Bool.self, forKey: .add2KStepsConfirmation)
        imgRegisterBackground = try values.decode(String.self, forKey: .imgRegisterBackground)
        footer_text = try values.decode(String.self, forKey: .footer_text)
        isShowSteps = try values.decode(String.self, forKey: .isShowSteps)
        isShowGoals = try values.decode(String.self, forKey: .isShowGoals)
        isShowBtnLeaderboard = try values.decode(String.self, forKey: .isShowBtnLeaderboard)
        isShowBtnCharity = try values.decode(String.self, forKey: .isShowBtnCharity)
        my_charity_link = try values.decode(String.self, forKey: .my_charity_link)
        league_logo_navigation_link = try values.decode(String.self, forKey: .league_logo_navigation_link)
//        sponsor_logo_navigation_link = try values.decode(String.self, forKey: .sponsor_logo_navigation_link)
//        pass_code_background = try values.decode(String.self, forKey: .pass_code_background)
    }
    
    var currentLeague : CurrentLeagueInfo?{
        return currentLeagueInfo?.first
    }
   
    mutating func setCurrentLeagueInfo(currentLeague:CurrentLeagueInfo){
        if currentLeagueInfo == nil{
            currentLeagueInfo = [CurrentLeagueInfo]()
            currentLeagueInfo?.append(currentLeague)
            
        }
    }
    
    mutating func setSubTeamValue(teamId: String?,teamName: String?, teamFlag: String?){
        self.strSubTeamName = teamName
        self.strSubTeamId = teamId
        self.strSubTeamFlag = teamFlag
    }
    
}




struct CurrentLeagueInfo : Codable,Mappable {
    var leagueId : String?
    var teamId : String?
    var subteamId : String?

    enum CodingKeys: String, CodingKey {

        case leagueId = "leagueId"
        case teamId = "teamId"
        case subteamId = "subteamId"
    }
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        leagueId <- map["leagueId"]
        teamId <- map["teamId"]
        subteamId <- map["subteamId"]
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        leagueId = try values.decodeIfPresent(String.self, forKey: .leagueId)
        teamId = try values.decodeIfPresent(String.self, forKey: .teamId)
        subteamId = try values.decodeIfPresent(String.self, forKey: .subteamId)
    }

    init(leagueId : String,teamId:String,subteamId:String) {
        self.leagueId = leagueId
        self.teamId = teamId
        self.subteamId = subteamId
    }
}

struct Bg_images : Codable
{
   
    let before_image: before_image?
    let after_join_before_event_start: after_join_before_event_start?
    let during_event_unachieved: during_event_unachieved?
    let during_event_achieved: during_event_achieved?
    let after_event_joined: after_event_joined?
    let after_event_without_joined: after_event_without_joined?
    
    enum CodingKeys: String, CodingKey {

        case before_image = "before_image"
        case after_join_before_event_start = "after_join_before_event_start"
        case during_event_unachieved = "during_event_unachieved"
        case during_event_achieved = "during_event_achieved"
        case after_event_joined = "after_event_joined"
        case after_event_without_joined = "after_event_without_joined"
    }
    
    
}


struct before_image : Codable
{
    var banner_url : String?
    var navigation_url : String?
    
    enum CodingKeys: String, CodingKey {
        case banner_url = "banner_url"
        case navigation_url = "navigation_url"
    }
    
    
}

struct after_join_before_event_start : Codable
{
    var banner_url : String?
    var navigation_url : String?
    
    enum CodingKeys: String, CodingKey {
        case banner_url = "banner_url"
        case navigation_url = "navigation_url"
    }
    
}

struct during_event_unachieved : Codable
{
    var banner_url : String?
    var navigation_url : String?
    enum CodingKeys: String, CodingKey {
        case banner_url = "banner_url"
        case navigation_url = "navigation_url"
    }
    
    
}

struct during_event_achieved : Codable
{
    var banner_url : String?
    var navigation_url : String?
    enum CodingKeys: String, CodingKey {
        case banner_url = "banner_url"
        case navigation_url = "navigation_url"
    }
    
    
}
struct after_event_joined : Codable
{
    var banner_url : String?
    var navigation_url : String?
    
    enum CodingKeys: String, CodingKey {
        case banner_url = "banner_url"
        case navigation_url = "navigation_url"
    }
    
    
}

struct after_event_without_joined : Codable
{
    var banner_url : String?
    var navigation_url : String?
    enum CodingKeys: String, CodingKey {
        case banner_url = "banner_url"
        case navigation_url = "navigation_url"
    }
    
}
