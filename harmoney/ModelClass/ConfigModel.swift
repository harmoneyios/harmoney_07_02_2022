//
//  ConfigModel.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 25/07/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

//struct ConfigModel : Codable {
//
//    let data : ConfigData
//}
//struct ConfigData: Codable {
//    let gamification : [Gamification]
//}
//struct  Gamification : Codable {
//    let Level : Int
//    let levelName : String
//    let levelIcon : String
//    let hBucks : Int
//    let jem : Int
//    let Runs : Run
//}
//
//struct Run : Codable {
//    let min : Int
//    let max : Int
//}


//--------------------------
// MARK: - Welcome
struct ConfigModel: Codable {
    let status: Int
    let message: String
    let data: ConfigData
}

// MARK: - DataClass
struct ConfigData: Codable {
    let source: [String]
    let gamification: [Gamification]
    let appUpdate: AppUpdate
    let walkthrough: Walkthrough
    let creditType: Int

}

// MARK: - AppUpdate
struct AppUpdate: Codable {
    let version, type, message: String
    let url: String
}

// MARK: - Gamification
struct Gamification: Codable {
    let Level: Int
    let levelName, levelIcon: String
    let Runs: Run
    let hBucks, jem, levelXP: Int
    let levelUpXP: JSONNull?
    let levelReachXP: Int
    let giftCard: ConfigGiftCard?

    enum CodingKeys: String, CodingKey {
        case Level = "Level"
        case levelName, levelIcon
        case Runs = "Runs"
        case hBucks, jem
        case levelXP = "levelXp"
        case levelUpXP = "levelUpXp"
        case levelReachXP = "levelReachXp"
        case giftCard
    }
}

// MARK: - GiftCard
struct ConfigGiftCard: Codable {
    let giftCardID: Int
    let giftCardName: String
    let giftCardImageURL: String

    enum CodingKeys: String, CodingKey {
        case giftCardID = "giftCardId"
        case giftCardName
        case giftCardImageURL = "giftCardImageUrl"
    }
}

// MARK: - Runs
struct Run: Codable {
    let min, max: Int
}
// MARK: - Walkthrough
struct Walkthrough: Codable {
    let levelCountSpotlight : levelCountSpotlight
    let xbCountSpotlight : xbCountSpotlight
    let harmoneyCountSpotlight : harmoneyCountSpotlight
    let soccerCountSpotlight : soccerCountSpotlight
    let notificationCountSpotlight : notificationCountSpotlight
    let stepCountSpotlight : stepCountSpotlight
    let milesCountSpotlight : milesCountSpotlight
    let caloriesCountSpotlight : caloriesCountSpotlight
    let bannerSpotlight : bannerSpotlight
    let profile: profile
    let hbot: hBot
    let home : home
    let earn : earn
    let spend : spend
    let stuff : stuff


}
// MARK: - Walkthrough
struct levelCountSpotlight: Codable {
    let headingStr, contentStr: String
}
struct xbCountSpotlight: Codable {
    let headingStr, contentStr: String
}
struct harmoneyCountSpotlight: Codable {
    let headingStr, contentStr: String
}
struct soccerCountSpotlight: Codable {
    let headingStr, contentStr: String
}
struct notificationCountSpotlight: Codable {
    let headingStr, contentStr: String
}
struct stepCountSpotlight: Codable {
    let headingStr, contentStr: String
}
struct milesCountSpotlight: Codable {
    let headingStr, contentStr: String
}
struct caloriesCountSpotlight: Codable {
    let headingStr, contentStr: String
}
struct bannerSpotlight: Codable {
    let headingStr, contentStr: String
}
struct home: Codable {
    let headingStr, contentStr: String
}
struct earn: Codable {
    let headingStr, contentStr: String
}
struct spend: Codable {
    let headingStr, contentStr: String
}
struct stuff: Codable {
    let headingStr, contentStr: String
}
struct profile: Codable {
    let headingStr, contentStr: String
}
struct hBot: Codable {
    let headingStr, contentStr: String
}
// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

