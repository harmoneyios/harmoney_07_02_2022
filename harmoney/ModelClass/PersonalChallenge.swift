//
//  PersonalChallenge.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/20/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import UIKit

public enum ChallengeType: String, Codable {
    case walk = "Walk"
    case run = "Run"
    case swim = "Swim"
    case bike = "Bike"
}

public enum ChallengeAcceptType: Int, Codable {
    case padding = 0
    case accept = 1
    case decline = 2
}

public enum ChallengeStatus: Int, Codable {
    case stopped = 0
    case started = 1
}

//Personal Challenge model
struct PersonalChallengeDataList: Codable {
    let data: [PersonalChallengeData]
}

struct PersonalChallengeData: Codable {
    var challengeId: String
    var challengeType: ChallengeType
    var challengeTask: Int
    var experiance: Int
    var harmoneyBucks: Int?
    let diamond: Int
    var isCompleted: Int
    var challengeProgress: Float?
    var challengeRunStep: Float?
    var aprovedId: String?
    var challengeTagTo: String?
    var challengeTagValue: String?
    var customReward: String?
    var createdDate: String
    var isClaimed: Int?
    var updatedTime: String?
    var challengeAcceptType: ChallengeAcceptType?
    //var challengeApproved: Int?
    var show: Int?
    var isStarted: ChallengeStatus?
    var taskFrom: taskFrom?
    var taskTo: taskTo?
    var  noAction: Bool
    var isAccepted: Int?

    
    enum CodingKeys: String, CodingKey {
        case challengeId = "tId"
        case challengeType = "tType"
        case challengeTask = "tCount"
        case experiance = "tXP"
        case harmoneyBucks = "tHBucks"
        case diamond = "tGem"
        case isCompleted = "tIsCompleted"
        case challengeProgress = "tCurrentCounts"
        case challengeRunStep = "tRunStep"
        case aprovedId = "tApprovalGuid"
        case challengeTagTo = "tTagTo"
        case challengeTagValue = "tTagValue"
        case customReward = "tCustomRewards"
        case createdDate = "tCreatedDateTime"
        case isClaimed = "tIsClaimed"
        case challengeAcceptType = "tIsApproved"
        case isAccepted = "isAccepted"
        //case challengeApproved = "tIsApproved"
        case show = "tShow"
        case isStarted = "letGetStart"
        case updatedTime = "tUpdatedDateTime"
        case taskFrom = "taskFrom"
        case taskTo = "taskTo"
        case noAction = "noAction"
    }
}
struct taskFrom: Codable {
    var guid: String?
    var name_encrypted: String?
    var profilePic: String?
    var name: String?{
        return getDecryptedString(cipherText: name_encrypted)
    }
   

    enum CodingKeys: String, CodingKey {
        case guid = "guid"
        case name_encrypted = "name"
        case profilePic = "profilePic"
    }
}

struct taskTo: Codable {
    var guid: String?
    var name_encrypted: String?
    var profilePic: String?
    var name: String?{
        return getDecryptedString(cipherText: name_encrypted)
    }
   

    enum CodingKeys: String, CodingKey {
        case guid = "guid"
        case name_encrypted = "name"
        case profilePic = "profilePic"
    }
}

struct PersonalChallenge: Codable {
    var data: PersonalChallengeData
}

//Personal Challenge model after update
struct UpdatedPersonalChallengeData: Codable {
    var challengeProgress: Float?
    var updatedTime: String?
    var isCompleted: Int
    var challengeRunStep: Float?

    enum CodingKeys: String, CodingKey {
        case challengeProgress = "tCurrentCounts"
        case updatedTime = "tUpdatedDateTime"
        case isCompleted = "tIsCompleted"
        case challengeRunStep = "tRunStep"

    }
}

struct UpdatedPersonalChallenge: Codable {
    let data: UpdatedPersonalChallengeData
}

//Personal Challenge model after claimed reward
struct ClaimedPersonalChallengeData: Codable {
    var isClaimed: Int
    
    enum CodingKeys: String, CodingKey {
        case isClaimed = "tIsClaimed"
    }
}

struct ClaimedPersonalChallenge: Codable {
    let data: ClaimedPersonalChallengeData
}
