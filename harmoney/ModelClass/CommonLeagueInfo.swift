//
//  CommonLeagueInfo.swift
//  Harmoney
//
//  Created by Saravanan on 08/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation

struct CommonLeagueInfo : Codable {
    let isActive : Bool?
    let labelName : String?
    let dropdownValues : [String]?
    let isMandatory : Bool?
    let isEditOrDropdown : Int?
    let inputType : Int?
    let formKey : String?
    

    enum CodingKeys: String, CodingKey {

        case isActive = "isActive"
        case labelName = "labelName"
        case dropdownValues = "dropdownValues"
        case isMandatory = "isMandatory"
        case isEditOrDropdown = "isEditOrDropdown"
        case inputType = "inputType"
        case formKey = "formKey"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        labelName = try values.decodeIfPresent(String.self, forKey: .labelName)
        dropdownValues = try values.decodeIfPresent([String].self, forKey: .dropdownValues)
        isMandatory = try values.decodeIfPresent(Bool.self, forKey: .isMandatory)
        isEditOrDropdown = try values.decodeIfPresent(Int.self, forKey: .isEditOrDropdown)
        inputType = try values.decodeIfPresent(Int.self, forKey: .inputType)
        formKey = try values.decodeIfPresent(String.self, forKey: .formKey)
    }

}
