//

//  CouponToolsModel.swift
//  Harmoney
//
//  Created by INQ Projects on 08/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation

// MARK: - CouponToolsModel
struct CouponToolsModel: Codable {
    let status: Status
    let amountOfCampaigns: String
    let couponInfo: [CouponInfo]

    enum CodingKeys: String, CodingKey {
        case status
        case amountOfCampaigns = "amount_of_campaigns"
        case couponInfo = "coupon_info"
    }
}

// MARK: - CouponInfo
struct CouponInfo: Codable {
    let id, code: String
    let url: String
    let status, subaccount, name, title: String
    let subtitle, couponInfoDescription, createdate, startdateEnabled: String
    let startdate, startdateType, expirydate, expirydateType: String
    let timezone, terms, termsShort, poweredby: String
    let poweredbylink: String
    let couponType, couponTags, couponCategory, language: String
    let value, amountLimitation, amountLimitationType, couponCategoryName: String
    let logoURL: String
    let banner1, banner2, banner3, banner4: String
    let banner5: String
    let couponImage: String
    let locations: [Location]?
    let singleUse: String
    let amountUsed: String?
    let amountAvailable: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case code, url, status, subaccount, name, title, subtitle
        case couponInfoDescription = "description"
        case createdate
        case startdateEnabled = "startdate_enabled"
        case startdate
        case startdateType = "startdate_type"
        case expirydate
        case expirydateType = "expirydate_type"
        case timezone, terms
        case termsShort = "terms_short"
        case poweredby, poweredbylink
        case couponType = "coupon_type"
        case couponTags = "coupon_tags"
        case couponCategory = "coupon_category"
        case language, value
        case amountLimitation = "amount_limitation"
        case amountLimitationType = "amount_limitation_type"
        case couponCategoryName = "coupon_category_name"
        case logoURL = "logo_url"
        case banner1, banner2, banner3, banner4, banner5
        case couponImage = "coupon_image"
        case locations
        case singleUse = "single_use"
        case amountUsed = "amount_used"
        case amountAvailable = "amount_available"
    }
}

// MARK: - Location
struct Location: Codable {
    let id, companyName, companyEmail, companyPhone: String
    let companyWebsite, companyPassword, companyStreet, companyZip: String
    let companyCity, companyState, companyCountry, companyLat: String
    let companyLong, companyHours: String

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case companyName = "company_name"
        case companyEmail = "company_email"
        case companyPhone = "company_phone"
        case companyWebsite = "company_website"
        case companyPassword = "company_password"
        case companyStreet = "company_street"
        case companyZip = "company_zip"
        case companyCity = "company_city"
        case companyState = "company_state"
        case companyCountry = "company_country"
        case companyLat = "company_lat"
        case companyLong = "company_long"
        case companyHours = "company_hours"
    }
}

// MARK: - Status
struct Status: Codable {
    let status: String
}



// MARK: - CouponValidationModel
struct CouponValidationModel: Codable {
    let status: CouponValidationModelStatus
    let amountOfResults, amountOfPages: String
    let data: [validationDatum]

    enum CodingKeys: String, CodingKey {
        case status
        case amountOfResults = "amount_of_results"
        case amountOfPages = "amount_of_pages"
        case data
    }
}

// MARK: - YouroToken
struct YouroTokenModel: Codable {
  
    let refresh, access: String
   

    enum CodingKeys: String, CodingKey {
        case refresh = "refresh"
        case access = "access"
    }
}

// MARK: - YouroEmotionID
struct YouroTextEmotionModel: Codable {
  
    let id : String
   

    enum CodingKeys: String, CodingKey {
        case id = "id"
    }
}

// MARK: - Datum
struct validationDatum: Codable {
    let sequenceid, session: String
    let status: DatumStatus
    let user: User
    let campaign: Campaign
}

// MARK: - Campaign
struct Campaign: Codable {
    let id, code: String
    let url: String
    let status, couponTags: String

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case code, url, status
        case couponTags = "coupon_tags"
    }
}

//enum Code: String, Codable {
//    case the8Pwc88 = "8pwc88"
//}
//
//enum ID: String, Codable {
//    case cam1059503 = "cam_1059503"
//}
//
//enum StatusEnum: String, Codable {
//    case live = "live"
//}

// MARK: - DatumStatus
struct DatumStatus: Codable {
    let createdDateUTC, createdDate, opened, openDateUTC: String
    let openDate, played, playedDateUTC, claimed: String
    let claimDateUTC, claimDate, claimtype, savedToPassbook: String
    let savedToPassbookDateUTC, savedToAndroidpay, savedToAndroidpayDateUTC, validated: String
    let validationDateUTC, ownValidationCode, validationValue, moneySpent: String
    let checkboxAcceptTerms, checkbox1, checkbox2, checkbox3: String
    let multipleChoiceAnswer: JSONNull?
    let redeemLocationID, voided, locked, unlockedDateUTC: String
    let value, customid, expirationDate, timezone: String
    let utmCampaign, utmSource, utmMedium, utmTerm: JSONNull?
    let utmContent: JSONNull?

    enum CodingKeys: String, CodingKey {
        case createdDateUTC = "created_date_utc"
        case createdDate = "created_date"
        case opened
        case openDateUTC = "open_date_utc"
        case openDate = "open_date"
        case played
        case playedDateUTC = "played_date_utc"
        case claimed
        case claimDateUTC = "claim_date_utc"
        case claimDate = "claim_date"
        case claimtype = "claim_type"
        case savedToPassbook = "saved_to_passbook"
        case savedToPassbookDateUTC = "saved_to_passbook_date_utc"
        case savedToAndroidpay = "saved_to_androidpay"
        case savedToAndroidpayDateUTC = "saved_to_androidpay_date_utc"
        case validated
        case validationDateUTC = "validation_date_utc"
        case ownValidationCode = "own_validation_code"
        case validationValue = "validation_value"
        case moneySpent = "money_spent"
        case checkboxAcceptTerms = "checkbox_accept_terms"
        case checkbox1 = "checkbox_1"
        case checkbox2 = "checkbox_2"
        case checkbox3 = "checkbox_3"
        case multipleChoiceAnswer = "multiple_choice_answer"
        case redeemLocationID = "redeem_location_ID"
        case voided, locked
        case unlockedDateUTC = "unlocked_date_utc"
        case value, customid
        case expirationDate = "expiration_date"
        case timezone
        case utmCampaign = "utm_campaign"
        case utmSource = "utm_source"
        case utmMedium = "utm_medium"
        case utmTerm = "utm_term"
        case utmContent = "utm_content"
    }
}

//enum Claimtype: String, Codable {
//    case db = "db"
//    case instantoffer = "instantoffer"
//}
//
//enum ExpirationDate: String, Codable {
//    case the20210207000000 = "2021-02-07 00:00:00"
//}
//
//enum Timezone: String, Codable {
//    case utc = "UTC"
//}
//
//enum Valvalue: String, Codable {
//    case homeMadeBurgerFor2 = "Home made Burger for $2"
//}

// MARK: - User
struct User: Codable {
    let gender, firstName, lastName, address: String
    let zip, city, birthday, email: String
    let phone, answer, custom1, custom2: String
    let custom3, custom4, custom5, custom6: String
    let custom7, customid, checkbox1, checkbox2: String
    let checkbox3, checkbox4, device, profilePicture: String
    let ipLat, ipLong: String

    enum CodingKeys: String, CodingKey {
        case gender
        case firstName = "first_name"
        case lastName = "last_name"
        case address, zip, city, birthday, email, phone, answer, custom1, custom2, custom3, custom4, custom5, custom6, custom7, customid, checkbox1, checkbox2, checkbox3, checkbox4, device
        case profilePicture = "profile_picture"
        case ipLat = "ip_lat"
        case ipLong = "ip_long"
    }
}

enum Device: String, Codable {
    case ios = "IOS"
    case macintosh = "Macintosh"
}
// MARK: - CouponValidationModelStatus
struct CouponValidationModelStatus: Codable {
    let status: String
}



// MARK: - CouponPurchasedModel
struct CouponPurchasedModel: Codable {
    let staus: Int
    let message: String
    let data: PurchasedDataClass
}

// MARK: - DataClass
struct PurchasedDataClass: Codable {
    let orderID: String

    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
    }
}

// MARK: - PurchasedCouponClaimed
struct PurchasedCouponClaimed: Codable {
    let staus: Int
    let message: String
    let data: PurchasedClaimed
}

// MARK: - DataClass
struct PurchasedClaimed: Codable {
}



// MARK: - CouponPurchasedListModel
struct CouponPurchasedListModel: Codable {
    let staus: Int
    let message: String
    var data: [CouponPurchasedDatum]
}

// MARK: - Datum
struct CouponPurchasedDatum: Codable {
       let id: String
       let guid: String
       let orderID: String
       let status: Int
       let orderTime: String
       let shippingInfo: [JSONAny]
       let productList: CouponProductList
       let gem: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case guid
        case orderID = "orderId"
        case status, orderTime,shippingInfo, productList, gem
    }
}
//enum GUID: String, Codable {
//    case qufecmickx = "qufecmickx"
//}

// MARK: - ProductList
struct CouponProductList: Codable {
        let guid: String
        let couponPrice: String
        let logoURL: String
        let name: String
        let status: String
        let onboardingCoupon: Int?
        let collection: Collection

    enum CodingKeys: String, CodingKey {
        case guid
        case couponPrice = "coupon_price"
        case logoURL = "logo_url"
        case name, status, onboardingCoupon, collection
    }
}

// MARK: - Collection
struct Collection: Codable {
        let compainID, claimed, validated: String
        let url: String
        let name, title, subtitle, collectionDescription: String
        let createdate: String
        let startdate: String
        let expirydate: String
        let terms: String
        let termsShort: String
        let value: String
        let couponImage: String
        let amountLimitation: String
        let amountLimitationType: String
        let amountUsed: String?
        let amountAvailable: Int?
        let code: String?
        let session: String

    enum CodingKeys: String, CodingKey {
        case compainID, claimed, validated, url, name, title, subtitle,code,session
        case collectionDescription = "description"
        case createdate, startdate, expirydate, terms
        case termsShort = "terms_short"
        case value
        case couponImage = "coupon_image"
        case amountLimitation = "amount_limitation"
        case amountLimitationType = "amount_limitation_type"
        case amountUsed = "amount_used"
        case amountAvailable = "amount_available"
    }
}

enum AmountAvailable: Codable {
    case integer(Int)
    case string(String)
    case null

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if container.decodeNil() {
            self = .null
            return
        }
        throw DecodingError.typeMismatch(AmountAvailable.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for AmountAvailable"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        case .null:
            try container.encodeNil()
        }
    }
}

enum AmountLimitationType: String, Codable {
    case claim = "claim"
    case validate = "validate"
}

enum Expirydate: String, Codable {
    case expiryDate = "Expiry Date"
    case the20210207000000 = "2021-02-07 00:00:00"
    case the20210219000000 = "2021-02-19 00:00:00"
}

enum Startdate: String, Codable {
    case startDate = "Start Date "
    case the19700101000000 = "1970-01-01 00:00:00"
}

enum TermsShort: String, Codable {
    case empty = ""
    case shortTrems = "short trems"
    case termsConditionsTextShort = "Terms & conditions text short"
}

enum CouponPrice: Codable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(CouponPrice.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for CouponPrice"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}
class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}


class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }

}


// MARK: - PurchasedCouponClaimedList
struct PurchasedCouponClaimedList: Codable {
    let staus: Int
    let message: String
    let data: [DatumClaimed]
}

// MARK: - Datum
struct DatumClaimed: Codable {
    let id, guid, orderID: String
       let status: Int
       let orderTime: String
       let shippingInfo: [JSONAny]
       let productList: claimedProductList
       let gem: Int
       let claimedDateTime: String

       enum CodingKeys: String, CodingKey {
           case id = "_id"
           case guid
           case orderID = "orderId"
           case status, orderTime, shippingInfo, productList, gem, claimedDateTime
       }
}

// MARK: - ProductList
struct claimedProductList: Codable {
    let guid, couponPrice, status: String
       let logoURL: String
       let name: String
       let collection: Collection

       enum CodingKeys: String, CodingKey {
           case guid
           case couponPrice = "coupon_price"
           case status
           case logoURL = "logo_url"
           case name, collection
       }
}

// MARK: - Collection
struct claimedCollection: Codable {
    let terms, code, value, claimed: String
      let title, createdate, compainID, name: String
      let amountLimitation, amountUsed: String
      let couponImage: String
      let validated, subtitle, collectionDescription: String
      let amountAvailable: Int
      let amountLimitationType, expirydate: String
      let url: String
      let termsShort, session, startdate: String

      enum CodingKeys: String, CodingKey {
          case terms, code, value, claimed, title, createdate, compainID, name
          case amountLimitation = "amount_limitation"
          case amountUsed = "amount_used"
          case couponImage = "coupon_image"
          case validated, subtitle
          case collectionDescription = "description"
          case amountAvailable = "amount_available"
          case amountLimitationType = "amount_limitation_type"
          case expirydate, url
          case termsShort = "terms_short"
          case session, startdate
      }
}
