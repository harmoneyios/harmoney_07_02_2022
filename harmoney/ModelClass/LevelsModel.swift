//
//  LevelsModel.swift
//  Harmoney
//
//  Created by Csongor Korosi on 8/5/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

//// MARK: - LevelsModel
//struct LevelsModel: Codable {
//    var data: LevelData
//}
//
//// MARK: - DataClass
//struct LevelData: Codable {
//    var levelInfo: [LevelInfo]
//    let userInfo: UserInfo
//}
//
//// MARK: - LevelInfo
//struct LevelInfo: Codable {
//    let level: Int
//    let levelName: String
//    let xp: XP
//    let hBucks, jem, levelXP: Int
//    let levelReachXp : Int?
//    let reward: Reward?
//    let giftCard: GiftCard?
//    var levelClaimStatus: Bool
//
//    enum CodingKeys: String, CodingKey {
//        case level, levelName, xp, hBucks, jem
//        case levelXP = "levelXp"
//        case reward, giftCard, levelClaimStatus
//        case levelReachXp
//    }
//}
//
//// MARK: - GiftCard
//struct GiftCard: Codable {
//    let giftCardID: Int
//    let giftCardName: String
//    let giftCardImageURL: String
//
//    enum CodingKeys: String, CodingKey {
//        case giftCardID = "giftCardId"
//        case giftCardName
//        case giftCardImageURL = "giftCardImageUrl"
//    }
//}
//
//// MARK: - Reward
//struct Reward: Codable {
//    let rewardID, rewardName: String
//    let isApproved: Bool
//
//    enum CodingKeys: String, CodingKey {
//        case rewardID = "rewardId"
//        case rewardName, isApproved
//    }
//}
//
//// MARK: - XP
//struct XP: Codable {
//    let min, max: Int
//}
//
//// MARK: - UserInfo
//struct UserInfo: Codable {
//    let guid, name, userType: String
//    let profilePic: String
//    let gender, zip, mobile: String
//    let userCredits: UserCredits
//}
//
//// MARK: - UserCredits
//struct UserCredits: Codable {
//    let xp, levelXP, gem, hBucks: Int
//    let level: Int
//    let levelName: String
//    let spend, earn, lastEarnHBucks: Int
//
//    enum CodingKeys: String, CodingKey {
//        case xp
//        case levelXP = "levelXp"
//        case gem, hBucks, level, levelName, spend, earn, lastEarnHBucks
//    }
//}






// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)


// MARK: - Welcome
struct LevelsModel: Codable {
    let status: Int?
    let message: String?
    var data: LevelData
}

// MARK: - DataClass
struct LevelData: Codable {
    var levelInfo: [LevelInfo]
    let userInfo: UserInfo?
}

// MARK: - LevelInfo
struct LevelInfo: Codable {
    let level: Int?
    let levelName, levelIcon: String?
    let xp: XP?
    let hBucks, jem, levelXP, levelReachXP: Int?
    let reward: Reward?
    let giftCard: GiftCard?
    var levelClaimStatus: Bool?

    enum CodingKeys: String, CodingKey {
        case level, levelName, levelIcon, xp, hBucks, jem
        case levelXP = "levelXp"
        case levelReachXP = "levelReachXp"
        case reward, giftCard, levelClaimStatus
    }
}
// MARK: - Reward
struct Reward: Codable {
    let rewardID, rewardName: String?
    let isApproved: Bool?

    enum CodingKeys: String, CodingKey {
        case rewardID = "rewardId"
        case rewardName, isApproved
    }
}
// MARK: - GiftCard
struct GiftCard: Codable {
    let giftCardID: Int?
    let giftCardName: String?
    let giftCardImageURL: String?

    enum CodingKeys: String, CodingKey {
        case giftCardID = "giftCardId"
        case giftCardName
        case giftCardImageURL = "giftCardImageUrl"
    }
}

// MARK: - XP
struct XP: Codable {
    let min, max: Int?
}

// MARK: - UserInfo
struct UserInfo: Codable {
    let guid, name, userType, profilePic: String?
    let gender, zip, mobile: String?
    let userCredits: UserCredits?
}

// MARK: - UserCredits
struct UserCredits: Codable {
    let xp, levelXP, gem: Int?
    let level: Int?
    let levelName: String?
    let spend, lastEarnHBucks: Int?
    let earn: Int?
    let hBucks: Int?

    enum CodingKeys: String, CodingKey {
        case xp
        case levelXP = "levelXp"
        case gem, hBucks, level, levelName, spend, earn, lastEarnHBucks
    }
}

//// MARK: - Encode/decode helpers
//
//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
