//
//  LoginModel.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/8/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    let data: LoginModelData
}

// MARK: - DataClass
struct LoginModelData: Codable {
    let guid, authToken, otp: String
    let firstTimeUser: Int
}
