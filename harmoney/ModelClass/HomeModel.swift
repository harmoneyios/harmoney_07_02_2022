//
//  HomeModel.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/26/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

// MARK: - HomeModel
struct HomeModel: Codable {
    var data: HomeData
}

// MARK: - DataClass
struct HomeData: Codable {
    var name: String{
        return getDecryptedString(cipherText: self.name_encrypted) ?? ""
    }
    var name_encrypted: String?
     
    let profilePic: String
    let points: HomePoints
    let notificationCount: Int
    let challenges: [Trending]//[HomeChallenge]
    let trending: [Trending]
    
    enum CodingKeys: String, CodingKey {
        case profilePic, points, notificationCount, challenges, trending
       case name_encrypted = "name"
    }
}

// MARK: - Challenge
struct HomeChallenge: Codable {
    let typeName, task: String
    let taskStatus, type : Int
    let cID: String?
    let imageUrl: String

    enum CodingKeys: String, CodingKey {
        case type, typeName, task, taskStatus
        case cID = "cId"
        case imageUrl = "imageUrl"
        
    }
}

// MARK: - Points
struct HomePoints: Codable {
    let xp, levelXP, gem, hBucks: Int
    let level, spend, earn: Int
    let lastEarnHBucks: Int

    enum CodingKeys: String, CodingKey {
        case xp
        case levelXP = "levelXp"
        case gem, hBucks, level, spend, earn, lastEarnHBucks
    }
}

// MARK: - Trending
struct Trending: Codable {
    let type: Int
    let imageURL: String
    let typeName: String
    let task: String
    let taskStatus: Int

    enum CodingKeys: String, CodingKey {
        case type
        case imageURL = "imageUrl"
        case typeName, task, taskStatus
    }
}


