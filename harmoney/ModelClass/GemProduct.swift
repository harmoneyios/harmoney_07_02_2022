//
//  GemProduct.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/14/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

//MARK: - GemProduct
struct GemProduct: Codable {
    var data: DataClass
}

//MARK: - DataClass
struct DataClass: Codable {
    var coupons: Coupons
}

//MARK: - Coupons
struct Coupons: Codable {
    var nonPromoList: [NonPromoList]
}

//MARK: - NonPromoList
struct NonPromoList: Codable {
    let intCouponType, intProductID, intProductCategoryType: Int
    let strProductCategoryName: String
    let strTaskThumbnailImage: String
    let strProductOffValueText: String
    let intProductClaimType, intProductClaimValue, intStockStatus: Int

    enum CodingKeys: String, CodingKey {
        case intCouponType
        case intProductID = "intProductId"
        case intProductCategoryType, strProductCategoryName, strTaskThumbnailImage, strProductOffValueText, intProductClaimType, intProductClaimValue, intStockStatus
    }
}
