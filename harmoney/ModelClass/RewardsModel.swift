//
//  RewardsModel.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 01/08/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

//struct RewardsModel : Codable {
//    let data : [RewardsData]?
//}
//
//struct RewardsData : Codable {
//    let taskId : String?
//    let taskType : Int?
//    let taskName : String?
//    let task : String?
//    let RewardName : String?
//    let taskImage : String?
//    let level : Int?
//}

import Foundation

// MARK: - Welcome
struct RewardsModel: Codable {
    let intCode: Int
    let strMessage: String
    let data: [RewardsData]
}

// MARK: - Datum
struct RewardsData: Codable {
    let taskID: String
    let taskType: Int
    let taskName, task, rewardName: String
    let taskImage: String?
    let type, frequncy, completedDateTime: String
    let level: Int?
    let hbucks: String


    enum CodingKeys: String, CodingKey {
        case taskID = "taskId"
        case taskType, taskName, task
        case rewardName = "RewardName"
        case taskImage, type, frequncy, completedDateTime, level, hbucks
    }
}
