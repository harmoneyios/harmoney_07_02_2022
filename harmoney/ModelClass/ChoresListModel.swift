import Foundation
import ObjectMapper


//Chores
//-----------
//tIsCompleted
//
//0- inprogress
//1- Completed
//2- Reward collected( congratulation screen next button clicked)
//
//tIsApproved
//1-Approved the chores to begin(parent approve)
//2-Reject the chores to begin(parent reject)
//
//isAccepted
//1- Chores accepted to collect reward (parent approve)
//2- chores not accepted to collect reward(parent reject)


class ChoresListModel: Mappable {
    var data : [ChoresModel]?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        data <- map["data"]
    }
}

class ChoresModel: Mappable {
    
    //    var chorseType : Int?
    
    
    var guid : String?
    var task_when : String?
    var task_name : String?
    var task_started_date : String?
    var task_xp_value : String?
    var task_id : String?
    var task_gems_value : String?
    var isCompleted : Int?
    var task_subName : String?
    var id : String?
    var task_reward : String?
    var isActive : String?
    var chorse_id : String?
    var currentDay : Int?
    var harmoney_bucks : String?
    var invited_guid : String?
    var task_ended_date : String?
    var lastCompletedDay : String?
    var isApproved : Bool?
    var isAccepted : Int?
    var task_until : String?
    var task_image_url : String?
    var taskList : [TaskList]?
    var taskFrom : TaskFrom?
    var taskTo : TaskTo?
    var totalDays : Int?
    var currentDayStatus : Int?
    var completedDays : Int?
    var noAction : Bool?
    
    
    
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        
        
        guid <- map["guid"]
        task_when <- map["task_when"]
        task_name <- map["task_name"]
        task_started_date <- map["task_started_date"]
        task_xp_value <- map["task_xp_value"]
        task_id <- map["task_id"]
        task_gems_value <- map["task_gems_value"]
        isCompleted <- map["isCompleted"]
        task_subName <- map["task_subName"]
        id <- map["_id"]
        task_reward <- map["task_reward"]
        isActive <- map["isActive"]
        chorse_id <- map["chorse_id"]
        currentDay <- map["currentDay"]
        harmoney_bucks <- map["harmoney_bucks"]
        invited_guid <- map["invited_guid"]
        task_ended_date <- map["task_ended_date"]
        lastCompletedDay <- map["lastCompletedDay"]
        isApproved <- map["tIsApproved"]
        isAccepted <- map["isAccepted"]
        task_until <- map["task_until"]
        task_image_url <- map["task_image_url"]
        taskList <- map["taskList"]
        totalDays <- map["totalDays"]
        currentDayStatus <- map["currentDayStatus"]
        completedDays <- map["completedDays"]
        taskFrom <- map["taskFrom"]
        taskTo <- map["taskTo"]
        noAction <- map["noAction"]
        
    }
    
    
    
    class TaskList: Mappable {
        var date : String?
        var day : Int?
        var status : Int?
        var lastUpdatedDateTime : String?
        
        required init?(map: Map){
        }
        
        func mapping(map: Map) {
            date <- map["date"]
            day <- map["day"]
            status <- map["status"]
            lastUpdatedDateTime <- map["lastUpdatedDateTime"]
        }
        
    }
    
    class TaskTo: Mappable {
        var guid: String?
        
        var name_encrypted: String?
        
        var name: String?{
            return getDecryptedString(cipherText: self.name_encrypted)
        }
        var profilePic: String?
       
            required init?(map: Map){
            }

           func mapping(map: Map) {
            guid <- map["guid"]
            name_encrypted <- map["name"]
            profilePic <- map["profilePic"]
            }

    }
        
        class TaskFrom: Mappable {
        var guid: String?
            var name_encrypted : String?
            
            var name: String?{
                return getDecryptedString(cipherText: self.name_encrypted)
            }
        var profilePic: String?
       
            required init?(map: Map){
            }

           func mapping(map: Map) {
            guid <- map["guid"]
            name_encrypted <- map["name"]
            profilePic <- map["profilePic"]
            }

    }
}


