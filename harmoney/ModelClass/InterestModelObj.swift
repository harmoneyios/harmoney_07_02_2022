//
//  InterestModelObj.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 29/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import ObjectMapper

struct InterestModelObj : Mappable {
    
    var Category : [InterestCategory]?
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        Category    <- map["Category"]
    }
    

}

struct InterestCategory : Mappable {
    
    var CategoryID : String?
    var CategoryGuid : String?
    var CategoryCode : String?
    var CategoryName : String?
    var CategoryImgPath : String?
    var Item : [InterestItem]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        CategoryID    <- map["CategoryID"]
        CategoryGuid    <- map["CategoryGuid"]
        CategoryCode    <- map["CategoryCode"]
        CategoryName    <- map["CategoryName"]
        CategoryImgPath    <- map["CategoryImgPath"]
        Item    <- map["Item"]
    }
    
}

struct InterestItem : Mappable {
    var ItemID : String?
    var ItemGuid : String?
    var ItemCode : String?
    var CategoryCode : String?
    var ItemName : String?
    var ItemImgPath : String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
           ItemID    <- map["ItemID"]
           ItemGuid    <- map["ItemGuid"]
           ItemCode    <- map["ItemCode"]
           CategoryCode    <- map["CategoryCode"]
           ItemName    <- map["ItemName"]
           ItemImgPath    <- map["ItemImgPath"]
       }
}


//   MARK: - CurrentUserInterests Model


struct CurrentIntrestsModel : Mappable {
    var response : [ItemResponse]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        response    <- map["response"]
    }
}

struct ItemResponse: Mappable {
    var _id : String?
    var  cat_code : String?
    var createdDateTime : String?
    var guid : String?
    var isChecked : Bool?
    var item_code : String?
    var lastUpdatedDateTime : String?
    var sessionToken : String?
    var user_id : String?
       
       init?(map: Map) {}
       mutating func mapping(map: Map) {
           _id    <- map["_id"]
        cat_code    <- map["cat_code"]
        createdDateTime    <- map["createdDateTime"]
        guid    <- map["guid"]
        isChecked    <- map["isChecked"]
        item_code    <- map["item_code"]
        lastUpdatedDateTime    <- map["lastUpdatedDateTime"]
        sessionToken    <- map["sessionToken"]
        user_id    <- map["user_id"]
       }
}
