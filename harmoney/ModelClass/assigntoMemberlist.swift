//
//  assigntoMemberlist.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 08/07/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation

struct AssignMemberDataList: Codable {
    let data: [AssignMemberData]
}
//memberGender,memberRelation,memberZip,memberName,memberEmail,memberDOB,memberMobile,
struct AssignMemberData: Codable {
    
    var memberId: String?
    var memberName_encrypted: String?
    var memberPhoto: String?
    var memberMobile_encrypted: String?
    var memberRelation_encrypted: String
    var memberEmail_encrypted: String
    var isPrimary: Int?
    var memberZip_encrypted: String?
    var memberDOB_encrypted: String?
    var memberGender_encrypted: String?
    var approveUserCreateChallenge: Bool?
    var approveUserBuy: Bool?
    var approveUserShareCharity: Bool?
    var sendHbucksToUser: String?
    var isActive: Int?
    var memberAccept: Int?
    var earnedPoints: Int?
    var inviteSendDateTime: String?
    var isSelectAvathar : Bool? = false
    var fromGuid: String?
    var memberGender: String?{
        return getDecryptedString(cipherText: memberGender_encrypted)
    }
    var memberRelation: String{
        return getDecryptedString(cipherText: memberRelation_encrypted) ?? ""
    }

    var memberZip: String?{
        return getDecryptedString(cipherText: memberZip_encrypted)
    }

    var memberName: String?{
        return getDecryptedString(cipherText: memberName_encrypted)
    }

    var memberEmail: String{
        return getDecryptedString(cipherText: memberEmail_encrypted) ?? ""
    }

    var memberDOB: String?{
        return getDecryptedString(cipherText: memberDOB_encrypted)
    }

    var memberMobile: String?{
        return getDecryptedString(cipherText: memberMobile_encrypted)
    }

    
    
    enum CodingKeys: String, CodingKey {
        case memberId = "memberId"
        case memberName_encrypted = "memberName"
        case memberPhoto = "memberPhoto"
        case memberMobile_encrypted = "memberMobile"
        case memberRelation_encrypted = "memberRelation"
        case memberEmail_encrypted = "memberEmail"
        case memberZip_encrypted = "memberZip"
        case memberDOB_encrypted = "memberDOB"
        case memberGender_encrypted = "memberGender"
        case approveUserCreateChallenge = "approveUserCreateChallenge"
        case approveUserBuy = "approveUserBuy"
        case approveUserShareCharity = "approveUserShareCharity"
        case sendHbucksToUser = "sendHbucksToUser"
        case isActive = "isActive"
        case memberAccept = "memberAccept"
        case earnedPoints = "earnedPoints"
        case inviteSendDateTime = "inviteSendDateTime"
        case isSelectAvathar = "isSelectAvathar"
        case fromGuid = "fromGuid"
    }
}



