//
//  Invites.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/10/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

//Invites model
struct InviteDataList: Codable {
    let data: [InviteData]
}
//memberRelation,memberName,memberMobile,memberEmail

struct InviteData: Codable {
    var memberId: String?
    var memberPhoto: String?
    
    var memberName_encrypted: String?
    var memberMobile_encrypted: String?
    var memberRelation_encrypted: String?
    var memberEmail_encrypted: String?
    
    var isPrimary: Int?
    var inviteStatus: Int?
    
    var memberName: String?{
        return getDecryptedString(cipherText: self.memberName_encrypted)
    }
    var memberMobile: String?{
        return getDecryptedString(cipherText: self.memberMobile_encrypted)
    }
    var memberRelation: String?{
        return getDecryptedString(cipherText: self.memberRelation_encrypted)
    }
    var memberEmail: String?{
        return getDecryptedString(cipherText: self.memberEmail_encrypted)
    }
    
    
    enum CodingKeys: String, CodingKey {
        case memberId = "memberId"
        case memberName_encrypted = "memberName"
        case memberPhoto = "memberPhoto"
        case memberMobile_encrypted = "memberMobile"
        case memberRelation_encrypted = "memberRelation"
        case memberEmail_encrypted = "memberEmail"
        case inviteStatus = "memberAccept"
        case isPrimary = "isPrimary"
    }
}

