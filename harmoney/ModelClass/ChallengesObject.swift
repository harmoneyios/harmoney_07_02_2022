//
//  ChallengesObject.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import ObjectMapper

struct ChallengesObject: Mappable {

    var status : Int32?
    var message : String?
    var data : ChallengeData?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        status    <- map["status"]
        message    <- map["message"]
        data    <- map["data"]
    }

}

struct ChallengeData : Mappable {
    var cId : String?
    var cName : String?
    var cStartDateTime : String?
    var cEndDateTime : String?
    var cDurationInHours : String?
    var cContent : String?
    var cType : String?
    var cStartTime : String?
    var cInfo : ChallengeInfo?
    var points : ChallengePoints?
    var cStatus : Int?
    var taskCount: String?
    var url: String?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        cId    <- map["cId"]
        cName    <- map["cName"]
        cStartDateTime    <- map["cStartDateTime"]
        cEndDateTime    <- map["cEndDateTime"]
        cDurationInHours    <- map["cDurationInHours"]
        cContent    <- map["cContent"]
        cType    <- map["cType"]
        cStartTime    <- map["cStartTime"]
        cInfo    <- map["cInfo"]
        points    <- map["points"]
        cStatus <- map["cStatus"]
        taskCount <- map["taskCount"]
        url <- map["url"]
    }
    
}

struct ChallengePoints: Mappable {
    var xp : Int?
    var hBucks : Int?
    var jem : Int?
    
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
       xp    <- map["xp"]
       hBucks    <- map["hBucks"]
       jem    <- map["jem"]
    }
}

struct ChallengeInfo : Mappable {
    
    var taskType : String?
    var taskCount : String?
    var cUnit: String?
    var createdDateTime : String?
    var completedCount: String?
    
   init?(map: Map) {
   }
   mutating func mapping(map: Map) {
      taskType    <- map["taskType"]
      taskCount    <- map["taskCount"]
      cUnit    <- map["cUnit"]
      createdDateTime    <- map["createdDateTime"]
    completedCount <- map["cCompletedCount"]
   }
}






