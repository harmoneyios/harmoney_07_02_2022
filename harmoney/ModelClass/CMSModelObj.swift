//
//  CMSModel.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 13/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

//guard let data = response.data else { return }
//let jsonObj = try? JSONDecoder().decode(CMSModel.self, from: data)


import UIKit

struct CMSModel: Decodable {
    let intCode: Int?
    let strMessage: String?
    let data : MetaData?
    
    enum CodingKeys:String,CodingKey {
        case intCode
        case strMessage
        case data
    }
}

struct MetaData: Decodable {
    let Login : Login?
    let UserType : UserType?
    let Zip : Zip?
    let OTP : OTP?
    let Name : Name?
    let Notification : NotificationSPage?
    let Motion : Motion?
    let Congrats : Congrats?
    
    enum CodingKeys : String,CodingKey {
        case Login
        case UserType
        case Zip
        case OTP
        case Name
        case Notification
        case Motion
        case Congrats
    }
}

struct Login: Decodable {
    let title1 : String?
    let subTitle1 : String?
    let desc1 : String?
    let button1 : String?
    let footer1 : String?
    let footer2 : String?
    
    enum CodingKeys: String,CodingKey {
        case title1
        case subTitle1
        case desc1
        case button1
        case footer1
        case footer2
        
    }
}

struct OTP: Decodable {
    let title1 : String?
    let desc1 : String?
    let button1 : String?
    let link1 : String?
    let link2 : String?
    
    enum CodingKeys: String,CodingKey {
        case title1
        case desc1
        case button1
        case link1
        case link2
    }
    }

 struct Name: Decodable {
 let title1 : String?
 let desc1 : String?
 let button1 : String?
    
 enum CodingKeys: String,CodingKey {
     case title1
     case desc1
     case button1
    
 }
 }

 struct UserType: Decodable {
 let title1 : String?
 let desc1 : String?
 let button1 : String?
 let button2 : String?
 let button3 : String?
 let button4 : String?
    
 enum CodingKeys: String,CodingKey {
     case title1
     case desc1
     case button1
     case button2
     case button3
     case button4
    
 }
 }
 struct Zip: Decodable {
 let title1 : String?
 let desc1 : String?
 let button1 : String?
    
 enum CodingKeys: String,CodingKey {
     case title1
     case desc1
     case button1
    
 }
 }
 struct NotificationSPage : Decodable {
 let title1 : String?
 let desc1 : String?
 let button1 : String?
 let notificationText : String?
    
 enum CodingKeys: String,CodingKey {
     case title1
     case desc1
     case button1
     case notificationText
    
 }
 }
 struct Motion: Decodable {
 let title1 : String?
 let desc1 : String?
 let button1 : String?
 let button2 : String?
    
 enum CodingKeys: String,CodingKey {
     case title1
     case desc1
     case button1
     case button2
     
 }
 }
 struct Congrats: Decodable {
     let title1 : String?
     let subTitle1 : String?
     let desc1 : String?
     let desc2 : String?
     let button1 : String?
     
     enum CodingKeys: String,CodingKey {
         case title1
         case subTitle1
         case desc1
         case desc2
         case button1
         
     }
 }
