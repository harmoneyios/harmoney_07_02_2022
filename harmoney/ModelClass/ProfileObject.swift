//
//  ProfileObject.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/18/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import ObjectMapper

struct ProfileObject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : [UserProfileData]?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }

    struct UserProfileData : Mappable {
            var guid : String?
            var zip_encrypted : String?
            var name_encrypted : String?
            var lastname_encrypted : String?
            var email_encrypted : String?
            var gender_encrypted : String?
            var dateOfBirth_encrypted : String?
        var contact_encrypted : String?
        var mobile_encrypted: String?
        
            var zip: String?{
                return getDecryptedString(cipherText:self.zip_encrypted)
            }
            var id: String?
            var schoolName: String?
            var name: String?{
                return getDecryptedString(cipherText: self.name_encrypted)
            }
        var contact: String?{
            return getDecryptedString(cipherText: self.contact_encrypted)
        }
        
            var lastname : String?{
                return getDecryptedString(cipherText: self.lastname_encrypted)
            }
        
            var email: String?{
                return getDecryptedString(cipherText: self.email_encrypted)
                  
            }
            var mobile: String?{
                return getDecryptedString(cipherText: self.mobile_encrypted)
                  
            }
            var age: String?
            var gender: String?{
                return getDecryptedString(cipherText: self.gender_encrypted)
                   
            }
            var profilePic: String?
            var userType: String?
            var challenges : [ChallengeData]?
            var bankAccount : String?
            var bankName : String?
            var routingNumber : String?
        var dateOfBirth : String?{
            return getDecryptedString(cipherText: self.dateOfBirth_encrypted)
      
        }
            var formFields : [Array<Any>]?
            var stringData : String?
            var parentLinkPayment : Bool?

        
        
        init?(map: Map) {

        }
        mutating func mapping(map: Map) {
            contact_encrypted  <- map["contact"]
             guid          <- map["guid"]
            zip_encrypted   <- map["zip"]
             id            <- map["_id"]
             schoolName    <- map["schoolName"]
            name_encrypted         <- map["name"]
            lastname_encrypted      <- map["lastname"]
            email_encrypted         <- map["email"]
             mobile_encrypted        <- map["mobile"]
             age           <- map["age"]
            gender_encrypted        <- map["gender"]
             profilePic    <- map["profilePic"]
             userType      <- map["userType"]
            challenges    <- map["challenges"]
            bankAccount   <- map["bankAccountNumber"]
            bankName     <- map["bankName"]
            routingNumber  <- map["routingNumber"]
            dateOfBirth_encrypted   <- map["dob"]
            formFields  <- map[""]
            stringData  <- map[""]
            parentLinkPayment  <- map["parentLinkPayment"]

        }
    }


