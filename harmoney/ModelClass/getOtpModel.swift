//
//  getOtpModel.swift
//  Harmoney
//
//  Created by sureshkumar on 12/21/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper
class getOtpModel:Mappable {
    
    var otp: String? = ""
    var sessionToken: String? = ""
    var firstTimeUser: String? = ""
    var guid: String? = ""
    var profileStatus: String? = ""
    var notificationCount: String? = ""
    var authToken: String? = ""
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        otp <- map["otp"]
        sessionToken <- map["sessionToken"]
        firstTimeUser <- map["firstTimeUser"]
        guid <- map["guid"]
        profileStatus <- map["profileStatus"]
        notificationCount <- map["notificationCount"]
        authToken <- map["authToken"]
        
    }
    }
    
    


struct sendOtpModel: Mappable {
   var status : Int?
   var message : String?
   var data : sendOtpModelData?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        status    <- map["status"]
        message    <- map["message"]
        data    <- map["data"]
    }
}

class sendOtpModelData:Mappable {
    var deviceType: String? = ""
    var guid: String? = ""
    var profileUpdateStatus: String? = ""
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        deviceType <- map["deviceType"]
        guid <- map["guid"]
        profileUpdateStatus <- map["profileUpdateStatus"]
    }
}
