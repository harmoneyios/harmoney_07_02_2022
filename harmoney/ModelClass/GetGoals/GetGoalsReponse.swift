//
//  GetGoalsReponse.swift
//  Harmoney
//
//  Created by Saravanan on 22/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct GetGoalsReponse : Codable {
    let status : Int?
    let result : [GetGoalsResult]?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case result = "result"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        result = try values.decodeIfPresent([GetGoalsResult].self, forKey: .result)
    }

}



struct GetGoalsResult : Codable {
    let _id : String?
    let date : String?
    let guid : String?
    let todaySteps : Int?
    let todayRuns : Int?
    let todayKarmaCoins : Double?
    let todayCalories : Int?
    let todayRunsNew : Int?
    let todayKarmaCoinsNew : Int?
    let todayDonation : Int?
    let todayGoals : Int?
    let currentLeague : [CurrentLeagueInfo]?
    let mobileInputDateTime : String?
    let serverDateTime : String?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case date = "date"
        case guid = "guid"
        case todaySteps = "todaySteps"
        case todayRuns = "todayRuns"
        case todayKarmaCoins = "todayKarmaCoins"
        case todayCalories = "todayCalories"
        case todayRunsNew = "todayRunsNew"
        case todayKarmaCoinsNew = "todayKarmaCoinsNew"
        case todayDonation = "todayDonation"
        case todayGoals = "todayGoals"
        case currentLeague = "currentLeague"
        case mobileInputDateTime = "mobileInputDateTime"
        case serverDateTime = "serverDateTime"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        guid = try values.decodeIfPresent(String.self, forKey: .guid)
        todaySteps = try values.decodeIfPresent(Int.self, forKey: .todaySteps)
        todayRuns = try values.decodeIfPresent(Int.self, forKey: .todayRuns)
        todayKarmaCoins = try values.decodeIfPresent(Double.self, forKey: .todayKarmaCoins)
        todayCalories = try values.decodeIfPresent(Int.self, forKey: .todayCalories)
        todayRunsNew = try values.decodeIfPresent(Int.self, forKey: .todayRunsNew)
        todayKarmaCoinsNew = try values.decodeIfPresent(Int.self, forKey: .todayKarmaCoinsNew)
        todayDonation = try values.decodeIfPresent(Int.self, forKey: .todayDonation)
        todayGoals = try values.decodeIfPresent(Int.self, forKey: .todayGoals)
        currentLeague = try values.decodeIfPresent([CurrentLeagueInfo].self, forKey: .currentLeague)
        mobileInputDateTime = try values.decodeIfPresent(String.self, forKey: .mobileInputDateTime)
        serverDateTime = try values.decodeIfPresent(String.self, forKey: .serverDateTime)
    }

}

