//
//  ClaimedCoupon.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/22/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

// MARK: - ClaimedCoupons
struct ClaimedCoupons: Codable {
    var data: [Datum]
}

// MARK: - Datum
struct Datum: Codable {
    let intCouponType, intProductID, intProductCategoryType: Int
    let strProductTitle, strProductSubTitle: String
    let strTaskThumbnailImage: String
    let strProductOffValueText: String
    let intProductClaimValue, intProductTotalQty, intStockStatus: Int
    let orderID, orderTime: String
    let couponCode: String
    let valide: String

    enum CodingKeys: String, CodingKey {
        case intCouponType
        case intProductID = "intProductId"
        case intProductCategoryType, strProductTitle, strProductSubTitle, strTaskThumbnailImage, strProductOffValueText, intProductClaimValue, intProductTotalQty, intStockStatus
        case orderID = "orderId"
        case orderTime
        case couponCode
        case valide
    }
}
