//
//  ExtensionClass.swift
//  Ludi
//
//  Created by Prema Ravikumar on 07/03/19.
//  Copyright © 2019 Inq. All rights reserved.
//

import UIKit
import Toast_Swift

class ConstantVariable: NSObject {
    
    static let shared = ConstantVariable()
    
    //Device Info
    let device_id = "\((UIDevice.current.identifierForVendor?.uuidString)!)"
    let device_type = "\(UIDevice.current.model)"
    let os_type = "\(UIDevice.current.systemName)"
    let os_version = "\(UIDevice.current.systemVersion)"
    var push_token = "\((UIDevice.current.identifierForVendor?.uuidString)!)"
    var isRestOffer = false
}
