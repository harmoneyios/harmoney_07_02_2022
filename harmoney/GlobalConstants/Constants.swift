//
//  Constants.swift
//  Harmoney
//
//  Created by GOPI K on 14/03/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import UIKit

enum StoryBoard {
    static let Goal = "Goal"
    static let Chores = "Chore"
    
    case home
    
    var storyboard: UIStoryboard {
        switch self {
        case .home:
            return UIStoryboard(name: "Home", bundle: nil)
        }
    }
}
enum DateFormatType {
    static let joinTeam = "yyyy-MM-dd HH:mm:ss"
    static let iso8601 = "yyyy-MM-dd HH:mm:ss Z"
    static let filter = "dd-MMM-yyyy"
    static let full = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    static let saveGoalDate = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    static let message = "dd MMM yyyy h:mm a"
    static let phrFormat = "dd/MM/yyyy h:mm a"
    static let time = "h:mm a"
    static let contactDetails = "dd MMM, yyyy"
    static let weekDayShort = "EEEE"
    static let homeTopics = "d MMM yy"
    static let month = "MMM"
    static let day = "d"
    static let pickerDate = "yyyy-MM-dd"
    static let year = "yyyy"
    static let channelDate = "h:mm a - MMMM dd"
    static let harmoneyDate = "MMM dd,yyyy"
    static let formattedDate = "dd/MM/yyyy"
    static let hBotTime = "hh:mm a"
}

enum notifications {
    static let clearAllNotification = "clearAllNotification"
    static let challengeAcceptingType = "Mode of Challenge"
    static let challengeDescription = """
    Please choose your mode of challenge. If watch mode, choose cycling in workout app on your watch. \n
    make sure that, your watch is paired with your device(iPhone) and GPS is enabled
"""
    static let challengeSwimmingDescription = """
    Please choose your mode of challenge. If watch mode, choose "Open Water Swim" in workout app on your watch. \n
    make sure that, your watch is paired with your device(iPhone) and GPS is enabled
"""
    static let nothavePermissiontoUseSameNumber = "Mobile no already used for registration. Please enter an alternative mobile number"
    static let relationshipMust = "Please choose a relationship"
    static let mobileNumberAlreadyUser = "Mobile no already used for registration. Please enter an alternative mobile number"
    static let underProgress = "UnderProgress"
}
enum ServiceBaseUrl {
    static var baseURL = "http://api.harmoneydemo.com/"
}
enum ApiEndPoints{
    static let getCategoryChores = "getCatagory/2352352"
    static let getSubCatagoryofChores = "getSubCatagory/"
}

var isFromAddChallenge : Bool = false
var challengeHbotType : Int = 1
let maxReachCount : CGFloat = 2000
