//
//  MethodHelper.swift
//  Harmoney
//
//  Created by Saravanan on 21/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

let cryptoKey = "SEFSTU9ORVlBRE1JTg=="
let cryptoIV = "34250a166dac0c51"
enum FilterDays:String, CaseIterable{
    case today = "Today", yesterday = "Yesterday", DayAgo_2 = "2 Days ago", Last2Days = "Last 2 Days", DayAgo_7 = "Last 7 Days", DayAgo_15 = "Last 15 Days", DayAgo_30 = "Last 30 Days", DayAgo_60 = "Last 60 Days", overall = "Overall"
}

extension FilterDays{
    func getFromAndToDate(leagueStartDate : String)->(String,String){
        var fromDate = Date()
        var toDate = Date()
        switch self {
        case .today:
            fromDate = Date()
        case .yesterday:
            fromDate = Calendar.current.date(byAdding: .day, value: -1, to: toDate) ?? Date()
            toDate = Calendar.current.date(byAdding: .day, value: -1, to: toDate) ?? Date()
        case .DayAgo_2:
            fromDate = Calendar.current.date(byAdding: .day, value: -2, to: toDate) ?? Date()
            toDate = Calendar.current.date(byAdding: .day, value: -2, to: toDate) ?? Date()
        case .DayAgo_7:
            fromDate = Calendar.current.date(byAdding: .day, value: -6, to: toDate) ?? Date()
        case .DayAgo_15:
            fromDate = Calendar.current.date(byAdding: .day, value: -14, to: toDate) ?? Date()
        case .DayAgo_30:
            fromDate = Calendar.current.date(byAdding: .day, value: -29, to: toDate) ?? Date()
        case .DayAgo_60:
            fromDate = Calendar.current.date(byAdding: .day, value: -59, to: toDate) ?? Date()
        case .overall:
            return (leagueStartDate,toDate.getFilterDateFormat())
        case .Last2Days:
            fromDate = Calendar.current.date(byAdding: .day, value: -1, to: toDate) ?? Date()
        }
        if leagueStartDate.getDateFromString() < fromDate{
            fromDate = leagueStartDate.getDateFromString()
        }
        return (fromDate.getFilterDateFormat(),toDate.getFilterDateFormat())
    }
}


enum GoalType:String{
    case dailyChallenge = "daily_challenge",
         fitnessChallenge = "fitness_challenge",
         hbotMorning = "hbot_morning",
         hbotEvening = "hbot_evening"
}

func getAttributedText(fullText:String, boldtext: String)->NSMutableAttributedString{
    
    let txt = fullText
    let arrAttribute : [NSAttributedString.Key : Any] = [ NSAttributedString.Key.font : ConstantString.labelTitileFont!,NSAttributedString.Key.foregroundColor : UIColor.darkGray]
    
    let attributeStr = NSMutableAttributedString(string: txt, attributes: arrAttribute)
    
    let str = NSString(string: fullText)
    let boldTextValue = str.range(of: "\(boldtext)")
    
    attributeStr.addAttribute(NSAttributedString.Key.font, value: ConstantString.textFieldFont!, range: boldTextValue)
    attributeStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: boldTextValue)
    
    return attributeStr
   
}
func getHBuckString(value: String?)->NSMutableAttributedString{
    let harmoneyAttachment = NSTextAttachment()
    
    harmoneyAttachment.image = UIImage(named: "harmoney")
    let boldFont = UIFont.futuraPTBoldFont(size: 16)
    
    let harmoneySize = harmoneyAttachment.image!.size
    
    harmoneyAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - harmoneySize.height) / 2, width: harmoneySize.width, height: harmoneySize.height)
    
    let harmoneyImageString = NSAttributedString(attachment: harmoneyAttachment)
    
    let attributedText = NSMutableAttributedString(string: "")
    
    attributedText.append(harmoneyImageString)
    attributedText.append(NSAttributedString(string: "   "))
    attributedText.append(NSAttributedString(string: value ?? "" + " "))
    return attributedText
}

func update2kStepCompletion(success : ((Int)->Void)?){
           
            if Reachability.isConnectedToNetwork() {
                //Params
                var data = [String : Any]()
                data["dateTime"] = Date().getDateStrFrom(format: DateFormatType.joinTeam)
                data["guid"] = UserDefaultConstants().guid ?? ""
                
                
                //Params, API TYPE, API URL
                let (url, method, param) = APIHandler().update2KCompletions(params: data)
                print("2k completion updated")
                print((url, method, param))
                
                AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                    DispatchQueue.main.async {
                            switch response.result {
                            case .success(let value):
                                if let value = value as? [String:Any] {
                                    print("2k completion Response ")
                                    print(value)
                                    let commonResponse = Mapper<CommonResponse>().map(JSON: value)
                                    if commonResponse?.status == 200{
                                        print("2k completion Successfully")
                                       
                                    }
                                }
                            case .failure(let error):
                                print(error)
                              //  AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                            }
                    }
                }
            }else{
               // self.view.toast("Internet Connection not Available!")
            }
        
}



func updateGoals(goalType:GoalType,success : ((Int)->Void)?){
    getGoals { (updatedGoalCount) in
        DispatchQueue.main.async {
            let goalCount = updatedGoalCount + 1
            let donationCount = goalCount / 3
            if Reachability.isConnectedToNetwork() {
                //Params
                var data = [String : Any]()
                data["dateTime"] = Date().goalUpdateFormat()
                data["guid"] = UserDefaultConstants().guid ?? ""
                data["goalFrom"] = goalType.rawValue
                data["goalCount"] = goalCount
                data["donationCount"] = donationCount
                data["transactionBody"] = {}
                
                //Params, API TYPE, API URL
                let (url, method, param) = APIHandler().udpateGoalDetails(params: data)
                print("Goal Updated da")
                print((url, method, param))
                
                AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                    DispatchQueue.main.async {
                            switch response.result {
                            case .success(let value):
                                if let value = value as? [String:Any] {
                                    print("Goals Updated Response ")
                                    print(value)
                                    let commonResponse = Mapper<CommonResponse>().map(JSON: value)
                                    if commonResponse?.status == 200{
                                        print("Goal Updated Successfully")
                                        success?(goalCount)
                                    }
                                }
                            case .failure(let error):
                                print(error)
                              //  AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                            }
                    }
                }
            }else{
               // self.view.toast("Internet Connection not Available!")
            }
        }
    }
}

func getJoinedImageName(teamName : String)->String{
    switch teamName {
    case "Bonobos":
        return CharityImage.bonobos
    case "Gorillas":
        return CharityImage.gorillas
    case "Orangutans":
        return CharityImage.orangutans
    case "Chimpanzees":
        return CharityImage.chimpanzees
    case "Humans":
        return CharityImage.humans
    default:
        return CharityImage.humans
    }
}
func getGoals(success : @escaping (Int)->Void){
    if Reachability.isConnectedToNetwork() {
        //Params
        let (fromDate, toDate) =  (Date().getFilterDateFormat(),Date().getFilterDateFormat())
        var data = [String : Any]()
        data["fromDate"] = fromDate
        data["toDate"] = toDate
        data["guid"] = UserDefaultConstants().guid ?? ""
        
        //Params, API TYPE, API URL
        let (url, method, param) = APIHandler().getGoalsDetails(params: data)
        print((url, method, param))
        AF.request(url, method: method, parameters: param).validate().responseJSON { response in
            
            DispatchQueue.main.async {
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            print("Goals Details Response ")
                            print(value)
                            let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                           do {
                               let goalResponse = try JSONDecoder().decode(GetGoalsReponse.self, from: jsonData!)
                            var goals_count = 0
                            if let goalsCount = goalResponse.result?.first?.todayGoals{
                                goals_count = goalsCount
                            }
                            success(goals_count)
                               // here we go, `temp` is an array of main object of the json
                           } catch {
                               print(error)
                           }
                        }
                    case .failure(let error):
                        print(error)
                      //  AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
            }
        }
    }else{
       // self.view.toast("Internet Connection not Available!")
    }
}



 func showINTopVc(vc: UIViewController){
    if var topController = UIApplication.shared.keyWindow?.rootViewController {
               while let presentedViewController = topController.presentedViewController {
                   topController = presentedViewController
               }
               if let navVC = topController as? UINavigationController{
                   navVC.viewControllers.last?.present(vc, animated: false, completion: nil)
               }else{
                   topController.present(vc, animated: false, completion: nil)
               }
    }
}


func loadHbotDataPlist(){
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentDirectory = paths[0] as! String
    let path = documentDirectory.appending("/hBotData.plist")
    let fileManager = FileManager.default
    if(!fileManager.fileExists(atPath: path)){
        if let bundlePath = Bundle.main.path(forResource: "hBotData", ofType: "plist"){
            HarmonySingleton.dictHbotData = NSMutableDictionary(contentsOfFile: bundlePath) as?[String: Any]
            
            do{
                try fileManager.copyItem(atPath: bundlePath, toPath: path)
            }catch{
                print("copy failure.")
            }
        }else{
            print("file myData.plist not found.")
        }
    }else{
        HarmonySingleton.dictHbotData = NSMutableDictionary(contentsOfFile: path) as?[String: Any]
        print("file myData.plist already exits at path.")
    }
    print("Plist path")
    print(path.description )
}

func saveHbotData(){
    let docsBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let customPlistURL = docsBaseURL.appendingPathComponent("hBotData.plist")
    
    guard let dictData =  HarmonySingleton.dictHbotData else {
        return
    }
            do  {
            let data = try PropertyListSerialization.data(fromPropertyList: dictData, format: PropertyListSerialization.PropertyListFormat.binary, options: 0)
                do {
                    try data.write(to: customPlistURL, options: .atomic)
                    print("Successfully write")
                }catch (let err){
                    print(err.localizedDescription)
                }
            }catch (let err){
                print(err.localizedDescription)
            }
}



func getEncryptedString(plainText : String?) -> String{
    guard plainText != nil else {
        return ""
    }
    
    let cryptLib = CryptLib()
    let cipherText = cryptLib.encryptPlainTextRandomIV(withPlainText: plainText, key: cryptoKey)
    print("cipherText \(cipherText! as String)")
    return cipherText!
  
   // return cryptLib.encryptPlainText(plainText, key: cryptoKey, iv: cryptoIV)
}

func getDecryptedString(cipherText : String?) -> String?{
    guard cipherText != nil else {
        return nil
    }
    if cipherText?.count ?? 0 <= 16 {
        return cipherText
    }
    let cryptLib = CryptLib()
    
    let decryptedString = cryptLib.decryptCipherTextRandomIV(withCipherText: cipherText, key: cryptoKey)
    
    print(decryptedString as Any)
    
    return decryptedString!

    
   //return cryptLib.decryptCipherText(cipherText, key: cryptoKey, iv: cryptoIV)
}
