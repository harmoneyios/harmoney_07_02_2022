//
//  ConstantString.swift
//  Ludi
//
//  Created by Prema Ravikumar on 09/03/19.
//  Copyright © 2019 Inq. All rights reserved.
//

import UIKit

class ConstantString: NSObject {
    
    static let titleFont = UIFont(name: "FuturaPT-Demi", size: 22)
    static let titleFontEdit = UIFont(name: "FuturaPT-Demi", size: 18)
    static let descriptionFont = UIFont(name: "FuturaPT-Book", size: 17)
    static let buttonTitleFont = UIFont(name: "FuturaPT-Book", size: 17)
    static let labelTitileFont = UIFont(name: "FuturaPT-Book", size: 16)
    static let textFieldFont = UIFont(name: "FuturaPT-Medium", size: 16)
    static let editTitleFont = UIFont(name: "FuturaPT-Book", size: 14)
    static let editTextFont = UIFont(name: "FuturaPT-Book", size: 18)
    static let intFieldFont = UIFont(name: "FuturaPT-Medium", size: 17)
    static let intButtonFont = UIFont(name: "FuturaPT-Light", size: 16)
     static let smallTitle = UIFont(name: "FuturaPT-Book", size: 13)
    static let editTextFont18 = UIFont(name: "FuturaPT-Medium", size: 18)
    
    static let viewBackgroundColor = UIColor(red: 246/250, green: 250/250, blue: 250/250, alpha: 1.0)
    static let buttonBackgroundColor = UIColor(red: 30/250, green: 63/250, blue: 102/250, alpha: 1.0)
    static let borderColor = UIColor(red: 112/250, green: 112/250, blue: 112/250, alpha: 1.0)
    static let buttonSelectedBackgroundColor = UIColor(red: 143/250, green: 152/250, blue: 179/250, alpha: 1.0)

    static let buttontextColor = UIColor(red: 255/250, green: 255/250, blue: 255/250, alpha: 1.0)
    static let menuBackSystemProfilepic =  UIColor(red:  41.0/255.0, green:  72.0/255.0, blue:  100.0/255.0, alpha: 1.0)
    static let textColor = UIColor(red: 22/250, green: 29/250, blue: 42/250, alpha: 1.0)
     static let tableCellColor = UIColor(red: 246/250, green: 245/250, blue: 246/250, alpha: 1.0)
    static let textFont = #colorLiteral(red: 0.06666666667, green: 0.1294117647, blue: 0.2, alpha: 1)
    static let labelFont = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    
    //Placeholder constatnt string
    static let name = "Name"
    static let password = "Password"
    static let confirmPassword = "Confirm Password"
    static let mobileNumberAndEmailId = "Mobile Number"
    static let mobileNumber = "Mobile Number"
    static let emailId = "Email Id"
    static let memberRelationship = "Relationship"
    //Key
    static let nameKey = "name"
    static let mobileNumberKey = "mobile"
    static let choiceCategory = "choiceCategory"
    static let emailIdKey = "email"
    static let sessionKey = "session_token"
    static let locationCity = "locaitonCity"
    static let locationCountry = "locationCountry"
    static let latitude = "latitude"
    static let longtitude = "longtitude"
    static let restaurantInfo = "restaurantInfo"
    static let deviceToken = "deviceToken"
    static let isUpdatedDeviceToken = "isUpdatedDeviceToken"
    static let GUID = "guid"
    static let YouroToken = "youroToken"
    static let userPhoneNumber = "userPhoneNumber"
    static let newUserPhoneNumber = "newUserPhoneNumber"
    static let GoalName = "GoalName"
    static let startDate = "startDate"

    //Placeholder
    static let zipCode = "Zip Code"
    static let age = "Age"
    static let chooseGender = "Choose Gender"
    static let emailID = "Email ID"
    static let bankName = "Bank Name"
    static let lastName = "Last Name"
    static let dateofBirth = "Date of Birth"
    static let accountNumber = "Account Number"
    static let routingNumber = "Routing Number"
    static let editProfile = "Edit Profile"
    static let done = "Done"
    static let firstName = "First Name"
    static let gender = "Gender"
    static let bankAccount = "Bank Account"
    static let sessionToken = "sessionToken"
    
    //Incremet strings
    static let stepsIcrementsString = "Enter steps in multiples of 2000"
    static let swimIcrementsString = "Enter laps in multiples of 2"
    static let runIcrementsString = "Enter miles in multiples of 2"
    static let bikeIcrementsString = "Enter miles in multiples of 2"

}
