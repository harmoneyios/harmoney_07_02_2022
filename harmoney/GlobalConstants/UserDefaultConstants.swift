//
//  DefaultConstants.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 13/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

final class UserDefaultConstants {
     static let shared = UserDefaultConstants()
    func reset() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    var login : Bool? {
        get {
            return UserDefaults.standard.bool(forKey: "login")
        }set
        {
            UserDefaults.standard.set(newValue, forKey: "login")
        }
    }
    var launchedBefore : Bool? {
        get {
            return UserDefaults.standard.bool(forKey: "launchedBefore")
        }set
        {
            UserDefaults.standard.set(newValue, forKey: "launchedBefore")
        }
    }
    var guid : String? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.GUID) as? String
        }set
        {
            UserDefaults.standard.set(newValue, forKey: ConstantString.GUID)
        }
    }
    
    var isDontShowPopup : Bool? {
        get {
            return UserDefaults.standard.value(forKey: "isDontShowPopup") as? Bool
        }set
        {
            UserDefaults.standard.set(newValue, forKey: "isDontShowPopup")
        }
    }
    
    var isUserReached2000Steps : Bool? {
        get {
            return UserDefaults.standard.value(forKey: "isUserReached2000Steps") as? Bool
        }set
        {
            UserDefaults.standard.set(newValue, forKey: "isUserReached2000Steps")
        }
    }
    
    var isMuteRequestNotifications : Bool? {
        get {
            return UserDefaults.standard.value(forKey: "isMuteRequestNotifications") as? Bool
        }set
        {
            UserDefaults.standard.set(newValue, forKey: "isMuteRequestNotifications")
        }
    }
    
    
    var lastHFLPopupDate : String? {
        get {
            return UserDefaults.standard.value(forKey: "lastHFLPopupDate") as? String
        }set
        {
            UserDefaults.standard.set(newValue, forKey: "lastHFLPopupDate")
        }
    }
    
    var youroTokens : String? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.YouroToken) as? String
        }set
        {
            UserDefaults.standard.set(newValue, forKey: ConstantString.YouroToken)
        }
    }
    var sessionToken : String? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.sessionToken) as? String
        }set
        {
            UserDefaults.standard.set(newValue, forKey: ConstantString.sessionToken)
        }
    }
    
//    var challangeStartDate : NSDate? {
//        get {
//            return UserDefaults.standard.value(forKey: ConstantString.startDate) as? NSDate
//        }set
//        {
//            UserDefaults.standard.set(newValue, forKey: ConstantString.startDate)
//        }
//    }
    
    var isUpdatedDeviceToken : Bool? {
        get{
            return UserDefaults.standard.bool(forKey: ConstantString.isUpdatedDeviceToken)
        }set{
            UserDefaults.standard.set(newValue, forKey: ConstantString.isUpdatedDeviceToken)
        }
    }
    
    var deviceToken : String? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.deviceToken) as? String
        }set{
            UserDefaults.standard.set(newValue, forKey: ConstantString.deviceToken)
        }
    }
    
    var sessionKey : String? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.sessionKey) as? String
        }set{
            UserDefaults.standard.set(newValue, forKey: ConstantString.sessionKey)
        }
    }
    
    var mobileNumberKey : String? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.mobileNumberKey) as? String
        }set{
            UserDefaults.standard.set(newValue, forKey: ConstantString.mobileNumberKey)
        }
    }
    var userPhoneNumber : [String]? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.userPhoneNumber) as? [String]
        }set{
            UserDefaults.standard.set(newValue, forKey: ConstantString.userPhoneNumber)
        }
    }
    
    var newuserPhoneNumber : [String]? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.newUserPhoneNumber) as? [String]
        }set{
            UserDefaults.standard.set(newValue, forKey: ConstantString.newUserPhoneNumber)
        }
    }
    
    var choiceCategory : Int? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.choiceCategory) as? Int
        }set{
            UserDefaults.standard.set(newValue, forKey: ConstantString.choiceCategory)
        }
    }
    
    var locationCity : String? {
           get {
               return UserDefaults.standard.value(forKey: ConstantString.locationCity) as? String
           }set{
               UserDefaults.standard.set(newValue, forKey: ConstantString.locationCity)
           }
       }
    
    var locationCountry : String? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.locationCountry) as? String
        }set{
            UserDefaults.standard.set(newValue, forKey: ConstantString.locationCountry)
        }
    }
    
    var latitude : String? {
        get {
            return UserDefaults.standard.value(forKey: ConstantString.latitude) as? String
        }set{
            UserDefaults.standard.set(newValue, forKey: ConstantString.latitude)
        }
    }
    
    var longtitude : String? {
           get {
               return UserDefaults.standard.value(forKey: ConstantString.longtitude) as? String
           }set{
               UserDefaults.standard.set(newValue, forKey: ConstantString.longtitude)
           }
       }
    
    var restaurantInfo : String? {
              get {
                  return UserDefaults.standard.value(forKey: ConstantString.restaurantInfo) as? String
              }set{
                  UserDefaults.standard.set(newValue, forKey: ConstantString.restaurantInfo)
              }
          }
    

    //#TempvariableFor Challenge2
    var videoUrl :String{
        get {
            return UserDefaults.standard.value(forKey: "videoUrl") as? String ?? ""
        }set{
            UserDefaults.standard.set(newValue, forKey: "videoUrl")
        }
    }
    
    var pushdDeviceToken : String{
        get{
            return UserDefaults.standard.value(forKey: "pushdDeviceToken") as? String ?? "pushNotificatiosNotRegistered"
        }set{
            UserDefaults.standard.set(newValue, forKey: "pushdDeviceToken")
        }
    }

    func clearDefaults() {
        UserDefaults.standard.removeObject(forKey: ConstantString.latitude)
        UserDefaults.standard.removeObject(forKey: ConstantString.longtitude)
        UserDefaults.standard.removeObject(forKey: ConstantString.locationCity)
        UserDefaults.standard.removeObject(forKey: ConstantString.locationCountry)
        UserDefaults.standard.removeObject(forKey: ConstantString.sessionKey)
        UserDefaults.standard.removeObject(forKey: ConstantString.mobileNumberKey)
        UserDefaults.standard.removeObject(forKey: ConstantString.restaurantInfo)
        UserDefaults.standard.removeObject(forKey: ConstantString.mobileNumber)
        UserDefaults.standard.removeObject(forKey: ConstantString.userPhoneNumber)
        UserDefaults.standard.removeObject(forKey: "login")
        UserDefaults.standard.removeObject(forKey: "DashBoardData")
        UserDefaults.standard.removeObject(forKey: "isUserReached2000Steps")
        UserDefaults.standard.removeObject(forKey: "isDontShowPopup")
    }
}
