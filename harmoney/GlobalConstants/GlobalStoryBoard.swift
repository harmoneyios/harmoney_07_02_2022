//
//  StoryBoardConstants.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 13/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

private let onboardSB = UIStoryboard(name: "Onboard", bundle: nil)
private let challengesSB = UIStoryboard(name: "EarnChallenges", bundle: nil)
private let homeSB = UIStoryboard(name: "Home", bundle: nil)
private let profileSB = UIStoryboard(name: "Profile", bundle: nil)
private let lithicSB = UIStoryboard(name: "Lithic", bundle: nil)
private let spendSB = UIStoryboard(name: "Spend", bundle: nil)
private let boughtSB = UIStoryboard(name: "Bought", bundle: nil)
private let generalSB = UIStoryboard(name: "General", bundle: nil)
private let HFLSB = UIStoryboard(name: "HFL", bundle: nil)
private let HFLLB = UIStoryboard(name: "Leaderboard", bundle: nil)
final class GlobalStoryBoard: NSObject {
//MARK: - OnboardStoryboard ViewControllers
    
    var splashVC : SplashScreenVC {
        return onboardSB.instantiateViewController(withIdentifier: "SplashScreenVC") as! SplashScreenVC
    }
    var newLoginVC : NewLoginViewController {
        return onboardSB.instantiateViewController(withIdentifier: "NewLoginViewController") as! NewLoginViewController
    }
    
    var loginVC : LoginViewController {
        return onboardSB.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    }
    
    var termsVC : TermsAndConditionsVC {
        return onboardSB.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
    }
    
    var otpVC : OtpViewController {
        return onboardSB.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
    }
    
    var inputVC : InputViewController {
        return onboardSB.instantiateViewController(withIdentifier: "InputViewController") as! InputViewController
    }
    
    var harmoneyBucksVC : HarmoneyBucksConfirmationVC {
        return onboardSB.instantiateViewController(withIdentifier: "HarmoneyBucksConfirmationVC") as! HarmoneyBucksConfirmationVC
    }
    
    var categoryVC : CategoryViewController {
        return onboardSB.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
    }
    
    var healthpermissionVC : HealthPermissionVC {
        return onboardSB.instantiateViewController(withIdentifier: "HealthPermissionVC") as! HealthPermissionVC
    }
    
    var bankAccountVC : BankAcountPermissionVC {
        return onboardSB.instantiateViewController(withIdentifier: "BankAcountPermissionVC") as! BankAcountPermissionVC
    }
    
    var earnHarmoneyVC : EarnHarmoneyVC {
        return onboardSB.instantiateViewController(withIdentifier: "EarnHarmoneyVC") as! EarnHarmoneyVC
    }
    
    var launchTabBAr : UITabBarController {
        return onboardSB.instantiateViewController(withIdentifier: "launchTabBAr") as! UITabBarController
    }
    
    /********************************************************************************/
    
    
    
//MARK: -  ChanllengeStoryBoard ViewControllers
    var earnTabVC : EarnTabViewController {
        return challengesSB.instantiateViewController(withIdentifier: "EarnTabViewController") as! EarnTabViewController
    }
    
    var dailyVC : DailyViewController {
        return challengesSB.instantiateViewController(withIdentifier: "DailyViewController") as! DailyViewController
    }
    
    var fitnessVC : PersonalFitnessViewController {
        return challengesSB.instantiateViewController(withIdentifier: "PersonalFitnessViewController") as! PersonalFitnessViewController
    }
    
    var communityVC : CommunityViewController {
        return challengesSB.instantiateViewController(withIdentifier: "CommunityViewController") as! CommunityViewController
    }
    
    var wellnessVC : WellnessViewController {
        return challengesSB.instantiateViewController(withIdentifier: "WellnessViewController") as! WellnessViewController
    }
    
    var challengeAlertVC : ChallangeAcceptViewcontroller {
        return  challengesSB.instantiateViewController(withIdentifier: "ChallangeAcceptViewcontroller") as! ChallangeAcceptViewcontroller
    }
    
    var pageViewController : EarnPageViewController {
        return challengesSB.instantiateViewController(withIdentifier: "EarnPageViewController") as! EarnPageViewController
    }
    
    var playerVC : PlayerVideoController {
        return challengesSB.instantiateViewController(withIdentifier: "PlayerVideoController") as! PlayerVideoController
    }
    
    var successVC : SuccesScreenVC {
        return challengesSB.instantiateViewController(withIdentifier: "SuccesScreenVC") as! SuccesScreenVC
    }
       
    var socialVC : SocialShareViewcontroller{
        return challengesSB.instantiateViewController(withIdentifier: "SocialShareViewcontroller") as! SocialShareViewcontroller
    }
    
    var challengeActionsVC : ChallengeActionsViewController {
        return challengesSB.instantiateViewController(withIdentifier: "ChallengeActionsViewController") as! ChallengeActionsViewController
    }
    
    var addChallengeVC : AddChallengeViewController {
        return challengesSB.instantiateViewController(withIdentifier: "AddChallengeViewController") as! AddChallengeViewController
    }
    
    var acceptChallengeVC : AcceptChallengeViewController {
        return challengesSB.instantiateViewController(withIdentifier: "AcceptChallengeViewController") as! AcceptChallengeViewController
    }

    var onBoardCompleteVC : OnboardingCompleteViewController{
       return challengesSB.instantiateViewController(withIdentifier: "OnboardingCompleteViewController") as! OnboardingCompleteViewController
    }
    
    var completedChallengeVC : CompletedChallengeViewController {
       return challengesSB.instantiateViewController(withIdentifier: "CompletedChallengeViewController") as! CompletedChallengeViewController
    }
    
    var deleteChallengeVC : DeleteChallangeViewController {
       return challengesSB.instantiateViewController(withIdentifier: "DeleteChallangeViewController") as! DeleteChallangeViewController
    }
    var restartChallengeVC : RestartChallageViewController {
       return challengesSB.instantiateViewController(withIdentifier: "RestartChallageViewController") as! RestartChallageViewController
    }
    var approveChallengeVC : ApproveChallengeViewController {
       return challengesSB.instantiateViewController(withIdentifier: "ApproveChallengeViewController") as! ApproveChallengeViewController
    }
    var requestnotificationVC : RequestNotificationVC{
        return challengesSB.instantiateViewController(withIdentifier: "RequestNotificationVC") as! RequestNotificationVC
    }
    
    var challengeDetailsVC : ChallengeDetailsViewController {
        return challengesSB.instantiateViewController(withIdentifier: "ChallengeDetailsViewController") as! ChallengeDetailsViewController
    }
    
   
    var removeChoreVC : RemoveChoreViewController {
       return challengesSB.instantiateViewController(withIdentifier: "RemoveChoreViewController") as! RemoveChoreViewController
    }
    var doneChoreVC : DoneChoreViewController {
          return challengesSB.instantiateViewController(withIdentifier: "DoneChoreViewController") as! DoneChoreViewController
       }
    
    var removeCouponVC : RemoveCouponAlertViewController {
       return challengesSB.instantiateViewController(withIdentifier: "RemoveCouponAlertViewController") as! RemoveCouponAlertViewController
    }
/********************************************************************************/
    
    
    //MARK: - Lithic view Controllers
    
    var lithicBankVC : EnrollConsumerVC {
        return lithicSB.instantiateViewController(withIdentifier: "EnrollConsumerVC") as! EnrollConsumerVC
        
    }
    var KycStatusVC: kycStatusVC {
        return lithicSB.instantiateViewController(withIdentifier: "kycStatusVC") as! kycStatusVC
        
    }
    var addLithicBankVC: addLithicVC {
        return lithicSB.instantiateViewController(withIdentifier: "addLithicVC") as! addLithicVC
        
    }
    var LithicAcctDetailsVC:  lithicAccountDetailsVC {
        return lithicSB.instantiateViewController(withIdentifier: "lithicAccountDetailsVC") as! lithicAccountDetailsVC
    }
    
    var LithicCreatecardVC: LithicCreateCardVC{
        return lithicSB.instantiateViewController(withIdentifier: "LithicCreateCardVC") as! LithicCreateCardVC
    }
    /********************************************************************************/
    
//MARK: -    ProfileStoryBoard ViewControllers
    var profileTabVC : ProfileTabViewController {
        return profileSB.instantiateViewController(withIdentifier: "ProfilePlaceHolderViewController") as! ProfileTabViewController
    }
   

    var myFriendsVC : MyFriendsViewController {
        return profileSB.instantiateViewController(withIdentifier: "MyFriendsViewController") as! MyFriendsViewController
    }
    var recapVC : RecapViewController {
        return profileSB.instantiateViewController(withIdentifier: "RecapViewController") as! RecapViewController
    }
    
//    @available(iOS 13.0, *)
//    var plaidVC : PlaidViewController {
//        return profileSB.instantiateViewController(withIdentifier: "PlaidViewController") as! PlaidViewController
//    }
   
    var localWebVC : LoclaWebViewController {
        return profileSB.instantiateViewController(withIdentifier: "LoclaWebViewController") as! LoclaWebViewController
    }
    
    var rewardWebVC : RewardsWebVwVC {
        return HFLSB.instantiateViewController(withIdentifier: "RewardsWebVwVC") as! RewardsWebVwVC
    }


    var editProfileVC : EditSettingsViewController {
        return profileSB.instantiateViewController(withIdentifier: "EditSettingsViewController") as! EditSettingsViewController
    }
    
    var profilePageVC : ProfilePageController {
        return profileSB.instantiateViewController(withIdentifier: "ProfilePageController") as! ProfilePageController
    }
    
    var familyVC : FamilyViewController {
        return profileSB.instantiateViewController(withIdentifier: "FamilyViewController") as! FamilyViewController
    }
    
    var interestsVC : InterestViewController {
        return profileSB.instantiateViewController(withIdentifier: "InterestViewController") as! InterestViewController
    }
    
    var profileSettingsVC : ProfileSettingsViewController {
        return profileSB.instantiateViewController(withIdentifier: "ProfileSettingsViewController") as! ProfileSettingsViewController
    }
    var customplaidVc : PlaidTransactionViewController {
        return profileSB.instantiateViewController(withIdentifier: "PlaidTransactionViewController") as! PlaidTransactionViewController
    }
    var addSubWallet : SubAccountViewController {
        return profileSB.instantiateViewController(withIdentifier: "SubAccountViewController") as! SubAccountViewController
    }
    var myProfileVC : MyPfrofileViewController {
        return profileSB.instantiateViewController(withIdentifier: "MyPfrofileViewController") as! MyPfrofileViewController
    }
    
    var customAlertVC : CustomAlertViewController {
        return profileSB.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
    }
    
    var thumbsAlertVC : ThumbsAlertViewController {
        return profileSB.instantiateViewController(withIdentifier: "ThumbsAlertViewController") as! ThumbsAlertViewController
    }

    var BankDetailAccountAddVC : BankDetailAccountAddViewcontroller {
        return profileSB.instantiateViewController(withIdentifier: "BankDetailAccountAddViewcontroller") as! BankDetailAccountAddViewcontroller
    }

    
    
    /********************************************************************************/
    
//MARK: - HomeStoryBoard ViewControllers
    var homeVC : HomeViewController {
        return homeSB.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
    }
    
    /********************************************************************************/

    
//MARK: - SpendBoard ViewControllers
    var chatSelectionPopUp : ChatSelectPopupViewController {
        return spendSB.instantiateViewController(withIdentifier: "ChatSelectPopupViewController") as! ChatSelectPopupViewController
    }
    
    /********************************************************************************/
    
//MARK: - GeneralStoryBoard ViewControllers
    var notificationsVC : NotificationsViewController {
        return generalSB.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
    }
    
    var spendVC : SpendTabViewController {
        return spendSB.instantiateViewController(withIdentifier: "SpendTabViewController") as! SpendTabViewController
    }
    
    
    var childInvitedParentPopUp : ChildInviteParentRequestPopUp {
        return generalSB.instantiateViewController(withIdentifier: "ChildInviteParentRequestPopUp") as! ChildInviteParentRequestPopUp
    }
    
    var childChoreAcceptPopUp : AcceptChildChorePopUp {
        return generalSB.instantiateViewController(withIdentifier: "AcceptChildChorePopUp") as! AcceptChildChorePopUp
    }
    
    var gemsVC : GemsViewController {
        return generalSB.instantiateViewController(withIdentifier: "GemsViewController") as! GemsViewController
    }
    
    var levelsVC : LevelsViewController {
        generalSB.instantiateViewController(withIdentifier: "LevelsViewController") as! LevelsViewController
    }
    
    var claimLevelRewardVC : ClaimLevelRewardViewController {
        generalSB.instantiateViewController(withIdentifier: "ClaimLevelRewardViewController") as! ClaimLevelRewardViewController
    }
    
    var requestNotificationVC : RequestNotificationVC {
        generalSB.instantiateViewController(withIdentifier: "RequestNotificationVC") as! RequestNotificationVC
    }
    
    
    /********************************************************************************/
    
    //MARK: - Shopped ViewControllers
    var rewardsVC : RewardsViewController {
        return boughtSB.instantiateViewController(withIdentifier: "RewardsViewController") as! RewardsViewController
    }

    var offersVC : OffersViewController {
        return boughtSB.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
    }

    var vouchersVC : VouchersViewController {
        return boughtSB.instantiateViewController(withIdentifier: "VouchersViewController") as! VouchersViewController
    }
    
    var OfferDetailsVC : OfferDetailsViewController {
        return boughtSB.instantiateViewController(withIdentifier: "OfferDetailsViewController") as! OfferDetailsViewController
    }
    
    var CouponList : CouponListViewController {
        return boughtSB.instantiateViewController(withIdentifier: "CouponListViewController") as! CouponListViewController
    }
    
    var couponDetails : QRCodeGenerateViewController {
        return boughtSB.instantiateViewController(withIdentifier: "QRCodeGenerateViewController") as! QRCodeGenerateViewController
    }
    
    var claimCouponList : CouponClaimViewController {
        return boughtSB.instantiateViewController(withIdentifier: "CouponClaimViewController") as! CouponClaimViewController
    }
    
    
    var couponTemplete : CouponTempleteViewController {
        return boughtSB.instantiateViewController(withIdentifier: "CouponTempleteViewController") as! CouponTempleteViewController
    }
    /********************************************************************************/
}


        
//Wellness StoryBoard
extension GlobalStoryBoard {
    var choreCreateVC : ChoreCreateVC {
        return challengesSB.instantiateViewController(withIdentifier: "ChoreCreateVC") as! ChoreCreateVC
    }
    var choreAddPopVC : AddChorePopVC {
        return challengesSB.instantiateViewController(withIdentifier: "AddChorePopVC") as! AddChorePopVC
    }

    var rewardsDetailsVC : RewardsDetailsVC {
        return boughtSB.instantiateViewController(withIdentifier: "RewardsDetailsVC") as! RewardsDetailsVC
    }
    
    var voucherDetailsVC : VoucherDetailsViewController {
        return boughtSB.instantiateViewController(withIdentifier: "VoucherDetailsViewController") as! VoucherDetailsViewController
    }

}
    
    

// HFL StoryBoard
extension GlobalStoryBoard {
    var clubTeamListVC : TeamListViewController {
        return HFLSB.instantiateViewController(withIdentifier: "TeamListViewController") as! TeamListViewController
    }
    var subTeamListViewController : SubTeamListViewController {
        return HFLSB.instantiateViewController(withIdentifier: "SubTeamListViewController") as! SubTeamListViewController
    }
    var enterPassCodeVC : EnterCodeViewController{
        return EnterCodeViewController(nibName: "EnterCodeViewController", bundle: nil)
    }
    
    var successAlertVC : HFLSuccessVC{
        return HFLSB.instantiateViewController(withIdentifier: "HFLSuccessVC") as! HFLSuccessVC
    }
    
    var leagueSucess2000StepsVC : LeagueSucessVC{
        return HFLSB.instantiateViewController(withIdentifier: "LeagueSucessVC") as! LeagueSucessVC
    }
    
    
    var hflDashBoardVC : HFLDashBoardVC{
        return HFLSB.instantiateViewController(withIdentifier: "HFLDashBoardVC") as! HFLDashBoardVC
    }
    
    var charityImageVC : CharityImageVC{
        return HFLSB.instantiateViewController(withIdentifier: "CharityImageVC") as! CharityImageVC
    }
    
    
    var hfl_DonationViewContoller : HFLDonationViewContoller{
        return HFLSB.instantiateViewController(withIdentifier: "HFLDonationViewContoller") as! HFLDonationViewContoller
    }
    
    
    var HFL_EarnPopup : HFLEarnPopup{
        return HFLEarnPopup(nibName: "HFLEarnPopup", bundle: nil)
    }
    
    var hfl_StatisticsViewController : HFLStatisticsViewController{
        return HFLSB.instantiateViewController(withIdentifier: "HFLStatisticsViewController") as! HFLStatisticsViewController
    }
    
    var hfl_SingleClubTeamList : SingleClubTeamListVC{
        return HFLSB.instantiateViewController(withIdentifier: "SingleClubTeamListVC") as! SingleClubTeamListVC
    }
    
    var hfl_LeaderBoardViewContoller : LeaderboardViewController{
            return HFLLB.instantiateViewController(withIdentifier: "LeaderboardViewController") as! LeaderboardViewController
        }

    var TeamSelection : TeamSelectionVC{
        return TeamSelectionVC(nibName: "TeamSelectionVC", bundle: nil)
    }
    
    var leagueStatiticsVc : LeagueStatisticsVc{
        return HFLSB.instantiateViewController(withIdentifier: "LeagueStatisticsVc") as! LeagueStatisticsVc
    }

    
}
