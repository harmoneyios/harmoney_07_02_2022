//
//  HarmonySingleton.swift
//  Harmoney
//
//  Created by GOPI K on 29/03/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

final class HarmonySingleton {
    static var dictHbotData : [String:Any]?
    static var shared = HarmonySingleton()
    var dashBoardData : DashBoardModel!{
        didSet{
            self.navHeaderViewEarnBoard.updateUI()
        }
    }
    var challegesModelData : ChallengesObject!
    var completeFitnessChatboat : Bool = false
    public static var previousVC:ChatPreviousVC = .none
    var checkChallengeStatus : ChallengeStatusList?
    var assigntomemberDataList : [AssignMemberData]?
    var choreFor:String = ""
    var assigntomemberDataListForSubAccount : [AssignMemberData]?
    var assigntomemberDataListForSubAccountAssignedKid : [AssignMemberData]?
    var getKidWalletList : [kidWalletData]?
    var addSilaToHarmoney : addHarmoneyWalletDataObject!
    var taskType:String = ""
    var taskTitle:String = ""
    var challengeDecide = ""
    var isHideForNewFlow = true
    public static var showNightMessageView : Bool = false
    public static var tid:String = ""
    var navHeaderViewEarnBoard = UINib(nibName: "HeaderView", bundle: .main).instantiate(withOwner: nil, options: nil).first as! HeaderView
    var mainTabBar : UITabBarController?
    var cofigModel : ConfigModel?
    var isSecondChallange : Bool {
        var issecond = false
        if HarmonySingleton.shared.dashBoardData.data?.challenges?.count ?? 0 > 1{
           let challange = HarmonySingleton.shared.dashBoardData.data?.challenges?[1]
            if challange?.cStatus == 0{
                issecond = true
            }
        }
        return issecond
    }
   
    //MARK: - Private methods
    
    //Fix number of xp for level
    func requiredNextLevelXp(userLevel: Int) -> Int {
        var nextLevelXp = 0
        
        if userLevel == 0 {
            nextLevelXp = 200
        } else if userLevel == 1 {
            nextLevelXp = 600
        } else if userLevel == 2 {
            nextLevelXp = 1800
        } else if userLevel == 3 {
            nextLevelXp = 5400
        } else if userLevel >= 4 {
            nextLevelXp = 16200
        }
        
        return nextLevelXp
    }
    
    //MARK: - Public methods
    
    //Calcualte the total required xp for the next level
    func calculateRequiredXP(userLevel: Int) -> Int {
        var totalXpForNextLevel = 0
        var userLevelToCalculate = userLevel
        
        let currentXpForLevel = requiredNextLevelXp(userLevel: userLevelToCalculate)
        userLevelToCalculate -= 1
        let nextLevelXp = requiredNextLevelXp(userLevel: userLevelToCalculate)
        totalXpForNextLevel = currentXpForLevel + nextLevelXp
        
        while userLevelToCalculate >= 0 {
            userLevelToCalculate -= 1
            
           let nextLevelXp2 = requiredNextLevelXp(userLevel: userLevelToCalculate)
           totalXpForNextLevel = totalXpForNextLevel + nextLevelXp2
        }
        
        return totalXpForNextLevel
    }
}


