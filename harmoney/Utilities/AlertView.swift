//
//  AlertView.swift
//  Harmoney
//
//  Created by GOPI K on 20/03/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
protocol AlertCallBack {
    func clickedOnOk()
}
class AlertView {
    var delegate : AlertCallBack?
    static let shared = AlertView()

  func showAlert(view: UIViewController, title: String, description: String) {
    let alert = UIAlertController(title: title, message: description, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction.init(title: "Okay", style: .cancel, handler: { (action) in
        self.delegate?.clickedOnOk()
    }))
    view.present(alert, animated: true, completion: nil)

 }
    func showAlertwithTwoAction(view: UIViewController, title: String, description: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction.init(title: "Device", style: .default, handler: { (action) in
            modeofChallenge = 1
            BaseViewController().locationmanage.startUpdatingLocation()
           // self.delegate?.clickedOnOk()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Watch", style: .default, handler: { (action) in
            modeofChallenge = 2
            BaseViewController().locationmanage.stopUpdatingLocation()
          //  self.delegate?.clickedOnOk()
        }))
        view.present(alert, animated: true, completion: nil)
        
    }
    // swimming popup alert
    func showAlertwithTwoActionWithSwimType(view: UIViewController, title: String, description: String) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction.init(title: "Device", style: .default, handler: { (action) in
            modeofChallenge = 3
            BaseViewController().locationmanage.startUpdatingLocation()
        }))
        alert.addAction(UIAlertAction.init(title: "Watch", style: .default, handler: { (action) in
            modeofChallenge = 4
        }))
        view.present(alert, animated: true, completion: nil)
        
    }
    // pool or water alert
 /*   func typeOfSwim() {
        let alert = UIAlertController(title: "Swimming Type", message: "Can you please choose the swimming in pool or water", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction.init(title: "Pool Swim", style: .default, handler: { (action) in
            modeofChallenge = 3
            
        }))
        alert.addAction(UIAlertAction.init(title: "Water Swim", style: .default, handler: { (action) in
            modeofChallenge = 4
        }))
     }
*/
}

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.harmoneyDarkBlueColor
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.futuraPTMediumFont(size: 16)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .none
    }
}
