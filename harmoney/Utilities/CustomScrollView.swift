//
//  CustomScrollView.swift
//  CreatingViews
//
//  Created by Dineshkumar kothuri on 09/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

class CustomScrollView: UIScrollView {
    var childViewControllers = [UIViewController]()
    private var framesArray = [CGRect]()
    var initialContentOffset : CGPoint?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.framesArray.append(self.frame)
        self.initialContentOffset = self.contentOffset
    }
    
    func addChildViewController(child cvc: UIViewController, toParent pvc:UIViewController) {
        print(self.subviews)
        var sframe = self.bounds
        sframe.origin.x  =  (self.framesArray.last?.origin.x ?? 0) + (self.framesArray.last?.size.width ?? 0) + 1
        sframe.origin.y  = 0
        let containerView = UIView.init(frame: sframe)
//        containerView.backgroundColor = UIColor.red
        self.addSubview(containerView)
        cvc.view.translatesAutoresizingMaskIntoConstraints = false
        cvc.willMove(toParent: pvc)
        pvc.addChild(cvc)
        containerView.addSubview(cvc.view)
        cvc.didMove(toParent: pvc)
        pvc.addChild(cvc)

        NSLayoutConstraint.activate([
            cvc.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            cvc.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -10),
            cvc.view.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 10),
            cvc.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -10)
        ])
        
        self.childViewControllers.append(cvc)
        self.framesArray.append(sframe)
    }
    func moveToIndex(index:Int) {
        var poin : CGPoint
        if index == 0{
            poin = self.initialContentOffset!
        }else{
            poin = CGPoint.init(x: self.framesArray[index].origin.x, y: self.framesArray[index].origin.y/2)
        }
        self.setContentOffset(poin, animated: true)
    }
}
