//
//  PedometerDebugView.swift
//  Harmoney
//
//  Created by Norbert Korosi on 10/06/2020.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class PedometerDebugView: UIView {
    
    let startDateText = "Start Date: "
    let endDateText = "End Date: "
    let nrOfStepsText = "Nr. of Steps: "
    let distanceText = "Distance: "
    let challengeSourceText = "Challenge Source: "
    let challengeIDText = "Challenge ID: "
    let activityTypeText = "Activity Type: "
    let challengeTypeText = "Challenge Type: "
    let progressText = "Progress: "
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var challengeSourceLabel: UILabel!
    @IBOutlet weak var challengeIDLabel: UILabel!
    @IBOutlet weak var challengeTypeLabel: UILabel!
    @IBOutlet weak var activityTypeLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var nrOfStepsLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    
    var dateFormatter: DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss"
        return df
    }
    
    var numberFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        
        return formatter
    }
    
    var challengeID: String? {
        didSet {
            guard let id = challengeID else {
                challengeIDLabel.text = challengeIDText + "--------"
                return
            }
            challengeIDLabel.text = challengeIDText + id
        }
    }
    
    var challengeSource: String? {
        didSet {
            guard let source = challengeSource else {
                challengeSourceLabel.text = challengeSourceText + "--------"
                return
            }
            challengeSourceLabel.text = challengeSourceText + source
        }
    }
    
    var challengeType: ChallengeType? {
        didSet {
            guard let type = challengeType else {
                challengeTypeLabel.text = challengeTypeText + "--------"
                return
            }
            challengeTypeLabel.text = challengeTypeText + type.rawValue
        }
    }
    
    var startDate: Date? {
        didSet {
            guard let date = startDate else {
                startDateLabel.text = startDateText + "--------"
                return
            }
            startDateLabel.text = startDateText + dateFormatter.string(from: date)
        }
    }
    
    var endDate: Date? {
        didSet {
            guard let date = endDate else {
                endDateLabel.text = endDateText + "--------"
                return
            }
            endDateLabel.text = endDateText + dateFormatter.string(from: date)
        }
    }
    
    var nrOfSteps: NSNumber? {
        didSet {
            guard let steps = nrOfSteps else {
                nrOfStepsLabel.text = nrOfStepsText + "--------"
                return
            }
            nrOfStepsLabel.text = nrOfStepsText + "\(steps.intValue)"
        }
    }
    
    var distance: NSNumber? {
        didSet {
            guard let dis = distance else {
                distanceLabel.text = distanceText + "--------"
                return
            }
            let prefix = " meters"
            distanceLabel.text = distanceText + "\(numberFormatter.string(from: dis) ?? "--------")" + prefix
        }
    }
    
//    var activityType: MotionActivityType? {
//        didSet {
//            guard let type = activityType else {
//                activityTypeLabel.text = activityTypeText + "--------"
//                return
//            }
//
//            activityTypeLabel.text = activityTypeText + type.rawValue
//        }
//    }
    
    var progress: Float? {
        didSet {
            guard let prog = progress else {
                progressLabel.text = progressText + "--------"
                return
            }
            let prefix = challengeType == .walk ? " steps" : " miles"
            progressLabel.text = progressText + "\(numberFormatter.string(from: NSNumber(value: prog)) ?? "--------")" + prefix
        }
    }
    
    //MARK: - Lifecycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("PedometerDebugView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //contentView.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        challengeSourceLabel.text = challengeSourceText + "--------"
        challengeIDLabel.text = challengeIDText + "--------"
        activityTypeLabel.text = activityTypeText + "--------"
        startDateLabel.text = startDateText + "--------"
        endDateLabel.text = endDateText + "--------"
        nrOfStepsLabel.text = nrOfStepsText + "--------"
        distanceLabel.text = distanceText + "--------"
        challengeTypeLabel.text = challengeTypeText + "--------"
        progressLabel.text = progressText + "--------"
    }
}
