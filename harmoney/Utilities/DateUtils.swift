//
//  DateUtils.swift
//  Harmoney
//
//  Created by Norbert Korosi on 29/07/2020.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

struct DateUtils {
    enum Format: String {
        case dayName = "EEE"
        case dayNameAndTime = "EEEE d, h:mm a"
        case dayNameAndDate = "EEE, dd MMM yyyy"
        case dayShortNameAndTime = "EEE h:mm a"
        case monthName = "MMMM"
        case monthNameAndDay = "MMM dd"
        case monthNameAndDayAndYear = "MMM dd, yyyy"
        case yearAndMonth = "yyyy/MM"
        case date = "MM/dd/yy"
        case euroFormatDate = "dd.MM.yyyy"
        case euroFormatDateSlash = "dd/MM/yyyy"
        case dateAndTime = "dd.MM.yyyy | HH:mm"
        case hourAndMinute = "HH:mm"
        case yearMonthDay = "yyyy-MM-dd"
        case hourAndMinuteAndSecond = "HH:mm:ss"
    }

    private static let formatter = DateFormatter()

    static func format(date: Date?, in format: Format) -> String? {
        guard let date = date else { return  nil }
        formatter.dateFormat = format.rawValue
        return formatter.string(from: date)
    }

    static func format(string: String?, in format: Format) -> Date? {
        guard let string = string else { return  nil }
        formatter.dateFormat = format.rawValue
        return formatter.date(from: string)
    }
}
