//
//  Utilities.swift
//  Ludi
//
//  Created by Prema Ravikumar on 17/03/19.
//  Copyright © 2019 Inq. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol UpdateLocation {
    func didUpdateLocationAddress(city: String, country: String)
}

class Utilities: NSObject {
    
    static let shared = Utilities()
    
    var taskUpdatedDate: [String: Date] = [:]
    
    func getCityAndCountryNameFromLocation(latitude: CLLocationDegrees, longtitude: CLLocationDegrees, delegate: UpdateLocation)  {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longtitude)
        
        var cityName: String = ""
        var countryName: String = ""
        
        geoCoder.reverseGeocodeLocation(location, completionHandler:
            {
                placemarks, error -> Void in
                
                // Place details
                guard let placeMark = placemarks?.first else { return }
                
                // City
                if let city = placeMark.subAdministrativeArea {
                    cityName = city
                }
                
                // Country
                if let country = placeMark.country {
                   countryName = country
                }
                
                delegate.didUpdateLocationAddress(city: cityName,country: countryName)
        })
    }
    
    public static func addNumberFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        
        return formatter
    }
    
    static func isValidEmail(emailString:String) -> Bool {
         let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
         
         let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
         return emailPred.evaluate(with: emailString)
     }
    
    static func formattedMobileNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXX-XXX-XXXX"
        var result = ""
        var index = cleanPhoneNumber.startIndex
        
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        
        return result
    }
    
    static func dummyGemProductList() -> [GemProduct] {
        var gemProducts = [GemProduct]()
        
//        let gemProduct1 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "acer_icon"))
//        gemProducts.append(gemProduct1)
//        
//        let gemProduct2 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "aflic_icon"))
//        gemProducts.append(gemProduct2)
//        
//        let gemProduct3 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "amazon_icon"))
//        gemProducts.append(gemProduct3)
//        
//        let gemProduct4 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "ap_icon"))
//        gemProducts.append(gemProduct4)
//        
//        let gemProduct5 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "apple_icon"))
//        gemProducts.append(gemProduct5)
//        
//        let gemProduct6 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "beats_icon"))
//        gemProducts.append(gemProduct6)
//        
//        let gemProduct7 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "bp_green_icon"))
//        gemProducts.append(gemProduct7)
//        
//        let gemProduct8 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "bt_icon"))
//        gemProducts.append(gemProduct8)
//        
//        let gemProduct9 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "burger_king_icon"))
//        gemProducts.append(gemProduct9)
//        
//        let gemProduct10 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "starbucks_icon"))
//        gemProducts.append(gemProduct10)
//        
//        let gemProduct11 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "vans_icon"))
//        gemProducts.append(gemProduct11)
//        
//        let gemProduct12 = GemProduct(productPrice: 5, discount: 20, productImage: UIImage(named: "h&m_icon"))
//        gemProducts.append(gemProduct12)
        
        return gemProducts
    }
    
    func isLowerVersionDevices() -> Bool {
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        return height < 600 ? true : false
    }
    static func appVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "\(version)  (\(build))"
    }
    
    static func appVersionCode() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let build = dictionary["CFBundleVersion"] as! String
        return build
    }
    
    static func appVersionName() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        return version
    }

}




