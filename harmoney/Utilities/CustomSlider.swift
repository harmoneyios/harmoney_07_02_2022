//
//  CustomSlider.swift
//  Harmoney
//
//  Created by GOPI K on 22/03/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

@IBDesignable
class CustomSlider: UISlider {
//custom slider track height
    @IBInspectable var trackHeight: CGFloat = 6

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
//Use properly calculated rect
        var newRect = super.trackRect(forBounds: bounds)
        newRect.size.height = trackHeight
        
        return newRect
    }
}
