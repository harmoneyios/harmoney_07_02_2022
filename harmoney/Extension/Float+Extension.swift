//
//  Float+Extension.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/25/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

extension Float {
    //Removes a decimal from a float if the decimal is equal to 0,1,2,10,50 etc
    var clean: String {
       return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
