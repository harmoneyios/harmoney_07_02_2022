//
//  UIFont+Extension.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/22/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

extension UIFont {
    static func futuraPTBoldFont (size: CGFloat) -> UIFont {
        return UIFont(name: "FuturaPT-Bold", size: size) ?? self.boldSystemFont(ofSize: size)
    }
    
    static func futuraPTMediumFont (size: CGFloat) -> UIFont {
        return UIFont(name: "FuturaPT-Medium", size: size) ?? self.systemFont(ofSize: size)
    }
    
    static func futuraPTBookFont (size: CGFloat) -> UIFont {
        return UIFont(name: "FuturaPT-Book", size: size) ?? self.systemFont(ofSize: size)
    }
    
    static func futuraPTDemiFont (size: CGFloat) -> UIFont {
        return UIFont(name: "FuturaPT-Demi", size: size) ?? self.systemFont(ofSize: size)
    }
    
    static func futuraPTLightFont (size: CGFloat) -> UIFont {
        return UIFont(name: "FuturaPT-Light", size: size) ?? self.systemFont(ofSize: size)
    }
}
