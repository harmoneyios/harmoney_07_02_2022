//
//  UIImageView+Extension.swift
//  Ludi
//
//  Created by Prema Ravikumar on 19/03/19.
//  Copyright © 2019 Inq. All rights reserved.
//

import Foundation
import UIKit
import Cloudinary
import Alamofire
extension UIImageView {
    
    public func imageFromURL(urlString: String) {
        
//        let activityIndicator = UIActivityIndicatorView(style: .gray)
//        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
//        activityIndicator.startAnimating()
//        if self.image == nil{
//            self.addSubview(activityIndicator)
//        }
//
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
            
        }).resume()
    }
    func set_image(_ image_path:String){

        let cloud = CLDCloudinary(configuration: CLDConfiguration(cloudinaryUrl: "cloudinary://554452487511558:H30xXC2ikcEbX8j4PxhvXGe9CDE@harmoney")!)
       // CLOUDINARY_URL=cloudinary://554452487511558:H30xXC2ikcEbX8j4PxhvXGe9CDE@harmoney
        var cloud_url = cloud.createUrl().setFormat("png").setResourceType(.image)
        cloud_url = cloud_url.setType(CLDType.upload)
        let imgString = cloud_url.generate(image_path, signUrl: false)!
        self.cldSetImage(imgString, cloudinary: cloud)
    }
    
    func setAlamofireImage(_ imagePath: String){
        AF.request(imagePath).responseData { (response) in
            if case .success(let imageData) = response.result {
                self.image = UIImage(data: imageData)
            }
        }
    }
}
