//
//  HPLabel.swift
//  HelloParentAdmin
//
//  Created by Sathish Kalimuthan on 19/12/19.
//  Copyright © 2019 Hello Parent. All rights reserved.
//

import Foundation
import UIKit

open class HPLabel : UILabel {
    @IBInspectable open var characterSpacing:CGFloat = 1 {
        didSet {
            let attributedString = NSMutableAttributedString(string: self.text!)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: self.characterSpacing, range: NSRange(location: 0, length: attributedString.length))
            self.attributedText = attributedString
        }

    }
}
