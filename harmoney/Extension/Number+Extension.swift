//
//  Number+Extension.swift
//  harmoney
//
//  Created by admin on 03/03/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

extension Float {
    func simplify(to min: Int = 0, max: Int = 2) -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = min
        formatter.maximumFractionDigits = max
        formatter.numberStyle = .decimal
        
        return formatter.string(from: self as NSNumber) ?? "0"
    }
}
