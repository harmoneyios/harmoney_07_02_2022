//
//  UiViewController+Extension.swift
//  Harmoney
//
//  Created by GOPI K on 23/04/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

extension UIViewController{
    var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        
        return false
    }
}
