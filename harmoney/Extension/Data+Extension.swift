//
//  Data+Extension.swift
//  Harmoney
//
//  Created by Norbert Korosi on 22/06/2020.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
    
}
