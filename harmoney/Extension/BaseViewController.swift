//
//  BaseViewController.swift
//  Harmoney
//
//  Created by INQ Projects on 22/02/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import WatchConnectivity
import CoreLocation
var isPairedWithWatch: Bool = false
var locations1: [CLLocation] = []
var travelleddistance  : Double = 0.0
var bikechallengeId : String = ""
class BaseViewController: UIViewController {

    var locationmanage = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        //check watch connectivity statuds
        self.watchConnectivityPairTesting()
        // Do any additional setup after loading the view.
    }
    
    func watchConnectivityPairTesting ()
    {
        if WCSession.isSupported() { // check if the device support to handle an Apple Watch
            let session = WCSession.default
            session.delegate = self
            session.activate() // activate the session
        }
     //  if modeofChallenge == 1 {
            locationPermissionRequest()
      //  }
    }
    //Location TrakingEnable
    func locationPermissionRequest() {
        locationmanage.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationmanage.delegate = self
            locationmanage.requestLocation()
           locationmanage.startUpdatingLocation()
        // Do any additional setup after loading the view.
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BaseViewController:  WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if activationState == .activated && session.isPaired { // Check if the iPhone is paired with the Apple Watch
               // Do stuff
            print("its paired")
            isPairedWithWatch = true
        }else {
            print("not paired")
            isPairedWithWatch = false
        }
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print(session)
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print(session)
    }
}
extension BaseViewController: CLLocationManagerDelegate {
    enum DistanceValue: Int {
        case meters, miles
    }
    
    func calculateDistanceBetweenLocations(_ firstLocation: CLLocation, secondLocation: CLLocation, valueType: DistanceValue) -> Double {
        var distance = 0.0
        let meters = firstLocation.distance(from: secondLocation)
        distance += meters
        switch valueType {
        case .meters:
            return distance
        case .miles:
            let miles = distance
            return miles
        }
    }
    func travelledDistance(locations : [CLLocation] ) -> Double {
        //let locations: [CLLocationCoordinate2D] = [...]
                var total: Double = 0.0
        if locations.count > 0 {
                for i in 0..<locations.count - 1 {
                    let start = locations[i]
                    let end = locations[i + 1]
                    let distance = start.distance(from: end)
                    total += distance
                }
        }
                print(total)
        travelleddistance = (total * 0.000621371192).rounded(toPlaces: 2)
        return travelleddistance
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
        if modeofChallenge == 1 || modeofChallenge == 3 {
            locationmanage.startUpdatingLocation()
            locations1.append(locations[locations.count - 1])
        }else {
          //  locationmanage.stopUpdatingLocation()
        }
    }
    
    func getDistance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance {
        // By Aviel Gross
        // https://stackoverflow.com/questions/11077425/finding-distance-between-cllocationcoordinate2d-points
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return from.distance(from: to)
    }
    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
        print(error)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
