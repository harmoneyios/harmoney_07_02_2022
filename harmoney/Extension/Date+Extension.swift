
//
//  Date+Extension.swift
//  Harmoney
//
//  Created by Saravanan on 18/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation


extension Date{
    func getFilterDateFormat()->String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        return dateFormat.string(from: self)
    }
    
    func goalUpdateFormat()->String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = DateFormatType.iso8601
        return dateFormat.string(from: self)
    }
    
    func getDateStrFrom(format : String = DateFormatType.formattedDate)->String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = format
        return dateFormat.string(from: self)
    }
    
}
