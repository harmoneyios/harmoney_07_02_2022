//
//  CuisinesTableViewCell.swift
//  Ludi
//
//  Created by Prema Ravikumar on 05/03/19.
//  Copyright © 2019 Inq. All rights reserved.
//

import UIKit

class CuisinesTableViewCell: UITableViewCell {
    
    //Outlet
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var brandNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var reedemBtn: UIButton!
    @IBOutlet weak var borderView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        reedemBtn.layer.cornerRadius = 12.5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
