//
//  NSError+Extension.swift
//  Attigo
//
//  Created by Norbert Korosi on 11/09/2019.
//  Copyright © 2019 Attigo. All rights reserved.
//

import Foundation

extension NSError {
    public static func createUnexpectedError() -> NSError {
        let userInfo = [NSLocalizedDescriptionKey: "Unexpected error occured"];
        let error = NSError(domain: APIService.defaultErrorDomain, code: APIService.defaultErrorCode, userInfo: userInfo)
        
        return error
    }
    
    public static func createInvalidURLError() -> NSError {
        let userInfo = [NSLocalizedDescriptionKey: "Invalid URL"];
        let error = NSError(domain: APIService.defaultErrorDomain, code: APIService.defaultErrorCode, userInfo: userInfo)
        
        return error
    }
    
    public static func createDefaultError(message: String?, statusCode: Int?) -> NSError {
        let userInfo = [NSLocalizedDescriptionKey: message ?? "Unexpected error occured"];
        let error = NSError(domain: APIService.defaultErrorDomain, code: statusCode ?? APIService.defaultErrorCode, userInfo: userInfo)
        
        return error
    }
}
