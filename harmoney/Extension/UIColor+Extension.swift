//
//  UIColor+Extension.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/20/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
    
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(hex: Int) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF
        )
    }
    
    static let harmoneyLightGreenColor = UIColor.init(hex: 0x85BF8E)
    static let harmoneyLightBlueColor = UIColor.init(hex: 0x3879C6)
    static let harmoneyLightRedColor = UIColor.init(hex: 0xD71C2B)
    static let harmoneyLightGrayColor = UIColor.init(hex: 0xDCE1F1)
    static let harmoneyDarkGrayColor = UIColor.init(hex: 0x8F98B3)
    static let harmoneyLightOrangeColor = UIColor.init(hex: 0xF8A79E)
    static let harmoneyBlueColor = UIColor.init(hex: 0x3879C6)
    static let harmoneyGrayColor = UIColor.init(hex: 0xCCCCCC)
    static let harmoneyDarkBlueColor = UIColor.init(hex: 0x1E3F66)
    static let harmoneyOpacityGrayColor = UIColor.init(hex: 0x707070)
    
    static let harmoneyRunGrayColor = UIColor.init(hex: 0x828690)
    static let harmoneySwimBlueColor = UIColor.init(hex: 0x5089CD)
    static let harmoneyBikeBrownColor = UIColor.init(hex: 0x3F1D0B)
    
    static let homeScreenBorderColor = UIColor.init(hex: 0x92BE91)
    static let homelightgrayColor = UIColor.init(hex: 0xEDEFF3)
    static let harmoneySectionHeaderColor = UIColor.init(hex: 0xF2F6FB)
    static let harmoneyPrimaryColor =  UIColor.init(hex: 0x2680EB)
    static let harmoneyNonPrimaryColor =  UIColor.init(hex: 0x707070)
    static let harmoneyNonPrimaryBorderColor = UIColor.init(hex: 0xDCE1F1)
    static let hflBlue = UIColor.init(hex: 0x1E3F66)
}


