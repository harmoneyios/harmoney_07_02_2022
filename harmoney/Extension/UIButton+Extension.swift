//
//  UIButton+Extension.swift
//  Harmoney
//
//  Created by GOPI K on 18/04/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

@IBDesignable
class ButtonExtension: UIButton {
    
    @IBInspectable var cornurRadius: CGFloat = 1.0 {
        didSet {
            layer.cornerRadius = cornurRadius
            clipsToBounds = true
        }
    }
    
    @IBInspectable var borderwidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderwidth
            clipsToBounds = true
        }
    }
    
    @IBInspectable var backgroundButtonColor: UIColor? {
        didSet {
            layer.backgroundColor = backgroundButtonColor?.cgColor
        }
    }
    
    @IBInspectable var attributedString: NSAttributedString? {
        didSet {
            setAttributedTitle(attributedString, for: .normal)
        }
    }
    
    @IBInspectable var titleColor: UIColor? {
        didSet {
            setTitleColor(titleColor, for: .normal)
        }
    }

    
//    @IBInspectable var tintColour: UIColor = UIColor.blue {
//        didSet {
//            imageView?.setImageColor(color: tintColour)
//        }
//    }
}


extension UIButton{
    func underline() {
            guard let text = self.titleLabel?.text else { return }
            let attributedString = NSMutableAttributedString(string: text)
            //NSAttributedStringKey.foregroundColor : UIColor.blue
            attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
            self.setAttributedTitle(attributedString, for: .normal)
        }
}
