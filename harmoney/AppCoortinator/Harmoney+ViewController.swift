//
//  Harmoney+ViewController.swift
//  Harmoney
//
//  Created by INQ Projects on 16/12/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import SVProgressHUD

struct ErrorConstants {
    static let genericErrorMessage = "Error Occurred. Please try again in sometime."
    static let serverError = "Server failed to process your request"
    static let badRequest = "Unable to process your request. Please try again."
    static let resourceNotFound = "Please try again later (Resource Not Found)."
}

extension UIViewController {
    class var identifier: String {
        return "\(self)"
    }

    /**
     This is a method to instatiate a view controller from a storyboard.
     - parameter storyboard: An object of type Storyboards that contains the desired view controller
     * the only rule here is that you should keep the storyboardId in the storyboard similar to the name of the viewController, if not then provide your storyboardId before instantiating your viewController.
     * It can be used as --->
     * let homeScene = HomeVC.instantiateFrom(storyboard: .main)
     */
   
    func toast(message: String, toastDuration: Double = 3.0) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        let toastLbl = UILabel()
        toastLbl.text = message
        toastLbl.textAlignment = .center
        toastLbl.font = UIFont.preferredFont(forTextStyle: .subheadline)
        toastLbl.textColor = UIColor.white
        toastLbl.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        toastLbl.lineBreakMode = .byWordWrapping
        toastLbl.numberOfLines = 0

        let textSize = toastLbl.intrinsicContentSize
        let labelHeight = ( textSize.width / window.frame.width ) * (textSize.height + 22.0)
        let labelWidth = min(textSize.width, window.frame.width - 40)
        let adjustedHeight = max(labelHeight, textSize.height + 20)

        toastLbl.frame = CGRect(x: 20, y: (window.frame.height - 90 ) - adjustedHeight, width: labelWidth + 20, height: adjustedHeight)
        toastLbl.center.x = window.center.x
        toastLbl.layer.cornerRadius = 10
        toastLbl.layer.masksToBounds = true

        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }

        DispatchQueue.main.async {
            window.addSubview(toastLbl)
            UIView.animate(withDuration: 1.0, delay: toastDuration, options: UIView.AnimationOptions.allowAnimatedContent, animations: {
                toastLbl.alpha = 0
            }, completion: { _ in
                toastLbl.removeFromSuperview()
            })
        }
    }

    @objc func handleError(error: Error?, message: String? = nil) {
        let defaultMessage: String = message ?? ErrorConstants.genericErrorMessage
        guard let throwableError = error else {
            toast(message: defaultMessage)
            return
        }
        var errorMessage: String = ErrorConstants.genericErrorMessage
        do {
            throw throwableError
        } catch NetworkError.unAuthorised {
            NotificationCenter.default.post(name: Notification.Name.userSessionExpired, object: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) { [weak self] in
                self?.toast(message: NetworkError.unAuthorised.localizedDescription)
            }
            return
        } catch let networkError as NetworkError {
            errorMessage = networkError.localizedDescription
        } catch {
            errorMessage = "\(error.localizedDescription). Please try again"
        }
        toast(message: errorMessage)
    }

    func showLoader() {
        SVProgressHUD.show()
    }

    func hideLoader() {
        SVProgressHUD.dismiss()
    }

    func checkForTextClicked(text: String, containerLabel: UILabel!, tapGesture: UITapGestureRecognizer, onSuccessCallBack: () -> Void) {

        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: containerLabel.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = containerLabel.lineBreakMode
        textContainer.maximumNumberOfLines = containerLabel.numberOfLines
            let labelSize = containerLabel.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = tapGesture.location(in: containerLabel)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(
            x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
            y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y
        )
        let locationOfTouchInTextContainer = CGPoint(
            x: locationOfTouchInLabel.x - textContainerOffset.x,
            y: locationOfTouchInLabel.y - textContainerOffset.y
        )
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        let targetRange = NSString(string: containerLabel.text!).range(of: text)
        if NSLocationInRange(indexOfCharacter, targetRange) {
            onSuccessCallBack()
        }
    }

}
extension Notification.Name {
    static let userSessionExpired = Notification.Name(rawValue: "AccountManagerUserSessionExpired")
    static let userDidLogout = Notification.Name(rawValue: "AccountManagerUserDidLogout")
}
