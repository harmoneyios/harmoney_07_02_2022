//
//  Coordinator.swift
//  FitKet
//
//  Created by Hariharan G on 5/19/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit


protocol Coordinator {
    func start()
}
