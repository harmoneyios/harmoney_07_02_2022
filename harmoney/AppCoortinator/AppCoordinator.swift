//
//  AppCoordinator.swift
//  FitKet
//
//  Created by Hariharan G on 5/19/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit
import Alamofire
class AppCoordinator: NSObject, Coordinator {

    private let window: UIWindow

    private(set) var configdatas: ConfigModel?

    init(window: UIWindow) {
        self.window = window

        super.init()
    }

    func start() {

        fetchUserConfiguration()
    
    }
    private func fetchUserConfiguration() {
        self.callConfig()

    }
    
    func callConfig()  {
        self.window.rootViewController?.showLoader()

        if Reachability.isConnectedToNetwork() {
        let (url, method, param) = APIHandler().getConfig(params: [:])
        AF.request(url, method: method, parameters: param).validate().responseJSON { response in
            self.window.rootViewController?.hideLoader()

                    switch response.result {
                       case .success(let value):
                        if let value = value as? [String:Any] {
                            let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                            let configModel = try? JSONDecoder().decode(ConfigModel.self, from: jsonData!)
                            self.configdatas = configModel
                            guard self.isAppUpdateRequired() == false else {
                                             return
                                         }
                        }
                        break
                        case .failure(let error):
                        break
                       }
            }
        }
    }

    func getAppUpdate() -> AppUpdate? {
        return  configdatas?.data.appUpdate
    }
    private func isAppUpdateRequired() -> Bool{
        if let appdata = getAppUpdate() {
            
            if appdata.type == "Critical",  let currentVersion: Int = Int(Utilities.appVersionCode()),
            let updateVersion:Int =  Int(appdata.version), currentVersion < updateVersion
             {
                
                let alert = UIAlertController(title: "App Update Available", message: appdata.message, preferredStyle: .alert)
                let action = UIAlertAction(title: "Update", style: .default, handler: { _ in

                    
                    if UIApplication.shared.canOpenURL(URL(string: appdata.url)!) {
                        UIApplication.shared.open(URL(string: appdata.url)!, options: [:]) { (flag) in
                            
                        }
                    }
                })
                action.setValue(UIColor.harmoneyLightBlueColor, forKey: "titleTextColor")
                alert.addAction(action)
                alert.setTitlet(font: UIFont.futuraPTBookFont(size: 20), color: UIColor.black)
                
                alert.setMessage(font:  UIFont.futuraPTBookFont(size: 15) ,  color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.9))

                window.rootViewController?.present(alert, animated: true, completion: nil)
                return true
            }
            return false
        }
        else {
            return false
        }
    }
}


