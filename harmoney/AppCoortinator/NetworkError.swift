//
//  NetworkError.swift
//  FitKet
//
//  Created by Hariharan G on 5/19/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

enum NetworkError: Error {
    case noInternetAccess
    case unAuthorised
    case requestTimedOut
    case badRequest
    case serverError (reason: String)
    case unknown(reason: String)
}

extension NetworkError: CustomStringConvertible {
    var description: String {
        switch self {
        case .noInternetAccess: return "Please check your internet connection and try again"
        case .unAuthorised: return "Session Expired. Please sign in again"
        case .requestTimedOut: return "Request timed out"
        case .badRequest: return ErrorConstants.badRequest
        case .serverError (let reason): return "\(ErrorConstants.serverError) (\(reason)). Please try again."
        case .unknown(let reason): return reason
        }
    }

    var localizedDescription: String {
        return self.description
    }

}
