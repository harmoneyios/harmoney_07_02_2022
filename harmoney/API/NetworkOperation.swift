//
//  NetworkOperation.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/1/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import Alamofire

struct NetworkOperation {
    var method: HTTPMethod
    var parameters: Parameters?
    var headers: HTTPHeaders?
    var encoder: ParameterEncoding
    var url: String
}
