//
//  APIService.swift
//  Ludi
//
//  Created by Prema Ravikumar on 17/03/19.
//  Copyright © 2019 Inq. All rights reserved.
//

import Foundation
class APIService: NSObject {
    
    static let defaultErrorCode : Int = 400
    static let defaultErrorDomain = "Backend Manager"
    
   // static let base_URL = "http://api.harmoneydemo.com/"
    //live server
    static let base_URL = "https://api.harmoney.ai/"
    // development server
//    static let base_URL = "https://stage.api.harmoney.ai/"
    static let getOtp =  base_URL + "v1/user/getOtp"
    static let sendOtp =  base_URL + "user/sendOtp"
    static let profile =  base_URL + "user/profile"
    static let dashboard =  base_URL + "getDashboard"
    static let getChallenges =  base_URL + "getChallenges"
    static let acceptChallenges =  base_URL + "acceptChallenge"
    static let updateUserChallenge =  base_URL + "updateUserChallenge"
    static let getPaymentInformation =  base_URL + "v1/sila/getAccountInformation"
    static let getWallet =  base_URL + "v1/sila/get_wallet"
    static let getAccount =  base_URL + "v1/sila/get_accounts"
    static let viewTransactionDetails =  base_URL + "v1/sila/get_transactions"
    static let paymentCheckHandle =  base_URL + "v1/sila/check_handle"
    static let subAccountRegister =  base_URL + "v1/sila/register_wallet"
    static let subAccountEdit =  base_URL + "v1/sila/update_wallet"
    static let deleteSilaAmount =  base_URL + "v1/sila/delete_account"
    static let getBalaceAfterissue =  base_URL + "v1/sila/get_account_balance"
    static let reduceHarmoneyWallet =  base_URL + "v1/sila/reduceHarmoneyWallet"
    static let transferAmountSila =  base_URL + "v1/sila/transfer_sila"
    static let addHarmoneyWallet =  base_URL + "v1/sila/addHarmoneyWallet"
    static let addharmoneytoparent =  base_URL + "v1/sila/addHarmoneyWalletParent"
    static let acceptAmountTransferPersonalChallenge = base_URL + "v1/notification/update"
    static let redeemSilaAmount =  base_URL + "v1/sila/redeem_sila"
    static let silaIssueAmount =  base_URL + "v1/sila/issue_sila"
    static let guidForMobileNumber =  base_URL + "v1/sila/getMobileToGuid"
    static let silaLinkAccount =  base_URL + "v1/sila/link_account"
    static let checkKYC =  base_URL + "v1/sila/check_kyc"
    static let paymentRegisterSila =  base_URL + "v1/sila/register"
    static let requestKYC =  base_URL + "v1/sila/request_kyc"
    static let getPlaidLinkToken =  base_URL + "v1/sila/plaid_link_token"
    static let getNotifications =  base_URL + "getNotifications"
    static let getAllNotifications =  base_URL + "v1/getNotifications"
    static let inviteFriends =  base_URL + "inviteFriends"
    static let collectReward =  base_URL + "collectReward"
    static let clearAllNotification =  base_URL + "v1/clearAllNotification/"
    static let AddTask =  base_URL + "personalFitness/addTask"
    static let updateTask =  base_URL + "personalFitness/updateTask"
    static let deleteTask =  base_URL + "personalFitness/deleteTask"
    static let editTask =  base_URL + "personalFitness/editTask"
    static let updateDeviceToken =  base_URL + "user/pushtokenUpdate"
    static let goalList =  base_URL + "goals"
    static let saveGoal =  base_URL + "goals"
    static let goalEnd =  base_URL + "goalsCompletion"
    static let updateNotification = base_URL + "v1/updateNotification"
 
    static let shipping =  base_URL + "shipping"
    static let logOut =  base_URL + "logout"
    
    //
    static let getAllCharity =  base_URL + "getAllCharity"
    static let getCharityDepartment =  base_URL + "getCharityDepartment"
    static let giveGoals =  base_URL + "givegoals"
    static let giveGoalDeletion =  base_URL + "giveGoalDeletion"

    static let profileUpdate =  base_URL + "user/profileUpdate"
    static let getProfile =  base_URL + "getUserProfile"
    static let challengeShare =  base_URL + "challengeShare"

    
    static let getinvitedMember =  base_URL + "v1/inviteMember"
    
    static let putInvitedMember =  base_URL + "v1/inviteMember"
    
    static let deleteInvitedMember =  base_URL + "v1/inviteMember"
    
    static let invitedMember =  base_URL + "v1/inviteMember"
    
    static let appConfig =   base_URL + "appConfig"
    
    static let getInterest =   base_URL + "v1/getAllInterests"
    
    static let updateInterest =   base_URL + "v1/createUserInterestedItem"

    static let getUserInterests = base_URL + "v1/getUserInterestedItem"
    
    //Personal Fitness
    static let addPersonalChallengeOld = base_URL + "v2/pf/addTask"
    static let addPersonalChallenge = base_URL + "v3/pf/addTask"
    static let getPersonalChallenges = base_URL + "v3/pf/getAllPersonalFitnessTask"
    static let getPersonalChallengesHbot = base_URL + "v1/yuru/getTask/pf"
   
    static let changePersonalChallengeStatus = base_URL + "v2/pf/letGetStart"
    static let deletePersonalChallenge = base_URL + "v2/pf/deleteTask"
    static let updatePersonalChallenge = base_URL + "v2/pf/updatePersonalFitness"
    static let claimPersonalChallengeReward = base_URL + "user/rewards/claim"
    static let getAllCustomReward = base_URL + "user/rewards/list"
    static let completePersonalChallenge = base_URL + "v2/pf/completePersonalFitness"
    static let getPersonalChallenge = base_URL + "v1/taskApprovalView"
    static let acceptPersonalChallenge = base_URL + "v1/taskApprovalAccept"
    static let rejectPersonalChallenge = base_URL + "v1/taskApprovalReject"
   

    //Invites
    static let getInvites = base_URL + "v1/inviteMember/"
    static let getAssingtoList = base_URL + "v1/inviteMember/assign/"
    static let inviteMember = base_URL + "v2/inviteMember"
//    static let acceptInvite = base_URL + "v2/inviteMemberAccept"
    static let acceptInvite = base_URL + "v3/inviteMemberAccept"
    static let acceptChores = base_URL + "v1/addChores"
    
    static let acceptChoresNew = base_URL + "v2/addChores"
//    static let acceptParentInvite = base_URL + "v2/inviteMemberAccept"
    static let acceptParentInvite = base_URL + "v3/inviteMemberAccept"

    static let rejectParentInvite = base_URL + "v1/inviteMemberReject"

    
    //Earn
    static let getSubCatagory = base_URL + "getSubCatagory"
    static let getChorslist = base_URL + "v2/getallChores"
    static let getChorslistHBot = base_URL + "v1/yuru/getTask/chores"
    static let deleteChore =  base_URL + "v1/deleteChores/"
    static let dailyCompleteChore =  base_URL + "v1/dailyCompletionChores"
    static let choreTaskDetails = base_URL + "v1/taskApprovalView"
    static let acceptChore = base_URL + "v1/taskApprovalAccept"
    static let rejectChore = base_URL + "v1/taskApprovalReject"
    static let getCityInformation =  base_URL + "v1/sila/get_city"

    static let rejectInvite = base_URL + "v1/inviteMemberReject"
    static let cancelInviteMember = base_URL + "v1/revokeInviteMember"

    //Home
    static let getHome = base_URL + "getHome/"
    static let completeChoreEarn = base_URL + "v1/completeChores"
    
    static let getConfig = base_URL + "user/config"
    
    //Coupons
    static let getAllCoupons = base_URL + "v1/getAllCoupons"
    static let getPurchasedCoupons = base_URL + "v1/purchasedCoupons"
    static let purchaseCoupon = base_URL + "v1/purchaseCoupon"
    static let claimCoupon = base_URL + "v1/claimCoupon"
    
    //DailyChallenge
    static let getDailyChallenges = base_URL + "user/getAllDailyChallanges"
    static let updateChallengeStatus = base_URL + "user/updateChallengeStatus"
    
    //Levels
    static let getLevels = base_URL + "user/levels"
    static let addRewardForLevel = base_URL + "user/levels/addRewards"
    static let claimLevelReward = base_URL + "user/levels/claim"
    static let acceptLevelReward = base_URL + "v1/customRewardAccept"
    static let rejectLevelReward = base_URL + "v1/customRewardReject"
    
    //Vouchers
    static let getVouchers = base_URL + "user/voucher/list"
    static let claimVoucher = base_URL + "user/voucher/claim"
    
    static let getFamilyInvites = base_URL + "v1/inviteMember"
    static let inviteAgaian = base_URL + "v1/inviteAgain"
    
    //getLeagueList
    
    static let getLeagueList = base_URL + "user/getLeagueList"
    
    //Update HealthKit data
    
    static let updateHealthkit = base_URL + "user/updateHealthKitData"
    
    //Coupon Details
    
    static let couponPurchase = base_URL + "v1/coupon/purchase"
    static let couponPurchaseList = base_URL + "v1/coupon/purchasedList"
    static let couponClaim = base_URL + "v1/coupon/claim"
    static let couponPurchaseClaimedList = base_URL + "v1/coupon/claimedList"

    //Coupon List
    static let getCouponToolsList = "https://api4coupons.com/v3/coupon/list"
    
    //Coupon Claims
    static let getCouponClaims = "https://api4coupons.com/v3/campaign/claims"
    
    //Coupon Validation
    static let getCouponValidation = "https://api4coupons.com/v3/campaign/validations"
    
    //yuro Get Token
    static let getYuroToken = "https://api.dev.cogapps.net/token/"
    
    //yuro Get TextEmotion
    static let getYuroTextEmotion = "https://api.dev.cogapps.net/simplified/"
    
    static let getTaskCountInfo = base_URL + "v1/yuru/gettaskcount"
    
    static let changePrimary = base_URL + "v1/inviteMember/primary"
    
    static let getYuruMorningMessage = base_URL + "v1/getYuruMsg"
    
    static let checkChallengeStatus = base_URL + "v2/pf/letsDoIt"
    
    
    //HFL
    static let getClubListOFLegue = base_URL + "v1/fplleague/franchiesList"
    static let getSubteamList = base_URL + "v2/fplleague/subTeamList"
    static let validatePassword = base_URL + "v2/fplleague/validate"
    static let joinTeam = base_URL + "v2/fplleague/joinNow"
    static let getHFLDashboardDetails = base_URL + "v2/fplleague/teamView"
    static let getTeamWiseLeaderBoard = base_URL + "v2/fplleague/teamWise"
    static let getStatisticsDetails = base_URL + "v2/fplleague/playerProfile"
    static let getChartDetails = base_URL + "v2/fplleague/playerProfileChart"
    
    static let getClubWiseLeaderBoard = base_URL + "v2/fplleague/clubWise"
    static let getUserWiseLeaderBoard = base_URL + "v2/fplleague/userWise"
    
    //static let getNewLeagueList = base_URL + "v2/fplleague/getLeaguesNew"
    static let getNewLeagueList = base_URL + "v2/fplleague/getLeaguesNewV1"
    static let getGoalsDetails = base_URL + "v2/fplleague/getDailyGoal"
    static let udpateGoalDetails = base_URL + "v2/fplleague/addDailyGoalDonation"
    static let update2KCompletions = base_URL + "v2/fplleague/add2KConfirmation"
    static let getPopupNotifications = base_URL + "v1/getActionNotifications"
    
    
    static let uploadDoc =  base_URL + "v1/sila/upload_document"

    
}
    


