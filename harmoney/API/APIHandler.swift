//
//  APIHandler.swift
//  Ludi
//
//  Created by Prema Ravikumar on 16/03/19.
//  Copyright © 2019 Inq. All rights reserved.
//

import UIKit
import Alamofire

class APIHandler: NSObject {
    
    func getOTP(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getOtp, .post, params)
    }
    
    func sendOtp(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
          
          return (APIService.sendOtp, .post, params)
      }
    func profile(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
          
          return (APIService.profile, .post, params)
      }
    func dashboard(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
          
          return (APIService.dashboard, .get, params)
      }
    
    func getChallenges(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
            
            return (APIService.getChallenges, .get, params)
        }
    
    func acceptChallenges(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
              
              return (APIService.acceptChallenges, .get, params)
          }
    
    func updateUserChallenge(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
            
            return (APIService.updateUserChallenge, .post, params)
        }
    func getWalletdata(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getWallet, .post, params)
    }
    func getInformationForPayment(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getPaymentInformation, .post, params)
    }
    func getAccountdata(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getAccount, .post, params)
    }
    func paymentCheckKYC(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.checkKYC, .post, params)
    }
    func paymentRegisterforSila(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.paymentRegisterSila, .post, params)
    }
    func paymentrequestKYC(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.requestKYC, .post, params)
    }
    func subAccountRegister(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.subAccountRegister, .post, params)
    }
    func subAccountEdit(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.subAccountEdit, .post, params)
    }
    func paymentUserCheckHandle(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.paymentCheckHandle, .post, params)
    }
    func viewTransactionDetails(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.viewTransactionDetails, .post, params)
    }
    func addHarmoneyWallet(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.addHarmoneyWallet, .post, params)
    }
    func addharmoneytoparent(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.addharmoneytoparent, .post, params)
    }
    func silaLinkAccount(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.silaLinkAccount, .post, params)
    }
    func deleteSilaAmount(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.deleteSilaAmount, .post, params)
    }
    func getGuidForMobileNumber(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.guidForMobileNumber, .post, params)
    }
    func transferAmountToSila(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.transferAmountSila, .post, params)
    }
    func reduceHarmoneyWallet(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.reduceHarmoneyWallet, .post, params)
    }
    func redeemSilaAmount(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.redeemSilaAmount, .post, params)
    }
    func silaIssueAmount(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.silaIssueAmount, .post, params)
    }
    func getBalanceAfterSilaissue(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getBalaceAfterissue, .post, params)
    }
    func getPlaidLinkToken(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getPlaidLinkToken, .post, params)
    }
    func getNotifications(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
                 
    return (APIService.getNotifications, .get, params)
    }
    
    func getAllNotifications(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
                 
    return (APIService.getAllNotifications, .get, params)
    }
    
    
    func inviteFriends(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
            
            return (APIService.inviteFriends, .post, params)
        }
    
    func collectReward(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
                   
      return (APIService.collectReward, .get, params)
      }
    
    func clearAllNotification(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
                      
      return (APIService.clearAllNotification, .get, params)
    }
    func getCityInformation(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getCityInformation, .post, params)
    }

    func AddTask(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
            
            return (APIService.AddTask, .post, params)
        }
    func updateTask(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
             
             return (APIService.updateTask, .post, params)
         }
    func deleteTask(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
             
             return (APIService.deleteTask, .post, params)
         }
    func editTask(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
             
             return (APIService.editTask, .post, params)
         }
    
    func shipping(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
          
          return (APIService.shipping, .post, params)
      }
    
    func logOut(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.logOut, .post, params)
    }
    
    func updateDeviceToken(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
               
               return (APIService.updateDeviceToken, .post, params)
           }
    
    func updateProfile(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.profileUpdate, .put, params)
    }
    
    func getProfile(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getProfile, .get, params)
    }
    
    func inviteMember(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        return (APIService.getinvitedMember, .get, params)
    }
    
    func inviteNewMember(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        return (APIService.invitedMember, .post, params)
    }
    
    func putInviteMember(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        return (APIService.putInvitedMember, .put, params)
    }
    
    func deleteInviteMember(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        return (APIService.deleteInvitedMember, .delete, params)
    }

    
     func challengeShare(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
                
                return (APIService.challengeShare, .post, params)
            }

    func appConfig(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
                 
                 return (APIService.appConfig, .get, params)
             }
    func getInterests(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getInterest, .get, params)
    }
    func updateInterest(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.updateInterest, .post, params)
    }
    func getUserInterests(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getUserInterests, .get, params)
    }
    
   func acceptChoreAPI(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.acceptChoresNew, .post, params)
    }
    
    func getSubCatagory(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getSubCatagory, .get, params)
    }

    func getChorslist(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getChorslist, .post, params)
    }
    func getChorslistHBot(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getChorslistHBot, .post, params)
    }

    func deleteChore(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        return (APIService.deleteChore, .delete, params)
    }
    func dailyCompleteChore(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
         
         return (APIService.dailyCompleteChore, .post, params)
     }
    func acceptParentInvite(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
         
         return (APIService.acceptParentInvite, .post, params)
     }
    func rejectParentInvite(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
         
         return (APIService.rejectParentInvite, .post, params)
     }
    func getChoreTaskDetails(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.choreTaskDetails, .post, params)
    }
    func acceptChoreInvite(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.acceptChore, .post, params)
    }
    func rejectChoreInvite(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.rejectChore, .post, params)
    }
    
    func completeChoreEarnPoints(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.completeChoreEarn, .put, params)
    }

    func getConfig(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getConfig, .get, params)
    }
    
    func getFamilyInvitees(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
           
           return (APIService.getFamilyInvites, .get, params)
       }

    
    func getLeagueList(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getLeagueList, .post, params)
    }

    func updateHealthkitData(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.updateHealthkit, .post, params)
    }

    
    func getClubListOfLeague(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getClubListOFLegue, .post, params)
    }
    func getSubteamList(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getSubteamList, .post, params)
    }
    
    func validateEventPassword(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.validatePassword, .post, params)
    }
    
    func joinTeam(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.joinTeam, .post, params)
    }
    
    func getHFLDashBoardDetails(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getHFLDashboardDetails, .post, params)
    }
    
    
    func getTeamWiseLeaderBoard(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getTeamWiseLeaderBoard, .post, params)
    }
    
    func getStatisticsDetails(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getStatisticsDetails, .post, params)
    }
    
    func getHFLChartDeatails(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getChartDetails, .post, params)
    }
    
    func getClubWiseLeaderBoard(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getClubWiseLeaderBoard, .post, params)
    }
    
    func getUserWiseLeaderBoard(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getUserWiseLeaderBoard, .post, params)
    }
    
    
    func getNewLeagueList(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getNewLeagueList, .post, params)
    }
    
    func getGoalsDetails(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getGoalsDetails, .post, params)
    }
    func udpateGoalDetails(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.udpateGoalDetails, .post, params)
    }
    func update2KCompletions(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.update2KCompletions, .post, params)
    }
    func getPopupNotifications(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.getPopupNotifications, .get, params)
        }

    func updloadDocument(params: [String:Any]) -> (String, HTTPMethod, [String:Any]) {
        
        return (APIService.uploadDoc, .post, params)
    }

}

