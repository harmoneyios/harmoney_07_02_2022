//
//  HarmoneyAPIService.swift
//  Harmoney
//
//  Created by sureshkumar on 12/21/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SVProgressHUD

public typealias APIDictionary = Dictionary<String, Any>

enum BackendError: Error {
    case network(error: Error) // Capture any underlying Error from the URLSession API
    case dataSerialization(error: Error)
    case jsonSerialization(error: Error)
    case xmlSerialization(error: Error)
    case objectSerialization(reason: String)
}

class HarmoneyAPIService {

    
    static let sharedAPI : HarmoneyAPIService = HarmoneyAPIService()
    func performPostRequest <T: AnyObject> (type: T.Type,urlString : String,_ params :Parameters,_ methodType : HTTPMethod ,_ header : HTTPHeaders, success :@escaping (_ response : AFDataResponse<T>) -> Void , failure : @escaping (( _ response : AFDataResponse<T>,_ error :NSError?) -> Void))->Void where T:Mappable {
     
         AF.request(urlString, method: methodType, parameters: params, encoding: JSONEncoding.prettyPrinted).responseObject { (response: AFDataResponse<T>) in
        
        
            //    print("Request: \(String(describing: response.request))")   // original url request
           //     print("Response: \(String(describing: response.response))") // http url response
             //   print("Result: \(response.result)")                     // response serialization result
                
          
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                   // print("Data: \(utf8Text)") // original server data as UTF8 string
                }
            
           
            
//            switch response.result {
//            case .success(let value):
//                success(value.self)
//                case .failure(let error):
//                    print(error)
//                    if let err = error as? URLError, err.code == .notConnectedToInternet {
//                      
//                        
//                    } else {
//                        failure(response,error as NSError)
//                    }
//                }
                
        }
        
        
    
        
    }
    
    
   

    
}
