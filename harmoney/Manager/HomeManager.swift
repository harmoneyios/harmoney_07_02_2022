//
//  HomeManager.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/26/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import Alamofire

class HomeManager {
    static let sharedInstance = HomeManager()
    var homeModel: HomeModel?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    private init() {}
    
    //MARK: Home API's
    
    func getHomeModel(handler: @escaping (Swift.Result<HomeModel, NSError>) -> Void) {
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: JSONEncoding.default, url: APIService.getHome + (UserDefaultConstants().guid ?? ""))
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.homeModel = try JSONDecoder().decode(HomeModel.self, from: data as! Data)
                    handler(.success(self.homeModel!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
}
