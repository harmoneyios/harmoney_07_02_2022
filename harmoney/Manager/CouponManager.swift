//
//  CouponManager.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/16/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import Alamofire

class CouponManager {
    static let sharedInstance = CouponManager()
    var gemProduct: GemProduct?
    var claimedCoupons: ClaimedCoupons?
    var couponPurchased: Bool = false
    var couponTools: CouponToolsModel?
    var couponValidation: CouponValidationModel?
    var youroToken: YouroTokenModel?
    var youroTextEmotion: YouroTextEmotionModel?
    var couponPurchasedList: CouponPurchasedListModel?
    var couponPurchasedClaimedList: PurchasedCouponClaimedList?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    
    private init() {}
    
//MARK: Coupon API's
    
    func getAllCoupons(handler: @escaping (Swift.Result<GemProduct, NSError>) -> Void) {
        let operation = NetworkOperation(method: .post, parameters: nil, headers: headers, encoder: JSONEncoding.default, url: APIService.getAllCoupons)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.gemProduct = try JSONDecoder().decode(GemProduct.self, from: data as! Data)

                    handler(.success(self.gemProduct!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }

        }
    }
    
    func purchaseCoupon(nonPromoList: NonPromoList, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        var product : [String: Any] = ["intProductId": nonPromoList.intProductID,
                                       "intProductClaimType": nonPromoList.intProductClaimType,
                                       "intProductClaimValue": nonPromoList.intProductClaimValue,
                                       "qty": 1]
        
        
        var params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "product": [product]]
        if HarmonySingleton.shared.isSecondChallange {
           params["onboardingCoupon"] = 0
        }
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.purchaseCoupon)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(_):
                self.couponPurchased = true
                
                handler(.success("Success"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func getPurchasedCoupons(handler: @escaping (Swift.Result<ClaimedCoupons, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "intpage": 1,
                                   "intproductContentSize": 50]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getPurchasedCoupons)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.claimedCoupons = try JSONDecoder().decode(ClaimedCoupons.self, from: data as! Data)

                    handler(.success(self.claimedCoupons!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }

        }
    }
    
    func claimCoupon(claimedCoupon: Datum, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "orderId": claimedCoupon.orderID]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.claimCoupon)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(_):
                self.claimedCoupons?.data.removeAll(where: {$0.orderID == claimedCoupon.orderID})
                
                handler(.success("Success"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    
    func getClaimedCouponList(handler: @escaping (Swift.Result<ClaimedCoupons, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "intpage": 1,
                                   "intproductContentSize": 50]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getPurchasedCoupons)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.claimedCoupons = try JSONDecoder().decode(ClaimedCoupons.self, from: data as! Data)

                    handler(.success(self.claimedCoupons!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }

        }
    }
    
    //MARK: Coupon List API's
        func getAllCouponsToolsList(handler: @escaping (Swift.Result<CouponToolsModel, NSError>) -> Void) {
            
            let param: [String: Any] = [
                "client_id" : "1663728399562114149494954323677",
                "client_secret" : "9b667u1yZqVJZDmzUWtwTY4fcb833yf",
                "only_active" : true
            ]
            
            let operation = NetworkOperation(method: .post, parameters: param, headers: headers, encoder: JSONEncoding.default, url: APIService.getCouponToolsList)
            
            BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
                switch result {
                case .success(let data):
                    do {
                        self.couponTools = try JSONDecoder().decode(CouponToolsModel.self, from: data as! Data)

                        handler(.success(self.couponTools!))
                    } catch {
                        handler(.failure(error as NSError))
                    }
                case .failure(let error):
                    handler(.failure(error as NSError))
                }

            }
        }
    
    //MARK: Coupon List Claims API's

    func getAllCouponsClaimsApi(claimedCouponID: String,currentTime: String, handler: @escaping (Swift.Result<CouponValidationModel, NSError>) -> Void) {
        
        let params : Parameters = [
            "client_id" : "1663728399562114149494954323677",
            "client_secret" : "9b667u1yZqVJZDmzUWtwTY4fcb833yf",
            "campaign" : claimedCouponID
//            "start_date" : currentTime
        ]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getCouponClaims)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.couponValidation = try JSONDecoder().decode(CouponValidationModel.self, from: data as! Data)

                    handler(.success(self.couponValidation!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    
    //MARK: Coupon List Validation API's

    func getAllCouponsValidationApi(claimedCoupon: CouponInfo,currentTime: String, handler: @escaping (Swift.Result<CouponValidationModel, NSError>) -> Void) {
        let params : Parameters = [
            "client_id" : "1663728399562114149494954323677",
            "client_secret" : "9b667u1yZqVJZDmzUWtwTY4fcb833yf",
            "campaign" : claimedCoupon.id
//            "start_date" : currentTime
        ]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getCouponValidation)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.couponValidation = try JSONDecoder().decode(CouponValidationModel.self, from: data as! Data)

                    handler(.success(self.couponValidation!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
//    func request(variable1: String?, variable2: String?, audioFilePath: URL, completion: @escaping (String) -> ()) {
//        let url = URL(string: "https://...")!
//
//        let headers: HTTPHeaders = [
//                "content-type": "multipart/form-data; boundary=---011000010111000001101001",
//                "accept": "application/json",
//                "authorization": "Bearer aetefdsfafdfjksjdflkdfljdlkfjlksdjfsdoieiworiouirewiouiuiufdspufpuiufdilkdjjjklkfjldskljfdjkljklsdljkljksd"
//            ]
//            var parameters: [String: Any] = [:]
//
//
//            if variable1 != nil {   parameters["var1"] = variable1 }
//            if variable2 != nil {   parameters["var2"] = variable2 }
//
//            // Let's check the data
//            print(parameters)
//
//            AF.upload(multipartFormData: { multipartFormData in
//
//                for (key,value) in parameters {
//                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
//                }
//                    multipartFormData.append(audioFilePath, withName: "audio.m4a")
//
//            }, to: url, headers: headers)
//                .responseJSON { response in
//                    switch response.result {
//                    case .success:
//                        do{
//                            let json = try JSON(data: response.data!)
//                            print(json)
//
//                            // Parse an Array
//                            let statusJson = json["status"].string
//
//                            if statusJson == "success" {
//                                completion("success")
//                            }
//                            else
//                            { completion(jsonErrorMsg!) }
//
//                        }   catch {
//                                print(error.localizedDescription)
//                        }
//
//                    case .failure(let encodingError):
//                        print("error:\(encodingError)")
//                    }
//                }
//    }
//
    
 
    func getYouroTokens(email: String,password: String, handler: @escaping (Swift.Result<YouroTokenModel, NSError>) -> Void) {
        let params : Parameters = [
            "email" : email,
            "password" : password,

        ]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getYuroToken)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.youroToken = try JSONDecoder().decode(YouroTokenModel.self, from: data as! Data)

                    handler(.success(self.youroToken!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
//    
//    func getYouroTextEmotion(text: String,recorded_at: String,language_code: String,header: String, handler: @escaping (Swift.Result<YouroTextEmotionModel, NSError>) -> Void) {
//        let params : Parameters = [
//            "text" : "Happy",
//            "recorded_at" : "2020-11-03T19:38:27+0300",
//            "language_code" : "en-US"
//        ]
//        let Auth_header = [ "Authorization" :  "Bearer \(header)" ]
//
//        let operation = NetworkOperation(method: .post, parameters: params, headers: Auth_header, encoder: JSONEncoding.default, url: APIService.getYuroTextEmotion)
//
//        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
//            switch result {
//            case .success(let data):
//                do {
//                    self.youroTextEmotion = try JSONDecoder().decode(YouroTextEmotionModel.self, from: data as! Data)
//
//                    handler(.success(self.youroTextEmotion!))
//                } catch {
//                    handler(.failure(error as NSError))
//                }
//            case .failure(let error):
//                handler(.failure(error as NSError))
//            }
//        }
//    }
    
 
    //MARK: Coupon List Update API's

    func updateCouponData(claimedCoupon: [String: Any], handler: @escaping (Swift.Result<CouponPurchasedModel, NSError>) -> Void) {
       
        let operation = NetworkOperation(method: .post, parameters: claimedCoupon, headers: headers, encoder: JSONEncoding.default, url: APIService.couponPurchase)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    let purchased = try JSONDecoder().decode(CouponPurchasedModel.self, from: data as! Data)

                    handler(.success(purchased))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    
    func getPurchasedCouponList(handler: @escaping (Swift.Result<CouponPurchasedListModel, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.couponPurchaseList)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.couponPurchasedList = try JSONDecoder().decode(CouponPurchasedListModel.self, from: data as! Data)

                    handler(.success(self.couponPurchasedList!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }

        }
    }
    
    func getPurchasedClaimedCouponList(handler: @escaping (Swift.Result<PurchasedCouponClaimedList, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.couponPurchaseClaimedList)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.couponPurchasedClaimedList = try JSONDecoder().decode(PurchasedCouponClaimedList.self, from: data as! Data)

                    handler(.success(self.couponPurchasedClaimedList!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }

        }
    }
    //MARK: Coupon List Update API's

    func claimPurchaseCoupon(orderId: String, handler: @escaping (Swift.Result<PurchasedCouponClaimed, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "orderId": orderId]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.couponClaim)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.couponPurchasedList?.data.removeAll(where: {$0.orderID == orderId})

                    let claimedPurchased = try JSONDecoder().decode(PurchasedCouponClaimed.self, from: data as! Data)

                    handler(.success(claimedPurchased))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
}
