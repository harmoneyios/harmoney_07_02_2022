//
//  PersonalFitnessManager.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/1/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

protocol PersonalFitnessManagerNotifier: class {
    func didInvite()
}

class PersonalFitnessManager {
    static let sharedInstance = PersonalFitnessManager()
    var personalChallengeList = [PersonalChallenge]()
    var notificationPersonalChallengeList = [PersonalChallenge]()
    var customRewardList = [PersonalChallenge]()
    var inviteList = [InviteData]()
    var notifier: [PersonalFitnessManagerNotifier]?
    var currentProgress: Float = 0.0
    var rewardsNewModel = [RewardsData]()
    var challengeCheckStatus = ChallengeStatusList?.self
    var checkActiveChallenge = false
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    private init() {}
    
    //MARK: Public methods
    
    public func challengeCanStart(challenge: PersonalChallenge?) -> Bool {
        guard
            challenge?.data.isStarted == .stopped,
            challenge?.data.isCompleted == 0,
            (challenge?.data.challengeAcceptType == .accept || challenge?.data.challengeAcceptType == .padding) else {
                return false
        }
        
        return true
    }
    
    public func getActiveChallenge() -> PersonalChallenge? {
//        guard let activityType = PedometerManager.sharedInstance.currentActivity else { return nil}
        let value =  personalChallengeList.contains(where: {$0.data.challengeId == bikechallengeId})
        if value != true {
            bikechallengeId = ""
        }
        
        let startedChallenge = personalChallengeList.first(where: {$0.data.isStarted == .started && $0.data.isCompleted == 0})
        
        return startedChallenge

//        switch activityType {
//
//        case .walking:
//            return startedChallenge?.data.challengeType == .walk ? startedChallenge : nil
//        case .running:
//            return startedChallenge?.data.challengeType == .run ? startedChallenge : nil
//        case .cycling:
//            return startedChallenge?.data.challengeType == .bike ? startedChallenge : nil
//        }
    }
    
    public func getStartedChallenge() -> PersonalChallenge? {
        return personalChallengeList.first(where: {$0.data.isStarted == .started})
    }
    
    public func getCurrentChallenge() -> PersonalChallenge? {
        return personalChallengeList.last
    }
    
    public func updateActiveChallengeProgress(progress: Float, handler: @escaping (Swift.Result<PersonalChallenge, NSError>) -> Void) {
    
        if progress >= 0 {
        if var activeChallenge = getActiveChallenge(), let index = getIndexForPersonalChallenge(personalChallenge: activeChallenge) {
            
            /*
            if activeChallenge.data.challengeProgress ?? 0 >= Float(activeChallenge.data.challengeTask){
                self.personalChallengeList[index] = activeChallenge
                handler(.success(activeChallenge))
                return
            }
            */
//            var tmpProgress : Float = 0.0
//
//            tmpProgress = progress

//            switch activeChallenge.data.challengeType {
//            case .walk:
//                tmpProgress = progress
//            case .run:
//                tmpProgress = progress / 1609 //miles
//            case .swim:
//                break
//            case .bike:
//                tmpProgress = progress / 1609 //miles
//                break
//            }
//            let finalProgress = tmpProgress - currentProgress
//            currentProgress = tmpProgress
            
        
            switch activeChallenge.data.challengeType {

            case .walk:
                activeChallenge.data.challengeProgress = progress
                activeChallenge.data.challengeRunStep = progress
            case .run:
//                let stepCount = ((progress * 10) / 160.9) * 0.1
                activeChallenge.data.challengeProgress = progress
                activeChallenge.data.challengeRunStep = progress
            case .swim:
                activeChallenge.data.challengeProgress = progress
                activeChallenge.data.challengeRunStep = progress

                break
            case .bike:
                activeChallenge.data.challengeProgress = progress
                activeChallenge.data.challengeRunStep = progress

                break
            }
           
            print("setup progress level-->",(progress))
            
//            self.personalChallengeList[index] = activeChallenge
//            handler(.success(activeChallenge))
            //TODO: - Remove the multiplier before release
            
//            activeChallenge.data.challengeProgress = finalProgress
            //Sabarish
//            activeChallenge.data.challengeProgress = progress * 10


////            activeChallenge.data.challengeProgress = finalProgress
            self.updatePersonalChallenge(personalChallenge: activeChallenge) { (result) in
                switch result {
                case .success(let updatedChallenge):
                    activeChallenge = updatedChallenge
                    self.personalChallengeList[index] = activeChallenge
                    handler(.success(activeChallenge))
                case .failure(let error):
                    handler(.failure(error))
                }
            }
        }
     }
    }
    
    public func registerForNotifier(delegate: PersonalFitnessManagerNotifier) {
        if notifier == nil {
            notifier = [PersonalFitnessManagerNotifier]()
        }
        
        notifier?.append(delegate)
    }
    
    public func notifyDelegatesForInvite() {
        guard let delegates = notifier else { return }
        
        for delegate in delegates {
            delegate.didInvite()
        }
    }
    
    public func getPersonalChallenge(atIndex index: Int) -> PersonalChallenge? {
        if index < self.personalChallengeList.count {
            return self.personalChallengeList[index]
        }
        
        return nil
    }
    
    public func getPersonalChallenge(byId id: String) -> PersonalChallenge? {
        return self.personalChallengeList.first(where: {$0.data.challengeId == id})
    }
    
    public func getIndexForPersonalChallenge(personalChallenge: PersonalChallenge?) -> Int? {
        return self.personalChallengeList.firstIndex(where: {$0.data.challengeId == personalChallenge?.data.challengeId})
    }
    
    public func removePersonalChallenge(personalChallenge: PersonalChallenge) {
       return self.personalChallengeList.removeAll(where: {$0.data.challengeId == personalChallenge.data.challengeId})
    }
    public func removePersonalChallengeOnDaily(personalChallengeId: String) {
       return self.personalChallengeList.removeAll(where: {$0.data.challengeId == personalChallengeId})
    }
    
    public func removePersonalChallengeAtIndex(atIndex index: Int) {
       self.personalChallengeList.remove(at: index)
    }
  
    //MARK: PersonalFitness API's
    
    func getDashboardData(handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: JSONEncoding.default, url: APIService.dashboard + "/" + UserDefaultConstants().guid!)

        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
            case .success(let json):
                if  let json = json as? [String: Any] {
                    HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: json)
                    
                    handler(.success(""))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func addPersonalChallenge(personalChallenge: PersonalChallenge, handler: @escaping (Swift.Result<PersonalChallenge, NSError>) -> Void) {
        var memberId:[String] = []
        if let assigntomemberDataList = HarmonySingleton.shared.assigntomemberDataList{
            for objMember in assigntomemberDataList{
                if objMember.isSelectAvathar ?? false {
                    if let fromguid = objMember.fromGuid{
                        memberId.append(fromguid)
                    }
                }
            }
        }
        if HarmonySingleton.shared.assigntomemberDataList?.count == 1{
            memberId.append(HarmonySingleton.shared.assigntomemberDataList?.first?.memberId ?? "")

        }
        var params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "tType" : personalChallenge.data.challengeType.rawValue,
                                   "tCount": personalChallenge.data.challengeTask,
                                   "tTagTo": personalChallenge.data.challengeTagTo ?? "",
                                   "tTagValue": personalChallenge.data.challengeTagValue ?? "",
                                   "tHarmoneyBugs": "",
                                   "tCustomRewards": personalChallenge.data.customReward ?? "",
                                   "tApprovalGuid": personalChallenge.data.aprovedId ?? "",
                                   "tXP": personalChallenge.data.experiance,
                                   "tGem": personalChallenge.data.diamond,
                                   "tHBucks": personalChallenge.data.harmoneyBucks ?? 0,
                                   "tIsApproved": personalChallenge.data.challengeAcceptType?.rawValue ?? 0,
                                   "assignedTo": memberId]
//        if memberId.count > 0 {
            params["tIsAccepted"] = 0
            params["tIsApproved"] = 1
//        }
        if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
            if personalChallenge.data.harmoneyBucks ?? 0 <= 0
            {
                params["tIsAccepted"] = 1
                params["tIsApproved"] = 1
            }
        }
        print("Fitness Data")
        print(params)
        print(APIService.addPersonalChallenge)
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.addPersonalChallenge)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            
            switch result {
            case .success(let data):
                do {
                    
                    let personalChallenge = try JSONDecoder().decode(PersonalChallenge.self, from: data as! Data)
              
                    
                    self.personalChallengeList.append(personalChallenge)
                    
                    handler(.success(personalChallenge))
                } catch {
                    
                    handler((.failure(error as NSError)))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func getPersonalChallenges(handler: @escaping (Swift.Result<[PersonalChallenge], NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getPersonalChallenges)
        print(params)
        print(operation.url)
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    if self.personalChallengeList.count > 0 {
                        self.personalChallengeList.removeAll()
                    }
                    let dataList = try JSONDecoder().decode(PersonalChallengeDataList.self, from: data as! Data)
                    
                    for data in dataList.data {
                        self.personalChallengeList.append(PersonalChallenge(data: data))
                    }
                    
                    handler(.success(self.personalChallengeList))
                    
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    func getPersonalChallengesHbot(handler: @escaping (Swift.Result<[PersonalChallenge], NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "date": DateUtils.format(date: Date(), in: .yearMonthDay) ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getPersonalChallengesHbot)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    if self.personalChallengeList.count > 0 {
                        self.personalChallengeList.removeAll()
                    }
                    let dataList = try JSONDecoder().decode(PersonalChallengeDataList.self, from: data as! Data)
                    
                    for data in dataList.data {
                        self.personalChallengeList.append(PersonalChallenge(data: data))
                    }
                    
                    handler(.success(self.personalChallengeList))
                    
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    func deletePersonalChallenge(personalChallenge: PersonalChallenge, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                    "tId": personalChallenge.data.challengeId]
           
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.deletePersonalChallenge)
           
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success( _):
                self.removePersonalChallenge(personalChallenge: personalChallenge)

                handler(.success("Challenge removed"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
//    
    func deletePersonalChallengeOnDaily(personalChallengeId: String, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                    "tId":personalChallengeId]
           
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.deletePersonalChallenge)
           
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success( _):
                self.removePersonalChallengeOnDaily(personalChallengeId: personalChallengeId)

                handler(.success("Challenge removed"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func changePersonalChallengeStatus(challenge: PersonalChallenge?, status: ChallengeStatus, handler: @escaping (Swift.Result<PersonalChallenge, NSError>) -> Void) {
        
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "tId": challenge?.data.challengeId ?? "",
                                   "status": status.rawValue]
        
        let operation = NetworkOperation(method: .put, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.changePersonalChallengeStatus)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(_):
                if var finalPersonalChallenge = self.getPersonalChallenge(byId: challenge?.data.challengeId ?? ""), let index = self.getIndexForPersonalChallenge(personalChallenge: challenge) {
                    
                    finalPersonalChallenge.data.isStarted = status
                    self.personalChallengeList[index] = finalPersonalChallenge
                    handler(.success(finalPersonalChallenge))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func getPersonalChallenge(taskId: String, requestUserGuid: String, handler: @escaping (Swift.Result<PersonalChallenge, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "type": 2,
                                   "requestUserGuid": requestUserGuid,
                                   "taskId": taskId]
           
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getPersonalChallenge)
           
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    let personalChallengeDataList = try JSONDecoder().decode(PersonalChallengeDataList.self, from: data as! Data)
                    
                    if let data = personalChallengeDataList.data.first {
                        let personalChallenge = PersonalChallenge(data: data)
                        
                        handler(.success(personalChallenge))
                    }
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func acceptPersonalChallenge(taskId: String, requestUserGuid: String, harmoneyBucks: Int, customReward: String, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "type": 2,
                                   "requestUserGuid": requestUserGuid,
                                   "taskId": taskId,
                                   "tHarmoneyBucks": harmoneyBucks,
                                   "tCustomRewards": customReward,
                                   "taskCompletionApproval": 1]
   
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.acceptPersonalChallenge)
           
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success( _):
                handler(.success("Success"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func rejectPersonalChallenge(taskId: String, requestUserGuid: String, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "type": 2,
                                   "requestUserGuid": requestUserGuid,
                                   "taskId": taskId]
           
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.rejectPersonalChallenge)
           
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success( _):
                handler(.success("Success"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func updatePersonalChallenge(personalChallenge: PersonalChallenge, handler: @escaping (Swift.Result<PersonalChallenge, NSError>) -> Void) {
        
//        if personalChallenge.data.challengeProgress ?? 0.0 > Float(personalChallenge.data.challengeTask){
//            handler(.success(personalChallenge))
//        }
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "tId": personalChallenge.data.challengeId,
                                   "tCount": personalChallenge.data.challengeProgress ?? 0.0,
                                   "tRunStep": personalChallenge.data.challengeRunStep ?? 0.0
        ]
        
        print("api progress -->",personalChallenge.data.challengeProgress ?? 0.0)
        print("Update Fitness challenge")
        print(params)
        print(APIService.updatePersonalChallenge)
        let operation = NetworkOperation(method: .put, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.updatePersonalChallenge)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    let updatedPersonalChallenge = try JSONDecoder().decode(UpdatedPersonalChallenge.self, from: data as! Data)
                    
                    if var finalPersonalChallenge = self.getPersonalChallenge(byId: personalChallenge.data.challengeId), let index = self.getIndexForPersonalChallenge(personalChallenge: personalChallenge) {
                        
                        print("final progress-->",updatedPersonalChallenge.data.challengeProgress as Any)
                        finalPersonalChallenge.data.challengeProgress = updatedPersonalChallenge.data.challengeProgress
                        finalPersonalChallenge.data.challengeRunStep = updatedPersonalChallenge.data.challengeRunStep
                        finalPersonalChallenge.data.isCompleted = updatedPersonalChallenge.data.isCompleted
                        self.personalChallengeList[index] = finalPersonalChallenge
                        
                        handler(.success(finalPersonalChallenge))
                    }
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    //API Modified By INQ
    func claimPersonalChallengeReward(personalChallenge: RewardsData, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "taskId": personalChallenge.taskID ?? "",
                                   "taskType":personalChallenge.taskType ?? 0]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.claimPersonalChallengeReward)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success( _):
                self.rewardsNewModel.removeAll(where: {$0.taskID == personalChallenge.taskID })
                        
                handler(.success("Challenge removed"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func completePersonalChallenge(personalChallenge: PersonalChallenge, userLevel: Int, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "tId": personalChallenge.data.challengeId,
                                   "tShow": personalChallenge.data.show ?? 0.0,
                                   "level": userLevel]
        
        print("Complete Fitness challenge")
        print(params)
        print(APIService.completePersonalChallenge)
        
        let operation = NetworkOperation(method: .put, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.completePersonalChallenge)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success( _):
                    self.removePersonalChallenge(personalChallenge: personalChallenge)
    
                handler(.success("Challenge removed"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    
    
    //API Modified By INQ
    func getAllCustomReward(handler: @escaping (Swift.Result<[RewardsData], NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getAllCustomReward)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    let dataList = try JSONDecoder().decode(RewardsModel.self, from: data as! Data)
                    
//                    var currentRewardList = [PersonalChallenge]()
//
//                    for data in dataList.data {
//                        currentRewardList.append(PersonalChallenge(data: data))
//                    }
                    
                    self.rewardsNewModel  = dataList.data
                    
                    handler(.success(self.rewardsNewModel))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
        
    //MARK: Invite API's
    func getInvites(handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: JSONEncoding.default, url: APIService.getInvites + UserDefaultConstants().guid!)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success(let data):
                   do {
                    let dataList = try JSONDecoder().decode(InviteDataList.self, from: data as! Data)
                    
                    if dataList.data.count > 0 {
                        let invite = dataList.data.last
                        
                        HarmonySingleton.shared.dashBoardData.data?.invite = invite
                    }
                       
                    handler(.success("Success"))
                   } catch {
                       handler(.failure(error as NSError))
                   }
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    func transferFitnessAmount(taskId: String, type: Int, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "type": type,
                                   "taskId": taskId]
           
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.acceptAmountTransferPersonalChallenge)
           
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success( _):
                handler(.success("Success"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }

    func assingToList(handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: JSONEncoding.default, url: APIService.getAssingtoList + UserDefaultConstants().guid!)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success(let data):
                   do {
                    let dataList = try JSONDecoder().decode(AssignMemberDataList.self, from: data as! Data)
                    
                    if dataList.data.count > 0 {
                        let assigntomemberData = dataList.data
                        
                        HarmonySingleton.shared.assigntomemberDataList = assigntomemberData
                    }
                       
                    handler(.success("Success"))
                   } catch {
                       handler(.failure(error as NSError))
                   }
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    func assingToListforSubAccount(handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: JSONEncoding.default, url: APIService.getAssingtoList + UserDefaultConstants().guid!)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success(let data):
                   do {
                    let dataList = try JSONDecoder().decode(AssignMemberDataList.self, from: data as! Data)
                    
                    if dataList.data.count > 0 {
                        var assigntomemberData = dataList.data
                        assigntomemberData.remove(at: 0)
                        HarmonySingleton.shared.assigntomemberDataListForSubAccount = assigntomemberData
                    }
                       
                    handler(.success("Success"))
                   } catch {
                       handler(.failure(error as NSError))
                   }
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    func inviteMember(inviteData: InviteData,inviteAgain:Bool, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().guid ?? "" , forKey: "guid")
        data.updateValue(getEncryptedString(plainText: inviteData.memberPhoto ?? "") , forKey: "memberPhoto")
        data.updateValue(getEncryptedString(plainText: inviteData.memberName ?? "") , forKey: "memberName")
        data.updateValue(getEncryptedString(plainText: inviteData.memberMobile ?? "") , forKey: "memberMobile")
        data.updateValue(getEncryptedString(plainText: inviteData.memberRelation ?? "") , forKey: "memberRelation")
        data.updateValue(getEncryptedString(plainText: inviteData.memberEmail ?? "") , forKey: "memberEmail")
//        data.updateValue(getEncryptedString(plainText: "") , forKey: "memberGender")

//        data.updateValue(getEncryptedString(plainText: "") , forKey: "memberzip")
//        data.updateValue(getEncryptedString(plainText: "") , forKey: "memberDOB")
//        data.updateValue(getEncryptedString(plainText: "") , forKey: "sendHbucksToUser")

//                                      "approveUserCreateChallenge": false,
//                                       "approveUserBuy": false,
//                                      "approveUserShareCharity": false,
        if inviteAgain {
            data.updateValue(getEncryptedString(plainText: inviteData.memberId) , forKey: "memberId")
        }
        let urlF = inviteAgain ? APIService.inviteAgaian : APIService.inviteMember
        let method : HTTPMethod = inviteAgain ? HTTPMethod.put : HTTPMethod.post
           let operation = NetworkOperation(method: method, parameters: data, headers: headers, encoder: JSONEncoding.default, url: urlF)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success( _):
                    HarmonySingleton.shared.dashBoardData.data?.invite = inviteData
                    
                    self.notifyDelegatesForInvite()
                
                    handler(.success("Success"))
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    
    func checkChallengeStatus(challengeCheckdata:ChallengeStatusList, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
            
            let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                       "tId": challengeCheckdata.taskId ?? "",
                                       "todayDate": DateUtils.format(date: Date(), in: .yearMonthDay) ?? ""]
            
            let operation = NetworkOperation(method: .put, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.checkChallengeStatus)
            
            BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
                switch result {
                case .success(let data):
                do {
                    let dataList = try JSONDecoder().decode(ChallengeStatusData.self, from: data as! Data)
                    
                    let challengeData = dataList.response
                    HarmonySingleton.shared.checkChallengeStatus = challengeData
                    handler(.success("Success"))
                } catch {
                    handler(.failure(error as NSError))
                }
                
                case .failure(let error):
                    handler(.failure(error as NSError))
                }
            }
        }
    
    func changePrimaryMember(inviteData: InviteData, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                      "memberId": inviteData.memberId ?? ""]
       
        let operation = NetworkOperation(method: .put, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.changePrimary)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success( _):
                
                    handler(.success("Success"))
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    
    func acceptInvite(invitedRequest: InvitedRequest, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
           let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                      "memberId": invitedRequest.memberId ?? "",
                                      "requestUserGuid": invitedRequest.requestUserGuid ?? ""]
           
           let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.acceptInvite)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success( _):
                    handler(.success("Success"))
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    
    func rejectInvite(invitedRequest: InvitedRequest, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
           let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                      "memberId": invitedRequest.memberId ?? "",
                                      "requestUserGuid": invitedRequest.requestUserGuid ?? ""]
           
           let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.rejectInvite)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success( _):
                    handler(.success("Success"))
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    
    func acceptInviteFromFamily(memberId: String, FromUserGuid: String, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
           let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                      "memberId": memberId,
                                      "requestUserGuid": FromUserGuid]
           
           let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.acceptInvite)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success( _):
                    handler(.success("Success"))
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    
    func rejectInviteFromFamily(memberId: String, FromUserGuid: String, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
           let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                      "memberId": memberId,
                                      "requestUserGuid": FromUserGuid]
           
           let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.rejectInvite)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success( _):
                    handler(.success("Success"))
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    
    func cancelInviteMember(memberID: String, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
           let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                      "memberId": memberID]
           
           let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.cancelInviteMember)
           
           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
               switch result {
               case .success( _):
                    handler(.success("Success"))
    
                   break
               case .failure(let error):
                   handler(.failure(error as NSError))
                   
                   break
               }
           }
       }
    
}
