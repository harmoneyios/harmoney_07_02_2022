////
////  PedometerManager.swift
////  Harmoney
////
////  Created by Csongor Korosi on 5/21/20.
////  Copyright © 2020 harmoney. All rights reserved.
////
//
//import Foundation
//import CoreMotion
//
//enum MotionActivityType: String {
//    case walking
//    case running
//    case cycling
//}
//
//class PedometerManager {
//    static let sharedInstance = PedometerManager()
//    let pedometer: CMPedometer
//    let motionManager: CMMotionActivityManager
//    var completedSteps = 0
//    var currentActivity: MotionActivityType?
//
//    var isActivityAvailable: Bool {
//        return CMMotionActivityManager.isActivityAvailable()
//    }
//
//    var activityAuthorizationSatus: CMAuthorizationStatus {
//        return CMMotionActivityManager.authorizationStatus()
//    }
//
//    /// Returns a Boolean value indicating whether step counting is available on the current device.
//    var isStepCountingAvailable: Bool {
//        return CMPedometer.isStepCountingAvailable()
//    }
//
//    /// Returns a Boolean value indicating whether distance estimation is available on the current device.
//    var isDistanceAvailable: Bool {
//        return CMPedometer.isDistanceAvailable()
//    }
//
//    /// Returns a Boolean value indicating whether pace information is available on the current device.
//    var isPaceAvailable: Bool {
//        return CMPedometer.isPaceAvailable()
//    }
//
//    /// Returns a Boolean value indicating whether pedometer events are available on the current device.
//    var isPedometerEventTrackingAvailable: Bool {
//        return CMPedometer.isPedometerEventTrackingAvailable()
//    }
//    /// Returns a value indicating whether the app is authorized to gather pedometer data.
//    var authorizationStatus: CMAuthorizationStatus {
//        return CMPedometer.authorizationStatus()
//    }
//
//    //MARK: - Lifecycle methods
//
//    private init() {
//        pedometer = CMPedometer()
//        motionManager = CMMotionActivityManager()
//    }
//
//    //MARK: - Public methods
//
//    func startMotionActivityUpdates(handler: @escaping (MotionActivityType?, Bool) -> Void) {
//        guard isActivityAvailable, activityAuthorizationSatus == .authorized else {
//            currentActivity = nil
//            return
//        }
//        motionManager.startActivityUpdates(to: .main) { (activity) in
////            print("Motion Activity Stationary: \(activity?.stationary ?? false)")
////            print("Motion Activity Walking: \(activity?.walking ?? false)")
////            print("Motion Activity Running: \(activity?.running ?? false)")
////            print("Motion Activity Cycling: \(activity?.cycling ?? false)")
////
//            let tmpActivity = self.currentActivity
//
//            if activity?.stationary ?? false {
//                self.currentActivity = .walking
//            } else if activity?.walking ?? false {
//                self.currentActivity = .walking
//            } else if activity?.running ?? false {
//                self.currentActivity = .running
//            } else if activity?.cycling ?? false {
//                self.currentActivity = .cycling
//            } else {
//                self.currentActivity = .walking
//            }
//
//            handler(self.currentActivity, tmpActivity != self.currentActivity)
//        }
//    }
//
//    /// Starts the delivery of recent pedestrian-related data to your app.
//    func startUpdates(startDate: Date, handler: @escaping CMPedometerHandler) {
//        guard
//            isStepCountingAvailable,
//            authorizationStatus == .authorized else { return }
//
//        PersonalFitnessManager.sharedInstance.currentProgress = 0.0
//        DailyChallengeManager.sharedInstance.currentProgress = 0
//
//        pedometer.startUpdates(from: startDate) { (data, error) in
//            guard let pedometerData = data else { return }
//
//            print("Pedometer data - start date: \(pedometerData.startDate)")
//            print("Pedometer data - end date: \(pedometerData.endDate)")
//            print("Pedometer data - number of steps: \(pedometerData.numberOfSteps)")
//            print("Pedometer data - distance: \(pedometerData.distance ?? -1)")
//            self.completedSteps = Int(truncating: pedometerData.numberOfSteps)
//            DispatchQueue.main.async {
//                 handler(data, error)
//            }
//        }
//    }
//
//    /// Stops the delivery of recent pedestrian data updates to your app.
//    func stopUpdates() {
//        PersonalFitnessManager.sharedInstance.currentProgress = 0.0
//        DailyChallengeManager.sharedInstance.currentProgress = 0
//        pedometer.stopUpdates()
//    }
//
//    /// Starts the delivery of pedometer events to your app.
//    func startEventUpdates() {
//        pedometer.startEventUpdates { (event, error) in
//            print(event as Any)
//        }
//    }
//
//    /// Stops the delivery of pedometer events to your app.
//    func stopEventUpdates() {
//        pedometer.stopEventUpdates()
//    }
//
//    /// Retrieves the data about the steps count between the specified start and end dates.
//    func getStepCount(startDate: Date, endDate: Date) {
//        guard isStepCountingAvailable else { return }
//
//        pedometer.queryPedometerData(from: startDate, to: endDate) { (data, error) in
//            print(data as Any)
//        }
//    }
//
//     // Retrieves the data about the distance between the specified start and end dates.
//    func getDistance(startDate: Date, endDate: Date) {
//        guard isDistanceAvailable else { return }
//
//        pedometer.queryPedometerData(from: startDate, to: endDate) { (data, error) in
//            print(data as Any)
//        }
//    }
//}
