//
//  VouchersManager.swift
//  Harmoney
//
//  Created by Csongor Korosi on 8/21/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import Alamofire

class VouchersManager {
    static let sharedInstance = VouchersManager()
    var vouchers: Voucher?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    private init() {}
    
//MARK: Coupon API's
    
    func getVouchers(handler: @escaping (Swift.Result<Voucher, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getVouchers)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.vouchers = try JSONDecoder().decode(Voucher.self, from: data as! Data)

                    handler(.success(self.vouchers!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }

        }
    }
    
    func claimVoucher(voucherData: VoucherData, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "vId": voucherData.vID,
                                   "vType": voucherData.vType]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.claimVoucher)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(_):
                self.vouchers?.data.removeAll(where: {$0.vID == voucherData.vID})
                
                handler(.success("Success"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
}
