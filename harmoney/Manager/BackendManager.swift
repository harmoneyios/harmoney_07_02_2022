//
//  BackendManager.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/21/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import Alamofire

class BackendManager {
    static let sharedInstance = BackendManager()
    
    private init() {}
    
    func startRequest(operation: NetworkOperation, result: @escaping (Swift.Result<Any, NSError>) -> Void) {
        guard let url = URL(string: operation.url) else {
            result(.failure(NSError.createInvalidURLError()))
        
            return
        }
        
        AF.request(url, method: operation.method, parameters: operation.parameters, encoding: operation.encoder, headers: operation.headers).validate().responseData { (response) in
            switch response.result {
            case .success(let data):
                result(.success(data))
            case .failure(let error):
                let errorMessage = response.data != nil ? String(data: response.data!, encoding: .utf8) : error.localizedDescription
                
                let errorCode = (error as NSError).code
                
                result(.failure(NSError.createDefaultError(message: errorMessage, statusCode: errorCode)))
            }
        }
    }
    
    func startRequestForJSON(operation: NetworkOperation, result: @escaping (Swift.Result<Any, NSError>) -> Void) {
        guard let url = URL(string: operation.url) else {
            result(.failure(NSError.createInvalidURLError()))
        
            return
        }
        
        AF.request(url, method: operation.method, parameters: operation.parameters, encoding: operation.encoder, headers: operation.headers).validate().responseJSON { (response) in
            switch response.result {
            case .success(let json):
                result(.success(json))
            case .failure(let error):
                let errorMessage = response.data != nil ? String(data: response.data!, encoding: .utf8) : error.localizedDescription
                
                let errorCode = (error as NSError).code
                
                result(.failure(NSError.createDefaultError(message: errorMessage, statusCode: errorCode)))
            }
        }
    }
}
