//
//  LevelsManager.swift
//  Harmoney
//
//  Created by Csongor Korosi on 8/5/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import Alamofire

class LevelsManager {
    static let sharedInstance = LevelsManager()
    var levelsModel: LevelsModel?
    var levelRewardClaimed: Bool = false
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    private init() {}
    
//MARK: Public Methods
    
    public func getLevelInfo(byLevel level: Int) -> LevelInfo? {
        return self.levelsModel?.data.levelInfo.first(where: {$0.level == level})
    }
    
    public func getIndexForLevelInfo(levelInfo: LevelInfo?) -> Int? {
        return self.levelsModel?.data.levelInfo.firstIndex(where: {$0.level == levelInfo?.level})
    }
    
//MARK: Coupon API's
    
    func getLevels(handler: @escaping (Swift.Result<LevelsModel, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getLevels)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.levelsModel = try JSONDecoder().decode(LevelsModel.self, from: data as! Data)

                    handler(.success(self.levelsModel!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }

        }
    }
    
    func addRewardForLevel(levelReward: String, levelInfo: LevelInfo, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "level": "\(levelInfo.level)" ,
                                   "rewardName": levelReward]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.addRewardForLevel)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(_):
                handler(.success("Success"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func claimLevelReward(levelInfo: LevelInfo, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        
        var giftCard = [String: Any]()
        
        if levelInfo.giftCard != nil {
            giftCard = ["giftCardId": levelInfo.giftCard?.giftCardID ,
                        "giftCardName": levelInfo.giftCard?.giftCardName,
                        "giftCardImageUrl": levelInfo.giftCard?.giftCardImageURL]
        }
        
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "level": "\(levelInfo.level)",
                                   "xp": levelInfo.levelXP,
                                   "gem": levelInfo.jem,
                                   "rewardId": levelInfo.reward?.rewardID ?? 0,
                                   "giftCard": levelInfo.giftCard != nil ? giftCard : NSNull()]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.claimLevelReward)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(_):
                if var finalLevelInfo = self.getLevelInfo(byLevel: levelInfo.level ?? 0), let index = self.getIndexForLevelInfo(levelInfo: levelInfo) {
                    
                    finalLevelInfo.levelClaimStatus = true
                    self.levelsModel?.data.levelInfo[index] = finalLevelInfo
                    handler(.success(finalLevelInfo))
                }
                
//                self.levelRewardClaimed = true
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func acceptLevelReward(invitedRequest: InvitedRequest, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "level": invitedRequest.level ?? 0,
                                   "requestUserGuid": invitedRequest.requestUserGuid ?? "",
                                   "rewardId": invitedRequest.rewardId ?? 0,
                                   "rewardName": invitedRequest.rewardName ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.acceptLevelReward)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(_):
                handler(.success("Success"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func rejectLevelReward(invitedRequest: InvitedRequest, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "level": invitedRequest.level ?? 0,
                                   "requestUserGuid": invitedRequest.requestUserGuid ?? "",
                                   "rewardId": invitedRequest.rewardId ?? "",
                                   "rewardName": invitedRequest.rewardName ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.rejectLevelReward)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(_):
                handler(.success("Success"))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
}
