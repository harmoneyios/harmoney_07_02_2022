//
//  HealthKitManager.swift
//  Harmoney
//
//  Created by Norbert Korosi on 2/06/2020.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import UIKit
import HealthKit

enum HealthError: Error {
    case deviceNotSupported
    case serviceNotAvailable
    case permissionDenied(type: HealthSampleType)
    case accessUnAuthorized(type: HealthSampleType)
    case unknown(message: String)
}

enum HealthSampleType {
    case walk
    case run
    case swim
    case bike
    case workout
    
    var sampleType: HKSampleType? {
        switch self {
            case .walk:
                return HKObjectType.quantityType(forIdentifier: .stepCount)
            case .run:
                return HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)
            case .swim:
                return HKObjectType.quantityType(forIdentifier: .distanceSwimming)
            case .bike:
                return HKObjectType.quantityType(forIdentifier: .distanceCycling)
            case .workout:
            return HKObjectType.workoutType()
        }
    }
    
    var unit: HKUnit {
        switch self {
            case .walk:
                return .count()
            case .run:
                return .mile()
            case .swim:
                return .meter()
            case .bike:
                return .mile()
        case .workout:
            return .mile()
        }
    }
}

class HealthKitManager {
    static let sharedInstance = HealthKitManager()

    //MARK: - Properties
    
    private var store: HKHealthStore
    private var sampleTypeList: Set<HKSampleType>?
    var isHealthDataAvailable: Bool {
        return HKHealthStore.isHealthDataAvailable()
    }
    
    //MARK: - Lifecycle methods
    
    private init() {
        store = HKHealthStore()
        sampleTypeList = setupSampleTypeList()
    }
    
    //MARK: - Private methods

    private func setupSampleTypeList() -> Set<HKSampleType>? {
        guard
            let walkSampleType = HealthSampleType.walk.sampleType,
            let runSampleType = HealthSampleType.run.sampleType,
            let swimSampleType = HealthSampleType.swim.sampleType,
            let bikeSampleType = HealthSampleType.bike.sampleType,
            let workout = HealthSampleType.workout.sampleType else {
                return nil
        }
        
        return Set<HKSampleType>(arrayLiteral: walkSampleType, runSampleType, swimSampleType, bikeSampleType,workout)
    }
    
    //MARK: - Public methods
    
    func requestAccess(onCompletion: @escaping(Bool, HealthError?) -> Void) {
        guard let sampleTypes = sampleTypeList else {
            onCompletion(false, .serviceNotAvailable)
            return
        }
        
        DispatchQueue.main.async {
            self.store.requestAuthorization(toShare: sampleTypes, read: sampleTypes) { (success, error) in
                guard error == nil , success else {
                    onCompletion(false, HealthError.unknown(message: error?.localizedDescription ?? "Unable to process your request"))
                    return
                }

                for type in sampleTypes {
                    self.store.enableBackgroundDelivery(for: type, frequency: .immediate) { (_, _) in }
                }
                onCompletion(success, nil)
            }
        }
    }
    
    func isAccessGranted(sampleType: HealthSampleType)  -> Bool {
        guard
            isHealthDataAvailable,
            let sampleType = sampleType.sampleType else {
                return false
        }
        
        switch store.authorizationStatus(for: sampleType) {
            case .notDetermined,
                 .sharingDenied:
                return false
            case .sharingAuthorized:
                return true
            @unknown default:
                return false
        }
    }
    
    func readDampleData(sampleType: HealthSampleType, startDate: Date, endDate: Date, completion: @escaping (Double?, Error?) -> Void) {
        
    }
    
    func saveSampleData(sampleType: HealthSampleType) {
        
    }
}
