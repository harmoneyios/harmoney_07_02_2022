//
//  DailyChallengeManager.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/21/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class DailyChallengeManager {
    static let sharedInstance = DailyChallengeManager()
    
    var completedProgress = 0
    var dailyChallenges = [DailyChallenge]()
    var onboardingChallenges = [ChallengeData]()
    
    
    var onboardingChallengesFinished: Bool {
//        return onboardingChallenges.count == 3 && onboardingChallenges.last?.cStatus == 1 && HarmonySingleton.shared.dashBoardData?.data?.challengeShareStatus == 1 && HarmonySingleton.shared.dashBoardData?.data?.collectMysteryBox == 1
        return HarmonySingleton.shared.dashBoardData?.data?.onboardingChallengeCompletionStatus ?? false
    }
    
    var showSocialMediaShare: Bool {
        return onboardingChallenges.count == 3 && onboardingChallenges.last?.cStatus == 1 && HarmonySingleton.shared.dashBoardData?.data?.challengeShareStatus == 0
    }
    
    var showMisteryBoxScreen: Bool {
        return HarmonySingleton.shared.dashBoardData?.data?.challengeShareStatus == 1 && HarmonySingleton.shared.dashBoardData?.data?.collectMysteryBox == 0
    }
    
    var currentProgress = 0
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    private init() {}
    
    //MARK: Public methods
    
    public func getStartedOnboardingWalkChallenge() -> ChallengeData? {
        return self.onboardingChallenges.first(where: {$0.cType == "1" && $0.cStatus == 0})
    }
    
    public func getStartedDailyWalkChallenge() -> DailyChallenge?  {
        return self.dailyChallenges.first(where: {$0.challegeTaskType == 1 && $0.userCompletionStatus == .accepted})
    }
    
    public func getActiveOnboardingWalkChallenge() -> ChallengeData? {
//        guard let activityType = PedometerManager.sharedInstance.currentActivity else { return nil}
        
        let startedOnboardingWalkChallenge = getStartedOnboardingWalkChallenge()
        
//        switch activityType {
//            case .walking:
                return startedOnboardingWalkChallenge
//            case .running:
//                return nil
//            case .cycling:
//                   return nil
//        }
    }
    
    public func getActiveDailyWalkChallenge() -> DailyChallenge? {
//        guard let activityType = PedometerManager.sharedInstance.currentActivity else { return nil}
        
        let startedDailyWalkChallenge = getStartedDailyWalkChallenge()
        
//        switch activityType {
//            case .walking:
                return startedDailyWalkChallenge
//            case .running:
//                return nil
//            case .cycling:
//                return nil
//        }
    }
    
    public func geOnboardingChallenge(byId id: String) -> ChallengeData? {
        return self.onboardingChallenges.first(where: {$0.cId == id})
    }
       
    public func getIndexForOnboardingChallenge(onboardingChallenge: ChallengeData?) -> Int? {
        return self.onboardingChallenges.firstIndex(where: {$0.cId == onboardingChallenge?.cId})
    }
       
    public func getDailyChallenge(byId id: String) -> DailyChallenge? {
        return self.dailyChallenges.first(where: {$0.challengeID == id})
    }
    
    public func getIndexForDailyChallenge(dailyChallenge: DailyChallenge?) -> Int? {
        return self.dailyChallenges.firstIndex(where: {$0.challengeID == dailyChallenge?.challengeID})
    }
    
    public func removeDailyChallenge(dailyChallenge: DailyChallenge) {
        return self.dailyChallenges.removeAll(where: {$0.challengeID == dailyChallenge.challengeID})
    }
    
    public func removeDailyChallengeAtIndex(atIndex index: Int) {
       self.dailyChallenges.remove(at: index)
    }
    
    public func updateActiveOnboardingChallengeProgress(progress: Int, handler: @escaping (Swift.Result<ChallengeData, NSError>) -> Void) {
        
        if var activeOnboardingChallenge = getActiveOnboardingWalkChallenge(), let index = getIndexForOnboardingChallenge(onboardingChallenge: activeOnboardingChallenge) {
            
            completedProgress = completedProgress + progress
            let tmpProgress = progress
               
            let finalProgress = completedProgress + (tmpProgress - currentProgress)
            currentProgress = tmpProgress
                    
            activeOnboardingChallenge.cInfo?.completedCount = String(finalProgress)
            
            self.updateOnboardingChallenge(challenge: activeOnboardingChallenge) { (result) in
                switch result {
                    
                case .success(let updatedChallenge):
                    activeOnboardingChallenge = updatedChallenge
                    self.onboardingChallenges[index] = activeOnboardingChallenge
                    handler(.success(activeOnboardingChallenge))
                case .failure(let error):
                    handler(.failure(error))
                }
            }
        }
    }
    
    public func updateActiveDailyChallengeProgress(progress: Int, handler: @escaping (Swift.Result<DailyChallenge, NSError>) -> Void) {
        if var activeDailyChallenge = getActiveDailyWalkChallenge(), let index = getIndexForDailyChallenge(dailyChallenge: activeDailyChallenge) {
            let tmpProgress = progress
            let completedProgress = activeDailyChallenge.taskCurrentValue ?? 0
            
            let maxValue = Int(activeDailyChallenge.taskValue) ?? 0
            let finalProgress = completedProgress + (tmpProgress - currentProgress)
            currentProgress = tmpProgress
               
            //TODO: - Remove the multiplier before release
               
            activeDailyChallenge.taskCurrentValue = Int(finalProgress)
            if activeDailyChallenge.taskCurrentValue ?? 0 >= maxValue {
                activeDailyChallenge.userCompletionStatus = .whatsNext
            }
            self.updateDailyChallenge(dailyChallenge: activeDailyChallenge, status: activeDailyChallenge.userCompletionStatus) { (result) in
                switch result {
                    
                case .success(let updatedDailyChallenge):
                    activeDailyChallenge = updatedDailyChallenge
                    self.dailyChallenges[index] = activeDailyChallenge
                    handler(.success(activeDailyChallenge))
                case .failure(let error):
                    handler(.failure(error))
                }
            }
        }
    }
    
    //MARK: Onboarding Challenges API's
    
    func getDashboardData(handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: URLEncoding.default, url: APIService.dashboard + "/" + UserDefaultConstants().guid!)

        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
            case .success(let json):
                if  let json = json as? [String: Any] {
                    let dashBoardData = Mapper<DashBoardModel>().map(JSON: json)
                    HarmonySingleton.shared.dashBoardData = dashBoardData
                    if let currentChallenges = dashBoardData?.data?.challenges {
                        self.onboardingChallenges = Array(currentChallenges.prefix(3))
                    }
                   
                    handler(.success(""))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func getOnboardingChallenges(handler: @escaping (Swift.Result<[ChallengeData], NSError>) -> Void) {
        
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: JSONEncoding.default, url: APIService.getChallenges + "/" + UserDefaultConstants().guid!)
        
        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
            case .success(let json):
                guard
                    let json = json as? [String: Any],
                    let onboardingChallenge = Mapper<ChallengesObject>().map(JSON: json)?.data else {
                        handler(.failure(NSError()))
                        return
                }
                if onboardingChallenge.cId != nil {
                    if onboardingChallenge.cName != nil{
//                        self.onboardingChallenges.removeAll()
                        if onboardingChallenge.cId != self.onboardingChallenges.first?.cId
                        {
                            self.onboardingChallenges.append(onboardingChallenge)

                        }

                    }
                }
                handler(.success(self.onboardingChallenges))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func acceptOnboardingChallenge(challenge: ChallengeData, handler: @escaping (Swift.Result<ChallengeData, NSError>) -> Void) {

        if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {

            PersonalFitnessManager.sharedInstance.changePersonalChallengeStatus(challenge: startedChallenge, status: .stopped) { (result) in
                switch result {
                    
                case .success(_):
                    let url = APIService.acceptChallenges + "/" + UserDefaultConstants().guid! + "/" + (challenge.cId ?? "")
                    let operation = NetworkOperation(method: .get, parameters: nil, headers: self.headers, encoder: JSONEncoding.default, url: url)
                    
                    BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
                        switch result {
                        case .success(_):
                            var acceptedChallenge = challenge
                            acceptedChallenge.cStatus = 0
                            acceptedChallenge.cInfo?.completedCount = "0"
                            handler(.success(acceptedChallenge))
                        case .failure(let error):
                            handler(.failure(error as NSError))
                        }
                    }
                case .failure(let error):
                     handler(.failure(error as NSError))
                }
            }
        } else {
            let url = APIService.acceptChallenges + "/" + UserDefaultConstants().guid! + "/" + (challenge.cId ?? "")
            let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: JSONEncoding.default, url: url)
            
            BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
                switch result {
                case .success(_):
                    var acceptedChallenge = challenge
                    acceptedChallenge.cStatus = 0
                    acceptedChallenge.cInfo?.completedCount = "0"
                    handler(.success(acceptedChallenge))
                case .failure(let error):
                    
                    handler(.failure(error as NSError))
                }
            }
        }
    }
    
    func updateOnboardingChallenge(challenge: ChallengeData, handler: @escaping (Swift.Result<ChallengeData, NSError>) -> Void) {
        let url = APIService.updateUserChallenge
        
        let challengeInfoParams: Parameters = ["cType": challenge.cType ?? "",
                                               "cCompletedCount": challenge.cInfo?.completedCount ?? "",
                                               "cUnit": challenge.cInfo?.cUnit ?? ""]
        
        let params: Parameters = ["guid": UserDefaultConstants().guid!,
                                  "cId": challenge.cId ?? "",
                                  "cStatus": challenge.cStatus ?? 0,
                                  "cInfo": challengeInfoParams]
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: url)
        print("Calling Onboarding Challange Update \(params)")
        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
            case .success(_):
                print(" Onboarding Challange Updated Successfully")
                //replace with the updated onboarding challenge
                if var finalOnboardingChallenge = self.geOnboardingChallenge(byId: challenge.cId ?? ""), let index = self.getIndexForOnboardingChallenge(onboardingChallenge: challenge) {
                    finalOnboardingChallenge = challenge
                    self.onboardingChallenges[index] = finalOnboardingChallenge
                    handler(.success(finalOnboardingChallenge))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func completeOnboardingChallenge(challenge: ChallengeData, handler: @escaping (Swift.Result<ChallengeData, NSError>) -> Void) {
        
    }
    
    //MARK: DailyChallenge API's
    
    func getDailyChallenges(handler: @escaping (Swift.Result<[DailyChallenge], NSError>) -> Void) {
        
      
        if HarmonySingleton.shared.dashBoardData.data?.challenges?.count == 0{
            let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                       "today": DateUtils.format(date: Date(), in: .yearMonthDay) ?? "","ChallengeType" : 1]
            print(params)
            print(APIService.getDailyChallenges)
            let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getDailyChallenges)
            
            BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
                switch result {
                case .success(let data):
                    do {
                        let dailyChallengeList = try JSONDecoder().decode(DailyChallengeList.self, from: data as! Data)
                        let notCompletedDailyChallenges = dailyChallengeList.data.filter({$0.userCompletionStatus != .completed})
                        self.dailyChallenges .removeAll()
                        self.dailyChallenges.append(contentsOf: notCompletedDailyChallenges)
                        
                        handler(.success(self.dailyChallenges))
                    } catch {
                        handler(.failure(error as NSError))
                    }
                case .failure(let error):
                    handler(.failure(error as NSError))
                }
            }

        }
    }
    
    func getTaskCount(handler: @escaping (Swift.Result<pendingTaskCount, NSError>) -> Void) {
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "date": DateUtils.format(date: Date(), in: .yearMonthDay) ?? ""]
        
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.getDailyChallenges)
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    let pendingTaskListValue = try JSONDecoder().decode(pendingTaskCount.self, from: data as! Data)
                    let labelMessage = pendingTaskListValue.data.msg
                    
                    
                    handler(.success(pendingTaskListValue))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    func updateDailyChallenge(dailyChallenge: DailyChallenge, status: DailyChallengeStatus, handler: @escaping (Swift.Result<DailyChallenge, NSError>) -> Void) {
        
        if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge(), status == .accepted {
            PersonalFitnessManager.sharedInstance.changePersonalChallengeStatus(challenge: startedChallenge, status: .stopped) { [self] (result) in
                switch result {
                case .success(_):
                         let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                                      "challengeId": dailyChallenge.challengeID,
                                                      "taskCurrentValue": dailyChallenge.taskCurrentValue ?? 0,
                                                      "userCompletionStatus": status.rawValue]
                           
                    let operation = NetworkOperation(method: .post, parameters: params, headers: self.headers, encoder: JSONEncoding.default, url: APIService.updateChallengeStatus)
                           
                           BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
                               switch result {
                               case .success(_):
                                   if var finalDailyChallenge = self.getDailyChallenge(byId: dailyChallenge.challengeID), let index = self.getIndexForDailyChallenge(dailyChallenge: dailyChallenge) {
                                           finalDailyChallenge.userCompletionStatus = status
                                           if finalDailyChallenge.userCompletionStatus == .rewards {
                                               self.removeDailyChallenge(dailyChallenge: finalDailyChallenge)
                                           } else {
                                               self.dailyChallenges[index] = finalDailyChallenge
                                           }
                                       
                                           handler(.success(finalDailyChallenge))
                                   }
                               case .failure(let error):
                                   handler(.failure(error as NSError))
                               }
                           }
                case .failure(let error):
                     handler(.failure(error as NSError))
                }
            }
        } else {
            let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                       "challengeId": dailyChallenge.challengeID,
                                       "taskCurrentValue": dailyChallenge.taskCurrentValue ?? 0,
                                       "userCompletionStatus": status.rawValue]
            
            let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: APIService.updateChallengeStatus)
            
            BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
                switch result {
                case .success(_):
                    if var finalDailyChallenge = self.getDailyChallenge(byId: dailyChallenge.challengeID), let index = self.getIndexForDailyChallenge(dailyChallenge: dailyChallenge) {
                            finalDailyChallenge = dailyChallenge
                            finalDailyChallenge.userCompletionStatus = status
                            
                            if finalDailyChallenge.userCompletionStatus == .rewards {
                                self.removeDailyChallenge(dailyChallenge: finalDailyChallenge)
                            } else {
                                self.dailyChallenges[index] = finalDailyChallenge
                            }
                        
                            handler(.success(finalDailyChallenge))
                    }
                case .failure(let error):
                    handler(.failure(error as NSError))
                }
            }
        }
    }
}
