//
//  LoginManager.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/8/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import Alamofire

class LoginManager {
    static let sharedInstance = LoginManager()
    var loginModel: LoginModel?
    
    private init() {}
    
    //MARK: Login API's
    
    func getOTP(handler: @escaping (Swift.Result<LoginModel, NSError>) -> Void) {
        let operation = NetworkOperation(method: .get, parameters: nil, headers: nil, encoder: JSONEncoding.default, url: APIService.getOtp + "/" + (UserDefaultConstants().guid ?? ""))
        
        BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
            switch result {
            case .success(let data):
                do {
                    self.loginModel = try JSONDecoder().decode(LoginModel.self, from: data as! Data)
                    UserDefaultConstants().guid = self.loginModel?.data.guid

                    handler(.success(self.loginModel!))
                } catch {
                    handler(.failure(error as NSError))
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
}
