//
//  NotificationsManager.swift
//  Harmoney
//
//  Created by Norbert Korosi on 19/06/2020.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class NotificationsManager {
    static let sharedInstance = NotificationsManager()
    var notifications = [NotificationDash]()
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    
    //MARK: - Lifecycle methods
       
    private init() {
        
    }
       
    //MARK: - Public methods
    
    /*
    func getAllNotifications(handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let operation = NetworkOperation(method: .get, parameters: nil, headers: nil, encoder: JSONEncoding.default, url: APIService.dashboard + "/" + UserDefaultConstants().guid!)

        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
            case .success(let json):
                if  let json = json as? [String: Any] {
                    let dashboard = Mapper<DashBoardModel>().map(JSON: json)
                    
                    HarmonySingleton.shared.dashBoardData = dashboard
                    
                    if let notifications = dashboard?.data?.notification {
                        self.notifications = notifications
                        
                        handler(.success(""))
                    }
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    */
    
    func getAllNotifications(handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: JSONEncoding.default, url: APIService.getAllNotifications + "/" + UserDefaultConstants().guid!)

        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
            case .success(let json):
                if  let json = json as? [String: Any] {
                    let notificationModel = Mapper<NotificationModel>().map(JSON: json)
                    
                    
                    if let notifications = notificationModel?.arrNotifications {
                        self.notifications = notifications
                        handler(.success(""))
                    }
                }
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    
    func removeAllNotifications(handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let url = APIService.clearAllNotification + (UserDefaultConstants().guid ?? "")
        
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: URLEncoding.default, url: url)
        
        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
                
            case .success(_):
                self.notifications.removeAll()
                handler(.success(""))
                
            case .failure(let error):
                handler(.failure(error))
            }
        }
    }
    
    func markNotificationsRead(handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let url = APIService.getNotifications + "/" + (UserDefaultConstants().guid ?? "")
        
        let operation = NetworkOperation(method: .get, parameters: nil, headers: headers, encoder: URLEncoding.default, url: url)
        
        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
            case .success(_):
                handler(.success(""))
                
            case .failure(let error):
                handler(.failure(error))
            }
        }
    }
    
    func deleteNotification(notificationObject : NotificationDash, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        let url = APIService.updateNotification
        var dictParams = [String : Any]()
        dictParams["guid"] = UserDefaultConstants().guid ?? ""
        dictParams["notificationId"] = notificationObject.id ?? ""
                        
        let operation = NetworkOperation(method: .post, parameters: dictParams, headers: headers, encoder: URLEncoding.default, url: url)
        
        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
            case .success(_):
                handler(.success(""))
            case .failure(let error):
                handler(.failure(error))
            }
        }
    }
    
    
    func sendDeviceToken(token: String) {
        let url = APIService.updateDeviceToken
        let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                   "deviceToken": token]
        
        print(params)
        let operation = NetworkOperation(method: .post, parameters: params, headers: headers, encoder: JSONEncoding.default, url: url)
        
        BackendManager.sharedInstance.startRequestForJSON(operation: operation) { (result) in
            switch result {
                
            case .success(_):
                break
            case .failure(_):
                break
            }
        }
    }
}
