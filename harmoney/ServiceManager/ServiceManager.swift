//
//  ServiceManager.swift
//  Harmoney
//
//  Created by GOPI K on 18/04/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


class ServiceManager {
    
    //MARK: - LocalVariable
    static let instance = ServiceManager()
    var sessionManager: Alamofire.Session?
    var currentVC: UIViewController?
    
    // MARK:- Execute get url method
    
    func requestToApi<T: BaseMappable>(type: T.Type, viewController : UIViewController, with endPointURL: String, urlMethod: HTTPMethod = .get, params: Parameters? = nil, headers:HTTPHeaders? = nil, showLoader: Bool? = true, encode: ParameterEncoding? = JSONEncoding.default, completion: @escaping(_ result: BaseMappable, _ statusCode: ResponseCode) -> Void) {
           
        let url = APIService.base_URL + endPointURL
           currentVC = viewController
           
           if showLoader == true {
               // start animation
           }
           
           print("url---> \(String(describing: url))")
           print("param---> \(String(describing: params))")
           print("headers---> \(String(describing: headers))")
           
           AF.request(url, method: urlMethod, parameters: params, encoding: encode!, headers: headers).validate().responseObject {
            (response: AFDataResponse<T>) in
               
               // response serialization result
               print("Result: \(response.result)")

               // stop loader animations

//               if let alamoError = response.result.error {
//                   if let err = alamoError as? URLError {
//                       print(err)
//                       self.errorCode(errorCode: err)
//                   }
//
//                   else if response.response?.statusCode == nil{
//                   // completion(response.result.value ?? nil, self.statusCodeForResponseCode(alamoError._code))
//                   }
//                   else {
//                    //completion(response, self.statusCodeForResponseCode((response.response?.statusCode)!))
//                   }
//               }
//               else {
//                completion(response.result.value!, self.statusCodeForResponseCode((response.response?.statusCode)!))
//               }
           }
       }
    
    //MARK: - STATUS CODE
    enum ResponseCode: Int {
        
        // Requested api returns expected response
        case sucess                 = 200
        
        // Email Id not found
        case inVaildEmail           = 201
        
        // Invaild OTP
        case inValidOTP             = 202
        
        // Password Mismatching
        case inValidPwd             = 203
        
        // Token Mismatching
        case tokenMismatch          = 204
        
        // File not found
        case FileUnvailable         = 206
        
        // SR ID not found
        case srIdUnvailable         = 207
        
        // ISR ID not found
        case isrIdUnvailable        = 208
        
        // File name not found
        case fileNameUnvailable     = 209
        
        // Some thing went wrong
        case unExpectedError        = 210
        
        // Invalid password
        case pwdInvalid             = 211
        
        // Previous password already exists
        case previousPwdSame        = 212
        
        case mobileNoAlreadyAvailabel = 302
        
        // User display or read permission missing
        case permissionDisAllow     = 230
        
        // The HTTP request is incomplete or malformed.
        case badRequest             = 400
        
        // Authorization is required to use the service
        case unAuthorization        = 401
        
        // User do not have permission to access the database.
        case forbidden              = 403
        
        // The named database is not running on the server, or the named web service does not exist.
        case notFound               = 404
        
        // The maximum connection idle time was exceeded while receiving the request
        case timeOut                = 408
        
        // An internal error occurred. The request could not be processed.
        case serviceUnavailable     = 500
        
        // No internet
        case noNetwork              = -1
    }
    
    func statusCodeForResponseCode(_ statusCode:Int) -> ResponseCode {
        switch statusCode {
        case 200:
            return .sucess
        case 201:
            return .inVaildEmail
        case 202:
            return .inValidOTP
        case 203:
            return .inValidPwd
        case 204:
            return .tokenMismatch
        case 206:
            return .FileUnvailable
        case 207:
            return .srIdUnvailable
        case 208:
            return .isrIdUnvailable
        case 209:
            return .fileNameUnvailable
        case 210:
            return .unExpectedError
        case 211:
            return .pwdInvalid
        case 212:
            return .previousPwdSame
        case 302:
            return .mobileNoAlreadyAvailabel
        case 230:
            return .permissionDisAllow
        case 400:
            return .badRequest
        case 401:
            return .unAuthorization
        case 403:
            return .forbidden
        case 404:
            return .badRequest
        case 408:
            return .timeOut
        case 500:
            return .serviceUnavailable
        default:
            return .timeOut
        }
    }
    
    func errorCode(errorCode: URLError) {
        
        guard let VC = currentVC else {
            return
        }
        switch errorCode.code {
        case .notConnectedToInternet:
            AlertView.shared.showAlert(view: VC, title: "Connect Failed", description: "The Internet connection appears to be offline")
            break
        default:
            AlertView.shared.showAlert(view: VC, title: "Connect Failed", description: "The Internet connection appears to be offline")
            break
        }
    }
    
    
    
}
