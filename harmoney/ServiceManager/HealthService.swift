//
//  HealthService.swift
//  Harmoney
//
//  Created by Hariharan G on 4/25/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import HealthKit

typealias HealthServiceSampleResultBlock = (_ sampledData: Double, _ error: HealthServiceError?) -> Void
enum HealthServiceError: Error {
    case deviceNotSupported
    case serviceNotAvailable(serviceType: HealthServiceType?)
    case permissionDenied(type: HealthServiceType)
    case accessUnAuthorized(type: HealthServiceType)
    case unknown(message: String)
}

enum HealthServiceType {
    case stepCount
    case distanceWalked
    case distanceCycled
    case distanceSwam
    case calories
    case workout

    var serviceObject: HKSampleType? {
        switch self {
        case .stepCount: return HKObjectType.quantityType(forIdentifier: .stepCount)
        case .distanceCycled: return HKObjectType.quantityType(forIdentifier: .distanceCycling)
        case .distanceWalked: return HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)
        case .distanceSwam: return HKObjectType.quantityType(forIdentifier: .distanceSwimming)
        case .calories: return HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)
        case .workout: return HKObjectType.workoutType()
        }
    }
    
    var unit: HKUnit {
        switch self {
        case .stepCount: return .count()
        case .distanceCycled : return .mile()
        case .distanceWalked: return .mile()
        case .distanceSwam: return .meter()
        case .calories: return .kilocalorie()
        case .workout: return .mile()
       
        }
    }

    var typeName: String {
        switch self {
        case .distanceWalked: return "Run"
        case .distanceCycled: return "Bike"
        case .distanceSwam: return "Swim"
        case .stepCount: return "Walk"
        case .calories: return "Calories"
        case .workout: return "workout"
        }
    }

   /* var minimumValueForAPICall: Double {
        switch self {
        case .distanceWalked, .distanceCycled: return 0.1
        case .distanceSwam: return 5.0
        case .stepCount: return 25.0
        case .calories: return 0.1

        }
    } */
}

class HealthDataSource: Codable {
    var name: String
    var sourceId: String
    
    init(name: String, sourceId: String) {
        self.name = name
        self.sourceId = sourceId
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case sourceId
    }
}

final class HealthService: NSObject {

    private let _store: HKHealthStore

    var isServiceAvailable: Bool {
        return HKHealthStore.isHealthDataAvailable()
    }

    var startDate = Date()
    var updatedValue = Double()

    var activeChallenge: PersonalChallenge? {
        return PersonalFitnessManager.sharedInstance.getActiveChallenge()
    }
    override init() {
        _store = HKHealthStore()
    }

    private func _getServiceObjectList() throws -> Set<HKSampleType> {
        guard isServiceAvailable else {
            throw HealthServiceError.deviceNotSupported
        }
        guard let stepCountServiceObject = HealthServiceType.stepCount.serviceObject else {
            throw HealthServiceError.serviceNotAvailable(serviceType: .stepCount)
        }
        guard let swimmedDistance = HealthServiceType.distanceSwam.serviceObject else {
            throw HealthServiceError.serviceNotAvailable(serviceType: .distanceSwam)
        }
        guard let walkedDistance = HealthServiceType.distanceWalked.serviceObject else {
            throw HealthServiceError.serviceNotAvailable(serviceType: .distanceWalked)
        }
        guard let cycledDistance = HealthServiceType.distanceCycled.serviceObject else {
            throw HealthServiceError.serviceNotAvailable(serviceType: .distanceCycled)
        }
        guard let calories = HealthServiceType.calories.serviceObject else {
            throw HealthServiceError.serviceNotAvailable(serviceType: .calories)
        }
        guard let workout = HealthServiceType.workout.serviceObject else {
            throw HealthServiceError.serviceNotAvailable(serviceType: .workout)
        }
       
        return Set<HKSampleType>(arrayLiteral: stepCountServiceObject, swimmedDistance, walkedDistance, cycledDistance,calories,workout)
    }

    func requestAccess(onCompletion completionBlock:  @escaping(_ success: Bool, _ error: HealthServiceError?) -> Void) {
        do {
            let healthKitTypes = try _getServiceObjectList()
            DispatchQueue.main.async {
                self._store.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { [weak self](success, error) in
                    guard error == nil , success else {
                        completionBlock(success, HealthServiceError.unknown(message: error?.localizedDescription ?? "Unable to process your request"))
                        return
                    }
                    // if not needed be enable background delivery
                    for type in healthKitTypes {
                        self?._store.enableBackgroundDelivery(for: type, frequency: .immediate) { (_, _) in
                            // do nothing
                            
                            print("enableBackgroundDeliveryenableBackgroundDelivery")
                        }
                    }
                    completionBlock(success, nil)
                }
            }
        } catch let error as HealthServiceError {
            completionBlock(false, error)
        } catch {
            completionBlock(false, HealthServiceError.unknown(message: error.localizedDescription))
        }
    }

    func isAccessGranted(forService serviceType: HealthServiceType) throws -> Bool {
        guard isServiceAvailable else {
            throw HealthServiceError.deviceNotSupported
        }
        guard let sampleType = serviceType.serviceObject  else {
            throw HealthServiceError.serviceNotAvailable(serviceType: serviceType)
        }
        switch _store.authorizationStatus(for: sampleType) {
        case .notDetermined: throw HealthServiceError.accessUnAuthorized(type: serviceType)
        case .sharingDenied: return true
        case .sharingAuthorized: return true
        @unknown default: throw HealthServiceError.unknown(message: "Unknown situation has happened...")
        }
    }

    func sampleData(forServiceType serviceType: HealthServiceType, sampleStartTime: Date = Calendar.current.startOfDay(for: Date()), sampleEndTime: Date = Date(), onCompletion completionBlock:  @escaping HealthServiceSampleResultBlock, onUpdate updateBlock: @escaping HealthServiceSampleResultBlock){
 
        do {
            let isAccessGrantedForService = try isAccessGranted(forService: serviceType)
            guard isAccessGrantedForService else {
                completionBlock(-1.0, .permissionDenied(type: serviceType))
                return
            }
            let predicate = HKQuery.predicateForSamples(withStart: sampleStartTime, end: sampleEndTime, options: .strictStartDate)
            
            print("sampleStartTime ----", sampleStartTime)
            print("sampleEndTime ----", sampleEndTime)
            let manualEntryPredicate = NSPredicate(format: "metadata.%K != YES", HKMetadataKeyWasUserEntered)
            
            let compoundPredicate = NSCompoundPredicate(type: .and, subpredicates: [manualEntryPredicate])

            
            var interval = DateComponents()
           // interval.day = 1
            interval.minute = 5
            let sampleType = serviceType.serviceObject
            
            let query = HKStatisticsCollectionQuery(quantityType: sampleType as! HKQuantityType, quantitySamplePredicate: compoundPredicate, options: [.cumulativeSum, .separateBySource], anchorDate: sampleStartTime, intervalComponents: interval)
            query.initialResultsHandler = { (_,result, error) in
                guard error == nil, let results = result else {
                    DispatchQueue.main.async {
                        completionBlock(-1.0, .unknown(message: error!.localizedDescription))
                    }
                    return
                }
                results.enumerateStatistics(from: sampleStartTime, to: sampleEndTime) {[weak self] (statistics, stop) in

                    self?._handleQueryResult(serviceType: serviceType, statistics: statistics, onCompletion:  updateBlock)
                }
            }
            query.statisticsUpdateHandler = { [weak self](_,statistics, statCollection, error) in
                guard error == nil else {
                    DispatchQueue.main.async {
                        completionBlock(-1.0, .unknown(message: error?.localizedDescription ?? "statistics is nil"))
                    }
                    return
                }
                if statistics?.sumQuantity() != nil {

                    print(sampleStartTime)

                    print(sampleEndTime)
                    
                    self?._handleQueryResult(serviceType: serviceType, statistics: statistics!, onCompletion: updateBlock)
                } else {
                    statCollection?.enumerateStatistics(from: sampleStartTime, to: sampleEndTime, with: { (result, stop) in

//                        self?._handleQueryResult(serviceType: serviceType, statistics: result, onCompletion: updateBlock)
                    })
                }
            }
            _store.execute(query)

        } catch let error as HealthServiceError {
            completionBlock(-1.0, error)
        } catch {
            completionBlock(-1.0, HealthServiceError.unknown(message: error.localizedDescription))
        }

    }
    private func _handleQueryResult(serviceType: HealthServiceType, statistics: HKStatistics, onCompletion completionBlock: @escaping HealthServiceSampleResultBlock) {
        
        
        guard let quantity = statistics.sumQuantity() else {
            DispatchQueue.main.async {
                completionBlock(-1.0, .unknown(message: "Unable to get statistics data"))
            }
            return
        }

        print(statistics.startDate)
        var currentTime = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let dateStr = self.activeChallenge?.data.updatedTime{
            currentTime = formatter.date(from: (dateStr) )! as NSDate
        }

//        if statistics.endDate < currentTime as Date{
//            return
//        }
        
        if self.startDate != statistics.startDate {
            self.startDate = statistics.startDate
            self.updatedValue = 0
        }
        
        let sampleData = quantity.doubleValue(for: serviceType.unit)

            if sampleData > 0 {
            if sampleData > self.updatedValue{
                print("sampleData - self.updatedValue ",sampleData - self.updatedValue )
            completionBlock(sampleData - self.updatedValue, nil)
            }else{
                if self.updatedValue == 0{
                completionBlock(sampleData, nil)
                }
            }
            
            print("sampleData new data--> ",sampleData )
            print("updatedValue ", self.updatedValue )

                self.updatedValue = sampleData

        }
    }
    func sampleDataforHealthkit(forServiceType serviceType: HealthServiceType, sampleStartTime: Date = Calendar.current.startOfDay(for: Date()), sampleEndTime: Date = Date(), onCompletion completionBlock:  @escaping HealthServiceSampleResultBlock, onUpdate updateBlock: @escaping HealthServiceSampleResultBlock){
 
        do {
            let isAccessGrantedForService = try isAccessGranted(forService: serviceType)
            guard isAccessGrantedForService else {
                completionBlock(-1.0, .permissionDenied(type: serviceType))
                return
            }
            let predicate = HKQuery.predicateForSamples(withStart: sampleStartTime, end: sampleEndTime, options: .strictStartDate)
            
            print("sampleStartTime ----", sampleStartTime)
            let manualEntryPredicate = NSPredicate(format: "metadata.%K != YES", HKMetadataKeyWasUserEntered)
            
            let compoundPredicate = NSCompoundPredicate(type: .and, subpredicates: [manualEntryPredicate,  predicate])

            var interval = DateComponents()
            interval.day = 1
            let sampleType = serviceType.serviceObject
            
            let query = HKStatisticsCollectionQuery(quantityType: sampleType as! HKQuantityType, quantitySamplePredicate: compoundPredicate, options: [.cumulativeSum, .separateBySource], anchorDate: sampleStartTime, intervalComponents: interval)
            query.initialResultsHandler = { (_,result, error) in
                guard error == nil, let results = result else {
                    DispatchQueue.main.async {
                        completionBlock(-1.0, .unknown(message: error!.localizedDescription))
                        
//                        let todayDate = "\(UserDefaults.standard.value(forKey: "startDate_Time") ?? "")"
//                        let tommorrowDate = "\(UserDefaults.standard.value(forKey: "updateDate_Time") ?? "")"
//
//                        if todayDate != tommorrowDate {
//                            let sTotal = UserDefaults.standard.value(forKey: "total_Stepcount")
//                            let totalS = "\(sTotal ?? "")"
//                            let totalSteps = Float(totalS) ?? 0
//                            UserDefaults.standard.set(totalSteps, forKey: "final_Stepcount")
//                            UserDefaults.standard.set(0, forKey: "update_StepCount")
//                        }

                    }
                    return
                }
                results.enumerateStatistics(from: sampleStartTime, to: sampleEndTime) {[weak self] (statistics, stop) in

                    self?.healthKithandleQueryResult(serviceType: serviceType, statistics: statistics, onCompletion:  updateBlock)
                }
            }
            query.statisticsUpdateHandler = { [weak self](_,statistics, statCollection, error) in
                guard error == nil else {
                    DispatchQueue.main.async {
                        completionBlock(-1.0, .unknown(message: error?.localizedDescription ?? "statistics is nil"))
                    }
                    return
                }
                if statistics?.sumQuantity() != nil {

                    print(sampleStartTime)

                    print(sampleEndTime)
                    
                    self?.healthKithandleQueryResult(serviceType: serviceType, statistics: statistics!, onCompletion: updateBlock)
                } else {
                    statCollection?.enumerateStatistics(from: sampleStartTime, to: sampleEndTime, with: { (result, stop) in

//                        self?._handleQueryResult(serviceType: serviceType, statistics: result, onCompletion: updateBlock)
                    })
                }
            }
            _store.execute(query)

        } catch let error as HealthServiceError {
            completionBlock(-1.0, error)
        } catch {
            completionBlock(-1.0, HealthServiceError.unknown(message: error.localizedDescription))
        }

    }
    
    private func healthKithandleQueryResult(serviceType: HealthServiceType, statistics: HKStatistics, onCompletion completionBlock: @escaping HealthServiceSampleResultBlock) {
        
        
        guard let quantity = statistics.sumQuantity() else {
            DispatchQueue.main.async {
                completionBlock(-1.0, .unknown(message: "Unable to get statistics data"))
            }
            return
        }

        print(statistics.startDate)
        var currentTime = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let dateStr = self.activeChallenge?.data.updatedTime{
            currentTime = formatter.date(from: (dateStr) )! as NSDate
        }

//        if statistics.endDate < currentTime as Date{
//            return
//        }
        
        if self.startDate != statistics.startDate {
            self.startDate = statistics.startDate
            self.updatedValue = 0
        }
        
        let sampleData = quantity.doubleValue(for: serviceType.unit)

            if sampleData > 0 {
            if sampleData > self.updatedValue{
                print("sampleData - self.updatedValue ",sampleData - self.updatedValue )
            completionBlock(sampleData - self.updatedValue, nil)
            }else{
                if self.updatedValue == 0{
                completionBlock(sampleData, nil)
                }
            }
            
                let startStepCount = UserDefaults.standard.value(forKey: "starting_StepCount")
                if startStepCount == nil {
                    UserDefaults.standard.set(sampleData, forKey: "starting_StepCount")
                    UserDefaults.standard.set(sampleData, forKey: "update_StepCount")

                    print("sampleData old data --> ",sampleData )

                }else{

                    UserDefaults.standard.set(sampleData, forKey: "update_StepCount")
                    print("sampleData new data --> ",sampleData )
                }

                print("updatedValue ", self.updatedValue )

                self.updatedValue = sampleData

        }
    }

    func getDistance(forServiceType serviceType: HealthServiceType, sampleStartTime: Date = Calendar.current.startOfDay(for: Date()), sampleEndTime: Date = Date(), onCompletion completionBlock:  @escaping HealthServiceSampleResultBlock) {
        
        
        
        guard let type = HKSampleType.quantityType(forIdentifier: .stepCount) else {
            fatalError("Something went wrong retriebing quantity type distanceWalkingRunning")
        } 
        let predicate = HKQuery.predicateForSamples(withStart: sampleStartTime, end: sampleEndTime, options: .strictStartDate)

        let query = HKStatisticsQuery(quantityType: type, quantitySamplePredicate: predicate, options: [.cumulativeSum]) { (query, statistics, error) in
            var value = 0
            
            if error != nil {
                print("something went wrong")
            } else if let quantity = statistics?.sumQuantity() {
                value = Int(quantity.doubleValue(for: HKUnit.count()))
            }
            DispatchQueue.main.async {
                if value == 0{
                    completionBlock(-1.0, error as? HealthServiceError)
                } else {
                    completionBlock(Double(value), error as? HealthServiceError)
                }
            }
        }
        _store.execute(query)
        
       }
    
    
    func getCalories(completion: @escaping (Double?, Error?) -> () ) {

         guard let type = HKSampleType.quantityType(forIdentifier: .activeEnergyBurned) else {

             fatalError("Something went wrong retriebing quantity type activeEnergyBurned")

         }

         let date =  Date()
         let cal = Calendar(identifier: Calendar.Identifier.gregorian)
         let newDate = cal.startOfDay(for: date)
         let manualEntryPredicate = NSPredicate(format: "metadata.%K != YES", HKMetadataKeyWasUserEntered)
         let predicate = HKQuery.predicateForSamples(withStart: newDate, end: Date(), options: .strictEndDate)
        let compoundPredicate = NSCompoundPredicate(type: .and, subpredicates: [manualEntryPredicate, predicate])
         let query = HKStatisticsQuery(quantityType: type, quantitySamplePredicate: compoundPredicate, options: [.cumulativeSum]) { (query, statistics, error) in

             var value = 0.00
            var valueInt : Int = 0

             if error != nil {

             } else if let quantity = statistics?.sumQuantity() {
                 value = quantity.doubleValue(for: HKUnit.kilocalorie())
             }
             DispatchQueue.main.async {
                 DispatchQueue.main.async {
                     if value == 0.00{
                         completion(value, nil)
                     } else {
                        completion(value, nil)
                     }
                 }
             }
         }
           _store.execute(query)
        }

       func getDistance(completion: @escaping (Double?, Error?) -> () ) {

           guard let stepCountType = HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning) else {
               fatalError("*** Unable to get the step count type ***")
           }

           var interval = DateComponents()
           interval.day = 1

           let date =  Date()
           let cal = Calendar(identifier: Calendar.Identifier.gregorian)
           let newDate = cal.startOfDay(for: date)
           let manualEntryPredicate = NSPredicate(format: "metadata.%K != YES", HKMetadataKeyWasUserEntered)

           let compoundPredicate = NSCompoundPredicate(type: .and, subpredicates: [manualEntryPredicate])

           let query = HKStatisticsCollectionQuery.init(quantityType: stepCountType,
                                                        quantitySamplePredicate: compoundPredicate,
                                                        options: [.cumulativeSum, .separateBySource],
                                                        anchorDate: newDate,
                                                        intervalComponents: interval)

           query.initialResultsHandler = {

               query, results, error in

               results?.enumerateStatistics(from: newDate,
                                            to: Date(), with: { (result, stop) in
                                               let value = result.sumQuantity()?.doubleValue(for: HKUnit.mile()) ?? 0
                                               DispatchQueue.main.async {
                                                   if value == 0.00{
                                                       completion(value, nil)
                                                   } else {
                                                      completion(value, nil)
                                                   }
                                              }

               })
           }
           _store.execute(query)
       }
    // get cycling distance
        func getCyclingDistance(completion: @escaping (Double?, Error?) -> ()) {
        
            
        guard let stepCountType = HKObjectType.quantityType(forIdentifier: .distanceCycling) else {
            fatalError("*** Unable to get the step count type ***")
        }

        var interval = DateComponents()
        interval.day = 1

        let date =  Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let newDate = cal.startOfDay(for: date)
        let manualEntryPredicate = NSPredicate(format: "metadata.%K != YES", HKMetadataKeyWasUserEntered)

        let compoundPredicate = NSCompoundPredicate(type: .and, subpredicates: [manualEntryPredicate])

        let query = HKStatisticsCollectionQuery.init(quantityType: stepCountType,
                                                     quantitySamplePredicate: nil,
                                                     options: [.cumulativeSum],
                                                     anchorDate: newDate,
                                                     intervalComponents: interval)

        query.initialResultsHandler = {

            query, results, error in

            results?.enumerateStatistics(from: newDate,
                                         to: Date(), with: { (result, stop) in
                                            let value = result.sumQuantity()?.doubleValue(for: HKUnit.mile()) ?? 0
                                            DispatchQueue.main.async {
                                                if value == 0.00{
                                                    completion(value, nil)
                                                } else {
                                                   completion(value, nil)
                                                }
                                           }

            })
        }
        _store.execute(query)
    }
    
    
    //get swmming distance
    func getswmmingDistance(completion: @escaping (Double?, Error?) -> ()) {
    
        
    guard let stepCountType = HKObjectType.quantityType(forIdentifier: .distanceSwimming) else {
        fatalError("*** Unable to get the step count type ***")
    }

    var interval = DateComponents()
    interval.day = 1

    let date =  Date()
    let cal = Calendar(identifier: Calendar.Identifier.gregorian)
    let newDate = cal.startOfDay(for: date)
    let manualEntryPredicate = NSPredicate(format: "metadata.%K != YES", HKMetadataKeyWasUserEntered)

    let compoundPredicate = NSCompoundPredicate(type: .and, subpredicates: [manualEntryPredicate])

    let query = HKStatisticsCollectionQuery.init(quantityType: stepCountType,
                                                 quantitySamplePredicate: nil,
                                                 options: [.cumulativeSum],
                                                 anchorDate: newDate,
                                                 intervalComponents: interval)

    query.initialResultsHandler = {

        query, results, error in

        results?.enumerateStatistics(from: newDate,
                                     to: Date(), with: { (result, stop) in
                                        let value = result.sumQuantity()?.doubleValue(for: HKUnit.mile()) ?? 0
                                        DispatchQueue.main.async {
                                            if value == 0.00{
                                                completion(value, nil)
                                            } else {
                                               completion(value, nil)
                                            }
                                       }

        })
    }
    _store.execute(query)
}
    //get swmming strokes
    func getswmmingStrokes(completion: @escaping (Double?, Error?) -> ()) {
    
        
    guard let stepCountType = HKObjectType.quantityType(forIdentifier: .swimmingStrokeCount) else {
        fatalError("*** Unable to get the step count type ***")
    }

    var interval = DateComponents()
    interval.day = 1

    let date =  Date()
    let cal = Calendar(identifier: Calendar.Identifier.gregorian)
    let newDate = cal.startOfDay(for: date)
    let manualEntryPredicate = NSPredicate(format: "metadata.%K != YES", HKMetadataKeyWasUserEntered)

    let compoundPredicate = NSCompoundPredicate(type: .and, subpredicates: [manualEntryPredicate])

    let query = HKStatisticsCollectionQuery.init(quantityType: stepCountType,
                                                 quantitySamplePredicate: nil,
                                                 options: [.cumulativeSum],
                                                 anchorDate: newDate,
                                                 intervalComponents: interval)

    query.initialResultsHandler = {

        query, results, error in

        results?.enumerateStatistics(from: newDate,
                                     to: Date(), with: { (result, stop) in
                                        let value = result.sumQuantity()?.doubleValue(for: HKUnit.mile()) ?? 0
                                        DispatchQueue.main.async {
                                            if value == 0.00{
                                                completion(value, nil)
                                            } else {
                                               completion(value, nil)
                                            }
                                       }

        })
    }
    _store.execute(query)
}



       func getTodaysStepsCollectionCumulitive(completion: @escaping (Double?, Error?) -> () ) {

           guard let stepCountType = HKObjectType.quantityType(forIdentifier: .stepCount) else {
               fatalError("*** Unable to get the step count type ***")
           }

           var interval = DateComponents()
           interval.day = 1
           let date =  Date()
           let cal = Calendar(identifier: Calendar.Identifier.gregorian)
           let newDate = cal.startOfDay(for: date)
           let manualEntryPredicate = NSPredicate(format: "metadata.%K != YES", HKMetadataKeyWasUserEntered)
           let compoundPredicate = NSCompoundPredicate(type: .and, subpredicates: [manualEntryPredicate])

           let query = HKStatisticsCollectionQuery.init(quantityType: stepCountType,
                                                        quantitySamplePredicate: compoundPredicate,
                                                        options: [.cumulativeSum, .separateBySource],
                                                        anchorDate: newDate,
                                                        intervalComponents: interval)
           query.initialResultsHandler = {
               query, results, error in

               results?.enumerateStatistics(from: newDate,
                                            to: Date(), with: { (result, stop) in
                                               let count = result.sumQuantity()?.doubleValue(for: HKUnit.count()) ?? 0
                                               if count > 0 {
                                                   print(result.sources)
                                                   print("todays steps count::\(count)")
                                               }
                                               completion(count, error as NSError?)
               })
           }
           _store.execute(query)
       }
    // write or manually ener cycling record to healthkid distance cycling . this is not required now
 /*   func writeDatatoHealthKitt(distance: Double, sampleStartTime: Date = Calendar.current.startOfDay(for: Date()), sampleEndTime: Date = Date(), onCompletion completionBlock:  @escaping HealthServiceSampleResultBlock) {
        self.requestAccess { [weak self](success, error) in
            guard error == nil, success else {
                print("Some error occurend when fetching permission...")
                return
            }
            
        }
            guard let type = HKSampleType.quantityType(forIdentifier: .distanceCycling) else {
                fatalError("Something went wrong retriebing quantity type distanceWalkingRunning")
            }
        let unit = HKUnit.mile()
        let quantityy = HKQuantity.init(unit: unit, doubleValue: distance)
        let sample = HKQuantitySample.init(type: type, quantity: quantityy, start: sampleStartTime, end: sampleEndTime)//(type: type, quantity: quantityy, start: sampleStartTime, end: sampleEndTime)
        self._store.save(sample) { (value, error) in
            print(error)
            print(value)
            completionBlock(distance, error as? HealthServiceError)
        }
    }*/
}
