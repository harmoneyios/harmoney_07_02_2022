//
//  AppDelegate.swift
//  Harmoney
//
//  Created by Prema Ravikumar on 19/12/19.
//  Copyright © 2019 Prema Ravikumar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreMotion
import UserNotifications
import Alamofire
import RAMAnimatedTabBarController
import FBSDKCoreKit
import SVProgressHUD
import ObjectMapper
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate{

    var window: UIWindow?
//    let tableViewController = MyMenuTableViewController()
    var appConfigdata =  [String:Any]()
    var cmsModelObject : CMSModel?
    private var coordinator: Coordinator!
    let notificationCenter = UNUserNotificationCenter.current()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
        [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UNUserNotificationCenter.current().delegate = self
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //loadHbotDataPlist()
        IQKeyboardManager.shared.enable = true        
        SVProgressHUD.setDefaultStyle(.dark)
        getAppconfigAPI()
        if UserDefaultConstants().login == true {
            initialView()
        }else{
            setupSplash()
        }
        
        print("User session Token")
        print(UserDefaultConstants().guid ?? "")
        
        
        
//        checkActivityType()
       
        if let dbData =  UserDefaults.standard.value(forKey: "DashBoardData"){
            let unRachive = NSKeyedUnarchiver.unarchiveObject(with: dbData as! Data)
            HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: unRachive as! [String : Any])            
        }

        setupAppCoordinator()
        coordinator.start()
        FirebaseApp.configure()
        
        notificationCenter.delegate = self
        
        
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
        loadHbotDataPlist()
        return true
    }
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        

        return true
    }
//    func checkActivityType() {
//        PedometerManager.sharedInstance.startMotionActivityUpdates { (activityType, changed) in
//
//            if changed {
//            }
//        }
//    }
    

    
    
    func registerForPushNotifications(content:HarmoneyBucksConfirmationVC) {
       UNUserNotificationCenter.current()
         .requestAuthorization(options: [.alert, .sound, .badge]) {
           granted, error in
           print("Permission granted: \(granted)")
//             guard granted else { return }
                DispatchQueue.main.async {
                    self.getNotificationSettings()
                    content.notificationAction()
            }
       }
     }
    

    func getNotificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { settings in
        print("Notification settings: \(settings)")
        
        guard settings.authorizationStatus == .authorized else { return }
        DispatchQueue.main.async {
          UIApplication.shared.registerForRemoteNotifications()
        }
      }
    }


    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        if response.notification.request.identifier == "Local Notification" {
                    print("Handling notifications with the Local Notification Identifier")
            
            let tabBar =  GlobalStoryBoard().launchTabBAr
            tabBar.selectedIndex = 4
            
            window?.rootViewController = tabBar
            
           
        }else{
            if response.notification.request.content.userInfo["notif_type"] as? String == "yuru" {
                let myvalue = response.notification.request.content.userInfo["notif_typeId"] as? Int
                if myvalue == 1
                {
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(myvalue, forKey: "yuruTypeIdDailyChallenge")
                    let notifiMessage = response.notification.request.content.userInfo["notif_msg"] as? String
                    let userDefaultsContent = UserDefaults.standard
                    userDefaultsContent.set(notifiMessage, forKey: "yuruNotificationContent")
                }else if myvalue == 3{
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(myvalue, forKey: "yuruTypeIdPersonalFitness")
                }else if myvalue == 4{
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(myvalue, forKey: "yuruTypeIdPersonalChore")
                }else if myvalue == 5{
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(myvalue, forKey: "yuruTypeIdPersonalNightMessage")
                }
                print(response.notification.request.content.userInfo)
                HarmonySingleton.showNightMessageView = true
                let tabBar =  GlobalStoryBoard().launchTabBAr
                tabBar.selectedIndex = 4
                
                window?.rootViewController = tabBar
                return
                
            }
            if let topController = keyWindow?.rootViewController {
                let vc = GlobalStoryBoard().notificationsVC
                topController.present(vc, animated: true, completion: nil)
            }
        }
      
     
      
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge]) //required to show notification when in foreground
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                   HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
        
    }

   func application(_ application: UIApplication,didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler:@escaping (UIBackgroundFetchResult) -> Void) {
    
    NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)

       let center =  UNUserNotificationCenter.current()
   //    let options: UNAuthorizationOptions = [.alert, .sound];
   //    center.requestAuthorization(options: options) {
   //      (granted, error) in
   //        if !granted {
   //          print("Something went wrong")
   //        }
   //    }
   //    center.getNotificationSettings { (settings) in
   //      if settings.authorizationStatus != .authorized {
   //        // Notifications not allowed
   //      }
   //    }

       let content = UNMutableNotificationContent()
       content.title = "Don't forget"
       content.body = "Buy some milk"
       content.sound = UNNotificationSound.default


       //notification trigger can be based on time, calendar or location
       let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
       repeats: false)

       //create request to display
       let request = UNNotificationRequest(identifier: "ContentIdentifier", content: content, trigger: trigger)

       //add request to notification center
       center.add(request) { (error) in
           if error != nil {
               print("error \(String(describing: error))")
           }
       }

   //    if application.applicationState == .active {
   //           let localNotification = UILocalNotification()
   //               localNotification.userInfo = userInfo
   //               localNotification.soundName = UILocalNotificationDefaultSoundName
   //               localNotification.alertBody = "Information"
   //               localNotification.fireDate = Date()
   //               UIApplication.shared.scheduleLocalNotification(localNotification)
   //           }
    HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
       LevelsManager.sharedInstance.levelRewardClaimed = false
       }
       
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = deviceToken.hexString
        NotificationsManager.sharedInstance.sendDeviceToken(token: token)
        UserDefaultConstants().pushdDeviceToken = token
        
//        if ConstantString.deviceToken != ""{
//            if ConstantString.deviceToken != token{
//                UserDefaultConstants().isUpdatedDeviceToken = true
//                UserDefaultConstants().deviceToken = token
//                updateDeviceToken()
//            }
//            else {
//                UserDefaultConstants().isUpdatedDeviceToken = false
//            }
//        }
//        else {
//            UserDefaultConstants().isUpdatedDeviceToken = true
//        }
//
//        UserDefaultConstants().deviceToken = token
    }
   
    
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print(userInfo)
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
            HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()    
    }
    
    func setupSplash() {
        self.window = UIWindow(frame: UIScreen.main.bounds)            
        self.window?.rootViewController = GlobalStoryBoard().splashVC
        self.window?.makeKeyAndVisible()
    }
    
    func presentDailyViewController() {
        window = UIWindow(frame: UIScreen.main.bounds)
        let tabBar =  GlobalStoryBoard().launchTabBAr
        tabBar.selectedIndex = 0
        
        window?.rootViewController = tabBar
        window?.makeKeyAndVisible()
    }

    func initialView() {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        if UserDefaultConstants().guid == nil {
            let loginViewController = GlobalStoryBoard().loginVC
            let nav = UINavigationController()
            
            nav.pushViewController(loginViewController, animated: true)
            window?.rootViewController = nav
            window?.makeKeyAndVisible()
        } else {
            if UserDefaultConstants().login! {
                let tabBar =  GlobalStoryBoard().launchTabBAr
                tabBar.selectedIndex = 0
                
                window?.rootViewController = tabBar
            } else {
                let loginViewController = GlobalStoryBoard().newLoginVC
                let nav = UINavigationController()
                
                nav.pushViewController(loginViewController, animated: true)
                window?.rootViewController = nav
            }
            window?.makeKeyAndVisible()
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        if let topController = UIApplication.topViewController() as? EarnTabViewController {
            if let dailyViewController = topController.pageViecontroller?.viewControllers?.first as? DailyViewController {
                dailyViewController.restartAnimation()
            } else  if let personalFitnessViewController = topController.pageViecontroller?.viewControllers?.first as? PersonalFitnessViewController {
                personalFitnessViewController.restartAnimation()
            }
        }
        if let topController = UIApplication.topViewController() as? HomeViewController {
            if let home = topController as? HomeViewController {
                home.restarthealthkit()
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    }
    func getAppconfigAPI() {
        
        let data = [String : Any]()

            if Reachability.isConnectedToNetwork(){
                let (url, method, param) = APIHandler().appConfig(params: data)
                AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                    guard let data = response.data else { return }
                    self.cmsModelObject = try? JSONDecoder().decode(CMSModel.self, from: data)                    
                }

            }
    }
    
    func updateDeviceToken() {
        
        if Reachability.isConnectedToNetwork() {
            var data = [String : Any]()
            let deviceToken = UserDefaultConstants().deviceToken ?? "1233456677"
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            data.updateValue(deviceToken, forKey: "deviceToken")

            let (url, method, param) = APIHandler().updateDeviceToken(params: data)
            
            AF.request(url, method: method, parameters: param, encoding: JSONEncoding.default).validate().responseJSON { response in
                switch response.result {
                    
                case .success(let value):
                    if  value is [String:Any] {
                        
                    }
                case .failure(let error):
                    print(error)
                }
            }
        } else {
        }
    }
    func setupAppCoordinator() {
        if (coordinator != nil ) {
            return
        }

        let appCoordinator = AppCoordinator(window: window!)
         coordinator = appCoordinator
//        let options = FirebaseOptions(contentsOfFile: Bundle.main.path(forResource: ConfigurationManager().configuration.googlePlistName, ofType: "plist")! )
//        FirebaseApp.configure(options: options!)

    }
    
  
    
    func scheduleNotification(notificationType: String) {
        
        let content = UNMutableNotificationContent() // Содержимое уведомления
        let categoryIdentifire = "Delete Notification Type"
        
        content.title = "Harmoney"
        content.body = "Good morning, Start your day positively by setting your goals for the day! "
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = categoryIdentifire
        
        var trigger = UNTimeIntervalNotificationTrigger(timeInterval: 7200, repeats: true)
        let identifier = "Local Notification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: "DeleteAction", title: "Delete", options: [.destructive])
        let category = UNNotificationCategory(identifier: categoryIdentifire,
                                              actions: [snoozeAction, deleteAction],
                                              intentIdentifiers: [],
                                              options: [])
        
        notificationCenter.setNotificationCategories([category])
        
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    
}


