//
//  SubListTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 31/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class SubListTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var walletNameLabel: UILabel!
    @IBOutlet weak var editWallet: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
