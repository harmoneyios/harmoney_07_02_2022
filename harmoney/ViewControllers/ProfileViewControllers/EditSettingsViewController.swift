//
//  EditSettingsViewController.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/20/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ObjectMapper
import IQKeyboardManagerSwift

class EditSettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var settingsTableView: UITableView!
    var stringDateFull : String?
    
  var profileUpdate = ProfileUpdateObject()
  var EditImgCellIdentifier = "EditImgTableViewCell"
  var EditTfCellIndetifier = "EditTfTableViewCell"
  var EditDateCellIdentifier = "EditDateTableViewCell"
  var EditGendeCellIdentifier = "EditGenderTableViewCell"
  var EditDropCellIdentifier = "EditDropTableViewCell"
  var EditDonCellIndentifier = "EditDoneTableViewCell"
    var currentTF : UITextField?
    var profileParentVC : MyPfrofileViewController?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
       super.viewDidLoad()
        
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        IQKeyboardManager.shared.enable = false
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12

       }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
           return 150//Choose your custom row height
        }else if indexPath.row == 7{
            return 78
        }else if indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10{
            return 0
        }
        return 88
    }
//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
        if indexPath.row == 0 {
          let cell = tableView.dequeueReusableCell(withIdentifier: EditImgCellIdentifier, for: indexPath) as! EditImgTableViewCell
           cell.closeBtn.addTarget(self, action: #selector(closeTapped(_:)), for: .touchUpInside)
            cell.editLbl.text = ConstantString.editProfile
            cell.editImgView.image = profileUpdate.profileImage
            cell.parentVC = self
          //  cell.closeBtn.addTarget(self, action: #selector(editBtnAction()), for: .touchUpInside)
            
            
            return cell
        }else if indexPath.row == 6  {
        let cell = tableView.dequeueReusableCell(withIdentifier: EditDateCellIdentifier, for: indexPath) as! EditDateTableViewCell
            cell.dateLbl.text = ConstantString.dateofBirth
            cell.stringDate = profileUpdate.dateOfBirth
            cell.parentVc = self
            if HarmonySingleton.shared.dashBoardData.data?.userType == .parent{
                cell.datePickerView.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
            }
            else if  HarmonySingleton.shared.dashBoardData.data?.userType == .adult{
                cell.datePickerView.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
            }
            else{
                cell.datePickerView.maximumDate = Calendar.current.date(byAdding: .year, value: 100, to: Date())
            }
           
            return cell
        }
        else if indexPath.row == 7  {
            let cell = tableView.dequeueReusableCell(withIdentifier: EditGendeCellIdentifier, for: indexPath) as! EditGenderTableViewCell
            cell.genderLbl.text = ConstantString.chooseGender
            cell.parentVC = self
            if profileUpdate.gender == "Male"
            {
                cell.handleGenderButtons(sender: cell.maleBtn)
                
            }
            else if profileUpdate.gender == "Female"
            {
                cell.handleGenderButtons(sender: cell.femaleBtn)
                //cell.genderClick(sender: cell.femaleBtn)
            }
                //handleGenderButtons(sender: cell.femaleBtn)}


            //cell.handleGenderButtons(sender: cell.maleBtn)
            return cell
//              cell.customTextField.text = profileObj?.data?.last?.email

        }
        else if indexPath.row == 8  {
        let cell = tableView.dequeueReusableCell(withIdentifier: EditDropCellIdentifier, for: indexPath) as! EditDropTableViewCell
            cell.editTf.addTarget(self, action: #selector(updateValuesOnUpdateProfileObject(textField:)), for: .allEvents)
            cell.editTf.tag = 100 + indexPath.row
        cell.bankNameLbl.text = ConstantString.bankName
        cell.editTf.text = profileUpdate.bankName
        return cell
        }
        else if indexPath.row == 11  {
        let cell = tableView.dequeueReusableCell(withIdentifier: EditDonCellIndentifier, for: indexPath) as! EditDoneTableViewCell
       cell.doneBtn.addTarget(self, action: #selector(updateUserProfile), for: .touchUpInside)
            
        return cell
        }else  {
        let cell = tableView.dequeueReusableCell(withIdentifier: EditTfCellIndetifier, for: indexPath) as! EditTfTableViewCell
            cell.editTextField.tag = 100 + indexPath.row
            cell.editTextField.delegate = self
            cell.editTextField.keyboardType = .default

            cell.editTextField.addTarget(self, action: #selector(updateValuesOnUpdateProfileObject(textField:)), for: .allEvents)
            //                if(isProfileEditing){
            if (indexPath.row == 3 || indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10){
                cell.editTextField.isEnabled = false
            }else{
                cell.editTextField.isEnabled = true
            }
            
            if indexPath.row == 1 {
                cell.nameLbl.text = ConstantString.firstName
                cell.editTextField.text = profileUpdate.name
                
            }
            if indexPath.row == 9  {
            cell.nameLbl.text = ConstantString.accountNumber
            cell.editTextField.text = profileUpdate.bankAccount
            }
            if indexPath.row == 2{
                cell.nameLbl.text = ConstantString.lastName
               cell.editTextField.text = profileUpdate.lastName
            }
            if indexPath.row == 3  {
                cell.nameLbl.text = ConstantString.mobileNumber
               cell.editTextField.text = profileUpdate.mobileNumber
            }
            if indexPath.row == 4  {
                cell.nameLbl.text = ConstantString.zipCode
                cell.editTextField.text = profileUpdate.zip
                cell.editTextField.keyboardType = .numberPad

               // cell.editTextField.
            }
            if indexPath.row == 5  {
                cell.nameLbl.text = ConstantString.emailID
                cell.editTextField.text = profileUpdate.email
                cell.editTextField.keyboardType = .emailAddress
            }
            if indexPath.row == 10  {
                cell.nameLbl.text = ConstantString.routingNumber
                cell.editTextField.text = profileUpdate.routingNumber
            }
            
           
        return cell
    }
}
    
        @objc func updateValuesOnUpdateProfileObject(textField:UITextField) {
            currentTF = textField
            let string = textField.text
            switch textField.tag {
                        case 101:
                            profileUpdate.name = string
                        case 102:
                            profileUpdate.lastName = string
                        case 103:
                            profileUpdate.mobileNumber = string
                        case 104:
                            profileUpdate.zip = string
                        case 105:
                            profileUpdate.email = string
                        case 106:
                            profileUpdate.dateOfBirth = string
                
                        case 108:
                            profileUpdate.bankName = string
                        case 109:
                            profileUpdate.bankAccount = string
                        case 110:
                           profileUpdate.routingNumber = string
//                        case 107:
//                            profileUpdate.bankAccount = string
                        default:
                            print(string!)
                        }
        }
//
        @objc func keyboardWillShow(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                settingsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
           }
        }

        @objc func keyboardWillHide(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                settingsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            }
        }
    @objc func updateUserProfile() {
        if let cf = currentTF{
            cf.resignFirstResponder()
        }
        
        if !(profileUpdate.email!.isEmpty) && !(profileUpdate.email!.isEmail){
             AlertView.shared.showAlert(view: self, title: "Email", description: "Please enter valid Email Id")
            return
        } else if (profileUpdate.name?.count == 0 ){
            AlertView.shared.showAlert(view: self, title: "First name", description: "Please enter your name")
            return

        }else if (profileUpdate.zip?.count ?? 0 < 5  || profileUpdate.zip == nil){
            AlertView.shared.showAlert(view: self, title: "Zip Code", description: "Please valid enter your zip code")
            return
            
        }
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
                        
            data.updateValue(getEncryptedString(plainText: profileUpdate.name!) , forKey: "name")
            data.updateValue(getEncryptedString(plainText: profileUpdate.lastName!) , forKey: "lastName")
            data.updateValue(profileUpdate.mobileNumber ?? "", forKey: "mobile")
            data.updateValue( getEncryptedString(plainText: profileUpdate.zip!), forKey: "zip")
//            data.updateValue(getEncryptedString(plainText: profileUpdate.age!), forKey: "age")
            data.updateValue(getEncryptedString(plainText: profileUpdate.dateOfBirth!) , forKey: "dob")
            data.updateValue( getEncryptedString(plainText: profileUpdate.email!) , forKey: "email")
           data.updateValue(profileUpdate.bankName!, forKey: "bankName")
           data.updateValue(profileUpdate.bankAccount!, forKey: "bankAccount")
            data.updateValue(profileUpdate.routingNumber!, forKey: "routingNumber")
            
            
            if let img = profileUpdate.profileImage {
                let imageData = resize(img)
                data.updateValue(imageData, forKey: "profilePic")
            } else {
                data.updateValue("", forKey: "profilePic")
            }
            
           data.updateValue("", forKey: "userType")
           data.updateValue("", forKey: "schoolId")
           data.updateValue("", forKey: "schoolName")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            data.updateValue(getEncryptedString(plainText: profileUpdate.gender!) , forKey: "gender")
            
            let (url, method, param) = APIHandler().updateProfile(params: data)

            AF.request(url, method: method, parameters: param, encoding: JSONEncoding.default,headers: headers).validate().responseJSON { response in
                switch response.result {

                case .success(let value):
                    if  value is [String:Any] {
                        self.profileParentVC?.editContainerView.isHidden = true
                        self.profileParentVC?.updateUIAfterEdit()
                    }
                case .failure(let error):
                    print(error)
                }
                SVProgressHUD.dismiss()
            }
        } else {
            SVProgressHUD.dismiss()
        }
        
    }
    func resize(_ image: UIImage) -> String {
            var actualHeight = Float(image.size.height)
            var actualWidth = Float(image.size.width)
            let maxHeight: Float = 300.0
            let maxWidth: Float = 400.0
            var imgRatio: Float = actualWidth / actualHeight
            let maxRatio: Float = maxWidth / maxHeight
            let compressionQuality: Float = 0.25
            //50 percent compression
            if actualHeight > maxHeight || actualWidth > maxWidth {
                if imgRatio < maxRatio {
                    //adjust width according to maxHeight
                    imgRatio = maxHeight / actualHeight
                    actualWidth = imgRatio * actualWidth
                    actualHeight = maxHeight
                }
                else if imgRatio > maxRatio {
                    //adjust height according to maxWidth
                    imgRatio = maxWidth / actualWidth
                    actualHeight = imgRatio * actualHeight
                    actualWidth = maxWidth
                }
                else {
                    actualHeight = maxHeight
                    actualWidth = maxWidth
                }
            }
            let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
            UIGraphicsBeginImageContext(rect.size)
            image.draw(in: rect)
            let img = UIGraphicsGetImageFromCurrentImageContext()
            let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
            UIGraphicsEndImageContext()
            let imagebase64String = imageData?.base64EncodedString()
                    print(imagebase64String!)
            //        let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
            return imagebase64String!
    //        return UIImage(data: imageData!) ?? UIImage()
        }

    @objc func closeTapped(_ sender: Any){
        profileParentVC?.editContainerView.isHidden = true
        self.profileParentVC?.updateUIAfterEdit()
        if let cf = currentTF{
                   cf.resignFirstResponder()
               }
    }

    


}


//extension EditTfTableViewCell : UITextFieldDelegate{
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        textField.resignFirstResponder()
//    }
//}
//MARK: UITextFieldDelegate Methods
extension EditSettingsViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 101 ||  textField.tag == 102 {
            guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
               let substringToReplace = textFieldText[rangeOfTextToReplace]
               let count = textFieldText.count - substringToReplace.count + string.count
           
            do {
               let regex = try NSRegularExpression(pattern: ".*[^A-Za-z].*", options: [])
               if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                   return false
               }
           }
           catch {
               print("ERROR")
           }
            
            return count <= 30
        } else if textField.tag == 104 {
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            textField.text = formattedNumber(number: newString)
            return false
        }
        return true
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXXXX"

        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
}
