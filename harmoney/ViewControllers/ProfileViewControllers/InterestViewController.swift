//
//  InterestViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ObjectMapper
import Toast_Swift


class InterestViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var interestTableView: UITableView!
    var interestObj : InterestModelObj?
    var expandCell = [MyInterestCellTableViewCell]()
    var currentInterests : CurrentIntrestsModel?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        expandCell.removeAll()
        getAllInterests()        
    }
    
    func getAllInterests()  {
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        let data = [String : Any]()
        var (url, method, param) = APIHandler().getInterests(params: data)
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.interestObj = Mapper<InterestModelObj>().map(JSON: value)
                            print(self.interestObj!)
                            self.getUserInterests(reload: true)
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
    }
    
    
    func getUserInterests(reload:Bool)  {

        if Reachability.isConnectedToNetwork() {
            let data = [String : Any]()
//            data.updateValue(UserDefaultConstants().guid!, forKey: "guid")
        var (url, method, param) = APIHandler().getUserInterests(params: data)
        url = url + "/" + UserDefaultConstants().guid!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.currentInterests = Mapper<CurrentIntrestsModel>().map(JSON: value)
                            print(self.currentInterests!)
                            if reload{
                                self.interestTableView.reloadData()
                            }
                        }
                        
                    case .failure(let error):
                        print(error)
                    }
            }
        }
    }
    
    
    
//    MARK: -TableView Delegates
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.interestObj?.Category?.count ?? 0
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyInterestCell", for: indexPath) as! MyInterestCellTableViewCell
        cell.tag = indexPath.row
        if let arr = interestObj?.Category{
            cell.appendData(obj: arr[indexPath.row], parentScreen: self)
        }
        
        
        return cell
        
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let ary = expandCell.filter({$0.tag == indexPath.row})
        if ary.count > 0 {
            if ary.last?.tag == indexPath.row{
            return UITableView.automaticDimension
            }else{
                return 60
            }
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MyInterestCellTableViewCell
        
        if expandCell.contains(cell){
            expandCell.remove(at: expandCell.firstIndex(of: cell)!)
        }else{
            expandCell.removeAll()
            expandCell.append(cell)
        }
        interestTableView.reloadData()
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
        

}
