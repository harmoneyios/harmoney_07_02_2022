//
//  BankDetailAccountAddViewcontroller.swift
//  Harmoney
//
//  Created by Mac on 16/07/20.
//  Copyright © 2020 harmoney. All rights reserved.
//


import UIKit
import Foundation
//import LinkKit

protocol  BankDetailAccountAddViewControllerDelegate: class {
    func linkAccountSuccess(metaData: [String : Any])
}
class BankDetailAccountAddViewcontroller: UIViewController {
    var parentLinkPaymentSuccess = false
    var delegate: BankDetailAccountAddViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork(){
            // depricated Framework tool
           // presentPlaidLinkWithCustomConfiguration()

        }else{
            self.view.toast("Internet Connection not Available!")
        }
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    func handleSuccessWithToken(_ publicToken: String, metadata: [String : Any]?) {
            parentLinkPaymentSuccess = true

            presentAlertViewWithTitle("Success", message: "Your account linked successfully")

        self.delegate?.linkAccountSuccess(metaData: metadata!)
        
    //        presentAlertViewWithTitle("Success", message: "token: \(publicToken)\nmetadata: \(metadata ?? [:])")
        }

        func handleError(_ error: Error, metadata: [String : Any]?) {
            

            presentAlertViewWithTitle("Failure", message: "Your account linked failure")

    //        presentAlertViewWithTitle("Failure", message: "error: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
        }
        
        func handleExitWithMetadata(_ metadata: [String : Any]?) {

            presentAlertViewWithTitle("Cancel", message: "You have cancelled your  link account")
        }

    func presentAlertViewWithTitle(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: false)

        }

        alert.addAction(OKAction)
        present(alert, animated: true, completion: nil)
    }
/* depricated Framework tool
    // MARK: Plaid Link setup with shared configuration from Info.plist
    func presentPlaidLinkWithSharedConfiguration() {
        // <!-- SMARTDOWN_PRESENT_SHARED -->
        // With shared configuration from Info.plist
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet
        }
        present(linkViewController, animated: true)
        // <!-- SMARTDOWN_PRESENT_SHARED -->
    }

    // MARK: Plaid Link setup with custom configuration
    func presentPlaidLinkWithCustomConfiguration() {
        // <!-- SMARTDOWN_PRESENT_CUSTOM -->
        // With custom configuration
        let linkConfiguration = PLKConfiguration(key: "adced9ed28e0da390c03521ee07faf", env: .sandbox, product: .auth)
        linkConfiguration.clientName = "Harmoney Inc"
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(configuration: linkConfiguration, delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet
        }
        present(linkViewController, animated: true)
        // <!-- SMARTDOWN_PRESENT_CUSTOM -->
    }

    // MARK: Start Plaid Link in update mode
    func presentPlaidLinkInUpdateMode() {
        // <!-- SMARTDOWN_UPDATE_MODE -->
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(publicToken: "5e130922b1398b0011fa9e62", delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet
        }
        present(linkViewController, animated: true)
        // <!-- SMARTDOWN_UPDATE_MODE -->
    }
*/
}
/* depricated Framework tool
// MARK: - PLKPlaidLinkViewDelegate Protocol
// <!-- SMARTDOWN_PROTOCOL -->
extension BankDetailAccountAddViewcontroller : PLKPlaidLinkViewDelegate
// <!-- SMARTDOWN_PROTOCOL -->
{

// <!-- SMARTDOWN_DELEGATE_SUCCESS -->
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didSucceedWithPublicToken publicToken: String, metadata: [String : Any]?) {
        dismiss(animated: true) {
            // Handle success, e.g. by storing publicToken with your service
            NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
            self.handleSuccessWithToken(publicToken, metadata: metadata)
        }
    }
// <!-- SMARTDOWN_DELEGATE_SUCCESS -->

// <!-- SMARTDOWN_DELEGATE_EXIT -->
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didExitWithError error: Error?, metadata: [String : Any]?) {
        dismiss(animated: true) {
            if let error = error {
                NSLog("Failed to link account due to: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
                self.handleError(error, metadata: metadata)
            }
            else {
                NSLog("Plaid link exited with metadata: \(metadata ?? [:])")
                self.handleExitWithMetadata(metadata)
            }
        }
    }
// <!-- SMARTDOWN_DELEGATE_EXIT -->
    
// <!-- SMARTDOWN_DELEGATE_EVENT -->
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didHandleEvent event: String, metadata: [String : Any]?) {
        NSLog("Link event: \(event)\nmetadata: \(metadata ?? [:])")
        
//        self.dismiss(animated: false, completion: nil)

    }
// <!-- SMARTDOWN_DELEGATE_EVENT -->
}
*/
