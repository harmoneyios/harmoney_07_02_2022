//
//  PlaidTransactionViewController.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 18/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ObjectMapper
import Toast_Swift
import IQKeyboardManagerSwift
import SDWebImage

protocol PlaidTransactionViewControllerDelegate: class {
    func didTapCloseButton(customAlertViewController: PlaidTransactionViewController)
}

class PlaidTransactionViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    weak var delegate: PlaidTransactionViewControllerDelegate?
    @IBOutlet weak var transactionFilterHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var transactionToTextField: UITextField!
    @IBOutlet weak var transactionTypetextField: UITextField!
    @IBOutlet weak var transactionFromtextfield: UITextField!
    @IBOutlet weak var transactionFilterView: UIView!
    @IBOutlet weak var filterMainStackUiview: UIStackView!
    @IBOutlet weak var filterCloseBtn: UIButton!
    @IBOutlet weak var filterHeadlabel: UILabel!
    @IBOutlet weak var transactionMainBackView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var mainBackView: UIView!
    @IBOutlet weak var transactionDetailTableview: UITableView!
    @IBOutlet weak var transactionDetailView: UIView!
    @IBOutlet weak var noTransactionView: UIView!
    @IBOutlet weak var transactionHeadingLabel: UILabel!
    
    @IBOutlet weak var typeTableview: UITableView!
    @IBOutlet weak var typeSelectionView: UIView!
    @IBOutlet weak var submitResetBtnView: UIView!
    var typeArray = [String]()
    var plaidTransactionDetailsTableViewCell = "plaidTransactionDetailsTableViewCell"
    var transactionTypeTableViewCell = "transactionTypeTableViewCell"
    var viewTransactionDetailObjectValue: viewTransactionDetailObject?
    let startdatePickerView = UIDatePicker()
    var startstringDate : String?{
        didSet{
            if self.startstringDate != nil && self.startstringDate?.count ?? 0 > 0 {
//                self.splitDateToTF()
            }
        }
    }
    
    let enddatePickerView = UIDatePicker()
    var endstringDate : String?{
        didSet{
            if self.endstringDate != nil && self.endstringDate?.count ?? 0 > 0 {
//                self.splitDateToTF()
            }
        }
    }
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        transactionToTextField.isUserInteractionEnabled = true
        transactionDetailTableview.delegate = self
        transactionDetailTableview.dataSource = self
        // Do any additional setup after loading the view.
        transactionDetailTableview.register(UINib.init(nibName: "plaidTransactionDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "plaidTransactionDetailsTableViewCell")
        // Do any additional setup after loading the view.
        
        typeTableview.delegate = self
        typeTableview.dataSource = self
        // Do any additional setup after loading the view.
        typeTableview.register(UINib.init(nibName: "transactionTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "transactionTypeTableViewCell")

        typeArray = ["issue","transfer","redeem"]
        
        
        //startDatePicker
        startdatePickerView.datePickerMode = .date
        startdatePickerView.addTarget(self, action: #selector(handleStartDatePicker(sender:)), for: .valueChanged)
        var components = DateComponents()
        components.month = -1
        var componentsMax = DateComponents()
//        componentsMax.year = -18
//        componentsMax.day = -1
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        let maxDate = Calendar.current.date(byAdding: componentsMax, to: Date())
        startdatePickerView.minimumDate = minDate
        startdatePickerView.maximumDate = maxDate
        if #available(iOS 13.4, *) {
            startdatePickerView.preferredDatePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        transactionFromtextfield.inputView = startdatePickerView
        
        self.transactionFromtextfield.tintColor = .clear
        
        //endDatePicker
        enddatePickerView.datePickerMode = .date
        enddatePickerView.addTarget(self, action: #selector(handleEndDatePicker(sender:)), for: .valueChanged)
        var componentsEnd = DateComponents()
        componentsEnd.month = -1
        var componentsMaxEnd = DateComponents()
//        componentsMaxEnd.year = -18
//        componentsMaxEnd.day = -1
        let minEndDate = Calendar.current.date(byAdding: componentsEnd, to: Date())
        let maxEndDate = Calendar.current.date(byAdding: componentsMaxEnd, to: Date())
        enddatePickerView.minimumDate = minEndDate
        enddatePickerView.maximumDate = maxEndDate
        if #available(iOS 13.4, *) {
            enddatePickerView.preferredDatePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        transactionToTextField.inputView = enddatePickerView
        transactionToTextField.isUserInteractionEnabled = false
        self.transactionToTextField.tintColor = .clear
        
    }
    @objc func handleStartDatePicker(sender: UIDatePicker) {
         let fulDate = DateFormatter()
         fulDate.dateFormat = "yyyy-MM-dd"
        startstringDate = fulDate.string(from: sender.date)
        enddatePickerView.minimumDate = sender.date
       
     transactionFromtextfield.text = startstringDate
        transactionToTextField.isUserInteractionEnabled = true
        var filterStartDate = ""
        if let filter = startstringDate {
            filterStartDate = "\(filter)T00:00:00.00+00:00"
        }
        UserDefaults.standard.set(filterStartDate, forKey: "filterStartDate")
        
    }
    @objc func handleEndDatePicker(sender: UIDatePicker) {
         let fulDate = DateFormatter()
         fulDate.dateFormat = "yyyy-MM-dd"
        endstringDate = fulDate.string(from: sender.date)
     transactionToTextField.text = endstringDate
        var filterEndDate = ""
        if let filter = endstringDate {
            filterEndDate = "\(filter)T24:00:00.00+00:00"
        }
        UserDefaults.standard.set(filterEndDate, forKey: "filterEndDate")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getTransactionDetails()
        transactionTypetextField.addTarget(self, action: #selector(updategoBtn(textField:)), for: .allEvents)
        
        self.typeSelectionView?.backgroundColor = UIColor.init(white: 0.1, alpha: 0.4)
        UserDefaults.standard.set("", forKey: "transactionType")
        UserDefaults.standard.set("", forKey: "filterEndDate")
        UserDefaults.standard.set("", forKey: "filterStartDate")
    }
    
    @objc func updategoBtn(textField:UITextField) {
       transactionTypetextField.resignFirstResponder()
        self.typeSelectionView.isHidden = false
       
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.typeSelectionView.isHidden = true
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if tableView.tag == 10{
            return typeArray.count
        }
        return (self.viewTransactionDetailObjectValue?.data?.transactions?.count ?? 0) + 1
      
       }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 10{
            return 50
            
        }
     
       if indexPath.row == 0{
            return 50
        }
        
        return 60
           
      
    }
    
    @IBAction func filterAction(_ sender: Any) {
        
        UserDefaults.standard.set("", forKey: "transactionType")
        UserDefaults.standard.set("", forKey: "filterEndDate")
        UserDefaults.standard.set("", forKey: "filterStartDate")
        transactionFromtextfield.text = ""
        transactionToTextField.text = ""
        transactionToTextField.isUserInteractionEnabled = false
        transactionTypetextField.text = ""
        self.transactionFilterView.isHidden = false
        self.transactionFilterHeightConstrain.constant = 230
        searchBtn.isHidden = false
        submitResetBtnView.isHidden = true
        
    }

    @IBAction func filterViewCloseAction(_ sender: Any) {
        self.transactionFilterView.isHidden = true
        self.transactionFilterHeightConstrain.constant = 0
    }
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }

        var tempDate = from
        var array = [tempDate]

        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }

        return array
    }
    
    @IBAction func searchAction(_ sender: Any) {
        searchBtn.isHidden = true
        submitResetBtnView.isHidden = false
        let filterStartDate = UserDefaults.standard.string(forKey: "filterStartDate") ?? ""
        if filterStartDate == ""
        {
            self.view.toast("Please select start Date")
            return
        }
        let filterEndDate = UserDefaults.standard.string(forKey: "filterEndDate") ?? ""
        if filterEndDate == ""
        {
            let fulDate = DateFormatter()
            fulDate.dateFormat = "yyyy-MM-dd"
           endstringDate = fulDate.string(from: Date())
           var filterEndDate = ""
           if let filter = endstringDate {
               filterEndDate = "\(filter)T24:00:00.00+00:00"
           }
           UserDefaults.standard.set(filterEndDate, forKey: "filterEndDate")
          filterEndDate = UserDefaults.standard.string(forKey: "filterEndDate") ?? ""
        }
        
        let formatter = DateFormatter()
        let components = filterStartDate.components(separatedBy: "T")
        let firstDate = components.first!
        let componentsEnd = filterEndDate.components(separatedBy: "T")
        let secondDate = componentsEnd.first!
       
        formatter.dateFormat = "yyyy-MM-dd"
        let firstDate1 = formatter.date(from: firstDate)
        let secondDate1 = formatter.date(from: secondDate)!
        if firstDate1?.compare(secondDate1) == .orderedDescending {
            self.view.toast("Please select start date less than end date")
            return
        }
    
        let transactionType = UserDefaults.standard.string(forKey: "transactionType")
        var result = self.viewTransactionDetailObjectValue?.data?.transactions?.filter { ($0.created ?? "") > filterStartDate }
        result = result?.filter { ($0.created ?? "") < filterEndDate }
        if transactionType == "" {
         
            print(result as Any)
        }else{
            result = result?.filter{ ($0.transaction_type ?? "") ==  transactionType }
        }
    
        
        self.viewTransactionDetailObjectValue?.data?.transactions = result
        transactionDetailTableview.reloadData()
    }
    @IBAction func resetAction(_ sender: Any) {
        UserDefaults.standard.set("", forKey: "transactionType")
        UserDefaults.standard.set("", forKey: "filterEndDate")
        UserDefaults.standard.set("", forKey: "filterStartDate")
        transactionFromtextfield.text = ""
        transactionToTextField.isUserInteractionEnabled = false
        transactionToTextField.text = ""
        transactionTypetextField.text = ""
        self.getTransactionDetails()
        
    }
    
    @IBAction func submitAction(_ sender: Any) {
        
      

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
    UITableViewCell {
        if tableView.tag == 10 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "transactionTypeTableViewCell", for: indexPath) as! transactionTypeTableViewCell
            cell.typeLabel.text = typeArray[indexPath.row].capitalized
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "plaidTransactionDetailsTableViewCell", for: indexPath) as! plaidTransactionDetailsTableViewCell
        
        if indexPath.row == 0{
            
            cell.amountLabel.text = "Amount"
            cell.typeLabel.text = ""
            cell.dateLabel.text = "Type & Date"
            cell.statusLabel.text = "Status"
            cell.typeLabel.isHidden = true
            cell.timeLabel.isHidden = true
       
            
        }else{
            cell.contentMainView.backgroundColor = UIColor.white
            cell.typeLabel.isHidden = false
            cell.timeLabel.isHidden = false
            cell.statusLabel.textColor = UIColor.black
            cell.typeLabel.text = self.viewTransactionDetailObjectValue?.data?.transactions?[indexPath.row - 1].transaction_type?.capitalized
            if let amout = self.viewTransactionDetailObjectValue?.data?.transactions?[indexPath.row - 1].sila_amount{
                cell.amountLabel.text = "$\(amout)"
            }
            
            cell.statusLabel.text = self.viewTransactionDetailObjectValue?.data?.transactions?[indexPath.row - 1].status?.capitalized
            let str = self.viewTransactionDetailObjectValue?.data?.transactions?[indexPath.row - 1].created ?? ""
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            if let date = formatter.date(from: str) {
                formatter.dateFormat = "hh:mm a"
                let timeStr = formatter.string(from: date)
                print(timeStr) //add timeStr to your timeLabel here...

                formatter.dateFormat = "yyyy-MM-dd"
                let dateStr = formatter.string(from: date)
                print(dateStr) //add dateStr to your dateLabel here...
                
                cell.dateLabel.text = "\(dateStr)"
                cell.timeLabel.text = "\(timeStr)"
                if #available(iOS 13.0, *) {
                    cell.dateLabel.textColor = UIColor.systemGray3
                    cell.timeLabel.textColor = UIColor.systemGray3
                } else {
                    // Fallback on earlier versions
                }
                
                let today = Date()
                let nextFiveDays = Calendar.current.date(byAdding: .day, value: 5, to: today)!

                let myRange = datesRange(from: date, to: nextFiveDays)
                print(myRange)
            }
        }
        
        return cell
        }
        
        return UITableViewCell()
      
            
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 10
        {
            UserDefaults.standard.set(typeArray[indexPath.row], forKey: "transactionType")
            transactionTypetextField.text = typeArray[indexPath.row]
            self.typeSelectionView.isHidden = true

        }
      
    }

    
    func getTransactionDetails(){
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            var (url, method, param) = APIHandler().viewTransactionDetails(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.viewTransactionDetailObjectValue = Mapper<viewTransactionDetailObject>().map(JSON: value)
                        if self.viewTransactionDetailObjectValue?.data?.transactions?.count ?? 0 > 0
                        {
                            self.noTransactionView.isHidden = true
                            self.transactionDetailView.isHidden = false
                            self.transactionDetailTableview.reloadData()
                        }else{
                            self.noTransactionView.isHidden = false
                            self.transactionDetailView.isHidden = true
                        }
                     
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }

}

//MARK: Action methods
extension PlaidTransactionViewController {
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        delegate?.didTapCloseButton(customAlertViewController: self)
    }
}
extension String {
    var toDate: Date {
        return Date.Formatter.customDate.date(from: self)!
    }
}

extension Date {
    struct Formatter {
        static let customDate: DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            return formatter
        }()
    }
    
    
}
