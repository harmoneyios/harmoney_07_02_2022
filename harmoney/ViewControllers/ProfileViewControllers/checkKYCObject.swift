//
//  checkKYCObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 12/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct checkKYCObject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : checkKYCObjectData?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }



struct checkKYCObjectData : Mappable {
    
    var message : String?
    var reference: String?
    var status: String?
    var entity_type: String?
    var success : Bool?
    var verification_status : String?
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        message          <- map["message"]
        reference           <- map["reference"]
        success            <- map["success"]
        status    <- map["status"]
        verification_status         <- map["verification_status"]
        entity_type         <- map["entity_type"]
        
    }
}
