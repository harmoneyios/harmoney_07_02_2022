//
//  getCityInformationObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 20/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct getCityInformationObject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : [getCityInformationObjectData]?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }


    struct getCityInformationObjectData : Mappable {

            var city : String?
            var value: String?
        
        init?(map: Map) {

        }
        mutating func mapping(map: Map) {
            city          <- map["city"]
            value           <- map["value"]
      
 
        }
    }



