//
//  BankDetailsAccountAddView.swift
//  Harmoney
//
//  Created by Mac on 15/07/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class BankDetailsAccountAddView: UIView {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var AddNewBtn: UIButton!
       
      var parentVC : MyPfrofileViewController?
       override func awakeFromNib() {
           super.awakeFromNib()
        
        borderView.layer.cornerRadius = 5
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 0.5)
        borderView.layer.shadowColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 0.5)
        borderView.layer.shadowOpacity = 1
        borderView.layer.shadowOffset = .zero
        borderView.layer.shadowRadius = 2
        
    }
    
//    @IBAction func maleClick(_ sender: Any) {
//        selectMaleOrfemale(isMale: true)
//
//    }
//    @IBAction func femaleClick(_ sender: Any) {
//        selectMaleOrfemale(isMale: false)
//    }
//    func selectMaleOrfemale(isMale male:Bool) {
//        if(male){
//            maleButton.backgroundColor = #colorLiteral(red: 0.1509042382, green: 0.3234898448, blue: 0.4761670828, alpha: 1)
//            maleButton.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//
//            femaleButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            femaleButton.titleLabel?.textColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 1)
//            parentVC?.profileUpdate.gender = "Male"
//        }else{
//           femaleButton.backgroundColor = #colorLiteral(red: 0.1509042382, green: 0.3234898448, blue: 0.4761670828, alpha: 1)
//           femaleButton.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//
//           maleButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//           maleButton.titleLabel?.textColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 1)
//             parentVC?.profileUpdate.gender = "FeMale"
//        }
//    }


}

