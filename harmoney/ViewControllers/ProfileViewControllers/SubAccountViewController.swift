//
//  SubAccountViewController.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 30/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ObjectMapper
import Toast_Swift
import IQKeyboardManagerSwift
import SDWebImage
import CRRefresh


protocol SubAccountViewControllerDelegate: class {
    func didTapCloseButton(customAlertViewController: SubAccountViewController)
}

class SubAccountViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var homeModel: HomeModel? {
        return HomeManager.sharedInstance.homeModel
    }
    weak var delegate: SubAccountViewControllerDelegate?
    
    @IBOutlet weak var walletHeader: UILabel!
    @IBOutlet weak var addSubWalletBtn: UIButton!
    @IBOutlet weak var newSubWalletBtn: UIButton!
    @IBOutlet weak var noWalletView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var walletListTableView: UITableView!
    @IBOutlet weak var walletListView: UIView!
    @IBOutlet weak var addWalletMainView: UIView!
    @IBOutlet weak var walletCreateMainView: UIView!
    @IBOutlet weak var WalletNameDetailView: UIView!
    @IBOutlet weak var walletNameLbl: UILabel!
    @IBOutlet weak var walletNameTxtField: UITextField!
    @IBOutlet weak var createWalletBtn: UIButton!
    @IBOutlet weak var assignToLabel: UILabel!
    var getInformationForPaymentObject : getInformationForPaymentObject?
    @IBOutlet weak var memberAddCollectionViewWidthConstrain: NSLayoutConstraint!
    @IBOutlet weak var kidsCollectionView: UICollectionView!
    @IBOutlet weak var assigntoMainBackView: UIView!
    var getAccountobj : getsilaAccountData?
    var getWalletObj : getwalletObject?
    var subAccountRegisterObjectValue: subAccountRegisterObject?
    var SubListTableViewCell = "SubListTableViewCell"
    var walletCount:Int = 0
    var selectedIndexInt: Int = -1
    var kidAmount:String = "0"
    var walletEditCreate:String = "create"
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        walletListTableView.delegate = self
        walletListTableView.dataSource = self
        // Do any additional setup after loading the view.
        walletListTableView.register(UINib.init(nibName: "SubListTableViewCell", bundle: nil), forCellReuseIdentifier: "SubListTableViewCell")
        // Do any additional setup after loading the view.
        
        kidsCollectionView.dataSource = self
        kidsCollectionView.delegate = self
        kidsCollectionView.register(UINib(nibName: ChoreHomeMemberCollectionViewCell.identifier, bundle: .main), forCellWithReuseIdentifier: ChoreHomeMemberCollectionViewCell.identifier)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInformation()
        self.getAssignedMemberdata()
//        getWallet()
    }

    private func getAssignedMemberdata() {
        PersonalFitnessManager.sharedInstance.assingToListforSubAccount { (result) in
            switch result {
            case .success(_):

                print(HarmonySingleton.shared.assigntomemberDataListForSubAccount as Any)
//                if self.walletEditCreate == "edit"
//                {
//                    if let row = self.selectedIndexPath?.row{
//                        if let kidobj = HarmonySingleton.shared.getKidWalletList?[row]
//                        {
//                            var index = -1
//                            for member in HarmonySingleton.shared.assigntomemberDataListForSubAccount! {
//                                index += 1
//                                if member.memberId == kidobj.kidGuid {
//                                    self.selectedIndexInt = index
//                                    break
//                                }
//                            }
//                        }
//                    }
//
//                }
                
//                self.choreUITable.reloadData()
                var widthForCollectionView:CGFloat = 70
                
                if HarmonySingleton.shared.assigntomemberDataListForSubAccount?.count ?? 0 > 1{
                    widthForCollectionView = CGFloat((HarmonySingleton.shared.assigntomemberDataListForSubAccount?.count ?? 0) * 50 + 20)
                }
                if widthForCollectionView > 400
                {
                    self.memberAddCollectionViewWidthConstrain.constant = 400
                }else{
                    self.memberAddCollectionViewWidthConstrain.constant = widthForCollectionView
                }
                self.kidsCollectionView.reloadData()
            case .failure(let error):
                self.toast(message: error.localizedDescription)
            }
        }
    }
    
    func getInformationforAssignedkid(){
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            var (url, method, param) = APIHandler().getInformationForPayment(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.getInformationForPaymentObject = Mapper<getInformationForPaymentObject>().map(JSON: value)
                        print(self.getInformationForPaymentObject!)
                        if self.getInformationForPaymentObject?.data?.kidWallet == nil
                        {
//                            self.walletListTableView.reloadData()
//                            self.noWalletView.isHidden = false
//                            self.walletListView.isHidden = true
//                            self.walletCreateMainView.isHidden = true
//                            self.walletHeader.text = "Wallet Details"
                            
                        }else{
                            HarmonySingleton.shared.getKidWalletList = self.getInformationForPaymentObject?.data?.kidWallet
//
                            if let objlist = HarmonySingleton.shared.getKidWalletList{
                                for obj in objlist
                                {
                                    HarmonySingleton.shared.assigntomemberDataListForSubAccount = HarmonySingleton.shared.assigntomemberDataListForSubAccount?.filter {$0.memberId != obj.kidGuid}
                                    self.kidsCollectionView.reloadData()
                                    if HarmonySingleton.shared.assigntomemberDataListForSubAccount?.count == 0 {
                                        self.walletCreateMainView.isHidden = true
                                        AlertView.shared.showAlert(view: self, title: "Add Kid", description: "Add kids to family then create sub wallet")
                                        AlertView.shared.delegate = self
                                    }else{
                                        
                                        self.noWalletView.isHidden = true
                                        self.walletListView.isHidden = true
                                        self.walletCreateMainView.isHidden = false
                                        self.walletHeader.text = "Wallet Create"
                                        self.walletNameTxtField.text = ""
                                    }
                                  

                                }
                               
                            }
                          

                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
   
  
    
    func setProfilePic(frstName: String?, lstName: String?, profileImageView: UIImageView){
     
         if let firstName = frstName, let lastName = lstName {
             profileImageView.setImageWith(firstName + " " + lastName, color: ConstantString.menuBackSystemProfilepic)
         } else {
             if let dispName = frstName {
                 profileImageView.setImageWith(dispName, color: ConstantString.menuBackSystemProfilepic)
             }
         }
     }
    @IBAction func createWalletAction(_ sender: Any) {
        

        if walletNameTxtField.text?.count == 0{
            AlertView.shared.showAlert(view: self, title: "Wallet Name", description: "Please enter your wallet name")
            return
        }
        if selectedIndexInt == -1{
            AlertView.shared.showAlert(view: self, title: "Select Kid", description: "Please select your kid")
            return
        }
        
                if Reachability.isConnectedToNetwork() {
                 SVProgressHUD.show()
                    var (url, method, param) = ("", HTTPMethod.get, [String:Any]())

                var data = [String : Any]()
                //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                    let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
                    data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
                    if let name = walletNameTxtField.text {
                        data.updateValue(getEncryptedString(plainText: "\(name.capitalized)'s Wallet") , forKey: "walletName")
                    }
                   
                    data.updateValue( getEncryptedString(plainText: HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberName ?? "No name") , forKey: "kidName")
                    data.updateValue(getEncryptedString(plainText: HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberMobile ?? "No name") , forKey: "kidMobileNumber")
                    data.updateValue(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberId ?? "No name", forKey: "kidGuid")
                    data.updateValue(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberId ?? "No name", forKey: "kidMemberId")
                    data.updateValue(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberPhoto ?? "No name", forKey: "kidImage")
                    if walletEditCreate == "create" {
                      (url, method, param) = APIHandler().subAccountRegister(params: data)
                    }else{
                        (url, method, param) = APIHandler().subAccountEdit(params: data)
                    }
              
                    AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                            SVProgressHUD.dismiss()
                            switch response.result {
                            case .success(let value):
                                if  let value = value as? [String:Any] {
                                    self.subAccountRegisterObjectValue = Mapper<subAccountRegisterObject>().map(JSON: value)
                                    if self.subAccountRegisterObjectValue?.data?.status == "SUCCESS"{
                                        self.getInformation()
                                       
                                    }else{
                                        self.view.toast("Sub Wallet Registration Failed")
                                    }
                                }
                            case .failure(let error):
                                print(error)
                            }
                    }
                }
    }
    
    func editWallet(){
        
        if walletNameTxtField.text?.count == 0{
            AlertView.shared.showAlert(view: self, title: "Wallet Name", description: "Please enter your wallet name")
            return
        }
        if selectedIndexInt == -1{
            AlertView.shared.showAlert(view: self, title: "Select Kid", description: "Please select your kid")
            return
        }
        
                if Reachability.isConnectedToNetwork() {
                 SVProgressHUD.show()
                var data = [String : Any]()
                //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                    let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
                    data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
                    if let name = walletNameTxtField.text {
                        data.updateValue("\(name.capitalized)'s Wallet", forKey: "walletName")
                    }
                   
                    data.updateValue(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberName ?? "No name", forKey: "kidName")
                    data.updateValue(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberMobile ?? "No name", forKey: "kidMobileNumber")
                    data.updateValue(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberId ?? "No name", forKey: "kidGuid")
                    data.updateValue(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberId ?? "No name", forKey: "kidMemberId")
                    data.updateValue(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[selectedIndexInt].memberPhoto ?? "No name", forKey: "kidImage")
                var (url, method, param) = APIHandler().subAccountEdit(params: data)
               
                    AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                            SVProgressHUD.dismiss()
                            switch response.result {
                            case .success(let value):
                                if  let value = value as? [String:Any] {
                                    self.subAccountRegisterObjectValue = Mapper<subAccountRegisterObject>().map(JSON: value)
                                    if self.subAccountRegisterObjectValue?.data?.status == "SUCCESS"{
                                        self.getInformation()
                                        
                                    }else{
                                        self.view.toast("Sub Wallet Registration Failed")
                                    }
                                }
                            case .failure(let error):
                                print(error)
                            }
                    }
                }
        
    }
    
    func getInformation(){
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            var (url, method, param) = APIHandler().getInformationForPayment(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.getInformationForPaymentObject = Mapper<getInformationForPaymentObject>().map(JSON: value)
                        print(self.getInformationForPaymentObject!)
                        if self.getInformationForPaymentObject?.data?.kidWallet == nil
                        {
                            self.walletListTableView.reloadData()
                            self.noWalletView.isHidden = false
                            self.walletListView.isHidden = true
                            self.walletCreateMainView.isHidden = true
                            self.walletHeader.text = "Kid's Wallet Details"
                            
                        }else{
                            HarmonySingleton.shared.getKidWalletList = self.getInformationForPaymentObject?.data?.kidWallet
                            self.noWalletView.isHidden = true
                            self.walletListView.isHidden = false
                            self.walletCreateMainView.isHidden = true
                            self.walletHeader.text = "Kid's Wallet Details"
                            self.walletListTableView.reloadData()
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func getWallet(kidGuid:String, kidwalletPrivatekey:String, cell:SubListTableViewCell){
        
        if Reachability.isConnectedToNetwork() {
//            SVProgressHUD.show()
            var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(kidwalletPrivatekey, forKey: "walletPrivateKey")
            var (url, method, param) = APIHandler().getWalletdata(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
//                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.getWalletObj = Mapper<getwalletObject>().map(JSON: value)
                        if let walletbalance = self.getWalletObj?.data?.sila_balance{
                          let visibleCells = self.walletListTableView.visibleCells
                            if visibleCells.contains(cell) {
                                cell.amountLabel.attributedText = getHBuckString(value: "\(walletbalance)")

                            }
                           
                        }
//                        self.walletListTableView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return  HarmonySingleton.shared.getKidWalletList?.count ?? 0
      
       }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
     
            return 120
        
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
    UITableViewCell {
   
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubListTableViewCell", for: indexPath) as! SubListTableViewCell
        cell.amountLabel.text = "0"
        getWallet(kidGuid: HarmonySingleton.shared.getKidWalletList?[indexPath.row].kidGuid ?? "", kidwalletPrivatekey: HarmonySingleton.shared.getKidWalletList?[indexPath.row].walletPrivateKey ?? "",cell:cell)
        if let name = HarmonySingleton.shared.getKidWalletList?[indexPath.row].walletName{
          
            let trimmedString = name.replacingOccurrences(of: " ", with: "")
            cell.walletNameLabel.text = "\(name)"
            self.setProfilePic(frstName: trimmedString, lstName: nil, profileImageView: cell.profileImage)
        }
     
//        cell.amountLabel.text = self.kidAmount
        cell.editWallet.addTarget(self, action: #selector(formInviteData(btn:)), for: .touchUpInside)
        cell.editWallet.tag = indexPath.row
            return cell

    }
    
    @objc func formInviteData(btn : UIButton)  {
   
//        if HarmonySingleton.shared.assigntomemberDataListForSubAccount?.count == 0 {
//            AlertView.shared.showAlert(view: self, title: "Add Kid", description: "Add kids to family then create sub wallet")
//            AlertView.shared.delegate = self
//        }else{
            print("Edit Action")
            noWalletView.isHidden = true
            walletListView.isHidden = true
            walletCreateMainView.isHidden = false
            walletHeader.text = "Edit Wallet"
            createWalletBtn .setTitle("Edit Wallet", for: .normal)
            walletEditCreate = "edit"
            selectedIndexInt = btn.tag
            self.getAssignedMemberdata()
//            kidsCollectionView.reloadData()
            if let name = HarmonySingleton.shared.getKidWalletList?[btn.tag].walletName{
                walletNameTxtField.text = "\(name.capitalized)"
            }
//        }
     
        
    }
    
    @IBAction func newAddSubWalletAct(_ sender: Any) {
        
        if HarmonySingleton.shared.assigntomemberDataListForSubAccount?.count == 0 {
            AlertView.shared.showAlert(view: self, title: "Add Kid", description: "Add kids to family then create sub wallet")
            AlertView.shared.delegate = self
        }else{
            

            noWalletView.isHidden = true
            walletListView.isHidden = true
            walletCreateMainView.isHidden = false
            walletHeader.text = "Create Wallet"
            createWalletBtn .setTitle("Create Wallet", for: .normal)
            walletNameTxtField.text = ""
            walletEditCreate = "create"
        }
   
    
    }
    
    
    @IBAction func addSubWalletAct(_ sender: Any) {
        self.getInformationforAssignedkid()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK: Action methods
extension SubAccountViewController {
    @IBAction func closeSubAccountButtonTapped(_ sender: UIButton) {
        delegate?.didTapCloseButton(customAlertViewController: self)
    }
}


extension SubAccountViewController: UICollectionViewDelegate {
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        let width = (self.frame.size.width - 12 * 3) / 3 //some width
//        let height = width * 1.5 //ratio
//        return CGSize(width: width, height: height)
//    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let isSelectedAvathar = !(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[indexPath.row].isSelectAvathar ?? false)
//        HarmonySingleton.shared.assigntomemberDataListForSubAccount?[indexPath.row].isSelectAvathar = isSelectedAvathar
//
////        collectionView.reloadItems(at: [indexPath])
//        collectionView.reloadData()
//    }
//}

func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    let cell = collectionView.cellForItem(at: indexPath) as! ChoreHomeMemberCollectionViewCell

    cell.tickIcon.isHidden = false
    selectedIndexInt = indexPath.row

}

func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    let cell: ChoreHomeMemberCollectionViewCell = collectionView.cellForItem(at: indexPath) as! ChoreHomeMemberCollectionViewCell

    cell.tickIcon.isHidden = true
    selectedIndexInt = -1
}
}

extension SubAccountViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return HarmonySingleton.shared.assigntomemberDataListForSubAccount?.count ?? 0
//        return 2
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChoreHomeMemberCollectionViewCell", for: indexPath) as! ChoreHomeMemberCollectionViewCell
       
        cell.memberName.text = HarmonySingleton.shared.assigntomemberDataListForSubAccount?[indexPath.row].memberName ?? "No name"
        
        if HarmonySingleton.shared.assigntomemberDataListForSubAccount?[indexPath.row].memberPhoto == "" {
            self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: cell.memberImageView)
        } else {
            cell.memberImageView.imageFromURL(urlString: HarmonySingleton.shared.assigntomemberDataListForSubAccount?[indexPath.row].memberPhoto ?? "")
        }
//       cell.tickIcon.isHidden = true
//
//        if HarmonySingleton.shared.assigntomemberDataListForSubAccount?.count == 1{
//
//            cell.tickIcon.isHidden = false
//        }else{
//            cell.tickIcon.isHidden = !(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[indexPath.row].isSelectAvathar ?? false)
//
//        }
        
//        cell.tickIcon.isHidden = !(HarmonySingleton.shared.assigntomemberDataListForSubAccount?[indexPath.row].isSelectAvathar ?? false)

        if HarmonySingleton.shared.assigntomemberDataListForSubAccount?.count == 1
        {
            cell.tickIcon.isHidden = false
            selectedIndexInt = indexPath.row
        }else{
            if selectedIndexInt == indexPath.row  {
                cell.tickIcon.isHidden = false
                selectedIndexInt = indexPath.row

            }else{

                cell.tickIcon.isHidden = true

                
            }
        }
   

        
        return cell
        
    }
    
    
   
}


//MARK: UITextFieldDelegate Methods
extension SubAccountViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == walletNameTxtField {
            guard let textFieldText = walletNameTxtField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 50
        }
     
        return true
    }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
extension SubAccountViewController : AlertCallBack{
    func clickedOnOk() {
        self.dismiss(animated: true, completion: nil)
    }
}
