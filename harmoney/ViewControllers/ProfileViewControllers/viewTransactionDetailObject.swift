//
//  viewTransactionDetailObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 18/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct viewTransactionDetailObject: Mappable {
       
        var status : Int32?
        var data : viewTransactionDetailObjectData?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            data    <- map["data"]
        }

    }


    struct viewTransactionDetailObjectData : Mappable {

         
            var success: Bool?
            var transactions : [transactionsData]?
     

        
        
        init?(map: Map) {

        }
        mutating func mapping(map: Map) {
      
            success      <- map["success"]
            transactions         <- map["transactions"]
 
        }
    }


struct transactionsData : Mappable {
    var transaction_type : String?
    var sila_amount : Int?
    var status: String?
    var created: String?

    
    
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        transaction_type          <- map["transaction_type"]
        sila_amount           <- map["sila_amount"]
        status            <- map["status"]
        created    <- map["created"]
    }
}
