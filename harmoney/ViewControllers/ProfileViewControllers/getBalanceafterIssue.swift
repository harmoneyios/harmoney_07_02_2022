//
//  getBalanceafterIssue.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 13/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct getBalanceafterIssue: Mappable {
       
        var status : Int32?
        var data : getBalanceafterIssuedata?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            data    <- map["data"]
        }

    }



struct getBalanceafterIssuedata : Mappable {
    

    var available_balance: Int?
    var current_balance: Int?
    var routing_number: String?
    var masked_account_number : String?
    var account_name : String?
    var success : Bool?
    var status : String?

    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        available_balance          <- map["available_balance"]
        current_balance           <- map["current_balance"]
        routing_number            <- map["routing_number"]
        masked_account_number    <- map["masked_account_number"]
        account_name    <- map["account_name"]
        success         <- map["success"]
        status    <- map["status"]
      
    }
}
