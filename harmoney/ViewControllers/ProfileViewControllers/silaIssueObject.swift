//
//  silaIssueObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 13/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct silaIssueObject: Mappable {
       
        var status : Int32?
        var data : silaIssueObjectdata?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            data    <- map["data"]
        }

    }


struct silaIssueObjectdata : Mappable {
    

    var message: String?
    var reference: String?
    var transaction_id : String?
    var descriptor : String?
    var success : Bool?
    var status : String?

    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        message          <- map["message"]
        reference           <- map["reference"]
        transaction_id            <- map["transaction_id"]
        descriptor    <- map["descriptor"]
        success         <- map["success"]
        status    <- map["status"]
      
    }
}
