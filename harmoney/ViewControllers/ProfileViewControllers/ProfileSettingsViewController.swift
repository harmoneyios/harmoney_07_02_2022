//
//  SettingsViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import HealthKit
import CoreMotion
import Alamofire

class ProfileSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var settingsTableView: UITableView!
        var cellIdentifier = "FyiTitleTableViewCell"
        var fyiDataIdentifier = "FyiDataTableViewCell"
        var logoutIndentifier = "LogoutTableViewCell"
        let urlArray = ["http://harmoney.ai/app-pages/about-harmoney.html",
                        "http://harmoney.ai/app-pages/privacypolicy.html", "http://harmoney.ai/app-pages/termsandconditions.html"]
        var arrayList = ["About Harmoney","Privacy Policy","Terms & Conditions","Logout"]
    
        private(set) var configdatas: ConfigModel?
 
        override func viewDidLoad() {
            super.viewDidLoad()
            self.callConfig()
            settingsTableView.delegate = self
            settingsTableView.dataSource = self
            self.settingsTableView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9647058824, blue: 0.9843137255, alpha: 1)
//            let dictionary = Bundle.main.infoDictionary!
//            let version = dictionary["CFBundleShortVersionString"] as! String
            
//            versionLabel.text = "Version:\(version)"

        }
    func gotoWebView(name: String, url:String)  {
        let vc = GlobalStoryBoard().localWebVC
        vc.titletext = name
        vc.websiteUrl = url
        self.present(vc, animated: true, completion: nil)

    }
            
    @objc func pushNotificationsStatus(sender:UISwitch) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings { (settings) in
                let swit = settings.notificationCenterSetting == .enabled ? true : false
                DispatchQueue.main.async {
                    sender.setOn(swit, animated: true)
                    sender.addTarget(self, action: #selector(self.gotoAppSettings), for: .valueChanged)
                }
            }
        }
    
    @objc func openSettinsForMotionFitness(sender:UISwitch) {
        DispatchQueue.main.async {
            let status = CMMotionActivityManager.authorizationStatus()
            sender.setOn(status == .authorized ? true : false, animated: true)
           sender.addTarget(self, action: #selector(self.gotoAppSettings), for: .valueChanged)
        }
    }
    
//    @objc func checkHealthKitPermissions(sender:UISwitch){
//
//        DispatchQueue.main.async {
//            let status = HKHealthStore().authorizationStatus(for: HKSampleType.workoutType())
//            sender.setOn(status == .sharingAuthorized ? true : false, animated: true)
//           sender.addTarget(self, action: #selector(self.gotoAppSettings), for: .valueChanged)
//        }
//
//    }
    
    func callConfig()  {
        if Reachability.isConnectedToNetwork() {
        let (url, method, param) = APIHandler().getConfig(params: [:])
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                    
                    switch response.result {
                       case .success(let value):
                        if let value = value as? [String:Any] {
                            let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                            self.configdatas = try? JSONDecoder().decode(ConfigModel.self, from: jsonData!)
                            HarmonySingleton.shared.cofigModel = self.configdatas
                            let versionStr = self.configdatas?.data.appUpdate.version
//                            self.versionLabel.text = versionStr
                        }
                        break
                        case .failure(let error):
                        break
                       }
            }
        }
    }
    
        
    @objc func gotoAppSettings()  {
        guard let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url) else {
                return
        }
        let optionsKeyDictionary = [UIApplication.OpenExternalURLOptionsKey(rawValue: "universalLinksOnly"): NSNumber(value: true)]

        UIApplication.shared.open(url, options: optionsKeyDictionary, completionHandler: nil)

    }
    
        func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              
           9
              
           }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 3{
           return 0//Choose your custom row height
        }else if indexPath.row == 1 || indexPath.row == 2 {
            return 0
        }else if indexPath.row == 4{
            return 50
        }
        else if indexPath.row == 8{
            return 80
        }
            return 45
    }
           
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: fyiDataIdentifier, for: indexPath) as! FyiDataTableViewCell
            cell.fyiNameLbl.text = "Allow health kit tracking"
            cell.fyiTextView.text = "Use phone sensors to track steps, miles and laps"
//            self.checkHealthKitPermissions(sender: cell.motionSwitch)
                return cell
            }
                else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: fyiDataIdentifier, for: indexPath) as! FyiDataTableViewCell
                cell.fyiNameLbl.text = "Allow motion and fitness tracking"
                cell.fyiTextView.text = "Use motion and fitness to monitor steps and fitness"
            self.openSettinsForMotionFitness(sender: cell.motionSwitch)
                return cell
                }
            else if indexPath.row == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: logoutIndentifier, for: indexPath) as! LogoutTableViewCell
           // let cell = tableView.dequeueReusableCell(withIdentifier: logoutIndentifier, for: indexPath) a
            let dictionary = Bundle.main.infoDictionary!
            let version = dictionary["CFBundleShortVersionString"] as! String
              cell.logoutLbl.text = "Logout"
            cell.logoutLbl.font = ConstantString.intFieldFont
            cell.appVersionLbl.text = "App Version:\(version)"
            return cell
             }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FyiTitleTableViewCell
            
            
                       if indexPath.row == 0 {
                            // cell.cellUiData(dict: (self.localJson.feed?.results![indexPath.row])!)
                           cell.fyiTitleLbl.font = ConstantString.labelTitileFont
                           cell.fyiTitleLbl.text = "Tracking Preferences"
                           cell.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9647058824, blue: 0.9843137255, alpha: 1)
                        cell.fyiTitleLbl.textColor = #colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1)
                           cell.fyiTitleSwitch.isHidden = true
                    
                       }
                       if indexPath.row == 3 {
                          cell.fyiTitleLbl.font = ConstantString.labelTitileFont
                           cell.fyiTitleLbl.text = "Notifications"
                        cell.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9647058824, blue: 0.9843137255, alpha: 1)
                        cell.fyiTitleLbl.textColor = #colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1)
                           cell.fyiTitleSwitch.isHidden = true
                        
                       }
                  if indexPath.row == 4 {
                    cell.fyiTitleLbl.font = ConstantString.labelTitileFont
                    cell.fyiTitleLbl.text = "Allow notifications"
                    self.pushNotificationsStatus(sender: cell.fyiTitleSwitch)
                    let vw = UIView.init(frame: CGRect.init(x: 0, y: cell.contentView.frame.size.height-6, width: cell.contentView.frame.size.width, height: 6))
                   vw.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9490196078, blue: 0.9647058824, alpha: 1)
                   cell.contentView.addSubview(vw)

                 }
            if indexPath.row == 5 {
            
            cell.fyiTitleLbl.text = "About Us"
            cell.fyiTitleLbl.font = ConstantString.intFieldFont
              cell.fyiTitleSwitch.isHidden = true
                let vw = UIView.init(frame: CGRect.init(x: 0, y: cell.contentView.frame.size.height-6, width: cell.contentView.frame.size.width, height: 1))
                vw.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.8823529412, blue: 0.9450980392, alpha: 1)
                cell.contentView.addSubview(vw)
                
             }
            if indexPath.row == 6 {
             
                cell.fyiTitleLbl.text = "Privacy Policy"
                cell.fyiTitleLbl.font = ConstantString.intFieldFont
               cell.fyiTitleSwitch.isHidden = true
                let vw = UIView.init(frame: CGRect.init(x: 0, y: cell.contentView.frame.size.height-6, width: cell.contentView.frame.size.width, height: 1))
                vw.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.8823529412, blue: 0.9450980392, alpha: 1)
                cell.contentView.addSubview(vw)
              }
            if indexPath.row == 7 {
            
            cell.fyiTitleLbl.text = "Terms & Conditions"
                cell.fyiTitleLbl.font = ConstantString.intFieldFont
              cell.fyiTitleSwitch.isHidden = true
                let vw = UIView.init(frame: CGRect.init(x: 0, y: cell.contentView.frame.size.height-6, width: cell.contentView.frame.size.width, height: 1))
                vw.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.8823529412, blue: 0.9450980392, alpha: 1)
                cell.contentView.addSubview(vw)
             }
             return cell
            
            }
    }

            // we know that cell is not empty now so we use ! to force unwrapping but you could also define cell as
            // let cell = (tableView.dequeue... as? UITableViewCell) ?? UITableViewCell(style: ...)

           
             
           
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    if indexPath.item == 8 {
        let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to logout?", preferredStyle: .alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            UserDefaultConstants().clearDefaults()
            PersonalFitnessManager.sharedInstance.personalChallengeList.removeAll()
              UserDefaults.standard.set(false, forKey: "login")
            DailyChallengeManager.sharedInstance.onboardingChallenges.removeAll()
            DailyChallengeManager.sharedInstance.dailyChallenges.removeAll()
            HarmonySingleton.shared.dashBoardData = nil
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.initialView()
            }))
       // refreshAlert.addAction(OKAction)
        present(refreshAlert, animated: true, completion: nil)

        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
        //          print("Handle Cancel Logic here")
        }))
    }else if (indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7){
        let myindex = indexPath.row - 5
        gotoWebView(name: arrayList[myindex], url: urlArray[myindex])
        }

}
        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */
/*
 {
     let urlString = self.urlArray[indexPath.row]
     if let url = URL(string: urlString)
     {
         UIApplication.shared.openURL(url)
     }
 }
 */


  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
