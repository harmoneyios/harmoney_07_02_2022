//
//  ThumbsAlertViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/27/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol ThumbsAlertViewControllerDelegate: class {
    func didTapThumbsUpButton(thumbsAlertViewController: ThumbsAlertViewController)
    func didTapThumbsDownButton(thumbsAlertViewController: ThumbsAlertViewController)
}

class ThumbsAlertViewController: UIViewController {
    weak var delegate: ThumbsAlertViewControllerDelegate?
    var alertTitle: String?
    var buttonTitle : String = "No"
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var btnNoOutlet: UIButton!
    
    //MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = alertTitle
        btnNoOutlet.setTitle(buttonTitle, for: .normal)
    }
}

//MARK: Action methods
extension ThumbsAlertViewController {
    @IBAction func thumbsDownButtonTapped(_ sender: UIButton) {
        delegate?.didTapThumbsDownButton(thumbsAlertViewController: self)
    }
    
    @IBAction func thumbsUpButtonTapped(_ sender: UIButton) {
        delegate?.didTapThumbsUpButton(thumbsAlertViewController: self)
    }
}
