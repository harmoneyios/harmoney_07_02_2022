//
//  subAccountRegisterObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 01/09/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct subAccountRegisterObject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : subAccountRegisterObjectData?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }



struct subAccountRegisterObjectData : Mappable {
    
    var message : String?
    var reference: String?
    var status: String?
    var wallet_nickname: String?
    var success : Bool?
    var queued_for_whitelist : Bool?
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        message          <- map["message"]
        reference           <- map["reference"]
        success            <- map["success"]
        status    <- map["status"]
        queued_for_whitelist         <- map["verification_status"]
        wallet_nickname         <- map["entity_type"]
        
    }
}
