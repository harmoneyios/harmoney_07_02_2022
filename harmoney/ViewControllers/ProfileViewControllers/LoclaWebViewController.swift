//
//  LoclaWebViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 24/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import WebKit

class LoclaWebViewController: UIViewController {
@IBOutlet weak var localWebView: WKWebView!
@IBOutlet weak var backClick: UIButton!
@IBOutlet weak var titleLabel
    : UILabel!
    @IBOutlet weak var backButton: UIButton!

    @IBOutlet weak var imgVwTitleLogo: UIImageView!
    var websiteUrl : String?
    var titletext : String?
    var push  = false

    override func viewDidLoad() {
        super.viewDidLoad()

        localWebView.navigationDelegate = self
        // Do any additional setup after loading the view.
        let link = URL(string:websiteUrl ?? "" )!
        let request = URLRequest(url: link)
        imgVwTitleLogo.isHidden = true
        titleLabel.isHidden = false
        if titletext == "HARMONEY" || titletext == "My Charity"{
            //titleLabel.attributedText = getHBuckString(value: titletext)
            imgVwTitleLogo.isHidden = false
            titleLabel.isHidden = true
        }else{
            titleLabel.text = titletext
        }
        
        localWebView.load(request)
        
        if push == true {
            backClick.isHidden = true
            backButton.isHidden = false

        } else {
            backClick.isHidden = false
            backButton.isHidden = true
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideLoader()
        if titletext == "HFL" {
            self.dismiss(animated: false, completion: nil)
        }
       
    }
    @IBAction func backCLick(_ sender: Any) {
        
        if push{
            self.navigationController?.popViewController(animated: true)
        }else {
            if titletext == "HFL" {
                self.didTapCloseHFLView()
            } else {
                self.dismiss(animated: true, completion: nil)

            }
        }
        if inputViewController == presentedViewController{
            self.dismiss(animated: true, completion: nil)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func backButtonAction(_ sender: Any) {
//        if push{
//            self.navigationController?.popViewController(animated: true)
//        }else {
//            self.dismiss(animated: true, completion: nil)
//        }
    }
   
    func didTapCloseHFLView() {
        let removeCouponViewController = GlobalStoryBoard().removeCouponVC
         removeCouponViewController.delegate = self
         removeCouponViewController.imageString = ""
        removeCouponViewController.titleString = "Are you sure you want to exit from Kindness League?"
        removeCouponViewController.descriptionString = "                                                            "
        self.present(removeCouponViewController, animated: true, completion: nil)

    }

}
extension LoclaWebViewController : WKNavigationDelegate,WKUIDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideLoader()
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showLoader()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        hideLoader()
    }
}

extension LoclaWebViewController : RemoveCouponViewControllerDelegate{
    func didTapThumbsDownButton(deleteChallangeViewController: RemoveCouponAlertViewController) {
        deleteChallangeViewController.dismiss(animated: true, completion: nil)

    }
    
    func didTapThumbsUpButton(deleteChallangeViewController: RemoveCouponAlertViewController, memberId: String) {
        deleteChallangeViewController.dismiss(animated: true, completion: {
            self.dismiss(animated: true, completion: nil)

        })

    }
    
}
