//
//  DateofBirthTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 12/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class DateofBirthTableViewCell: UITableViewCell {
    
    
    var profileUpdate = ProfileUpdateObject()
    var parentVc : EditSettingsViewController?

    @IBOutlet weak var dateTextField: UITextField!
    let datePickerView = UIDatePicker()
    var stringDate : String?{
        didSet{
            if self.stringDate != nil && self.stringDate?.count ?? 0 > 0 {
                self.splitDateToTF()
            }
        }
    }
   // let datePicker = UIDatePicker()
    
   
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        
       //showDatePicker()
        //monthTextField.inputView = datePicker
//        monthTextField.addTarget(self, action: #selector(dp(_:)), for: .allEvents)
//        dateTextField.addTarget(self, action: #selector(dp(_:)), for: .allEvents)
//        yearTextField.addTarget(self, action: #selector(dp(_:)), for: .allEvents)
        datePickerView.datePickerMode = .date
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        var components = DateComponents()
        components.year = -100
        var componentsMax = DateComponents()
        componentsMax.year = -18
        componentsMax.day = -1
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        let maxDate = Calendar.current.date(byAdding: componentsMax, to: Date())
        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = maxDate
        if #available(iOS 13.4, *) {
            datePickerView.preferredDatePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        dateTextField.inputView = datePickerView
        
        self.dateTextField.tintColor = .clear
        
    }
   

    func setUI() {
        dateTextField.font = ConstantString.editTextFont

        
    }


       @objc func handleDatePicker(sender: UIDatePicker) {
            let fulDate = DateFormatter()
            fulDate.dateFormat = "yyyy-MM-dd"
            stringDate = fulDate.string(from: sender.date)
        dateTextField.text = stringDate
       }
    
    func splitDateToTF()  {
        
//        let arr : [String] = (stringDate?.components(separatedBy: " "))!
//        monthTextField.text = arr[0]
//        yearTextField.text = arr[2]
//        dateTextField.text = arr[1]
//        parentVc?.profileUpdate.dateOfBirth = stringDate
    }
   
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
