//
//  getPlaidTokenObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 13/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct getPlaidTokenObject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : plaidTokenData?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }


struct plaidTokenData : Mappable {
    
    var message : String?
    var status: String?
    var link_token: String?
    var success : Bool?
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        message          <- map["message"]
        success            <- map["success"]
        status    <- map["status"]
        link_token         <- map["link_token"]
        
    }
}
