//
//  MyPfrofileViewController.swift
//  Harmoney
//


//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ObjectMapper
import Toast_Swift
import IQKeyboardManagerSwift
import SDWebImage

class ProfileUpdateObject{
    
    
    var name:String?
    var lastName:String?
    var mobileNumber:String?
    var zip:String?
    var dateOfBirth:String?
    var dateOfBirthForSila:String?
    var address:String?
    var city:String?
    var state:String?
    var ssn:String?
    var email:String?
//    var age:String?
    var contact:String?
    var gender:String?
    var emailID:String?
    var bankName:String?
    var profileImage:UIImage?
    var profileImageStr : String?
    var bankAccount:String?
    var routingNumber:String?
    var formFields : Array<Any>?
    var parentLinkPayment: Bool?
    var addressAlias:String?
    var address2:String?
    var country:String?
    var contactAlias:String?
    var cryptoAlias:String?
    var cryptoAddress:String?
    var type:String?
    var stateCode:String?

}

class MyPfrofileViewController: UIViewController, UITableViewDataSource,UITableViewDelegate,BankDetailAccountAddViewControllerDelegate,TermsAndConditionsUpdateDelegate{
    @IBOutlet weak var shadowLayer: UIView!
        @IBOutlet weak var imageTableView: UITableView!
        @IBOutlet weak var settingsTableView: UITableView!
        @IBOutlet weak var editBarView: UIView!
        
        @IBOutlet weak var editProlieBtn: UIButton!
        @IBOutlet weak var editContainerView: UIView!
        @IBOutlet weak var checkBalance: UIButton!
        @IBOutlet weak var walletbalanceLbl: UILabel!
        @IBOutlet weak var walletNameLbl: UILabel!
        @IBOutlet weak var walletDetailView: UIView!
        @IBOutlet weak var bankDetailsAccountAddView: BankDetailsAccountAddView!
        @IBOutlet weak var bankDetailsAccountEditView: BankDetailsAccountEditView!
    
    @IBOutlet weak var btnAddLithicBank: UIButton!
    @IBOutlet weak var addNewBankBtnHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var addLithicbankHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var noAccountLabel: UILabel!
        @IBOutlet weak var addnewBankBtn: UIButton!
        var profileUpdate = ProfileUpdateObject()
      var profileObj : ProfileObject?
        var getWalletObj : getwalletObject?
       var cellIdentifier = "ProfileImgTableViewCell"
       var profileCellIndetifier = "ProfileTableViewCell"
        var toView:String = "INFO"
       var saveCellIdentifier = "SaveTableViewCell"
        var editProfileVC : EditSettingsViewController?
        var getInformationForPaymentObject : getInformationForPaymentObject?
        var data = [String : Any]()
        var parentLinkPaymentUpdated = false
    @IBOutlet weak var btnCheckBox: UIButton!

    @IBOutlet weak var termAndConditionLabel: UILabel!
    var isUserTappedTermsAndConditions = false
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
        override func viewDidLoad() {
            settingsTableView.delegate = self
            settingsTableView.dataSource = self
            super.viewDidLoad()
            self .setupInitial()
            IQKeyboardManager.shared.enable = true

           // NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
            //      NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
            
            shadowLayer.layer.cornerRadius = 5
            shadowLayer.layer.borderWidth = 1
            shadowLayer.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 0.5)
            shadowLayer.layer.shadowColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 0.5)
            shadowLayer.layer.shadowOpacity = 1
            shadowLayer.layer.shadowOffset = .zero
            shadowLayer.layer.shadowRadius = 2

            getUserProfile()
            editContainerView.isHidden = true
            walletbalanceLbl.attributedText = getHBuckString(value:"0")
          
           
        }
    
    @objc func tappedLabel(tapGestureRecognizer: UITapGestureRecognizer) {
        self.isUserTappedTermsAndConditions = true
        self.navigateToTermandCondition()
    }
    
    func navigateToTermandCondition() {
     
        let terms = GlobalStoryBoard().termsVC
        terms.isUserTappedTermsAndConditions = self.isUserTappedTermsAndConditions
        terms.modalPresentationStyle = .fullScreen
        
        terms.delegate = self as TermsAndConditionsUpdateDelegate
        self.present(terms, animated: true, completion: nil)
        self.isUserTappedTermsAndConditions = false
    }
    func setupInitial() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedLabel(tapGestureRecognizer:)))
        termAndConditionLabel.isUserInteractionEnabled = true
        termAndConditionLabel.addGestureRecognizer(tapGesture)
        
       
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: "terms and conditions", attributes: underlineAttribute)
       termAndConditionLabel.attributedText = underlineAttributedString
       
        
     
        
    }
    func updateTermsAndConditions() {
    
       // self.otpPage()
    }
    @IBAction func onClickBtnChkboc(_ sender: Any) {
        btnCheckBox.isSelected.toggle()
        if btnCheckBox.isSelected == true{
            addnewBankBtn.isEnabled = true
            addnewBankBtn.backgroundColor = ConstantString.buttonBackgroundColor
        }
        if btnCheckBox.isSelected == false
        {
            addnewBankBtn.isEnabled = false
            addnewBankBtn.backgroundColor = UIColor.systemGray
        }
    }
    
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.navigationController?.isNavigationBarHidden = true
            self.btnAddLithicBank.isHidden = true
            hideAccountScreens()
            if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
                
                self.termAndConditionLabel.isHidden = true
                self.btnCheckBox.isHidden = true

                addNewBankBtnHeightConstrain.constant = 0
                addnewBankBtn.isHidden = true
                    self.noAccountLabel.isHidden = true
                    self.noAccountLabel.text = ""
                    self.walletDetailView.isHidden = false
            }else{
              
                addNewBankBtnHeightConstrain.constant = 35
                addnewBankBtn.isHidden = false
                getInformation()

            }
            getWallet()
        }
        func hideAccountScreens()  {
            //suba Start
            // bank account feature tempararly hide
           bankDetailsAccountAddView.isHidden = false
    //        bankDetailsAccountEditView.isHidden = true
            // suba end
//            if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
               bankDetailsAccountAddView.isHidden = false
//                bankDetailsAccountEditView.isHidden = true
//            }
        }
        
        func updateUIAfterEdit()  {
            getUserProfile()
            settingsTableView.reloadData()
            imageTableView.reloadData()
        }
        
        @IBAction func backtoProfileClick(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
      
        
        @IBAction func addAccountData(_ sender: Any) {
    ////        self.view.toast(notifications.underProgress)
    //        /*
    //        let bankaccountLinkVC = GlobalStoryBoard().BankDetailAccountAddVC
    //        bankaccountLinkVC.delegate = self
    //        self.navigationController?.pushViewController(bankaccountLinkVC, animated: false) */
    //
    //        let couponDetails = GlobalStoryBoard().myFriendsVC
    //
    ////                 couponDetails.purchasedCoupon = purchasedCouponList?.data[indexPath.row]
    ////                 couponDetails.isPurchased = true
    ////            self.isPurchased = true
    ////            self.orderId = purchasedCouponList?.data[indexPath.row].orderID ?? ""
    ////            presentRemoveCouponViewController(imageUrl: purchasedCouponList?.data[indexPath.row].productList.logoURL ?? "")
    //        self.navigationController?.pushViewController(couponDetails, animated: false)
    ////            self.present(couponDetails, animated: true, completion: nil)
    //        self.paymentCheckHandle()
            
            if toView == "INFO"{
                self.paymentCheckHandle()
            }else if toView == "KYC"{
                let couponDetails = GlobalStoryBoard().myFriendsVC
                couponDetails.showView = "KYC"
                self.navigationController?.pushViewController(couponDetails, animated: false)
            }else if toView == "WALLET"{
                let couponDetails = GlobalStoryBoard().myFriendsVC
                couponDetails.showView = "WALLET"
                self.navigationController?.pushViewController(couponDetails, animated: false)
            }
       
        }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              
           // return self.arrayList.count
            if tableView == imageTableView{
                return 1
            }else{
                return 8
            }
              
           }
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if tableView == imageTableView{
                return 125
            }
            return 375/7
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
            UITableViewCell {
            if indexPath.row == 0 && tableView == imageTableView  {
              let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ProfileImgTableViewCell
                cell.editButton.addTarget(self, action: #selector(editBtnAction(_:)), for: .touchUpInside)
                cell.profileImgView.tag = 200
                if profileUpdate.profileImageStr == "" || profileUpdate.profileImageStr == nil{
                    self.setProfilePic(frstName: profileUpdate.name, lstName: nil, profileImageView: cell.profileImgView)
                }else {
                    cell.profileImgView!.sd_setImage(with: URL(string: profileUpdate.profileImageStr ?? ""), placeholderImage: UIImage.init(named: "useravatar"), options: SDWebImageOptions.init(), context: nil)
                }
                cell.parentVC = self
                return cell
              }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: profileCellIndetifier, for: indexPath) as! ProfileTableViewCell
                    cell.parentVC = self
                cell.borderView.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.8823529412, blue: 0.9450980392, alpha: 1)
                //cell.backgroundView.
    //            cell.layer.masksToBounds = true
    //            cell.layer.cornerRadius = 5
    //            cell.layer.borderWidth = 0.3
    //            cell.layer.shadowOffset = CGSize(width: -1, height: 1)
    //            cell.layer.borderColor = UIColor.lightGray.cgColor
               
                    cell.customTextField.tag = indexPath.row + 100
                   // cell.customView.isHidden = true
                   // cell.customTextField.delegate = self
                  //   cell.customTextField.addTarget(self, action: #selector(updateValuesOnUpdateProfileObject(textField:)), for: .allEvents)
                    //                if(isProfileEditing){
                    //                    cell.enableEditProfileUI()
                
                    if indexPath.row == 0 {
                        cell.customLbl.text = ConstantString.firstName
                        cell.customTextField.text = profileUpdate.name
                        self.walletNameLbl.text = profileUpdate.name
                    }
                    if indexPath.row == 1{
                        cell.customLbl.text = ConstantString.lastName
                        cell.customTextField.text = profileUpdate.lastName
                    }
                    if indexPath.row == 2{
                        cell.customLbl.text = ConstantString.mobileNumber
                        cell.customTextField.text = profileUpdate.contact
                    }
                    if indexPath.row == 3  {
                        cell.customLbl.text = ConstantString.zipCode
                        cell.customTextField.text = profileUpdate.zip
                    }
                    if indexPath.row == 4  {
                    cell.customLbl.text = ConstantString.dateofBirth
                    cell.customTextField.text = profileUpdate.dateOfBirth
                        
                    
                        
                    }
                    if indexPath.row == 5  {
                        
                        cell.customLbl.text = ConstantString.emailID
                        cell.customTextField.text = profileUpdate.email
                    }
    //                if indexPath.row == 6  {
    //                    cell.customLbl.text = ConstantString.age
    //                    cell.customTextField.text = profileUpdate.age
    //
    //                  //  cell.customView.isHidden = false
    //                }
                    if indexPath.row == 6  {
                       cell.customLbl.text = ConstantString.gender
                        cell.customTextField.text = profileUpdate.gender
                        cell.borderView.isHidden = true
                    }
                    
                        if indexPath.row == 7  {
                            cell.customLbl.text = "Link Bank"
                            cell.customTextField.text = profileUpdate.bankName
                            cell.borderView.isHidden = true
                        }
                
                
                return cell
                }
        
        }
                


    func getUserProfile(){
    
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            let data = [String : Any]()
            var (url, method, param) = APIHandler().getProfile(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            
            let header: HTTPHeaders = [
                "sessionToken": UserDefaultConstants().sessionToken ?? ""
            ]
            print(url)
            AF.request(url, method: method, parameters: param,headers: header).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                print(response.result)
                switch response.result {
                    
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.profileObj =  Mapper<ProfileObject>().map(JSON: value)
                        print(self.profileObj!)
                        print(value)
                        //self.settingsTableView.reloadData()
                        self.fillUpdateProfileObjectFirstTime(obj: self.profileObj!)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
        
        func paymentCheckHandle(){
            
            if Reachability.isConnectedToNetwork() {
                SVProgressHUD.show()
                var data = [String : Any]()
                //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                let (url, method, param) = APIHandler().paymentUserCheckHandle(params: data)
                
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success:
                        let couponDetails = GlobalStoryBoard().myFriendsVC
                        couponDetails.showView = "INFO"
                        couponDetails.profileUpdate = self.profileUpdate
                        self.navigationController?.pushViewController(couponDetails, animated: false)
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
        
    
    func getInformation(){
            
            if Reachability.isConnectedToNetwork() {
                SVProgressHUD.show()
                var data = [String : Any]()
                //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                let (url, method, param) = APIHandler().getInformationForPayment(params: data)
                
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.getInformationForPaymentObject = Mapper<getInformationForPaymentObject>().map(JSON: value)
                            print(self.getInformationForPaymentObject!)
                            if (self.getInformationForPaymentObject?.data?.user == nil || self.getInformationForPaymentObject?.data?.user?.walletPrivateKey == nil)
                            {
                                self.addnewBankBtn.isEnabled = false
                                self.addnewBankBtn.backgroundColor = UIColor.systemGray
                                self.addnewBankBtn .setTitle("Create Harmoney Wallet", for: .normal)
                                self.noAccountLabel.isHidden = false
                                self.noAccountLabel.text = "No bank account or credit card added"
                                self.walletDetailView.isHidden = true
                                self.toView = "INFO"
                            }else{
                                    UserDefaults.standard.set(self.getInformationForPaymentObject?.data?.user?.cryptoAddress, forKey: "cryptoAddress")
                                    UserDefaults.standard.set(self.getInformationForPaymentObject?.data?.user?.walletPrivateKey, forKey: "walletPrivateKey")
                                    self.walletNameLbl.text = self.getInformationForPaymentObject?.data?.user?.cryptoAlias

                                    if self.getInformationForPaymentObject?.data?.user?.processStage == 1{
                                        self.addnewBankBtn.isEnabled = false
                                        self.addnewBankBtn.backgroundColor = UIColor.systemGray
                                        self.addnewBankBtn .setTitle("Create Harmoney Wallet", for: .normal)
                                        self.noAccountLabel.isHidden = false
                                        self.noAccountLabel.text = "No bank account or credit card added"
                                        self.walletDetailView.isHidden = true
                                        self.toView = "INFO"
                                    }
                                    else if (self.getInformationForPaymentObject?.data?.user?.processStage == 2 || self.getInformationForPaymentObject?.data?.user?.processStage == 3){
                                        self.toView = "KYC"
                                        self.noAccountLabel.isHidden = true
                                        self.noAccountLabel.text = ""
                                        self.walletDetailView.isHidden = true
                                        self.addnewBankBtn .setTitle("View KYC", for: .normal)
                                        self.termAndConditionLabel.isHidden = true
                                        self.btnCheckBox.isHidden = true
                                        self.addnewBankBtn.backgroundColor = ConstantString.buttonBackgroundColor

                                    }else{
                                        self.toView = "WALLET"
                                        self.noAccountLabel.isHidden = true
                                        self.noAccountLabel.text = ""
                                        self.walletDetailView.isHidden = false
                                        self.addnewBankBtn .setTitle("View Wallet", for: .normal)
                                        self.termAndConditionLabel.isHidden = true
                                        self.btnCheckBox.isHidden = true
                                        self.addnewBankBtn.backgroundColor = ConstantString.buttonBackgroundColor

                                    }
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
        
        @IBAction func checkBalanceAct(_ sender: Any) {
            walletbalanceLbl.text = "Checking Balance..."
    //        self.checkBalanceAfterIssue()
            self.getWallet()
        }

        
    
    func getWallet(){
        if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            self.walletNameLbl.text = profileUpdate.name
            self.walletbalanceLbl.attributedText =  getHBuckString(value: "\(HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0)")
        }else{
            if Reachability.isConnectedToNetwork() {
                SVProgressHUD.show()
                var data = [String : Any]()
                //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
                data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
                var (url, method, param) = APIHandler().getWalletdata(params: data)
                
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.getWalletObj = Mapper<getwalletObject>().map(JSON: value)
                            
                            if let walletbalance = self.getWalletObj?.data?.sila_balance{
                                self.walletbalanceLbl.attributedText = getHBuckString(value: "\(walletbalance)")
                            }
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
    }
        
    //    @objc func keyboardWillShow(notification: NSNotification) {
    //        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
    //            settingsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
    //       }
    //    }

    //    @objc func keyboardWillHide(notification: NSNotification) {
    //        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
    //            settingsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    //        }
    //    }

        func linkAccountSuccess(metaData: [String : Any]) {
             profileUpdate.parentLinkPayment = true
             data.updateValue(true, forKey: "parentLinkPayment")

            if let account_id = metaData["account_id"] as? String {
                data.updateValue(account_id, forKey: "bankAccountNumber")
                profileUpdate.bankAccount = account_id
            }
            if let institution = metaData["institution"] as? [String : Any] {
                if let name = institution["name"] as? String {
                    data.updateValue(name, forKey: "bankName")
                    profileUpdate.bankName = name

                }
            }
                bankDetailsAccountEditView.isHidden = false
                bankDetailsAccountEditView.bankName.text = profileUpdate.bankName
                bankDetailsAccountEditView.accountId.text = profileUpdate.bankAccount
           

            data.updateValue(metaData, forKey: "bankLinkResponse")
            parentLinkPaymentUpdated = true
            updateUserProfile()
            hideAccountScreens()
        }
        func updateUserProfile() {
            
            if Reachability.isConnectedToNetwork() {
                SVProgressHUD.show()
                            
                data.updateValue(getEncryptedString(plainText: profileUpdate.name!), forKey: "name")
                data.updateValue(profileUpdate.mobileNumber!, forKey: "mobile")
                data.updateValue(getEncryptedString(plainText: profileUpdate.zip!), forKey: "zip")
            //    data.updateValue( getEncryptedString(plainText: profileUpdate.age!), forKey: "age")
                data.updateValue(getEncryptedString(plainText: profileUpdate.email!) , forKey: "email")
                data.updateValue(profileUpdate.bankName!, forKey: "bankName")
                data.updateValue(getEncryptedString(plainText: profileUpdate.lastName!) , forKey: "lastName")
                if let img = profileUpdate.profileImage {
                    let imageData = resize(img)
                    data.updateValue(imageData, forKey: "profilePic")
                } else {
                    data.updateValue("", forKey: "profilePic")
                }
                
               data.updateValue("", forKey: "userType")
               data.updateValue("", forKey: "schoolId")
               data.updateValue("", forKey: "schoolName")
                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                data.updateValue(getEncryptedString(plainText: profileUpdate.gender!) , forKey: "gender")
                
                let (url, method, param) = APIHandler().updateProfile(params: data)

                AF.request(url, method: method, parameters: param, encoding: JSONEncoding.default,headers: headers).validate().responseJSON { response in
                    switch response.result {

                    case .success(let value):
                        if  value is [String:Any] {
                            if !self.parentLinkPaymentUpdated {
                            self.navigationController?.popViewController(animated: true)
                            }else {
                                self.parentLinkPaymentUpdated = false
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                    SVProgressHUD.dismiss()
                    self.parentLinkPaymentUpdated = false
                }
            } else {
                SVProgressHUD.dismiss()
            }
            
        }

        @objc func editBtnAction(_ sender: Any) {
            editProfileVC?.profileParentVC = self
            editProfileVC?.profileUpdate = self.profileUpdate
            editProfileVC?.settingsTableView.reloadData()
            editContainerView.isHidden = false
        }
    
    // MARK: - Action Methods
    
        
    @IBAction func onClickAddLithicBankbtn(_ sender: Any) {
        let addLithicBankVC = GlobalStoryBoard().lithicBankVC
        addLithicBankVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(addLithicBankVC, animated: true)
       
    }
    // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            if segue.identifier == "EditSettingsViewController" {
                editProfileVC = (segue.destination as! EditSettingsViewController)
                
            
            }
        }
        
        func fillUpdateProfileObjectFirstTime(obj:ProfileObject) {
            profileUpdate.name = obj.data?.last?.name ?? ""
            profileUpdate.lastName = obj.data?.last?.lastname ?? ""
            profileUpdate.email = obj.data?.last?.email ?? ""
            profileUpdate.mobileNumber = obj.data?.last?.mobile ?? ""
    //        profileUpdate.age = obj.data?.last?.age ?? ""
            profileUpdate.gender = obj.data?.last?.gender ?? ""
            profileUpdate.dateOfBirth = obj.data?.last?.dateOfBirth ?? ""
            profileUpdate.zip = obj.data?.last?.zip ?? ""
            profileUpdate.bankName = obj.data?.last?.bankName ?? ""
            profileUpdate.bankAccount = obj.data?.last?.bankAccount ?? ""
            profileUpdate.contact = obj.data?.last?.contact ?? ""
            profileUpdate.routingNumber = obj.data?.last?.routingNumber ?? ""
            profileUpdate.parentLinkPayment = obj.data?.last?.parentLinkPayment ?? false

            if profileUpdate.parentLinkPayment == true {
                bankDetailsAccountEditView.isHidden = false
                bankDetailsAccountEditView.bankName.text = profileUpdate.bankName
                bankDetailsAccountEditView.accountId.text = profileUpdate.bankAccount
            }
            else{
                bankDetailsAccountAddView.isHidden = false
            }
            let imageView = UIImageView()
            if obj.data?.last?.profilePic == "" {
                self.setProfilePic(frstName: obj.data?.last?.name ?? "", lstName: nil, profileImageView: imageView)
                profileUpdate.profileImage = imageView.image

            }else {
                if let imagePath = obj.data?.last?.profilePic  {
                    self.profileUpdate.profileImageStr = imagePath
                    if let url = URL(string: imagePath) {
                        DispatchQueue.main.async {
                            if let data = try? Data(contentsOf: url){
                                self.profileUpdate.profileImage = UIImage(data: data)
                            }
                        }
                    }
                    else {
                        self.setProfilePic(frstName: obj.data?.last?.name ?? "", lstName: nil, profileImageView: imageView)
                        profileUpdate.profileImage = imageView.image

                    }
                    self.imageTableView.reloadData()
                }else{
                    self.setProfilePic(frstName: obj.data?.last?.name ?? "", lstName: nil, profileImageView: imageView)
                    profileUpdate.profileImage = imageView.image
                }
            }
           
            settingsTableView.reloadData()
            imageTableView.reloadData()
            hideAccountScreens()
            
        }
        
        func setProfilePic(frstName: String?, lstName: String?, profileImageView: UIImageView){
        
            if let firstName = frstName, let lastName = lstName {
                profileImageView.setImageWith(firstName + " " + lastName, color: .lightGray)
            } else {
                if let dispName = frstName {
                    profileImageView.setImageWith(dispName, color: .lightGray)
                }
            }
        }
        func resize(_ image: UIImage) -> String {
                var actualHeight = Float(image.size.height)
                var actualWidth = Float(image.size.width)
                let maxHeight: Float = 300.0
                let maxWidth: Float = 400.0
                var imgRatio: Float = actualWidth / actualHeight
                let maxRatio: Float = maxWidth / maxHeight
                let compressionQuality: Float = 0.25
                //50 percent compression
                if actualHeight > maxHeight || actualWidth > maxWidth {
                    if imgRatio < maxRatio {
                        //adjust width according to maxHeight
                        imgRatio = maxHeight / actualHeight
                        actualWidth = imgRatio * actualWidth
                        actualHeight = maxHeight
                    }
                    else if imgRatio > maxRatio {
                        //adjust height according to maxWidth
                        imgRatio = maxWidth / actualWidth
                        actualHeight = imgRatio * actualHeight
                        actualWidth = maxWidth
                    }
                    else {
                        actualHeight = maxHeight
                        actualWidth = maxWidth
                    }
                }
                let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
                UIGraphicsBeginImageContext(rect.size)
                image.draw(in: rect)
                let img = UIGraphicsGetImageFromCurrentImageContext()
                let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
                UIGraphicsEndImageContext()
                let imagebase64String = imageData?.base64EncodedString()
                        print(imagebase64String!)
                //        let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
                return imagebase64String!
        //        return UIImage(data: imageData!) ?? UIImage()
            }




    //extension MyPfrofileViewController : UITextFieldDelegate {
    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    ////        let newString = (text as NSString).replacingCharacters(in: range, with: string)
    ////               textField.text = formattedNumber(number: newString)
    ////               return false
    //        return true
    //    }
        
    }


