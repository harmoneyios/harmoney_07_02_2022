//
//  FamilyMemberCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 02/12/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class FamilyMemberCell: UITableViewCell {

    @IBOutlet weak var primaryImage: UIImageView!
    @IBOutlet weak var resendBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var primaryLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var memberDetailstn: UIButton!
    @IBOutlet weak var mainBackView: UIView!
    @IBOutlet weak var primaryBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
        
}
