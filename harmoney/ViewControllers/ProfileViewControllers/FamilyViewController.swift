//
//  FamilyViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import ObjectMapper

enum FamilyViewControllerType {
    case invite
    case acceptInvite
    case showFamilyList
    case none
}

class FamilyViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    @IBOutlet weak var addKidView: AddKidCustomView!
    @IBOutlet weak var inviteParentView: InviteParentView!
    
    @IBOutlet weak var removeMemberView: RemoveMemberView!
    @IBOutlet weak var resendInviteView: ResendInviteView!
    @IBOutlet weak var familyListView: FamilyMembersView!
    var type: FamilyViewControllerType = .invite
    var  customAlertViewController: CustomAlertViewController?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    
    //MARK: Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        
//        self.getInvites()
        self.familyListView.onLoadSetUp()
        super.viewWillAppear(animated)
//        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
//            familyListView.isHidden = false
//            familyListView.delegate = self
//        }else{
////            familyListView.isHidden = true
//            familyListView.isHidden = false
//            familyListView.delegate = self
//        }
        resendInviteView.isHidden = false
        switch type {
        case .invite:
            
            if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
                
    //            familyListView.isHidden = true
                inviteParentView.isHidden = false
                inviteParentView.delegate = self
                
                addKidView.isHidden = true
                resendInviteView.isHidden = true
                removeMemberView.isHidden = true
                familyListView.isHidden = false
                familyListView.delegate = self
                familyListView.addfamilyClick .setTitle("Add Your Parent", for: .normal)
                
            }else{
                familyListView.isHidden = false

                addKidView.isHidden = false
                addKidView.delegate = self
                inviteParentView.isHidden = true
                resendInviteView.isHidden = true
                removeMemberView.isHidden = true
                familyListView.delegate = self
                familyListView.addfamilyClick .setTitle("Add Your Kids", for: .normal)
               
            }
            break
        case .acceptInvite:
            addKidView.isHidden = false
            addKidView.delegate = self
            
            inviteParentView.isHidden = true
            break
        case .showFamilyList:
            inviteParentView.isHidden = true
            familyListView.isHidden = false
             break
        case .none:
            inviteParentView.isHidden = true
            addKidView.isHidden = true
            break
        }
//        if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.parent{
//            inviteParentView.isHidden = false
//        }else{
//            inviteParentView.isHidden = false
//        }
        
    }
   
    private func getInvites() {
        PersonalFitnessManager.sharedInstance.getInvites { (result) in
            switch result {
            case .success(_):
                print(result)
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }

}

//MARK: UI Methods
extension FamilyViewController {
    func presentCustomAlertViewController() {
        customAlertViewController = GlobalStoryBoard().customAlertVC
        customAlertViewController?.alertTitle = "Invite Sent"
        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            customAlertViewController?.alertMessage = "Yay! The invitation has been sent!\nWaiting for your parent to accept your invite."

        } else {
            customAlertViewController?.alertMessage = "Yay! Invite sent! \n Waiting for your kid to accept your invitation!"
        }
        customAlertViewController?.delegate = self
        tabBarController?.present(customAlertViewController!, animated: true, completion: nil)
    }
    
    func showUploadImageOptions() {
        let alert = UIAlertController.init(title: "Upload Image", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction.init(title: "Select Picture", style: .default) { (action) in
            self.selectPictureFromLibrary()
        }
        
        let action2 = UIAlertAction.init(title: "Capture Image", style: .default) { (action) in
            self.captureProfileImage()
        }
        
        let action3 = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectPictureFromLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated:true)
    }
    
    func captureProfileImage() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
             imagePicker.sourceType = UIImagePickerController.SourceType.camera
        }
       
        imagePicker.allowsEditing = false
        
        self.present(imagePicker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
        var image : UIImage!

        if let img = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as? UIImage
        {
            image = img

        }
        else if let img = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage
        {
            image = img
        }
        addKidView.profileImageView.image = image

        picker.dismiss(animated: true,completion: nil)
 }
    
    func getFamilyMembersList() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")
        
        if Reachability.isConnectedToNetwork(){
            var (url, method, param) = APIHandler().getFamilyInvitees(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            
            SVProgressHUD.show()
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                                                                        
                    }
                    SVProgressHUD.dismiss()
                case .failure(let error):
                    print(error)
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                }
            }
        } else{
            self.view.toast("Internet Connection not Available!")
        }
    }
    
    func getDashBoardData() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")
        if Reachability.isConnectedToNetwork(){
            var (url, method, param) = APIHandler().dashboard(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: value)
                       
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.familyListView.onLoadSetUp()
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }else{
            self.view.toast("Internet Connection not Available!")
        }
        
    }

}

//MARK: CustomAlertViewControllerDelegate Methods
extension FamilyViewController: CustomAlertViewControllerDelegate {
    func didTapOkayButton(customAlertViewController: CustomAlertViewController) {
        customAlertViewController.dismiss(animated: true, completion: nil)
        HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
//        self.inviteParentView.isHidden = true
        self.inviteParentView.reloadInviteParentViewData()
        self.addKidView.reloadInviteKidViewData()
        self.resendInviteView.isHidden = true
        self.familyListView.isHidden = false
        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            self.inviteParentView.isHidden = false

        }else{
            self.addKidView.isHidden = false

        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.familyListView.onLoadSetUp()
        }        
//        self.tabBarController?.selectedIndex = 1
    }
}

//MARK: InviteParentViewDelegate Methods
extension FamilyViewController: InviteParentViewDelegate {
    func didTapInviteButton(inviteParentView: InviteParentView, inviteData: InviteData) {
        PersonalFitnessManager.sharedInstance.inviteMember(inviteData: inviteData, inviteAgain: false) { (result) in
            switch result {
            case .success(_):                
                self.presentCustomAlertViewController()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
}

//MARK: InviteParentViewDelegate Methods
extension FamilyViewController: AddKidCustomViewDelegate {
    func didTapProfileImageView(addKidCustomView: AddKidCustomView) {
        showUploadImageOptions()
    }
    
    func didTapAddKidButton(addKidCustomView: AddKidCustomView, inviteData: InviteData) {
        PersonalFitnessManager.sharedInstance.inviteMember(inviteData: inviteData, inviteAgain: false) { (result) in
            switch result {
            case .success(_):
                self.presentCustomAlertViewController()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
}

extension FamilyViewController : ResendInviteDelegate{
    func didTapResendInvite(inviteParentView: FamilyMembersView, inviteData: InviteData) {
        
        self.familyListView.isHidden = true
        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            self.inviteParentView.isHidden = true

        }else{
            self.addKidView.isHidden = true

        }
        self.removeMemberView.isHidden = true
        self.resendInviteView.isHidden = false
        self.resendInviteView.delegate = self
        self.resendInviteView.resendData = inviteData
        self.resendInviteView.loadResendView()
       
       
    }
    func didTapRemoveMember(inviteParentView: FamilyMembersView, inviteData: InviteData) {
        
        self.familyListView.isHidden = true
//        self.addKidView.isHidden = true
//        self.inviteParentView.isHidden = true
        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            self.inviteParentView.isHidden = true

        }else{
            self.addKidView.isHidden = true

        }
        self.resendInviteView.isHidden = true
        self.removeMemberView.isHidden = false
        self.removeMemberView.delegate = self
        self.removeMemberView.resendData = inviteData
        self.removeMemberView.reloadData()
       
       
    }
    func didTapCancelInvite(inviteParentView: FamilyMembersView, inviteData: InviteData) {
        let removeCouponViewController = GlobalStoryBoard().removeCouponVC
         removeCouponViewController.delegate = self
         removeCouponViewController.imageString = ""
        removeCouponViewController.titleString = "Are you sure want to remove \(inviteData.memberName ?? "") from Harmoney's family member list? \n Continue"
        removeCouponViewController.descriptionString = ""
        removeCouponViewController.memberID = inviteData.memberId ?? ""
        tabBarController?.present(removeCouponViewController, animated: false, completion: nil)

    }
    
    func showInfo() {
        
            customAlertViewController = GlobalStoryBoard().customAlertVC
            customAlertViewController?.alertTitle = "Info"
//            if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
//                customAlertViewController?.alertMessage = "Yay! The invitation has been sent! Waiting for your parent to accept your invite."
//
//            } else {
                customAlertViewController?.alertMessage = "Choose your Primary Member for getting Challenge Approval. "
//            }
            customAlertViewController?.delegate = self
            tabBarController?.present(customAlertViewController!, animated: true, completion: nil)
       
    }
    
    func showMemberDetails(inviteParentView: FamilyMembersView, inviteData: InviteData){
        
        
        
    }

    
    func changePrimary(inviteParentView: FamilyMembersView, inviteData: InviteData){
        PersonalFitnessManager.sharedInstance.changePrimaryMember(inviteData: inviteData) { (result) in
            switch result {
            case .success(_):
//                self.presentCustomAlertViewController()
                self.getDashBoardData()
                self.familyListView.onLoadSetUp()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }

    }
    
    
}

extension FamilyViewController : RemoveMemberViewDelegate{
    
    func closeRemoveMemberView() {
        self.removeMemberView.isHidden = true
        self.resendInviteView.isHidden = true
        self.familyListView.isHidden = false
//        self.inviteParentView.isHidden = false
//        self.addKidView.isHidden = false
        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            self.inviteParentView.isHidden = false

        }else{
            self.addKidView.isHidden = false

        }
    }
    
    func didTapRemoveMemberButton(resendInviteView: RemoveMemberView, inviteData: InviteData) {
        let removeCouponViewController = GlobalStoryBoard().removeCouponVC
         removeCouponViewController.delegate = self
         removeCouponViewController.imageString = ""
        removeCouponViewController.titleString = "Are you sure you want to remove family member?"
        removeCouponViewController.descriptionString = "You will lose this person from family members List"
        removeCouponViewController.memberID = inviteData.memberId ?? ""
        tabBarController?.present(removeCouponViewController, animated: false, completion: nil)
    }
   
}

extension FamilyViewController : ResendInviteViewDelegate{
    func didTapResendButton(resendInviteView: ResendInviteView, inviteData: InviteData) {
        PersonalFitnessManager.sharedInstance.inviteMember(inviteData: inviteData, inviteAgain: true) { (result) in
            switch result {
            case .success(_):
                self.presentCustomAlertViewController()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func closeResendView() {
        self.resendInviteView.isHidden = true
        self.removeMemberView.isHidden = true
        self.familyListView.isHidden = false
//        self.inviteParentView.isHidden = false
//        self.addKidView.isHidden = false
        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            self.inviteParentView.isHidden = false

        }else{
            self.addKidView.isHidden = false

        }
    }
}

//MARK: - DeleteChallangeViewControllerDelegate Methods

extension FamilyViewController: RemoveCouponViewControllerDelegate {
    
    func didTapThumbsDownButton(deleteChallangeViewController: RemoveCouponAlertViewController) {
        deleteChallangeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapThumbsUpButton(deleteChallangeViewController: RemoveCouponAlertViewController, memberId: String) {

        PersonalFitnessManager.sharedInstance.cancelInviteMember(memberID: memberId) { (result) in
            switch result {
            case .success(_):
                self.getDashBoardData()
                self.removeMemberView.isHidden = true
                self.resendInviteView.isHidden = true
                self.familyListView.isHidden = false
//                self.inviteParentView.isHidden = false
//                self.addKidView.isHidden = false
                if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
                    self.inviteParentView.isHidden = false

                }else{
                    self.addKidView.isHidden = false

                }
                deleteChallangeViewController.dismiss(animated: true) {
                }
            case .failure(let error):
                self.view.toast(error.localizedDescription)
                }
            }
          
        
    }
}
