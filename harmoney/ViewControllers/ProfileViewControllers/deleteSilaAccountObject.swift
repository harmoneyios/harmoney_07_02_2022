//
//  deleteSilaAccountObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 17/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct deleteSilaAccountObject: Mappable {
       
        var status : Int32?
        var data : [deleteSilaAccountObjectdata]?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            data    <- map["data"]
        }

    }


struct  deleteSilaAccountObjectdata : Mappable {
      
    var message: String?
    var account_name: String?
    var status : String?
    var success : Bool?

    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        message          <- map["message"]
        account_name           <- map["account_name"]
        success         <- map["success"]
        status    <- map["status"]
      
    }
}
