//
//  transactionTypeTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 14/09/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class transactionTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var selectionBtn: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
