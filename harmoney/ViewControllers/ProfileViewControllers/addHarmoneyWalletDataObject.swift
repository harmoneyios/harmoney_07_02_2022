//
//  addHarmoneyWalletDataObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 14/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct addHarmoneyWalletDataObject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : addHarmoneyWalletDataObjectdata?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }

struct addHarmoneyWalletDataObjectdata : Mappable {
    

    var xp: Int?
    var hBucks: Int?
    var jem : Int?
    var level : Int?
  
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        xp          <- map["xp"]
        hBucks           <- map["hBucks"]
        jem            <- map["jem"]
        level    <- map["level"]
    }
}
