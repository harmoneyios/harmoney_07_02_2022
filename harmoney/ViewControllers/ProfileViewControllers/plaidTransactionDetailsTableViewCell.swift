//
//  plaidTransactionDetailsTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 18/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class plaidTransactionDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var contentMainView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
