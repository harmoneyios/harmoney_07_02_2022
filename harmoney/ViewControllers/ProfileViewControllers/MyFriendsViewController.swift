//
//  MyFriendsViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 24/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import JKSteppedProgressBar
import Alamofire
import SVProgressHUD
import ObjectMapper
import Toast_Swift
import IQKeyboardManagerSwift
import SDWebImage

import LinkKit
// <!-- SMARTDOWN_IMPORT_LINKKIT -->

protocol LinkOAuthHandling {
    var linkHandler: Handler? { get }
    var oauthRedirectUri: URL? { get }
}

class MyFriendsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate, LinkOAuthHandling, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    var linkHandler: Handler?
    
    var oauthRedirectUri: URL?
    
    @IBOutlet weak var deleteAccount: UIButton!
    
    @IBOutlet weak var stepProgressBackViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var stepProgressMainBackView: UIView!
    @IBOutlet weak var addWalletButton: UIButton!
    @IBOutlet weak var transferMobileNumberTxt: UITextField!
    @IBOutlet weak var transferAmountTxt: UITextField!
    @IBOutlet weak var walletAmountTxt: UITextField!
    @IBOutlet weak var servicesMainView: UIView!
    @IBOutlet weak var walletToBankView: UIView!
    @IBOutlet weak var transferToUserView: UIView!
    @IBOutlet weak var AddMoneyView: UIView!
    @IBOutlet weak var walletDetailMainView: UIView!
    @IBOutlet weak var accountDetailMainView: UIView!
    @IBOutlet weak var accountDetailTableView: UITableView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var checkBalance: UIButton!
    @IBOutlet weak var amountTxtField: UITextField!
    @IBOutlet weak var goBtn: UIButton!
    @IBOutlet weak var viewTransactionDetailsBtn: UIButton!
    @IBOutlet weak var redeemBtn: UIButton!
    @IBOutlet weak var transferBtn: UIButton!
    @IBOutlet weak var issueBtn: UIButton!
    
    @IBOutlet weak var addMoneySeperator: UIView!
    
    @IBOutlet weak var walletNamedlbl: UILabel!
    @IBOutlet weak var redeemSeperator: UIView!
    @IBOutlet weak var transferUserSeperator: UIView!
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    @IBOutlet weak var viewTransactionBtn: UIButton!
    @IBOutlet weak var transactionBottomView: UIView!
    @IBOutlet weak var transactionHeaderView: UIView!
    @IBOutlet weak var transactionView: UIView!
    @IBOutlet weak var accountStatusLabel: UILabel!
    @IBOutlet weak var accountTypeLabel: UILabel!
    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var linkbankDetailsStack: UIView!
    @IBOutlet weak var nobankLinkStack: UIView!
    @IBOutlet weak var walletbalanceLbl: UILabel!
    @IBOutlet weak var walletNameLbl: UILabel!
    @IBOutlet weak var walletContinueBtn: UIButton!
    @IBOutlet weak var walletSaveLabel: UILabel!
    @IBOutlet weak var walletCreateBtn: UIButton!
    @IBOutlet weak var walletCreateView: UIView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var ReSubmitBtn: UIButton!

    @IBOutlet weak var kycStatus: UILabel!
    @IBOutlet weak var nameRegister: UILabel!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var kycMainView: UIView!
    @IBOutlet weak var registerMainView: UIView!
    @IBOutlet weak var progressBar: SteppedProgressBar!
    @IBOutlet weak var settingTableView: UITableView!
    var regObj : registerobject?
    var kycObj : requestKYCobject?
    var checkkycObj : checkKYCObject?
    var getWalletObj : getwalletObject?
    var getAccountobj : getsilaAccountData?
    var getLinkToken : getPlaidTokenObject?
    var silaLinkAccountObjeValue : silaLinkAccountObject?
    var silaIssueValue : silaIssueObject?
    var getBalaceAfterIssueValue : getBalanceafterIssue?
    var addHarmoneyWalletValue : addHarmoneyWalletDataObject?
    var getGuidForMobileNumber : guidForMobileNumberObject?
    var deleteSilaAccountObject :  deleteSilaAccountObject?
    var transfertoUserObjectValue:  transfertoUserObject?
    var getInformationForPaymentObject : getInformationForPaymentObject?
    var  customAlertViewController: PlaidTransactionViewController?
    var  subAddWallet: SubAccountViewController?
    var getCityInformationObjectValue: getCityInformationObject?
    var  thumbsAlertViewController: ThumbsAlertViewController?
//    var progressBar: FlexibleSteppedProgressBar!
//    let stepIndicatorView = StepIndicatorView()
    var currentTF : UITextField?
    var profileCellIndetifier = "plaidRegisterTableViewCell"
    var DateofBirthTableViewCellIdentifier = "DateofBirthTableViewCell"
    var cityTableViewCellIdentifier = "CityTableViewCell"
    var registerTableViewCellIdentifier = "registerTableViewCell"
    var selectAction:String = "issue"
    var showView:String = "info"
    var alertString: String = "issueWallet"
    var profileUpdate = ProfileUpdateObject()
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    var imageUpload = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addWalletButton.isHidden = false  //remove hidden
        settingTableView.delegate = self
        settingTableView.dataSource = self
        // Do any additional setup after loading the view.
        settingTableView.register(UINib.init(nibName: "registerTableViewCell", bundle: nil), forCellReuseIdentifier: "registerTableViewCell")
        settingTableView.register(UINib.init(nibName: "DateofBirthTableViewCell", bundle: nil), forCellReuseIdentifier: "DateofBirthTableViewCell")
        settingTableView.register(UINib.init(nibName: "CityTableViewCell", bundle: nil), forCellReuseIdentifier: "CityTableViewCell")
        accountDetailTableView.register(UINib.init(nibName: "accountDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "accountDetailsTableViewCell")
        progressBar.titles = ["Start", "Register", "KYC", "Wallets"]
        
        
        if showView == "INFO"{
            infoView.isHidden = false
            kycMainView.isHidden = true
            walletCreateView.isHidden = true
            progressBar.currentTab = 2
            progressBar.activeImages = [
              UIImage(named: "green tick")!,
              UIImage(named: "green2")!,
              UIImage(named: "gray3")!,
              UIImage(named: "gray4")!,
            ]
        }else if showView == "KYC"{
            kycMainView.isHidden = false
            infoView.isHidden = true
            walletCreateView.isHidden = true
            progressBar.currentTab = 3
            progressBar.activeImages = [
              UIImage(named: "green tick")!,
              UIImage(named: "green tick")!,
              UIImage(named: "green3")!,
              UIImage(named: "gray4")!,
            ]
        }else if showView == "WALLET"{
            walletCreateView.isHidden = false
            infoView.isHidden = true
            kycMainView.isHidden = true
            progressBar.currentTab = 4
            self.getWallet()
            self.getAccount()
            progressBar.activeImages = [
              UIImage(named: "green tick")!,
              UIImage(named: "green tick")!,
              UIImage(named: "green tick")!,
              UIImage(named: "green4")!,
            ]
        }
        
//        walletCreateView.isHidden = false
        kycStatus.textColor = UIColor.harmoneyBlueColor
        nameRegister.textColor = UIColor.harmoneyBlueColor
        
       amountTxtField.delegate = self
        amountTxtField.keyboardType = .numberPad
        
//        self.getCityInformation()

//        amountTxtField.addTarget(self, action: #selector(updategoBtn(textField:)), for: .touchUpInside)
        self.linkbankDetailsStack.cardView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInformation()
//        getWallet()
    }
    func presentThumbsAlertViewController(alertTitle: String) {
        thumbsAlertViewController = GlobalStoryBoard().thumbsAlertVC
        thumbsAlertViewController?.alertTitle = alertTitle
        thumbsAlertViewController?.delegate = self
        present(thumbsAlertViewController!, animated: true, completion: nil)
    }
    func getInformation(){
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            var (url, method, param) = APIHandler().getInformationForPayment(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.getInformationForPaymentObject = Mapper<getInformationForPaymentObject>().map(JSON: value)
                        print(self.getInformationForPaymentObject!)
                        self.walletNameLbl.text = self.getInformationForPaymentObject?.data?.user?.cryptoAlias
                        self.walletNamedlbl.text = self.getInformationForPaymentObject?.data?.user?.cryptoAlias
                        
                        if (self.getInformationForPaymentObject?.data?.user?.processStage == 2){
                            self.requestKyc()
                        }
                        if (self.getInformationForPaymentObject?.data?.user?.processStage == 3){
                          
                            let firstName = self.getInformationForPaymentObject?.data?.user?.firstName
                            self.nameRegister.text = firstName
                            self.refreshStatus()
                        }
                        if self.getInformationForPaymentObject?.data?.kidWallet == nil
                        {
                            self.addWalletButton.setTitle("Add kid wallet", for: .normal)
                        }else{
//                            if self.getInformationForPaymentObject?.data?.user?.walletPrivateKey == nil{
//                                self.addnewBankBtn .setTitle("Add Bank", for: .normal)
//                                self.noAccountLabel.isHidden = false
//                                self.noAccountLabel.text = "No bank account or credit card added"
//                                self.walletDetailView.isHidden = true
//                                self.toView = "INFO"
//                            }
                            self.addWalletButton.setTitle("View kid wallet", for: .normal)
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    @IBAction func closeBtnAct(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if imageUpload == false {
            self.navigationController?.popViewController(animated: true)
            UserDefaults.standard.removeObject(forKey: "registerCity")
        }else{
            imageUpload = false
        }
    }
    
    @objc func updategoBtn(textField:UITextField) {
        currentTF = textField
        let string = textField.text
        goBtn.backgroundColor = UIColor.harmoneyBlueColor
        goBtn.titleLabel?.textColor = UIColor.white
        goBtn.isEnabled = true
    }
    
    @IBAction func addSubAccount(_ sender: Any) {
        
        self.presentSubWallet()
        
    }
    
    @IBAction func transactionDetailView(_ sender: Any) {
        
        self.presentCustomAlertViewController()
    }
    
    func presentCustomAlertViewController() {
        customAlertViewController = GlobalStoryBoard().customplaidVc
  
        customAlertViewController?.delegate = self
       tabBarController?.present(customAlertViewController!, animated: true, completion: nil)
//        self.navigationController?.pushViewController(customAlertViewController!, animated: false)
    }
    
    
    func presentSubWallet() {
        subAddWallet = GlobalStoryBoard().addSubWallet
  
        subAddWallet?.delegate = self
       tabBarController?.present(subAddWallet!, animated: true, completion: nil)
//        self.navigationController?.pushViewController(customAlertViewController!, animated: false)
    }
    
    @IBAction func deleteBtnAct(_ sender: Any) {
        
        
//        let refreshAlert = UIAlertController(title: "Refresh", message: "All data will be lost.", preferredStyle: UIAlertController.Style.alert)
//
//        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
//              print("Handle Ok logic here")
//        }))
//
//        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
//              print("Handle Cancel Logic here")
//        }))
//
//        present(refreshAlert, animated: true, completion: nil)
        
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            data.updateValue(self.getAccountobj?.data?.last?.account_name! ?? "name", forKey: "accountName")
        var (url, method, param) = APIHandler().deleteSilaAmount(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.deleteSilaAccountObject = Mapper<deleteSilaAccountObject>().map(JSON: value)
//                            if self.deleteSilaAccountObject?.data?.last?.status == "SUCCESS"
//                            {
                                self.getWallet()
                                self.getAccount()
//                            }e

                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 500{

            
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = formattedNumber(number: newString)
            return false
        }
    
        if textField.tag == 100 ||  textField.tag == 101 {
            guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
               let substringToReplace = textFieldText[rangeOfTextToReplace]
               let count = textFieldText.count - substringToReplace.count + string.count
           
            do {
               let regex = try NSRegularExpression(pattern: ".*[^A-Za-z].*", options: [])
               if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                   return false
               }
           }
           catch {
               print("ERROR")
           }
            
            return count <= 30
        } else if textField.tag == 105 {
            
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 5
        }else if textField.tag == 104{
                    guard let textFieldText = textField.text,
                        let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                            return false
                    }
                    let substringToReplace = textFieldText[rangeOfTextToReplace]
                    let count = textFieldText.count - substringToReplace.count + string.count
                    return count <= 30
        }else if textField.tag == 106{
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 9
        }else if textField.tag == 109{
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            textField.text = formattedNumber(number: newString)
            return false
        }
     
        return true

    }
    
 
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXX-XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }

    func textFieldDidEndEditing(_ textField: UITextField)  {// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("textFieldDidEndEditing")
        
//        currentTF = textField
//        let string = textField.text
//        if string?.count ?? 0 > 0
//        {
//            goBtn.backgroundColor = UIColor.clear
//            goBtn.titleLabel?.textColor = UIColor.harmoneyLightBlueColor
//            goBtn.isEnabled = true
//        }else{
//            goBtn.backgroundColor = UIColor.clear
//            goBtn.titleLabel?.textColor = UIColor.harmoneyLightBlueColor
//            goBtn.isEnabled = true
//        }
   

    }
 
    
    func handleSuccessWithTokenLocal(_ publicToken: String, accountID: String, accountName:String) {
        
        print("public-token_in local controller: \(publicToken) metadatainlocalController: \(accountID)")
        
      
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            data.updateValue(getEncryptedString(plainText: publicToken) , forKey: "token")
            data.updateValue(getEncryptedString(plainText: publicToken) , forKey: "token")
            data.updateValue(getEncryptedString(plainText: accountName)  , forKey: "accountName")
            data.updateValue(getEncryptedString(plainText: accountID) , forKey: "accountId")
            data.updateValue(getEncryptedString(plainText: "link") , forKey: "plaidTokenType")
            var (url, method, param) = APIHandler().silaLinkAccount(params: data)
            
              AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.silaLinkAccountObjeValue = Mapper<silaLinkAccountObject>().map(JSON: value)
                        if self.silaLinkAccountObjeValue?.data?.status == "SUCCESS" {
                            self.getAccount()
                        }
                        }
                        
                    
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
    
    @IBAction func checkBalanceAct(_ sender: Any) {
        walletbalanceLbl.text = "Checking Balance..."
//        self.checkBalanceAfterIssue()
        self.getWallet()
    }
    func checkBalanceAfterIssue() {
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            data.updateValue(self.accountNameLabel.text!, forKey: "accountName")
    
        var (url, method, param) = APIHandler().getBalanceAfterSilaissue(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.getBalaceAfterIssueValue = Mapper<getBalanceafterIssue>().map(JSON: value)
                            if let current_balance = self.getBalaceAfterIssueValue?.data?.current_balance {
                                self.walletbalanceLbl.attributedText = getHBuckString(value: "\(current_balance)")

                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }

    @IBAction func goBtnAct(_ sender: Any) {
        
        if selectAction == "issue"{
            self.issueAct()
        }else if selectAction == "redeem" {
            self.redeemAct()
        }else if selectAction == "transfer" {
//            self.transferAct()
        }
        
    }
    
    
    @IBAction func addMoneyAct(_ sender: Any) {
        alertString = "issueWallet"
        let amount: Int? = Int(amountTxtField.text ?? "0")
        
        if amount == nil || amount ?? 0 <= 0 {
            toast(message: "Please enter valid amount")
            return
        }else{
            if let walletamount = amount{
                presentThumbsAlertViewController(alertTitle: "Do you want transfer $\(walletamount) to your wallet")

            }

        }
        
    }
    
    
    @IBAction func transferToUserAct(_ sender: Any) {
        
        self.getGuid()
    }
    func getGuid() {
        
        let guid:String = ""
            if Reachability.isConnectedToNetwork() {
             SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
              
                data.updateValue(transferMobileNumberTxt.text ?? "0", forKey: "moblieNumber")
            var (url, method, param) = APIHandler().getGuidForMobileNumber(params: data)
           
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                        SVProgressHUD.dismiss()
                        switch response.result {
                        case .success(let value):
                            if  let value = value as? [String:Any] {
                                self.getGuidForMobileNumber = Mapper<guidForMobileNumberObject>().map(JSON: value)
                                let guid = self.getGuidForMobileNumber?.data?.last?.guid
                                let userName = self.getGuidForMobileNumber?.data?.last?.name
                                
                                var data = [String : Any]()
                                //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                                    let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
                                    data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
                                data.updateValue(self.transferAmountTxt.text ?? "", forKey: "amount")
                                data.updateValue(guid ?? "", forKey: "transferGuid")
                                    let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
                                    data.updateValue(cryptoAddress ?? "", forKey: "walletAddress")
                                var (url, method, param) = APIHandler().transferAmountToSila(params: data)
                               
                                AF.request(url, method: method, parameters: param,headers: self.headers).validate().responseJSON { response in
                                            SVProgressHUD.dismiss()
                                        switch response.result {
                                        case .success(let value):
                                            if  let value = value as? [String:Any] {
                                                self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                                                AlertView.shared.showAlert(view: self, title: "Transaction", description: "Transaction submitted for processing")

                                                if self.transfertoUserObjectValue?.data?.status == "SUCCESS" {
                                                    self.reduceHarmoneyWallet(amount: self.transferAmountTxt.text ?? "0")
                                                }
                                                self.transferAmountTxt.text = ""
                                                self.transferMobileNumberTxt.text = ""
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 90) {
                                                    self.getWallet()
                                                }                                                }
                                        case .failure(let error):
                                            print(error)
                                        }
                                    }
                            }
                        case .failure(let error):
                            print(error)
                        }
                }
            }
       
    }
    
    func reduceHarmoneyWallet(amount: String){
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        data.updateValue(amount, forKey: "amount")
      
        var (url, method, param) = APIHandler().reduceHarmoneyWallet(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
//                            self.view.toast(self.addHarmoneyWalletValue?.message ?? "Wallet amount added to harmoney account")
//                            AlertView.shared.showAlert(view: self, title: "Transaction", description: self.addHarmoneyWalletValue?.message ?? "")
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    
    @IBAction func walletToBankAct(_ sender: Any) {
        alertString = "redeemWallet"
        let amount: Int? = Int(walletAmountTxt.text ?? "0")
        
        if amount == nil || amount ?? 0 <= 0 {
            toast(message: "Please enter valid amount")
            return
        }else{
            if let walletamount = amount{
                presentThumbsAlertViewController(alertTitle: "Do you want to redeem \(walletamount) to your bank account")

            }

        }
       
    }
    func transferAct() {

        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            data.updateValue(transferAmountTxt.text ?? "", forKey: "amount")
            data.updateValue(transferMobileNumberTxt.text ?? "", forKey: "transferGuid")
            let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
            data.updateValue(cryptoAddress ?? "", forKey: "walletAddress")
        var (url, method, param) = APIHandler().transferAmountToSila(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.silaIssueValue = Mapper<silaIssueObject>().map(JSON: value)
//                            self.checkBalanceAfterIssue()
                            self.getWallet()
//                            self.addHarmoneyWallet()
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    
    
    func redeemAct() {
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            data.updateValue(walletAmountTxt.text ?? "0", forKey: "amount")
            data.updateValue(self.getAccountobj?.data?.last?.account_name! ?? "name", forKey: "accountName")
            data.updateValue("Transfer same account", forKey: "descriptor")
            data.updateValue("SAME_DAY_ACH", forKey: "processingType")
        var (url, method, param) = APIHandler().redeemSilaAmount(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                            AlertView.shared.showAlert(view: self, title: "Transaction", description: "Transaction submitted for processing")
                            self.walletAmountTxt.text = ""
                            DispatchQueue.main.asyncAfter(deadline: .now() + 90) {
                                self.getWallet()
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    func issueAct() {
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            data.updateValue(amountTxtField.text!, forKey: "amount")
            data.updateValue(self.getAccountobj?.data?.last?.account_name! ?? "name", forKey: "accountName")
            data.updateValue("Transfer same account", forKey: "descriptor")
            data.updateValue("SAME_DAY_ACH", forKey: "processingType")
        var (url, method, param) = APIHandler().silaIssueAmount(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.silaIssueValue = Mapper<silaIssueObject>().map(JSON: value)
                            if self.silaIssueValue?.data?.status == "SUCCESS" {
                                self.addHarmoneyWallet(amount: self.amountTxtField.text ?? "0")
                            }
                            self.amountTxtField.text = ""
                            AlertView.shared.showAlert(view: self, title: "Transaction", description: "Transaction submitted for processing")
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    func addHarmoneyWallet(amount: String){
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        data.updateValue(amount, forKey: "amount")
      
        var (url, method, param) = APIHandler().addHarmoneyWallet(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    
    
    func getCityInformation(){
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            var (url, method, param) = APIHandler().getCityInformation(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.getCityInformationObjectValue = Mapper<getCityInformationObject>().map(JSON: value)
                        print(self.getCityInformationObjectValue!)
                        var memberId:[String] = []
                        if let assigntomemberDataList = self.getCityInformationObjectValue?.data{
                            for objMember in assigntomemberDataList{
                               
                                memberId.append(objMember.city ?? "AnyCity")
                                
                            }
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    
    @IBAction func viewTransactionBtnAct(_ sender: Any) {
    }
    @IBAction func redeemBtnAct(_ sender: Any) {
//        redeemBtn.backgroundColor = UIColor.harmoneyLightBlueColor
//        redeemBtn.titleLabel?.textColor = UIColor.white
//        transferBtn.backgroundColor = UIColor.clear
//        transferBtn.titleLabel?.textColor = UIColor.lightGray
//        issueBtn.backgroundColor = UIColor.clear
//        issueBtn.titleLabel?.textColor = UIColor.lightGray
//        selectAction = "redeem"
        
        self.AddMoneyView.isHidden = false
        self.addMoneySeperator.backgroundColor = UIColor.lightGray
        self.transferToUserView.isHidden = true
        self.transferUserSeperator.backgroundColor = UIColor.lightGray
        self.redeemSeperator.backgroundColor = UIColor.harmoneyBlueColor
        self.walletToBankView.isHidden = false

    }
    @IBAction func transferBtnAct(_ sender: Any) {
//        transferBtn.backgroundColor = UIColor.harmoneyLightBlueColor
//        transferBtn.titleLabel?.textColor = UIColor.white
//        issueBtn.backgroundColor = UIColor.clear
//        issueBtn.titleLabel?.textColor = UIColor.lightGray
//        redeemBtn.backgroundColor = UIColor.clear
//        redeemBtn.titleLabel?.textColor = UIColor.lightGray
//        selectAction = "transfer"
        
        self.AddMoneyView.isHidden = true
        self.addMoneySeperator.backgroundColor = UIColor.lightGray
        self.transferToUserView.isHidden = false
        self.transferUserSeperator.backgroundColor = UIColor.harmoneyBlueColor
        self.redeemSeperator.backgroundColor = UIColor.lightGray
        self.walletToBankView.isHidden = true

    }
    @IBAction func issueBtnAct(_ sender: Any) {
//        issueBtn.backgroundColor = UIColor.harmoneyLightBlueColor
//        issueBtn.titleLabel?.textColor = UIColor.white
//        transferBtn.backgroundColor = UIColor.clear
//        transferBtn.titleLabel?.textColor = UIColor.lightGray
//        redeemBtn.backgroundColor = UIColor.clear
//        redeemBtn.titleLabel?.textColor = UIColor.lightGray
//        selectAction = "issue"
        
        self.AddMoneyView.isHidden = false
        self.addMoneySeperator.backgroundColor = UIColor.harmoneyBlueColor
        self.transferToUserView.isHidden = true
        self.transferUserSeperator.backgroundColor = UIColor.lightGray
        self.redeemSeperator.backgroundColor = UIColor.lightGray
        self.walletToBankView.isHidden = true
        
        
    }
    @IBAction func walletContinueAct(_ sender: Any) {
       
      /*  if #available(iOS 13.0, *) {
            let couponDetails = GlobalStoryBoard().plaidVC
            self.present(couponDetails, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }*/

        
            enum PlaidLinkUILayer {
                case UIKit
                case swiftUI
            }
            enum PlaidLinkSampleFlow {
                case linkToken
                case linkPublicKey // for compatability with LinkKit v1
            }
            #warning("Select your desired Plaid Link sample flow and UI layer")
            let tuple: (PlaidLinkSampleFlow, PlaidLinkUILayer) = (flow: .linkToken, ui: .UIKit)
            switch tuple {
                case (.linkToken, .UIKit):
                    if #available(iOS 13.0, *) {
                        presentPlaidLinkUsingLinkToken()
                    } else {
                        // Fallback on earlier versions
                    }
                case (.linkToken, .swiftUI):
                    if #available(iOS 13.0, *) {
                        presentSwiftUILinkToken()
                        if #available(iOS 13.0, *) {
                            presentPlaidLinkUsingPublicKey()
                        } else {
                            // Fallback on earlier versions
                        }   // Fallback on earlier versions
                    }
                case (.linkPublicKey, .UIKit):
                    if #available(iOS 13.0, *) {
                        presentPlaidLinkUsingPublicKey()
                    } else {
                        // Fallback on earlier versions
                    }
                case (.linkPublicKey, .swiftUI):
                    if #available(iOS 13.0, *) {
                        presentSwiftUIPublicKey()
                    } else {
                        // Fallback on earlier versions
                    }
            }
        
          
        
    }
    @IBAction func walleteCreateAction(_ sender: Any) {
        walletSaveLabel.isHidden = false
        walletCreateBtn .setTitle("Add Additional Wallet", for: .normal)
        walletContinueBtn.isEnabled = true
    }
    @IBAction func ReSubmitBtnAct(_ sender: Any) {
        imageUpload = true

        let alert = UIAlertController.init(title: "Upload Image", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction.init(title: "Select Picture", style: .default) { (action) in
            self.selectPictureFromLibrary()
        }
        
        let action2 = UIAlertAction.init(title: "Capture Image", style: .default) { (action) in
            self.captureProfileImage()
        }
        
        let action3 = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        self.present(alert, animated: true, completion: nil)

    }
    func selectPictureFromLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated:true)
    }
    
    func captureProfileImage() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
             imagePicker.sourceType = UIImagePickerController.SourceType.camera
        }
       
        imagePicker.allowsEditing = false
        
        self.present(imagePicker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
        var image : UIImage!

        if let img = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as? UIImage
        {
            image = img

        }
        else if let img = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage
        {
            image = img
        }
        let imageData = resize(image)

        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            data.updateValue(imageData, forKey: "kycDocument")

            let (url, method, param) = APIHandler().updloadDocument(params: data)

            AF.request(url, method: method, parameters: param, encoding: JSONEncoding.default,headers: headers).validate().responseJSON { response in
                switch response.result {

                case .success(let value):
                    
                    if let value = value as? [String:Any] {
                        if let sucessMessage = value["message"] as? String {
                            self.kycStatus.text = sucessMessage
                            self.kycStatus.textColor = UIColor.green

                        }
                    }
                case .failure(let error):
                    print(error)
                }
                SVProgressHUD.dismiss()
            }
        } else {
            SVProgressHUD.dismiss()
        }
        
        picker.dismiss(animated: true,completion: nil)

 }
    
    func resize(_ image: UIImage) -> String {
            var actualHeight = Float(image.size.height)
            var actualWidth = Float(image.size.width)
            let maxHeight: Float = 300.0
            let maxWidth: Float = 400.0
            var imgRatio: Float = actualWidth / actualHeight
            let maxRatio: Float = maxWidth / maxHeight
            let compressionQuality: Float = 0.25
            //50 percent compression
            if actualHeight > maxHeight || actualWidth > maxWidth {
                if imgRatio < maxRatio {
                    //adjust width according to maxHeight
                    imgRatio = maxHeight / actualHeight
                    actualWidth = imgRatio * actualWidth
                    actualHeight = maxHeight
                }
                else if imgRatio > maxRatio {
                    //adjust height according to maxWidth
                    imgRatio = maxWidth / actualWidth
                    actualHeight = imgRatio * actualHeight
                    actualWidth = maxWidth
                }
                else {
                    actualHeight = maxHeight
                    actualWidth = maxWidth
                }
            }
            let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
            UIGraphicsBeginImageContext(rect.size)
            image.draw(in: rect)
            let img = UIGraphicsGetImageFromCurrentImageContext()
            let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
            UIGraphicsEndImageContext()
            let imagebase64String = imageData?.base64EncodedString()
                    print(imagebase64String!)
            //        let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
            return imagebase64String!
    //        return UIImage(data: imageData!) ?? UIImage()
        }

    @IBAction func continueAct(_ sender: Any) {
        infoView.isHidden = true
        kycMainView.isHidden = true
        walletCreateView.isHidden = false
        progressBar.currentTab = 4
        progressBar.activeImages = [
          UIImage(named: "green tick")!,
          UIImage(named: "green tick")!,
          UIImage(named: "green tick")!,
          UIImage(named: "green4")!,
        ]
        self.getWallet()
        self.getAccount()
    }
    func getWallet(){
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            var (url, method, param) = APIHandler().getWalletdata(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.getWalletObj = Mapper<getwalletObject>().map(JSON: value)
                      
                        if let walletbalance = self.getWalletObj?.data?.sila_balance{
                            self.walletbalanceLbl.attributedText = getHBuckString(value: "\(walletbalance)")
                            if walletbalance <= 10{
//                                AlertView.shared.showAlert(view: self, title: "Balance Info!!!", description: "Your balance is low transfer from bank to wallet")
//                                self.appDelegate?.scheduleNotification(notificationType: "Stay Possitive")

                            }
                        }
                        
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
 
    func getAccount(){
            if Reachability.isConnectedToNetwork() {
             SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
                data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            var (url, method, param) = APIHandler().getAccountdata(params: data)
           
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                        SVProgressHUD.dismiss()
                        switch response.result {
                        case .success(let value):
                            if  let value = value as? [String:Any] {
                                
                                self.getAccountobj = Mapper<getsilaAccountData>().map(JSON: value)
                                if self.getAccountobj?.data?.count == 0{
                                    self.stepProgressMainBackView.isHidden = false
                                    self.stepProgressBackViewHeightConstrain.constant = 80
                                    self.accountDetailMainView.isHidden = true
                                    self.nobankLinkStack.isHidden = false
                                    self.linkbankDetailsStack.isHidden = true
                                    self.walletContinueBtn.isHidden = false
                                    self.servicesMainView.isHidden = true
                                    self.deleteAccount.isHidden = true
                                    self.getPlaidLinkToken()
                                }else{
                                    self.stepProgressMainBackView.isHidden = true
                                    self.stepProgressBackViewHeightConstrain.constant = 0
                                    self.accountDetailMainView.isHidden = false
                                    self.deleteAccount.isHidden = false
                                    self.nobankLinkStack.isHidden = true
                                    self.linkbankDetailsStack.isHidden = false
                                    self.walletContinueBtn.isHidden = false
                                    self.accountDetailTableView.reloadData()
                                    self.servicesMainView.isHidden = false
                                    self.AddMoneyView.isHidden = false
                                    self.addMoneySeperator.backgroundColor = UIColor.harmoneyBlueColor
                                    self.transferToUserView.isHidden = true
                                    self.transferUserSeperator.backgroundColor = UIColor.lightGray
                                    self.redeemSeperator.backgroundColor = UIColor.lightGray
                                    self.walletToBankView.isHidden = true
              
                                }
                            }
                        case .failure(let error):
                            print(error)
                        }
                }
            }
      
        }
    func getPlaidLinkToken(){
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
            var (url, method, param) = APIHandler().getPlaidLinkToken(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        
                        self.getLinkToken = Mapper<getPlaidTokenObject>().map(JSON: value)
                        UserDefaults.standard.set(self.getLinkToken?.data?.link_token, forKey: "plaidLinkToken")
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
    @IBAction func refreshStatus(_ sender: Any) {
        //self.nameRegister.text = self.profileUpdate.name
        self.refreshStatus()
    }
    @IBAction func requestKyc(_ sender: Any) {

    
        self.requestKyc()
   
       
    }
    
    func refreshStatus() {
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
       
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue("iscljlpyxk", forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
        var (url, method, param) = APIHandler().paymentCheckKYC(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            print("value = ", value)
                            self.checkkycObj = Mapper<checkKYCObject>().map(JSON: value)
                            let string = self.checkkycObj?.data?.verification_status
                            if string == "passed" {
                                self.kycStatus.text = "Passed ID verification"
                                self.kycStatus.textColor = UIColor.green
                                self.continueBtn.isEnabled = true
                                self.continueBtn.backgroundColor = UIColor.harmoneyBlueColor
                                self.ReSubmitBtn.isEnabled = false
                                self.ReSubmitBtn.backgroundColor = UIColor.lightGray

                            }else{
                        
                                self.kycStatus.text = string
                                self.kycStatus.textColor = UIColor.green
                                self.ReSubmitBtn.isEnabled = true
                                self.ReSubmitBtn.backgroundColor = UIColor.harmoneyBlueColor

                            }
                      
                        }
                    case .failure(let error):
                        print(error)
                        self.kycStatus.text = "Failure" + error.localizedDescription
                        self.kycStatus.textColor = UIColor.green
                    }
            }
        }
  

    }
    
    
    
    func requestKyc() {
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
        var (url, method, param) = APIHandler().paymentrequestKYC(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.kycObj = Mapper<requestKYCobject>().map(JSON: value)
                            let string = self.kycObj?.data?.message
                            if ((string?.contains("submitted for KYC review")) != nil) {
                                self.kycStatus.text = "Submitted for KYC review"
                                self.kycStatus.textColor = UIColor.harmoneyBlueColor
                                self.continueBtn.isEnabled = false
                                self.ReSubmitBtn.isEnabled = true
                                self.ReSubmitBtn.backgroundColor = UIColor.harmoneyBlueColor

                            }
                      
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
    
    
    }
    @IBAction func registerAction(_ sender: Any) {
        
        if let cf = currentTF{
            cf.resignFirstResponder()
        }
        
        if (profileUpdate.name?.count == 0 || profileUpdate.name == nil){
            AlertView.shared.showAlert(view: self, title: "First name", description: "Please enter your first name")
            return
            
        }else if (profileUpdate.lastName?.count == 0 || profileUpdate.lastName == nil){
            AlertView.shared.showAlert(view: self, title: "Last name", description: "Please enter your last name")
            return
            
        }else if (profileUpdate.address?.count == 0 || profileUpdate.address == nil){
            AlertView.shared.showAlert(view: self, title: "Address", description: "Please enter your address")
            return
            
        }else if (profileUpdate.city?.count == 0 || profileUpdate.city == nil){
            AlertView.shared.showAlert(view: self, title: "City", description: "Please enter your city")
            return
            
        }else if (profileUpdate.zip?.count ?? 0 < 5  || profileUpdate.zip == nil){
            AlertView.shared.showAlert(view: self, title: "Zip Code", description: "Please valid enter your zip code")
            return
            
        }else if ((profileUpdate.ssn?.count ?? 0 < 9 ) || profileUpdate.ssn == nil){
            AlertView.shared.showAlert(view: self, title: "Social Security Number", description: "Please valid enter social security number")
            return
            
        }else if (profileUpdate.dateOfBirthForSila?.count == 0 || profileUpdate.dateOfBirthForSila == nil ){
            AlertView.shared.showAlert(view: self, title: "Date of Birth", description: "Please enter your date of birth")
            return
            
        }else if ((!(profileUpdate.email?.isEmpty ?? false) && !(profileUpdate.email?.isEmail ?? false)) || (profileUpdate.email == nil) || (profileUpdate.email == "")){
            AlertView.shared.showAlert(view: self, title: "Email", description: "Please enter valid Email Id")
            return
        }else if (profileUpdate.mobileNumber?.count == 0 || profileUpdate.mobileNumber == nil){
            AlertView.shared.showAlert(view: self, title: "Phone Number", description: "Please enter phone number")
            return
            
        }
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            data.updateValue( getEncryptedString(plainText: profileUpdate.name) , forKey: "firstName")
            data.updateValue(getEncryptedString(plainText: profileUpdate.lastName), forKey: "lastName")
            data.updateValue( getEncryptedString(plainText: "123 Main Street") , forKey: "address")
            data.updateValue(getEncryptedString(plainText: "Anytwon"), forKey: "city")
//            stateCode
            let stateCode = UserDefaults.standard.value(forKey: "stateCode")
            let stCode = stateCode as? String ?? "NY"
            data.updateValue(getEncryptedString(plainText: stCode) , forKey: "state")
            data.updateValue(getEncryptedString(plainText: profileUpdate.ssn), forKey: "ssn")
            data.updateValue(getEncryptedString(plainText:  profileUpdate.mobileNumber), forKey: "phone")
            data.updateValue(getEncryptedString(plainText: profileUpdate.zip), forKey: "zip")
            data.updateValue(getEncryptedString(plainText: profileUpdate.dateOfBirthForSila), forKey: "dateOfBirth")
            data.updateValue( getEncryptedString(plainText: profileUpdate.email) , forKey: "email")
            data.updateValue(getEncryptedString(plainText: ""), forKey: "address2")
            data.updateValue(getEncryptedString(plainText: "your_address_alias"), forKey: "addressAlias")
            data.updateValue(getEncryptedString(plainText: "US") , forKey: "country")
            data.updateValue(getEncryptedString(plainText: "your_contact_alias") , forKey: "contactAlias")
            if let walletname = profileUpdate.name
            {
                data.updateValue(getEncryptedString(plainText: "\(walletname)'s wallet") , forKey: "cryptoAlias")
            }
            
            data.updateValue("", forKey: "cryptoAddress")
            data.updateValue("individual", forKey: "type")
            
            
            let (url, method, param) = APIHandler().paymentRegisterforSila(params: data)
            
            AF.request(url, method: method, parameters: param, encoding: JSONEncoding.default,headers: headers).validate().responseJSON { response in
                switch response.result {
                
                case .success(let value):
                    //                    if  value is [String:Any] {
                    //                        self.profileParentVC?.editContainerView.isHidden = true
                    //                        self.profileParentVC?.updateUIAfterEdit()
                    //                    }
                    if  let value = value as? [String:Any] {
                        self.regObj = Mapper<registerobject>().map(JSON: value)
                        if self.regObj?.data?.status == "FAILURE"{
                            self.view.toast("Register FAILURE")
                        }else{
                            self.infoView.isHidden = true
                            self.kycMainView.isHidden = false
                           
                            self.walletCreateView.isHidden = true
                            UserDefaults.standard.set(self.regObj?.data?.walletPrivateKey, forKey: "walletPrivateKey")
                            
                            self.nameRegister.text = self.profileUpdate.name
                            self.progressBar.currentTab = 3
                            self.progressBar.activeImages = [
                                UIImage(named: "green tick")!,
                                UIImage(named: "green tick")!,
                                UIImage(named: "green3")!,
                                UIImage(named: "gray4")!,
                            ]
                            
                            self.requestKyc()
                        }
                        
                    }
                case .failure(let error):
                    print(error)
                }
                SVProgressHUD.dismiss()
            }
        } else {
            SVProgressHUD.dismiss()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 10{
            return 4
        }
            return 10
      
       }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        if tableView.tag == 10
        {
            return 40
            
        }
        return 375/5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
    UITableViewCell {
        
        if tableView.tag == 10
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "accountDetailsTableViewCell", for: indexPath) as! accountDetailsTableViewCell
            if indexPath.row == 0
            {
                cell.customLbl.text = "Account"
                cell.customTextField.text = self.getAccountobj?.data?.last?.account_number
            }else if indexPath.row == 1
            {
                cell.customLbl.text = "Name"
                cell.customTextField.text = self.getAccountobj?.data?.last?.account_name
            }else if indexPath.row == 2
            {
                cell.customLbl.text = "Type"
                cell.customTextField.text = self.getAccountobj?.data?.last?.account_type
            }else if indexPath.row == 3
            {
                cell.customLbl.text = "Status"
                cell.customTextField.text = self.getAccountobj?.data?.last?.account_status?.capitalized
                cell.customTextField.textColor = UIColor.harmoneyLightGreenColor
            }
            return cell
            
        }
        
        if indexPath.row == 7{
            //            cell.customTextField.placeholder = "dd/mm/yy"
            //            cell.customTextField.title = "Your Date of Birth"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: DateofBirthTableViewCellIdentifier, for: indexPath) as! DateofBirthTableViewCell
            //                cell.stringDate = profileUpdate.dateOfBirth
            //                cell.parentVc = self
            cell.dateTextField.tag = 100 + indexPath.row
            cell.datePickerView.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
            print("Hello\(cell.datePickerView.minimumDate)")
            
            cell.dateTextField.text = profileUpdate.dateOfBirthForSila

//            let dateOfBirth = self.getInformationForPaymentObject?.data?.user?.dateOfBirth
//            if dateOfBirth != nil{
//                cell.dateTextField.text = dateOfBirth
//            }else{
//                cell.dateTextField.text = profileUpdate.dateOfBirth
//            }

            cell.dateTextField.addTarget(self, action: #selector(updateValuesOnUpdateProfileObject(textField:)), for: .allEvents)
            
            return cell
            
        }
        if indexPath.row == 3{
//            else  if indexPath.row == 3{
//                cell.customTextField.placeholder = "Select City"
//                cell.customLabel.text = "City"
//                cell.customTextField.text = profileUpdate.city
//            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cityTableViewCellIdentifier, for: indexPath) as! CityTableViewCell
           
            cell.customTextField.tag = 100 + indexPath.row
            
            let country = self.getInformationForPaymentObject?.data?.user?.country
            if country != nil{
                cell.customTextField.text = country
            }else{
                let registerCity = UserDefaults.standard.value(forKey: "registerCity")
                cell.customTextField.text = registerCity as? String
            }
            
            cell.customTextField.addTarget(self, action: #selector(updateValuesOnUpdateProfileObject(textField:)), for: .allEvents)
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: registerTableViewCellIdentifier, for: indexPath) as! registerTableViewCell
        
        
        cell.customTextField.tag = 100 + indexPath.row
        cell.customTextField.delegate = self
        cell.customTextField.keyboardType = .default
        
        cell.customTextField.addTarget(self, action: #selector(updateValuesOnUpdateProfileObject(textField:)), for: .allEvents)
        if indexPath.row == 0
        {
            cell.customTextField.placeholder = "Enter first name"
            cell.customLabel.text = "First Name*"
            let firstName = self.getInformationForPaymentObject?.data?.user?.firstName
            if firstName != nil{
                cell.customTextField.text = firstName
                profileUpdate.name = firstName
            }else{
                cell.customTextField.text = profileUpdate.name
            }
        }else  if indexPath.row == 1{
            cell.customTextField.placeholder = "Enter last name"
            cell.customLabel.text = "Last Name*"
            let lastName = self.getInformationForPaymentObject?.data?.user?.lastName
            if lastName != nil{
                cell.customTextField.text = lastName
                profileUpdate.lastName = lastName
            }else{
                cell.customTextField.text = profileUpdate.lastName
            }
        }else  if indexPath.row == 2{
            cell.customTextField.placeholder = "Enter Street Name"
            cell.customLabel.text = "Address*"
            let address = self.getInformationForPaymentObject?.data?.user?.address
            if address != nil{
                cell.customTextField.text = address
                profileUpdate.address = address
            }else{
                cell.customTextField.text = profileUpdate.address
            }
        }else  if indexPath.row == 4{
            cell.customTextField.placeholder = "Enter City"
            cell.customLabel.text = "City*"
            let state = self.getInformationForPaymentObject?.data?.user?.state
            if state != nil{
                cell.customTextField.text = state
                profileUpdate.state = state
            }else{
                cell.customTextField.text = profileUpdate.state
            }
        }else  if indexPath.row == 5{
            cell.customTextField.placeholder = "Enter zip code"
            cell.customLabel.text = "Zip Code*"
            cell.customTextField.keyboardType = .numberPad
            let zip = self.getInformationForPaymentObject?.data?.user?.zip
            if zip != nil{
                cell.customTextField.text = zip
                profileUpdate.zip = zip
            }else{
                cell.customTextField.text = profileUpdate.zip
            }
        }else  if indexPath.row == 6{
            cell.customTextField.placeholder = "Enter SSN Number"
            cell.customLabel.text = "Social Security Number*"
            cell.customTextField.keyboardType = .numberPad
            let ssn = self.getInformationForPaymentObject?.data?.user?.ssn
            if ssn != nil{
                cell.customTextField.text = ssn
                profileUpdate.ssn = ssn
            }else{
                cell.customTextField.text = profileUpdate.ssn
            }
        }else  if indexPath.row == 8{
            cell.customTextField.placeholder = "Enter email Address"
            cell.customLabel.text = "Email Address*"
            cell.customTextField.keyboardType = .emailAddress
            let email = self.getInformationForPaymentObject?.data?.user?.email
            if email != nil{
                cell.customTextField.text = email
                profileUpdate.email = email
            }else{
                cell.customTextField.text = profileUpdate.email
            }
        }else  if indexPath.row == 9{
            cell.customTextField.placeholder = "Enter Phone Number"
            cell.customLabel.text = "Phone Number*"
            cell.customTextField.keyboardType = .phonePad
            let phone = self.getInformationForPaymentObject?.data?.user?.phone
            if phone != nil{
                cell.customTextField.text = phone
                profileUpdate.mobileNumber = phone
            }else{
                cell.customTextField.text = profileUpdate.mobileNumber
            }
        }
        
        return cell
        
    }
  
    @objc func updateValuesOnUpdateProfileObject(textField:UITextField) {
        currentTF = textField
        let string = textField.text
        switch textField.tag {
        case 100:
            profileUpdate.name = string
        case 101:
            profileUpdate.lastName = string
        case 102:
            profileUpdate.address = string
        case 103:
            profileUpdate.state = string
        case 104:
            profileUpdate.city = string
        case 105:
            profileUpdate.zip = string
        case 106:
            profileUpdate.ssn = string
        case 107:
            profileUpdate.dateOfBirthForSila = string
        case 108:
            profileUpdate.email = string
        case 109:
            profileUpdate.mobileNumber = string
            
        default:
            print(string!)
        }
    }

}
extension MyFriendsViewController: PlaidTransactionViewControllerDelegate {
    func didTapCloseButton(customAlertViewController: PlaidTransactionViewController) {
        customAlertViewController.dismiss(animated: true, completion: nil)

    }
    
}

extension MyFriendsViewController: SubAccountViewControllerDelegate {
    func didTapCloseButton(customAlertViewController: SubAccountViewController) {
        customAlertViewController.dismiss(animated: true, completion: nil)

    }
    
}

extension UIView{
    func cardView() -> Void {
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.5
    }
}

// MARK: - ThumbsAlertViewControllerDelegate methods

extension MyFriendsViewController: ThumbsAlertViewControllerDelegate {
    func didTapThumbsUpButton(thumbsAlertViewController: ThumbsAlertViewController) {
        if alertString == "issueWallet"
        {
            issueAct()

        }else{
            redeemAct()
        }
        thumbsAlertViewController.dismiss(animated: true, completion: nil)

    }
    
    func didTapThumbsDownButton(thumbsAlertViewController: ThumbsAlertViewController) {
        thumbsAlertViewController.dismiss(animated: true, completion: nil)
    }
}
 



