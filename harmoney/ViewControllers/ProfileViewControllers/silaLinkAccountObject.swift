//
//  silaLinkAccountObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 13/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct silaLinkAccountObject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : silaLinkAccountObjectData?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }

struct silaLinkAccountObjectData : Mappable {
    

    var reference: String?
    var status: String?
    var message : String?
    var success : Bool?
    var account_name : String?
    var account_owner_name : String?
    var entity_name : String?
    var match_score : String?
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        message          <- map["message"]
        reference           <- map["reference"]
        success            <- map["success"]
        status    <- map["status"]
        account_owner_name         <- map["account_owner_name"]
        entity_name    <- map["entity_name"]
        match_score    <- map["match_score"]
    }
}



