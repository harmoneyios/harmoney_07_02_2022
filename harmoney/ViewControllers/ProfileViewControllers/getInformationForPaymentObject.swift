//
//  getInformationForPaymentObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 14/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct getInformationForPaymentObject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : getInformationForPaymentObjectData?
    
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }



    struct getInformationForPaymentObjectData : Mappable {

            var guid : String?
            var id: String?
            var processStage: Int?
            var processStageName: String?
            var kycDateTime : String?
            var lastUpdatedDateTime: String?
            var user : userData?
            var kidWallet: [kidWalletData]?
     
        init?(map: Map) {

        }
        mutating func mapping(map: Map) {
             guid          <- map["guid"]
            processStage           <- map["processStage"]
             id            <- map["_id"]
            processStageName    <- map["processStageName"]
            kycDateTime         <- map["kycDateTime"]
            lastUpdatedDateTime      <- map["lastUpdatedDateTime"]
            user         <- map["user"]
            kidWallet         <- map["kidWallet"]
 
        }
    }

struct kidWalletData : Mappable {
    var kidName : String?
    var kidGuid : String?
    var kidMobileNumber: String?
    var kidMemberId: String?
    var kidImage: String?
    var walletName: String?
    var walletPrivateKey : String?
    var walletAddress: String?

    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        kidName          <- map["kidName"]
        kidGuid           <- map["kidGuid"]
        kidMobileNumber            <- map["kidMobileNumber"]
        kidMemberId    <- map["kidMemberId"]
        kidImage         <- map["kidImage"]
        walletName      <- map["walletName"]
        walletPrivateKey         <- map["walletPrivateKey"]
        walletAddress        <- map["walletAddress"]

    }
}

struct userData : Mappable {
    var handle : String?
    var city_encypted : String?
    var zip_encypted: String?
    var state_encypted: String?
    var address_encypted: String?
    var addresAlias_encypted: String?
    var email_encypted: String?
    var cryptoAlias_encypted: String?
    var dateOfBirth_encypted: String?
    var firstName_encypted: String?
    var lastName_encypted: String?
    var ssn_encypted: String?
    var country_encypted: String?
    var address2_encypted: String?
    var addressAlias_encypted: String?
    
    var city : String?{
        get{
        return getDecryptedString(cipherText: self.city_encypted)
              }
              set{
              
              }
    }
    var zip: String?{
        get{
        return getDecryptedString(cipherText: self.zip_encypted)
              }
              set{
              
              }
    }
    var state: String?{
        get{
        return getDecryptedString(cipherText: self.state_encypted)
              }
              set{
              
              }
    }
    var address: String?{
        get{
        return getDecryptedString(cipherText: self.address_encypted)
              }
              set{
              
              }
    }
    var addresAlias: String?{
        get{
        return getDecryptedString(cipherText: self.addresAlias_encypted)
              }
              set{
              
              }
    }
    var phone : String?
    var email: String?{
        get{
        return getDecryptedString(cipherText: self.email_encypted)
              }
              set{
              
              }
}
    var contactAlias: String?
    var cryptoAddress: String?
    var cryptoAlias: String?{
        get{
        return getDecryptedString(cipherText: self.cryptoAlias_encypted)
              }
              set{
              
              }
    }
    var dateOfBirth: String?{
        get{
        return getDecryptedString(cipherText: self.dateOfBirth_encypted)
              }
              set{
              
              }
    }
    var firstName: String?{
        get{
        return getDecryptedString(cipherText: self.firstName_encypted)
              }
              set{
              
              }
    }
    var lastName : String?{
        get{
        return getDecryptedString(cipherText: self.lastName_encypted)
              }
              set{
              
              }
}
    var ssn : String?{
        get{
        return getDecryptedString(cipherText: self.ssn_encypted)
              }
              set{
              
              }
    }
    var entity_name : String?
    var business_type : String?
    var business_website : String?
    var doing_business_as : String?
    var naics_code : String?
    var ein : String?
    var country : String?{
        get{
        return getDecryptedString(cipherText: self.country_encypted)
              }
              set{
              
              }
    }
    var businessTypeUuid : String?
    var type : String?
    var address2 : String?{
        get{
        return getDecryptedString(cipherText: self.address2_encypted)
              }
              set{
              
              }
    }
    var addressAlias : String?
    {
        get{
        return getDecryptedString(cipherText: self.addressAlias_encypted)
              }
              set{
              
              }
    }
    var deviceFingerprint : String?
    var smsOptIn : String?
    var walletPrivateKey : String?
    var processStage : Int?
    var processStageName : String?
    var registerDateTime : String?
    var lastUpdatedDateTime : String?
    
    
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        handle          <- map["handle"]
        city_encypted           <- map["city"]
        zip_encypted            <- map["zip"]
        state_encypted    <- map["state"]
        address_encypted         <- map["address"]
        addresAlias_encypted      <- map["addresAlias"]
        email_encypted         <- map["email"]
        phone        <- map["phone"]
        email_encypted           <- map["email"]
        contactAlias        <- map["contactAlias"]
        cryptoAddress    <- map["cryptoAddress"]
        cryptoAlias_encypted      <- map["cryptoAlias"]
        dateOfBirth_encypted    <- map["dateOfBirth"]
        firstName_encypted   <- map["firstName"]
        lastName_encypted     <- map["lastName"]
        ssn_encypted  <- map["ssn"]
        entity_name   <- map["entity_name"]
        business_type  <- map["business_type"]
        business_website  <- map["business_website"]
        doing_business_as  <- map["doing_business_as"]
        naics_code  <- map["naics_code"]
        ein  <- map["ein"]
        country_encypted  <- map["country"]
        businessTypeUuid  <- map["businessTypeUuid"]
        type  <- map["type"]
        address2_encypted  <- map["address2"]
        addressAlias_encypted  <- map["addressAlias"]
        deviceFingerprint  <- map["deviceFingerprint"]
        smsOptIn  <- map["smsOptIn"]
        walletPrivateKey  <- map["walletPrivateKey"]
        processStage  <- map["processStage"]
        processStageName  <- map["processStageName"]
        registerDateTime  <- map["registerDateTime"]
        lastUpdatedDateTime  <- map["lastUpdatedDateTime"]
    }
}
