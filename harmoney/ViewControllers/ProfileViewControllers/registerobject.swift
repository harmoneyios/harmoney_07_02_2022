//
//  registerobject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 12/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct registerobject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : silaregisterObjectData?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }

    struct silaregisterObjectData : Mappable {
            var message : String?
            var reference: String?
            var status: String?
            var walletPrivateKey: String?
            var success : Bool?
        
        init?(map: Map) {

        }
        mutating func mapping(map: Map) {
            message          <- map["message"]
            reference           <- map["reference"]
            success            <- map["success"]
            status    <- map["status"]
            walletPrivateKey         <- map["walletPrivateKey"]

        }
    }
