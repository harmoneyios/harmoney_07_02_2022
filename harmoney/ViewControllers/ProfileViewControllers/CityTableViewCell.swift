//
//  CityTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 20/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SVProgressHUD
import ObjectMapper
import Toast_Swift
import IQKeyboardManagerSwift
import SDWebImage

class CityTableViewCell: UITableViewCell, UITextFieldDelegate {
    var getCityInformationObjectValue: getCityInformationObject?
    var cityArray = [String]()
    var cityValueArray = [String]()
    var profileUpdate = ProfileUpdateObject()
    @IBOutlet weak var customTextField: DropDown!{
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            customTextField.leftView = paddingView
            customTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            customTextField.attributedPlaceholder = NSAttributedString(string: "Select State", attributes:attributes)
        }
    }
    @IBOutlet weak var customLabel: UILabel!
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.getCityInformation()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupDropDowns() {
      
       customTextField.optionArray = self.cityArray
        customTextField.text = ""
        customTextField.selectedRowColor =  #colorLiteral(red: 0.9294117647, green: 0.937254902, blue: 0.9529411765, alpha: 1)
        customTextField.checkMarkEnabled = false
        customTextField.arrowColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        customTextField.isSearchEnable = false
        customTextField.adjustsFontSizeToFitWidth = true
        customTextField.minimumFontSize = 12
        customTextField.delegate = self
        customTextField.didSelect { (string, int1, int2) in
            self.customTextField.text = "\(string)"
//            self.profileUpdate.city = self.customTextField.text
            UserDefaults.standard.set(self.customTextField.text, forKey: "registerCity")
           UserDefaults.standard.set(self.cityValueArray[int1], forKey: "stateCode")
            
        }
        
    }
    
    
    func getCityInformation(){
        
        self.cityValueArray.removeAll()
        self.cityArray.removeAll()
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            var (url, method, param) = APIHandler().getCityInformation(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.getCityInformationObjectValue = Mapper<getCityInformationObject>().map(JSON: value)
                        print(self.getCityInformationObjectValue!)
                       
                        if let assigntomemberDataList = self.getCityInformationObjectValue?.data{
                            for objMember in assigntomemberDataList{
                               
                                self.cityArray.append(objMember.city ?? "AnyCity")
                                self.cityValueArray.append(objMember.value ?? "AY")
                                
                            }
                        }
                        self.setupDropDowns()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}
