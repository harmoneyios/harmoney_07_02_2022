//
//  FamilyMembersView.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 02/12/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

protocol ResendInviteDelegate {
    func didTapResendInvite(inviteParentView: FamilyMembersView, inviteData: InviteData)
    func didTapCancelInvite(inviteParentView: FamilyMembersView, inviteData: InviteData)
    func showInfo()
    func changePrimary(inviteParentView: FamilyMembersView, inviteData: InviteData)
    func didTapRemoveMember(inviteParentView: FamilyMembersView, inviteData: InviteData)
}

class FamilyMembersView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var familyListTV: UITableView!
    @IBOutlet weak var familyListView: UIView!
    @IBOutlet weak var addfamilyClick: UIButton!
    var delegate : ResendInviteDelegate?
    var  customAlertViewController: CustomAlertViewController?
    var resendValue:Bool = false
    
    let refresh = UIRefreshControl()
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    
    //MARK: - Lifecycle methods

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    @objc func formInviteData(btn : UIButton)  {
        if resendValue == false
        {
            return;
        }
        let invObj = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?[btn.tag]
        
        let inviteData = InviteData(memberId: invObj?.memberId, memberPhoto: "", memberName_encrypted: invObj?.memberName_encrypted, memberMobile_encrypted: invObj?.memberMobile_encrypted, memberRelation_encrypted: invObj?.memberRelation_encrypted , memberEmail_encrypted: invObj?.memberEmail_encrypted, inviteStatus: invObj?.memberAccept)
    
   
        delegate?.didTapResendInvite(inviteParentView: self, inviteData: inviteData)
    }
    @objc func viewMemberDetails(btn : UIButton)  {
        let invObj = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?[btn.tag]
        
        let inviteData = InviteData(memberId: invObj?.memberId, memberPhoto: "", memberName_encrypted: invObj?.memberName_encrypted, memberMobile_encrypted: invObj?.memberMobile_encrypted, memberRelation_encrypted: invObj?.memberRelation_encrypted , memberEmail_encrypted: "", inviteStatus: invObj?.memberAccept)
        
        delegate?.didTapRemoveMember(inviteParentView: self, inviteData: inviteData)
    }
    @objc func formPrimaryData(btn : UIButton)  {
        let invObj = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?[btn.tag]
        
        let inviteData = InviteData(memberId: invObj?.memberId, memberPhoto: "", memberName_encrypted: invObj?.memberName_encrypted, memberMobile_encrypted: invObj?.memberMobile_encrypted, memberRelation_encrypted: invObj?.memberRelation_encrypted , memberEmail_encrypted: "", inviteStatus: invObj?.memberAccept)
    
        
        delegate?.changePrimary(inviteParentView: self, inviteData: inviteData)
    }
    @objc func cancelInviteData(btn : UIButton)  {
        if resendValue == false
        {
            return;
        }
        let invObj = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?[btn.tag]
        
        let inviteData = InviteData(memberId: invObj?.memberId, memberPhoto: "", memberName_encrypted: invObj?.memberName_encrypted, memberMobile_encrypted: invObj?.memberMobile_encrypted, memberRelation_encrypted: invObj?.memberRelation_encrypted , memberEmail_encrypted: "", inviteStatus: invObj?.memberAccept)
   
        
        delegate?.didTapCancelInvite(inviteParentView: self, inviteData: inviteData)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    func commonInit() {
        Bundle.main.loadNibNamed("FamilyMembersView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
               
        familyListTV.register(UINib.init(nibName: "FamilyMemberCell", bundle: nil), forCellReuseIdentifier: "FamilyMemberCell")
        let headerNib = UINib.init(nibName: "DemoHeaderView", bundle: Bundle.main)
        familyListTV.register(headerNib, forHeaderFooterViewReuseIdentifier: "DemoHeaderView")
        familyListTV.refreshControl = refresh
        refresh.addTarget(self, action: #selector(getFamilyListMembers), for: .valueChanged)
//        var memberId:[String] = []
//        if let assigntomemberDataList = HarmonySingleton.shared.dashBoardData.data?.invitedRequest{
//            for objMember in assigntomemberDataList{
//                if objMember.memberAccept == 1 {
//                    if objMember.isPrimary == 1 {
//                        if let fromguid = objMember.fromGuid{
//                            memberId.append(fromguid)
//                        }
//                    }
//                }
//            }
//        }
//        if memberId.count == 0{
//            if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
//                if let assigntomemberDataList = HarmonySingleton.shared.dashBoardData.data?.invitedRequest{
//                    for objMember in assigntomemberDataList{
//                        if objMember.memberAccept == 1 {
//                            let inviteData = InviteData(memberId:objMember.memberId, memberName: objMember.memberName, memberPhoto: "", memberMobile: objMember.memberMobile, memberRelation: objMember.memberRelation ?? "", memberEmail: "", inviteStatus:objMember.memberAccept )
//                            delegate?.changePrimary(inviteParentView: self, inviteData:inviteData)
//                            return;
//
//                        }
//
//                    }
//                }
//            }
//
//        }
    }
        @objc func getFamilyListMembers()  {
        getDashBoardData()
    }
    func getDashBoardData() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")
        if Reachability.isConnectedToNetwork(){
            var (url, method, param) = APIHandler().dashboard(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    self.refresh.endRefreshing()
                    if  let value = value as? [String:Any] {
                        HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: value)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.onLoadSetUp()
                        }
                    }
                case .failure(let error):
                    self.refresh.endRefreshing()
                    print(error)
                }
            }
        }else{
            self.refresh.endRefreshing()
            self.toast("Internet Connection not Available!")
        }
        
    }
    
    func onLoadSetUp()  {
        if HarmonySingleton.shared.dashBoardData.data?.invitedRequest?.count ?? 0 > 0{
                   familyListView.isHidden = false
               }else{
                   familyListView.isHidden = true
               }
        familyListTV.reloadData()
    }
    @IBAction func addClick(_ sender: Any) {
        self.isHidden = true
//        familyListView.isHidden = false
    }
    @IBAction func addMemberClick(_ sender: Any) {
        self.isHidden = true
    }
    
    func setProfilePic(frstName: String?, lstName: String?, profileImageView: UIImageView){
//        let menuBackSystem =  UIColor(red:  41.0/255.0, green:  72.0/255.0, blue:  100.0/255.0, alpha: 1.0)
         if let firstName = frstName, let lastName = lstName {
             profileImageView.setImageWith(firstName + " " + lastName, color: ConstantString.menuBackSystemProfilepic)
         } else {
             if let dispName = frstName {
                 profileImageView.setImageWith(dispName, color: ConstantString.menuBackSystemProfilepic)
             }
         }
     }

}

extension FamilyMembersView : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        HarmonySingleton.shared.dashBoardData.data?.invitedRequest?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyMemberCell", for: indexPath) as! FamilyMemberCell
        let invObj = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?[indexPath.row]
        cell.nameLabel.text = invObj?.memberName
        if invObj?.memberPhoto?.count ?? 0 == 0

        {
      
            self.setProfilePic(frstName: invObj?.memberName, lstName: nil, profileImageView: cell.profileImage)

          
        }else{
            
                      //  if let url = URL(string: invObj?.memberPhoto ?? "") {

//                            if url.absoluteString.range(of: "jpg") != nil {

                              //  self.setProfilePic(frstName: invObj?.memberName, lstName: nil, profileImageView: cell.profileImage)
//                            }

                       // }else{
                            cell.profileImage?.sd_setImage(with: URL.init(string: invObj?.memberPhoto ?? ""))
                      //  }

        }
       
        if let value = invObj?.memberRelation{
            if let valueMobile = invObj?.memberMobile{
                let descriptionValue = "\(value) | \(valueMobile)"
                let fullString = NSMutableAttributedString(string: descriptionValue)
                cell.descriptionLabel.attributedText = fullString
            }
        }
        if invObj?.isPrimary == 1{
            cell.primaryLabel.text = "Primary"
            cell.primaryLabel.textColor = UIColor.harmoneyPrimaryColor
            if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
                cell.mainBackView.layer.borderColor = UIColor.harmoneyPrimaryColor.cgColor

            }else{
                cell.mainBackView.layer.borderColor = UIColor.harmoneyNonPrimaryBorderColor.cgColor

            }
            cell.primaryBtn.setBackgroundImage(UIImage.init(named: "primaryTick"), for: .normal)
//            cell.isUserInteractionEnabled = true
           
        }else{
            cell.primaryLabel.text = "Make Primary"
            cell.primaryLabel.textColor = UIColor.harmoneyNonPrimaryColor
            cell.mainBackView.layer.borderColor = UIColor.harmoneyNonPrimaryBorderColor.cgColor
            cell.primaryBtn.setBackgroundImage(UIImage.init(named: "notselect"), for: .normal)
//            cell.isUserInteractionEnabled = false
        }
        let secondicon = NSTextAttachment()
       var secondiconImage : UIImage
//        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
//            cell.primaryBtn.isHidden = false
//            cell.primaryLabel.isHidden = false
//
//        }else{
//            cell.primaryBtn.isHidden = true
//            cell.primaryLabel.isHidden = true
//        }
        
        
        if(invObj?.memberAccept == 1){
           secondiconImage = #imageLiteral(resourceName: "accepted")
            cell.resendBtn.isHidden = true
            cell.cancelBtn.isHidden = true
            cell.statusLabel.isHidden = true
            cell.memberDetailstn.isHidden = false
            cell.memberDetailstn.isUserInteractionEnabled = true
            if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
                cell.primaryBtn.isHidden = false
                cell.primaryLabel.isHidden = false

            }else{
                cell.primaryBtn.isHidden = true
                cell.primaryLabel.isHidden = true

            }

        }else if(invObj?.memberAccept == 2){
            secondiconImage = #imageLiteral(resourceName: "invited")
            cell.resendBtn.isHidden = false
            cell.cancelBtn.isHidden = false
            cell.statusLabel.isHidden = false
            cell.memberDetailstn.isHidden = true
            cell.memberDetailstn.isUserInteractionEnabled = false
            if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
                cell.primaryBtn.isHidden = true
                cell.primaryLabel.isHidden = true
                
            }else{
                cell.primaryBtn.isHidden = true
                cell.primaryLabel.isHidden = true
            }

        }else{
            secondiconImage = #imageLiteral(resourceName: "invited")
            cell.resendBtn.isHidden = false
            cell.cancelBtn.isHidden = false
            cell.statusLabel.isHidden = true
            cell.memberDetailstn.isHidden = true
            cell.memberDetailstn.isUserInteractionEnabled = false
            if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
                cell.primaryBtn.isHidden = true
                cell.primaryLabel.isHidden = true
                
            }else{
                cell.primaryBtn.isHidden = true
                cell.primaryLabel.isHidden = true
            }

        }
        secondicon.bounds = CGRect(x: 0, y: (cell.descriptionLabel.font.capHeight - 12).rounded() / 2, width: 12, height: 12)
        secondicon.image = secondiconImage
        let hrString = NSAttributedString(attachment: secondicon)
//        fullString.append(hrString)
//        cell.descriptionLabel.attributedText = fullString
       
      
        cell.primaryBtn.tag = indexPath.row
        cell.primaryBtn.addTarget(self, action: #selector(formPrimaryData(btn:)), for: .touchUpInside)
        
        if invObj?.memberAccept == 0 && invObj?.isAcceptReject == true{
            resendValue = false
            cell.resendBtn .setTitle("Accept", for: .normal)
            cell.cancelBtn .setTitle("Reject", for: .normal)
            cell.resendBtn.tag = indexPath.row
            cell.resendBtn.addTarget(self, action: #selector(AcceptBtn(btn:)), for: .touchUpInside)
            cell.cancelBtn.tag = indexPath.row
            cell.cancelBtn.addTarget(self, action: #selector(RejectBtn(btn:)), for: .touchUpInside)
        }else{
            resendValue = true
            cell.resendBtn .setTitle("Resend", for: .normal)
            cell.cancelBtn .setTitle("Cancel", for: .normal)
            cell.resendBtn.tag = indexPath.row
            cell.resendBtn.addTarget(self, action: #selector(formInviteData(btn:)), for: .touchUpInside)
            cell.cancelBtn.tag = indexPath.row
            cell.cancelBtn.addTarget(self, action: #selector(cancelInviteData(btn:)), for: .touchUpInside)
        }
       
        cell.memberDetailstn.tag = indexPath.row
        cell.memberDetailstn.addTarget(self, action: #selector(viewMemberDetails(btn:)), for: .touchUpInside)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        cell.primaryImage.isUserInteractionEnabled = false
        cell.primaryImage.addGestureRecognizer(tapGestureRecognizer)
        return cell
    }
    
    @objc func AcceptBtn(btn : UIButton)  {

        let invObj = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?[btn.tag]
        PersonalFitnessManager.sharedInstance.acceptInviteFromFamily(memberId: invObj?.memberId ?? "", FromUserGuid: invObj?.fromGuid ?? "") { (result) in
            switch result {
            case .success(_):
                self.getFamilyListMembers()
                self.familyListTV.reloadData()
            case .failure(let error):
                self.toast(error.localizedDescription)
            }
        }

    }
    
    @objc func RejectBtn(btn : UIButton)  {

        let invObj = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?[btn.tag]
        PersonalFitnessManager.sharedInstance.rejectInviteFromFamily(memberId: invObj?.memberId ?? "", FromUserGuid: invObj?.fromGuid ?? "") { (result) in
            switch result {
            case .success(_):
                self.getFamilyListMembers()
                self.familyListTV.reloadData()
            case .failure(let error):
                self.toast(error.localizedDescription)
            }
        }

    }


    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let vw = UIView()
//        vw.backgroundColor = UIColor.harmoneySectionHeaderColor
//        let lbl = UILabel.init(frame: CGRect.init(x: 40, y: 5, width: 200, height: 30))
//        lbl.text = "Invite Sent"
//        lbl.font = UIFont.init(name: "FuturaPT-Demi", size: 18)
//        lbl.textColor = #colorLiteral(red: 0.1176470588, green: 0.2470588235, blue: 0.4, alpha: 1)
//        vw.addSubview(lbl)
//        return vw
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "DemoHeaderView") as! DemoHeaderView
        headerView.backgroundColor = UIColor.red

//           if let indexdata = self.choreSubCatagoryListData?.data?.chorselist?[section]{
//               headerView.lblTitle.text = indexdata.chorserName
//               headerView.subTaskLbl.text = indexdata.chorserSubTitle
//
//           }

//       headerView.addBtn.tag = section
//
        headerView.addBtn.addTarget(self,
                                    action: #selector(self.showInfo),
                                    for: .touchUpInside)
        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            
            headerView.addBtn.isHidden = false
            headerView.subTaskLbl.isHidden = false
        }else{
            headerView.addBtn.isHidden = true
            headerView.subTaskLbl.isHidden = true
        }
       return headerView
    }
    
    @objc func showInfo() {
        
        delegate?.showInfo()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
        let invObj = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?[indexPath.row]
        if invObj?.isPrimary == 1{
            
            let inviteData = InviteData(memberId: invObj?.memberId, memberPhoto: "", memberName_encrypted: invObj?.memberName_encrypted, memberMobile_encrypted: invObj?.memberMobile_encrypted, memberRelation_encrypted: invObj?.memberRelation_encrypted , memberEmail_encrypted: "", inviteStatus: invObj?.memberAccept)
        
            delegate?.didTapRemoveMember(inviteParentView: self, inviteData: inviteData)
        }else{
//            let invObj = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?[btn.tag]
            
            let inviteData = InviteData(memberId: invObj?.memberId, memberPhoto: "", memberName_encrypted: invObj?.memberName_encrypted, memberMobile_encrypted: invObj?.memberMobile_encrypted, memberRelation_encrypted: invObj?.memberRelation_encrypted , memberEmail_encrypted: "", inviteStatus: invObj?.memberAccept)

            
            delegate?.changePrimary(inviteParentView: self, inviteData: inviteData)
        }
       
    }
    
}
