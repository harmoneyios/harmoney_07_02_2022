//
//  ProfileViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ProfileTabViewController: UIViewController {
    
    typealias InviteState = (Bool, FamilyViewControllerType)
    
    private enum ButtonTags : Int{
        case profile = 104
        case family = 105
        case interest = 106
        case myFriends = 107
        case recap = 108
        case settings = 109
    }
    
    var pageViecontroller  : ProfilePageController?
    
    @IBOutlet weak var navBar: UIView!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var myFamilyBtn: UIButton!
    @IBOutlet weak var myInterestBtn: UIButton!
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var myFriendsBtn: UIButton!
    @IBOutlet weak var recapBtn: UIButton!
    var lineView = UIView()
    
    var inviteState: InviteState?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HarmonySingleton.previousVC = .profile
        self.myFriendsBtn.removeFromSuperview()
        self.recapBtn.removeFromSuperview()
//        self.myFamilyBtn.removeFromSuperview()
        self.myInterestBtn.removeFromSuperview()
//        if HarmonySingleton.shared.dashBoardData.data?.userType == .adult {
//            myFamilyBtn.removeFromSuperview()
//        }
        myFamilyBtn.isHidden = false
        setUpProfileUI()
        
        // Do any additional setup after loading the view.
       changeBorderView(sender: profileBtn)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
        
        if let state = inviteState, state.0  {
            pageViecontroller?.familyVC.type = state.1
            handleButtonClicks(sender: myFamilyBtn)
        } else {
            handleButtonClicks(sender: profileBtn)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        inviteState = nil
    }
    
    func changeBorderView(sender:UIButton) {
        lineView.removeFromSuperview()
        //lineView = UIView(frame: CGRect.init(x: sender.frame.size.width/2 - 6 , y: sender.frame.size.height-3, width: view.frame.size.width / 2 , height:  3))
        lineView.backgroundColor = #colorLiteral(red: 0.1607843137, green: 0.2823529412, blue: 0.4274509804, alpha: 1)
        sender.addSubview(lineView)
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.bottomAnchor.constraint(equalTo: sender.bottomAnchor).isActive = true
        lineView.leadingAnchor.constraint(equalTo: sender.leadingAnchor).isActive = true
        lineView.trailingAnchor.constraint(equalTo: sender.trailingAnchor).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 3).isActive = true
    }
    func setupNavBar(){
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = navBar.bounds
        navBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
        navBar.autoresizesSubviews = true
    }
    
    func setUpProfileUI(){
            
        profileBtn.titleLabel?.font = ConstantString.editTitleFont
        myFamilyBtn.titleLabel?.font = ConstantString.editTitleFont
        myInterestBtn.titleLabel?.font = ConstantString.editTitleFont
        myFamilyBtn.titleLabel?.font = ConstantString.editTitleFont
        recapBtn.titleLabel?.font = ConstantString.editTitleFont
        settingsBtn.titleLabel?.font = ConstantString.editTitleFont
        
            
        profileBtn.tag = ButtonTags.profile.rawValue
        myFamilyBtn.tag = ButtonTags.family.rawValue
        myInterestBtn.tag = ButtonTags.interest.rawValue
        myFriendsBtn.tag = ButtonTags.myFriends.rawValue
        recapBtn.tag = ButtonTags.recap.rawValue
        settingsBtn.tag = ButtonTags.settings.rawValue
        handleButtonsBackGroundColor()
        profileBtn .setTitleColor(#colorLiteral(red: 0.1176470588, green: 0.2470588235, blue: 0.4, alpha: 1), for: .normal)
        
        }
        func handleButtonsBackGroundColor() {
            profileBtn .setTitleColor(ConstantString.buttonSelectedBackgroundColor, for: .normal)
            if HarmonySingleton.shared.dashBoardData.data?.userType != .adult {
                
               // myFamilyBtn .setTitleColor(#colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1), for: .normal)
            }
//            myInterestBtn .setTitleColor(#colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1), for: .normal)
//            myFriendsBtn .setTitleColor(#colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1), for: .normal)
//            recapBtn .setTitleColor(#colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1), for: .normal)
             settingsBtn .setTitleColor(#colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1), for: .normal)
        }
        func handleButtonClicks(sender:UIButton)  {
//            print("Cliclked on \(sender.titleLabel?.text ?? "") tag Num: \(sender.tag ?? 0)")
            pageViecontroller?.setViewControllertoPage(index: sender.tag - 104)
            handleButtonsBackGroundColor()
            changeBorderView(sender: sender)
            sender.setTitleColor(ConstantString.buttonBackgroundColor, for: .normal)
        }
        @IBAction func profileBtnClick(_ sender: Any) {
            handleButtonClicks(sender: sender as! UIButton)
        }
        
        @IBAction func familyBtnClick(_ sender: Any) {
            handleButtonClicks(sender: sender as! UIButton)
        }
        @IBAction func interestBtnClick(_ sender: Any) {
            handleButtonClicks(sender: sender as! UIButton)
        }
        

        @IBAction func myFriendsBtnClick(_ sender: Any) {
              handleButtonClicks(sender: sender as! UIButton)
        }
      
        @IBAction func recapBtnClick(_ sender: Any) {
              handleButtonClicks(sender: sender as! UIButton)
          }
    
        @IBAction func settingsBtnClick(_ sender: Any) {
            handleButtonClicks(sender: sender as! UIButton)
        }
        
          
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "profilePageSegue"{
               pageViecontroller = segue.destination as! ProfilePageController
           }
        }
           

    }

