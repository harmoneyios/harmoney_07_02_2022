//
//  ProfilePageController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ProfilePageController: UIPageViewController {

    var myProfileVC = GlobalStoryBoard().myProfileVC
    var familyVC = GlobalStoryBoard().familyVC
    var interestsVC = GlobalStoryBoard().interestsVC
    var profileSettingsVC = GlobalStoryBoard().profileSettingsVC
    var myFriends = GlobalStoryBoard().myFriendsVC
    var recapVC = GlobalStoryBoard().recapVC
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        // Do any additional setup after loading the view.
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                direction: .forward,
                animated: true,
                completion: nil)
        }
        
        for subview in self.view.subviews {
            if let scrollView = subview as? UIScrollView {
                scrollView.bounces = false
                break;
            }
        }

        
    }
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [myProfileVC,
                familyVC,
                interestsVC,
                myFriends,
                recapVC,
                profileSettingsVC]
    }()
    
    func setViewControllertoPage(index:Int) {
        
        setViewControllers([orderedViewControllers[index]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProfilePageController : UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
//        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
//            return nil
//        }
//
//        let previousIndex = viewControllerIndex - 1
//
//        guard previousIndex >= 0 else {
//            return nil
//        }
//
//        guard orderedViewControllers.count > previousIndex else {
//            return nil
//        }
//
//        return orderedViewControllers[previousIndex]
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
//        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
//                 return nil
//             }
//
//             let nextIndex = viewControllerIndex + 1
//             let orderedViewControllersCount = orderedViewControllers.count
//
//             guard orderedViewControllersCount != nextIndex else {
//                 return nil
//             }
//
//             guard orderedViewControllersCount > nextIndex else {
//                 return nil
//             }
//
//             return orderedViewControllers[nextIndex]
        
        return nil
    }
    
    
}

