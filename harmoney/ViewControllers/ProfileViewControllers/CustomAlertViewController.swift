//
//  CustomAlertViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/10/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol CustomAlertViewControllerDelegate: class {
    func didTapOkayButton(customAlertViewController: CustomAlertViewController)
}

class CustomAlertViewController: UIViewController {
    weak var delegate: CustomAlertViewControllerDelegate?
    var alertTitle: String? 
    var alertMessage: String?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

//MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = alertTitle
        subtitleLabel.text = alertMessage
    }
}

//MARK: Action methods
extension CustomAlertViewController {
    @IBAction func okayButtonTapped(_ sender: UIButton) {
        delegate?.didTapOkayButton(customAlertViewController: self)
    }
}
