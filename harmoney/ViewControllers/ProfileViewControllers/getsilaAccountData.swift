//
//  getsilaAccountData.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 13/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct getsilaAccountData: Mappable {
       
        var status : Int32?
        var data : [silaAccountObject]?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            data    <- map["data"]
        }

    }

struct silaAccountObject : Mappable {
    

    var account_number: String?
    var routing_number: String?
    var account_name : String?
    var account_type : String?
    var active : Bool?
    var account_status : String?
    var account_link_status : String?
    var match_score : String?
    var entity_name : String?
    var account_owner_name : String?
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        account_number          <- map["account_number"]
        routing_number           <- map["routing_number"]
        account_name            <- map["account_name"]
        account_type    <- map["account_type"]
        active         <- map["active"]
        account_status    <- map["account_status"]
        account_link_status    <- map["account_link_status"]
        match_score    <- map["match_score"]
        entity_name    <- map["entity_name"]
        account_owner_name    <- map["account_owner_name"]
    }
}


  
