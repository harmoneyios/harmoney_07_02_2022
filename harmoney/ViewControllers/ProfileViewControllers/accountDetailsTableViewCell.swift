//
//  accountDetailsTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 16/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class accountDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var customLbl: UILabel!
    @IBOutlet weak var customTextField: UITextField!
    @IBOutlet weak var bgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUI(){
        customLbl.font = ConstantString.labelTitileFont
        customTextField.font = ConstantString.editTextFont18

    }

}
