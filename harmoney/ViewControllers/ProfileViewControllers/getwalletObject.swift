//
//  getwalletObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 13/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct getwalletObject: Mappable {
       
        var status : Int32?
        var message : String?
        var data : getwalletObjectData?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
            data    <- map["data"]
        }

    }

struct getwalletObjectData : Mappable {
    

    var reference: String?
    var status: String?
    var is_whitelisted : Bool?
    var success : Bool?
    var sila_balance : Int?
    var wallet : walletData?
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        is_whitelisted          <- map["is_whitelisted"]
        reference           <- map["reference"]
        success            <- map["success"]
        status    <- map["status"]
        sila_balance         <- map["sila_balance"]
        wallet    <- map["wallet"]
    }
}

    struct walletData : Mappable {
        

        var nickname: String?
        var blockchain_address: String?
        var blockchain_network : String?
        var defaultvalue : Bool?
        
        init?(map: Map) {
            
        }
        mutating func mapping(map: Map) {
            nickname          <- map["nickname"]
            blockchain_address           <- map["blockchain_address"]
            blockchain_network            <- map["blockchain_network"]
            defaultvalue    <- map["default"]
            
        }
}
