//
//  guidForMobileNumberObject.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 17/08/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct guidForMobileNumberObject: Mappable {
       
        var status : Int32?
        var data : [guidForMobileNumberObjectdata]?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            data    <- map["data"]
        }

    }


struct guidForMobileNumberObjectdata : Mappable {
    

    var guid: String?
    var name: String?
   

    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        guid          <- map["guid"]
        name           <- map["name"]
    
      
    }
}
