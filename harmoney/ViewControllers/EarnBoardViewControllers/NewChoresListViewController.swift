//
//  NewChoresListViewController.swift
//  Harmoney
//
//  Created by Mac on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SVProgressHUD
import ObjectMapper
import IQKeyboardManagerSwift


class NewChoresListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

@IBOutlet weak var choresTableView: UITableView!
  var chorePendingAndApproval = false
    var choreSubCategoryModel : ChoreSubCategoryModel?

  var newChoreCellIdentifier = "newChoreCell"
  var choreListCellIdentifier = "choreListCell"
  var wellnessParentVC : WellnessViewController?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
       super.viewDidLoad()
        choresTableView.delegate = self
        choresTableView.dataSource = self
        getAllSubCategory()
        
    }
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (choreSubCategoryModel?.data?.chorselist?.count ?? 0) + 1

       }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
           return 45//Choose your custom row height
        }
            return 80
    }

   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
        if indexPath.row == 0 {
          let cell = tableView.dequeueReusableCell(withIdentifier: newChoreCellIdentifier, for: indexPath) as! newChoreCell
           cell.closeBtn.addTarget(self, action: #selector(closeTapped(_:)), for: .touchUpInside)
            return cell
        }
            
            let indexdata = choreSubCategoryModel?.data?.chorselist?[indexPath.row - 1]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: choreListCellIdentifier, for: indexPath) as! choreListCell

            cell.typeImageView.imageFromURL(urlString: indexdata!.choserImage!)
            
            
            cell.borderView.layer.borderColor = UIColor(red: 130/250, green: 134/250, blue: 143/250, alpha: 1.0).cgColor

//            if (indexPath.row == 1 || indexPath.row == 4)
//            {
//                cell.borderView.layer.borderColor = UIColor(red: 133/250, green: 191/250, blue: 142/250, alpha: 1.0).cgColor
//            }
//            else if (indexPath.row == 2 || indexPath.row == 5)
//            {
//                cell.borderView.layer.borderColor = UIColor(red: 56/250, green: 121/250, blue: 198/250, alpha: 1.0).cgColor
//            }
//            else if (indexPath.row == 3 || indexPath.row == 6)
//            {
//                cell.borderView.layer.borderColor = UIColor(red: 130/250, green: 134/250, blue: 143/250, alpha: 1.0).cgColor
//            }
            
            cell.borderView.layer.borderWidth = 0.5
            cell.borderView.layer.cornerRadius = 25.0
            cell.titleLabel.text =  indexdata!.chorserName!
            cell.descriptionLabel.text =  "Task: " + indexdata!.chorserSubTitle!
            return cell
}
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0{
        wellnessParentVC?.choreListContainerView.isHidden = true
        wellnessParentVC?.newChoreview.isHidden = true
        
        wellnessParentVC?.createChoreVC?.vmObj.defaultChoreObj = choreSubCategoryModel?.data?.chorselist![indexPath.row - 1]
        wellnessParentVC?.createChoreView.isHidden = false
        }
        
    }

    @objc func closeTapped(_ sender: Any){
        
        wellnessParentVC?.getAllSubCategory()
    }
    
    func getAllSubCategory()  {
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        let data = [String : Any]()

        var (url, method, param) = APIHandler().getSubCatagory(params: data)
            url = url + "/" + UserDefaultConstants().guid! + "/9ivx2a7afj"
        
   
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                               case .success(let value):
                                   if let value = value as? [String:Any] {
                                       let personalChallenge = Mapper<ChoreSubCategoryModel>().map(JSON: value)
                                    
                                    self.choreSubCategoryModel = personalChallenge
                                     
                                    self.choresTableView.reloadData()
                                                                        
                                   }

                                   break
                               case .failure(let error):

                                   break
                               }
            }
        }
    }
    
}


