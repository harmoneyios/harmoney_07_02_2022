//
//  DeleteChallangeViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/28/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol DeleteChallangeViewControllerDelegate: class {
    func didTapThumbsDownButton(deleteChallangeViewController: DeleteChallangeViewController)
    func didTapThumbsUpButton(deleteChallangeViewController: DeleteChallangeViewController,challenge:PersonalChallenge)
    func didTapThumbsUpButtonChallengeDeleteOnboarding(deleteChallangeViewController: DeleteChallangeViewController,onboarding:ChallengeData)
    func didTapThumbsUpButtonChallengeDeleteDaillyChalleng(deleteChallangeViewController: DeleteChallangeViewController,daillyChallenge:DailyChallenge?)
    func didTapThumbsUpButtonOnPersonalChallenge(deleteChallangeViewController: DeleteChallangeViewController,challenge:PersonalChallenge)
}

class DeleteChallangeViewController: UIViewController {
    @IBOutlet weak var challengeTypeImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var describtionLabel: UILabel!
    
    weak var delegate: DeleteChallangeViewControllerDelegate?
    var indexPath: IndexPath?
    var personalChallenge: PersonalChallenge?
    var onboardingChallenge: ChallengeData?
    var daillyChallenge : DailyChallenge?
    
    var personalChallengId:String?
    
//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
         if HarmonySingleton.shared.taskType == "buttonTap"{
            configureUI()
         }else{
            titleLabel.text = HarmonySingleton.shared.checkChallengeStatus?.taskTitle
            describtionLabel.text = HarmonySingleton.shared.checkChallengeStatus?.displayMessage
            
            if HarmonySingleton.shared.checkChallengeStatus?.taskTitle == "Walk"{
                challengeTypeImageView.image = UIImage(named: "walk_medium_icon")
            }else if HarmonySingleton.shared.checkChallengeStatus?.taskTitle == "Swim"{
                challengeTypeImageView.image = UIImage(named: "swim_icon")
            }else if HarmonySingleton.shared.checkChallengeStatus?.taskTitle == "Run"{
                challengeTypeImageView.image = UIImage(named: "run_icon")
            }else if HarmonySingleton.shared.checkChallengeStatus?.taskTitle == "Bike"{
                challengeTypeImageView.image = UIImage(named: "bike_icon")
            }
         }
        
    }
    
    //MARK: UI Methods
    func configureUI() {
        switch personalChallenge?.data.challengeType {
         case .walk:
             challengeTypeImageView.image = UIImage(named: "walk_medium_icon")
             break
         case .swim:
             challengeTypeImageView.image = UIImage(named: "swim_icon")
             break
         case .run:
             challengeTypeImageView.image = UIImage(named: "run_icon")
             break
         case .bike:
             challengeTypeImageView.image = UIImage(named: "bike_icon")
             break
         case .none:
             break
         }
     }
}

//MARK: Action Methods
extension DeleteChallangeViewController {
    @IBAction func thumbsUpIconTapped(_ sender: UIButton) {
        
        if HarmonySingleton.shared.taskType == "buttonTap"{
        delegate?.didTapThumbsUpButton(deleteChallangeViewController: self,challenge: personalChallenge!)
        return
        }else{
            if HarmonySingleton.shared.checkChallengeStatus?.taskType == 2
            {
             if HarmonySingleton.shared.taskTitle == "Onboarding"
             {
                 self.delegate?.didTapThumbsUpButtonChallengeDeleteOnboarding(deleteChallangeViewController: self,onboarding:onboardingChallenge!)
                
             }else  if HarmonySingleton.shared.taskTitle == "Daily"{
                
                 self.delegate?.didTapThumbsUpButtonChallengeDeleteDaillyChalleng(deleteChallangeViewController: self,daillyChallenge:daillyChallenge)
             }
             else{
                 delegate?.didTapThumbsUpButtonOnPersonalChallenge(deleteChallangeViewController: self,challenge:personalChallenge!)
               

             }
             return

            }

        }
        
        
    }
    
    @IBAction func thumbsDownIconTapped(_ sender: Any) {
        delegate?.didTapThumbsDownButton(deleteChallangeViewController: self)
    }
}
