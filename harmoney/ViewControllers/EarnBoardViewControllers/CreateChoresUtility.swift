//
//  ChoresUIUtility.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

enum ChoresTableCells: CaseIterable {
    case header
    case name
    case day
    case date
    case reward
    case bucks
    case invite
    case accept
    case assignTo
}

class ChoreDataCells  {
    
    static let shared = ChoreDataCells()
    let vcVm = CreateChoreVM.shared
    var parentRef : ChoreCreateVC?
    var rows : [ChoresTableCells] = HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child ? [.header,.name,.day,.assignTo,.reward,.bucks,.invite,.accept] : [.header,.name,.day,.assignTo,.reward,.bucks,.accept]
    func cellForRow(cellType:ChoresTableCells , table : UITableView, indexPath:IndexPath) -> UITableViewCell {
        var parentInvitationBool =  false
        let dat = HarmonySingleton.shared.dashBoardData.data
        let isParentInvited = dat?.invitedRequest?.count ?? 0 > 0 ? true : false
        if dat?.userType == .child && (!isParentInvited || isParentInvited) && dat?.parentApproval == ParentApproval.notApproved{
               parentInvitationBool = true
            
            
        }else if dat?.userType == .adult {
            parentInvitationBool = true
        }else{
            parentInvitationBool = false
        }
        switch cellType {
        case .header:
            let cell = table.dequeueReusableCell(withIdentifier: "ChoresHeaderCell", for: indexPath) as! ChoresHeaderCell
            cell.choreTitle.text = vcVm.defaultChoreObj?.chorserName ?? "Sample"
            cell.choreImg.imageFromURL(urlString: vcVm.defaultChoreObj?.choserImage ?? "")
            return cell
        case .name:
            let cell = table.dequeueReusableCell(withIdentifier: "ChoreNameCell", for: indexPath) as! ChoreNameCell
            if vcVm.defaultChoreObj?.chorserSubTitle == "Custom"
            {
                cell.choreNameTextField.text = vcVm.choreCustomText
                if vcVm.choreCustomText == "Custom" {
                    cell.choreNameTextField.text = ""
                    
                }
                cell.choreNameLabel.isHidden = false
                cell.choreNameTextField.isHidden = false
//                CreateChoreVM.shared.choreCustomText = cell.choreNameTextField.text ?? "Custom"
            }else{
                cell.choreNameLabel.text = vcVm.defaultChoreObj?.chorserSubTitle ?? "Sample"
                cell.choreNameTextField.isHidden = true
                CreateChoreVM.shared.choreCustomText = cell.choreNameLabel.text ?? "Sample"

            }
            return cell
        case .day:
            let cell = table.dequeueReusableCell(withIdentifier: "ChoreDayCell", for: indexPath) as! ChoreDayCell
            return cell
        case .date:
            let cell = table.dequeueReusableCell(withIdentifier: "ChoreDateCell", for: indexPath) as! ChoreDateCell
            return cell
        case.assignTo:
            let cell = table.dequeueReusableCell(withIdentifier: "AssignToTableViewCell", for: indexPath) as! AssignToTableViewCell
            var widthForCollectionView:CGFloat = 80
            
            if HarmonySingleton.shared.assigntomemberDataList?.count ?? 0 > 1{
                widthForCollectionView = CGFloat((HarmonySingleton.shared.assigntomemberDataList?.count ?? 0) * 80 + 20)
            }
            if widthForCollectionView > 400
            {
                cell.memberCollectionViewWidthConstrain.constant = 400
            }else{
                cell.memberCollectionViewWidthConstrain.constant = widthForCollectionView
            }
            cell.memberCollectionView.reloadData()
//            CreateChoreVM.shared.parentRef?.choreUITable.reloadData()
            return cell
        case .reward:
            let cell = table.dequeueReusableCell(withIdentifier: "ChoreRewardCell", for: indexPath) as! ChoreRewardCell
            cell.choreNameLabel.text = vcVm.defaultChoreObj?.chorserName ?? "Sample"
            cell.setXPandGemCount(xp: vcVm.choreXpvalue, gem: vcVm.choreGemValue)
            cell.rewardAttributeLabel.isHidden = true
            return cell
        case .bucks:
            let cell = table.dequeueReusableCell(withIdentifier: "ChoreBucksCell", for: indexPath) as! ChoreBucksCell
            cell.choreBucksTF.text = parentInvitationBool ? "" : vcVm.choreBucksvalue
            cell.choreRewardsTF.text = vcVm.choreRewardMsg
            if HarmonySingleton.shared.choreFor == "own"
            {
                cell.choreBucksTF.text = ""
                CreateChoreVM.shared.choreBucksvalue = ""
                cell.choreBucksTF.isUserInteractionEnabled = false
                cell.hbucksInfoLabel.isHidden = false
                if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
                    cell.hbucksInfoLabel.isHidden = true
                }else{
                    cell.hbucksInfoLabel.isHidden = false
                }
            }else{
                cell.choreBucksTF.isUserInteractionEnabled = true
                cell.hbucksInfoLabel.isHidden = true
               
            }
            return cell
        case .invite:
            let cell = table.dequeueReusableCell(withIdentifier: "ChoreInviteCell", for: indexPath) as! ChoreInviteCell
            return cell
        case .accept:
            let cell = table.dequeueReusableCell(withIdentifier: "ChoreAcceptCell", for: indexPath) as! ChoreAcceptCell
            return cell
        }
        
    }
    func defaultSections()  {
        
        let dat = HarmonySingleton.shared.dashBoardData.data
        let isParentInvited = dat?.invitedRequest?.count ?? 0 > 0 ? true : false
        if dat?.userType == RegistrationType.child {
            if isParentInvited && (dat?.parentApproval)! == ParentApproval.approved{
                self.rows = [.header,.name,.day,.reward,.bucks,.accept]
            }else{
                
                self.rows = [.header,.name,.day,.reward,.bucks,.invite,.accept]
                if let assigntomemberDataList = dat?.invitedRequest{
                    for objMember in assigntomemberDataList{
                        if objMember.memberAccept == 1
                        {
                            self.rows = [.header,.name,.day,.reward,.bucks,.accept]

                        }
                    }
                }
                
            }
        }else if dat?.userType == RegistrationType.adult {
            self.rows = [.header,.name,.day,.reward,.bucks,.accept]
        }else{
//            if !(dat?.bankAccount?.isEmpty ?? true){
              self.rows = [.header,.name,.day,.assignTo,.reward,.bucks,.accept]
//            }else{
//                self.rows = [.header,.name,.day,.reward,.bucks,.invite,.accept]
//            }
            
        }
        vcVm.parentRef?.calculateTableViewHeight()

    }
    func ifdaySelectedUI()  {
        
        
        let dat = HarmonySingleton.shared.dashBoardData.data
        let isParentInvited = dat?.invitedRequest?.count ?? 0 > 0 ? true : false
        if dat?.userType == RegistrationType.child {
            if isParentInvited && (dat?.parentApproval)! == ParentApproval.approved{
                self.rows = [.header,.name,.day,.date,.reward,.bucks,.accept]
            }else{
                self.rows = [.header,.name,.day,.date,.reward,.bucks,.invite,.accept]
            }
        }else if dat?.userType == RegistrationType.adult {
            self.rows = [.header,.name,.day,.date,.reward,.bucks,.accept]
        }else{
            
//            if !(dat?.bankAccount?.isEmpty ?? true){
                self.rows = [.header,.name,.day,.date,.assignTo,.reward,.bucks,.accept]
//            }else{
//                self.rows = [.header,.name,.day,.date,.reward,.bucks,.invite,.accept]
//            }
        }
        vcVm.parentRef?.calculateTableViewHeight()
    }
    func ifParentExistUI()  {
        
        self.rows = [.header,.name,.day,.date,.reward,.bucks,.accept]
        vcVm.parentRef?.calculateTableViewHeight()
    }
    
        
    
    func rowsHeight(cellType:ChoresTableCells) -> CGFloat {
        switch cellType {
//            [125,43,70,80,81,234,70,99]
        case .header:
            return 135
        case .name:
            return 43
        case .day:
            return 70
        case .date:
            return 80
        case .reward:
            return 0//81
        case .bucks:
            return 234
        case .invite:
            return 70
        case .accept:
            return 99
        case .assignTo:
        return 130
        }
        
    }
    
}

