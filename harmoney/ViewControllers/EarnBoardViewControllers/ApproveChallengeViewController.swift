//
//  ApproveChallengeViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/11/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import UICircularProgressRing
//import LinkKit

protocol ApproveChallengeViewControllerDelegate: class {
    func didTapAcceptChallengeButton(approveChallengeViewController: ApproveChallengeViewController)
    func didTapDeclineChallengeButton(approveChallengeViewController: ApproveChallengeViewController)
    func didTapCloseButton(approveChallengeViewController: ApproveChallengeViewController)
    func didTapAcceptLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController)
    func didTapDeclineLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController)
}

class ApproveChallengeViewController: UIViewController {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardViewPosY: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var challengeTypeLabel: UILabel!
    @IBOutlet weak var challengeTaskLabel: UILabel!
    @IBOutlet weak var challengeTypeImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var harmoneyBucksContainerView: UIView!
    @IBOutlet weak var customRewardStackView: UIStackView!
    @IBOutlet weak var addBankAccountView: UIView!
    @IBOutlet weak var harmoneyTextfield: UITextField!
    @IBOutlet weak var customRewardTextfield: UITextField!
    @IBOutlet weak var harmoneyBucksLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var levelCustomView: UIView!
    @IBOutlet weak var circularProgressView: UICircularProgressRing!
    @IBOutlet weak var customRewardTextViewConstraintTop: NSLayoutConstraint!
    @IBOutlet weak var buttonsCustomViewConstraintTop: NSLayoutConstraint!
    
    weak var delegate: ApproveChallengeViewControllerDelegate?
    var indexPath: IndexPath?
    var personalChallenge: PersonalChallenge?
    var invitedRequest: InvitedRequest?
    let animationTimeInterval: Double = 0.2
    var startPosition: CGFloat = 0
    var kidName: String?

//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        animateIn()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        cardView.roundCorners([.topLeft, .topRight], radius: 13)
        logoImageView.roundCorners([.topLeft, .bottomLeft], radius: 15)
    }
}

//MARK: Private Methods
/*
extension ApproveChallengeViewController {
    func presentPlaidLinkWithCustomConfiguration() {
        let linkConfiguration = PLKConfiguration(key: "adced9ed28e0da390c03521ee07faf", env: .sandbox, product: .auth)
        linkConfiguration.clientName = "Harmoney Inc"
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(configuration: linkConfiguration, delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet
        }
        present(linkViewController, animated: true)
    }
} */

//MARK: UI Methods
extension ApproveChallengeViewController {
    func configureUI() {
        if invitedRequest != nil {
            nameLabel.text = invitedRequest?.kidName ?? ""
            challengeTaskLabel.isHidden = true
            challengeTypeLabel.isHidden = true
            harmoneyBucksContainerView.isHidden = true
            customRewardStackView.isHidden = false
            harmoneyBucksLabel.isHidden = true
            levelCustomView.isHidden = false
            customRewardTextfield.text = invitedRequest?.rewardName ?? ""
            levelLabel.text = String(format: "%d", invitedRequest?.level ?? 0)
            circularProgressView.style = .ontop
//            customRewardTextViewConstraintTop.constant = -75
//            buttonsCustomViewConstraintTop.constant = 50
        } else {
            levelCustomView.isHidden = true
            harmoneyTextfield.isUserInteractionEnabled = true
//            customRewardTextViewConstraintTop.constant = -75
//            buttonsCustomViewConstraintTop.constant = 50
      
            //suba start
           /* if HarmonySingleton.shared.dashBoardData.data?.bankAccount == nil && personalChallenge?.data.harmoneyBucks ?? 0 > 0 {
                addBankAccountView.isHidden = false
                harmoneyTextfield.isUserInteractionEnabled = true
            }
            */
           // suba end
            startPosition = cardViewPosY.constant
            challengeTaskLabel.text = String(format: "%d", personalChallenge?.data.challengeTask ?? 0)
            harmoneyTextfield.text = String(format: "%d", personalChallenge?.data.harmoneyBucks ?? 0)
            nameLabel.text = kidName ?? ""
            
            if personalChallenge?.data.customReward?.count ?? 0 > 0 {
                customRewardStackView.isHidden = false
                customRewardTextfield.text = personalChallenge?.data.customReward
            } else {
                customRewardStackView.isHidden = true
            }
            
            configureUiBasedOnChallenge()
        }
    }
    
    func configureUiBasedOnChallenge() {
        switch personalChallenge?.data.challengeType {
        case .walk:
            challengeTypeLabel.text = "Walk"
            challengeTypeLabel.text = "No. of Steps"
            challengeTypeImageView.image = UIImage(named: "walk_medium_icon")
        case .swim:
            challengeTypeLabel.text = "Swim"
            challengeTypeLabel.text = "No. of Laps"
            challengeTypeImageView.image = UIImage(named: "swim_icon")
        case .run:
            challengeTypeLabel.text = "Run"
            challengeTypeLabel.text = "No. of Miles"
            challengeTypeImageView.image = UIImage(named: "run_icon")
        case .bike:
            challengeTypeLabel.text = "Bike"
            challengeTypeLabel.text = "No. of Miles"
            challengeTypeImageView.image = UIImage(named: "bike_icon")
        case .none: break
        }
    }
    
    func animateIn() {
        UIView.animate(withDuration: animationTimeInterval) {
            self.cardViewPosY.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func animateOut() {
        UIView.animate(withDuration: animationTimeInterval, animations: {
            self.cardViewPosY.constant = self.startPosition
            self.view.layoutIfNeeded()
        }, completion: { (finished) in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

//MARK: Action Methods
extension ApproveChallengeViewController {
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        delegate?.didTapCloseButton(approveChallengeViewController: self)
    }
    
    @IBAction func acceptChallengeButtonTapped(_ sender: UIButton) {
        if invitedRequest != nil {
            invitedRequest?.rewardName = customRewardTextfield.text
            
            delegate?.didTapAcceptLevelRewardButton(approveChallengeViewController: self)
        } else {
            let harmoneyBucks = Int(harmoneyTextfield.text ?? "")
            personalChallenge?.data.harmoneyBucks = harmoneyBucks
            
            if personalChallenge?.data.harmoneyBucks ?? 0 > 0 {
                //suba start
                
                delegate?.didTapAcceptChallengeButton(approveChallengeViewController: self)
                /*
                if HarmonySingleton.shared.dashBoardData.data?.bankAccount?.count ?? 0 > 0 {
                    delegate?.didTapAcceptChallengeButton(approveChallengeViewController: self)
                } else {
                   self.view.toast("Set up a bank account to approve Harmoney bucks for your kid’s challenge")
                } */
                //suba end
            } else {
                delegate?.didTapAcceptChallengeButton(approveChallengeViewController: self)
            }
        }
    }
    
    @IBAction func declineChallengeButtonTapped(_ sender: UIButton) {
        if invitedRequest != nil {
            invitedRequest?.rewardName = customRewardTextfield.text
            
            delegate?.didTapDeclineLevelRewardButton(approveChallengeViewController: self)
        } else {
            delegate?.didTapDeclineChallengeButton(approveChallengeViewController: self)
        }
    }
    
    @IBAction func addBankAccountButtonTapped(_ sender: UIButton) {
      //  presentPlaidLinkWithCustomConfiguration()
    }
}
/* depricated Framework tool
extension ApproveChallengeViewController: PLKPlaidLinkViewDelegate {
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didSucceedWithPublicToken publicToken: String, metadata: [String : Any]?) {
        dismiss(animated: true) {
                
        }
    }

    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didExitWithError error: Error?, metadata: [String : Any]?) {
        dismiss(animated: true) {
                
        }
    }
}
*/
