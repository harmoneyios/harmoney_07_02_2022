//
//  ChoreStatusVC.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 19/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
enum ProgessRows: String {
    case task = "Task"
    case when = "When"
    case until = "Until"
    case bucks = "Harmoney Bucks"
    case reward = "Reward"
}
class ChoreProgressVm {
    static let shared = ChoreProgressVm()
    var sectTwoTitles = ["Task","When","Until","Harmoney Bucks", "Reward"]
    var sectTwoDescriptions = ["Task","When","Until","Harmoney Bucks", "Reward"]
    var noRows : [ProgessRows] = [.task, .when, .until, .bucks, .reward]
    var dataObj : ChoresModel?{
        didSet{
            if dataObj?.task_when == "Just One Time"{
            self.sectTwoDescriptions = [(self.dataObj!.task_subName ?? ""),
                                        self.dataObj?.task_when ?? "",
                                        self.dataObj?.harmoney_bucks ?? "0",
                                        self.dataObj?.task_reward ?? ""]
                noRows = [.task, .when, .bucks, .reward]
            }else{
                self.sectTwoDescriptions = [(self.dataObj!.task_subName ?? ""),
                self.dataObj?.task_when ?? "",
                self.dataObj?.task_until ?? "",
                self.dataObj?.harmoney_bucks ?? "0",
                self.dataObj?.task_reward ?? ""]
                noRows = [.task, .when, .until, .bucks, .reward]
            }
        }
    }
}


class ChoreProgressVC: UIViewController {
    @IBOutlet weak var choreUITable: UITableView!
    var wellnessParentVC : WellnessViewController?
    var vmObj = ChoreProgressVm.shared
    @IBOutlet weak var shadowLayer: UIView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.shadowLayer.clipsToBounds = false;
        self.shadowLayer.layer.masksToBounds = false;
        shadowLayer.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.3659284607)
        shadowLayer.layer.shadowOffset = CGSize.init(width: 1, height: 1)
        shadowLayer.layer.shadowRadius = 5
        shadowLayer.layer.shadowOpacity = 1
        
        tableViewHeight.constant = 490
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func closeClick(_ sender: Any) {
        wellnessParentVC?.choreProgressContainerView.isHidden = true
    }
    
    func handleProrgessCellUI(indexPath:IndexPath,cell:ChoreProgressCell) {
        let totalCount = vmObj.dataObj?.taskList?.count ?? 0 + 2
        if indexPath.row == 0{
            cell.topLine.isHidden = true
        }else{
            cell.topLine.isHidden = false
        }
        
        if indexPath.row == totalCount + 1 {
            cell.bottomLine.isHidden = true
        }else{
            cell.bottomLine.isHidden = false
        }
        
        if indexPath.row == 0 || indexPath.row == totalCount + 1 {
            cell.desclbl.isHidden = true
            cell.imgView?.image = UIImage.init(named: "start")
            cell.titleLbl.text = indexPath.row == 0 ? "Start" : "Complete"
//            cell.bottomLine.backgroundColor = #colorLiteral(red: 0.4475242496, green: 0.7607882619, blue: 0.5380363464, alpha: 1)
        }else{
            let obj = vmObj.dataObj?.taskList![indexPath.row-1]
            cell.desclbl.isHidden = true
            cell.titleLbl.text =  "Day\(obj?.day ?? 0)- \(obj?.date ?? "")"
//            cell.desclbl.text = (obj?.status == 0) ? "Yet to Start" : "Completed"
//            cell.imgView?.image = (obj?.status == 0) ? UIImage.init(named: "yettostart") : UIImage.init(named: "tick")
            cell.imgView?.image = UIImage.init(named: "yettostart")
//            if obj?.status == 1{
//             cell.topLine.backgroundColor = #colorLiteral(red: 0.4475242496, green: 0.7607882619, blue: 0.5380363464, alpha: 1)
//             cell.bottomLine.backgroundColor = #colorLiteral(red: 0.4475242496, green: 0.7607882619, blue: 0.5380363464, alpha: 1)
//            }
        }
    }
}

extension ChoreProgressVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChoresHeaderCell", for: indexPath) as! ChoresHeaderCell            
            cell.choreTitle.text = vmObj.dataObj?.task_name ?? ""
            cell.choreImg.imageFromURL(urlString: vmObj.dataObj?.task_image_url ?? "")
            return cell
        }
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChoreNameCell", for: indexPath) as! ChoreNameCell
            cell.choreTskHeading.text = vmObj.noRows[indexPath.row].rawValue    
            cell.choreNameLabel.text = vmObj.sectTwoDescriptions[indexPath.row]
            cell.choreNameLabel.lineBreakMode = .byTruncatingTail
            cell.choreNameLabel.textColor = #colorLiteral(red: 0.9725490196, green: 0.6549019608, blue: 0.6196078431, alpha: 1)
            cell.choreTskHeading.adjustsFontSizeToFitWidth = true
            return cell
        }
        if indexPath.section == 2 {
           let cell = tableView.dequeueReusableCell(withIdentifier: "ChoreRewardCell", for: indexPath) as! ChoreRewardCell
            cell.choreNameLabel.text = vmObj.dataObj?.task_name ?? ""
            cell.setXPandGemCount(xp: vmObj.dataObj?.task_xp_value ?? "", gem: vmObj.dataObj?.task_gems_value ?? "")
            cell.rewardAttributeLabel.isHidden = true
           return cell
        }
        if indexPath.section == 3 {
           let cell = tableView.dequeueReusableCell(withIdentifier: "ChoreProgressCell", for: indexPath) as! ChoreProgressCell
            handleProrgessCellUI(indexPath: indexPath, cell: cell)
           return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {return 1}
        if section == 1 {return vmObj.noRows.count}
        if section == 2 {return 1}
//        if section == 3 {return (vmObj.dataObj?.taskList?.count ?? 0) + 2}
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {return 135}
        if indexPath.section == 1 {return 50}
        if indexPath.section == 2 {return 0/*80*/}
        if indexPath.section == 3 {return 70}
        return 0
    }
}


