//
//  DailyViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 15/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import Alamofire
import Toast_Swift
import Lottie
//import LinkKit
import SummerSlider
import RAMAnimatedTabBarController
import SVProgressHUD
import CoreMotion
import ObjectMapper
import ObjectiveC


var isSecondOnboardingChallange = false

class DailyViewController:  BaseViewController {
  
    var shareimage : UIImage?

    @IBOutlet weak var bottomShareVw: UIView!
    //MARK: - Constants
    
    let healthService = HealthService()

    let challengeCellReuseIdentifier = "challengeCell"
    let shareContentCellReuseIdentifier = "ShareContentCell"
    let dailyMychallengeTableviewCell = "dailyMychallengeTableviewCell"
    var  thumbsAlertViewController: ThumbsAlertViewController?
    var getInformationForPaymentObject : getInformationForPaymentObject?
    var timerSteps : Timer?
    var ch1Obj : ChallengeData?
    var isStarting: Bool = true
    //MARK: - IBOutlets
    var homeModel: HomeModel? {
        return HomeManager.sharedInstance.homeModel
    }
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var challengeTableView: UITableView!
    var deleteChallengeViewController: DeleteChallangeViewController?
    var pageViecontroller  : EarnPageViewController?
    var shoppedPageViewController : ShoppedPageViewController?
    var onboardingChallesCnameCheck = [ChallengeData]()
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    //MARK: - Properties
    
//    var onboardingChallenges: [ChallengeData] {
//        return DailyChallengeManager.sharedInstance.onboardingChallenges
//    }
    var onboardingChallenges: [ChallengeData] {
        return DailyChallengeManager.sharedInstance.onboardingChallenges.filter{ value in
            return value.cName != nil
        }
    }
    var dailyChallenges: [DailyChallenge] {
        return DailyChallengeManager.sharedInstance.dailyChallenges
    }
    
    var activeOnboardingWalkChallenge: ChallengeData? {
        return DailyChallengeManager.sharedInstance.getActiveOnboardingWalkChallenge()
    }
    
    var startedOnboardingWalkChallenge: ChallengeData? {
        DailyChallengeManager.sharedInstance.getStartedOnboardingWalkChallenge()
    }
    
    var activeDailyWalkChallenge: DailyChallenge? {
        return DailyChallengeManager.sharedInstance.getActiveDailyWalkChallenge()
    }
    
    var startedDailyWalkChallenge: DailyChallenge? {
        DailyChallengeManager.sharedInstance.getStartedDailyWalkChallenge()
    }
    
    var levelsModel: LevelsModel? {
          return LevelsManager.sharedInstance.levelsModel
      }
    
    var challengeAcceptViewController : ChallangeAcceptViewcontroller?
    var completedChallengeViewController: CompletedChallengeViewController?
    var debugView: PedometerDebugView?
    var getFootCountUpdates = OSDataPermissions()
    //MARK: - Lifecycle methods
    var sectionArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        bottomShareVw.layer.cornerRadius = 12
        bottomShareVw.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 0.5)
        bottomShareVw.layer.shadowColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 0.5)
        bottomShareVw.layer.shadowOpacity = 1
        bottomShareVw.layer.shadowOffset = .zero
        bottomShareVw.layer.shadowRadius = 2
        getLevels()
        getAllChallenges()
        NotificationCenter.default.addObserver(self, selector: #selector(onboardingOfferClaimed), name: Notification.Name("OnboardingOfferClaimed"), object: nil)
        getFootCountUpdates.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(onboardingCouponClaimed), name: Notification.Name("OnboardingCouponClaimed"), object: nil)

        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(foregroundCall), name: UIApplication.didBecomeActiveNotification, object: nil)

        if HarmonySingleton.shared.isHideForNewFlow == false{
            if HarmonySingleton.shared.dashBoardData.data?.userType != .child {
                if HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0 <= 10
                {
                    self.getInformation()
                }
            }
        }
        
        
    }
    
    
    
    private func startTimer(){
        stopTimer()
        //foregroundCall
        timerSteps = Timer.scheduledTimer(timeInterval: 20,
                                   target: self,
                                 selector: #selector(self.updateNewFuction),
                                 userInfo: nil,
                                  repeats: true)
      
    }
    
    @objc func updateNewFuction(){
        self.healthService.getTodaysStepsCollectionCumulitive { (stepCount, error) in
            DispatchQueue.main.async {
                let stringValue = String(format: "%.0f", stepCount ?? 0)
                if let intStepsCount = Int(stringValue){
                    UserDefaults.standard.setValue(intStepsCount, forKey: "InitalChallangeSteps")
                    self.footStepsReading(footSteps: intStepsCount)
                }
            }
    }
    }
    private func stopTimer(){
        if timerSteps != nil{
            timerSteps?.invalidate()
            timerSteps = nil
        }
    }
    func getInformation(){
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let nowString = formatter.string(from: now)

                if let lastTime = UserDefaults.standard.string(forKey: "savedDate"), lastTime == nowString {
                    // Already notified today, skip
                    print("same date - no action")
                    return
                }
                
                UserDefaults.standard.set(nowString, forKey: "savedDate")
            
            if Reachability.isConnectedToNetwork() {
               
                var data = [String : Any]()
                //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                var (url, method, param) = APIHandler().getInformationForPayment(params: data)
                print("get info\((url, method, param))")
                
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.getInformationForPaymentObject = Mapper<getInformationForPaymentObject>().map(JSON: value)
                         
                            if ((self.getInformationForPaymentObject?.data?.user == nil) && (HarmonySingleton.shared.dashBoardData.data?.userType != .child))
                            {
                                UserDefaults.standard.set("NoKey", forKey: "keyWallet")
                                print("No account")
                                self.presentThumbsAlertViewController(alertTitle: " No Wallet!!!\nCreate a wallet for you?",buttonTitle : "Later")
                            
                            }else{
                                //if self.getInformationForPaymentObject?.data?.user?.walletPrivateKey == nil{
                                if ((self.getInformationForPaymentObject?.data?.user?.walletPrivateKey == nil) && (HarmonySingleton.shared.dashBoardData.data?.userType != .child)){
                                    UserDefaults.standard.set("NoKey", forKey: "keyWallet")
                                    print("No account")
                                self.presentThumbsAlertViewController(alertTitle: " No Wallet!!!\nCreate a wallet for you?",buttonTitle : "Later")
                                    
                                    
                                }else{
                                    UserDefaults.standard.set(self.getInformationForPaymentObject?.data?.user?.walletPrivateKey, forKey: "keyWallet")
                                    print("low balance")
                                    self.showBalanceAlert()
                                    
                                }
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
    
    func showBalanceAlert() {
        
        presentThumbsAlertViewController(alertTitle: "Low Balance!!!\nTransfer funds to wallet?")
    }
    
    func presentThumbsAlertViewController(alertTitle: String, buttonTitle : String = "No") {
        thumbsAlertViewController = GlobalStoryBoard().thumbsAlertVC
        thumbsAlertViewController?.alertTitle = alertTitle
        thumbsAlertViewController?.buttonTitle = buttonTitle
        thumbsAlertViewController?.delegate = self
        self.present(thumbsAlertViewController!, animated: true, completion: nil)
    }
    
    @objc func onboardingOfferClaimed() {
       getAllChallenges()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "OnboardingOfferClaimed"), object: nil)
    }
    @objc func onboardingCouponClaimed() {
        if onboardingChallenges.count == 1
        {
            let onboardingChallenge = onboardingChallenges[0]
            
            let challengeDictionary = onboardingChallenge.toJSON()
            letsDoitFlowForOnboarding(onboardingChallange: onboardingChallenge, challenge: challengeDictionary)
            tabBarController?.selectedIndex = 2
    //        if let topController = UIApplication.topViewController() as? BoughtTabViewController {
    //
    //          topController.setupIndicatorView(buttonTag: 2)
    //          topController.shoppedPageViewController?.setViewControllerToPage(index: 2)
    //
    //        }
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "OnboardingCouponClaimed"), object: nil)
        }else{
            let onboardingChallenge = onboardingChallenges[1]
            
            let challengeDictionary = onboardingChallenge.toJSON()
            letsDoitFlowForOnboarding(onboardingChallange: onboardingChallenge, challenge: challengeDictionary)
            tabBarController?.selectedIndex = 2
    //        if let topController = UIApplication.topViewController() as? BoughtTabViewController {
    //
    //          topController.setupIndicatorView(buttonTag: 2)
    //          topController.shoppedPageViewController?.setViewControllerToPage(index: 2)
    //
    //        }
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "OnboardingCouponClaimed"), object: nil)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        DailyChallengeManager.sharedInstance.onboardingChallenges.removeAll()
//        DailyChallengeManager.sharedInstance.dailyChallenges.removeAll()
//        HarmonySingleton.shared.dashBoardData = nil
//        getAllChallenges()
        
        //Sabarish
//        if UserDefaults.standard.value(forKey: "CurrentDate") != nil && UserDefaults.standard.value(forKey: "ChallangeSteps") != nil{
//            getFootCountUpdates.updateFootCount(whenSteps: UserDefaults.standard.value(forKey: "ChallangeSteps") as! Int)
//        }
//        self.healthService.getTodaysStepsCollectionCumulitive { (stepCount, error) in
//            DispatchQueue.main.async {
//                let stringValue = String(format: "%.0f", stepCount ?? 0)
//                completedChallangeFootSteps = Int(stringValue) ?? 0
//            }
//    }
        self.getHomeModel()
        self.getDashBoardData()
        HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
        
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopTimer()
    }
    @objc func foregroundCall() {
        return
        if UserDefaults.standard.value(forKey: "CurrentDate") != nil && UserDefaults.standard.value(forKey: "ChallangeSteps") != nil{
            getFootCountUpdates.updateFootCount(whenSteps: UserDefaults.standard.value(forKey: "ChallangeSteps") as! Int)
//            self.healthService.getTodaysStepsCollectionCumulitive { (stepCount, error) in
//                DispatchQueue.main.async {
//                    let stringValue = String(format: "%.0f", stepCount ?? 0)
//                    completedChallangeFootSteps = Int(stringValue) ?? 0
//                }
//        }
        }
    }
    func getLevels() {
           LevelsManager.sharedInstance.getLevels { (result) in
           }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        restartAnimation()
        
    }

    @IBAction func shareClick(_ sender: UIButton) {
//        showSocialShare(sender)
       // socialMediaShareNewFunc()
        AlertManager.shared.shareScreenToSocialMedia(controller: self)
    }
    func getHomeModel() {
        HomeManager.sharedInstance.getHomeModel { (result) in
            switch result {
            case .success(_):
                self.configureUI()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    func configureUI()  {
        self.challengeTableView.reloadData()
    }
    func getDashBoardData() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")
        if Reachability.isConnectedToNetwork(){
            
            var (url, method, param) = APIHandler().dashboard(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: value)
                       
                        if HarmonySingleton.shared.dashBoardData.data?.onboardingChallengeCompletionStatus == true
                        {
                            if HarmonySingleton.shared.dashBoardData.data?.challenges?.count == 0{
                            
                                    self.getDailyChallenges()

                            }
                            
                        }
//                        self.getAllChallenges()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }else{
            self.view.toast("Internet Connection not Available!")
        }
        
    }
    func socialMediaShareNewFunc()  {
//        let vc = UIStoryboard.init(name: "General", bundle: nil).instantiateViewController(withIdentifier: "NewSocialShareVC") as! NewSocialShareVC
//        vc.delegate = self
//     if #available(iOS 13.0, *) {
//            vc.modalPresentationStyle = .automatic
//        } else {
//            vc.modalPresentationStyle = .overFullScreen
//        }
//        vc.shareimage = takeScreenshot(false)
//        self.present(vc, animated: true, completion: nil)
        
//        if !commentTextView.text.contains("Enter what's on your mind?") {
//            message = commentTextView.text
//        }
        if let img = takeScreenshot(false) {
            ShareInstagram(image: img)
        }
        
    }
    
    func ShareInstagram(image:UIImage) {
        let message = "Hi friends please use this application"
//        if !commentTextView.text.contains("Enter what's on your mind?") {
//            message = commentTextView.text
//        }
        InstagramManager.sharedManager.postImageToInstagramWithCaption(imageInstagram: image, instagramCaption: message, view: view)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "challengAlertSegue" {
            challengeAcceptViewController = (segue.destination as! ChallangeAcceptViewcontroller)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //addDebugView()
    }
    open func takeScreenshot(_ shouldSave: Bool = true) -> UIImage? {
           var screenshotImage :UIImage?
           let layer = UIApplication.shared.keyWindow!.layer
           let scale = UIScreen.main.scale
           UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
           guard let context = UIGraphicsGetCurrentContext() else {return nil}
           layer.render(in:context)
           screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
           if let image = screenshotImage, shouldSave {
               UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
           }
           return screenshotImage
       }
    
    //MARK: - UI methods
    
    func restartAnimation() {
        self.challengeTableView.reloadData()
    }
    
    func addDebugView() {
        guard debugView == nil else { return }
        
        let height: CGFloat = 200
        let width: CGFloat = 240
        let y = view.frame.maxY - height
        
        debugView = PedometerDebugView(frame: CGRect(x: 0, y: y, width: width, height: height))
        view.addSubview(debugView!)
    }
    
    func presentCompletedChallengeViewControllerForDailyChallenge(dailyChallenge: DailyChallenge, indexPath: IndexPath?) {
        completedChallengeViewController = GlobalStoryBoard().completedChallengeVC
        completedChallengeViewController?.dailyChallenge = dailyChallenge
        completedChallengeViewController?.indexPath = indexPath
        completedChallengeViewController?.delegate = self

        tabBarController?.present(completedChallengeViewController!, animated: true, completion: nil)
    }

    func presentCompletedChallengeViewControllerForOnboardingChallenge(onboardinChallenge: ChallengeData, indexPath: IndexPath?) {
        completedChallengeViewController = GlobalStoryBoard().completedChallengeVC
        completedChallengeViewController?.onboardingChallenge = onboardinChallenge
        completedChallengeViewController?.indexPath = indexPath
        completedChallengeViewController?.delegate = self

        tabBarController?.present(completedChallengeViewController!, animated: true, completion: nil)
    }
    
    func presentSuccesViewControllerForOnboardingChallenge(onboardingChallenge: ChallengeData) {
        let vc = GlobalStoryBoard().successVC
        vc.modalPresentationStyle = .fullScreen
        vc.parentVC = self
        vc.challengeObj = onboardingChallenge
        self.present(vc, animated: true, completion: nil)
    }
    
    func presentVideoPlayerControllerForDailyChallenge(dailyChallenge: DailyChallenge) {
        let videoController = GlobalStoryBoard().playerVC
        videoController.parentVC = self
        videoController.delegate = self
        videoController.dailyChallenge = dailyChallenge
        navigationController?.isNavigationBarHidden = false
        videoController.modalPresentationStyle = .fullScreen
        self.present(videoController, animated: true, completion: nil)
    }
    
    func presentVideoPlayerControllerForOnboardingChallenge(onboardingChallenge: ChallengeData) {
        let videoController = GlobalStoryBoard().playerVC
        videoController.parentVC = self
        videoController.delegate = self
        videoController.onboardingChallenge = onboardingChallenge
        navigationController?.isNavigationBarHidden = false
        videoController.modalPresentationStyle = .fullScreen
        self.present(videoController, animated: true, completion: nil)
    }
    
    func presentAcceptOnboardingChallengeScreen(onboardingChallenge: ChallengeData) {
        containerView.isHidden = false
        bottomShareVw.isHidden = true
        let challengeDictionary = onboardingChallenge.toJSON()
        challengeAcceptViewController?.onboardingChallenge = onboardingChallenge
        challengeAcceptViewController?.initialSetup(delegate: self, challenge: challengeDictionary)
    }
    
    @objc func showSocialShare(_ sender: UIButton) {
        let socialViewController = GlobalStoryBoard().socialVC
        socialViewController.parentView = self
        socialViewController.delegate = self
        if #available(iOS 13.0, *) {
            socialViewController.modalPresentationStyle = .automatic
        } else {
            socialViewController.modalPresentationStyle = .overFullScreen
        }
        self.present(socialViewController, animated: true, completion: nil)
    }
    
    //MARK: - Private methods

    private func initialSetUp()  {
        
        sectionArray = ["Daily Challenge"]

        challengeTableView.register(UINib.init(nibName: "ChallengeTableViewCell", bundle: nil), forCellReuseIdentifier: challengeCellReuseIdentifier)
        challengeTableView.register(UINib.init(nibName: "ShareContentCell", bundle: nil), forCellReuseIdentifier: shareContentCellReuseIdentifier)
        challengeTableView.register(UINib.init(nibName: "dailyMychallengeTableviewCell", bundle: nil), forCellReuseIdentifier: dailyMychallengeTableviewCell)

        containerView.isHidden = true
        bottomShareVw.isHidden = false
//        getAllChallenges()
    }
        
//    func checkActivityType() {
//        PedometerManager.sharedInstance.startMotionActivityUpdates { (activityType, changed) in
//
//            if changed {
//                self.setupPedometerUpdates()
//            }
//        }
//    }
    
    private func setupPedometerUpdates() {

        if (self.ch1Obj?.cStatus == 0 && self.ch1Obj?.cId == "1") || (self.activeOnboardingWalkChallenge != nil){
//            PedometerManager.sharedInstance.startUpdates(startDate: Date()) { (data, error) in
//
//                let progress = data?.numberOfSteps.intValue ?? 0
//
//                DailyChallengeManager.sharedInstance.updateActiveOnboardingChallengeProgress(progress: progress) { (result) in
//                    switch result {
//
//                    case .success(let newActiveOnboardingChallenge):
//                        self.debugView?.challengeSource = String(describing: type(of: newActiveOnboardingChallenge))
//                        self.debugView?.challengeID = newActiveOnboardingChallenge.cId
//                        self.debugView?.startDate = data?.startDate
//                        self.debugView?.endDate = data?.endDate
//                        self.debugView?.nrOfSteps = data?.numberOfSteps
//                        self.debugView?.distance = data?.distance
//                        self.debugView?.activityType = PedometerManager.sharedInstance.currentActivity
//                        self.debugView?.challengeType = .walk
//                        self.debugView?.progress = Float(PedometerManager.sharedInstance.completedSteps)
//
//                        let completedCount = PedometerManager.sharedInstance.completedSteps
//                        let taskCount = Int(newActiveOnboardingChallenge.taskCount ?? "0") ?? 0
//
//                        if completedCount >= taskCount {
//                            self.debugView?.challengeSource = nil
//                            self.debugView?.challengeID = nil
//                            self.debugView?.startDate = nil
//                            self.debugView?.endDate = nil
//                            self.debugView?.nrOfSteps = nil
//                            self.debugView?.distance = nil
//                            self.debugView?.activityType = nil
//                            self.debugView?.challengeType = nil
//                            self.debugView?.progress = nil
//                        }
//
//                    case .failure(let error):
//                        print(error)
//                        PedometerManager.sharedInstance.stopUpdates()
//                    }
//                }
//            }
        } else if self.activeDailyWalkChallenge != nil {
//
//            let serviceTye = HealthServiceType.stepCount
//
//            let date = Date.init(timeIntervalSinceNow: 86400)
//            let currentTime = NSDate()
//
//            let formatter = DateFormatter()
//            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
////            if let dateStr = self.activeChallenge?.data.updatedTime{
////                currentTime = formatter.date(from: (dateStr) )! as NSDate
////            }
//
//            healthService.sampleData(forServiceType: serviceTye,
//                                     sampleStartTime: currentTime as Date,
//                                     sampleEndTime: date as Date) { (value, error) in
//                guard error == nil else {
//                    return
//                }
////                print("ASDfasdfaMaryyyyyyyyyy \(value)")
//            } onUpdate: { (updateValue, updateError) in
//                guard updateError == nil else {
//                    return
//                }
//                DailyChallengeManager.sharedInstance.updateActiveDailyChallengeProgress(progress: Int(updateValue)) { (result) in
//                    switch result {
//                    case .success(let newActiveDailyChallenge):
////                        UserDefaultConstants().challangeStartDate  = Date() as NSDate
//                    case .failure(let error):
//                        print(error)
//                    }
//                }
//            }
        }
            
            
            
//            PedometerManager.sharedInstance.startUpdates(startDate: Date()) { (data, error) in
//
//                let progress = data?.numberOfSteps.intValue ?? 0
//
//                DailyChallengeManager.sharedInstance.updateActiveDailyChallengeProgress(progress: progress) { (result) in
//                    switch result {
//
//                    case .success(let newActiveDailyChallenge):
//                        self.debugView?.challengeSource = String(describing: type(of: newActiveDailyChallenge))
//                        self.debugView?.challengeID = newActiveDailyChallenge.challengeID
//                        self.debugView?.startDate = data?.startDate
//                        self.debugView?.endDate = data?.endDate
//                        self.debugView?.nrOfSteps = data?.numberOfSteps
//                        self.debugView?.distance = data?.distance
//                        self.debugView?.activityType = PedometerManager.sharedInstance.currentActivity
//                        self.debugView?.challengeType = .walk
//                        self.debugView?.progress = Float(newActiveDailyChallenge.taskCurrentValue ?? 0)
//
//                        if newActiveDailyChallenge.userCompletionStatus == .whatsNext {
//                            self.debugView?.challengeSource = nil
//                            self.debugView?.challengeID = nil
//                            self.debugView?.startDate = nil
//                            self.debugView?.endDate = nil
//                            self.debugView?.nrOfSteps = nil
//                            self.debugView?.distance = nil
//                            self.debugView?.activityType = nil
//                            self.debugView?.challengeType = nil
//                            self.debugView?.progress = nil
//                        }
//
//                        self.challengeTableView.reloadData()
//                    case .failure(let error):
//                        print(error)
//                        PedometerManager.sharedInstance.stopUpdates()
//                    }
//                }
//            }
//        } else {
////            PedometerManager.sharedInstance.stopUpdates()
//            self.debugView?.challengeSource = nil
//            self.debugView?.challengeID = nil
//            self.debugView?.startDate = nil
//            self.debugView?.endDate = nil
//            self.debugView?.nrOfSteps = nil
//            self.debugView?.distance = nil
////            self.debugView?.activityType = nil
//            self.debugView?.challengeType = nil
//            self.debugView?.progress = nil
//        }
    }
    
    
    private func setupPedometerUpdatesNew() {
        let progress = completedChallangeFootSteps
        if ((self.ch1Obj?.cStatus == 0 && self.ch1Obj?.cId == "1") || (self.activeOnboardingWalkChallenge != nil) && onboardingChallenges.count > 0){
            DailyChallengeManager.sharedInstance.updateActiveOnboardingChallengeProgress(progress: progress) { (result) in
                    switch result {
                    case .success(let _):
                        self.challengeTableView.reloadData()
                    case .failure(let error):
                        print(error)
                    }
            }
        }else if dailyChallenges.count > 0/*self.activeDailyWalkChallenge != nil*/ {
                DailyChallengeManager.sharedInstance.updateActiveDailyChallengeProgress(progress: progress) { (result) in
                    switch result {
                             
                    case .success(let newActiveDailyChallenge):
                        self.challengeTableView.reloadData()
                    case .failure(let error):
                        print(error)
                    }
            }
        }
    }
    
    
    private func getOnboardingChallenges() {
        DailyChallengeManager.sharedInstance.getOnboardingChallenges { (result) in
            switch result {
            
           

            case .success(_):
//                for onboardingChallengeObj in self.onboardingChallenges {
//                    if onboardingChallengeObj.cName != nil {
//
//                        self.onboardingChallesCnameCheck.append(onboardingChallengeObj)
//                    }
//
//                }
//                DailyChallengeManager.sharedInstance.onboardingChallenges.removeAll()
//                DailyChallengeManager.sharedInstance.onboardingChallenges = self.onboardingChallesCnameCheck
                self.challengeTableView.reloadData()
               self.getDailyChallenges()
            case .failure(let error):
                self.getDailyChallenges()
                //self.view.toast(error.localizedDescription)
            }
        }
    }
    
    private func getDailyChallenges() {
        guard DailyChallengeManager.sharedInstance.onboardingChallengesFinished else { return }
        if HarmonySingleton.shared.dashBoardData.data?.challenges?.count == 0
        {
            DailyChallengeManager.sharedInstance.getDailyChallenges { (result) in
                switch result {
                case .success(_):
                   // self.foregroundCall()
                    self.updateNewFuction()
                    self.challengeTableView.reloadData()
                    self.startTimer()
    //                self.setupPedometerUpdates()
                case .failure(let error):
                    //self.view.toast(error.localizedDescription)
                    break
                }
            }

        }
    }
    
    private func acceptOnboardingChallenge(onboardingChallenge: ChallengeData) {
        DailyChallengeManager.sharedInstance.acceptOnboardingChallenge(challenge: onboardingChallenge) { (result) in
             switch result {
                case .success(let acceptedOnboardingChallenge):
                    let chObj = acceptedOnboardingChallenge
                    
                    if chObj.cId == "1" && chObj.cStatus == 0{
                        self.ch1Obj = chObj
                    //    self.getFootCountUpdates.updateFootCount(whenSteps: Int((acceptedOnboardingChallenge.cInfo?.taskCount)!)!)
//                        self.checkActivityType()
                    }
                    if(chObj.cId == "3"){
                       HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
                        }
                    }
                    self.updateOnboardingChallenge(challenge: acceptedOnboardingChallenge, reloadAllChallenges: true) { (result) in
                    }
                case .failure(let error):
                    break
                    //self.view.toast(error.localizedDescription)
            }
        }
    }
    
    private func updateDailyChallenge(challenge: DailyChallenge, status: DailyChallengeStatus, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        SVProgressHUD.show()
        DailyChallengeManager.sharedInstance.updateDailyChallenge(dailyChallenge: challenge, status: status) { (result) in
            SVProgressHUD.dismiss()
            switch result {
            case .success(let dailyChallenge):
                self.challengeTableView.reloadData()
                handler(.success(dailyChallenge))
            case .failure(let error):
                self.challengeTableView.reloadData()
                 handler(.failure(error as NSError))
            }
        }
    }
    
    private func refreshHeaderViewAfterDailyChallengeCompleted(dailyChallenge: DailyChallenge) {
        let currentLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        let currentHarmoneyBucks = HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0
        let currentGem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
        let currentXP = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
        
        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: currentLevel)
        let finalXP = currentXP + dailyChallenge.xp
        let levelUp = finalXP >= totalXp
        
        if levelUp {
            HarmonySingleton.shared.dashBoardData.data?.points?.level = currentLevel + 1
        }
        
        HarmonySingleton.shared.dashBoardData.data?.points?.hbucks = currentHarmoneyBucks + dailyChallenge.hBucks
      
        HarmonySingleton.shared.dashBoardData.data?.points?.jem = currentGem + (Int(dailyChallenge.gem ?? "0") ?? 0)
        
       
        HarmonySingleton.shared.dashBoardData.data?.points?.xp = currentXP + dailyChallenge.xp
        
        HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
    }
    
    private func refreshHeaderViewAfterOnboardingChallengeCompleted(onboardingChallenge: ChallengeData) {
        let currentLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        let currentHarmoneyBucks = HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0
        let currentGem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
        let currentXP = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
        
        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: currentLevel)
        let finalXP = currentXP + (onboardingChallenge.points?.xp ?? 0)
        let levelUp = finalXP >= totalXp
        
        if levelUp {
            HarmonySingleton.shared.dashBoardData.data?.points?.level = currentLevel + 1
        }
        
        HarmonySingleton.shared.dashBoardData.data?.points?.hbucks = currentHarmoneyBucks + (onboardingChallenge.points?.hBucks ?? 0)
        HarmonySingleton.shared.dashBoardData.data?.points?.jem = currentGem + (onboardingChallenge.points?.jem ?? 0)
        HarmonySingleton.shared.dashBoardData.data?.points?.xp = currentXP + (onboardingChallenge.points?.xp ?? 0)
        
        HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
    }
    
    //MARK: - Public methods
    
    func getAllChallenges() {
        //Get Started Onboarding Challenges
        DailyChallengeManager.sharedInstance.getDashboardData { (result) in
            switch result {
             //Get not started onboarding challenges
            case .success(_):
                self.challengeTableView.reloadData()
//                self.setupPedometerUpdates()
                if DailyChallengeManager.sharedInstance.showMisteryBoxScreen {
                    self.showOnboardCompleteVC()
                }
                self.getOnboardingChallenges()
            case .failure(let error):
                 self.getOnboardingChallenges()
                //self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func updateOnboardingChallenge(challenge: ChallengeData, reloadAllChallenges:Bool = false,  handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        DailyChallengeManager.sharedInstance.updateOnboardingChallenge(challenge: challenge) { (result) in
            switch result {
                   
            case .success(let finalOnboardingChallenge):
                if reloadAllChallenges {
                    DailyChallengeManager.sharedInstance.onboardingChallenges.removeAll()
                    DailyChallengeManager.sharedInstance.dailyChallenges.removeAll()
                    self.getAllChallenges()
                } else {
                    self.challengeTableView.reloadData()
                }
                 handler(.success(finalOnboardingChallenge))
            case .failure(let error):
                handler(.failure(error as NSError))
                //self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func showSuccessScreenAfterSharing()  {
        let dat = ["cId":"4",
                   "points":["xp":50]] as [String : Any]
        let challengeObj = Mapper<ChallengeData>().map(JSON: dat)
        let vc = GlobalStoryBoard().successVC
        vc.modalPresentationStyle = .fullScreen
        vc.challengeObj = challengeObj
        vc.parentVC = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func showSuccessForCollectingRewards() {
        let dat = ["cId":"5",
                   "points":["xp":50]] as [String : Any]
        let challengeObj = Mapper<ChallengeData>().map(JSON: dat)
        let vc = GlobalStoryBoard().successVC
        vc.modalPresentationStyle = .fullScreen
        vc.challengeObj = challengeObj
        vc.parentVC = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func presentDeleteChallengeViewController(personalChallenge: ChallengeData) {
        deleteChallengeViewController = GlobalStoryBoard().deleteChallengeVC
//        deleteChallengeViewController?.indexPath = indexPath
//        deleteChallengeViewController?.personalChallenge = personalChallenge
        deleteChallengeViewController?.onboardingChallenge = personalChallenge
        deleteChallengeViewController?.delegate = self
        tabBarController?.present(deleteChallengeViewController!, animated: true, completion: nil)
    }
    
    func presentDeleteChallengeViewControllerDailly(personalChallenge: DailyChallenge) {
        deleteChallengeViewController = GlobalStoryBoard().deleteChallengeVC
//        deleteChallengeViewController?.indexPath = indexPath
//        deleteChallengeViewController?.personalChallenge = personalChallenge
        deleteChallengeViewController?.daillyChallenge = personalChallenge
        deleteChallengeViewController?.delegate = self
        tabBarController?.present(deleteChallengeViewController!, animated: true, completion: nil)
    }
    
    func showOnboardCompleteVC()  {
        let vc = GlobalStoryBoard().onBoardCompleteVC
        vc.modalPresentationStyle = .overFullScreen
        vc.parentVC = self
        self.present(vc, animated: true, completion: nil)
    }
}

//MARK: - UITableView methods

extension DailyViewController: UITableViewDataSource, UITableViewDelegate {
     func numberOfSections(in tableView: UITableView) -> Int {
        if  onboardingChallenges.count > 0 {
            return 1
        } else if dailyChallenges.count > 0{
            return sectionArray.count
        } else {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = UIColor.harmoneySectionHeaderColor
        let lbl = UILabel.init(frame: CGRect.init(x: 20, y: 5, width: 200, height: 30))
        if DailyChallengeManager.sharedInstance.onboardingChallengesFinished {
           
//            if  onboardingChallenges.count > 0 {
//                lbl.text = "Onboarding Challenge"
//            } else if (dailyChallenges.count > 0){
//                lbl.text = sectionArray[section]
//            }
           
            lbl.text = "Daily Challenge"
            
        }else{
            if  onboardingChallenges.count > 0 {
                lbl.text = "Onboarding Challenge"
            } else if (dailyChallenges.count > 0){
                lbl.text = sectionArray[section]
            }
        }
      
        lbl.font = UIFont.futuraPTMediumFont(size: 18)
        lbl.textColor = #colorLiteral(red: 0.1176470588, green: 0.2470588235, blue: 0.4, alpha: 1)
        vw.addSubview(lbl)
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if DailyChallengeManager.sharedInstance.onboardingChallengesFinished {
//           return onboardingChallenges.count//dailyChallenges.count
//        }
//
//        return DailyChallengeManager.sharedInstance.showSocialMediaShare ? 4 : onboardingChallenges.count

        if section == 0 {
            return onboardingChallenges.count > 0 ? onboardingChallenges.count  : dailyChallenges.count > 0 ? dailyChallenges.count  : 1

        } else if section == 1{
            return 1
        } else {
            return 0
        }
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

//        if indexPath.section ==  0{

            let userDefaultsCompledailytChallengeComplete = UserDefaults.standard
            userDefaultsCompledailytChallengeComplete.set("No", forKey: "setBackGroundDaily")

            
            if onboardingChallenges.count > 0 || dailyChallenges.count > 0 {
                
                            let cell = tableView.dequeueReusableCell(withIdentifier: challengeCellReuseIdentifier, for: indexPath) as! ChallengeTableViewCell
                        cell.delegate = self
                cell.isFromController = "dailytableview"
                        if DailyChallengeManager.sharedInstance.onboardingChallengesFinished {
                            if HarmonySingleton.shared.dashBoardData.data?.challenges?.count == 0{
                              
                                if dailyChallenges.count > 0{
                                cell.dailyChallenge = dailyChallenges[indexPath.row]
    //
                                        cell.challengeCanStart = dailyChallenges[indexPath.row].userCompletionStatus == .notStarted
                                    if dailyChallenges[indexPath.row].challegeTaskType == 1{
                                        if dailyChallenges[indexPath.row].userCompletionStatus == .notStarted{
                                            cell.delegate?.didTapChallengeTableViewCellStartButton(challengeTableViewCell: cell, fromController: "dailytableview")
    //                                        cell.whatsNextButton.isHidden = true
                                        }
                                    }
                                  

                                }else{
                                    
                                    if onboardingChallenges.count > indexPath.row{
                                        cell.onboardingChallenge = onboardingChallenges[indexPath.row]
                                        cell.challengeCanStart = onboardingChallenges[indexPath.row].cStatus == nil
                                    }
                                }

                            }else{
                                if onboardingChallenges.count > indexPath.row{
                                    cell.onboardingChallenge = onboardingChallenges[indexPath.row]
                                    cell.challengeCanStart = onboardingChallenges[indexPath.row].cStatus == nil
                                }
                               
                            }
                      
                        } else {
                        if onboardingChallenges.count > 0{
                            cell.onboardingChallenge = onboardingChallenges[indexPath.row]
                            cell.challengeCanStart = onboardingChallenges[indexPath.row].cStatus == nil
                        } else if dailyChallenges.count > 0{
                            cell.dailyChallenge = dailyChallenges[indexPath.row]

                                cell.challengeCanStart = dailyChallenges[indexPath.row].userCompletionStatus == .notStarted

                        }
                        }
                        cell.animateButton()
                        cell.refreshButton.isHidden = true
                        return cell
            //        }
            }
            
//            else {
//
//                let cell = tableView.dequeueReusableCell(withIdentifier: dailyMychallengeTableviewCell, for: indexPath) as! dailyMychallengeTableviewCell
//
//                cell.homeModel = homeModel
//                cell.myChallengeCollectionView.reloadData()
//                cell.delegate = self
//
//                return cell
//            }
            
//        }
//        else {
//
//            let cell = tableView.dequeueReusableCell(withIdentifier: dailyMychallengeTableviewCell, for: indexPath) as! dailyMychallengeTableviewCell
//
//            cell.homeModel = homeModel
//            cell.myChallengeCollectionView.reloadData()
//            cell.delegate = self
//
//            return cell
//
//        }
        
           return UITableViewCell()
    }
        
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.section == 0 {
//            let challengeTableViewCell = tableView.cellForRow(at: indexPath) as! ChallengeTableViewCell
//           if let onboardingChallenge = challengeTableViewCell.onboardingChallenge,onboardingChallenges.count > 0 {
//               presentAcceptOnboardingChallengeScreen(onboardingChallenge: onboardingChallenge)
//            }
//        } else {
//            
//            self.tabBarController?.selectedIndex = 1
//            isFromMyChallange = true
//
//        }
//      
//    }
}

//MARK: - ChallengeTableViewCellDelegate methods

extension DailyViewController: ChallengeTableViewCellDelegate {
    func didTapChallengeTableViewCellVideoButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        if let dailyChallenge = challengeTableViewCell.dailyChallenge {
            presentVideoPlayerControllerForDailyChallenge(dailyChallenge: dailyChallenge)
        } else if let onboardingChallenge = challengeTableViewCell.onboardingChallenge {
            presentVideoPlayerControllerForOnboardingChallenge(onboardingChallenge: onboardingChallenge)
        }
    }
    
    func didTapChallengeTableViewCellStartButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        //Show Accept Challenge Screen
//        print(onboardingChallenges.count,dailyChallenges.count)
        if let onboardingChallenge = challengeTableViewCell.onboardingChallenge,onboardingChallenges.count > 0 {
//            presentAcceptOnboardingChallengeScreen(onboardingChallenge: onboardingChallenge)
            let challengeDictionary = onboardingChallenge.toJSON()
            completedChallangeFootSteps = 0
            if challengeDictionary["cType"] as! String == "1"{
               letsDoitFlowForOnboarding(onboardingChallange: onboardingChallenge, challenge: challengeDictionary)
            }else if challengeDictionary["cType"] as! String == "2" {
                isSecondOnboardingChallange = true
                
                let gemsViewController = GlobalStoryBoard().CouponList
//                gemsViewController.delegate = self
                gemsViewController.isfromSecondChallange = true
                present(gemsViewController, animated: true, completion: nil)
            }
                        
        } else if var dailyChallenge = challengeTableViewCell.dailyChallenge {
            dailyChallenge.taskCurrentValue = 1
            if dailyChallenge.challengeType == 1 && dailyChallenge.challegeTaskType == 1 { //Walk Challange
                self.healthService.getTodaysStepsCollectionCumulitive { (stepCount, error) in
                    DispatchQueue.main.async {
                        let stringValue = String(format: "%.0f", stepCount ?? 0)
                        if let intStepsCount = Int(stringValue){
                            UserDefaults.standard.setValue(intStepsCount, forKey: "InitalChallangeSteps")
                            self.footStepsReading(footSteps: intStepsCount)
                        }
                        
                      //  self.getFootCountUpdates.updateFootCount(whenSteps: Int(stringValue) ?? 0)
                       // completedChallangeFootSteps = Int(stringValue) ?? 0
                    }
            }
               
//                getFootCountUpdates.updateFootCount(whenSteps: Int(dailyChallenge.taskValue)!)
                
            }
            updateDailyChallenge(challenge: dailyChallenge, status: .accepted) { (result) in
                self.challengeTableView.reloadData()
            }
        }
    }
    
    func didTapChallengeTableViewCellWhatsNextButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        
        //Show Complete Daily Challenge Screen
        if let dailyChallenge = challengeTableViewCell.dailyChallenge, let indexPath = challengeTableView.indexPath(for: challengeTableViewCell)  {
            
            
                let userDefaultsCompledailytChallengeComplete = UserDefaults.standard
                userDefaultsCompledailytChallengeComplete.set("dailyChallengCompleted", forKey: "completeDailyChallengeChatboat")
                
                    let userDefaultsCompledailytChallengeCompleteexperianceLabel = UserDefaults.standard
                    userDefaultsCompledailytChallengeCompleteexperianceLabel.set(dailyChallenge.xp , forKey: "completeDailyChallengeChatboatexperianceLabel")
                    let userDefaultsCompledailytChallengeCompleteharmoneyBucksLabel = UserDefaults.standard
                    userDefaultsCompledailytChallengeCompleteharmoneyBucksLabel.set(dailyChallenge.hBucks , forKey: "CompledailytChallengeChatboatharmoneyBucksLabel")
                    let userDefaultsCompledailytChallengeCompletediamondLabel = UserDefaults.standard
                    userDefaultsCompledailytChallengeCompletediamondLabel.set(dailyChallenge.gem , forKey: "completeDailyChallengeChatboatdiamondLabel")
                    let userDefaultsCompledailytChallengeCompletechallengeTitleLabel = UserDefaults.standard
                    userDefaultsCompledailytChallengeCompletechallengeTitleLabel.set(dailyChallenge.title , forKey: "completeDailyChallengeChatboatchallengeTitleLabel")
                    let userDefaultsCompledailytChallengeCompletetaskLabel = UserDefaults.standard
                    userDefaultsCompledailytChallengeCompletetaskLabel.set(dailyChallenge.subTitle , forKey: "completeDailyChallengeChatboattaskLabel")
                  
            presentCompletedChallengeViewControllerForDailyChallenge(dailyChallenge: dailyChallenge, indexPath: indexPath)
            refreshHeaderViewAfterDailyChallengeCompleted(dailyChallenge: dailyChallenge)
         //Show Complete Onboarding Challenge Screen
        } else if let onboardingChallenge = challengeTableViewCell.onboardingChallenge, let indexPath = challengeTableView.indexPath(for: challengeTableViewCell) {
            presentCompletedChallengeViewControllerForOnboardingChallenge(onboardinChallenge: onboardingChallenge, indexPath: indexPath)
            refreshHeaderViewAfterOnboardingChallengeCompleted(onboardingChallenge: onboardingChallenge)
        }
    }
    
    func didTapChallengeTableViewCellRemoveButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        
        if let onboardingChallenge = challengeTableViewCell.onboardingChallenge,onboardingChallenges.count > 0 {

            HarmonySingleton.shared.taskTitle = "Onboarding"
            self.presentDeleteChallengeViewController(personalChallenge: onboardingChallenge)
                        
        } else if var dailyChallenge = challengeTableViewCell.dailyChallenge {
            HarmonySingleton.shared.taskTitle = "Daily"
            self.presentDeleteChallengeViewControllerDailly(personalChallenge: dailyChallenge)
        }


    }
    
    func didTapChallengeTableViewCellCustomButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
      
    }
    
    func didTapChallengeTableViewCellNotAprovedButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {}
}

//MARK: - ChallangeAcceptViewDelegate methods

extension DailyViewController: ChallangeAcceptViewDelegate {
    func didAccept(_ challangeViewController: ChallangeAcceptViewcontroller, challenge: Challenge) {
        containerView.isHidden = true
        bottomShareVw.isHidden = false
        if let onboardingChallenge = challangeViewController.onboardingChallenge {
             acceptOnboardingChallenge(onboardingChallenge: onboardingChallenge)
        }
        if challenge["cType"] as! String == "2" {
            isSecondOnboardingChallange = true
            let gemsViewController = GlobalStoryBoard().CouponList
//            gemsViewController.delegate = self
            present(gemsViewController, animated: true, completion: nil)
        }
    }
    func letsDoitFlowForOnboarding(onboardingChallange:ChallengeData,challenge:Challenge)  {
//        containerView.isHidden = true
//        bottomShareVw.isHidden = false
//        if let onboardingChallenge = challangeViewController.onboardingChallenge {
             acceptOnboardingChallenge(onboardingChallenge: onboardingChallange)
//        }
//        if challenge["cType"] as! String == "2" {
//            isSecondOnboardingChallange = true
//            let gemsViewController = GlobalStoryBoard().gemsVC
//            gemsViewController.delegate = self
//            present(gemsViewController, animated: true, completion: nil)
//        }
    }
    
    func didClose(_ challangeViewController: ChallangeAcceptViewcontroller) {
        containerView.isHidden = true
        bottomShareVw.isHidden = false
    }
    
}

extension DailyViewController: DeleteChallangeViewControllerDelegate {
    func didTapThumbsUpButton(deleteChallangeViewController: DeleteChallangeViewController, challenge: PersonalChallenge) {
        
    }
    func didTapThumbsUpButtonOnPersonalChallenge(deleteChallangeViewController: DeleteChallangeViewController, challenge: PersonalChallenge) {
        
    }
    func didTapThumbsDownButton(deleteChallangeViewController: DeleteChallangeViewController) {
        deleteChallangeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapThumbsUpButtonChallengeDeleteDaillyChalleng(deleteChallangeViewController: DeleteChallangeViewController,daillyChallenge:DailyChallenge?) {
        SVProgressHUD.show()
            deleteChallangeViewController.dismiss(animated: true) {
                UIView.animate(withDuration: 0.5) {
                    

                    PersonalFitnessManager.sharedInstance.deletePersonalChallengeOnDaily(personalChallengeId:HarmonySingleton.shared.checkChallengeStatus?.taskId ?? "0") { (result) in
                            SVProgressHUD.dismiss()
                            switch result {
                            case .success(_):
                                if var dailyChallenge = daillyChallenge {
                                    dailyChallenge.taskCurrentValue = 1
                                    if dailyChallenge.challengeType == 1 && dailyChallenge.challegeTaskType == 1 { //Walk Challange
                                        completedChallangeFootSteps = 0
                                        self.getFootCountUpdates.updateFootCount(whenSteps: Int(dailyChallenge.taskValue)!)
                                    }
                                    self.updateDailyChallenge(challenge: dailyChallenge, status: .accepted) { (result) in
                                        self.challengeTableView.reloadData()
                                    }
                                }
                               
                            case .failure(let error):
//                                self.reloadPedometerUpdates = true
                                self.view.toast(error.localizedDescription)
                            }
                        }
//                    }
                }
            }
    }
    
    func didTapThumbsUpButtonChallengeDeleteOnboarding(deleteChallangeViewController: DeleteChallangeViewController,onboarding:ChallengeData) {
        SVProgressHUD.show()
            deleteChallangeViewController.dismiss(animated: true) {
                UIView.animate(withDuration: 0.5) {
                    

                    PersonalFitnessManager.sharedInstance.deletePersonalChallengeOnDaily(personalChallengeId:HarmonySingleton.shared.checkChallengeStatus?.taskId ?? "0") { (result) in
                            SVProgressHUD.dismiss()
                            switch result {
                            case .success(_):
                                let challengeDictionary = onboarding.toJSON()
                                
                                if challengeDictionary["cType"] as! String == "1"{
                                    self.acceptOnboardingChallenge(onboardingChallenge: onboarding)
                                }else if challengeDictionary["cType"] as! String == "2" {
                                    isSecondOnboardingChallange = true
                                    
                                    let gemsViewController = GlobalStoryBoard().CouponList
                    //                gemsViewController.delegate = self
                                    gemsViewController.isfromSecondChallange = true
                                    self.present(gemsViewController, animated: true, completion: nil)
                                }
                            
                            case .failure(let error):
//                                self.reloadPedometerUpdates = true
                                self.view.toast(error.localizedDescription)
                            }
                        }
//                    }
                }
            }
    }
}


extension DailyViewController : GemsViewDelegates{
    func gemsClickedFromOnboarding() {
        let onboardingChallenge = onboardingChallenges[1]
        let challengeDictionary = onboardingChallenge.toJSON()
        letsDoitFlowForOnboarding(onboardingChallange: onboardingChallenge, challenge: challengeDictionary)
        tabBarController?.selectedIndex = 3
    }
}
extension DailyViewController : CouponTemplateViewDelegates{
    func couponClaimedClickedFromOnboarding() {
        let onboardingChallenge = onboardingChallenges[1]
        let challengeDictionary = onboardingChallenge.toJSON()
        letsDoitFlowForOnboarding(onboardingChallange: onboardingChallenge, challenge: challengeDictionary)
        tabBarController?.selectedIndex = 3
    }
}

//MARK: - CompletedChallengeViewControllerDelegate methods

extension DailyViewController: CompletedChallengeViewControllerDelegate {
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, onboardingChallenge: ChallengeData) {
        var updatedOnboardingChallenge = onboardingChallenge
        updatedOnboardingChallenge.cStatus = 1
        print("Clicked on What's Next")
        self.updateOnboardingChallenge(challenge: updatedOnboardingChallenge, reloadAllChallenges: true) { (result) in
            switch result {
                
            case .success(let data):
                self.getDashBoardData()
                UserDefaults.standard.removeObject(forKey: "CurrentDate")
                UserDefaults.standard.removeObject(forKey: "ChallangeSteps")
                UserDefaults.standard.removeObject(forKey: "InitalChallangeSteps")
                PersonalFitnessManager.sharedInstance.checkActiveChallenge = true
                let chObj = data as! ChallengeData
                if(chObj.cId == "3" && chObj.cStatus == 1){
                    HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                       HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
                   }
                }else{
                    HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
                }
//                if (chObj.cId == "2" && chObj.cStatus == 1){
//                    let levelInfo = self.levelsModel?.data.levelInfo[1]
//                    self.presentClaimLevelRewardViewController(levelInfo: levelInfo!)
//                }
                
            case .failure(_):
                break
            }
        }
    }
    
    
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, dailyChallenge: DailyChallenge) {
      
        updateDailyChallenge(challenge: dailyChallenge, status: .rewards) { (result) in
            switch result {
            case .success(_):
                self.getDashBoardData()
                updateGoals(goalType: .dailyChallenge, success: nil)
                UserDefaults.standard.removeObject(forKey: "CurrentDate")
                UserDefaults.standard.removeObject(forKey: "ChallangeSteps")
                UserDefaults.standard.removeObject(forKey: "InitalChallangeSteps")
                PersonalFitnessManager.sharedInstance.checkActiveChallenge = true
                HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
               
            case .failure(_):
                break
            }
        }
    }
    
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, personalChallenge: PersonalChallenge) {}
   
    func didTapNextButtonLevelupPopup(completedChallengeViewController: CompletedChallengeViewController){
        let levelInfo = self.levelsModel?.data.levelInfo[1]
        if levelInfo != nil {
        self.presentClaimLevelRewardViewController(levelInfo: levelInfo!)
        }
    }
    func presentClaimLevelRewardViewController(levelInfo: LevelInfo) {
        self.getDashBoardData()
        let claimLevelRewardViewController = GlobalStoryBoard().claimLevelRewardVC
        claimLevelRewardViewController.levelInfo = levelInfo
        claimLevelRewardViewController.isFromOnboarding = true
        claimLevelRewardViewController.delegate = self
        present(claimLevelRewardViewController, animated: true, completion: nil)
    }
}

//MARK: - PlayerVideoControllerDelegate methods

extension DailyViewController: PlayerVideoControllerDelegate {
    func didTapPlayerVideoControllerDoneButton(playerVideoController: PlayerVideoController) {
        
        if let dailyChallenge = playerVideoController.dailyChallenge {
            presentCompletedChallengeViewControllerForDailyChallenge(dailyChallenge: dailyChallenge, indexPath: nil)
        } else if var onboardingChallenge = playerVideoController.onboardingChallenge {
            presentCompletedChallengeViewControllerForOnboardingChallenge(onboardinChallenge: onboardingChallenge, indexPath: nil)
        }
    }
}

//MARK: - SocialShareViewcontrollerDelegate methods

extension DailyViewController: SocialShareViewcontrollerDelegate {
    func socialShareViewcontrollerDelegateDidShare(socialShareViewcontroller: SocialShareViewcontroller) {
        challengeTableView.reloadData()
    }
}


extension DailyViewController : NewSocialMediaShareDelegate
{
    func socialMedaiShareSuccess() {
        socialMedaiPost()
    }
    func socialMedaiPost() {
        let ac = UIAlertController.init(title: "Yay! You have posted your winning moments on social media", message: "", preferredStyle: .alert)
        let font = UIFont(name: "FuturaPT-Medium", size: 20)
        let color = UIColor.black
        ac.setTitlet(font: font, color: color)
        let ok = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        ac.addAction(ok)
        present(ac, animated: true, completion: nil)
        
    }
}


extension UIAlertController {
    func setTitlet(font: UIFont?, color: UIColor?) {
        guard let title = self.title else { return }
        let attributeString = NSMutableAttributedString(string: title)//1
        if let titleFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : titleFont],//2
                                          range: NSMakeRange(0, title.utf8.count))
        }
        
        if let titleColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor],//3
                                          range: NSMakeRange(0, title.utf8.count))
        }
        self.setValue(attributeString, forKey: "attributedTitle")//4
    }
    
    //Set message font and message color
    func setMessage(font: UIFont?, color: UIColor?) {
        guard let message = self.message else { return }
        let attributeString = NSMutableAttributedString(string: message)
        if let messageFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : messageFont],
                                          range: NSMakeRange(0, message.utf8.count))
        }
        
        if let messageColorColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : messageColorColor],
                                          range: NSMakeRange(0, message.utf8.count))
        }
        let paragraphStyle = NSMutableParagraphStyle()
            // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 4.0 // Whatever line spacing you want in points
        
            // *** Apply attribute to string ***
        attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributeString.length))
        self.setValue(attributeString, forKey: "attributedMessage")
    }
    
}


extension DailyViewController : CollectLevelRewardsDelegate{
    func leveleRegardsCollected(){
        let levelsViewController = GlobalStoryBoard().levelsVC
        levelsViewController.isFromOnboarding = true
        LevelsManager.sharedInstance.levelRewardClaimed = true
        present(levelsViewController, animated: true, completion: nil)
    }
}


extension DailyViewController : FootStepsDelegates{
    //This Method calls on Final Update
    func footStepsReading(footSteps: Int) {
        completedChallangeFootSteps = footSteps
        setupPedometerUpdatesNew()
        DispatchQueue.main.async {
            self.challengeTableView.reloadData()
        }
    }
    //This Method calls on every stepcount
    func footStepsContinuousUpdate(footSteps: Int) {
        if onboardingChallenges.count > 0 || dailyChallenges.count > 0
        {
        completedChallangeFootSteps = footSteps
        setupPedometerUpdatesNew()
            DispatchQueue.main.async {
                self.challengeTableView.reloadData()
            }
        }
    }
}
extension DailyViewController : myChallenegeDelegate {
    func didTapOnMychallenegeCell(challengeTableViewCell: dailyMychallengeTableviewCell, selectedIndex: Int) {
        if let topController = UIApplication.topViewController() as? EarnTabViewController {
            if isFromMyChallange {
                if myChallengeType == 2 {
                    topController.handleButtonClicks(sender: topController.wellnesBtn)
                } else {
                    topController.handleButtonClicks(sender: topController.personalBtn)
                }
                isFromMyChallange = false
            }
        }
    }
}

extension DailyViewController: ThumbsAlertViewControllerDelegate {
    func didTapThumbsUpButton(thumbsAlertViewController: ThumbsAlertViewController) {
        thumbsAlertViewController.dismiss(animated: true, completion: nil)
        let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "keyWallet")
        if walletPrivateKeyValue as! String == "NoKey"
       {        self.tabBarController?.selectedIndex = 3
        }else{
            if ((self.getInformationForPaymentObject?.data?.user?.processStage == 1)  ||   (self.getInformationForPaymentObject?.data?.user == nil) || (self.getInformationForPaymentObject?.data?.user?.walletPrivateKey == nil)) {
              
                    let couponDetails = GlobalStoryBoard().myFriendsVC
                    couponDetails.showView = "INFO"
                    self.navigationController?.pushViewController(couponDetails, animated: false)
                
            }else if (self.getInformationForPaymentObject?.data?.user?.processStage == 2 || self.getInformationForPaymentObject?.data?.user?.processStage == 3){
              
                    let couponDetails = GlobalStoryBoard().myFriendsVC
                    couponDetails.showView = "KYC"
                    self.navigationController?.pushViewController(couponDetails, animated: false)
               
            }else{

                let couponDetails = GlobalStoryBoard().myFriendsVC
                couponDetails.showView = "WALLET"
                self.navigationController?.pushViewController(couponDetails, animated: false)

            }
        }
    }
    
    func didTapThumbsDownButton(thumbsAlertViewController: ThumbsAlertViewController) {
        thumbsAlertViewController.dismiss(animated: true, completion: nil)
    }
        
}
