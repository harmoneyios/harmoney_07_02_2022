//
//  RemoveChoreViewController.swift
//  Harmoney
//
//  Created by Mac on 28/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol RemoveChoreViewControllerDelegate: class {
    func didTapThumbsDownButton(removeChoreViewController: RemoveChoreViewController)
    func didTapThumbsUpButton(removeChoreViewController: RemoveChoreViewController)
}

class RemoveChoreViewController: UIViewController {
    @IBOutlet weak var choreTypeImageView: UIImageView!
    
    weak var delegate: RemoveChoreViewControllerDelegate?
    var selectedTag: Int?
    var choresListModel: ChoresListModel?
    
//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let indexData = choresListModel?.data![selectedTag!]
        choreTypeImageView.imageFromURL(urlString: indexData?.task_image_url ?? "")
    }
}

//MARK: Action Methods
extension RemoveChoreViewController {
    @IBAction func thumbsUpIconTapped(_ sender: UIButton) {
        delegate?.didTapThumbsUpButton(removeChoreViewController: self)
    }
    
    @IBAction func thumbsDownIconTapped(_ sender: UIButton) {
        delegate?.didTapThumbsDownButton(removeChoreViewController: self)
    }
}
