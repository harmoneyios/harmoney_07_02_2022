//
//  plyerVideoController.swift
//  Harmoney
//
//  Created by Mac on 20/04/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Foundation
import AVKit

protocol PlayerVideoControllerDelegate: class {
    func didTapPlayerVideoControllerDoneButton(playerVideoController: PlayerVideoController)
}

class PlayerVideoController: UIViewController{

    @IBOutlet private weak var videoContentView: UIView!
    @IBOutlet private weak var muteButton: UIButton!
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var doneButton: UIButton!
    @IBOutlet private weak var parentButtonView: UIView!

    weak var delegate: PlayerVideoControllerDelegate?
    
    var parentVC : DailyViewController?
    var getChallangeData = [String:Any]()
    var dailyChallenge: DailyChallenge?
    var onboardingChallenge: ChallengeData?
    
    private var player: AVPlayer?
    private var isMuted: Bool = false
    private var playerLayer : AVPlayerLayer?
    override func viewDidLoad() {
        super.viewDidLoad()

        muteButton.layer.cornerRadius = 15

        var url = NSURL()
        if let dailyChallenge = self.dailyChallenge {
            url = NSURL(string: dailyChallenge.taskValue)!
        } else if let onboardingChallenge = self.onboardingChallenge {
             url = NSURL(string: onboardingChallenge.url ?? "")!
        } else {
            
            url = NSURL(string: UserDefaultConstants().videoUrl)!
        }
        
        player = AVPlayer(url: url as URL)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer!.frame = videoContentView.layer.bounds
        playerLayer!.name = "videoPlayer"

        NotificationCenter.default.addObserver(self,
                                           selector: #selector(playerItemDidReachEnd),
                                                     name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                     object: nil) // Add observer

        videoContentView.layer.addSublayer(playerLayer!)
        player?.play()
    }


    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let vidlayer = videoContentView.layer.sublayers?.first(where: { (layer) -> Bool in
            return layer.name == "videoPlayer"
        }) {
            vidlayer.frame = videoContentView.layer.bounds
        }
    
    }
    private func toggleMuteButton() {
        if isMuted {
            muteButton.setImage(UIImage(named: "mute"), for: .normal)
        } else {
            muteButton.setImage(UIImage(named: "unmute"), for: .normal)
        }
    }


    @IBAction func replayButtonPressed(_ sender: Any) {
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    @IBAction func doneButtonPressed(_ sender: Any) {
        //parentVC?.task2LocalStatus = true
        self.dismiss(animated: true, completion: nil)
        delegate?.didTapPlayerVideoControllerDoneButton(playerVideoController: self)
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        player?.pause()
        playerLayer?.removeFromSuperlayer()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func muteButtonPressed(_ sender: Any) {
        isMuted = !isMuted
        player?.isMuted = isMuted
        toggleMuteButton()
    }

    @objc func playerItemDidReachEnd(notification: NSNotification) {
        backButton.isHidden = true
        parentButtonView.isHidden = false
        
//        self.globalHomeViewcontroller.comfromPlayerViedeo = true
//        self.globalHomeViewcontroller.showSuccessStepTwo(challange: self.getChallangeData)
//        navigationController?.popViewController(animated: true)
    }
    
  //   Remove Observer
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

  

    
override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
}
        
}
