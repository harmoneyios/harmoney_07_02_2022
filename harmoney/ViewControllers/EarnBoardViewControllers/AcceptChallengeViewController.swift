//
//  AcceptChallengeViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/25/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol AcceptChallengeViewControllerDelegate: class {
    func gotItButtonTapped(acceptChallengeViewController: AcceptChallengeViewController)
}

class AcceptChallengeViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var gotItButton: UIButton!
    weak var delegate: AcceptChallengeViewControllerDelegate?
    var challengeType: ChallengeType?
    var alertTitle: String?
    
//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //configureUI()
    }

//MARK: UI Methods
   func configureUI() {
    titleLabel.text = alertTitle
    
    switch challengeType {
        case .walk:
            imageView.image = UIImage(named: "walk_medium_icon")
        case .swim:
            imageView.image = UIImage(named: "swim_icon")
        case .run:
            imageView.image = UIImage(named: "run_icon")
        case .bike:
            imageView.image = UIImage(named: "bike_icon")
        case .none:
            break
        }
    }
}

//MARK: Action Methods
extension AcceptChallengeViewController {
    @IBAction func gotItButtonTapped(_ sender: UIButton) {
        gotItButton.isUserInteractionEnabled = false
        delegate?.gotItButtonTapped(acceptChallengeViewController: self)
    }
}
