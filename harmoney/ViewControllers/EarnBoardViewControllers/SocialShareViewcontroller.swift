//
//  SocialShareViewcontroller.swift
//  Harmoney
//
//  Created by Mac on 22/04/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Foundation
import FBSDKShareKit
import Alamofire
import Toast_Swift
import StoreKit

protocol SocialShareViewcontrollerDelegate: class {
    func socialShareViewcontrollerDelegateDidShare(socialShareViewcontroller: SocialShareViewcontroller)
}

class SocialShareViewcontroller: UIViewController, SharingDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate{

    private struct ColorConst {
        static let borderGrey = UIColor.darkGray.withAlphaComponent(0.75)
    }

    var parentView = DailyViewController()

    weak var delegate: SocialShareViewcontrollerDelegate?
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var shareImageView: UIImageView!

    @IBOutlet weak var captureButton: UIButton!

    @IBOutlet weak var facebookShareButton: UIButton!
    @IBOutlet weak var igShareButton: UIButton!

    @IBOutlet weak var chooseImageView: UIView!
    @IBOutlet weak var chosenImageView: UIImageView!
    @IBOutlet weak var commentTextView: UITextView!

    var getChallangeData = [String:Any]()
    private var imagePickerController: UIImagePickerController?

    var selectedImage: UIImage? {
        didSet {
            chosenImageView.image = selectedImage
        }
    }
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        let fullString = NSMutableAttributedString(string: "Post it on Social Media and earn ")
        fullString.setAttributes([NSAttributedString.Key.font: UIFont(name: "FuturaPT-Bold", size: 15)!], range: NSRange(location: 0, length: fullString.length))
        var myMutableString = NSMutableAttributedString()
        let secondiconImage = UIImage(named: "h-black")!
        let secondicon = NSTextAttachment()
        secondicon.bounds = CGRect(x: 0, y: (descriptionLabel.font.capHeight - 12).rounded() / 2, width: 12, height: 12)
        secondicon.image = secondiconImage
        let hrString = NSAttributedString(attachment: secondicon)
        fullString.append(hrString)
        myMutableString = NSMutableAttributedString(string: "1\n")
        myMutableString.setAttributes([NSAttributedString.Key.font : UIFont(name: "FuturaPT-Bold", size: 15)!
            , NSAttributedString.Key.foregroundColor : UIColor(red: 22 / 255.0, green: 29 / 255.0, blue: 42 / 255.0, alpha: 1.0)], range: NSRange(location:0,length:1))
        fullString.append(myMutableString)

        myMutableString = NSMutableAttributedString(string: "Share your winning moments and earnings to the world!")

        myMutableString.setAttributes([NSAttributedString.Key.font : UIFont(name: "FuturaPT-Book", size: 14)!
            , NSAttributedString.Key.foregroundColor : UIColor(red: 143 / 255.0, green: 152 / 255.0, blue: 179 / 255.0, alpha: 1.0)], range: NSRange(location:0,length:53))

        fullString.append(myMutableString)
        descriptionLabel.attributedText = fullString
        descriptionLabel.superview?.layer.maskedCorners = CACornerMask(arrayLiteral: .layerMinXMinYCorner, .layerMaxXMinYCorner)
        descriptionLabel.superview?.layer.cornerRadius = 20


        shareImageView.superview?.layer.cornerRadius = 25.0
        shareImageView.superview?.layer.borderWidth = 1.0
        shareImageView.superview?.layer.borderColor = UIColor(red: 232.0/255.0, green: 233.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor

        commentTextView.layer.cornerRadius = 5
        commentTextView.layer.borderWidth = 1.0
        commentTextView.layer.borderColor = ColorConst.borderGrey.cgColor
        commentTextView.delegate = self
        setDefaultCommentView()

//        facebookShareButton.setImage(UIImage(named: "fb_disabled"), for: .disabled)
        facebookShareButton.setImage(UIImage(named: "facebook"), for: .normal)
//        igShareButton.setImage(UIImage(named: "if_disabled"), for: .disabled)
        igShareButton.setImage(UIImage(named: "Instagram"), for: .normal)

    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        checkForImageAndHandleUI()
    }

    @IBAction func selectImagePressed(_ sender: Any) {
        showImagePickerSelectionSheet()
    }

    @IBAction func shareInFacebookPressed(_ sender: Any) {
        ShareFacebook(image: selectedImage!)
    }

    @IBAction func shareInIGPressed(_ sender: Any) {
        ShareInstagram(image: selectedImage!)
    }


    private func setDefaultCommentView()  {
        commentTextView.text = "Enter what's on your mind?"
        commentTextView.font = UIFont(name: "FuturaPT-Book", size: 16)
        commentTextView.textColor = ColorConst.borderGrey
    }


    private func checkForImageAndHandleUI() {
        if selectedImage == nil {
            chosenImageView.isHidden = true
            chooseImageView.isHidden = false
            disableSocialMediaInteractions()
        } else {
            chosenImageView.isHidden = false
            chooseImageView.isHidden = true
            enableSocialMediaInteractions()
        }
    }

    private func disableSocialMediaInteractions() {
        facebookShareButton.isEnabled = false
        igShareButton.isEnabled = false
    }

    private func enableSocialMediaInteractions() {
        facebookShareButton.isEnabled = true
        igShareButton.isEnabled = true
    }


    @IBAction func closeBtnAction(_ sender: Any) {
//        UpdateShareDataAPI(sharedToFb: true)
        dismiss(animated: true, completion: nil)
    }

    private func showImagePickerSelectionSheet() {
        let alertController = UIAlertController(title: nil , message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Take Picture", style: .default) { [weak self](_) in
            self?.showImagePicker(withPickerType: .camera)
        }
        let galleryAction = UIAlertAction(title: "Choose from Photos", style: .default) { [weak self](_) in
            self?.showImagePicker(withPickerType: .photoLibrary)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cameraAction)
        alertController.addAction(galleryAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }


    func showImagePicker(withPickerType sourceType: UIImagePickerController.SourceType) {
        guard UIImagePickerController.isSourceTypeAvailable(sourceType) else {
            print("ERROR: Source type not available now...")
            return
        }

        imagePickerController = UIImagePickerController()
        imagePickerController?.delegate = self
        imagePickerController?.sourceType = sourceType
        if sourceType == .camera {
            imagePickerController?.cameraCaptureMode = .photo
            imagePickerController?.cameraDevice = .front
        }
        imagePickerController?.allowsEditing = true
        present(imagePickerController!, animated: true, completion: nil)
    }
    
    func ShareFacebook(image:UIImage) {
        
        let photo:SharePhoto = SharePhoto()
        photo.image = image
        photo.isUserGenerated = true

        
        let content:SharePhotoContent = SharePhotoContent()
        content.photos = [photo]

        if !commentTextView.text.contains("Enter what's on your mind?") {
            let hashtag = Hashtag(commentTextView.text)
            content.hashtag = hashtag

        }

        let sharedialog = ShareDialog(fromViewController: self, content: content, delegate: self)
        sharedialog.fromViewController = self
        sharedialog.mode = .automatic
        sharedialog.show()
    }
    
    func ShareInstagram(image:UIImage) {
        var message = "Hi friends please use this application"
        if !commentTextView.text.contains("Enter what's on your mind?") {
            message = commentTextView.text
        }
        InstagramManager.sharedManager.postImageToInstagramWithCaption(imageInstagram: image, instagramCaption: message, view: view)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//
//        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
//            return
//        }
//        selectedImage = image
//        checkForImageAndHandleUI()
//        picker.dismiss(animated: true, completion: nil)
//    }
//
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
        var image : UIImage!

        if let img = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as? UIImage
        {
            image = img

        }
        else if let img = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage
        {
            image = img
        }
        selectedImage = image
        checkForImageAndHandleUI()

        picker.dismiss(animated: true,completion: nil)
 }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        if sharer.shareContent.pageID != nil {
            print("Share: Success")
        }
        self.UpdateShareDataAPI(sharedToFb: true)
    }

    private func showInstallFb() {
        let alertController = UIAlertController(title: "Facebook Not Installed", message: "Please install the facebook application to use this feature.", preferredStyle: .alert)
        let installFb = UIAlertAction(title: "Install Facebook", style: .default) { (_) in
            let storeVC = SKStoreProductViewController()
            storeVC.loadProduct(withParameters: [SKStoreProductParameterITunesItemIdentifier: "284882215"]) { (success, error) in
                print("Success: \(success), error: \(error.debugDescription)")
            }
            self.present(storeVC, animated: true, completion: nil)
        }
        let dismiss = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(installFb)
        alertController.preferredAction = installFb
        alertController.addAction(dismiss)
        present(alertController, animated: true, completion: nil)
    }

    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        let nserror = error as NSError
        if nserror.domain == "com.facebook.sdk.share" && nserror.code == 2 {
            showInstallFb()
        } else {
            print("Share: Fail with reason \(error.localizedDescription)")
        }
    }

    func sharerDidCancel(_ sharer: Sharing) {
        print("Share: Cancel")
    }

    

    func UpdateShareDataAPI(sharedToFb: Bool) {
        guard Reachability.isConnectedToNetwork() else {
            return
        }

        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        data.updateValue(sharedToFb ? "1": "2", forKey: "shareTo")
        data.updateValue("1", forKey: "shareStatus")
        let (url, method, param) = APIHandler().challengeShare(params: data)
            
        AF.request(url, method: method, parameters: param, encoding: JSONEncoding.default,headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let _):
                self.dismiss(animated: true) {
                    HarmonySingleton.shared.dashBoardData.data?.challengeShareStatus = 1
                    HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
                    self.parentView.showSuccessScreenAfterSharing()
                    self.delegate?.socialShareViewcontrollerDelegateDidShare(socialShareViewcontroller: self)
                }

            case .failure(let error):
                print(error)
                }
//                self.globalHomeViewcontroller.getDashboardData()
                self.dismiss(animated: true, completion: nil)
        }
    }
}

extension SocialShareViewcontroller: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text.contains("Enter what's on your mind?") {
            textView.text = ""
        }
        return true
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        let text = textView.text.replacingOccurrences(of: " ", with: "")
        if text.isEmpty {
            setDefaultCommentView()
        }
    }
}
