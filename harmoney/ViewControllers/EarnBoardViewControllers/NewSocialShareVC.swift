//
//  NewSocialShareVC.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 26/10/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Foundation
import FBSDKShareKit
import Alamofire
import Toast_Swift
import StoreKit

protocol NewSocialMediaShareDelegate {
    func socialMedaiShareSuccess()
}

class NewSocialShareVC: UIViewController,SharingDelegate {
        
    var shareimage : UIImage?
    var delegate : NewSocialMediaShareDelegate?
    var isSocialMediaShare = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isSocialMediaShare {
            delegate?.socialMedaiShareSuccess()
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func fbClick(_ sender: Any) {
        if let img = shareimage {
            ShareFacebook(image: img)
        }
    }
    @IBAction func instaClick(_ sender: Any) {
        if let img = shareimage {
            ShareInstagram(image: img)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func showInstallFb() {
        let alertController = UIAlertController(title: "Facebook Not Installed", message: "Please install the facebook application to use this feature.", preferredStyle: .alert)
        let installFb = UIAlertAction(title: "Install Facebook", style: .default) { (_) in
            let storeVC = SKStoreProductViewController()
            storeVC.loadProduct(withParameters: [SKStoreProductParameterITunesItemIdentifier: "284882215"]) { (success, error) in
                print("Success: \(success), error: \(error.debugDescription)")
            }
            self.present(storeVC, animated: true, completion: nil)
        }
        let dismiss = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(installFb)
        alertController.preferredAction = installFb
        alertController.addAction(dismiss)
        present(alertController, animated: true, completion: nil)
    }

    
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
              if sharer.shareContent.pageID != nil {
                  print("Share: Success")
              }
//              self.UpdateShareDataAPI(sharedToFb: true)
            isSocialMediaShare = true
            delegate?.socialMedaiShareSuccess()
//            self.dismiss(animated: true, completion: nil)
          }

       
       func sharer(_ sharer: Sharing, didFailWithError error: Error) {
           let nserror = error as NSError
           if nserror.domain == "com.facebook.sdk.share" && nserror.code == 2 {
               showInstallFb()
           } else {
               print("Share: Fail with reason \(error.localizedDescription)")
           }
       }

       func sharerDidCancel(_ sharer: Sharing) {
           print("Share: Cancel")
       }
    
    func ShareFacebook(image:UIImage) {
        
        let photo:SharePhoto = SharePhoto()
        photo.image = image
        photo.isUserGenerated = true

        
        let content:SharePhotoContent = SharePhotoContent()
        content.photos = [photo]

//        if !commentTextView.text.contains("Enter what's on your mind?") {
//            let hashtag = Hashtag(commentTextView.text)
//            content.hashtag = hashtag
//
//        }

        let sharedialog = ShareDialog(fromViewController: self, content: content, delegate: self)
        sharedialog.fromViewController = self
        sharedialog.mode = .automatic
        sharedialog.show()
    }
    
    func ShareInstagram(image:UIImage) {
        var message = "Hi friends please use this application"
//        if !commentTextView.text.contains("Enter what's on your mind?") {
//            message = commentTextView.text
//        }
        InstagramManager.sharedManager.postImageToInstagramWithCaption(imageInstagram: image, instagramCaption: message, view: view)
        isSocialMediaShare = true       
    }


}
