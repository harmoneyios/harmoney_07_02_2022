//
//  AddChallengeViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/21/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol AddChallengeViewControllerDelegate: class {
    func didTapAddChallenge(addChallengeViewController: AddChallengeViewController, personalChallenge: PersonalChallenge)
    func didTapCloseAddChallenge(addChallengeViewController: AddChallengeViewController)
    func didTapInviteParentButton(addChallengeViewController: AddChallengeViewController)
}

class AddChallengeViewController: UIViewController {
    var acceptChallengeViewController: AcceptChallengeViewController?
    weak var delegate: AddChallengeViewControllerDelegate?
    var personalChallenge: PersonalChallenge? {
        didSet {
            addChallengeCustomView.isHidden = false
            addChallengeCustomView.personalChallenge = personalChallenge
        }
    }
    
    @IBOutlet weak var addChallengeCustomView: AddChallengeCustomView!

//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        addChallengeCustomView.delegate = self
        
        PersonalFitnessManager.sharedInstance.registerForNotifier(delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshAddPersonalChallnege()
    }
}

//MARK: UI Methods
extension AddChallengeViewController {
    func presentAcceptChallengeViewController(challengeType: ChallengeType,userType : RegistrationType = .parent) {
        acceptChallengeViewController = GlobalStoryBoard().acceptChallengeVC
        acceptChallengeViewController?.challengeType = challengeType
        acceptChallengeViewController?.delegate = self
        if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child {
            acceptChallengeViewController?.alertTitle = "Yay! You have accepted the challenge!"
            if  personalChallenge?.data.harmoneyBucks ?? 0 > 0 {
                acceptChallengeViewController?.alertTitle = "Yay! New challenge has been added!"
             }
        
        }else{
            if userType == .parent{
                acceptChallengeViewController?.alertTitle = "Yay! You have accepted the challenge!"
            }else{
                acceptChallengeViewController?.alertTitle = "Task assigned to kid successfully"
            }
           
        }
    
        tabBarController?.present(acceptChallengeViewController!, animated: true, completion: nil)
    }
    
    func refreshAddPersonalChallnege() {
        PersonalFitnessManager.sharedInstance.getDashboardData { (result) in
            switch result {
            case .success( _):
                self.getInvites()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    private func getInvites() {
        PersonalFitnessManager.sharedInstance.getInvites { (result) in
            switch result {
            case .success(_):
                self.addChallengeCustomView.personalChallenge = self.personalChallenge
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
}

//MARK: AddChallengeCustomViewDelegate Methods
extension AddChallengeViewController: AddChallengeCustomViewDelegate {
    func pullToRefresh(){
        refreshAddPersonalChallnege()
    }
    func didTapInviteParentButton(addChallengeCustomView: AddChallengeCustomView) {
        delegate?.didTapInviteParentButton(addChallengeViewController: self)
    }
    
    func didTapCloseButton(addChallengeCustomView: AddChallengeCustomView) {
        delegate?.didTapCloseAddChallenge(addChallengeViewController: self)
    }
    
    func didTapAddChallengeButton(addChallengeCustomView: AddChallengeCustomView, personalChallenge: PersonalChallenge) {
        print("didTapAddChallengeButton pressed" )
    
        PersonalFitnessManager.sharedInstance.addPersonalChallenge(personalChallenge: personalChallenge) { (result) in
            // distance cycling info update to healthkid
         /*   HealthService().writeDatatoHealthKitt(distance: 5.00) { (do, err) in
                
                    print(err)
            }//writeDatatoHealthKitt(distance: 5.00, sampleStartTime: personalChallenge.data.createdDate!, sampleEndTime: personalChallenge.data.createdDate) { (totalDistamce,err) in
            */
            switch result {
            case .success(let addedPersonalChallenge):
                self.personalChallenge = addedPersonalChallenge
                
                var memberId:[String] = []
                if let assigntomemberDataList = HarmonySingleton.shared.assigntomemberDataList{
                    for objMember in assigntomemberDataList{
                        if objMember.isSelectAvathar ?? false {
                            if let fromguid = objMember.fromGuid{
                                memberId.append(fromguid)
                            }
                        }
                    }
                }
                if HarmonySingleton.shared.assigntomemberDataList?.count == 1{
                    memberId.append(HarmonySingleton.shared.assigntomemberDataList?.first?.memberId ?? "")
                }
                
                var isType :RegistrationType = .parent
                if memberId.count > 1 {
                    isType = .child
                }else if !memberId.contains(addedPersonalChallenge.data.taskFrom?.guid ?? ""){
                    isType = .child
                }
                
                self.acceptChallengeViewController?.challengeType = addedPersonalChallenge.data.challengeType
                self.presentAcceptChallengeViewController(challengeType: addedPersonalChallenge.data.challengeType, userType: isType)
                
                
                break
            case .failure(let error):
                
                PersonalFitnessManager.sharedInstance.getPersonalChallenges { (result) in
                    switch result {
                    case .success(_):
               
                            if let startedChallenge = PersonalFitnessManager.sharedInstance.getCurrentChallenge() {
                                print(startedChallenge)
                                self.personalChallenge = startedChallenge
                                self.acceptChallengeViewController?.challengeType = startedChallenge.data.challengeType
                                self.presentAcceptChallengeViewController(challengeType: startedChallenge.data.challengeType)

                        }
                    case .failure(let error):
               
                        self.view.toast(error.localizedDescription)
                    }
                }
                
//                self.view.toast(error.localizedDescription)
                
                break
            }
        }
    }
}

//MARK: AcceptChallengeViewControllerDelegate Methods
extension AddChallengeViewController: AcceptChallengeViewControllerDelegate {
    func gotItButtonTapped(acceptChallengeViewController: AcceptChallengeViewController) {
        delegate?.didTapAddChallenge(addChallengeViewController: self, personalChallenge: self.personalChallenge!)
        
        acceptChallengeViewController.dismiss(animated: true) {
            self.addChallengeCustomView.scrollView.setContentOffset(.zero, animated: false)
        }
    }
}

//MARK: PersonalFitnessManagerNotifier Methods
extension AddChallengeViewController: PersonalFitnessManagerNotifier {
    func didInvite() {
        addChallengeCustomView.configureHarmoneyBucks()
    }
}
