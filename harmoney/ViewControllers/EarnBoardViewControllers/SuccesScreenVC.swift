//
//  SuccesScreenVC.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import SummerSlider
import Lottie
import Alamofire
import Toast_Swift
//import LinkKit
import RAMAnimatedTabBarController
import SVProgressHUD
import CoreMotion

class SuccesScreenVC: UIViewController {

    @IBOutlet weak var slider: SummerSlider!
    @IBOutlet weak var msgLblObe: UILabel!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var msgLblTwo: UILabel!
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var totalXPLabel: UILabel!
    @IBOutlet weak var xpLabel: UILabel!
    @IBOutlet weak var nextLevelLabel: UILabel!
    @IBOutlet weak var secondAnimationView: UIView!
    @IBOutlet weak var diamondImageView: UIImageView!
    @IBOutlet weak var diamondHarmoneyBLbl: UILabel!
    
    var animationViewLayer =  AnimationView()
    var collectionRewardAnimation =  AnimationView()
    var challengeObj : ChallengeData?
    var parentVC : DailyViewController?
    
    let totalChallenges = 3;
    var sliderCurrentPosition : Float = 0
    var sliderSlidePosition : Float = 0
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
               animationView.addSubview(animationViewLayer)
               animationViewLayer.translatesAutoresizingMaskIntoConstraints = false
               animationViewLayer.leadingAnchor.constraint(equalTo: animationView.leadingAnchor,constant: 0).isActive = true
               animationViewLayer.trailingAnchor.constraint(equalTo: animationView.trailingAnchor,constant: 0).isActive = true
               animationView.topAnchor.constraint(equalTo: animationViewLayer.topAnchor).isActive = true
        
        collectionRewardAnimation.frame = CGRect.init(x: 0, y: 0, width: 100, height: 120)
//        collectionRewardAnimation.center = secondAnimationView.center
        secondAnimationView.addSubview(collectionRewardAnimation)
//        collectionRewardAnimation.translatesAutoresizingMaskIntoConstraints = false
//        collectionRewardAnimation.leadingAnchor.constraint(equalTo: secondAnimationView.leadingAnchor,constant: 0).isActive = true
//        collectionRewardAnimation.trailingAnchor.constraint(equalTo: secondAnimationView.trailingAnchor,constant: 0).isActive = true
//        collectionRewardAnimation.topAnchor.constraint(equalTo: secondAnimationView.topAnchor).isActive = true
             
        
        slider.setThumbImage(UIImage(named: "xp"), for: .normal)
        slider.unselectedBarColor =  UIColor(red: 255 / 255.0, green: 255 / 255.0, blue: 255 / 255.0, alpha: 1.0)
        slider.selectedBarColor =  UIColor(red: 248 / 255.0, green: 167 / 255.0, blue: 158 / 255.0, alpha: 1.0)
        slider.minimumValue = 0
        slider.maximumValue = 1
        
        if((challengeObj) != nil){
            if (challengeObj?.cId == "5"){
                let path = Bundle.main.path(forResource: "collectionReward",
                                            ofType: "json") ?? ""
                collectionRewardAnimation.animation = Animation.filepath(path)
                collectionRewardAnimation.contentMode = .scaleAspectFill
                collectionRewardAnimation.loopMode = .repeatBackwards(4)
                collectionRewardAnimation.play()
                                
            }else{
                let path = Bundle.main.path(forResource: "successAnimationFour",
                                            ofType: "json") ?? ""
                animationViewLayer.animation = Animation.filepath(path)
                animationViewLayer.contentMode = .scaleAspectFill
                animationViewLayer.loopMode = .loop
                animationViewLayer.play()
            }
            updateDataOnScreen()
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        UIView.animate(withDuration: 1.4, animations: {
            self.slider.setValue(self.sliderSlidePosition, animated: true)
             self.slider.layoutIfNeeded()
        }, completion: {
        (value: Bool) in
        })
    }
    
    func updateDataOnScreen() {
        let level = Int(challengeObj?.cId ?? "0")
        var sliderCurrentPostion = Float(level!-1)/Float(totalChallenges)
        if level == 4 || level == 5{ sliderCurrentPostion = 1 }
        sliderSlidePosition = Float(level!)/Float(totalChallenges)
        
        self.slider.setValue(sliderCurrentPostion, animated: true)
        nextLevelLabel.text = "Next: Level 2"
        xpLabel.text = "+\(challengeObj?.points?.xp ?? 50) XP"
//        totalXPLabel.text = "\((level! >= 4 ? 3 : level! ) * 50)/150 XP"
          totalXPLabel.text = "200/200 XP"
        
    
        if challengeObj?.cId == "1"{
            msgLblObe.text = "You have successfully completed the first challenge"
            msgLblTwo.text = ""//"Next challenge for you is Dancercize"
        }
        else if challengeObj?.cId == "2"{
            msgLblObe.text = "You have successfully completed the second challenge."
            msgLblTwo.text = ""//"Next challenge for you is Donate to Charity"
        }else if challengeObj?.cId == "3"{
            msgLblObe.text =  "You have successfully completed the third challenge"
            msgLblTwo.text = ""//"hare your experience on Facebook & Instagram to earn h1"
        }else if challengeObj?.cId == "4"{
            diamondImageView.image = #imageLiteral(resourceName: "h-r")
            diamondHarmoneyBLbl.isHidden = false
            diamondImageView.isHidden = false
            msgLblObe.text =  "You have successfully completed the fourth challenge"
            msgLblTwo.text = ""
            nextLevelLabel.text = "Next: Level 2"
        }else if challengeObj?.cId == "5"{
            msgLblObe.text =  "You have received your first Harmoney Gem! And moved to Level 2"
            msgLblTwo.text = "Now all the Harmoney features are unlocked for you"
            nextLevelLabel.text = "Next: Level 2"
            diamondImageView.isHidden = false
            diamondHarmoneyBLbl.isHidden = false
            nextBtn.setTitle("Collect Rewards", for: .normal)
        }
    }
    
    @IBAction func nextClick(_ sender: Any) {
        if(challengeObj?.cId == "4"){
            //parentVC?.getDashBoardData()
         
            self.dismiss(animated: true, completion: nil)
            self.parentVC?.getAllChallenges()
        }else if(challengeObj?.cId == "5"){
            self.collectRewardsAction()
        }else{
            //let obj = challengeObj?.toJSON()
            //parentVC?.updateUserChallenge(status: "1", getChallangeData: obj!)
//            if var challenge = self.challengeObj {
//                challenge.cStatus = 1
//                parentVC?.updateOnboardingChallenge(challenge: challenge)
//            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func collectRewardsAction() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")

        if Reachability.isConnectedToNetwork(){
            var (url, method, param) = APIHandler().collectReward(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
                    self.parentVC?.getAllChallenges()
                    self.dismiss(animated: true, completion: nil)
                    if  let value = value as? [String:Any] {
                        if let data = value["data"] as? [String:Any] {
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }

        }else{
            self.view.toast("Internet Connection not Available!")
        }
    }
}
