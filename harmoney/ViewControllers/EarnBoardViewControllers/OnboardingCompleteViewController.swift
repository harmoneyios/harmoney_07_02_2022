//
//  OnboardingCompleteViewController.swift
//  Harmoney
//
//  Created by Hariharan G on 4/29/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class OnboardingCompleteViewController: UIViewController {

    @IBOutlet private weak var _containerView: UIView!
    @IBOutlet private weak var _openButton: UIButton!
    var parentVC : DailyViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        _containerView.layer.cornerRadius = 25
        _openButton.layer.cornerRadius = 15
        // Do any additional setup after loading the view.
    }
    

    @IBAction func openChestButtonPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            self.parentVC!.showSuccessForCollectingRewards()
        }
    }
}
