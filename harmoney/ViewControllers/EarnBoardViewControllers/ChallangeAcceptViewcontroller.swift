//
//  ChallangeAcceptViewcontroller.swift
//  Harmoney
//
//  Created by Mac on 19/04/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Foundation

typealias Challenge = [String: Any]

protocol ChallangeAcceptViewDelegate: class {
    
    func didAccept(_ challangeViewController: ChallangeAcceptViewcontroller, challenge: Challenge)
    func didClose(_ challangeViewController: ChallangeAcceptViewcontroller)
}

final class ChallangeAcceptViewcontroller: UIViewController {
    
    @IBOutlet weak var challangeAcceptView: ChallangeAcceptView!
    var onboardingChallenge: ChallengeData?
    weak var delegate: ChallangeAcceptViewDelegate?
    
    func initialSetup(delegate: ChallangeAcceptViewDelegate, challenge: Challenge) {
        self.delegate = delegate
        challangeAcceptView.ChallangeAcceptViewControl = self
        load(challenge)
    }
    
    private func load(_ challenge: Challenge) {
        self.challangeAcceptView.getChallangeData(challengesData: challenge)
    }
    
    func closeBtnAction() {
        delegate?.didClose(self)
    }
    
    func acceptChallenge(challenge: Challenge) {
        delegate?.didAccept(self, challenge: challenge)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
