//
//  WellnessViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 15/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import ObjectMapper
class WellnessViewController: BaseViewController {

    @IBOutlet weak var newChoreview: NewChoreView!
    @IBOutlet weak var choreListContainerView: UIView!
    @IBOutlet weak var chorePendingAndApprovalContainerView: UIView!
    @IBOutlet weak var choreProgressContainerView: UIView!
    @IBOutlet weak var createChoreView: UIView!
    
    var newChoresListVC : NewChoresListViewController?
    var chorePendingAndApprovalVC : ChorePendingAndApprovalViewController?
    var createChoreVC : ChoreCreateVC?
    var choreProgressvc : ChoreProgressVC?    
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
                
        newChoreview.newChoreButton.addTarget(self, action: #selector(didTapNewChoreButton), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    func handleViewControllersUI(nwCHoreVw:Bool, nwChoreVC:Bool, chorePendingVC:Bool, createChoreVC:Bool,choreProgressVC:Bool)  {
        newChoreview.isHidden = nwCHoreVw ? true : false
        choreListContainerView.isHidden = nwChoreVC ? true : false
        chorePendingAndApprovalContainerView.isHidden = chorePendingVC ? true : false
        createChoreView.isHidden = createChoreVC ? true : false
        choreProgressContainerView.isHidden = createChoreVC ? true : false
    }
    
//    func chorePendingAndApprovalShowAndHide(chorePendingAndApproval :Bool) {
//
//        if chorePendingAndApproval {
//            chorePendingAndApprovalVC?.choresTableView.reloadData()
//            chorePendingAndApprovalContainerView.isHidden = false
//        }
//        else {
//            newChoreview.isHidden = false
//        }
//
//        newChoresListVC?.chorePendingAndApproval = chorePendingAndApproval
//
//
//    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createChoreVC?.wellnessParentVC = self
        choreProgressvc?.wellnessParentVC = self
        newChoresListVC?.wellnessParentVC = self
        chorePendingAndApprovalVC?.wellnessParentVC = self
        getAllSubCategory()
        
    }
    func getAllSubCategory()  {
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
         var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")

        let (url, method, param) = APIHandler().getChorslist(params: data)
        self.handleViewControllersUI(nwCHoreVw: false, nwChoreVC: true, chorePendingVC: true, createChoreVC: true, choreProgressVC: true)
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                               case .success(let value):
                                
                                if let value = value as? [String:Any] {
                                    let personalChallenge = Mapper<ChoresListModel>().map(JSON: value)
                                    
                                    if personalChallenge?.data?.count ?? 0 > 0 {
                                        DispatchQueue.main.async {
                                        self.handleViewControllersUI(nwCHoreVw: true, nwChoreVC: true, chorePendingVC: false, createChoreVC: true, choreProgressVC: true)
                                        }
                                    }else{
                                        DispatchQueue.main.async {
                                        self.handleViewControllersUI(nwCHoreVw: false, nwChoreVC: true, chorePendingVC: true, createChoreVC: true, choreProgressVC: true)
                                        }
                                    }
                                    DispatchQueue.main.async {
                                        if isFromAddChallenge == true{
                                            isFromAddChallenge = false
                                            self.showAddChoreView()
                                        }
                                    }
                                }
                                   break
                               case .failure(let error):
                                   break
                               }
            }
        }
    }
    
    @objc func didTapNewChoreButton(_ sender: Any) {
//        editProfileVC?.profileUpdate = self.profileUpdate
        showAddChoreView()
    }

    func showAddChoreView(){
        newChoresListVC?.getAllSubCategory()
        handleViewControllersUI(nwCHoreVw: true, nwChoreVC: false, chorePendingVC: true, createChoreVC: true, choreProgressVC: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "NewChoresListViewController" {
            newChoresListVC = (segue.destination as! NewChoresListViewController)
        }
        if segue.identifier == "ChorePendingAndApprovalViewController" {
                chorePendingAndApprovalVC = (segue.destination as! ChorePendingAndApprovalViewController)
            }
        if segue.identifier == "ChoreCreateVC" {
            createChoreVC = (segue.destination as! ChoreCreateVC)
        }
        if segue.identifier == "ChoreProgressVC" {
            choreProgressvc = (segue.destination as! ChoreProgressVC)
        }
        
    }
}


