//
//  GetSensorData.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import CoreMotion

var completedChallangeFootSteps = 0

protocol FootStepsDelegates {
    func footStepsReading(footSteps:Int)
    func footStepsContinuousUpdate(footSteps:Int)
}

class OSDataPermissions: NSObject {
    var delegate : FootStepsDelegates?
//    private let pedometer = CMPedometer()
    private let healthService = HealthService()
    var globalSteps = 0

    func updateFootCount(whenSteps are:Int)  {
        var challengeDate : Date
        if UserDefaults.standard.value(forKey: "CurrentDate") != nil {
            challengeDate = UserDefaults.standard.value(forKey: "CurrentDate") as! Date
        }else{
            challengeDate = Date()
            UserDefaults.standard.setValue(challengeDate, forKey: "CurrentDate")
        }
        
        if UserDefaults.standard.value(forKey: "InitalChallangeSteps") != nil {
            if let initialSteps = UserDefaults.standard.value(forKey: "InitalChallangeSteps") as? Int{
                if globalSteps == 0 {
                    globalSteps = initialSteps
                    UserDefaults.standard.setValue(globalSteps, forKey: "ChallangeSteps")
                    self.delegate?.footStepsContinuousUpdate(footSteps: self.globalSteps)
                    return
                }
            }
        }
        
        let serviceTye = HealthServiceType.stepCount
        let date = Date.init(timeIntervalSinceNow: 86400)

        if globalSteps == 0 && are > 0{
            self.updateValue(updateValue: are, are: globalSteps)
            return
        }
        
        healthService.sampleData(forServiceType: serviceTye,
                                 sampleStartTime: challengeDate as Date,
                                 sampleEndTime: date as Date) { (value, error) in
            guard error == nil else {
                return
            }
        } onUpdate: { (updateValue, updateError) in
            guard updateError == nil else {
                return
            }
            self.updateValue(updateValue: Int(updateValue), are: are)
        }
        
          
  }
    
    func updateValue(updateValue : Int, are: Int){
        self.globalSteps = self.globalSteps + Int(updateValue)
        UserDefaults.standard.setValue(globalSteps, forKey: "ChallangeSteps")
        UserDefaults.standard.setValue(globalSteps, forKey: "InitalChallangeSteps")
        self.delegate?.footStepsContinuousUpdate(footSteps: self.globalSteps)
        if(Int(updateValue) >= are){
            self.delegate?.footStepsReading(footSteps: self.globalSteps)
        }
    }
    
}
