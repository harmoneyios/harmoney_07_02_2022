//
//  DoneChoreViewController.swift
//  Harmoney
//
//  Created by Mac on 28/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol DoneChoreViewControllerDelegate: class {
    func okayButton(doneChoreViewController: DoneChoreViewController)
}

class DoneChoreViewController: UIViewController {
    @IBOutlet weak var choreTypeImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    weak var delegate: DoneChoreViewControllerDelegate?
    var selectedTag: Int?
    var choresListModel: ChoresListModel?
    
//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let indexData = choresListModel?.data![selectedTag!]
        
       let formatter = DateFormatter()
       formatter.dateFormat = "yyyy-MM-dd"
           
       guard let dayObj = indexData?.taskList?.filter({$0.date == formatter.string(from: Date())}) else{
           return
       }
        choreTypeImageView.imageFromURL(urlString: indexData?.task_image_url ?? "")
        var descmsg = ""
        if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            descmsg = "Yay! You successfully completed full \(indexData?.task_name ?? "").\nWaitings for your parent's approval."
        }else{
            "Yay! You successfully completed full \(indexData?.task_name ?? "")."
        }
        titleLabel.text =  "Congrats!\nYou have successfully completed the chore! "
        descriptionLabel.text =  descmsg
        descriptionLabel.adjustsFontSizeToFitWidth = true
        
    }
}

//MARK: Action Methods
extension DoneChoreViewController {
    @IBAction func okayButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        delegate?.okayButton(doneChoreViewController: self)       
    }
    
}
