//
//  RestartChallageViewController.swift
//  Harmoney
//
//  Created by INQ Projects on 04/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

protocol RestartChallangeViewControllerDelegate: class {
    func didTapThumbsDownButton(restartChallangeViewController: RestartChallageViewController)
    func didTapThumbsUpButton(restartChallangeViewController: RestartChallageViewController,challenge:PersonalChallenge)
}

class RestartChallageViewController: UIViewController {
    @IBOutlet weak var challengeTypeImageView: UIImageView!
    
    weak var delegate: RestartChallangeViewControllerDelegate?
    var indexPath: IndexPath?
    var personalChallenge: PersonalChallenge?
    
//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    //MARK: UI Methods
    func configureUI() {
        switch personalChallenge?.data.challengeType {
         case .walk:
             challengeTypeImageView.image = UIImage(named: "walk_medium_icon")
             break
         case .swim:
             challengeTypeImageView.image = UIImage(named: "swim_icon")
             break
         case .run:
             challengeTypeImageView.image = UIImage(named: "run_icon")
             break
         case .bike:
             challengeTypeImageView.image = UIImage(named: "bike_icon")
             break
         case .none:
             break
         }
     }
}

//MARK: Action Methods
extension RestartChallageViewController {
    @IBAction func thumbsUpIconTapped(_ sender: UIButton) {
        delegate?.didTapThumbsUpButton(restartChallangeViewController: self,challenge: personalChallenge!)
    }
    
    @IBAction func thumbsDownIconTapped(_ sender: Any) {
        delegate?.didTapThumbsDownButton(restartChallangeViewController: self)
    }
}
