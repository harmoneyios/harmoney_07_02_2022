//
//  PersonalFitnessViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 15/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import SVProgressHUD
import Foundation
import Alamofire
import Toast_Swift
import ObjectMapper
import IQKeyboardManagerSwift

class PersonalFitnessViewController: BaseViewController {
    
    @IBOutlet weak var noChallengeContainerView: UIView!
    @IBOutlet weak var challengeTableVIew: UITableView!
    @IBOutlet weak var addNewChallengeButton: UIButton!
    @IBOutlet weak var addChallengeContainerView: UIView!
    @IBOutlet weak var challengeDetailsContainerView: UIView!
    
    let refresh = UIRefreshControl()

    let healthService = HealthService()

    let cellReuseIdentifier = "challengeCell"
    var personalChallengeList: [PersonalChallenge] {
        return PersonalFitnessManager.sharedInstance.personalChallengeList
    }
    
    var activeChallenge: PersonalChallenge? {
        return PersonalFitnessManager.sharedInstance.getActiveChallenge()
    }
    
    var startedChallenge: PersonalChallenge? {
        return PersonalFitnessManager.sharedInstance.getStartedChallenge()
    }
    var levelsModel: LevelsModel? {
          return LevelsManager.sharedInstance.levelsModel
      }
    var reloadPedometerUpdates = true
    var initialHealthKitCall = false
    var foreGroundHealthKitCall = false

    var challengeActionsViewController: ChallengeActionsViewController?
    var addChallengeViewController: AddChallengeViewController?
    var completedChallengeViewController: CompletedChallengeViewController?
    var deleteChallengeViewController: DeleteChallangeViewController?
    var customAlertViewController: CustomAlertViewController?
    var challengeDetailsViewController: ChallengeDetailsViewController?
    var restartChallengeViewController: RestartChallageViewController?
    var approveChallengeViewController: ApproveChallengeViewController?
    var debugView: PedometerDebugView?
    var activePersonalChallenge: PersonalChallenge?
    var getGuidForMobileNumber : guidForMobileNumberObject?
    var transfertoUserObjectValue:  transfertoUserObject?
    var getWalletObj : getwalletObject?
    var addHarmoneyWalletValue : addHarmoneyWalletDataObject?
    
    var screenLoadedFirstTime = false
    
    var stepCountTimer = Timer()
    var oldStepCountProgress = false
    var progressCount :Float = 0.0
    var nextDayProgress = false

    @IBOutlet weak var debugTextview: UITextView!
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    //MARK: Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        oldStepCountProgress = false
        configureNoChallengeUI()
        setupTableView()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(foregroundCall), name: UIApplication.didBecomeActiveNotification, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view .endEditing(true)
      self.getAllNotifications()
        getPersonalChallenges(isfromRefresh: false)
        IQKeyboardManager.shared.enable = true
        
    }
    private func getAllNotifications() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")
        if Reachability.isConnectedToNetwork(){
            var (url, method, param) = APIHandler().dashboard(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: value)
                       
                        if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
                        }else{
                            if HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0 <= 10
                            {
                                if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
                                    print("Am child")
                                }else{
                                  
                                    
                                }
                            }
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }else{
           
            self.view.toast("Internet Connection not Available!")
        }
        
    }
    @objc func foregroundCall() {
        foreGroundHealthKitCall = true
        initialHealthKitCall = false
        
        setupPedometerUpdates()
        
//        var serviceTye = HealthServiceType.stepCount
//
//        if self.activeChallenge?.data.challengeType == .run{
//            serviceTye = HealthServiceType.distanceWalked
//        }else if self.activeChallenge?.data.challengeType == .swim{
//            serviceTye = HealthServiceType.distanceSwam
//        }else if self.activeChallenge?.data.challengeType == .bike{
//            serviceTye = HealthServiceType.distanceCycled
//        }
//
//        let date = Date.init(timeIntervalSinceNow: 86400)
//        var currentTime = NSDate()
//
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        if let dateStr = self.activeChallenge?.data.updatedTime{
//            currentTime = formatter.date(from: (dateStr) )! as NSDate
//        }
//
//        healthService.getDistance(forServiceType: serviceTye, sampleStartTime: currentTime as Date, sampleEndTime: date as Date) { (value, error) in
//            guard error == nil else {
//                return
//            }
//            PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: Float(value)) { (result) in
//                switch result {
//                case .success(let newActiveChallenge):
//                    if self.reloadPedometerUpdates {
//                        self.challengeTableVIew.reloadData()
//                    }
//                case .failure(let error):
//                    print(error)
//                }
//            }
//        }
    }
    
    func getStartDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: "2020-12-13T12:25:00") // replace Date String
    }
    func getEndDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: "2020-12-13T12:26:00") // replace Date String
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if reloadPedometerUpdates {
            restartAnimation()
        }
        
        if PersonalFitnessManager.sharedInstance.checkActiveChallenge {
            PersonalFitnessManager.sharedInstance.checkActiveChallenge = false
            
            if screenLoadedFirstTime {
                self.setupPedometerUpdates()
            }
        }
        
        screenLoadedFirstTime = true
  
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stepCountTimer.invalidate()
      
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //addDebugView()
      
        

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddChallengeViewController" {
            addChallengeViewController = segue.destination as? AddChallengeViewController
            addChallengeViewController?.delegate = self
        } else if segue.identifier == "ChallengeDetailsViewController" {
            challengeDetailsViewController = segue.destination as? ChallengeDetailsViewController
            challengeDetailsViewController?.delegate = self
        }
    }
    
    
    @IBAction func ActionCopyText(_ sender: Any) {
        UIPasteboard.general.string = debugTextview.text
        self.view.toast("Log Text Copied")

    }
}

//MARK: - Private Methods

extension PersonalFitnessViewController {
    private func getPersonalChallenges(isfromRefresh:Bool) {
        SVProgressHUD.show()
        PersonalFitnessManager.sharedInstance.getPersonalChallenges { (result) in
            switch result {
            case .success(_):
                self.refresh.endRefreshing()
                SVProgressHUD.dismiss()
                
                self.configureNoChallengeUI()
                self.challengeTableVIew.reloadData()
                self.setupPedometerUpdates()
              
//                if isfromRefresh == false {
                    if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                        print(startedChallenge)
                        self.progressCount = startedChallenge.data.challengeRunStep ?? 0
                        let challengeId = startedChallenge.data.challengeId
                        let curentStep = startedChallenge.data.challengeRunStep

                        UserDefaults.standard.set(curentStep, forKey: "current_stepCount")

                        let guid =  UserDefaultConstants().guid ?? ""
                        UserDefaults.standard.set(guid, forKey: "current_userGuid")
                        UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                        
//                        self.removeUserDefaults()
                        self.getCurrentTimeAndDate()
                        self.timerMethod()
                     
//                    }
                }
//                self.checkActivityType()
            case .failure(let error):
                self.refresh.endRefreshing()
                SVProgressHUD.dismiss()
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
//    func checkActivityType() {
//        PedometerManager.sharedInstance.startMotionActivityUpdates { (activityType, changed) in
//
//            if changed {
//                self.setupPedometerUpdates()
//            }
//        }
//    }
    
    private func setupHealthData(){
        

    }
    private func setupforHealthKitData() {
        
        if self.activeChallenge != nil && !initialHealthKitCall{
//            if UserDefaultConstants().challangeStartDate != nil {
//
//            }else {
//                UserDefaultConstants().challangeStartDate  = Date() as NSDate
//            }
            
            var serviceTye = HealthServiceType.stepCount
            
            if self.activeChallenge?.data.challengeType == .run{
                serviceTye = HealthServiceType.distanceWalked
            }else if self.activeChallenge?.data.challengeType == .swim{
                serviceTye = HealthServiceType.distanceSwam
            }else if self.activeChallenge?.data.challengeType == .bike{
                serviceTye = HealthServiceType.distanceCycled
            }

            
//            var date = Date.init(timeIntervalSinceNow: 86400)
//            if foreGroundHealthKitCall{
//                foreGroundHealthKitCall = false
//                date = currentTime as Date
//            }
//
//            let formatter = DateFormatter()
////            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//            if let dateStr = self.activeChallenge?.data.updatedTime{
//                currentTime = formatter.date(from: (dateStr) )! as NSDate
//            }
//            self.initialHealthKitCall = true

            //   Get the start of the day
     
            let startDateAndTime = UserDefaults.standard.value(forKey: "startDate_Time")
            let currentTime = NSDate()
            print("today-->",currentTime)
           
            if startDateAndTime == nil{
                return
            }
            if self.activeChallenge?.data.challengeType != .bike{
                healthService.sampleDataforHealthkit(forServiceType: serviceTye,
                                                     sampleStartTime: startDateAndTime as! Date,
                                                     sampleEndTime: currentTime as Date) { (value, error) in
                    guard error == nil else {
                        print("health service error-->",error ?? "")
                        return
                    }
                } onUpdate: { (updateValue, updateError) in
                    guard updateError == nil else {
                        return
                    }
                }
                
            }
//            PedometerManager.sharedInstance.startUpdates(startDate: Date()) { (data, error) in
//
//                let progress = self.activeChallenge?.data.challengeType == .walk ? data?.numberOfSteps.floatValue ?? 0.0 : data?.distance?.floatValue ?? 0.0
//
//                PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: progress) { (result) in
//                    switch result {
//
//                    case .success(let newActiveChallenge):
//                        self.debugView?.challengeSource = String(describing: type(of: newActiveChallenge))
//                        self.debugView?.challengeID = newActiveChallenge.data.challengeId
//                        self.debugView?.startDate = data?.startDate
//                        self.debugView?.endDate = data?.endDate
//                        self.debugView?.nrOfSteps = data?.numberOfSteps
//                        self.debugView?.distance = data?.distance
//                        self.debugView?.activityType = PedometerManager.sharedInstance.currentActivity
//                        self.debugView?.challengeType = newActiveChallenge.data.challengeType
//                        self.debugView?.progress = newActiveChallenge.data.challengeProgress
//
//                        if newActiveChallenge.data.isCompleted == 1 {
//                            self.debugView?.challengeSource = nil
//                            self.debugView?.challengeID = nil
//                            self.debugView?.startDate = nil
//                            self.debugView?.endDate = nil
//                            self.debugView?.nrOfSteps = nil
//                            self.debugView?.distance = nil
//                            self.debugView?.activityType = nil
//                            self.debugView?.challengeType = nil
//                            self.debugView?.progress = nil
//                        }
//
//                        if self.reloadPedometerUpdates {
//                            self.challengeTableVIew.reloadData()
//                        }
//                    case .failure(let error):
//                        print(error)
//                        PedometerManager.sharedInstance.stopUpdates()
//                    }
//                }
//            }
        } else {
//            PedometerManager.sharedInstance.stopUpdates()
            self.debugView?.challengeSource = nil
            self.debugView?.challengeID = nil
            self.debugView?.startDate = nil
            self.debugView?.endDate = nil
            self.debugView?.nrOfSteps = nil
            self.debugView?.distance = nil
//            self.debugView?.activityType = nil
            self.debugView?.challengeType = nil
            self.debugView?.progress = nil
        }
    }
    // must be internal or public.
    @objc func updateHealthKitDatappp() {
        if Reachability.isConnectedToNetwork(){
        }else{
        self.view.toast("Internet Connection not Available!")
        return
    }
    
        let oldStepCount = UserDefaults.standard.value(forKey: "starting_StepCount")
        let newStepCount = UserDefaults.standard.value(forKey: "update_StepCount")
        
        let oldValue = "\(oldStepCount ?? "")"
        let newValue = "\(newStepCount ?? "")"

        let float1 = Float(oldValue) ?? 0
        let float2 = Float(newValue) ?? 0
        
        var stepCount = Float()

        if  oldStepCount != nil {
            
            self.getCurrentTimeAndDate()

            let todayDate = "\(UserDefaults.standard.value(forKey: "startDate_Time") ?? "")"
            let tommorrowDate = "\(UserDefaults.standard.value(forKey: "updateDate_Time") ?? "")"

            if todayDate == tommorrowDate {
                stepCount = float2 - float1
                let sCounts =  String(format: "%.2f", stepCount)
                stepCount = Float(sCounts) ?? 0
                UserDefaults.standard.removeObject(forKey: "final_Stepcount")

                if oldStepCountProgress == false && progressCount > 0 && stepCount == 0 {
                    oldStepCountProgress = true
                    if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                        switch startedChallenge.data.challengeType {
                        case .walk:
                            let progressStepCount =  (progressCount)
                            stepCount = progressStepCount
                            let oldStep = float1 - (progressCount)
                            let newStep = oldStep + stepCount

                            UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
                            UserDefaults.standard.set(newStep, forKey: "update_StepCount")

                            progressCount = 0.0
                            break
                        case .swim:
                            //distance swim or pool swim
                            if modeofChallenge == 4 {
                                HomeViewController().healthService.getswmmingDistance { (distance, error) in
                                    DispatchQueue.main.async {
                                        let stringValue = String(format: "%.2f", distance ?? 0.0)
                                        stepCount = Float(distance!)
                                    }
                                }
                            }else {
                                stepCount = Float(travelleddistance)
                            }
                            let progressStepCount = (progressCount)
                            stepCount = progressStepCount
                            let oldStep = float1 - (progressCount)
                            let newStep = oldStep + stepCount

                            UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
                            UserDefaults.standard.set(newStep, forKey: "update_StepCount")

                            progressCount = 0.0
                            break
                        case .run:
                            let progressStepCount = (progressCount)
                            stepCount = progressStepCount
                            let oldStep = float1 - (progressCount)
                            let newStep = oldStep + stepCount

                            UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
                            UserDefaults.standard.set(newStep, forKey: "update_StepCount")

                            progressCount = 0.0
                            break
                        case .bike:
                            if modeofChallenge == 2 {
                                HomeViewController().healthService.getCyclingDistance { (distance, error) in
                                    DispatchQueue.main.async {
                                        
                                        let stringValue = String(format: "%.2f", distance ?? 0.0)
                                        stepCount = Float(distance!)
                                        print("cycling distance \(stringValue)")
                                        //  self.KmsLabel.text = stringValue
                                    }
                                }
                            }else if modeofChallenge == 1 {
                                stepCount = Float(travelleddistance)
                            }
                            let progressStepCount = (progressCount)
                            stepCount = progressStepCount
                            let oldStep = float1 - (progressCount)
                            let newStep = oldStep + stepCount

                            UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
                            UserDefaults.standard.set(newStep, forKey: "update_StepCount")

                            progressCount = 0.0
                            break
                        }
                    }
                }
                
                UserDefaults.standard.set(stepCount, forKey: "total_Stepcount")

            } else {

                if float1 == 0{

                    let sTotal = UserDefaults.standard.value(forKey: "final_Stepcount")
                    let totalS = "\(sTotal ?? "")"

                    let totalSteps = Float(totalS) ?? 0
                    let progressStepCount = totalSteps

                    stepCount = float2 + progressStepCount
                    
                    UserDefaults.standard.set(stepCount, forKey: "total_Stepcount")

                   
                } else {
                    stepCount = float2 - float1
                    UserDefaults.standard.set(stepCount, forKey: "total_Stepcount")
                    UserDefaults.standard.set(0, forKey: "starting_StepCount")
                    UserDefaults.standard.set(0, forKey: "update_StepCount")
                    UserDefaults.standard.set(true, forKey: "nextDay_Progress")

                }
                let sCounts =  String(format: "%.2f", stepCount)
                stepCount = Float(sCounts) ?? 0

            }
            
        } else if oldStepCount == nil && progressCount > 0 {
            
            if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {

                switch startedChallenge.data.challengeType {
                    case .walk:
    
                        let progressStepCount = (progressCount)
                        stepCount = progressStepCount
                        
                        break
                    case .swim:
                        if modeofChallenge == 3 {
                            let progressStepCount = travelleddistance // (progressCount)
                            stepCount = Float (progressStepCount)
                        }else {
                        let progressStepCount = (progressCount)
                        stepCount = progressStepCount
                        }
                       break
                    case .run:
                        let progressStepCount = (progressCount)
                        stepCount = progressStepCount
                        break
                    case .bike:
                        if modeofChallenge == 1 {
                            let progressStepCount = travelleddistance // (progressCount)
                            stepCount = Float (progressStepCount)
                        }else {
                            let progressStepCount = (progressCount)
                            stepCount = progressStepCount
                        }
                        break
                    }
                
                
            }
        } else {
            /*if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                switch startedChallenge.data.challengeType {
                case .walk:
                    
                    let progressStepCount = (progressCount)
                    stepCount = progressStepCount
                    
                    break
                case .swim:
                    let progressStepCount = (progressCount)
                    stepCount = progressStepCount
                    break
                case .run:
                    if modeofChallenge == 3 {
                        let progressStepCount = travelleddistance // (progressCount)
                        stepCount = Float (progressStepCount)
                    }else {
                        let progressStepCount = (progressCount)
                        stepCount = progressStepCount
                    }
                    
                    break
                case .bike:
                    if modeofChallenge == 1 {
                        let progressStepCount = travelleddistance // (progressCount)
                        stepCount = Float (progressStepCount)
                    }else {
                        let progressStepCount = (progressCount)
                        stepCount = progressStepCount
                    }
                    break
                }
            }*/
        }
        
        self.setupforHealthKitData()
                
        
//        if  oldStepCount != nil {
//        if oldStepCountProgress == false {
//            oldStepCountProgress = true
//            if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
//                switch startedChallenge.data.challengeType {
//                case .walk:
//
//                    let progressStepCount = stepCount + (progressCount / 10)
//                    stepCount = progressStepCount
//                    let oldStep = float1 - (progressCount / 10)
//                    let newStep = oldStep + stepCount
//
//                    UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
//                    UserDefaults.standard.set(newStep, forKey: "update_StepCount")
//
//                    progressCount = 0.0
//                    break
//                case .swim:
//
//                    break
//                case .run:
//                    let progressStepCount = stepCount + (progressCount)
//                    stepCount = progressStepCount
//                    let oldStep = float1 - (progressCount)
//                    let newStep = oldStep + stepCount
//
//                    UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
//                    UserDefaults.standard.set(newStep, forKey: "update_StepCount")
//
//                    progressCount = 0.0
//                    break
//                case .bike:
//
//                    break
//                }
//            }
//        }
//        }
        print("current step count-->",stepCount)
        
       let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge()
        
        let startDateAndTime = UserDefaults.standard.value(forKey: "startDate_Time")
        let today = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let currentTime = formatter .string(from: today as Date)
        
        let count = startedChallenge?.data.challengeRunStep ?? 0
        
        let currentGuid = UserDefaults.standard.value(forKey: "current_userGuid")
        let challengeId = UserDefaults.standard.value(forKey: "challenge_id")
        
        let totalStep = stepCount == 0 ? count : stepCount
        

      //  let debugText = "\nStarting Time-->\(startDateAndTime ?? "")\nCurrent Time-->\(currentTime )\nStarting Step count -->\(oldValue )\nUpdate Step count -->\(newValue)\nCurrent Step count-->\(stepCount) \nStep count from backend --> \(count)\nCurrent UserId -->\(currentGuid ?? "")\nCurrent challenge id -->\(challengeId ?? "")\nTotal step count -->\(totalStep)\n"
        
        //debugTextview.text += debugText
        let reminingValue = Float((startedChallenge?.data.challengeTask) ?? 0) - stepCount
        if modeofChallenge == 1 || modeofChallenge == 3 {
            let distance = BaseViewController().travelledDistance(locations: locations1)
           // BaseViewController().locationPermissionRequest()
            stepCount = Float(travelleddistance)
     //   debugTextview.text = "\n Step count-->\(stepCount)     \n Update Step count -->\(travelleddistance)\n Remaing to walk --> \(reminingValue)"
        print("\(debugTextview.text ?? "")")
        }else {
     //      debugTextview.text = "\n Step count-->\(stepCount)     \n Update Step count -->\(reminingValue)\n Remaing to walk --> \(reminingValue)"
            print("\(debugTextview.text ?? "")")        }
        
        if stepCount != 0  {
        PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: Float(stepCount)) { (result) in
            switch result {
            case .success(let newActiveChallenge):
                        if self.reloadPedometerUpdates {
                            self.challengeTableVIew.reloadData()
                        }
            case .failure(let error):
                print(error)
            }
        }
        }
        
        if personalChallengeList.count == 0{
            stepCountTimer.invalidate()
            debugTextview.text = ""
        }
    }
    
//    @objc func updateHealthKitData() {
//        self.setupforHealthKitData()
//        let oldStepCount = UserDefaults.standard.value(forKey: "starting_StepCount")
//        let newStepCount = UserDefaults.standard.value(forKey: "update_StepCount")
//        let oldValue = "\(oldStepCount ?? "")"
//        let newValue = "\(newStepCount ?? "")"
//
//        let float1 = Float(oldValue) ?? 0
//        let float2 = Float(newValue) ?? 0
//
//        var stepCount = Float()
//        if float2 >= float1 {
//            stepCount = float2 - float1
//        } else {
//            stepCount = 0
//        }
//
//        let sCounts =  String(format: "%.2f", stepCount)
//        stepCount = Float(sCounts) ?? 0
//
//        if  oldStepCount != nil {
//        if oldStepCountProgress == false {
//            oldStepCountProgress = true
//            if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
//                switch startedChallenge.data.challengeType {
//                case .walk:
//
//                    let progressStepCount = stepCount + (progressCount / 10)
//                    stepCount = progressStepCount
//                    let oldStep = float1 - (progressCount / 10)
//                    let newStep = oldStep + stepCount
//
//                    UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
//                    UserDefaults.standard.set(newStep, forKey: "update_StepCount")
//
//                    progressCount = 0.0
//                    break
//                case .swim:
//
//                    break
//                case .run:
//                    let progressStepCount = stepCount + (progressCount)
//                    stepCount = progressStepCount
//                    let oldStep = float1 - (progressCount)
//                    let newStep = oldStep + stepCount
//
//                    UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
//                    UserDefaults.standard.set(newStep, forKey: "update_StepCount")
//
//                    progressCount = 0.0
//                    break
//                case .bike:
//
//                    break
//                }
//            }
//        }
//        }
//        print("current step count-->",stepCount)
//
//       let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge()
//
//        let startDateAndTime = UserDefaults.standard.value(forKey: "startDate_Time")
//        let today = NSDate()
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        let currentTime = formatter .string(from: today as Date)
//
//        let count = startedChallenge?.data.challengeRunStep ?? 0
//        let debugText = "\nStarting Time-->\(startDateAndTime ?? "")\nCurrent Time-->\(currentTime )\nStarting Step count -->\(oldValue )\nUpdate Step count -->\(newValue)\nCurrent Step count-->\(stepCount) \nStep count from backend --> \(count)\n"
//        debugTextview.text += debugText
//
//        PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: Float(stepCount)) { (result) in
//            switch result {
//            case .success(let newActiveChallenge):
//                        if self.reloadPedometerUpdates {
//                            self.challengeTableVIew.reloadData()
//                        }
//            case .failure(let error):
//                print(error)
//            }
//        }
//    }
    public func getCurrentTimeAndDate(){
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let startTime = cal.startOfDay(for: date)
       
        let startTimedate = UserDefaults.standard.value(forKey: "startDate_Time")
        if startTimedate == nil {
            UserDefaults.standard.set(startTime, forKey: "startDate_Time")
            UserDefaults.standard.set(startTime, forKey: "updateDate_Time")

        }else{
            UserDefaults.standard.set(startTime, forKey: "startDate_Time")

        }
        
        let todayDate = "\(UserDefaults.standard.value(forKey: "startDate_Time") ?? "")"
        let tommorrowDate = "\(UserDefaults.standard.value(forKey: "updateDate_Time") ?? "")"
        
        if todayDate == tommorrowDate {
            UserDefaults.standard.set(0, forKey: "final_Stepcount")

        } else {
            let nextDay = UserDefaults.standard.bool(forKey: "nextDay_Progress")
            if nextDay {
                UserDefaults.standard.set(false, forKey: "nextDay_Progress")
                let sTotal = UserDefaults.standard.value(forKey: "total_Stepcount")
                UserDefaults.standard.set(sTotal, forKey: "final_Stepcount")
            }

        }
       

        
    }
    
    func timerMethod()  {
        self.stepCountTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.updateHealthKitDatappp), userInfo: nil, repeats: true)
    }
    
    func removeUserDefaults()  {
        UserDefaults.standard.removeObject(forKey: "startDate_Time")
        UserDefaults.standard.removeObject(forKey: "updateDate_Time")
        UserDefaults.standard.removeObject(forKey: "starting_StepCount")
        UserDefaults.standard.removeObject(forKey: "update_StepCount")
    }
    
    func showAlertView(challenge: PersonalChallenge?)  {
        let alert = UIAlertController(title: "Alert", message: "Are you want to remove current challenge", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            let challengeId = challenge?.data.challengeId ?? ""
            
            self.startChallenge(challenge: challenge)
            
            UserDefaults.standard.set(true, forKey: "newChallenge_Created")
            UserDefaults.standard.set(challengeId, forKey: "challenge_id")
            
            self.stepCountTimer.invalidate()
            self.removeUserDefaults()
//            self.oldStepCountProgress = false
            if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                if startedChallenge.data.isCompleted == 0{
                PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: Float(0)) { (result) in
                    switch result {
                    case .success(let newActiveChallenge):
                                if self.reloadPedometerUpdates {
                                    self.challengeTableVIew.reloadData()
                                }
                    case .failure(let error):
                        print(error)
                    }
                }
            } else {
                
            }
        }
             
            }))
       // refreshAlert.addAction(OKAction)
        present(alert, animated: true, completion: nil)

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
        //          print("Handle Cancel Logic here")
        }))
    }
    private func setupPedometerUpdates() {
        
//        if self.activeChallenge != nil && !initialHealthKitCall{
////            if UserDefaultConstants().challangeStartDate != nil {
////
////            }else {
////                UserDefaultConstants().challangeStartDate  = Date() as NSDate
////            }
//
//            var serviceTye = HealthServiceType.stepCount
//
//            if self.activeChallenge?.data.challengeType == .run{
//                serviceTye = HealthServiceType.distanceWalked
//            }else if self.activeChallenge?.data.challengeType == .swim{
//                serviceTye = HealthServiceType.distanceSwam
//            }else if self.activeChallenge?.data.challengeType == .bike{
//                serviceTye = HealthServiceType.distanceCycled
//            }
//
//            var currentTime = NSDate()
//
//            var date = Date.init(timeIntervalSinceNow: 86400)
//            if foreGroundHealthKitCall{
//                foreGroundHealthKitCall = false
//                date = currentTime as Date
//            }
//
//            let formatter = DateFormatter()
//            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//            if let dateStr = self.activeChallenge?.data.updatedTime{
//                currentTime = formatter.date(from: (dateStr) )! as NSDate
//            }
//            self.initialHealthKitCall = true
//
//
//            healthService.sampleData(forServiceType: serviceTye,
//                                     sampleStartTime: currentTime as Date,
//                                     sampleEndTime: date as Date) { (value, error) in
//                guard error == nil else {
//
//                    print("health service error-->",error ?? "")
//                    return
//                }
//
//
//                PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: Float(value)) { (result) in
//                    switch result {
//                    case .success(let newActiveChallenge):
//                        if self.reloadPedometerUpdates {
//                            self.challengeTableVIew.reloadData()
//                        }
//                    case .failure(let error):
//                        print(error)
//                    }
//                }
//
//            } onUpdate: { (updateValue, updateError) in
//                guard updateError == nil else {
//                    return
//                }
//                PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: Float(updateValue)) { (result) in
//                    switch result {
//                    case .success(let newActiveChallenge):
////                        UserDefaultConstants().challangeStartDate  = Date() as NSDate
//                        if self.reloadPedometerUpdates {
//                            self.challengeTableVIew.reloadData()
//                        }
//                    case .failure(let error):
//                        print(error)
//                    }
//                }
//            }
//
//
//
////            PedometerManager.sharedInstance.startUpdates(startDate: Date()) { (data, error) in
////
////                let progress = self.activeChallenge?.data.challengeType == .walk ? data?.numberOfSteps.floatValue ?? 0.0 : data?.distance?.floatValue ?? 0.0
////
////                PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: progress) { (result) in
////                    switch result {
////
////                    case .success(let newActiveChallenge):
////                        self.debugView?.challengeSource = String(describing: type(of: newActiveChallenge))
////                        self.debugView?.challengeID = newActiveChallenge.data.challengeId
////                        self.debugView?.startDate = data?.startDate
////                        self.debugView?.endDate = data?.endDate
////                        self.debugView?.nrOfSteps = data?.numberOfSteps
////                        self.debugView?.distance = data?.distance
////                        self.debugView?.activityType = PedometerManager.sharedInstance.currentActivity
////                        self.debugView?.challengeType = newActiveChallenge.data.challengeType
////                        self.debugView?.progress = newActiveChallenge.data.challengeProgress
////
////                        if newActiveChallenge.data.isCompleted == 1 {
////                            self.debugView?.challengeSource = nil
////                            self.debugView?.challengeID = nil
////                            self.debugView?.startDate = nil
////                            self.debugView?.endDate = nil
////                            self.debugView?.nrOfSteps = nil
////                            self.debugView?.distance = nil
////                            self.debugView?.activityType = nil
////                            self.debugView?.challengeType = nil
////                            self.debugView?.progress = nil
////                        }
////
////                        if self.reloadPedometerUpdates {
////                            self.challengeTableVIew.reloadData()
////                        }
////                    case .failure(let error):
////                        print(error)
////                        PedometerManager.sharedInstance.stopUpdates()
////                    }
////                }
////            }
//        } else {
////            PedometerManager.sharedInstance.stopUpdates()
//            self.debugView?.challengeSource = nil
//            self.debugView?.challengeID = nil
//            self.debugView?.startDate = nil
//            self.debugView?.endDate = nil
//            self.debugView?.nrOfSteps = nil
//            self.debugView?.distance = nil
////            self.debugView?.activityType = nil
//            self.debugView?.challengeType = nil
//            self.debugView?.progress = nil
//        }
    }
    
    private func startChallenge(challenge: PersonalChallenge?) {
        SVProgressHUD.show()
        if startedChallenge != nil {
            //Stop the currently active challenge
            changeChallengeStatus(challenge: startedChallenge, status: .stopped) { (result) in
                switch result {
                    
                case .success(_):
                    self.changeChallengeStatus(challenge: challenge, status: .started) { (result) in
                        switch result {
                            
                        case .success(_):
                            SVProgressHUD.dismiss()
                            self.challengeTableVIew.reloadData()
                            if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                                print(startedChallenge)
                                self.progressCount = startedChallenge.data.challengeRunStep ?? 0
                            }
                            
                            let challengeId = self.startedChallenge?.data.challengeId
                            let guid =  UserDefaultConstants().guid ?? ""
                            UserDefaults.standard.set(guid, forKey: "current_userGuid")
                            UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                            
                            self.getCurrentTimeAndDate()
                            self.timerMethod()
                            self.setupforHealthKitData()
//                            self.setupPedometerUpdates()
                        case .failure(_):
                            self.view.toast("Failed to start this challenge.")
                        }
                    }
                case .failure(_):
                    SVProgressHUD.dismiss()
                    self.view.toast("Failed to start this challenge.")
                }
            }
            
        } else {
            changeChallengeStatus(challenge: challenge, status: .started) { (result) in
                switch result {
                    
                case .success(_):
                    SVProgressHUD.dismiss()
                    self.challengeTableVIew.reloadData()
                    if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                        print(startedChallenge)
                        self.progressCount = startedChallenge.data.challengeRunStep ?? 0
                    }
                    let challengeId = self.startedChallenge?.data.challengeId
                    let guid =  UserDefaultConstants().guid ?? ""
                    UserDefaults.standard.set(guid, forKey: "current_userGuid")
                    UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                    
                    self.getCurrentTimeAndDate()
                    self.timerMethod()
                    self.setupforHealthKitData()
//                    self.setupPedometerUpdates()
                case .failure(_):
                    SVProgressHUD.dismiss()
                    self.view.toast("Failed to start this challenge.")
                }
            }
        }
    }
    
    private func changeChallengeStatus(challenge: PersonalChallenge?, status: ChallengeStatus, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        PersonalFitnessManager.sharedInstance.changePersonalChallengeStatus(challenge: challenge, status: status) { (result) in
            switch result {
                
            case .success(let response):
                handler(.success(response))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
}

//MARK: - UI Methods

extension PersonalFitnessViewController {
    func restartAnimation() {
        self.challengeTableVIew.reloadData()
    }
    
    func configureNoChallengeUI() {
        getAllNotifications()
        if personalChallengeList.count == 0 {
            noChallengeContainerView.isHidden = false
            addNewChallengeButton.isHidden = true
        } else {
            addNewChallengeButton.isHidden = false
            noChallengeContainerView.isHidden = true
            challengeTableVIew.isHidden = false
            addChallengeContainerView.isHidden = true
            reSelectAllMembers()
        }
    }
    
    func setupTableView() {
        challengeTableVIew.register(UINib.init(nibName: "ChallengeTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        challengeTableVIew.refreshControl = refresh
        refresh.addTarget(self, action: #selector(getpersonalchallenges), for: .valueChanged)
        challengeTableVIew.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 80, right: 0)
        challengeTableVIew.contentInset.bottom = addNewChallengeButton.frame.height
    }
    
    @objc func getpersonalchallenges()  {
        getPersonalChallenges(isfromRefresh: true)
        setupPedometerUpdates()
    }
    //regresh data from health kit
    @objc func getpersonalchallengesInfoFromHealthKit()  {
        //self.setupforHealthKitData()
        getPersonalChallenges(isfromRefresh: true)
        if modeofChallenge == 2 {
            HomeViewController().healthService.getCyclingDistance { (distance, error) in
                DispatchQueue.main.async {
                    
                    let stringValue = String(format: "%.2f", distance ?? 0.0)
                    print("cycling distance \(stringValue)")
                    self.progressCount = Float(distance ?? 0)
                    //  self.KmsLabel.text = stringValue
                }
            }
        }else  if modeofChallenge == 4 {
            HomeViewController().healthService.getswmmingDistance { (distance, error) in
                
                let stringValue = String(format: "%.2f", distance ?? 0.0)
                print("cycling distance \(stringValue)")
                self.progressCount = Float(distance ?? 0)
            }
        }
        //setupPedometerUpdates()
    }
    func addDebugView() {
        guard debugView == nil else { return }
        
        let height: CGFloat = 200
        let width: CGFloat = 240
        let y = view.frame.maxY - height
        
        debugView = PedometerDebugView(frame: CGRect(x: 0, y: y, width: width, height: height))
        view.addSubview(debugView!)
    }
    
    func presentChallengeActionsViewController() {
        if challengeActionsViewController == nil {
            challengeActionsViewController = GlobalStoryBoard().challengeActionsVC
            challengeActionsViewController?.delegate = self
        }
        tabBarController?.present(challengeActionsViewController!, animated: false, completion: nil)
    }
    
    func presentDeleteChallengeViewController(personalChallenge: PersonalChallenge, indexPath: IndexPath) {
        deleteChallengeViewController = GlobalStoryBoard().deleteChallengeVC
        deleteChallengeViewController?.indexPath = indexPath
        deleteChallengeViewController?.personalChallenge = personalChallenge
        deleteChallengeViewController?.delegate = self
        tabBarController?.present(deleteChallengeViewController!, animated: true, completion: nil)
    }
    func presentRestartChallengeViewController(personalChallenge: PersonalChallenge, indexPath: IndexPath) {
        restartChallengeViewController = GlobalStoryBoard().restartChallengeVC
        restartChallengeViewController?.indexPath = indexPath
        restartChallengeViewController?.personalChallenge = personalChallenge
        restartChallengeViewController?.delegate = self
        tabBarController?.present(restartChallengeViewController!, animated: true, completion: nil)
    }
    func presentRestartChallengeViewControllerOnPersonalChallenge(personalChallenge: PersonalChallenge) {
        restartChallengeViewController = GlobalStoryBoard().restartChallengeVC
        restartChallengeViewController?.personalChallenge = personalChallenge
        restartChallengeViewController?.delegate = self
        tabBarController?.present(restartChallengeViewController!, animated: true, completion: nil)
    }
    func presentCompletedChallengeViewController(personalChallenge: PersonalChallenge, indexPath: IndexPath) {
        completedChallengeViewController = GlobalStoryBoard().completedChallengeVC
        completedChallengeViewController?.personalChallenge = personalChallenge
        completedChallengeViewController?.indexPath = indexPath
        completedChallengeViewController?.delegate = self

        tabBarController?.present(completedChallengeViewController!, animated: true, completion: nil)
    }
    
    func presentCustomAlertViewController(isMessage: String, isTitle: String) {
        customAlertViewController = GlobalStoryBoard().customAlertVC
        
        if isTitle == ""
        {
            if HarmonySingleton.shared.checkChallengeStatus?.taskType == 3
            {
                customAlertViewController?.alertTitle = "Onboarding challenge"
            }else{
                customAlertViewController?.alertTitle = "Daily challenge"
            }
        }else{
            customAlertViewController?.alertTitle = isTitle
        }

        
//        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
//            customAlertViewController?.alertMessage = "Yay! The invitation has been sent! Waiting for your parent to accept your invite."
//
//        } else {
            customAlertViewController?.alertMessage = isMessage
//        }
        customAlertViewController?.delegate = self
        tabBarController?.present(customAlertViewController!, animated: true, completion: nil)
    }
    
    func removeTableViewCell(indexPathToRemove: IndexPath) {
        do {
            let index = try indexPathToRemove
            UIView.animate(withDuration: 1) {
                self.challengeTableVIew.performBatchUpdates({
                    self.challengeTableVIew.deleteRows(at: [index], with: .fade)
                }){(completed) in
                    self.reloadPedometerUpdates = true
                    self.restartAnimation()
                    self.configureNoChallengeUI()
                }
            }
        } catch {
            
        }
    }
}

//MARK: - Action Methods

extension PersonalFitnessViewController {
    @IBAction func newChallengeButtonTapped(_ sender: UIButton) {
        presentChallengeActionsViewController()
    }
}

//MARK: - UITableViewDelegate Methods

extension PersonalFitnessViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let personalChallenge = personalChallengeList[indexPath.row]
        challengeDetailsContainerView.isHidden = false
        
        challengeDetailsViewController?.personalChallenge = personalChallenge
    }
}

//MARK: - ChallengeTableViewCellDelegate Methods

extension PersonalFitnessViewController: ChallengeTableViewCellDelegate {
    func didTapChallengeTableViewCellStartButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        
        if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
            if startedChallenge.data.isCompleted == 0{
                if let indexPath = challengeTableVIew.indexPath(for: challengeTableViewCell)  {
                    presentRestartChallengeViewController(personalChallenge: challengeTableViewCell.personalChallenge! ,indexPath: indexPath)
                }
                
//            self.showAlertView(challenge: challengeTableViewCell.personalChallenge)
            }else {
                self.stepCountTimer.invalidate()
                self.removeUserDefaults()
//                self.oldStepCountProgress = false
                let challengeId = challengeTableViewCell.personalChallenge?.data.challengeId ?? ""

                UserDefaults.standard.set(true, forKey: "newChallenge_Created")
                UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                
                self.startChallenge(challenge: challengeTableViewCell.personalChallenge)
            }
        } else {
            switch challengeTableViewCell.personalChallenge?.data.challengeType {
            
                case .walk:
                    BaseViewController().locationmanage.stopUpdatingLocation()
                    modeofChallenge  = 0
                    break;
                case .swim:
                    if isPairedWithWatch {
                        //BaseViewController().locationmanage.stopUpdatingLocation()
                            AlertView().showAlertwithTwoActionWithSwimType(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeSwimmingDescription)
                    }else {
                        //gps enable and track the location
                        modeofChallenge = 3
                        BaseViewController().locationPermissionRequest()
                    }
                    break;
                case .run:
                    BaseViewController().locationmanage.stopUpdatingLocation()
                    modeofChallenge  = 0
                    break;
                case .bike:
                    if isPairedWithWatch {
                        BaseViewController().locationmanage.stopUpdatingLocation()
                            AlertView().showAlertwithTwoAction(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeDescription)
                    }else {
                        //gps enable and track the location
                        modeofChallenge = 1
                        BaseViewController().locationPermissionRequest()
                    }
                    break;
                case .none:
                    break;
            }
            self.stepCountTimer.invalidate()
            self.removeUserDefaults()
//            self.oldStepCountProgress = false
            let challengeId = challengeTableViewCell.personalChallenge?.data.challengeId ?? ""

            UserDefaults.standard.set(true, forKey: "newChallenge_Created")
            UserDefaults.standard.set(challengeId, forKey: "challenge_id")
            
            self.startChallenge(challenge: challengeTableViewCell.personalChallenge)
        }
       
    }
    
    func didTapChallengeTableViewCellNotAprovedButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
            challengeDetailsContainerView.isHidden = false
            challengeDetailsViewController?.personalChallenge = challengeTableViewCell.personalChallenge!
        }
    }
    
    func didTapChallengeTableViewCellWhatsNextButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
         if let personalChallenge = challengeTableViewCell.personalChallenge, let indexPath = challengeTableVIew.indexPath(for: challengeTableViewCell)  {
            // getPersonalChallenges(isfromRefresh: false) saroTest
            self.reloadPedometerUpdates = false
            let userDefaultsCompletChallengeComplete = UserDefaults.standard
            userDefaultsCompletChallengeComplete.set("completed", forKey: "completeFitnessChatboat")
                  
                  let userDefaultsCompletChallenge = UserDefaults.standard
                  if personalChallenge.data.challengeType == Harmoney.ChallengeType.walk
                  {
                      userDefaultsCompletChallenge.set("walk", forKey: "challengeType")
                  }else if personalChallenge.data.challengeType == Harmoney.ChallengeType.bike{
                    userDefaultsCompletChallenge.set("bike", forKey: "challengeType")
                  }else if personalChallenge.data.challengeType == Harmoney.ChallengeType.swim{
                    userDefaultsCompletChallenge.set("swim", forKey: "challengeType")
                  }else if personalChallenge.data.challengeType == Harmoney.ChallengeType.run{
                    userDefaultsCompletChallenge.set("run", forKey: "challengeType")
                  }
                  let userDefaultsCompletexperiance = UserDefaults.standard
                  userDefaultsCompletexperiance.set(personalChallenge.data.experiance, forKey: "experiance")
                  let userDefaultschallengeTask = UserDefaults.standard
                  userDefaultschallengeTask.set(personalChallenge.data.challengeTask, forKey: "challengeTask")
                  let userDefaultsCompletChallengeharmoneyBucks = UserDefaults.standard
                  userDefaultsCompletChallengeharmoneyBucks.set(personalChallenge.data.harmoneyBucks, forKey: "harmoneyBucks")
                  let userDefaultsCompletChallengediamond = UserDefaults.standard
                  userDefaultsCompletChallengediamond.set(personalChallenge.data.diamond, forKey: "diamond")
        
            self.presentCompletedChallengeViewController(personalChallenge: personalChallenge, indexPath: indexPath)
        }
    }
    
    func didTapChallengeTableViewCellRemoveButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        HarmonySingleton.shared.taskTitle = "personal"
        if let indexPath = challengeTableVIew.indexPath(for: challengeTableViewCell)  {
            presentDeleteChallengeViewController(personalChallenge: challengeTableViewCell.personalChallenge! ,indexPath: indexPath)
            
        }
    }
    
    func didTapChallengeTableViewCellCustomButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        presentCustomAlertViewController(isMessage: fromController, isTitle: "")
    }
    
    func didTapChallengeTableViewCellVideoButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {}
}

//MARK: - UITableViewDataSource Methods

extension PersonalFitnessViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personalChallengeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let userDefaultsCompledailytChallengeComplete = UserDefaults.standard
        userDefaultsCompledailytChallengeComplete.set("No", forKey: "setBackGround")
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! ChallengeTableViewCell
        
        
        var personalChallenge = personalChallengeList[indexPath.row]
        //personalChallenge.data.isCompleted = 1
        //personalChallenge.data.challengeAcceptType = .accept
        
        cell.delegate = self
        cell.isFromController = "FitnessTableview"
        cell.personalChallenge = personalChallenge
        cell.challengeCanStart = PersonalFitnessManager.sharedInstance.challengeCanStart(challenge: personalChallenge)
        cell.refreshButton.addTarget(self, action: #selector(getpersonalchallengesInfoFromHealthKit), for: .touchUpInside)
        cell.deciedeBtn.tag = indexPath.row
        cell.deciedeBtn.addTarget(self, action: #selector(showApproveChallengeViewController), for: .touchUpInside)
        cell.animateButton()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    @objc func showApproveChallengeViewController(_ sender: UIButton) {
        var personalChallenge = personalChallengeList[sender.tag]
        approveChallengeViewController = GlobalStoryBoard().approveChallengeVC
        approveChallengeViewController?.personalChallenge = personalChallenge
        approveChallengeViewController?.kidName = personalChallenge.data.taskTo?.name
       approveChallengeViewController?.delegate = self
        HarmonySingleton.shared.challengeDecide = "personalFitnessList"
        present(approveChallengeViewController!, animated: true, completion: nil)
    }
}

//MARK: - ChallengeActionsViewControllerDelegate Methods

extension PersonalFitnessViewController: ChallengeActionsViewControllerDelegate {
    func challengeActionsViewControllerDidTapWalk(_ challengeActionsViewController: ChallengeActionsViewController) {
        reloadPedometerUpdates = false
        addChallengeContainerView.isHidden = false

        let walkData = PersonalChallengeData(challengeId: "", challengeType: .walk, challengeTask: 20, experiance: 200, harmoneyBucks: 0, diamond: 1, isCompleted: 0, challengeProgress: 0, aprovedId: "", createdDate: "", isClaimed: 0, challengeAcceptType: .padding, noAction: false)
        
        let walkChallenge = PersonalChallenge(data: walkData)
        
        addChallengeViewController?.personalChallenge = walkChallenge
    }
    
    func challengeActionsViewControllerDidTapRun(_ challengeActionsViewController: ChallengeActionsViewController) {
        reloadPedometerUpdates = false
        addChallengeContainerView.isHidden = false
        
        let runData = PersonalChallengeData(challengeId: "", challengeType: .run, challengeTask: 2, experiance: 200, harmoneyBucks: 0, diamond: 1, isCompleted: 0, challengeProgress: 0, aprovedId: "", createdDate: "", isClaimed: 0, challengeAcceptType: .padding, noAction: false)
        
        let runChallenge = PersonalChallenge(data: runData)
        
        addChallengeViewController?.personalChallenge = runChallenge
    }
    
    func challengeActionsViewControllerDidTapSwim(_ challengeActionsViewController: ChallengeActionsViewController) {
        reloadPedometerUpdates = false
        addChallengeContainerView.isHidden = false
        
        let swimData = PersonalChallengeData(challengeId: "", challengeType: .swim, challengeTask: 2, experiance: 200, harmoneyBucks: 0, diamond: 1, isCompleted: 0, challengeProgress: 0, aprovedId: "", createdDate: "", isClaimed: 0, challengeAcceptType: .padding, noAction: false)
         
        let swimChallenge = PersonalChallenge(data: swimData)
        
        addChallengeViewController?.personalChallenge = swimChallenge
    }
    
    func challengeActionsViewControllerDidTapBike(_ challengeActionsViewController: ChallengeActionsViewController) {
        reloadPedometerUpdates = false
        addChallengeContainerView.isHidden = false
        
        let bikeData = PersonalChallengeData(challengeId: "", challengeType: .bike, challengeTask: 2, experiance: 200, harmoneyBucks: 0, diamond: 1, isCompleted: 0, challengeProgress: 0, aprovedId: "", createdDate: "", isClaimed: 0, challengeAcceptType: .padding, noAction: false)
        
        let bikeChallenge = PersonalChallenge(data: bikeData)
        
        addChallengeViewController?.personalChallenge = bikeChallenge
    }
}

//MARK: CustomAlertViewControllerDelegate Methods
extension PersonalFitnessViewController: CustomAlertViewControllerDelegate {
    func didTapOkayButton(customAlertViewController: CustomAlertViewController) {
        customAlertViewController.dismiss(animated: true, completion: nil)
        getPersonalChallenges(isfromRefresh: false)
        self.challengeTableVIew.reloadData()
    }
}

//MARK: - AddChallengeViewControllerDelegate Methods

extension PersonalFitnessViewController: AddChallengeViewControllerDelegate {
    func didTapInviteParentButton(addChallengeViewController: AddChallengeViewController) {
        if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue == RegistrationType.child.rawValue {
            let profileNavigationViewController = tabBarController?.viewControllers?[3] as! UINavigationController
            let profileViewController = profileNavigationViewController.viewControllers[0] as! ProfileTabViewController
            profileViewController.inviteState = (true, .invite)
            
            tabBarController?.selectedIndex = 3
            tabBarController?.title = "challenge"
        } else {
            tabBarController?.selectedIndex = 3
        }
    }
    
    func didTapCloseAddChallenge(addChallengeViewController: AddChallengeViewController) {
        reloadPedometerUpdates = true
        addChallengeContainerView.isHidden = true
       reSelectAllMembers()
        
    }
    
    
    func reSelectAllMembers(){
        if let count = HarmonySingleton.shared.assigntomemberDataList?.count{
            for i in 0..<count{
                HarmonySingleton.shared.assigntomemberDataList?[i].isSelectAvathar = false
            }
        }
    }
    func didTapAddChallenge(addChallengeViewController: AddChallengeViewController, personalChallenge: PersonalChallenge) {
       getPersonalChallenges(isfromRefresh: false)
        configureNoChallengeUI()
        self.view .endEditing(true)
        UIView.animate(withDuration: 1) {
            self.challengeTableVIew.performBatchUpdates({
                self.challengeTableVIew.insertRows(at: [IndexPath(row: self.personalChallengeList.count - 1, section: 0)], with: .right)
            }) { (finished) in
                self.challengeTableVIew.setContentOffset(.zero, animated: true)
                self.reloadPedometerUpdates = true
            }
        }
    }
}

//MARK: CompletedChallengeViewControllerDelegate Methods

extension PersonalFitnessViewController: CompletedChallengeViewControllerDelegate {
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, personalChallenge: PersonalChallenge) {
        PersonalFitnessManager.sharedInstance.completePersonalChallenge(personalChallenge: personalChallenge, userLevel: completedChallengeViewController.levelDiference) { (result) in
                 switch result {
                     case .success( _):
                       completedChallengeViewController.dismiss(animated: true) {
                        updateGoals(goalType: .fitnessChallenge, success: nil)
                        
                           UIView.animate(withDuration: 1) {
                            if completedChallengeViewController.indexPath != nil {
                                self.reloadPedometerUpdates = true
                                self.challengeTableVIew.reloadData()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    self.getPersonalChallenges(isfromRefresh: false)
                                }
                                
//                                   self.removeTableViewCell(indexPathToRemove: indexPath)
                               }
                           }
                       }

                    HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
                     case .failure(let error):
                         self.view.toast(error.localizedDescription)
                 }
            }
    }
    
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, dailyChallenge: DailyChallenge) {}
    
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, onboardingChallenge: ChallengeData) {}
    func didTapNextButtonLevelupPopup(completedChallengeViewController: CompletedChallengeViewController){
        let levelInfo = self.levelsModel?.data.levelInfo[1]
        self.presentClaimLevelRewardViewController(levelInfo: levelInfo!)
       // self.getPersonalChallenges(isfromRefresh: false)
    }
    func presentClaimLevelRewardViewController(levelInfo: LevelInfo) {
        let claimLevelRewardViewController = GlobalStoryBoard().claimLevelRewardVC
        claimLevelRewardViewController.levelInfo = levelInfo
        claimLevelRewardViewController.isFromOnboarding = true
        claimLevelRewardViewController.delegate = self
        present(claimLevelRewardViewController, animated: true, completion: nil)
    }
}
extension PersonalFitnessViewController : CollectLevelRewardsDelegate{
    func leveleRegardsCollected(){
        let levelsViewController = GlobalStoryBoard().levelsVC
        levelsViewController.isFromOnboarding = true
        LevelsManager.sharedInstance.levelRewardClaimed = true
        present(levelsViewController, animated: true, completion: nil)
    }
}
//MARK: - DeleteChallangeViewControllerDelegate Methods

extension PersonalFitnessViewController: DeleteChallangeViewControllerDelegate {
    func didTapThumbsUpButtonChallengeDeleteDaillyChalleng(deleteChallangeViewController: DeleteChallangeViewController, daillyChallenge: DailyChallenge?) {
        
    }
    
    func didTapThumbsUpButtonChallengeDeleteOnboarding(deleteChallangeViewController: DeleteChallangeViewController, onboarding: ChallengeData) {
        
    }
    
    func didTapThumbsDownButton(deleteChallangeViewController: DeleteChallangeViewController) {
        deleteChallangeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapThumbsUpButtonOnPersonalChallenge(deleteChallangeViewController: DeleteChallangeViewController,challenge:PersonalChallenge) {
        SVProgressHUD.show()
            deleteChallangeViewController.dismiss(animated: true) {
                UIView.animate(withDuration: 0.5) {
                    if let indexPath = deleteChallangeViewController.indexPath {
                        self.reloadPedometerUpdates = false
                        let currentActiveChallengeId = self.activeChallenge?.data.challengeId
                        if currentActiveChallengeId == challenge.data.challengeId{
                            self.stepCountTimer .invalidate()
                        }
                        PersonalFitnessManager.sharedInstance.deletePersonalChallengeOnDaily(personalChallengeId:HarmonySingleton.shared.checkChallengeStatus?.taskId ?? "0") { (result) in
                            SVProgressHUD.dismiss()
                            switch result {
                            case .success(_):
                             
                                    
                                    if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                                        if startedChallenge.data.isCompleted == 0{
                                           
                                            self.presentRestartChallengeViewControllerOnPersonalChallenge(personalChallenge: challenge)
                                          
                                            
                            //            self.showAlertView(challenge: challengeTableViewCell.personalChallenge)
                                        }else {
                                            self.stepCountTimer.invalidate()
                                            self.removeUserDefaults()
                            //                self.oldStepCountProgress = false
                                            let challengeId = challenge.data.challengeId

                                            UserDefaults.standard.set(true, forKey: "newChallenge_Created")
                                            UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                                            
                                            self.startChallenge(challenge: challenge)
                                        }
                                    } else {
                                        switch challenge.data.challengeType {
                                        
                                            case .walk:
                                                BaseViewController().locationmanage.stopUpdatingLocation()
                                                modeofChallenge  = 0
                                                break;
                                            case .swim:
                                                if isPairedWithWatch {
                                                    //BaseViewController().locationmanage.stopUpdatingLocation()
                                                        AlertView().showAlertwithTwoActionWithSwimType(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeSwimmingDescription)
                                                }else {
                                                    //gps enable and track the location
                                                    modeofChallenge = 3
                                                    BaseViewController().locationPermissionRequest()
                                                }
                                                break;
                                            case .run:
                                                BaseViewController().locationmanage.stopUpdatingLocation()
                                                modeofChallenge  = 0
                                                break;
                                            case .bike:
                                                if isPairedWithWatch {
                                                    BaseViewController().locationmanage.stopUpdatingLocation()
                                                        AlertView().showAlertwithTwoAction(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeDescription)
                                                }else {
                                                    //gps enable and track the location
                                                    modeofChallenge = 1
                                                    BaseViewController().locationPermissionRequest()
                                                }
                                                break;
                                            
                                        }
                                        self.stepCountTimer.invalidate()
                                        self.removeUserDefaults()
                            //            self.oldStepCountProgress = false
                                        let challengeId = challenge.data.challengeId

                                        UserDefaults.standard.set(true, forKey: "newChallenge_Created")
                                        UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                                        
                                        self.startChallenge(challenge: challenge)
                                    }
                                   
                                
                            case .failure(let error):
                                self.reloadPedometerUpdates = true
                                self.view.toast(error.localizedDescription)
                            }
                        }
                    }
                }
            }
    }
    
    func didTapThumbsUpButton(deleteChallangeViewController: DeleteChallangeViewController,challenge:PersonalChallenge) {
        SVProgressHUD.show()
            deleteChallangeViewController.dismiss(animated: true) {
                UIView.animate(withDuration: 0.5) {
                    if let indexPath = deleteChallangeViewController.indexPath {
                        self.reloadPedometerUpdates = false
                        let currentActiveChallengeId = self.activeChallenge?.data.challengeId
                        if currentActiveChallengeId == challenge.data.challengeId{
                            self.stepCountTimer .invalidate()
                        }
                        PersonalFitnessManager.sharedInstance.deletePersonalChallenge(personalChallenge: deleteChallangeViewController.personalChallenge!) { (result) in
                            SVProgressHUD.dismiss()
                            switch result {
                            case .success(_):
                                self.removeTableViewCell(indexPathToRemove: indexPath)
                                
                                if currentActiveChallengeId == deleteChallangeViewController.personalChallenge?.data.challengeId {
                                    self.setupPedometerUpdates()
                                }
                            case .failure(let error):
                                self.reloadPedometerUpdates = true
                                self.view.toast(error.localizedDescription)
                            }
                        }
                    }
                }
            }
    }
}


//MARK: - RestartChallangeViewControllerDelegate Methods

extension PersonalFitnessViewController: RestartChallangeViewControllerDelegate {
    func didTapThumbsDownButton(restartChallangeViewController: RestartChallageViewController) {
        restartChallangeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapThumbsUpButton(restartChallangeViewController: RestartChallageViewController,challenge:PersonalChallenge) {
        
        restartChallangeViewController.dismiss(animated: true) {
            UIView.animate(withDuration: 1) {
                let challengeId = challenge.data.challengeId
                
                self.startChallenge(challenge: challenge)
                
                UserDefaults.standard.set(true, forKey: "newChallenge_Created")
                UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                
                self.stepCountTimer.invalidate()
                self.removeUserDefaults()
    //            self.oldStepCountProgress = false
                if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                    if startedChallenge.data.isCompleted == 0{
                    PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: Float(0)) { (result) in
                        switch result {
                        case .success(let newActiveChallenge):
                            travelleddistance = 0
                            if challenge.data.challengeType.rawValue == "Bike"{
                                if isPairedWithWatch {
                                    BaseViewController().locationmanage.stopUpdatingLocation()
                                        AlertView().showAlertwithTwoAction(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeDescription)
                                }else {
                                    //gps enable and track the location
                                    modeofChallenge = 1
                                    BaseViewController().locationPermissionRequest()
                                    BaseViewController().locationmanage.startUpdatingLocation()
                                }
                            }else if challenge.data.challengeType.rawValue == "Swim"{
                                if isPairedWithWatch {
                                    AlertView().showAlertwithTwoActionWithSwimType(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeSwimmingDescription)
                                }else {
                                    //gps enable and track the location
                                    modeofChallenge = 3
                                    BaseViewController().locationPermissionRequest()
                                }
                            } else {
                                BaseViewController().locationmanage.stopUpdatingLocation()
                                modeofChallenge  = 0
                            }
                                    if self.reloadPedometerUpdates {
                                        self.challengeTableVIew.reloadData()
                                    }
                        case .failure(let error):
                            print(error)
                        }
                    }
                } else {
                    
                }
            }
            }
        }
            
    }
}

//MARK: - ChallengeDetailsViewControllerDelegate Methods

extension PersonalFitnessViewController: ChallengeDetailsViewControllerDelegate {
    func didTapCloseButton(challengeDetailsViewController: ChallengeDetailsViewController) {
        challengeDetailsContainerView.isHidden = true
//        self.stepCountTimer.invalidate()
    }
}


//MARK: - ApproveChallengeViewControllerDelegate Methods

extension PersonalFitnessViewController: ApproveChallengeViewControllerDelegate {
    func didTapAcceptChallengeButton(approveChallengeViewController: ApproveChallengeViewController) {
       
        
//        approveChallengeViewController.personalChallenge?.data.challengeAcceptType = .accept
                
        let harmoneyBucks = Int(approveChallengeViewController.harmoneyTextfield.text ?? "")
        let customReward = approveChallengeViewController.customRewardTextfield.text
        
        var walletPrivateKeyKid:String = "[]"
        if let assigntomemberDataList = HarmonySingleton.shared.getKidWalletList{
            for objMember in assigntomemberDataList{
                if objMember.kidGuid == approveChallengeViewController.personalChallenge?.data.taskTo?.guid {
                    if let fromguid = objMember.walletAddress{
                        walletPrivateKeyKid = fromguid
                    }
                }
            }
        }
        
        if HarmonySingleton.shared.challengeDecide == "personalFitnessList"{
            if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
            }else{
                if self.approveChallengeViewController?.harmoneyTextfield.text ?? "" != "0" || self.approveChallengeViewController?.harmoneyTextfield.text ?? ""  != ""
                {
                    if HarmonySingleton.shared.getKidWalletList == nil && HarmonySingleton.shared.isHideForNewFlow == false
                    {
                        SVProgressHUD.dismiss()
                        AlertView.shared.showAlert(view: self, title: "Wallet Create", description: "Create sub Wallet to transfer amount")
                        AlertView.shared.delegate = self
                        return
                    }
                }
            }
            PersonalFitnessManager.sharedInstance.acceptPersonalChallenge(taskId: approveChallengeViewController.personalChallenge?.data.challengeId ?? "", requestUserGuid: approveChallengeViewController.personalChallenge?.data.taskTo?.guid ?? "", harmoneyBucks: harmoneyBucks ?? 0, customReward: customReward ?? "") { (result) in
                switch result {
                case .success(_):
                    self.transactionToUser(transferGuid: approveChallengeViewController.personalChallenge?.data.taskTo?.guid ?? "", harmoneyBucks: approveChallengeViewController.harmoneyTextfield.text ?? "",cryptoAddresskid: walletPrivateKeyKid)
                   
                        PersonalFitnessManager.sharedInstance.transferFitnessAmount(taskId: approveChallengeViewController.personalChallenge?.data.challengeId ?? "", type: 15) { (result) in
                            switch result {
                            case .success(_):
                                self.getAllNotifications()
                                approveChallengeViewController.dismiss(animated: true, completion: nil)
                                self.presentCustomAlertViewController(isMessage: "Yay! You have transfer Amount to kid.", isTitle: "Amount Approved")
                                self.challengeTableVIew.reloadData()
                               
                            case .failure(let error):
                                self.view.toast(error.localizedDescription)
                            }
                        }
                    
                    


                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
            }
        }else{
            PersonalFitnessManager.sharedInstance.acceptPersonalChallenge(taskId: approveChallengeViewController.personalChallenge?.data.challengeId ?? "", requestUserGuid: approveChallengeViewController.personalChallenge?.data.taskTo?.guid ?? "", harmoneyBucks: harmoneyBucks ?? 0, customReward: customReward ?? "") { (result) in
                switch result {
                case .success(_):
                    self.getAllNotifications()
                    approveChallengeViewController.dismiss(animated: true, completion: nil)
                    self.presentCustomAlertViewController(isMessage: "Yay! You have approved your kid's challenge completion!", isTitle: "Challenge Approved")

                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
            }

        }
        
    }
    func transactionToUser(transferGuid:String, harmoneyBucks:String, cryptoAddresskid:String) {
        
        if HarmonySingleton.shared.isHideForNewFlow {
            self.addHarmoneyWallet(amount: harmoneyBucks, requestUserGuid: transferGuid )
            return
        }
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
        data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
        data.updateValue(harmoneyBucks, forKey: "amount")
        data.updateValue(transferGuid , forKey: "transferGuid")
        let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
        data.updateValue(cryptoAddresskid, forKey: "walletAddress")
        var (url, method, param) = APIHandler().transferAmountToSila(params: data)
        
        AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                if  let value = value as? [String:Any] {
                    self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                    if self.transfertoUserObjectValue?.data?.status == "SUCCESS" {
                        self.view.toast(self.transfertoUserObjectValue?.data?.message ?? "")
                        self.reduceHarmoneyWallet(amount: harmoneyBucks )
//                        self.addHarmoneyWallet(amount: harmoneyBucks, requestUserGuid: transferGuid )
                    }else{
                        
                        if Reachability.isConnectedToNetwork() {
                            SVProgressHUD.show()
                            var data = [String : Any]()
                            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                            
                            data.updateValue("535-354-5454", forKey: "moblieNumber")
                            var (url, method, param) = APIHandler().getGuidForMobileNumber(params: data)
                            
                            AF.request(url, method: method, parameters: param,headers: self.headers).validate().responseJSON { response in
                                SVProgressHUD.dismiss()
                                switch response.result {
                                case .success(let value):
                                    if  let value = value as? [String:Any] {
                                        self.getGuidForMobileNumber = Mapper<guidForMobileNumberObject>().map(JSON: value)
                                        let guid = self.getGuidForMobileNumber?.data?.last?.guid
                                        let userName = self.getGuidForMobileNumber?.data?.last?.name
                                        if guid == nil
                                        {
                                            
                                        }else{
                                            
                                            var data = [String : Any]()
                                            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                                            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                                            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
                                            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
                                            data.updateValue(harmoneyBucks , forKey: "amount")
                                            data.updateValue(guid ?? "", forKey: "transferGuid")
                                            let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
                                            data.updateValue(cryptoAddress ?? "", forKey: "walletAddress")
                                            var (url, method, param) = APIHandler().transferAmountToSila(params: data)
                                            
                                            AF.request(url, method: method, parameters: param,headers: self.headers).validate().responseJSON { response in
                                                SVProgressHUD.dismiss()
                                                switch response.result {
                                                case .success(let value):
                                                    
                                                    if  let value = value as? [String:Any] {
                                                        self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                                                        self.view.toast(self.transfertoUserObjectValue?.data?.message ?? "")
                                                        self.reduceHarmoneyWallet(amount: harmoneyBucks )
                                                        self.addHarmoneyWallet(amount: harmoneyBucks, requestUserGuid: transferGuid )
                                                    }
                                                case .failure(let error):
                                                    print(error)
                                                }
                                            }
                                        }
                                        
                                    }
                                case .failure(let error):
                                    print(error)
                                }
                            }
                        }
                    }
                    
                    //                                                        self.transferAmountTxt.text = ""
                    //                                                        self.transferMobileNumberTxt.text = ""
                    //                                                        DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
                    //                                                            self.getWallet()
                    //                                                        }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
    func addHarmoneyWallet(amount: String, requestUserGuid: String){
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(requestUserGuid, forKey: "guid")
            
        data.updateValue(amount, forKey: "amount")
      
        var (url, method, param) = APIHandler().addHarmoneyWallet(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
//                            self.view.toast(self.addHarmoneyWalletValue?.message ?? "Wallet amount added to harmoney account")
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    
    func reduceHarmoneyWallet(amount: String){
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        data.updateValue(amount, forKey: "amount")
      
        var (url, method, param) = APIHandler().reduceHarmoneyWallet(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
//                            self.view.toast(self.addHarmoneyWalletValue?.message ?? "Wallet amount added to harmoney account")
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    func didTapDeclineChallengeButton(approveChallengeViewController: ApproveChallengeViewController) {
        
        
        approveChallengeViewController.personalChallenge?.data.challengeAcceptType = .decline
        
    
        
        PersonalFitnessManager.sharedInstance.rejectPersonalChallenge(taskId: approveChallengeViewController.personalChallenge?.data.challengeId ?? "", requestUserGuid:  approveChallengeViewController.personalChallenge?.data.taskTo?.guid ?? "") { (result) in
            switch result {
            case .success(_):
                self.getAllNotifications()
                approveChallengeViewController.dismiss(animated: true, completion: nil)
                self.challengeTableVIew.reloadData()
                self.presentCustomAlertViewController(isMessage: "You have rejected the task", isTitle: "Challenge Rejected")
               
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func didTapCloseButton(approveChallengeViewController: ApproveChallengeViewController) {
        approveChallengeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapAcceptLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController) {
        LevelsManager.sharedInstance.acceptLevelReward(invitedRequest: approveChallengeViewController.invitedRequest!) { (result) in
            switch result {
            case .success(_):
                approveChallengeViewController.dismiss(animated: true, completion: nil)
                self.view.toast("Level reward accepted.")
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func didTapDeclineLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController) {
        LevelsManager.sharedInstance.rejectLevelReward(invitedRequest: approveChallengeViewController.invitedRequest!) { (result) in
            switch result {
            case .success(_):
                approveChallengeViewController.dismiss(animated: true, completion: nil)
                self.view.toast("Level reward rejected.")
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
}

//MARK: - CustomAlertViewControllerDelegate Methods
extension PersonalFitnessViewController : AlertCallBack{
    func clickedOnOk() {
        self.dismiss(animated: true, completion: nil)
    }
}
