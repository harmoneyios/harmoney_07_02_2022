//
//  ChildRequestPopUpVC.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 29/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SVProgressHUD

class ChildInviteParentRequestPopUp: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    var selectedData : NotificationDash?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = selectedData?.message

        // Do any additional setup after loading the view.
    }
    @IBAction func rejectedClick(_ sender: Any) {
        accepetAndRejectAPI(isAccept: false)

    }
    

    @IBAction func acceptedClick(_ sender: Any) {
        accepetAndRejectAPI(isAccept: true)
        }
    
    func accepetAndRejectAPI(isAccept:Bool) {
        
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        data.updateValue(selectedData?.request?.requestUserGuid ?? "11", forKey: "requestUserGuid")
        data.updateValue(selectedData?.request?.memberId ?? "11", forKey: "memberId")

        var (url, method, param) = APIHandler().rejectParentInvite(params: data)
           
            if isAccept{
                (url, method, param) = APIHandler().acceptParentInvite(params: data)
            }
        
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                               case .success(let value):
                                
                                if let value = value as? [String:Any] {
                                    
                                    
                                    if let message = value["message"] as? String{
                                    self.view.toast(message)
                                  }
                                }
                                self.dismiss(animated: true, completion: nil)


                                   break
                               case .failure(let error):
                                self.dismiss(animated: true, completion: nil)

                                   break
                               }
            }
        }


    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
