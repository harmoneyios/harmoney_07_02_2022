//
//  EarnPageViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

class EarnPageViewController: UIPageViewController {

    var dailyVC = GlobalStoryBoard().dailyVC
    var fitnessVC = GlobalStoryBoard().fitnessVC
    var communityVC = GlobalStoryBoard().communityVC
    var wellnessVC = GlobalStoryBoard().wellnessVC
    var getInformationForPaymentObject : getInformationForPaymentObject?
    var  thumbsAlertViewController: ThumbsAlertViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
print("hello earn")
        
        self .getInformation()
        dataSource = self
        // Do any additional setup after loading the view.
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                direction: .forward,
                animated: true,
                completion: nil)
        }
        
    }
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [dailyVC,
                fitnessVC,
//                communityVC,
                wellnessVC]
    }()
    
    func setViewControllertoPage(index:Int) {      
        
        setViewControllers([orderedViewControllers[index]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }


    
    func getInformation(){
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let nowString = formatter.string(from: now)

                if let lastTime = UserDefaults.standard.string(forKey: "savedDate"), lastTime == nowString {
                    // Already notified today, skip
                    print("same date - no action")
                    return
                }
                
                UserDefaults.standard.set(nowString, forKey: "savedDate")
            
            if Reachability.isConnectedToNetwork() {
                SVProgressHUD.show()
                var data = [String : Any]()
                //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                
                let headers: HTTPHeaders = [
                    "Authorization": UserDefaultConstants().sessionToken ?? ""
                   
                ]
                var (url, method, param) = APIHandler().getInformationForPayment(params: data)
                print("get info\((url, method, param))")
                
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.getInformationForPaymentObject = Mapper<getInformationForPaymentObject>().map(JSON: value)
                            print(self.getInformationForPaymentObject!)
                            if ((self.getInformationForPaymentObject?.data?.user == nil) && (HarmonySingleton.shared.dashBoardData.data?.userType != .child))
                            {
                                        UserDefaults.standard.set("NoKey", forKey: "keyWallet")
                                        print("No account user nil")
                                        self.presentThumbsAlertViewController(alertTitle: " No Wallet!!!\nCreate a wallet for you?",buttonTitle : "Later")
                            }else{
                                if self.getInformationForPaymentObject?.data?.user?.walletPrivateKey == nil{
                                    UserDefaults.standard.set("NoKey", forKey: "keyWallet")
                                    print("No account walletprivatekey")
                              //  self.presentThumbsAlertViewController(alertTitle: " No Wallet!!!\nCreate a wallet for you?",buttonTitle : "Later")
                                    
                                    
                                }else{
                                    UserDefaults.standard.set(self.getInformationForPaymentObject?.data?.user?.walletPrivateKey, forKey: "keyWallet")
                                    print("low balance")
                                   // self.showBalanceAlert()
                                    
                                }
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }

    func presentThumbsAlertViewController(alertTitle: String, buttonTitle : String = "No") {
        thumbsAlertViewController = GlobalStoryBoard().thumbsAlertVC
        thumbsAlertViewController?.alertTitle = alertTitle
        thumbsAlertViewController?.buttonTitle = buttonTitle
        thumbsAlertViewController?.delegate = self
        self.present(thumbsAlertViewController!, animated: true, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension EarnPageViewController: ThumbsAlertViewControllerDelegate {
    func didTapThumbsUpButton(thumbsAlertViewController: ThumbsAlertViewController) {
        thumbsAlertViewController.dismiss(animated: true, completion: nil)
        let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "keyWallet")
        if walletPrivateKeyValue as! String == "NoKey"
       {        self.tabBarController?.selectedIndex = 3
        }else {
            if ((self.getInformationForPaymentObject?.data?.user?.processStage == 1)  ||   (self.getInformationForPaymentObject?.data?.user == nil) || (self.getInformationForPaymentObject?.data?.user?.walletPrivateKey == nil)) {
              
                    let couponDetails = GlobalStoryBoard().myFriendsVC
                    couponDetails.showView = "INFO"
                    self.navigationController?.pushViewController(couponDetails, animated: false)
                
            }else if (self.getInformationForPaymentObject?.data?.user?.processStage == 2 || self.getInformationForPaymentObject?.data?.user?.processStage == 3){
              
                    let couponDetails = GlobalStoryBoard().myFriendsVC
                    couponDetails.showView = "KYC"
                    self.navigationController?.pushViewController(couponDetails, animated: false)
               
            }else{

                let couponDetails = GlobalStoryBoard().myFriendsVC
                couponDetails.showView = "WALLET"
                self.navigationController?.pushViewController(couponDetails, animated: false)

            }
           }
    }
    
    func didTapThumbsDownButton(thumbsAlertViewController: ThumbsAlertViewController) {
        thumbsAlertViewController.dismiss(animated: true, completion: nil)
    }
        
}


extension EarnPageViewController : UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
//        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
//            return nil
//        }
//        
//        let previousIndex = viewControllerIndex - 1
//        
//        guard previousIndex >= 0 else {
//            return nil
//        }
//        
//        guard orderedViewControllers.count > previousIndex else {
//            return nil
//        }
//        
//        return orderedViewControllers[previousIndex]
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
//        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
//                 return nil
//             }
//
//             let nextIndex = viewControllerIndex + 1
//             let orderedViewControllersCount = orderedViewControllers.count
//
//             guard orderedViewControllersCount != nextIndex else {
//                 return nil
//             }
//
//             guard orderedViewControllersCount > nextIndex else {
//                 return nil
//             }
//
//             return orderedViewControllers[nextIndex]
        
        return nil
    }
    
    
}
