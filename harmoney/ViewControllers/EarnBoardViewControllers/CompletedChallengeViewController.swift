//
//  CompletedChallengeViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/27/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Lottie
import SummerSlider
import SVProgressHUD
import Alamofire

protocol CompletedChallengeViewControllerDelegate: class {
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, personalChallenge: PersonalChallenge)
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, dailyChallenge: DailyChallenge)
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, onboardingChallenge: ChallengeData)
    func didTapNextButtonLevelupPopup(completedChallengeViewController: CompletedChallengeViewController)
}
protocol CompletedChallengeViewControllerDelegateForChore: class {
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, chore: ChoresModel)
}

class CompletedChallengeViewController: UIViewController {
    @IBOutlet weak var topAnimationView: UIView!
    @IBOutlet weak var harmoneyBucksLabel: UILabel!
    @IBOutlet weak var slider: SummerSlider!
    @IBOutlet weak var experianceCounterLabel: AnimatedLabel!
    @IBOutlet weak var nextLevelLabel: UILabel!
    @IBOutlet weak var experianceEarnedLabel: UILabel!
    @IBOutlet weak var nextLevelExperianceLabel: UILabel!
    @IBOutlet weak var customRewardView: UIView!
    @IBOutlet weak var customRewardLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var earnGemsLbl: UILabel!
    @IBOutlet weak var rewrdHeadingLbl: UILabel!
    
    @IBOutlet weak var diamondImageview: UIImageView!
    
    var indexPath: IndexPath?
    var animationViewLayer =  AnimationView()
    var sliderCurrentPosition : Float = 0
    var sliderSlidePosition : Float = 0
    var personalChallenge: PersonalChallenge?
    var dailyChallenge: DailyChallenge?
    var onboardingChallenge: ChallengeData?
    var choreTask: ChoresModel?
    weak var delegate: CompletedChallengeViewControllerDelegate?
    weak var delegateChore: CompletedChallengeViewControllerDelegateForChore?
    var baseCurrentLevel = 0
    var levelDiference = 0
    var isFromChoresVC = false
    var isLevelUpPopupDisplay = false
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    var levelsModel: LevelsModel? {
          return LevelsManager.sharedInstance.levelsModel
      }
//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.slider.setValue(0, animated: false)
        baseCurrentLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        
        configuteTopAnimationView()
        callConfigAPI()
        setupInitialUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !isFromChoresVC {
            if dailyChallenge != nil {
                startAnimationsForDailyChallenge()
            } else if onboardingChallenge != nil {
                startAnimationForOnboardinChallenge()
            } else {
                startAnimationsForPersonalChallenge()
            }
        }
    }
}

//MARK: UI Methods
extension CompletedChallengeViewController {
    func animateSlider(completion: ((Bool) -> Void)? = nil) {
        
        UIView.animate(withDuration: 2.0, animations: {
            self.slider.setValue(self.sliderSlidePosition, animated: false)
            self.slider.layoutIfNeeded()
        }) { (finished) in
            if let handler = completion {
                handler(finished)
            }
        }
    }
    
    func startAnimationForOnboardinChallenge() {
        let userLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        let userCurrentXp = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: userLevel - 1)
        let currentTotalXp = userCurrentXp + (onboardingChallenge?.points?.xp ?? 0)
        let levelUp = currentTotalXp >= totalXp
//        let to = levelUp ? totalXp : currentXp
        
        if(currentTotalXp >= totalXp){
            self.experianceCounterLabel.countFromCurrent(to: Float(totalXp), duration: .brisk)
             }else{
                 self.experianceCounterLabel.countFromCurrent(to: Float(currentTotalXp), duration: .brisk)
             }
        
        self.nextLevelExperianceLabel.text = String(format: "/%d", totalXp) //"/200"//
        
        nextLevelLabel.text = String(format: "Next: Level %d", userLevel + 1)
        
        sliderSlidePosition = Float(currentTotalXp) / Float(200)
        
        animateSlider { (finsihed) in
            if finsihed {
                if levelUp {
                    let currentLvl =  HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
                    let currentXp =  HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
                    HarmonySingleton.shared.dashBoardData.data?.points?.level = currentLvl + 1
                    HarmonySingleton.shared.dashBoardData.data?.points?.xp =  currentXp + (totalXp - userCurrentXp)
                    self.onboardingChallenge?.points?.xp! -= (totalXp - userCurrentXp)
//                    self.experianceCounterLabel.countFromCurrent(to: 0.0, duration: .noAnimation)
//                    self.experianceCounterLabel.countFromCurrent(to: Float(currentTotalXp), duration: .noAnimation)
//                    self.nextLevelExperianceLabel.text = "/200"//String(format: "/%d", totalXp)
//                    self.slider.setValue(0, animated: false)brinok
//                    self.slider.layoutIfNeeded()
                    
//                    self.startAnimationForOnboardinChallenge()
                    self.isLevelUpPopupDisplay = true
                } else {
                   
                    let currentXp =  HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
                    HarmonySingleton.shared.dashBoardData.data?.points?.xp = currentXp + (self.onboardingChallenge?.points?.xp ?? 0)
                }
                
                self.nextButton.isUserInteractionEnabled = true
            }
        }
    }
    
    func startAnimationsForDailyChallenge() {
        let userLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        let userCurrentXp = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: userLevel-1)
        let currentTotalXp = userCurrentXp + (dailyChallenge?.xp ?? 0)
        let levelUp = currentTotalXp >= totalXp
        let to = levelUp ? totalXp : currentTotalXp
        
        if(currentTotalXp >= totalXp){
            self.experianceCounterLabel.countFromCurrent(to: Float(totalXp), duration: .brisk)
             }else{
                 self.experianceCounterLabel.countFromCurrent(to: Float(currentTotalXp), duration: .brisk)
             }
        self.nextLevelExperianceLabel.text = String(format: "/%d", totalXp)
        nextLevelLabel.text = String(format: "Next: Level %d", userLevel + 1)
        
        sliderSlidePosition = Float(currentTotalXp) / Float(totalXp)
        
        animateSlider { (finsihed) in
            if finsihed {
                if levelUp {
                    let currentLvl =  HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
                    let currentXp =  HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
                    HarmonySingleton.shared.dashBoardData.data?.points?.level = currentLvl + 1
                    HarmonySingleton.shared.dashBoardData.data?.points?.xp = currentXp + (totalXp - userCurrentXp)
                    self.dailyChallenge?.xp -= (totalXp - userCurrentXp)
//                    self.experianceCounterLabel.countFromCurrent(to: 0.0, duration: .noAnimation)
                    
//                    self.experianceCounterLabel.countFromCurrent(to: Float(currentTotalXp), duration: .noAnimation)
//                    self.nextLevelExperianceLabel.text = String(format: "/%d", totalXp)

//                    self.slider.setValue(0, animated: false)
//                    self.slider.layoutIfNeeded()
                    
//                    self.startAnimationsForDailyChallenge()
                    self.isLevelUpPopupDisplay = true

                } else {
                    
                    let currentXp =  HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
                    HarmonySingleton.shared.dashBoardData.data?.points?.xp = currentXp + (self.dailyChallenge?.xp ?? 0)
                }
                
                self.nextButton.isUserInteractionEnabled = true
            }
        }
    }
    
    func startAnimationsForPersonalChallenge() {
        let userLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        let userCurrentXp = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: userLevel-1)
        let currentTotalXp = userCurrentXp + (personalChallenge?.data.experiance ?? 0)
        let levelUp = currentTotalXp >= totalXp
        let to = levelUp ? totalXp : currentTotalXp
        
        if(currentTotalXp >= totalXp){
            self.experianceCounterLabel.countFromCurrent(to: Float(totalXp), duration: .brisk)
        }else{
            self.experianceCounterLabel.countFromCurrent(to: Float(currentTotalXp), duration: .brisk)
        }
        self.nextLevelExperianceLabel.text = String(format: "/%d", totalXp)
        nextLevelLabel.text = String(format: "Next: Level %d", userLevel + 1)
        
        sliderSlidePosition = Float(currentTotalXp) / Float(totalXp)
        
        animateSlider { [self] (finsihed) in
            if finsihed {
                if levelUp {
                    let currentLvl =  HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
                    let currentXp =  HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
                    HarmonySingleton.shared.dashBoardData.data?.points?.level = currentLvl + 1
                    HarmonySingleton.shared.dashBoardData.data?.points?.xp = currentXp + (totalXp - userCurrentXp)
                    self.personalChallenge?.data.experiance -= (totalXp - userCurrentXp)
//                    self.experianceCounterLabel.countFromCurrent(to: 0.0, duration: .noAnimation)
//                    self.experianceCounterLabel.countFromCurrent(to: Float(currentTotalXp), duration: .noAnimation)
//                    self.nextLevelExperianceLabel.text = String(format: "/%d", totalXp)
//                    self.slider.setValue(0, animated: false)
//                    self.slider.layoutIfNeeded()
                    
//                    self.startAnimationsForPersonalChallenge()
                    self.isLevelUpPopupDisplay = true

                } else {
//                    self.experianceCounterLabel.countFromCurrent(to: Float(currentTotalXp), duration: .brisk)
//                    self.nextLevelExperianceLabel.text = String(format: "/%d", totalXp)
                    let currentXp =  HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
                    HarmonySingleton.shared.dashBoardData.data?.points?.xp = currentXp + (self.personalChallenge?.data.experiance ?? 0)
                }
                
                self.nextButton.isUserInteractionEnabled = true
            }
        }
    }
    
    func setupInitialUI() {
        if !isFromChoresVC {
            nextButton.isUserInteractionEnabled = false
        }
        
       if dailyChallenge != nil {
            setupForDailyChallenge()
        } else if onboardingChallenge != nil {
            setupForOnboardingChallenge()
        } else {
            setupForPersonalChallenge()
        }
//        nextLevelExperianceLabel.isHidden = true
        configureSlider()
        
        let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
        
        if creditType == 1 {
            self.diamondImageview.image = UIImage(named: "soccer")

        } else if creditType == 2 {
            self.diamondImageview.image = UIImage(named: "diamond")

        } else {
            self.diamondImageview.image = UIImage(named: "touchdown")

        }
        
    }
    
    func setupForOnboardingChallenge() {
        customRewardView.isHidden = true
        self.isLevelUpPopupDisplay = false
        let userLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        let userCurrentXp = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: userLevel-1)
        let currentHarmoneyBucks = HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0
        let currentGem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
        
        HarmonySingleton.shared.dashBoardData.data?.points?.hbucks = currentHarmoneyBucks + (onboardingChallenge?.points?.hBucks ?? 0)
        HarmonySingleton.shared.dashBoardData.data?.points?.jem = currentGem + (onboardingChallenge?.points?.jem ?? 0)
        
        nextLevelLabel.text = String(format: "Next: Level %d", userLevel + 1)
        if(userCurrentXp >= totalXp){

            experianceCounterLabel.countFromZero(to: Float(totalXp), duration: .noAnimation)
        }else{
            experianceCounterLabel.countFromZero(to: Float(userCurrentXp), duration: .noAnimation)
        }
        
        nextLevelExperianceLabel.text = String(format: "/%d", totalXp)
        harmoneyBucksLabel.text = String(format: "%d", onboardingChallenge?.points?.hBucks ?? 0)
        earnGemsLbl.text = String(format: "%d", onboardingChallenge?.points?.jem ?? 0)
        experianceEarnedLabel.text = String(format: "+%d XP", onboardingChallenge?.points?.xp ?? 0)
        sliderCurrentPosition = Float(userCurrentXp) / Float(totalXp)
    }
    
    func setupForDailyChallenge() {
        customRewardView.isHidden = true
        self.isLevelUpPopupDisplay = false
        let userLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        let userCurrentXp = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: userLevel-1)
        let currentHarmoneyBucks = HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0
        let currentGem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
        
        HarmonySingleton.shared.dashBoardData.data?.points?.hbucks = currentHarmoneyBucks + (dailyChallenge?.hBucks ?? 0)
        let gem = dailyChallenge?.gem ?? "0"
        
        HarmonySingleton.shared.dashBoardData.data?.points?.jem = currentGem + (Int(gem) ?? 0)
        
        nextLevelLabel.text = String(format: "Next: Level %d", userLevel + 1)
        if(userCurrentXp >= totalXp){

            experianceCounterLabel.countFromZero(to: Float(totalXp), duration: .noAnimation)
        }else{
            experianceCounterLabel.countFromZero(to: Float(userCurrentXp), duration: .brisk)
        }
        nextLevelExperianceLabel.text = String(format: "/%d", totalXp)
        harmoneyBucksLabel.text = String(format: "%d", dailyChallenge?.hBucks ?? 0)
        experianceEarnedLabel.text = String(format: "+%d XP", dailyChallenge?.xp ?? 0)
        sliderCurrentPosition = Float(userCurrentXp) / Float(totalXp)
        earnGemsLbl.text = gem
    }
    
    func setupForPersonalChallenge() {
        if personalChallenge?.data.customReward?.count ?? 0 > 0 {
            customRewardLabel.text = personalChallenge?.data.customReward
        } else {
            customRewardView.isHidden = true
        }
        self.isLevelUpPopupDisplay = false
        let userLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        let userCurrentXp = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: userLevel-1)
        let currentHarmoneyBucks = HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0
        let currentGem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
        
        HarmonySingleton.shared.dashBoardData.data?.points?.hbucks = currentHarmoneyBucks + (personalChallenge?.data.harmoneyBucks ?? 0)
        HarmonySingleton.shared.dashBoardData.data?.points?.jem = currentGem + (personalChallenge?.data.diamond ?? 0)
        
        nextLevelLabel.text = String(format: "Next: Level %d", userLevel + 1)
        if(userCurrentXp >= totalXp){

            experianceCounterLabel.countFromZero(to: Float(totalXp), duration: .noAnimation)
        }else{
            experianceCounterLabel.countFromZero(to: Float(userCurrentXp), duration: .noAnimation)
        }
        nextLevelExperianceLabel.text = String(format: "/%d", totalXp)
        harmoneyBucksLabel.text = String(format: "%d", personalChallenge?.data.harmoneyBucks ?? 0)
        experianceEarnedLabel.text = String(format: "+%d XP", personalChallenge?.data.experiance ?? 0)
        sliderCurrentPosition = Float(userCurrentXp) / Float(totalXp)
        
        earnGemsLbl.text = String(format: "%d", personalChallenge?.data.diamond ?? 0)
    }
    
    func configuteTopAnimationView() {
        topAnimationView.addSubview(animationViewLayer)
        animationViewLayer.translatesAutoresizingMaskIntoConstraints = false
        animationViewLayer.leadingAnchor.constraint(equalTo: topAnimationView.leadingAnchor,constant: 0).isActive = true
        animationViewLayer.trailingAnchor.constraint(equalTo: topAnimationView.trailingAnchor,constant: 0).isActive = true
        topAnimationView.topAnchor.constraint(equalTo: animationViewLayer.topAnchor).isActive = true
    
        let path = Bundle.main.path(forResource: "successAnimationFour",
                                ofType: "json") ?? ""
        animationViewLayer.animation = Animation.filepath(path)
        animationViewLayer.contentMode = .scaleAspectFill
        animationViewLayer.loopMode = .loop
        animationViewLayer.play()
    }

    func configureSlider() {
        slider.setThumbImage(UIImage(named: "xp"), for: .normal)
        slider.unselectedBarColor =  UIColor(red: 255 / 255.0, green: 255 / 255.0, blue: 255 / 255.0, alpha: 1.0)
        slider.selectedBarColor =  UIColor(red: 248 / 255.0, green: 167 / 255.0, blue: 158 / 255.0, alpha: 1.0)
        slider.minimumValue = 0
        slider.maximumValue = 1
        
        if !isFromChoresVC {
            slider.setValue(sliderCurrentPosition, animated: true)
        }
    }
}

//MARK: Action Methods
extension CompletedChallengeViewController {
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        levelDiference = (HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0) - baseCurrentLevel
        if isFromChoresVC {
            delegateChore?.didTapNextButton(completedChallengeViewController: self, chore: choreTask!)
        } else {
            if let personalChallenge = self.personalChallenge {
                 delegate?.didTapNextButton(completedChallengeViewController: self, personalChallenge: personalChallenge)
            } else if let dailyChallenge = self.dailyChallenge {
                delegate?.didTapNextButton(completedChallengeViewController: self, dailyChallenge: dailyChallenge)
            } else if let onboardingChallenge = self.onboardingChallenge {
                delegate?.didTapNextButton(completedChallengeViewController: self, onboardingChallenge: onboardingChallenge)
            }
        }
        
        if (isLevelUpPopupDisplay){
            delegate?.didTapNextButtonLevelupPopup(completedChallengeViewController: self)
        }
        
    }
   
}
extension CompletedChallengeViewController : CollectLevelRewardsDelegate{
    func leveleRegardsCollected(){
        let levelsViewController = GlobalStoryBoard().levelsVC
        levelsViewController.isFromOnboarding = true
        LevelsManager.sharedInstance.levelRewardClaimed = true
        present(levelsViewController, animated: true, completion: nil)
    }
}

extension CompletedChallengeViewController {
    func callConfigAPI() {
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        let (url, method, param) = APIHandler().getConfig(params: [:])
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                       case .success(let value):
                        if let value = value as? [String:Any] {
                            let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                            let configModel = try? JSONDecoder().decode(ConfigModel.self, from: jsonData!)
                            if(self.isFromChoresVC){
                                self.choresDataForDisplay(config: configModel!)
                            }
                        }
                        break
                        case .failure(let error):
                        break
                       }
            }
        }
    }
    func choresDataForDisplay(config:ConfigModel) {
        customRewardView.isHidden = false
        let currentXP = (HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0) + Int(choreTask?.task_xp_value ?? "0")!
        let filArr = config.data.gamification.filter({($0.Runs.min <= currentXP) && ($0.Runs.max > currentXP)})
        var nextlvlMaxXp = 100000000
        var nextLevel = 0
        if filArr.count>0{
            nextlvlMaxXp = (filArr.last?.Runs.max)!
            nextLevel = (filArr.last?.Level)! + 1
        }
        
        displayUI(gems: choreTask?.task_gems_value ?? "0",
                  bucks: choreTask?.harmoney_bucks ?? "0",
                  rewardMsg: choreTask?.task_reward ?? "",
                  totalXP: "\(currentXP)",
                  maxXPforLevel: "\(nextlvlMaxXp)",
                  nextLevel: "\(nextLevel)",
                  taskXP: choreTask?.task_xp_value ?? "",
                  nextlevelMaxXP: "\(nextlvlMaxXp+1)")
    }
    func displayUI(gems:String,
                   bucks:String,
                   rewardMsg:String,
                   totalXP:String,
                   maxXPforLevel:String,
                   nextLevel:String,
                   taskXP:String,
                   nextlevelMaxXP:String)  {
        earnGemsLbl.text = gems //Earn Gems
        harmoneyBucksLabel.text = bucks.count > 0 ? bucks : "0" //Earn HB
        if rewardMsg.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
            customRewardLabel.text = rewardMsg //Reward Message
        }else{
           customRewardLabel.isHidden = true
            rewrdHeadingLbl.isHidden = true
        }
        experianceCounterLabel.countFromCurrent(to: Float(totalXP)!, duration: .brisk) //Earn XP + Existed XP
//        experianceCounterLabel.isHidden = true
        
        nextLevelLabel.text = "Next: Level\(nextLevel)" // Level Number
        experianceEarnedLabel.text = "+\(taskXP) XP"//Earn XP
        
    //totalXP: "\(currentXP)",
   // maxXPforLevel: "\(nextlvlMaxXp)",
        
        nextLevelExperianceLabel.text = "/\(nextlevelMaxXP)" //Next level Max XP
  
        self.nextButton.isUserInteractionEnabled = true
        
        let position = Float(Int(totalXP)!) / Float(Int(nextlevelMaxXP)!)
//            UIView.animate(withDuration: 2.0, animations: {
                self.slider.setValue(position, animated: false)
//                self.slider.layoutIfNeeded()
//            })
        }
}
