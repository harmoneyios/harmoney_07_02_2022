//
//  ChallengeActionsViewController.swift
//  Harmoney
//
//  Created by Norbert Korosi on 20/05/2020.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol ChallengeActionsViewControllerDelegate: class {
    func challengeActionsViewControllerDidTapWalk(_ challengeActionsViewController: ChallengeActionsViewController)
    func challengeActionsViewControllerDidTapRun(_ challengeActionsViewController: ChallengeActionsViewController)
    func challengeActionsViewControllerDidTapSwim(_ challengeActionsViewController: ChallengeActionsViewController)
    func challengeActionsViewControllerDidTapBike(_ challengeActionsViewController: ChallengeActionsViewController)
}

class ChallengeActionsViewController: UIViewController {

    let animationTimeInterval: Double = 0.2
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var walkButton: UIButton!
    @IBOutlet weak var stackViewPosY: NSLayoutConstraint!
    
    weak var delegate: ChallengeActionsViewControllerDelegate?
    
    var startPosition: CGFloat = 0
    
    //MARK: Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        animateIn()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        walkButton.roundCorners([.topLeft, .topRight], radius: 20)
    }
}

//MARK: UI & Action Methods

extension ChallengeActionsViewController {
    func configureUI() {
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        startPosition = stackViewPosY.constant
    }
    
    func animateIn() {
        UIView.animate(withDuration: animationTimeInterval) {
            self.stackViewPosY.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func animateOut() {
        UIView.animate(withDuration: animationTimeInterval, animations: {
            self.stackViewPosY.constant = self.startPosition
            self.view.layoutIfNeeded()
        }, completion: { (finished) in
            self.dismiss(animated: false, completion: nil)
        })
    }
    
    @IBAction func handleTapGesture(_ gesture: UITapGestureRecognizer) {
        animateOut()
    }
    
    @IBAction func walkButtonTapped(_ sender: UIButton) {
        animateOut()
        delegate?.challengeActionsViewControllerDidTapWalk(self)
    }
    
    @IBAction func runButtonTapped(_ sender: UIButton) {
        animateOut()
        delegate?.challengeActionsViewControllerDidTapRun(self)
    }
    
    @IBAction func swimButtonTapped(_ sender: UIButton) {
        animateOut()
        delegate?.challengeActionsViewControllerDidTapSwim(self)
    }
    
    @IBAction func bikeButtonTapped(_ sender: UIButton) {
        animateOut()
        delegate?.challengeActionsViewControllerDidTapBike(self)
    }
}
