//
//  ChorePendingAndApprovalViewController.swift
//  Harmoney
//
//  Created by Mac on 19/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SVProgressHUD
import ObjectMapper
import IQKeyboardManagerSwift

class ChorePendingAndApprovalViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var choresTableView: UITableView!
    let refresh = UIRefreshControl()
    
  var choreAcceptAndPendingCell = "choreAcceptAndPendingCell"
  var wellnessParentVC : WellnessViewController?
  var ChoresListModel : ChoresListModel?
  var removeChoreViewController: RemoveChoreViewController?
  var doneChoreViewController: DoneChoreViewController?
  var selectedTag = 0
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
       super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        choresTableView.delegate = self
        choresTableView.dataSource = self
        choresTableView.refreshControl = refresh
        refresh.addTarget(self, action: #selector(getAllSubCategory), for: .valueChanged)
        choresTableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 80, right: 0)        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getAllSubCategory()
         NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData), name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)

        }
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
        }
    @objc func onDidReceiveData(){
           getAllSubCategory()
       }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        getAllSubCategory()

    }


    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ChoresListModel?.data?.count ?? 0

       }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: choreAcceptAndPendingCell, for: indexPath) as! choreAcceptAndPendingCell

        cell.cellForRow(indexPath: self.ChoresListModel!, indexpath: indexPath, backgroundColor: "false")
            cell.doneBtn.tag = indexPath.row
            cell.doneBtn.addTarget(self, action: #selector(presentDonePopup), for: .touchUpInside)
            
            cell.removeBtn.tag = indexPath.row
            cell.removeBtn.addTarget(self, action: #selector(presentConfirmPopup), for: .touchUpInside)
            
            cell.rmvNwBtn.tag = indexPath.row
            cell.rmvNwBtn.addTarget(self, action: #selector(presentConfirmPopup), for: .touchUpInside)
        
        cell.decideBtn.tag = indexPath.row
        cell.decideBtn.addTarget(self, action: #selector(decideBtnAct), for: .touchUpInside)
            
            return cell
}
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        wellnessParentVC?.choreProgressContainerView.isHidden = false
//        wellnessParentVC?.handleViewControllersUI(nwCHoreVw: true, nwChoreVC: true, chorePendingVC: true, createChoreVC: true, choreProgressVC: false)
        ChoreProgressVm.shared.dataObj = ChoresListModel?.data![indexPath.row]
        wellnessParentVC?.choreProgressvc?.choreUITable.reloadData()
    }

     @IBAction func pushToChoreList(_ sender: Any) {
        wellnessParentVC?.handleViewControllersUI(nwCHoreVw: true, nwChoreVC: false, chorePendingVC: true, createChoreVC: true, choreProgressVC: true)
    }
    
    @objc func getAllSubCategory()  {
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
         var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")

        let (url, method, param) = APIHandler().getChorslist(params: data)
        print("Get chore list")
            print((url, method, param))
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                               case .success(let value):
                                
                                if let value = value as? [String:Any] {
                                    let personalChallenge = Mapper<ChoresListModel>().map(JSON: value)
                                 self.ChoresListModel = personalChallenge
                                    DispatchQueue.main.async {
                                        self.choresTableView.reloadData()
                                        if self.ChoresListModel?.data?.count ?? 0 > 0{                                           
                                        }else{
                                            if self.wellnessParentVC?.chorePendingAndApprovalContainerView.isHidden == false
                                            {
                                                self.wellnessParentVC?.handleViewControllersUI(nwCHoreVw: false, nwChoreVC: true, chorePendingVC: true, createChoreVC: true, choreProgressVC: true)
                                            }
                                        }
                                    }
                                    self.refresh.endRefreshing()
                                }
                                   break
                               case .failure(let error):
                                    self.refresh.endRefreshing()
                                   break
                               }
            }
        }else{
            self.refresh.endRefreshing()
        }
    }
    
    @objc func presentDonePopup(_ sender: UIButton) {
        let refreshAlert = UIAlertController(title: "", message: "Are you sure about the chore completion?", preferredStyle: UIAlertController.Style.alert)

        let yesAlert = UIAlertAction(title: "Yes", style: .default, handler: { [self] (action: UIAlertAction!) in
            print("Handle Ok logic here")
          okayButton(sender: sender)
      })
        if #available(iOS 13.0, *) {
            yesAlert.setValue(UIColor.harmoneyDarkBlueColor, forKey: "titleTextColor")
        } else {
            // Fallback on earlier versions
        }
        refreshAlert.addAction(yesAlert)
        
        let cancelAlert = UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
      })
        if #available(iOS 13.0, *) {
            cancelAlert.setValue(UIColor.systemGray2, forKey: "titleTextColor")
        } else {
            // Fallback on earlier versions
        }
        refreshAlert.addAction(cancelAlert)

            self.present(refreshAlert, animated: true, completion: nil)
       
    }
    
    @objc func decideBtnAct(_ sender: UIButton) {
        let vc = GlobalStoryBoard().childChoreAcceptPopUp
        vc.modalPresentationStyle = .overFullScreen
        vc.choresListModel = self.ChoresListModel
        vc.selectedTag = sender.tag
        vc.acceptBtnPress = "ChoreLisBtnPress"
//        vc.notificationsViewController = self
//        vc.notificationData = notifications[(notifications.count-1) - indexPath.row]
//        vc.delegate = self
//        UserDefaults.standard.set(true, forKey: notification.request?.taskId ?? "")

//        self.alreadyAccept = true
        self.present(vc, animated: true, completion: nil)
    }
    
    func showchoreDonePopUp(sender: UIButton) {
        selectedTag = sender.tag
        doneChoreViewController = GlobalStoryBoard().doneChoreVC
        doneChoreViewController?.selectedTag = sender.tag
        doneChoreViewController?.choresListModel = ChoresListModel
        doneChoreViewController?.delegate = self
        tabBarController?.present(doneChoreViewController!, animated: true, completion: nil)
    }
    @objc func presentConfirmPopup(_ sender: UIButton) {
        if sender.titleLabel?.text == "What's Next"{
            let challengeVc = GlobalStoryBoard().completedChallengeVC
            let indexData = self.ChoresListModel!.data![sender.tag]
            challengeVc.delegateChore = self
            challengeVc.choreTask = indexData
            challengeVc.isFromChoresVC = true
            self.present(challengeVc, animated: true, completion: nil)
            let userDefaults = UserDefaults.standard
            let myString = userDefaults.string(forKey: "completeChoreChatboat")
            if myString == "completedChore"{
                
            }else{
                
                let userDefaultsCompletChallengeComplete = UserDefaults.standard
                userDefaultsCompletChallengeComplete.set("completedChore", forKey: "completeChoreChatboat")
    //            let indexData = ChoresListModel?.data![selectedTag]
                
                let taskName = indexData.task_name
                let userDefaultstaskName = UserDefaults.standard
                userDefaultstaskName.set(taskName, forKey: "ChoreCompletetaskName")
                let Description = indexData.task_subName
                let userDefaultsDescription = UserDefaults.standard
                userDefaultsDescription.set(Description, forKey: "ChoreCompleteDescription")
                let experianceString = indexData.task_xp_value ?? "200"
                let userDefaultsexperianceString = UserDefaults.standard
                userDefaultsexperianceString.set(experianceString, forKey: "experianceString")
                var harmoneyString = indexData.harmoney_bucks  ?? "10"
                let userDefaultsharmoneyString = UserDefaults.standard
                userDefaultsharmoneyString.set(harmoneyString, forKey: "harmoneyString")
                var ImgUrl = indexData.task_image_url
                let userDefaultsImgUrlString = UserDefaults.standard
                userDefaultsImgUrlString.set(ImgUrl, forKey: "choreImgUrl")
            }
        }else{
            selectedTag = sender.tag
            removeChoreViewController = GlobalStoryBoard().removeChoreVC
            removeChoreViewController?.selectedTag = sender.tag
            removeChoreViewController?.choresListModel = ChoresListModel
            removeChoreViewController?.delegate = self
            tabBarController?.present(removeChoreViewController!, animated: true, completion: nil)
        }
    }
}



extension ChorePendingAndApprovalViewController: RemoveChoreViewControllerDelegate {
    
    func didTapThumbsDownButton(removeChoreViewController: RemoveChoreViewController ) {
        
        removeChoreViewController.dismiss(animated: true, completion: nil)

        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        
        let data = [String : Any]()
            
            let indexData = ChoresListModel?.data![selectedTag]

        var (url, method, param) = APIHandler().deleteChore(params: data)
            url = url + (indexData?.chorse_id ?? "1")
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                               case .success(let value):
                                
                                if let value = value as? [String:Any] {
                                    self.ChoresListModel?.data?.remove(at: self.selectedTag)
                                    self.getAllSubCategory()                                    
                                    if let message = value["message"] as? String{
                                    self.view.toast(message)
                                        
                                  }
                                }
                                   break
                               case .failure(let error):

                                   break
                               }
            }
        }

    }
    
    func didTapThumbsUpButton(removeChoreViewController: RemoveChoreViewController) {
        removeChoreViewController.dismiss(animated: true, completion: nil)

    }
    
    
    }


extension ChorePendingAndApprovalViewController {
    func okayButton(sender:UIButton) {
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        
            let indexData = ChoresListModel?.data![sender.tag]
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        print("clicked on tag \(sender.tag), Chore Name : \(indexData?.task_name)\(indexData?.chorse_id)")
            
        guard let dayObj = indexData?.taskList?.filter({$0.date == formatter.string(from: Date())}) else{
            return
        }

        var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            data.updateValue(indexData?.chorse_id ?? "1", forKey: "chorse_id")
            data.updateValue((dayObj.last?.day) ?? "1", forKey: "taskDay")
            
        let (url, method, param) = APIHandler().dailyCompleteChore(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
              
                    switch response.result {
                               case .success(let value):
                                if let value = value as? [String:Any] {
                                    if let message = value["message"] as? String{
                                  //  self.view.toast(message)
                                      //  updateGoals(goalType: .fitnessChallenge, success: nil)
                                        if HarmonySingleton.shared.isHideForNewFlow{
                                            DispatchQueue.main.async {
                                               
                                                self.getAllSubCategory()
                                            }
                                        }else{
                                            self.showchoreDonePopUp(sender: sender)
                                        }                                        
                                        
                                  }
                                }
                                   break
                               case .failure(let error):

                                   break
                               }
            }
        }


    }
}

extension ChorePendingAndApprovalViewController : CompletedChallengeViewControllerDelegateForChore{
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, chore: ChoresModel) {
        clollectChoreReward(chore: chore)
    }

    
    func clollectChoreReward(chore:ChoresModel) {
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            data.updateValue(chore.chorse_id ?? "1", forKey: "chorse_id")

        let (url, method, param) = APIHandler().completeChoreEarnPoints(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                               case .success(let value):
                                if let value = value as? [String:Any] {
                                    if let message = value["message"] as? String{
                                    self.view.toast(message)
                                       // updateGoals(goalType: .fitnessChallenge, success: nil)
                                        HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
                                        self.getAllSubCategory()
                                  }
                                }
                                   break
                               case .failure(let error):
                                self.view.toast(error.localizedDescription)
                                   break
                               }
            }
        }
    }
}

extension ChorePendingAndApprovalViewController : DoneChoreViewControllerDelegate{
    func okayButton(doneChoreViewController: DoneChoreViewController) {
        DispatchQueue.main.async {
            self.getAllSubCategory()
        }
    }
    
    
}

// MARK: - ThumbsAlertViewControllerDelegate methods

extension ChorePendingAndApprovalViewController: ThumbsAlertViewControllerDelegate {
    func didTapThumbsUpButton(thumbsAlertViewController: ThumbsAlertViewController) {
      
        thumbsAlertViewController.dismiss(animated: true, completion: nil)

    }
    
    func didTapThumbsDownButton(thumbsAlertViewController: ThumbsAlertViewController) {
        thumbsAlertViewController.dismiss(animated: true, completion: nil)
    }
}
