//
//  AcceptChildChorePopUp.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 29/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import ObjectMapper
import IQKeyboardManagerSwift
enum AcceptChoreRows : CaseIterable {
    case header, name, task, when, until, bucks, reward, accept, bank
}
protocol AddBankDetailsDelegate {
    func moveToBankDetails()
}
class AcceptChildChorePopUp: UIViewController {
    @IBOutlet weak var choretableView: UITableView!
    var delegate : AddBankAccountDelegate?
    var notificationData : NotificationDash?
    var getGuidForMobileNumber : guidForMobileNumberObject?
    var transfertoUserObjectValue:  transfertoUserObject?
    var getWalletObj : getwalletObject?
    var addHarmoneyWalletValue : addHarmoneyWalletDataObject?
    var choreViewData : ChoresModel?
    var choresListModel: ChoresListModel?
    var selectedTag: Int?
    var acceptBtnPress: String?
    var requestUserGuid = ""
    var taskId = ""
    var kidname = ""
    var harmoneyBucks = "0"
    var customRewards = ""
    var  customAlertViewController: CustomAlertViewController?
    var notificationsViewController : NotificationsViewController?
    var requestNotificationsVc : RequestNotificationVC?
    var rows : [AcceptChoreRows] = [.header, .name, .task, .when, .until, .bucks, .reward,.accept]
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        let indexData = choresListModel?.data![selectedTag!]
        if acceptBtnPress == "ChoreLisBtnPress"
        {
            taskId = indexData?.chorse_id ?? ""
            requestUserGuid = indexData?.taskTo?.guid ?? ""
            kidname = indexData?.taskTo?.name ?? ""
        }
        // Do any additional setup after loading the view.
        if (HarmonySingleton.shared.dashBoardData.data?.bankAccount?.isEmpty ?? true){
            //suba start
          //  rows = [.header, .name, .task, .when, .until, .bucks, .reward, .bank,.accept]
            //suba end
        }
        getChoreDetails(getChore: true, accept: false, reject: false)
        IQKeyboardManager.shared.enable = false
    }
    func bankAccoun()  {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            self.choretableView.contentInset = .zero
        } else {
            self.choretableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
    }
    
    @IBAction func closeClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getChoreDetails(getChore:Bool,accept:Bool,reject:Bool){
        
//        if accept{
//            guard let bucks = Int(harmoneyBucks), bucks >= 1 else{
//            self.view.toast("Please enter Minimum 1 Harmoney Bucks")
//                return
//            }
//        }
        
        var (url, method, param) = ("", HTTPMethod.get, [String:Any]())
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        data.updateValue("1", forKey: "type") //For Chores the Type is 1
        data.updateValue(notificationData?.request?.requestUserGuid ?? "", forKey: "requestUserGuid")
        data.updateValue(notificationData?.request?.taskId ?? "", forKey: "taskId")
        if acceptBtnPress == "ChoreLisBtnPress"
        {
            data.updateValue(requestUserGuid, forKey: "requestUserGuid")
            data.updateValue(taskId, forKey: "taskId")
            
        }
        if accept{
        data.updateValue(harmoneyBucks, forKey: "tHarmoneyBucks")
        data.updateValue(customRewards, forKey: "tCustomRewards")
            if notificationData?.type == 12 || acceptBtnPress == "ChoreLisBtnPress" {
                data.updateValue(1, forKey: "taskCompletionApproval")
            }
        }
        
        if Reachability.isConnectedToNetwork(){
            SVProgressHUD.show()
            if getChore{
               (url, method, param) = APIHandler().getChoreTaskDetails(params: data)
            }else if accept{
                if notificationData?.type == 12 || acceptBtnPress == "ChoreLisBtnPress" 
                {
                    if (self.harmoneyBucks == "0" || self.harmoneyBucks == "") && HarmonySingleton.shared.isHideForNewFlow == false
                    {
                       

                    }else{
                        if Int(HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0) < Int(harmoneyBucks) ?? 0 && HarmonySingleton.shared.isHideForNewFlow == false   //parent bucks gr
                        
                        {
                          AlertView.shared.showAlert(view: self, title: "Chore", description: "Insufficient balance to transfer amount")
                            SVProgressHUD.dismiss()
                            AlertView.shared.delegate = self
                            return
                        }else{
                            
                          
                            if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
                     
                            }else{
   
                                if HarmonySingleton.shared.getKidWalletList == nil  && HarmonySingleton.shared.isHideForNewFlow == true // bug fix ios-shouldn't  allow parent to approve the task before create wallet for kids
                                    {
                                      self.view.toast("Create Kid Wallet")
                                        SVProgressHUD.dismiss()
                                        AlertView.shared.showAlert(view: self, title: "Wallet Create", description: "Create sub Wallet to transfer amount")
                                        AlertView.shared.delegate = self
                                        return
                                    }
                                
                            }
                            
                            var walletPrivateKeyKid:String = "[]"
                            if let assigntomemberDataList = HarmonySingleton.shared.getKidWalletList{
                                for objMember in assigntomemberDataList{
                                    if objMember.kidGuid == notificationData?.request?.requestUserGuid {
                                        if let fromguid = objMember.walletAddress{
                                            walletPrivateKeyKid = fromguid
                                        }
                                    }
                                }
                            }

                            self.transactionToUser(guid: notificationData?.request?.requestUserGuid ?? "", cryptoAddresskid: walletPrivateKeyKid)
                            
                        }
                    }
                  
                }
               (url, method, param) = APIHandler().acceptChoreInvite(params: data)
            }else if reject{
                (url, method, param) = APIHandler().rejectChoreInvite(params: data)
            }
            print("ChoreAccept Params")
            print((url, method, param))
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        if getChore{
                            let choreData = Mapper<ChoresListModel>().map(JSON: value)
                            self.choreViewData = choreData?.data?[0]
                            self.harmoneyBucks = self.choreViewData?.harmoney_bucks ?? "0"
                            self.customRewards = self.choreViewData?.task_reward ?? ""
                            if self.choreViewData?.task_when == "Just One Time"{
                                self.rows.remove(at: self.rows.firstIndex(of: .until)!)
                            }
                            self.choretableView.reloadData()
                            
                            if accept{
                                if self.acceptBtnPress == "ChoreLisBtnPress"
                                {
                                    PersonalFitnessManager.sharedInstance.transferFitnessAmount(taskId: self.taskId, type: 12) { (result) in
                                        switch result {
                                        case .success(_):
            //                                    self.getAllNotifications()
            //                                    approveChallengeViewController.dismiss(animated: true, completion: nil)
                                            self.presentCustomAlertViewController()
                                            self.notificationsViewController?.refreshNotification()
                                            AlertView.shared.showAlert(view: self, title: "Chore", description: value["message"] as! String)
                                            AlertView.shared.delegate = self
                                           
                                        case .failure(let error):
                                            self.view.toast(error.localizedDescription)
                                        }
                                    }

                                }else{
                                    PersonalFitnessManager.sharedInstance.transferFitnessAmount(taskId: self.notificationData?.request?.taskId ?? "", type: 12) { (result) in
                                        switch result {
                                        case .success(_):
            //                                    self.getAllNotifications()
            //                                    approveChallengeViewController.dismiss(animated: true, completion: nil)
                                            self.presentCustomAlertViewController()
                                            self.notificationsViewController?.refreshNotification()
                                            AlertView.shared.showAlert(view: self, title: "Chore", description: value["message"] as! String)
                                            AlertView.shared.delegate = self
                                           
                                           
                                        case .failure(let error):
                                            self.view.toast(error.localizedDescription)
                                        }
                                    }

                                }
                            }
                            
                        }else{
                            if accept{
//                                if HarmonySingleton.shared.isHideForNewFlow{
//                                    
//                                    self.presentCustomAlertViewController()
//                                    self.notificationsViewController?.refreshNotification()
//                                    AlertView.shared.showAlert(view: self, title: "Chore", description: value["message"] as! String)
//                                    AlertView.shared.delegate = self
//                                    return
//                                }
                                if self.acceptBtnPress == "ChoreLisBtnPress"
                                {
                                    PersonalFitnessManager.sharedInstance.transferFitnessAmount(taskId: self.taskId, type: 12) { (result) in
                                        switch result {
                                        case .success(_):
            //                                    self.getAllNotifications()
            //                                    approveChallengeViewController.dismiss(animated: true, completion: nil)
                                            self.presentCustomAlertViewController()
                                            self.notificationsViewController?.refreshNotification()
                                            AlertView.shared.showAlert(view: self, title: "Chore", description: value["message"] as! String)
                                            AlertView.shared.delegate = self
                                           
                                        case .failure(let error):
                                            self.view.toast(error.localizedDescription)
                                        }
                                    }

                                }else{
                                    PersonalFitnessManager.sharedInstance.transferFitnessAmount(taskId: self.notificationData?.request?.taskId ?? "", type: 12) { (result) in
                                        switch result {
                                        case .success(_):
            //                                    self.getAllNotifications()
            //                                    approveChallengeViewController.dismiss(animated: true, completion: nil)
                                            self.presentCustomAlertViewController()
                                            self.notificationsViewController?.refreshNotification()
                                            AlertView.shared.showAlert(view: self, title: "Chore", description: value["message"] as! String)
                                            AlertView.shared.delegate = self
                                           
                                           
                                        case .failure(let error):
                                            self.view.toast(error.localizedDescription)
                                        }
                                    }

                                }
                            }else{
                                AlertView.shared.showAlert(view: self, title: "Chore", description: value["message"] as! String)
                                AlertView.shared.delegate = self

                            }
                          
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }else{
            self.view.toast("Internet Connection not Available!")
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func handleCellUI(cell:ChorePopupCellOne, indexPath:IndexPath) {
//        if indexPath.row <= 4 {
//            cell.handleUI(hidedscLbl: false, hidebucksVw: true, hiderewardVw: true)
//        }else if(indexPath.row == 5){
//            cell.handleUI(hidedscLbl: true, hidebucksVw: false, hiderewardVw: true)
//        }else if(indexPath.row == 6){
//            cell.handleUI(hidedscLbl: true, hidebucksVw: true, hiderewardVw: false)
//        }else{
//            cell.handleUI(hidedscLbl: true, hidebucksVw: true, hiderewardVw: true)
//        }
        if (rows[indexPath.row] == .header) || (rows[indexPath.row] == .name) || (rows[indexPath.row] == .task) || (rows[indexPath.row] == .when) || (rows[indexPath.row] == .until) {
            cell.handleUI(hidedscLbl: false, hidebucksVw: true, hiderewardVw: true)
        }else if(rows[indexPath.row] == .bucks){
            cell.handleUI(hidedscLbl: true, hidebucksVw: false, hiderewardVw: true)
        }else if(rows[indexPath.row] == .reward){
            cell.handleUI(hidedscLbl: true, hidebucksVw: true, hiderewardVw: false)
        }else{
            cell.handleUI(hidedscLbl: true, hidebucksVw: true, hiderewardVw: true)
        }
        
    }
    
    func updateNotification(taskId:String)  {
     
    }
    
    func transactionToUser(guid:String, cryptoAddresskid:String) {
        if HarmonySingleton.shared.isHideForNewFlow{
            
            self.addHarmoneyWallet(amount: self.harmoneyBucks )
            return
        }
      
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
        data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
        data.updateValue(self.harmoneyBucks , forKey: "amount")
//        data.updateValue(guid , forKey: "transferGuid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "transferGuid")
        let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
        data.updateValue(cryptoAddresskid, forKey: "walletAddress")
        var (url, method, param) = APIHandler().transferAmountToSila(params: data)
        
        AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                if  let value = value as? [String:Any] {
                    self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                    if self.transfertoUserObjectValue?.data?.status == "SUCCESS" {
                        self.view.toast(self.transfertoUserObjectValue?.data?.message ?? "")
                        self.reduceHarmoneyWallet(amount: self.harmoneyBucks )
//                        self.addHarmoneyWallet(amount: self.harmoneyBucks )
                    }else{
                        
                        if Reachability.isConnectedToNetwork() {
                            SVProgressHUD.show()
                            var data = [String : Any]()
                            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                            
                            data.updateValue("535-354-5454", forKey: "moblieNumber")
                            var (url, method, param) = APIHandler().getGuidForMobileNumber(params: data)
                            
                            AF.request(url, method: method, parameters: param,headers: self.headers).validate().responseJSON { response in
                                SVProgressHUD.dismiss()
                                switch response.result {
                                case .success(let value):
                                    if  let value = value as? [String:Any] {
                                        self.getGuidForMobileNumber = Mapper<guidForMobileNumberObject>().map(JSON: value)
                                        let guid = self.getGuidForMobileNumber?.data?.last?.guid
                                        let userName = self.getGuidForMobileNumber?.data?.last?.name
                                        if guid == nil
                                        {
                                            
                                        }else{
                                            
                                            var data = [String : Any]()
                                            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                                            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                                            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
                                            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
                                            data.updateValue(self.harmoneyBucks , forKey: "amount")
                                            data.updateValue(guid ?? "", forKey: "transferGuid")
                                            let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
                                            data.updateValue(cryptoAddress ?? "", forKey: "walletAddress")
                                            var (url, method, param) = APIHandler().transferAmountToSila(params: data)
                                            
                                            AF.request(url, method: method, parameters: param,headers: self.headers).validate().responseJSON { response in
                                                SVProgressHUD.dismiss()
                                                switch response.result {
                                                case .success(let value):
                                                    
                                                    if  let value = value as? [String:Any] {
                                                        self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                                                        self.view.toast(self.transfertoUserObjectValue?.data?.message ?? "")
                                                        self.reduceHarmoneyWallet(amount: self.harmoneyBucks )
                                                        self.addHarmoneyWallet(amount: self.harmoneyBucks )
                                                    }
                                                case .failure(let error):
                                                    print(error)
                                                }
                                            }
                                        }
                                        
                                    }
                                case .failure(let error):
                                    print(error)
                                }
                            }
                        }
                    }
                    
                    //                                                        self.transferAmountTxt.text = ""
                    //                                                        self.transferMobileNumberTxt.text = ""
                    //                                                        DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
                    //                                                            self.getWallet()
                    //                                                        }
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    func addHarmoneyWallet(amount: String){
        print("addWalletCalled")
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(notificationData?.request?.requestUserGuid ?? "", forKey: "guid")
            
        data.updateValue(amount, forKey: "amount")
      
        var (url, method, param) = APIHandler().addHarmoneyWallet(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
//                            self.view.toast(self.addHarmoneyWalletValue?.message ?? "Wallet amount added to harmoney account")
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    
    func reduceHarmoneyWallet(amount: String){
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        data.updateValue(amount, forKey: "amount")
      
        var (url, method, param) = APIHandler().reduceHarmoneyWallet(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
//                            self.view.toast(self.addHarmoneyWalletValue?.message ?? "Wallet amount added to harmoney account")
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    func handleUIforHeaderCell(cell:ChoresHeaderCell)  {
        cell.choreImg.imageFromURL(urlString: choreViewData?.task_image_url ?? "")
        cell.choreTitle.text = choreViewData?.task_name
    }
    func handleDataforChorePopupCellOne(cell:ChorePopupCellOne, indexPath:IndexPath)  {
        if notificationData?.type == 12 || acceptBtnPress == "ChoreLisBtnPress" {
           cell.handleUI(hidedscLbl: false, hidebucksVw: true, hiderewardVw: true)
        }
        var key = ""
        switch rows[indexPath.row] {
        case .name:
            key = "Name"
            cell.descpLable.text = notificationData?.request?.kidName ?? ""
            if acceptBtnPress == "ChoreLisBtnPress"
            {
                cell.descpLable.text = kidname
            }
        case .task:
            key = "Task"
            cell.descpLable.text = choreViewData?.task_subName
        case .when:
            key = "When"
            cell.descpLable.text = choreViewData?.task_when
        case .until:
            key = "Until"
            cell.descpLable.text = choreViewData?.task_until
        case .bucks:
            key = "Harmoney Bucks"
            cell.bucksTF.text = choreViewData?.harmoney_bucks
            cell.descpLable.text = choreViewData?.harmoney_bucks
            cell.handleUI(hidedscLbl: false, hidebucksVw: false, hiderewardVw: true)
        case .reward:
            key = "Reward"
            cell.rewardsTF.text = choreViewData?.task_reward
            cell.descpLable.text = choreViewData?.task_reward
        default:
            print("")
            
        }
        cell.nameLabel.text = key
    }
    @objc func collectBucks(tf:UITextField) {
        harmoneyBucks = tf.text ?? "0"
    }
    @objc func collectRewards(tf:UITextField) {
        customRewards = tf.text ?? ""
    }
}


extension AcceptChildChorePopUp : UITableViewDelegate,UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if rows[indexPath.row] == .header{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChoresHeaderCell", for: indexPath) as! ChoresHeaderCell
            handleUIforHeaderCell(cell: cell)
            return cell
        }
        if rows[indexPath.row] == .accept{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChorePopUpAcceptCell", for: indexPath) as! ChorePopUpAcceptCell
            cell.parentVC = self
            return cell
        }
        if rows[indexPath.row] == .bank{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddAccountCell", for: indexPath) as! AddAccountCell
            cell.parentVC = self
            cell.delegate = self
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChorePopupCellOne", for: indexPath) as! ChorePopupCellOne
            self.handleCellUI(cell: cell, indexPath: indexPath)
            handleDataforChorePopupCellOne(cell: cell, indexPath: indexPath)
            cell.bucksTF.addTarget(self, action: #selector(collectBucks(tf:)), for: .editingChanged)
            cell.rewardsTF.addTarget(self, action: #selector(collectRewards(tf:)), for: .editingChanged)
            return cell
        }
                        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if rows[indexPath.row] == .header{
            return 140
        }
        if rows[indexPath.row] == .accept{
            return 60
        }
        if rows[indexPath.row] == .bank{
            return 90
        }
        return 50
    }
}


extension AcceptChildChorePopUp : AlertCallBack{
    func clickedOnOk() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AcceptChildChorePopUp : AddBankAccountDelegate{
    func addBankAccountCallBack() {
        self.dismiss(animated: false, completion: nil)
        delegate?.addBankAccountCallBack()
    }
}
extension AcceptChildChorePopUp {
    func presentCustomAlertViewController() {
        customAlertViewController = GlobalStoryBoard().customAlertVC
        
        customAlertViewController?.alertTitle = "Amount Approved"
            customAlertViewController?.alertMessage = "Yay! You have transfer Amount to kid."
        customAlertViewController?.delegate = self
        tabBarController?.present(customAlertViewController!, animated: true, completion: nil)
    }
}

extension AcceptChildChorePopUp: CustomAlertViewControllerDelegate {
    func didTapOkayButton(customAlertViewController: CustomAlertViewController) {
        customAlertViewController.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
        
    }
}
