//
//  AddChorePopVC.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 20/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
protocol AddChorePopupReceived {
    func newChoreAdded()
}
class AddChorePopVC: UIViewController {
    var delegate : AddChorePopupReceived?
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var choreImg: UIImageView!
    
    @IBOutlet weak var imgVw: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        choreImg.imageFromURL(urlString: CreateChoreVM.shared.defaultChoreObj?.choserImage ?? "")
              titleLbl.adjustsFontSizeToFitWidth = true
              if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child {
                  titleLbl.text = "Yay! Chore has been added!"
              }else{
                  titleLbl.text = "Awesome! \n  You have added a chore successfully!"
              }
    }
    

    @IBAction func gotItClick(_ sender: Any) {
        delegate?.newChoreAdded()
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
