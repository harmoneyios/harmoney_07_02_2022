//
//  ChoreCreateVC.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ObjectMapper
import IQKeyboardManagerSwift

class CreateChoreVM {
   static let shared = CreateChoreVM()
    var parentRef : ChoreCreateVC?
    var choreHeading = ""
    var choreImage = UIImage.init()
    var choreDay = "Select"
    var choreDate = ""
    var choreXpvalue = "200"
    var choreGemValue = "1"
    var choreBucksvalue = "1"
    var choreRewardMsg = ""
    var choreCustomText = "Custom"
    
    
    var daySelected = false
    var defaultChoreObj : chorselist?{
        didSet{
            self.setDefaults()
        }
    }
    func setDefaults()  {
        self.choreHeading = ""
        self.choreImage = UIImage.init()
        self.daySelected = false
        self.choreDay = "Select"
        self.choreCustomText = "Custom"
        self.choreDate = ""
        self.choreXpvalue = "0"
        self.choreGemValue = "0"
        self.choreBucksvalue = ""
        self.choreRewardMsg = ""
        parentRef!.choreUIhandler.defaultSections()
        self.parentRef?.choreUITable.reloadData()
    }
}

class ChoreCreateVC: UIViewController {
    var wellnessParentVC : WellnessViewController?
    var vmObj = CreateChoreVM.shared
    var choreUIhandler = ChoreDataCells.shared
    var addChorePopUp = GlobalStoryBoard().choreAddPopVC
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var shadowLayer: UIView!
    @IBOutlet weak var choreUITable: UITableView!
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        vmObj.parentRef = self
        choreUITable.reloadData()
        addChorePopUp.delegate = self
        // Do any additional setup after loading the view.
        self.shadowLayer.clipsToBounds = false;
        self.shadowLayer.layer.masksToBounds = false;
        shadowLayer.layer.shadowColor =  #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.3659284607)
        shadowLayer.layer.shadowOffset = CGSize.init(width: 1, height: 1)
        shadowLayer.layer.shadowRadius = 5
        shadowLayer.layer.shadowOpacity = 1
        IQKeyboardManager.shared.enable = false
//        [shadowLayer.layer setShadowColor:[[UIColor whiteColor] CGColor]];
//        [shadowLayer.layer setShadowOffset:CGSizeMake(0, 0)];
//        [shadowLayer.layer setShadowRadius:5.0];
//        [shadowLayer.layer setShadowOpacity:1];
        

    }
    
    private func getAssignedMemberdata() {
        PersonalFitnessManager.sharedInstance.assingToList { (result) in
            switch result {
            case .success(_):
//                self.addChallengeCustomView.personalChallenge = self.personalChallenge
                print(HarmonySingleton.shared.assigntomemberDataList as Any)
                self.choreUITable.reloadData()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func calculateTableViewHeight() {
        var height : CGFloat = 0
//        choreUIhandler.rowsHeight(cellType: choreUIhandler.rows[indexPath.row])
        for item in choreUIhandler.rows {
            height += choreUIhandler.rowsHeight(cellType: item)
        }
        tableViewHeight.constant = height + 20        
        self.view.layoutIfNeeded()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        choreUITable.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
        getDashBoardData()
        self.getAssignedMemberdata()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData), name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
    }
    
    @objc func onDidReceiveData(){
        getDashBoardData()
    }
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollVw.contentInset = .zero
        } else {
            scrollVw.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
    }
    
    
    func getDashBoardData() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")
        if Reachability.isConnectedToNetwork(){
            SVProgressHUD.show()
            var (url, method, param) = APIHandler().dashboard(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: value)
                        
                        self.choreUIhandler.defaultSections()
                        self.vmObj.setDefaults()
                        self.calculateTableViewHeight()
                        self.choreUITable.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }else{
            self.view.toast("Internet Connection not Available!")
        }
        
    }
    
    @IBAction func closeClick(_ sender: Any) {
        
        wellnessParentVC?.choreListContainerView.isHidden = false
        wellnessParentVC?.newChoreview.isHidden = false
        wellnessParentVC?.createChoreView.isHidden = true
        choreUITable.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ChoreCreateVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return choreUIhandler.rows.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = choreUIhandler.cellForRow(cellType:choreUIhandler.rows[indexPath.row] , table: tableView, indexPath: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return choreUIhandler.rowsHeight(cellType: choreUIhandler.rows[indexPath.row])
    }
}


extension ChoreCreateVC: AddChorePopupReceived{
    func newChoreAdded() {
                        
        wellnessParentVC?.chorePendingAndApprovalVC!.getAllSubCategory()
        CreateChoreVM.shared.setDefaults()
        wellnessParentVC?.createChoreView.isHidden = true
        wellnessParentVC?.choreProgressContainerView.isHidden = true
        wellnessParentVC?.choreListContainerView.isHidden = true
        wellnessParentVC?.newChoreview.isHidden = true
        wellnessParentVC?.chorePendingAndApprovalContainerView.isHidden = false
    }
    
    
}
