//
//  EarnTabViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 14/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

private enum ButtonTags : Int{
    case daily = 100
    case personal = 101
//    case community = 102
    case wellness = 102
}
 

class EarnTabViewController: UIViewController {
    
    @IBOutlet weak var wellnessView: UIView!
    @IBOutlet weak var personalView: UIView!
    @IBOutlet weak var dailyView: UIView!
    @IBOutlet weak var dailyBtn: UIButton!
    @IBOutlet weak var personalBtn: UIButton!
//    @IBOutlet weak var communityBtn: UIButton!
    @IBOutlet weak var wellnesBtn: UIButton!
    @IBOutlet weak var navBar: UIView!
    
    var challengeType = Int()
    var pageViecontroller  : EarnPageViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpUI()
       
    }
    
    func setupNavBar(){
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = navBar.bounds
        navBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
    }
    
    func setUpUI(){
        HarmonySingleton.previousVC = .earn
        dailyBtn.titleLabel?.font = ConstantString.descriptionFont
        personalBtn.titleLabel?.font = ConstantString.descriptionFont
//        communityBtn.titleLabel?.font = ConstantString.descriptionFont
        wellnesBtn.titleLabel?.font = ConstantString.descriptionFont
        
        dailyBtn.tag = ButtonTags.daily.rawValue
        dailyView.tag = ButtonTags.daily.rawValue
        personalBtn.tag = ButtonTags.personal.rawValue
        personalView.tag = ButtonTags.personal.rawValue
//        communityBtn.tag = ButtonTags.community.rawValue
        wellnesBtn.tag = ButtonTags.wellness.rawValue
        wellnessView.tag = ButtonTags.wellness.rawValue
        handleButtonsBackGroundColor()
        dailyBtn .setTitleColor(ConstantString.buttonBackgroundColor, for: .normal)
        
    }
    
    func handleButtonsBackGroundColor() {
        dailyBtn .setTitleColor(ConstantString.buttonSelectedBackgroundColor, for: .normal)
        personalBtn .setTitleColor(ConstantString.buttonSelectedBackgroundColor, for: .normal)
//        communityBtn .setTitleColor(ConstantString.buttonSelectedBackgroundColor, for: .normal)
        wellnesBtn .setTitleColor(ConstantString.buttonSelectedBackgroundColor, for: .normal)
    }
    
   
    func handleButtonClicks(sender:UIButton)  {
        pageViecontroller?.setViewControllertoPage(index: sender.tag - 100)
        handleButtonsBackGroundColor()
        sender.setTitleColor(ConstantString.buttonBackgroundColor, for: .normal)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
//        if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
//            self.tabBarController?.title = "challenge"
//        }
        
        setupNavBar()
        
        if isFromAddChallenge{
            
            if challengeHbotType == 1{
                isFromAddChallenge = false
                handleButtonClicks(sender: personalBtn)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.pageViecontroller?.fitnessVC.presentChallengeActionsViewController()
                }
            }else{
                handleButtonClicks(sender: wellnesBtn)
            }
        }
        if isFromMyChallange {
            
            print(myChallengeType)
            
            if myChallengeType == 2 {
//                wellnesBtn.isSelected = true
//                personalBtn.isSelected = false
                handleButtonClicks(sender: wellnesBtn)
            } else {
//                personalBtn.isSelected = true
//                wellnesBtn.isSelected = false
                handleButtonClicks(sender: personalBtn)
            }
            pageViecontroller?.setViewControllertoPage(index: myChallengeType)
            isFromMyChallange = false
        }

    }
    
    @IBAction func dailyClick(_ sender: UIButton) {        
        handleButtonClicks(sender: sender)
    }
    @IBAction func personalClick(_ sender: UIButton) {
        handleButtonClicks(sender: sender)
    }
    @IBAction func communityClick(_ sender: UIButton) {
        handleButtonClicks(sender: sender)
    }
    @IBAction func wellnessClick(_ sender: UIButton) {
//        if HarmonySingleton.shared.dashBoardData.data?.challenges?.count ?? 0 >= 3 {
//           if HarmonySingleton.shared.dashBoardData.data?.challenges?[2].cStatus == 1 {
//                handleButtonClicks(sender: sender)
//            }
//        }else{
//            self.view.toast("Please Complete All Challenges")
//        }
        handleButtonClicks(sender: sender)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "earnPageSegue"{
            pageViecontroller = segue.destination as? EarnPageViewController
        }
    }
    

}
