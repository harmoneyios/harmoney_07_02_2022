//
//  ChallengeDetailsViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/13/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import UICircularProgressRing

protocol ChallengeDetailsViewControllerDelegate: class {
    func didTapCloseButton(challengeDetailsViewController: ChallengeDetailsViewController)
}

class ChallengeDetailsViewController: UIViewController {
    @IBOutlet weak var circleProgressView: UICircularProgressRing!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var taskCounterLabel: UILabel!
    @IBOutlet weak var experianceLabel: UILabel!
    @IBOutlet weak var challengeImageView: UIImageView!
    @IBOutlet weak var taskCounterAttributedLabel: UILabel!
    @IBOutlet weak var customRewardLabel: UILabel!
    @IBOutlet weak var customRewardStackView: UIStackView!
    @IBOutlet weak var harmoneyBucksLabel: UILabel!
    
    weak var delegate: ChallengeDetailsViewControllerDelegate?
    var personalChallenge: PersonalChallenge? {
        didSet {
           configureUI()
        }
    }
    var onboardingChallenge: ChallengeData?

//MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureUI()
    }
}

//MARK: UI methods
extension ChallengeDetailsViewController {
    func configureUI() {
        circleProgressView.style = .ontop
        circleProgressView.maxValue = CGFloat(personalChallenge?.data.challengeTask ?? 0)
        circleProgressView.value = CGFloat(personalChallenge?.data.challengeProgress ?? 0)
        titleLabel.text = personalChallenge?.data.challengeType.rawValue
        taskCounterLabel.text = String(format: "%d", personalChallenge?.data.challengeTask ?? 0)
        harmoneyBucksLabel.text = String(format: "%d", personalChallenge?.data.harmoneyBucks ?? 0)
        
        if personalChallenge?.data.customReward?.count ?? 0 > 0  {
            customRewardLabel.text = personalChallenge?.data.customReward
        } else {
            customRewardLabel.text = ""
        }
        
        setupExperianceAttributedLabel()
        
        switch personalChallenge?.data.challengeType {
        case .walk:
            challengeImageView.image = UIImage(named: "walk_icon")
            taskNameLabel.text = "No. of Steps*"
            setupTaskCounterAttributedLabel(challengeTypeString: " Steps")
            circleProgressView.outerRingColor = UIColor.harmoneyLightGreenColor
            circleProgressView.innerRingColor = UIColor.harmoneyLightGreenColor
            break
        case .swim:
            challengeImageView.image = UIImage(named: "swim_icon")
            setupTaskCounterAttributedLabel(challengeTypeString: " Laps")
            taskNameLabel.text = "No. of Laps*"
            circleProgressView.outerRingColor = UIColor.harmoneySwimBlueColor
            circleProgressView.innerRingColor = UIColor.harmoneySwimBlueColor
            break
        case .run:
            challengeImageView.image = UIImage(named: "run_icon")
            taskNameLabel.text = "No. of Miles*"
            setupTaskCounterAttributedLabel(challengeTypeString: " Miles")
            circleProgressView.outerRingColor = UIColor.harmoneyRunGrayColor
            circleProgressView.innerRingColor = UIColor.harmoneyRunGrayColor
            break
        case .bike:
            challengeImageView.image = UIImage(named: "bike_icon")
            taskNameLabel.text = "No. of Miles*"
            setupTaskCounterAttributedLabel(challengeTypeString: " Miles")
            circleProgressView.outerRingColor = UIColor.harmoneyBikeBrownColor
            circleProgressView.innerRingColor = UIColor.harmoneyBikeBrownColor
            break
        case .none:
            break
        }
    }
    
    func setupExperianceAttributedLabel() {
        let experianceString = "\(self.personalChallenge?.data.experiance ?? 0) "
        let diamondString = String(format: "%d", self.personalChallenge?.data.diamond ?? 0)
        
        let xpAttachment = NSTextAttachment()
        let diamondAttachment = NSTextAttachment()
        xpAttachment.image = UIImage(named: "xp")
        
        let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
        
        if creditType == 1 {
            diamondAttachment.image = UIImage(named: "soccer")

        } else if creditType == 2 {
            diamondAttachment.image = UIImage(named: "diamond")

        } else {
            diamondAttachment.image = UIImage(named: "touchdown")
        }
        
        let boldFont = UIFont.futuraPTBoldFont(size: 16)
        
        let imageSize = xpAttachment.image!.size
        let image2Size = diamondAttachment.image!.size
        xpAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - imageSize.height) / 2, width: imageSize.width, height: imageSize.height)
        diamondAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - image2Size.height) / 2, width: image2Size.width, height: image2Size.height)
        
        let xpImageString = NSAttributedString(attachment: xpAttachment)
        let diamondImageString = NSAttributedString(attachment: diamondAttachment)
        
        let attributedText = NSMutableAttributedString(string: "to get ")
        attributedText.append(NSAttributedString(string: experianceString + " "))
        attributedText.append(xpImageString)
        attributedText.append(NSAttributedString(string: "  "))
        attributedText.append(diamondImageString)
        attributedText.append(NSAttributedString(string:"  " + diamondString + " "))
        attributedText.append(NSAttributedString(string: " and start leveling up"))
        
        let lightRange = (attributedText.string as NSString).range(of: attributedText.string)
        let boldRangeExperiance = (attributedText.string as NSString).range(of: experianceString)
        let boldRangeDiamond = (attributedText.string as NSString).range(of: diamondString)
        
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBookFont(size: 16)], range:lightRange)
        attributedText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.harmoneyLightOrangeColor], range:boldRangeExperiance)
        attributedText.addAttributes([NSAttributedString.Key.font: boldFont], range:boldRangeExperiance)
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBoldFont(size: 16)], range:boldRangeDiamond)
        attributedText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.harmoneyLightOrangeColor], range:boldRangeDiamond)
        
        experianceLabel.attributedText = attributedText
    }
    
    func setupTaskCounterAttributedLabel(challengeTypeString: String) {
        let formatter = Utilities.addNumberFormatter()
        
        let challengeTaskString = formatter.string(from: (personalChallenge?.data.challengeTask ?? 0) as NSNumber)
        let text = "Complete " + (challengeTaskString ?? "") + challengeTypeString
        let attributedText = NSMutableAttributedString(string: text)
        
        let boldRange = (attributedText.string as NSString).range(of: (challengeTaskString ?? ""))
        let lightRange = (attributedText.string as NSString).range(of: text)

        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBookFont(size: 16)], range:lightRange)
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBoldFont(size: 16)], range:boldRange)
        
        taskCounterAttributedLabel.attributedText = attributedText
    }
}

//MARK: Action methods
extension ChallengeDetailsViewController {
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        delegate?.didTapCloseButton(challengeDetailsViewController: self)
    }
}
