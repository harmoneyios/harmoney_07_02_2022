//
//  TCellRequestNotification.swift
//  Harmoney
//
//  Created by Saravanan on 08/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Toast_Swift
import SVProgressHUD
import ObjectMapper
import IQKeyboardManagerSwift

class TCellRequestNotification: UITableViewCell {
    
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    static let identifier = "TCellRequestNotification"
    static func nib()->UINib{
       return UINib(nibName: TCellRequestNotification.identifier, bundle: nil)
    }
    
    var notification: NotificationDash? {
        didSet {
            configureNotification()
        }
    }
    weak var delegate: ApproveChallengeViewControllerDelegate?
    var personalChallenge: PersonalChallenge?
    var invitedRequest: InvitedRequest?
    let animationTimeInterval: Double = 0.2
    var startPosition: CGFloat = 0
    var kidName: String?
    var approveChallengeViewController: ApproveChallengeViewController?
    
    
    //MARK: - Action methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func configureNotification() {
        
        if let url = URL(string: notification?.profilePictureURL ?? "") {
            imgVwProfile.imageFromURL(urlString: url.absoluteString)
        } else {
            switch notification?.type {
            case 1:
                imgVwProfile.image = UIImage(named: "footsteps")
            case 2:
                imgVwProfile.image = UIImage(named: "selectedBought")
            case 3:
                imgVwProfile.image = UIImage(named: "shareOutfilled2")
            case 4:
                imgVwProfile.image = UIImage(named: "harmoneycircle")
            case 5:
                imgVwProfile.image = UIImage(named: "harmoneycircle")
            case 6:
                let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
                
                if creditType == 1 {
                    imgVwProfile.image = UIImage(named: "soccer")

                } else if creditType == 2 {
                    imgVwProfile.image = UIImage(named: "diamond")

                } else {
                    imgVwProfile.image = UIImage(named: "touchdown")
                }
//                imgVwProfile.image = UIImage(named: "diamond")
            default:
                imgVwProfile.image = UIImage(named: "harmoneycircle")
                break
            }
            
            imgVwProfile.image = UIImage(named: "footsteps")
        }
        
    //   indicatorImageView.isHidden = true

        if notification?.type == 10 || notification?.type == 11 || notification?.type == 12 {
            if notification?.request?.status == 1 {
               // indicatorImageView.isHidden = true
            } else {
              //  indicatorImageView.isHidden = true//false
            }
        }
        var message = notification?.message
        if let emojiUnicode = notification?.emoji?.uniCode {
            message = "\(message ?? "") \(emojiUnicode.decodeEmoji())"
        }
        
        lblContent.text = message
        let str = notification?.createdDateTime?.UTCToLocal(incomingFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outGoingFormat: "MMM d, yyyy h:mm a")
       
        
        
        lblContent.font = UIFont.futuraPTMediumFont(size: 17)
        lblContent.textColor = UIColor.black
        
        if notification?.isRead == 1{
            lblContent.font = UIFont.futuraPTLightFont(size: 17)
            lblContent.textColor = UIColor.darkGray
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
   
 
}

