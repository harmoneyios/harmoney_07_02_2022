//
//  RequestNotificationVC.swift
//  Harmoney
//
//  Created by Saravanan on 08/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import SVProgressHUD
import ObjectMapper
import IQKeyboardManagerSwift

enum NotificationpopupScreens:Int {
    case inviteParent = 10
    case acceptChore = 11
}


protocol RequestNotificationViewControllerDelegate: class {
    func didTapAcceptChallengeButton(approveChallengeViewController: ApproveChallengeViewController)
    func didTapDeclineChallengeButton(approveChallengeViewController: ApproveChallengeViewController)
    func didTapCloseButton(approveChallengeViewController: ApproveChallengeViewController)
    func didTapAcceptLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController)
    func didTapDeclineLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController)
}

class RequestNotificationVC: UIViewController {
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnMuteNotification: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    var childInvitePopUp : ChildInviteParentRequestPopUp?
    var alreadyAccept : Bool = false
    var getGuidForMobileNumber : guidForMobileNumberObject?
    var transfertoUserObjectValue:  transfertoUserObject?
    var getWalletObj : getwalletObject?
    var addHarmoneyWalletValue : addHarmoneyWalletDataObject?
    let refresh = UIRefreshControl()
    var personalChallenge: PersonalChallenge?
    var invitedRequest: InvitedRequest?

    weak var delegate: RequestNotificationViewControllerDelegate?
    
    var notifications: [NotificationDash] {
        return NotificationsManager.sharedInstance.notifications
    }
    var  thumbsAlertViewController: ThumbsAlertViewController?
    var approveChallengeViewController: ApproveChallengeViewController?
    var customAlertViewController: CustomAlertViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        getAllNotifications()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func onMuteNotification(_ sender: Any) {
        btnMuteNotification.isSelected = !btnMuteNotification.isSelected
        UserDefaultConstants.shared.isMuteRequestNotifications = btnMuteNotification.isSelected
    }
    
}

extension RequestNotificationVC{
    func acceptInvite(thumbsAlertViewController: ThumbsAlertViewController? = nil,index : Int){
        
            let notification = notifications[(notifications.count-1) - index]
            
            guard let request  = notification.request else {
                return
            }
            
            PersonalFitnessManager.sharedInstance.acceptInvite(invitedRequest: request) { (result) in
                switch result {
                case .success(_):
                    thumbsAlertViewController?.dismiss(animated: true, completion: nil)
                    self.getAllNotifications()
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
            }
        
    }
    
    func rejectInvite(thumbsAlertViewController: ThumbsAlertViewController? = nil,index : Int){
        let notification = notifications[(notifications.count-1) - index]
        
        guard let request  = notification.request else {
            return
        }
        
        PersonalFitnessManager.sharedInstance.rejectInvite(invitedRequest: request) { (result) in
            switch result {
            case .success(_):
                thumbsAlertViewController?.dismiss(animated: true, completion: nil)
                self.getAllNotifications()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
}
extension RequestNotificationVC: ThumbsAlertViewControllerDelegate {
    func didTapThumbsUpButton(thumbsAlertViewController: ThumbsAlertViewController) {
        if let indexPath = tblVw.indexPathForSelectedRow {
            self.acceptInvite(thumbsAlertViewController: thumbsAlertViewController, index: indexPath.row)
        }
    }
    
    func didTapThumbsDownButton(thumbsAlertViewController: ThumbsAlertViewController) {
        if let indexPath = tblVw.indexPathForSelectedRow {
            self.rejectInvite(index: indexPath.row)
        }
    }
}
extension RequestNotificationVC: AddBankAccountDelegate {
    func addBankAccountCallBack() {
        self.dismiss(animated: false, completion: nil)
        HarmonySingleton.shared.mainTabBar?.selectedIndex = 3
    }
}

extension RequestNotificationVC{
    func initalSetup(){
        btnMuteNotification.isSelected = UserDefaultConstants.shared.isMuteRequestNotifications ?? false
        registerCells()
        lblNotificationCount.text = "Notifications(\(notifications.count))"
    }
    func presentThumbsAlertViewController(alertTitle: String) {
        thumbsAlertViewController = GlobalStoryBoard().thumbsAlertVC
        thumbsAlertViewController?.alertTitle = alertTitle
        thumbsAlertViewController?.delegate = self
        present(thumbsAlertViewController!, animated: true, completion: nil)
    }
    
    func presentApproveChallengeViewController(personalChallenge: PersonalChallenge, kidName: String) {
        approveChallengeViewController = GlobalStoryBoard().approveChallengeVC
        approveChallengeViewController?.personalChallenge = personalChallenge
        approveChallengeViewController?.kidName = kidName
        approveChallengeViewController?.delegate = self
        
        present(approveChallengeViewController!, animated: true, completion: nil)
    }
    
    func presentApproveLevelUpCustomRewardViewController(invitedRequest: InvitedRequest) {
        approveChallengeViewController = GlobalStoryBoard().approveChallengeVC
        approveChallengeViewController?.invitedRequest = invitedRequest
        approveChallengeViewController?.delegate = self
        
        present(approveChallengeViewController!, animated: true, completion: nil)
    }
    
    func presentCustomAlertViewController(alertMessage: String, alertTitle: String) {
        customAlertViewController = GlobalStoryBoard().customAlertVC
        customAlertViewController?.alertTitle = alertTitle
        customAlertViewController?.alertMessage = alertMessage
        customAlertViewController?.delegate = self
        present(customAlertViewController!, animated: true, completion: nil)
    }
    
    func registerCells(){
        tblVw.register(TCellRequestNotification.nib(), forCellReuseIdentifier: TCellRequestNotification.identifier)
        tblVw.tableFooterView = UIView()
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.refreshControl = refresh
        refresh.addTarget(self, action: #selector(getNotificationList), for: .valueChanged)
    }
    @objc func getNotificationList()  {
        getAllNotifications()
    }
    private func setupNotificationsUI() {
        if notifications.count > 0 {
            tblVw.isHidden = false
            tblVw.reloadData()
        } else {
            tblVw.isHidden = true
            //emptyNotificationsView.isHidden = false
        }
    }
    
    private func getAllNotifications() {
        NotificationsManager.sharedInstance.getAllNotifications { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    self.refresh.endRefreshing()
                    self.setupNotificationsUI()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        // self.readAllNotifications()
                    }
                case .failure(let error):
                    self.refresh.endRefreshing()
                    self.view.toast(error.localizedDescription)
                }
            }
        }
    }

}

//MARK: - Table View
extension RequestNotificationVC : UITableViewDelegate{
    
}

extension RequestNotificationVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TCellRequestNotification.identifier) as? TCellRequestNotification
        let notification = notifications[(notifications.count-1) - indexPath.row]
        cell?.notification = notification
        cell?.selectionStyle = .none
        cell?.btnAccept.tag = indexPath.row
        cell?.btnReject.tag = indexPath.row
        cell?.btnAccept.addTarget(self, action: #selector(onClickAcceptbtn(sender:)), for: .touchUpInside)
        cell?.lblTitle.text = notification.request?.kidName
        cell?.btnReject.addTarget(self, action: #selector(onClickRejectbtn(sender:)), for: .touchUpInside)
        return cell!
    }
    @objc func onClickAcceptbtn(sender:UIButton){
        
        let notification = notifications[(notifications.count-1) - sender.tag]
        if notification.request?.status ?? 0 == 0 {
            if notification.status == 1{
                
            }else{
                if notification.type == 10 {
                    self.acceptInvite(index: sender.tag)
                } else if notification.type == 11 || notification.type == 12 || notification.type == 15 {
                    if notification.request?.taskFor == 2 || notification.request?.taskFor == 15 {
                        PersonalFitnessManager.sharedInstance.getPersonalChallenge(taskId: notification.request?.taskId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "") { (result) in
                            switch result {
                            case .success(let personalChallenge):
                                let indexPath = self.tblVw.indexPathForSelectedRow!
                                let currentCell = self.tblVw.cellForRow(at: indexPath)! as! TCellRequestNotification
                                currentCell.btnAccept.setTitle("Approve", for: .normal)
                                self.acceptForApproveChallenage(hBucks: "\(personalChallenge.data.harmoneyBucks ?? 0)", reward: personalChallenge.data.customReward ?? "", personalChallenge: personalChallenge, isFromAnotherView: false, index: sender.tag)
                                
                            case .failure(let error):
                                self.view.toast(error.localizedDescription)
                            }
                        }
                    } else {
                        
                        let vc = GlobalStoryBoard().childChoreAcceptPopUp
                        vc.modalPresentationStyle = .overFullScreen
                        vc.requestNotificationsVc = self
                        vc.notificationData = notifications[(notifications.count-1) - sender.tag]
                        vc.delegate = self
                        UserDefaults.standard.set(true, forKey: notification.request?.taskId ?? "")
                        
                        self.alreadyAccept = true
                        self.present(vc, animated: true, completion: nil)
                        
                    }
                    
                } else if notification.type == 13 {
                    self.acceptLevelUP(inviteRequest: notification.request!)
                }
            }
            
        } else {
            if notification.type == 10 {
                view.toast("Already accepted/rejected this invite request.")
            } else if notification.type == 11 {
                view.toast("Already accepted/rejected this challenge.")
            }
        }
    }
    
    @objc func onClickRejectbtn(sender:UIButton){
        
        let notification = notifications[(notifications.count-1) - sender.tag]
        if notification.request?.status ?? 0 == 0 {
            if notification.status == 1{
                
            }else{
                if notification.type == 10 {
                    self.rejectInvite(index: sender.tag)
                }else if notification.type == 11 || notification.type == 12 || notification.type == 15 {
                    if notification.request?.taskFor == 2 || notification.request?.taskFor == 15 {
                        PersonalFitnessManager.sharedInstance.getPersonalChallenge(taskId: notification.request?.taskId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "") { (result) in
                            switch result {
                            case .success(let personalChallenge):
                                self.rejectLevelUP(inviteRequest: notification.request!, approveChallengeViewController: self.approveChallengeViewController)
                              
                            case .failure(let error):
                                self.view.toast(error.localizedDescription)
                            }
                        }
                    } else {
                        self.rejectLevelUP(inviteRequest: (approveChallengeViewController?.invitedRequest!)!, approveChallengeViewController: approveChallengeViewController)
//
//                        let vc = GlobalStoryBoard().childChoreAcceptPopUp
//                        vc.modalPresentationStyle = .overFullScreen
//                        vc.requestNotificationsVc = self
//                        vc.notificationData = notifications[(notifications.count-1) - sender.tag]
//                        vc.delegate = self
//                        UserDefaults.standard.set(true, forKey: notification.request?.taskId ?? "")
//                        self.alreadyAccept = true
//                        self.present(vc, animated: true, completion: nil)
                        
                    }
                    
                } else if notification.type == 13 {
                    self.rejectLevelUP(inviteRequest: notification.request!)
                    
                 
                }
            }
            
        } else {
            if notification.type == 10 {
                view.toast("Already accepted/rejected this invite request.")
            } else if notification.type == 11 {
                view.toast("Already accepted/rejected this challenge.")
            }
        }
            }

    
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
        let notification = notifications[(notifications.count-1) - indexPath.row]
        if notification.request?.status ?? 0 == 0 {
            if notification.status == 1{
                
            }else{
                if notification.type == 10 {
                    presentThumbsAlertViewController(alertTitle: notification.message ?? "")
                } else if notification.type == 11 || notification.type == 12 || notification.type == 15 {
                    if notification.request?.taskFor == 2 || notification.request?.taskFor == 15 {
                        PersonalFitnessManager.sharedInstance.getPersonalChallenge(taskId: notification.request?.taskId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "") { (result) in
                            switch result {
                            case .success(let personalChallenge):
                                self.presentApproveChallengeViewController(personalChallenge: personalChallenge, kidName: notification.request?.kidName ?? "")
                            case .failure(let error):
                                self.view.toast(error.localizedDescription)
                            }
                        }
                    } else {
                        
                        //                    if UserDefaults.standard.bool(forKey: notification.request?.taskId ?? "") == false{
                        
                        //                    if alreadyAccept == false{
                        let vc = GlobalStoryBoard().childChoreAcceptPopUp
                        vc.modalPresentationStyle = .overFullScreen
                        vc.requestNotificationsVc = self
                        vc.notificationData = notifications[(notifications.count-1) - indexPath.row]
                        vc.delegate = self
                        UserDefaults.standard.set(true, forKey: notification.request?.taskId ?? "")
                        
                        self.alreadyAccept = true
                        self.present(vc, animated: true, completion: nil)
                        //                    }else{
                        //                        view.toast("Already accepted/rejected this chore request.")
                        //
                        //                    }
                        
                    }
                    
                } else if notification.type == 13 {
                    presentApproveLevelUpCustomRewardViewController(invitedRequest: notification.request!)
                }
            }
            
        } else {
            if notification.type == 10 {
                view.toast("Already accepted/rejected this invite request.")
            } else if notification.type == 11 {
                view.toast("Already accepted/rejected this challenge.")
            }
        }
    }
    
}

extension RequestNotificationVC{
//    func rejectForApproveChallenge(hBucks: String,reward : String,personalChallenge: PersonalChallenge?, isFromAnotherView: Bool = true, index : Int){
//        let notification = notifications[(notifications.count-1) - sender.tag]
//        if notification.request?.status ?? 0 == 0 {
//            if notification.status == 1{
//                
//            }else{
//                if notification.type == 10 {
//                    self.rejectInvite(index: sender.tag)
//                }else if notification.type == 11 || notification.type == 12 || notification.type == 15 {
//                    if notification.request?.taskFor == 2 || notification.request?.taskFor == 15 {
//                        PersonalFitnessManager.sharedInstance.getPersonalChallenge(taskId: notification.request?.taskId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "") { (result) in
//                            switch result {
//                            case .success(let personalChallenge):
//                                self.rejectLevelUP(inviteRequest: notification.request!, approveChallengeViewController: self.approveChallengeViewController)
//                              
//                            case .failure(let error):
//                                self.view.toast(error.localizedDescription)
//                            }
//                        }
//       
//    }

    func acceptForApproveChallenage(hBucks: String,reward : String,personalChallenge: PersonalChallenge?, isFromAnotherView: Bool = true, index : Int){
       
        let notification = notifications[(notifications.count-1) - index]
        let harmoneyBucks = Int(hBucks)
        let customReward = reward
        
        
        var walletPrivateKeyKid:String = "[]"
        if let assigntomemberDataList = HarmonySingleton.shared.getKidWalletList{
            for objMember in assigntomemberDataList{
                if objMember.kidGuid == notification.request?.requestUserGuid {
                    if let fromguid = objMember.walletAddress{
                        walletPrivateKeyKid = fromguid
                    }
                }
            }
        }
        
        if notification.type == 15{
            if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
            }else{
                if hBucks != "0" || hBucks != ""
                {
                    if HarmonySingleton.shared.getKidWalletList == nil && HarmonySingleton.shared.isHideForNewFlow == false
                    {
                        SVProgressHUD.dismiss()
                        AlertView.shared.showAlert(view: self, title: "Wallet Create", description: "Create sub Wallet to transfer amount")
                        AlertView.shared.delegate = self
                        return
                    }
                }
            }
            PersonalFitnessManager.sharedInstance.acceptPersonalChallenge(taskId: personalChallenge?.data.challengeId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "", harmoneyBucks: harmoneyBucks ?? 0, customReward: customReward ) { (result) in
                switch result {
                case .success(_):
                   
                    self.transactionToUser(transferGuid: notification.request?.requestUserGuid ?? "", harmoneyBucks: hBucks,cryptoAddresskid: walletPrivateKeyKid)
                       
                        
                    PersonalFitnessManager.sharedInstance.transferFitnessAmount(taskId: personalChallenge?.data.challengeId ?? "", type: 15) { (result) in
                            switch result {
                            case .success(_):
                                self.getAllNotifications()
                                
                                if isFromAnotherView{
                                    self.approveChallengeViewController?.dismiss(animated: true, completion: nil)
                                }
                                
                                self.presentCustomAlertViewController(alertMessage: String(format: "Yay! You have transfer Amount to kid.", notification.request?.kidName ?? ""), alertTitle: "Amount Approved")
                            case .failure(let error):
                                self.view.toast(error.localizedDescription)
                            }
                        }
                    
                    
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
            }

          
        }else{
            PersonalFitnessManager.sharedInstance.acceptPersonalChallenge(taskId: personalChallenge?.data.challengeId ?? "" , requestUserGuid: notification.request?.requestUserGuid ?? "", harmoneyBucks: harmoneyBucks ?? 0, customReward: customReward ) { (result) in
                switch result {
                case .success(_):
                    self.getAllNotifications()
                    if isFromAnotherView{
                        self.approveChallengeViewController?.dismiss(animated: true, completion: nil)
                    }
                    self.presentCustomAlertViewController(alertMessage: String(format: "Yay! You have approved kid's task.", notification.request?.kidName ?? ""), alertTitle: "Challenge Approved")
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
            }

        }
    }
}


extension RequestNotificationVC{
    func acceptLevelUP(inviteRequest : InvitedRequest, approveChallengeViewController : ApproveChallengeViewController? = nil){
        LevelsManager.sharedInstance.acceptLevelReward(invitedRequest: inviteRequest) { (result) in
            switch result {
            case .success(_):
                approveChallengeViewController?.dismiss(animated: true, completion: nil)
                self.view.toast("Level reward accepted.")
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func rejectLevelUP(inviteRequest : InvitedRequest, approveChallengeViewController : ApproveChallengeViewController? = nil){
        
        LevelsManager.sharedInstance.rejectLevelReward(invitedRequest: inviteRequest) { (result) in
            switch result {
            case .success(_):
                approveChallengeViewController?.dismiss(animated: true, completion: nil)
                self.view.toast("Level reward rejected.")
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
}
extension RequestNotificationVC: ApproveChallengeViewControllerDelegate {
            func didTapAcceptChallengeButton(approveChallengeViewController: ApproveChallengeViewController) {

                guard let indexPath = tblVw.indexPathForSelectedRow else { return }
                
                self.acceptForApproveChallenage(hBucks: approveChallengeViewController.harmoneyTextfield.text ?? "", reward: approveChallengeViewController.customRewardTextfield.text ?? "", personalChallenge: approveChallengeViewController.personalChallenge, isFromAnotherView: true, index: indexPath.row)
                
            }
         

    
    func addHarmoneyAmount(transferGuid:String, harmoneyBucks:String){
        self.reduceHarmoneyWallet(amount: harmoneyBucks )
        self.addHarmoneyWallet(amount: harmoneyBucks, requestUserGuid: transferGuid )
    }
    func transactionToUser(transferGuid:String, harmoneyBucks:String, cryptoAddresskid:String) {
        
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
        data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
        data.updateValue(harmoneyBucks, forKey: "amount")
        data.updateValue(transferGuid , forKey: "transferGuid")
        let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
        data.updateValue(cryptoAddresskid, forKey: "walletAddress")
        let (url, method, param) = APIHandler().transferAmountToSila(params: data)
        print(url, method, param)
        AF.request(url, method: method, parameters: param).validate().responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                if  let value = value as? [String:Any] {
                    self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                    if self.transfertoUserObjectValue?.data?.status == "SUCCESS" {
                        self.view.toast(self.transfertoUserObjectValue?.data?.message ?? "")
                        self.reduceHarmoneyWallet(amount: harmoneyBucks )
                        //                        self.addHarmoneyWallet(amount: harmoneyBucks, requestUserGuid: transferGuid )
                    }else{
                        
                        if Reachability.isConnectedToNetwork() {
                            SVProgressHUD.show()
                            var data = [String : Any]()
                            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                            
                            data.updateValue("535-354-5454", forKey: "moblieNumber")
                            var (url, method, param) = APIHandler().getGuidForMobileNumber(params: data)
                            
                            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                                SVProgressHUD.dismiss()
                                switch response.result {
                                case .success(let value):
                                    if  let value = value as? [String:Any] {
                                        self.getGuidForMobileNumber = Mapper<guidForMobileNumberObject>().map(JSON: value)
                                        let guid = self.getGuidForMobileNumber?.data?.last?.guid
                                        let userName = self.getGuidForMobileNumber?.data?.last?.name
                                        if guid == nil
                                        {
                                            
                                        }else{
                                            
                                            var data = [String : Any]()
                                            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                                            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                                            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
                                            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
                                            data.updateValue(harmoneyBucks , forKey: "amount")
                                            data.updateValue(guid ?? "", forKey: "transferGuid")
                                            let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
                                            data.updateValue(cryptoAddress ?? "", forKey: "walletAddress")
                                            var (url, method, param) = APIHandler().transferAmountToSila(params: data)
                                            
                                            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                                                SVProgressHUD.dismiss()
                                                switch response.result {
                                                case .success(let value):
                                                    
                                                    if  let value = value as? [String:Any] {
                                                        self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                                                        self.view.toast(self.transfertoUserObjectValue?.data?.message ?? "")
                                                        self.reduceHarmoneyWallet(amount: harmoneyBucks )
                                                        self.addHarmoneyWallet(amount: harmoneyBucks, requestUserGuid: transferGuid )
                                                    }
                                                case .failure(let error):
                                                    print(error)
                                                }
                                            }
                                        }
                                        
                                    }
                                case .failure(let error):
                                    print(error)
                                }
                            }
                        }
                    }
                    
                    //                                                        self.transferAmountTxt.text = ""
                    //                                                        self.transferMobileNumberTxt.text = ""
                    //                                                        DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
                    //                                                            self.getWallet()
                    //                                                        }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
    func addHarmoneyWallet(amount: String, requestUserGuid: String){
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(requestUserGuid, forKey: "guid")
            
            data.updateValue(amount, forKey: "amount")
            
            var (url, method, param) = APIHandler().addHarmoneyWallet(params: data)
            print(url,method,param)
            
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
                        //                            self.view.toast(self.addHarmoneyWalletValue?.message ?? "Wallet amount added to harmoney account")
                        
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
    
    func reduceHarmoneyWallet(amount: String){
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            data.updateValue(amount, forKey: "amount")
            
            var (url, method, param) = APIHandler().reduceHarmoneyWallet(params: data)
            print(url, method, param)
            
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
                        //                            self.view.toast(self.addHarmoneyWalletValue?.message ?? "Wallet amount added to harmoney account")
                        
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        
    }
    func didTapDeclineChallengeButton(approveChallengeViewController: ApproveChallengeViewController) {
        guard let indexPath = tblVw.indexPathForSelectedRow else { return }
        
        approveChallengeViewController.personalChallenge?.data.challengeAcceptType = .decline
        
        let notification = notifications[(notifications.count-1) - indexPath.row]
        
        PersonalFitnessManager.sharedInstance.rejectPersonalChallenge(taskId: approveChallengeViewController.personalChallenge?.data.challengeId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "") { (result) in
            switch result {
            case .success(_):
                self.getAllNotifications()
                approveChallengeViewController.dismiss(animated: true, completion: nil)
                self.presentCustomAlertViewController(alertMessage: String(format: "You have rejected the task", notification.request?.kidName ?? ""), alertTitle: "Challenge Rejected")
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func didTapCloseButton(approveChallengeViewController: ApproveChallengeViewController) {
        approveChallengeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapAcceptLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController) {
        self.acceptLevelUP(inviteRequest: approveChallengeViewController.invitedRequest!, approveChallengeViewController: approveChallengeViewController)
    }
    
    func didTapDeclineLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController) {
        self.rejectLevelUP(inviteRequest: approveChallengeViewController.invitedRequest!, approveChallengeViewController: approveChallengeViewController)
        
    }
}
extension RequestNotificationVC : AlertCallBack{
    func clickedOnOk() {
        self.dismiss(animated: true, completion: nil)
    }
}
extension RequestNotificationVC: CustomAlertViewControllerDelegate {
    func didTapOkayButton(customAlertViewController: CustomAlertViewController) {
        customAlertViewController.dismiss(animated: true, completion: nil)
        
        self.getAllNotifications()
    }
}
