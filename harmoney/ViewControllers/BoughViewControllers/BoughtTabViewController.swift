//
//  BoughtTabViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 23/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class BoughtTabViewController: UIViewController {
    @IBOutlet weak var navBar: UIView!
    @IBOutlet weak var indicatorView1: UIView!
    @IBOutlet weak var indicatorView2: UIView!
    @IBOutlet weak var indicatorView3: UIView!
    
    @IBOutlet weak var vwVoucher: UIView!
    @IBOutlet weak var btnvoucher: UIButton!
    var shoppedPageViewController : ShoppedPageViewController?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        HarmonySingleton.previousVC = .myStuf
        vwVoucher.isHidden = true
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getDashBoardData()
        configureUI()
        if isSecondOnboardingChallange {
            setupIndicatorView(buttonTag: 2)
            shoppedPageViewController?.setViewControllerToPage(index: 0)
            isSecondOnboardingChallange = false
        }
    }
    func getDashBoardData() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")
        if Reachability.isConnectedToNetwork(){
            var (url, method, param) = APIHandler().dashboard(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: value)
                        
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }else{
            self.view.toast("Internet Connection not Available!")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShoppedPageViewControllerSegue" {
            shoppedPageViewController = segue.destination as? ShoppedPageViewController
        }
    }
}

//MARK: UI Methods
extension BoughtTabViewController {
    func configureUI() {
        setupNavBar()
    }
    
    func setupNavBar() {
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = navBar.bounds
        navBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
    }
    
    func handleButtonClicks(sender:UIButton) {
        shoppedPageViewController?.setViewControllerToPage(index: sender.tag)
    }
    
    func setupIndicatorView(buttonTag: Int) {
        switch buttonTag {
        case 0:
            indicatorView1.isHidden = false
            indicatorView2.isHidden = true
            indicatorView3.isHidden = true
        case 1:
            indicatorView1.isHidden = true
            indicatorView2.isHidden = false
            indicatorView3.isHidden = true
        case 2:
            indicatorView1.isHidden = true
            indicatorView2.isHidden = true
            indicatorView3.isHidden = false
        default:
            return
        }
    }
}

//MARK: - Action Methods
extension BoughtTabViewController {
    @IBAction func offersButtonTapped(_ sender: UIButton) {
        setupIndicatorView(buttonTag: sender.tag)
        shoppedPageViewController?.setViewControllerToPage(index: sender.tag)
    }
    
    @IBAction func rewardsButtonTapped(_ sender: UIButton) {
        setupIndicatorView(buttonTag: sender.tag)
        shoppedPageViewController?.setViewControllerToPage(index: sender.tag)
    }
    
    @IBAction func vouchersButtonTapped(_ sender: UIButton) {
        setupIndicatorView(buttonTag: sender.tag)
        shoppedPageViewController?.setViewControllerToPage(index: sender.tag)
    }
}


