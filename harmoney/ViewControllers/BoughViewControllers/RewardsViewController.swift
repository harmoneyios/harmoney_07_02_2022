//
//  RewardsViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/8/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class RewardsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var rewardsDetailsViewController: RewardsDetailsVC?

    var acceptChallengeViewController: AcceptChallengeViewController?
    let cellReuseIdentifier = "customRewardCell"
    var indexPath: IndexPath?
    var customRewardList: [RewardsData] {
        return PersonalFitnessManager.sharedInstance.rewardsNewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureUI()
        getAllCustomReward()
    }
}

//MARK: UI Methods
extension RewardsViewController {
    func configureUI() {
        
        setupTableView()
    }
    
    func getAllCustomReward() {
        PersonalFitnessManager.sharedInstance.getAllCustomReward { (result) in
            switch result {
            case .success(_):
                self.tableView.reloadData()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func setupTableView() {
        tableView.register(UINib.init(nibName: "CustomRewardTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func removeTableViewCell(indexPathToRemove: IndexPath) {
        UIView.animate(withDuration: 1) {
            self.tableView.performBatchUpdates({
                self.tableView.deleteRows(at: [indexPathToRemove], with: .fade)
            })
        }
    }
    
    func presentAcceptChallengeViewController(challengeType: ChallengeType) {
        acceptChallengeViewController = GlobalStoryBoard().acceptChallengeVC
        acceptChallengeViewController?.challengeType = challengeType
        acceptChallengeViewController?.delegate = self
        acceptChallengeViewController?.alertTitle = "Yay! You have successfully claimed a reward."
        tabBarController?.present(acceptChallengeViewController!, animated: true, completion: nil)
    }
    func backFunctionTapped() {
        self.tabBarController?.selectedIndex = 2
    }
}

//MARK: - UITableViewDelegate Methods
extension RewardsViewController: UITableViewDelegate, CustomRewardTableViewCellDelegate {
    func didTapClaimButton(customRewardTableViewCell: CustomRewardTableViewCell) {
        
        //Mani Sabarish
//        PersonalFitnessManager.sharedInstance.claimPersonalChallengeReward(personalChallenge: customRewardTableViewCell.customReward!) { (result) in
//            switch result {
//                case .success( _):
//                   // self.presentAcceptChallengeViewController(challengeType: customRewardTableViewCell.customReward?.taskType ?? 0)
//                   // self.dismiss(animated: true, completion: nil)
//                    if let indexPath = self.tableView.indexPath(for: customRewardTableViewCell)  {
//                        self.removeTableViewCell(indexPathToRemove: indexPath)
//                        self.view.toast("Reward Successfully claimed")
//                    //self.getAllCustomReward()
//                    }
//                case .failure(let error):
//                    self.view.toast(error.localizedDescription)
//            }
//        }
//
        
    }
}

//MARK: - UITableViewDataSource Methods
extension RewardsViewController: UITableViewDataSource ,RewardOfferDetailsDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if customRewardList.count == 0 {
               self.tableView.setEmptyMessage("No Rewards Available")
           } else {
               self.tableView.restore()
           }
        
        return customRewardList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CustomRewardTableViewCell
        
        let customReward = customRewardList[indexPath.row]
        cell.customReward = customReward
        cell.delegate = self
        cell.smallClaimButton.isUserInteractionEnabled = false
        cell.tabButton.isUserInteractionEnabled = false

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let customReward = customRewardList[indexPath.row]
        self.presentOfferDetailViewController(rewardsData: customReward)
    }

    func presentOfferDetailViewController(rewardsData: RewardsData) {
        rewardsDetailsViewController = GlobalStoryBoard().rewardsDetailsVC
        rewardsDetailsViewController?.rewardsData = rewardsData
        rewardsDetailsViewController?.delegate = self
        tabBarController?.present(rewardsDetailsViewController!, animated: true, completion: nil)
    }
}

//MARK: AcceptChallengeViewControllerDelegate Methods
extension RewardsViewController: AcceptChallengeViewControllerDelegate {
    func gotItButtonTapped(acceptChallengeViewController: AcceptChallengeViewController) {
        acceptChallengeViewController.dismiss(animated: true)
    }
}
