//
//  OffersViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/8/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit


class OffersViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var claimedCoupons: ClaimedCoupons?
    let cellReuseIdentifier = "customRewardCell"
    var indexPath: IndexPath?
    var offerDetailsViewController: OfferDetailsViewController?
    var tappedCustomRewardTableViewCell: CustomRewardTableViewCell?
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getClaimedCoupons()
    }
}

//MARK: UI Methods
extension OffersViewController {
    func configureUI() {
        setupTableView()
        getClaimedCoupons()
    }
    
    func setupTableView() {
        tableView.register(UINib.init(nibName: "CustomRewardTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func removeTableViewCell(indexPathToRemove: IndexPath) {
        UIView.animate(withDuration: 1) {
            self.tableView.performBatchUpdates({
                self.tableView.deleteRows(at: [indexPathToRemove], with: .fade)
            })
        }
    }
    
    func getClaimedCoupons() {
        CouponManager.sharedInstance.getPurchasedCoupons { (result) in
            switch result {
            case .success(let claimedCoupons):
                self.claimedCoupons = claimedCoupons
                
                self.tableView.reloadData()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func presentOfferDetailViewController(claimedCoupon: Datum) {
        offerDetailsViewController = GlobalStoryBoard().OfferDetailsVC
        offerDetailsViewController?.claimedCoupon = claimedCoupon
        offerDetailsViewController?.delegate = self
        tabBarController?.present(offerDetailsViewController!, animated: true, completion: nil)
    }
}

//MARK: UI Action
extension OffersViewController {
    
}

//MARK: - UITableViewDelegate Methods
extension OffersViewController: UITableViewDelegate, CustomRewardTableViewCellDelegate, OfferDetailsDelegate {
    
    func deleteCoupounTapped() {
        CouponManager.sharedInstance.claimCoupon(claimedCoupon: tappedCustomRewardTableViewCell!.claimedCoupon!) { (result) in
            switch result {
            case .success(_):
                self.claimedCoupons = CouponManager.sharedInstance.claimedCoupons
                                
                
                if let indexPath = self.tableView.indexPath(for: self.tappedCustomRewardTableViewCell!) {
                    self.removeTableViewCell(indexPathToRemove: indexPath)
                }
                if HarmonySingleton.shared.isSecondChallange {
                    self.tabBarController?.selectedIndex = 1
                    NotificationCenter.default.post(name: Notification.Name("OnboardingOfferClaimed"), object: nil)
                }
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    func backFunctionTapped() {
        self.tabBarController?.selectedIndex = 1
    }
    func didTapClaimButton(customRewardTableViewCell: CustomRewardTableViewCell) {
        tappedCustomRewardTableViewCell = customRewardTableViewCell
        self.presentOfferDetailViewController(claimedCoupon: customRewardTableViewCell.claimedCoupon!)
    }
}

//MARK: - UITableViewDataSource Methods
extension OffersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return claimedCoupons?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CustomRewardTableViewCell
//        cell.smallClaimButton.isHidden = true
        cell.smallClaimButton.setTitle("View", for: .normal)
        cell.smallClaimButton.isEnabled = false
        let claimedCoupon = claimedCoupons?.data[indexPath.row]
        cell.claimedCoupon = claimedCoupon
        cell.delegate = self
        
        return cell
    }
}

