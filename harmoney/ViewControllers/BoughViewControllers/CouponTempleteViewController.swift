//
//  CouponTempleteViewController.swift
//  Harmoney
//
//  Created by INQ Projects on 19/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import WebKit
protocol CouponTemplateViewDelegates {
    func couponClaimedClickedFromOnboarding()
}
class CouponTempleteViewController: UIViewController {

    @IBOutlet weak var navigationBar: CustomNavigationBar!
    
    @IBOutlet weak var couponWebView: WKWebView!
    
    @IBOutlet weak var currentGemsLbl: UILabel!
    
    @IBOutlet weak var diamondImageview: UIImageView!
    
    @IBOutlet weak var currentGemCountTitle: UILabel!
    
    var couponData : CouponInfo?
    
    var websiteUrl : String?
    var couponID : String?
    var currentDateTime : String?
    
    var claimsDataList : CouponValidationModel?
    var delegate : CouponTemplateViewDelegates?

    var validationListCount : Int?
    var claimListCount : Int?

    var currentGem = 0

    var isUpdateClaim : Bool = false
    var isFromSecondChallenge : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
        self.getCouponValidationListFirst()
        self.getCouponClaimsListFirst()
        HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideLoader()
        couponWebView.cleanAllCookies()
        couponWebView.refreshCookies()
    }
    func setupView()  {
        
        self.isUpdateClaim = false
        let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
        
        if creditType == 1 {
            self.diamondImageview.image = UIImage(named: "soccer_shine")
            currentGemCountTitle.text = "Current goal(s) count"

        } else if creditType == 2 {
            diamondImageview.image = UIImage(named: "diamond_shine")
            currentGemCountTitle.text = "Current gem(s) count"

        } else {
            self.diamondImageview.image = UIImage(named: "touchdown_shine")
            currentGemCountTitle.text = "Current touchdown(s) count"

        }
//        diamondImageview.image = UIImage(named: "diamond_shine_icon")

        self.currentGem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
        currentGemsLbl.text = "\(self.currentGem)"
        
        couponWebView.navigationDelegate = self
        // Do any additional setup after loading the view.
        let link = URL(string:websiteUrl ?? "" )!
        let request = URLRequest(url: link)
        couponWebView.load(request)
        
        navigationBar.delegate = self
        navigationBar.title = "Coupon Details"
        
        let today = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm"
        
        formatter.calendar = Calendar.current
        formatter.timeZone = TimeZone.current
        formatter.timeZone = TimeZone(abbreviation: "UTC")

        currentDateTime = formatter .string(from: today as Date)
        
        let today1 = NSDate()
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        formatter1.calendar = Calendar.current
        formatter1.timeZone = TimeZone.current
        formatter1.timeZone = TimeZone(abbreviation: "UTC")

        let testtime = formatter1 .string(from: today1 as Date)
        
        print("claim-->",(testtime) )
    }
    
    func getCouponClaimsList()  {
        
        CouponManager.sharedInstance.getAllCouponsClaimsApi(claimedCouponID: couponData!.id,currentTime:currentDateTime ?? "") { (result) in
            switch result {case .success(let coupons):
                print(coupons)
                let arrayCount = Int(coupons.amountOfResults)
                if self.claimListCount! < arrayCount! {
                    self.hideLoader()
                    if self.isUpdateClaim == false {
                        let claimedCoupon = coupons.data.last
                        self.updateClaimDetails(claimDatas:claimedCoupon!)
                        self.isUpdateClaim = true
                    }
                    
                }
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    func getCouponValidationList()  {
        CouponManager.sharedInstance.getAllCouponsValidationApi(claimedCoupon: couponData!,currentTime:currentDateTime ?? "") { (result) in
            switch result {case .success(let coupons):
                let arrayCount = Int(coupons.amountOfResults)
                if self.validationListCount! < arrayCount! {
                    self.hideLoader()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                        self.dismiss(animated: true, completion:{
                        })
                    }
                }
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }

    }
    
    func getCouponValidationListFirst()  {
        CouponManager.sharedInstance.getAllCouponsValidationApi(claimedCoupon: couponData!,currentTime:currentDateTime ?? "") { (result) in
            switch result {case .success(let coupons):
                let arrayCount = coupons.amountOfResults
                self.validationListCount = Int(arrayCount)
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    func getCouponClaimsListFirst()  {
        CouponManager.sharedInstance.getAllCouponsClaimsApi(claimedCouponID: couponData!.id,currentTime:currentDateTime ?? "") { (result) in
            switch result {case .success(let coupons):
                let arrayCount = coupons.amountOfResults
                self.claimListCount = Int(arrayCount)
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func updateClaimDetails(claimDatas:validationDatum)  {
        
        let collection : [String : Any] = [
            "compainID":couponData!.id,
            "claimed":"1",
            "validated":"0",
            "url":couponData!.url,
            "name":couponData!.name,
            "title":couponData!.title,
            "subtitle":couponData!.subtitle,
            "description":couponData!.couponInfoDescription,
            "createdate":couponData!.createdate,
            "startdate":couponData!.startdate,
            "expirydate":couponData!.expirydate,
            "terms":couponData!.terms,
            "terms_short":couponData!.termsShort,
            "value":couponData!.value,
            "coupon_image":couponData!.couponImage,
            "amount_limitation":couponData!.amountLimitation,
            "amount_limitation_type":couponData!.amountLimitationType,
            "amount_used":couponData!.amountUsed ?? "",
            "amount_available":couponData!.amountAvailable ?? "",
            "code":couponData!.code,
            "session":claimDatas.session

    ]
        let Param : [String:Any] = [
            "guid":  UserDefaultConstants().guid ?? "",
            "logo_url":couponData!.logoURL,
            "name":couponData!.name,
            "coupon_price":couponData!.poweredby,
            "status":"1",
            "onboardingCoupon": (isFromSecondChallenge == true) ? 1 : 0,
            "collection": collection
            ]
        
        CouponManager.sharedInstance.updateCouponData(claimedCoupon: Param) { (result) in
            switch result {case .success(let coupons):
               print(coupons)
                let gemCount = Int(self.couponData!.poweredby) ?? 0
                
                let gem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
                let gemAfterClaimCoupon = gem - gemCount 
                HarmonySingleton.shared.dashBoardData.data?.points?.jem = gemAfterClaimCoupon
                self.currentGem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
                self.currentGemsLbl.text = "\(self.currentGem)"
                self.view.toast(coupons.message)
//                isSecondOnboardingChallange = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                    self.dismiss(animated: true, completion:{
                    })
                }
                if (self.isFromSecondChallenge == true){
                    isSecondOnboardingChallange = true
                    NotificationCenter.default.post(name: Notification.Name("OnboardingCouponClaimed"), object: nil)
                }

            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }

}
extension CouponTempleteViewController : WKNavigationDelegate,WKUIDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.getCouponValidationList()
        self.getCouponClaimsList()
        hideLoader()
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showLoader()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        hideLoader()
    }
}
// MARK: - CustomNavigationBarDelegate methods

extension CouponTempleteViewController: CustomNavigationBarDelegate {
    func customNavigationBarDidTapLeftButton(_ navigationBar: CustomNavigationBar) {
        dismiss(animated: true, completion: nil)
    }
    
    func customNavigationBarDidTapRightButton(_ navigationBar: CustomNavigationBar) {
        
    }
}
