//
//  OfferDetailsViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/29/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol OfferDetailsDelegate {
    func deleteCoupounTapped()
    func backFunctionTapped()
}

class OfferDetailsViewController: UIViewController {
    @IBOutlet weak var navigationBarView: CustomNavigationBar!
    @IBOutlet weak var barcodeImageView: UIImageView!
    @IBOutlet weak var discountValueLabel: UILabel!
    @IBOutlet weak var couponCodeLabel1: UILabel!
    @IBOutlet weak var couponCodeLabel2: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var validDateLabel: UILabel!

    @IBOutlet weak var deleteOfferBtn: UIButton!
    var claimedCoupon: Datum?
    var delegate : OfferDetailsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    @IBAction func deleteOfferClck(_ sender: Any) {
        delegate?.deleteCoupounTapped()
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UI methods

extension OfferDetailsViewController {
    
    func configureUI() {
        navigationBarView.delegate = self
        navigationBarView.title = "Offer Details"
        deleteOfferBtn.layer.cornerRadius = 10
        barcodeImageView.image = generateBarcode(from: claimedCoupon?.couponCode ?? "")
        discountValueLabel.text = String(format: "%@ one purchase", claimedCoupon?.strProductOffValueText ?? "")
        couponCodeLabel1.text = claimedCoupon?.couponCode ?? ""
        descriptionLabel.text = String(format: "Excludes gift card purchases and brand collabs %@ up to maximum discount of $10 ", claimedCoupon?.strProductTitle ?? "")
        couponCodeLabel2.text = claimedCoupon?.couponCode ?? ""
        validDateLabel.text = String(format: "Valid until %@", claimedCoupon?.valide ?? "")
    }
    
    func generateBarcode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    
    
}

// MARK: - CustomNavigationBarDelegate methods

extension OfferDetailsViewController: CustomNavigationBarDelegate {
    func customNavigationBarDidTapLeftButton(_ navigationBar: CustomNavigationBar) {
        if HarmonySingleton.shared.isSecondChallange {
            delegate?.backFunctionTapped()            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func customNavigationBarDidTapRightButton(_ navigationBar: CustomNavigationBar) {
        
    }
}
