//
//  ShoppedPageViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/8/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ShoppedPageViewController: UIPageViewController {
    var vouchersVC = GlobalStoryBoard().vouchersVC
    var rewardsVC = GlobalStoryBoard().rewardsVC
    var offersVC = GlobalStoryBoard().offersVC
    var claimedCouponVC = GlobalStoryBoard().claimCouponList

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self

        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                direction: .forward,
                animated: true,
                completion: nil)
        }
        
        for subview in self.view.subviews {
            if let scrollView = subview as? UIScrollView {
                scrollView.bounces = false
                break
            }
        }
    }
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [claimedCouponVC,
                rewardsVC,
                vouchersVC
                
                ]
    }()
    
    func setViewControllerToPage(index: Int) {
        
        setViewControllers([orderedViewControllers[index]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
}

extension ShoppedPageViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return nil
    }  
}
