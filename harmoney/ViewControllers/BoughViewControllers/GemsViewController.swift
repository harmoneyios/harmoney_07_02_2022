//
//  GemsViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/13/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
protocol GemsViewDelegates {
    func gemsClickedFromOnboarding()
}
class GemsViewController: UIViewController {
    @IBOutlet weak var currentGemsLbl: UILabel!
    @IBOutlet weak var navigationBarView: CustomNavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var diamondImageview: UIImageView!
    
    var delegate : GemsViewDelegates?
    var gemProduct: GemProduct?
    let reuseIdentifier = "cell"
    var currentGem = 0
    var isfromSecondChallange : Bool?
// MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        getGemProducts()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if CouponManager.sharedInstance.couponPurchased {
            HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewAfterCouponClaimed(lastDiamondValue: Float(self.currentGem))
            CouponManager.sharedInstance.couponPurchased = false
        }
    }
}

// MARK: - UI methods

extension GemsViewController {
    func configureUI() {
        let nib = UINib(nibName: "GemProductCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        
        let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
        
        if creditType == 1 {
            self.diamondImageview.image = UIImage(named: "soccer_shine")

        } else if creditType == 2 {
            diamondImageview.image = UIImage(named: "diamond_shine")

        } else {
            self.diamondImageview.image = UIImage(named: "touchdown_shine")

        }
        navigationBarView.delegate = self
        navigationBarView.title = "Gems"
        
        self.currentGem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
        currentGemsLbl.text = "\(self.currentGem)"
    }
    
    func removeCollectionViewCell(indexPathToRemove: IndexPath) {
        UIView.animate(withDuration: 1) {
            self.collectionView.performBatchUpdates({
                self.gemProduct?.data.coupons.nonPromoList.remove(at: indexPathToRemove.row)
                
                self.collectionView.deleteItems(at: [indexPathToRemove])
            })
        }
    }
    
    func getGemProducts() {
        CouponManager.sharedInstance.getAllCoupons { (result) in
            switch result {case .success(let gemProduct):
                self.gemProduct = gemProduct
                self.collectionView.reloadData()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
}

//MARK: UICollectionViewDelegate Methods

extension GemsViewController: UICollectionViewDelegate {
    
}

//MARK: UICollectionViewDataSource Methods

extension GemsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gemProduct?.data.coupons.nonPromoList.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! GemProductCollectionViewCell
        let coupon = self.gemProduct?.data.coupons.nonPromoList[indexPath.row]
        
        cell.coupon = coupon
        
        cell.delegate = self
        
        return cell
    }
}

//MARK: UICollectionViewDataSource Methods

extension GemsViewController: GemProductCollectionViewCellDelegate {
    func didTapBuyButton(coupon: NonPromoList, gemProductCollectionViewCell: GemProductCollectionViewCell) {
        if HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0 >= coupon.intProductClaimValue {
            CouponManager.sharedInstance.purchaseCoupon(nonPromoList: coupon) { (result) in
                            switch result {case .success(_):
            //                    if let indexPath = self.collectionView.indexPath(for: gemProductCollectionViewCell)  {
            //                        self.removeCollectionViewCell(indexPathToRemove: indexPath)
            //                    }
                                
                                let gem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
                                let gemAfterClaimCoupon = gem - coupon.intProductClaimValue
                                
                                HarmonySingleton.shared.dashBoardData.data?.points?.jem = gemAfterClaimCoupon
                                CouponManager.sharedInstance.couponPurchased = true
                                
                                self.view.toast("Yay! you got this offer")
                                if self.isfromSecondChallange ?? false{
                                    self.dismiss(animated: false, completion: nil)
                                    self.delegate?.gemsClickedFromOnboarding()
                                }
                            case .failure(let error):
                                self.view.toast(error.localizedDescription)
                            }
                        }
        } else {
            
                let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
                if creditType == 1 {
                    self.view.toast(String(format: "Oops this offer requires %d goal(s)", coupon.intProductClaimValue))

                } else if creditType == 2 {
                    self.view.toast(String(format: "Oops this offer requires %d gem(s)", coupon.intProductClaimValue))

                } else {
                    self.view.toast(String(format: "Oops this offer requires %d touchdown(s)", coupon.intProductClaimValue))

            }
        }
    }
}

// MARK: - CustomNavigationBarDelegate methods

extension GemsViewController: CustomNavigationBarDelegate {
    func customNavigationBarDidTapLeftButton(_ navigationBar: CustomNavigationBar) {
        dismiss(animated: true, completion: nil)
    }
    
    func customNavigationBarDidTapRightButton(_ navigationBar: CustomNavigationBar) {
        
    }
}
