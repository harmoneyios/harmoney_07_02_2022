//
//  RewardsDetailsVC.swift
//  Harmoney
//
//  Created by INQ Projects on 17/12/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol RewardOfferDetailsDelegate {
    func backFunctionTapped()
}
enum RewardsRows: String {
    case task = "Task"
    case type = "Type"
    case bucks = "Harmoney Bucks"
    case reward = "Reward"
    case frequent = "Frequently"
    case completeddate = "Completed Date"
}
class RewardsProgressVm {
    static let shared = RewardsProgressVm()
    var sectTwoTitles = ["Task","Type","Harmoney Bucks","Reward","Frequently","Completed Date"]
    var sectTwoDescriptions = ["Task","Type","Harmoney Bucks","Reward","Frequently","Completed Date"]
    var noRows : [RewardsRows] = [.task, .type, .bucks, .bucks, .reward, .frequent, .completeddate]
    var dataObj : RewardsData?{
        didSet{
            if dataObj?.type == "Personal Fitness"{
                self.sectTwoDescriptions = [self.dataObj!.taskName ,
                                        self.dataObj?.type ?? "",
                                        self.dataObj?.hbucks ?? "",
                                        self.dataObj?.rewardName ?? "",
                                        self.dataObj?.frequncy ?? "",
                                        self.dataObj?.completedDateTime ?? ""]
                noRows = [.task, .type, .bucks, .bucks, .reward, .frequent, .completeddate]
            }else{
                self.sectTwoDescriptions = [self.dataObj!.taskName ,
                                            self.dataObj?.type ?? "",
                                            self.dataObj?.hbucks ?? "",
                                            self.dataObj?.rewardName ?? "",
                                            self.dataObj?.frequncy ?? "",
                                            self.dataObj?.completedDateTime ?? ""]
                    noRows = [.task, .type, .bucks, .bucks, .reward, .frequent, .completeddate]
            }
        }
    }
}

class RewardsDetailsVC: UIViewController {

    @IBOutlet weak var choreUITable: UITableView!
    @IBOutlet weak var shadowLayer: UIView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var claimButton: UIButton!
    var rewardsData: RewardsData?
    var delegate : RewardOfferDetailsDelegate?
    var rewardObj = RewardsProgressVm.shared

    var datasArray = [String]()
    var titleArray = [String]()

    
    @IBOutlet weak var customNavigationbar: CustomNavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let completedTime = self.rewardsData?.completedDateTime.UTCToLocal(incomingFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outGoingFormat: "MMM d, yyyy")
        let hbucks = (self.rewardsData?.hbucks == "") ? "0" : (self.rewardsData?.hbucks ?? "") as String
        
        if rewardsData?.type == "Personal Fitness"{
            datasArray = [self.rewardsData!.taskName ,
                          self.rewardsData?.type ?? "",
                          hbucks,
                          self.rewardsData?.rewardName ?? "",
                          completedTime ?? ""]
            titleArray = ["Task","Type","Harmoney Bucks","Reward","Completed Date"]
        } else {
            datasArray = [self.rewardsData!.taskName ,
                          self.rewardsData?.type ?? "",
                          hbucks,
                          self.rewardsData?.rewardName ?? "",
                          self.rewardsData?.frequncy ?? "",
                          completedTime ?? ""]
            titleArray = ["Task","Type","Harmoney Bucks","Reward","Frequently","Completed Date"]
            
        }
        
        // Do any additional setup after loading the view.
        self.shadowLayer.clipsToBounds = false;
        self.shadowLayer.layer.masksToBounds = false;
        shadowLayer.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.3659284607)
        shadowLayer.layer.shadowOffset = CGSize.init(width: 1, height: 1)
        shadowLayer.layer.shadowRadius = 5
        shadowLayer.layer.shadowOpacity = 1
        
        tableViewHeight.constant = 490
        customNavigationbar.delegate = self
        customNavigationbar.title = "Reward Details"

    }
    
    @IBAction func closeClick(_ sender: Any) {
//        rewardParentVC?.choreProgressContainerView.isHidden = true
    }
    @IBAction func claimButtonAction(_ sender: Any) {
        
        PersonalFitnessManager.sharedInstance.claimPersonalChallengeReward(personalChallenge: rewardsData!) { [self] (result) in
                    switch result {
                        case .success( _):
                           // self.presentAcceptChallengeViewController(challengeType: customRewardTableViewCell.customReward?.taskType ?? 0)
                           // self.dismiss(animated: true, completion: nil)
//                            if let indexPath = self.tableView.indexPath(for: customRewardTableViewCell)  {
//                                self.removeTableViewCell(indexPathToRemove: indexPath)
//                                self.view.toast("Reward Successfully claimed")
//                            //self.getAllCustomReward()
//                            }
                            self.view.toast("Reward Successfully claimed")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                if HarmonySingleton.shared.isSecondChallange {
                                    delegate?.backFunctionTapped()
                                }
                                dismiss(animated: true, completion: nil)
                            }
                        case .failure(let error):
                            self.view.toast(error.localizedDescription)
                    }
                }
    }
    
}

extension RewardsDetailsVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChoresHeaderCell", for: indexPath) as! ChoresHeaderCell
            cell.choreTitle.text = rewardsData?.taskName ?? ""
            cell.choreImg.imageFromURL(urlString: rewardsData?.taskImage ?? "")
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChoreNameCell", for: indexPath) as! ChoreNameCell
            cell.choreTskHeading.text = titleArray[indexPath.row]
            cell.choreNameLabel.text = datasArray[indexPath.row]
            cell.choreNameLabel.lineBreakMode = .byTruncatingTail
            cell.choreNameLabel.textColor = #colorLiteral(red: 0.9725490196, green: 0.6549019608, blue: 0.6196078431, alpha: 1)
            cell.choreTskHeading.adjustsFontSizeToFitWidth = true
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {return 1}
        if section == 1 {return datasArray.count}
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {return 135}
        if indexPath.section == 1 {return 50}
       
        return 0
    }
}
// MARK: - CustomNavigationBarDelegate methods

extension RewardsDetailsVC: CustomNavigationBarDelegate {
    func customNavigationBarDidTapLeftButton(_ navigationBar: CustomNavigationBar) {
        if HarmonySingleton.shared.isSecondChallange {
            delegate?.backFunctionTapped()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func customNavigationBarDidTapRightButton(_ navigationBar: CustomNavigationBar) {
        
    }
}

