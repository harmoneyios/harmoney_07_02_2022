//
//  VouchersViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/8/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class VouchersViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var voucherDetailsVC: VoucherDetailsViewController?
    
    var vouchers: Voucher?
    let cellReuseIdentifier = "customRewardCell"
    var indexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getVouchers()
    }
}

//MARK: UI Methods
extension VouchersViewController {
    func configureUI() {
        setupTableView()
        getVouchers()
    }
    
    func setupTableView() {
        tableView.register(UINib.init(nibName: "CustomRewardTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func removeTableViewCell(indexPathToRemove: IndexPath) {
        UIView.animate(withDuration: 1) {
            self.tableView.performBatchUpdates({
                self.tableView.deleteRows(at: [indexPathToRemove], with: .fade)
            })
        }
    }
    
    func getVouchers() {
        VouchersManager.sharedInstance.getVouchers { (result) in
            switch result {
            case .success(let vouchers):
                self.vouchers = vouchers
                
                self.tableView.reloadData()
            case .failure(let error):
//                 self.view.toast(error.localizedDescription)
                print(error.localizedDescription)
            }
        }
    }
    func backFunctionTapped() {
        self.tabBarController?.selectedIndex = 2
    }
}

//MARK: UI Action
extension OffersViewController {
    
}

//MARK: - UITableViewDelegate Methods
extension VouchersViewController: UITableViewDelegate, CustomRewardTableViewCellDelegate {
    func didTapClaimButton(customRewardTableViewCell: CustomRewardTableViewCell) {
//        VouchersManager.sharedInstance.claimVoucher(voucherData: customRewardTableViewCell.voucherData!) { (result) in
//            switch result {
//            case .success(_):
//                self.vouchers = VouchersManager.sharedInstance.vouchers
//                
//                if let indexPath = self.tableView.indexPath(for: customRewardTableViewCell)  {
//                    self.removeTableViewCell(indexPathToRemove: indexPath)
//                     self.view.toast("Voucher Claimed")
//                }
//            case .failure(let error):
//                self.view.toast(error.localizedDescription)
//            }
//        }
    }
}

//MARK: - UITableViewDataSource Methods
extension VouchersViewController: UITableViewDataSource,VoucherOfferDetailsDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if vouchers?.data.count == 0 {
               self.tableView.setEmptyMessage("No Voucher Available")
           } else {
               self.tableView.restore()
           }
        return vouchers?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CustomRewardTableViewCell
        
        let voucherData = vouchers?.data[indexPath.row]
        cell.voucherData = voucherData!
        cell.delegate = self
        cell.smallClaimButton.isUserInteractionEnabled = false
        cell.tabButton.isUserInteractionEnabled = false
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let customVoucher = vouchers?.data[indexPath.row]
        self.presentOfferDetailViewController(voucherData: customVoucher!)
    }

    func presentOfferDetailViewController(voucherData: VoucherData) {
        voucherDetailsVC = GlobalStoryBoard().voucherDetailsVC
        voucherDetailsVC?.voucherData = voucherData
        voucherDetailsVC?.delegate = self
        tabBarController?.present(voucherDetailsVC!, animated: true, completion: nil)
    }
   
}

