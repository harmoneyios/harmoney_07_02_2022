//
//  CouponClaimViewController.swift
//  Harmoney
//
//  Created by INQ Projects on 09/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class CouponClaimViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var purchasedCouponList: CouponPurchasedListModel?
    let cellReuseIdentifier = "ClaimListTableViewCell"
    var indexPath: IndexPath?
    var offerDetailsViewController: OfferDetailsViewController?
    var tappedCustomRewardTableViewCell: CustomRewardTableViewCell?
    
    var claimedCouponList: PurchasedCouponClaimedList?

    var couponInfoData : CouponInfo?
    var index = IndexPath()
    
    var sectionArray = [String]()
    
    var isPurchsedList : Bool = false
    var isClaimedList : Bool = false
    var isPurchased:Bool = false
    var orderId:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        sectionArray = ["Purchase Coupon","Claimed Coupon"]
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getPurchasedListCoupons()
        self.getClaimedListCoupons()

    }
}

//MARK: UI Methods
extension CouponClaimViewController {
    func configureUI() {
        setupTableView()
//        getClaimedCoupons()
       
    }
    
    func setupTableView() {
        tableView.register(UINib.init(nibName: "ClaimListTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension

    }
    
    func removeTableViewCell(indexPathToRemove: IndexPath) {
        UIView.animate(withDuration: 1) {
            self.tableView.performBatchUpdates({
                self.tableView.deleteRows(at: [indexPathToRemove], with: .fade)
            })
        }
    }
    
    func getPurchasedListCoupons() {
        CouponManager.sharedInstance.getPurchasedCouponList { (result) in
            switch result {
            case .success(let purchasedCoupons):
                self.purchasedCouponList = purchasedCoupons
                self.isPurchsedList = true
                if self.isPurchsedList && self.isClaimedList {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    func getClaimedListCoupons() {
        CouponManager.sharedInstance.getPurchasedClaimedCouponList { (result) in
            switch result {
            case .success(let purchasedCoupons):
                self.claimedCouponList = purchasedCoupons
                self.isClaimedList = true
                if self.isPurchsedList && self.isClaimedList {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func presentOfferDetailViewController(claimedCoupon: Datum) {
        offerDetailsViewController = GlobalStoryBoard().OfferDetailsVC
        offerDetailsViewController?.claimedCoupon = claimedCoupon
        offerDetailsViewController?.delegate = self
        tabBarController?.present(offerDetailsViewController!, animated: true, completion: nil)
    }
}

//MARK: UI Action
extension CouponClaimViewController {
    
}

//MARK: - UITableViewDelegate Methods
extension CouponClaimViewController: UITableViewDelegate, CustomRewardTableViewCellDelegate, OfferDetailsDelegate {
    
    func deleteCoupounTapped() {
        CouponManager.sharedInstance.claimCoupon(claimedCoupon: tappedCustomRewardTableViewCell!.claimedCoupon!) { (result) in
            switch result {
            case .success(_):
                self.purchasedCouponList = CouponManager.sharedInstance.couponPurchasedList
                
                if let indexPath = self.tableView.indexPath(for: self.tappedCustomRewardTableViewCell!) {
                    self.removeTableViewCell(indexPathToRemove: indexPath)
                }
                if HarmonySingleton.shared.isSecondChallange {
                    self.tabBarController?.selectedIndex = 1
                    NotificationCenter.default.post(name: Notification.Name("OnboardingOfferClaimed"), object: nil)
                }
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    func backFunctionTapped() {
        self.tabBarController?.selectedIndex = 1
    }
    func didTapClaimButton(customRewardTableViewCell: CustomRewardTableViewCell) {
        tappedCustomRewardTableViewCell = customRewardTableViewCell
        self.presentOfferDetailViewController(claimedCoupon: customRewardTableViewCell.claimedCoupon!)
    }
    
    func claimedCouponApimethod(couponOrderId: String, isRemovedClicked:Bool)  {
        
        CouponManager.sharedInstance.claimPurchaseCoupon(orderId:couponOrderId) { (result) in
            switch result {case .success(let coupons):
               print(coupons)
                let successMessage = coupons.message
                if self.isPurchased {
                self.view.toast(successMessage)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self.dismiss(animated: true, completion:{
                        })
                    }
                } else {
                    if isRemovedClicked {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            self.dismiss(animated: true, completion:{
                            })
                        }
                    }
                }
                
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
}

//MARK: - UITableViewDataSource Methods
extension CouponClaimViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if purchasedCouponList?.data.count == 0 && claimedCouponList?.data.count == 0 {
            return 1
           } else {
            return sectionArray.count
           }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if purchasedCouponList?.data.count == 0 && claimedCouponList?.data.count == 0 {
               self.tableView.setEmptyMessage("No Coupon Available")
            let vw = UIView()
            vw.backgroundColor = UIColor.clear
            let lbl = UILabel.init(frame: CGRect.init(x: 20, y: 5, width: 200, height: 30))
            lbl.text = ""
            lbl.font = UIFont.futuraPTMediumFont(size: 18)
            lbl.textColor = UIColor.clear
            vw.addSubview(lbl)
            return vw
            
        }
        let vw = UIView()
        vw.backgroundColor = UIColor.harmoneySectionHeaderColor
        let lbl = UILabel.init(frame: CGRect.init(x: 20, y: 5, width: 200, height: 30))
        lbl.text = sectionArray[section]
        lbl.font = UIFont.futuraPTMediumFont(size: 18)
        lbl.textColor = #colorLiteral(red: 0.1176470588, green: 0.2470588235, blue: 0.4, alpha: 1)
        vw.addSubview(lbl)
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if purchasedCouponList?.data.count == 0 && claimedCouponList?.data.count == 0 {
               self.tableView.setEmptyMessage("No Coupon Available")
           } else {
               self.tableView.restore()
            if section == 0 {
                if purchasedCouponList != nil {
                    return (purchasedCouponList?.data.count)!
                }
            } else {
                if claimedCouponList != nil {
                    return (claimedCouponList?.data.count)!
                }
            }
            
           }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! ClaimListTableViewCell
        cell.smallClaimButton.setTitle("View", for: .normal)
        cell.smallClaimButton.isEnabled = false
        
        if indexPath.section == 0 {
            let purchaseCoupon = purchasedCouponList?.data[indexPath.row]
            cell.purchasedCouponTools = purchaseCoupon

        } else{
            let claimedCoupon = claimedCouponList?.data[indexPath.row]
            cell.claimedCouponTools = claimedCoupon
            cell.smallClaimButton.setTitle("Remove", for: .normal)
            cell.smallClaimButton.isHidden = true

        }

        cell.delegate = self
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let couponDetails = GlobalStoryBoard().couponDetails
        if indexPath.section == 0 {
            couponDetails.purchasedCoupon = purchasedCouponList?.data[indexPath.row]
            couponDetails.isPurchased = true
//            self.isPurchased = true
//            self.orderId = purchasedCouponList?.data[indexPath.row].orderID ?? ""
//            presentRemoveCouponViewController(imageUrl: purchasedCouponList?.data[indexPath.row].productList.logoURL ?? "")
            self.present(couponDetails, animated: true, completion: nil)
          
        } else{
//            couponDetails.claimedCoupon = claimedCouponList?.data[indexPath.row]
//            couponDetails.isPurchased = false
//            self.isPurchased = false
//            self.orderId = claimedCouponList?.data[indexPath.row].orderID ?? ""
//          presentRemoveCouponViewController(imageUrl: claimedCouponList?.data[indexPath.row].productList.logoURL ?? "")
        }
      
    }
    
    func presentRemoveCouponViewController(imageUrl:String) {
       let removeCouponViewController = GlobalStoryBoard().removeCouponVC
        removeCouponViewController.delegate = self
        removeCouponViewController.imageString = "1"
        removeCouponViewController.titleString = "Are you sure you want to remove this coupon?"
        removeCouponViewController.descriptionString = "You will loose purchased this coupon so far Continue with delete?"
        removeCouponViewController.memberID = ""
        self.present(removeCouponViewController, animated: true, completion: nil)
    }
}

//MARK: - DeleteChallangeViewControllerDelegate Methods

extension CouponClaimViewController: RemoveCouponViewControllerDelegate {
    func didTapThumbsDownButton(deleteChallangeViewController: RemoveCouponAlertViewController) {
        deleteChallangeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapThumbsUpButton(deleteChallangeViewController: RemoveCouponAlertViewController,memberId:String) {
       
        if self.isPurchased {
            self.claimedCouponApimethod(couponOrderId: self.orderId ,isRemovedClicked: false)
        } else {
            self.claimedCouponApimethod(couponOrderId: self.orderId,isRemovedClicked: false)

        }
            deleteChallangeViewController.dismiss(animated: true) {

            }
    }
}
