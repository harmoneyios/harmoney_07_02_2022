//
//  RemoveCouponAlertViewController.swift
//  Harmoney
//
//  Created by INQ Projects on 28/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

protocol RemoveCouponViewControllerDelegate: class {
    func didTapThumbsDownButton(deleteChallangeViewController: RemoveCouponAlertViewController)
    func didTapThumbsUpButton(deleteChallangeViewController: RemoveCouponAlertViewController, memberId:String)
}

class RemoveCouponAlertViewController: UIViewController {
    
    @IBOutlet weak var challengeTypeImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var describtionLabel: UILabel!
    
    weak var delegate: RemoveCouponViewControllerDelegate?
    var imageString = String()
    var titleString = String()
    var descriptionString = String()
    var memberID = String()

    
//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    //MARK: UI Methods
    func configureUI() {
        if imageString != "" {
            challengeTypeImageView.imageFromURL(urlString:imageString)
            
        }else if imageString == "1"{
            
            self.challengeTypeImageView.isHidden = true
            
        }else {
            challengeTypeImageView.image = UIImage(named: "harmoneycircle")
        }
        titleLabel.text = titleString
        describtionLabel.text = descriptionString

     }
}

//MARK: Action Methods
extension RemoveCouponAlertViewController {
    @IBAction func thumbsUpIconTapped(_ sender: UIButton) {
        delegate?.didTapThumbsUpButton(deleteChallangeViewController: self,memberId: memberID)
    }
    
    @IBAction func thumbsDownIconTapped(_ sender: Any) {
        delegate?.didTapThumbsDownButton(deleteChallangeViewController: self)
    }
}
