//
//  QRCodeGenerateViewController.swift
//  Harmoney
//
//  Created by INQ Projects on 08/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class QRCodeGenerateViewController: UIViewController {
    
    @IBOutlet weak var navigationBarView: CustomNavigationBar!
    @IBOutlet weak var barcodeImageView: UIImageView!
    @IBOutlet weak var discountValueLabel: UILabel!
    @IBOutlet weak var couponCodeLabel1: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var validDateLabel: UILabel!

    @IBOutlet weak var deleteOfferBtn: UIButton!
    
    @IBOutlet weak var companyLogoImage: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var gemCountLabel: UILabel!
    
    @IBOutlet weak var diamondImageview: UIImageView!
    var removeCouponViewController: DeleteChallangeViewController?

    var claimedCoupon: DatumClaimed?
    var purchasedCoupon: CouponPurchasedDatum?

    var delegate : OfferDetailsDelegate?
    var couponValidationList: CouponValidationModel?
    var couponData : CouponInfo?
    var isValidated : Bool = false
    var isPurchased = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isPurchased {
            self.getCouponClaimsList(couponCompainId: self.purchasedCoupon?.productList.collection.compainID ?? "",couponSessionId:self.purchasedCoupon?.productList.collection.session ?? "")
            configurePurchasedUI()

        } else {
            self.getCouponClaimsList(couponCompainId:claimedCoupon?.productList.collection.compainID ?? "",couponSessionId:self.claimedCoupon?.productList.collection.session ?? "" )
            configureClaimedUI()

        }
        let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
        
        if creditType == 1 {
            self.diamondImageview.image = UIImage(named: "soccer_black")

        } else if creditType == 2 {
            diamondImageview.image = UIImage(named: "diamond_black")

        } else {
            self.diamondImageview.image = UIImage(named: "touchdown_black")

        }
    }
    @IBAction func deleteOfferClck(_ sender: Any) {
        if self.isValidated {
            self.claimedCouponApimethod(couponOrderId: claimedCoupon?.orderID ?? "",isRemovedClicked: true)

        } else {
            if isPurchased {
                presentRemoveCouponViewController(imageUrl: purchasedCoupon?.productList.logoURL ?? "")
            }else {
                presentRemoveCouponViewController(imageUrl: claimedCoupon?.productList.logoURL ?? "")

            }
                
        }
    }
   
    func claimedCouponApimethod(couponOrderId: String, isRemovedClicked:Bool)  {
        
        CouponManager.sharedInstance.claimPurchaseCoupon(orderId:couponOrderId) { (result) in
            switch result {case .success(let coupons):
               print(coupons)
                let successMessage = coupons.message
                if self.isPurchased {
                self.view.toast(successMessage)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        self.dismiss(animated: true, completion:{
                        })
                    }
                } else {
                    if isRemovedClicked {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            self.dismiss(animated: true, completion:{
                            })
                        }
                    }
                }
                
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    func getCouponClaimsList(couponCompainId:String, couponSessionId: String)  {
        CouponManager.sharedInstance.getAllCouponsClaimsApi(claimedCouponID: couponCompainId,currentTime:"") { (result) in
            switch result {case .success(let coupons):
                self.couponValidationList = coupons
                let fArray = self.couponValidationList?.data.filter {$0.session  == couponSessionId}
                let isvalidate = fArray?.first?.status.validated
                if isvalidate == "0" {
                    self.isValidated  = false
                } else {
                    if self.isPurchased {
                        self.claimedCouponApimethod(couponOrderId: self.purchasedCoupon?.orderID ?? "",isRemovedClicked: false)
                    } else {
                        self.claimedCouponApimethod(couponOrderId: self.claimedCoupon?.orderID ?? "",isRemovedClicked: false)
                    }
                    self.isValidated  = true
                }
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    func presentRemoveCouponViewController(imageUrl:String) {
       let removeCouponViewController = GlobalStoryBoard().removeCouponVC
        removeCouponViewController.delegate = self
        removeCouponViewController.imageString = "1"
        removeCouponViewController.titleString = "Are you sure you want to claim this coupon?"
        removeCouponViewController.descriptionString = "You will loss purchased this coupon so far Continue with delete?"
        removeCouponViewController.memberID = ""
        self.present(removeCouponViewController, animated: true, completion: nil)
    }
}

// MARK: - UI methods

extension QRCodeGenerateViewController {
    
    func configurePurchasedUI() {
        navigationBarView.delegate = self
        navigationBarView.title = "Coupon Details"
        deleteOfferBtn.layer.cornerRadius = 10
        deleteOfferBtn.titleLabel?.text = "Claim"
        deleteOfferBtn .setTitle("Claim" , for: .normal)
        companyLogoImage.imageFromURL(urlString: purchasedCoupon?.productList.logoURL ?? "")
        
        let urlString = purchasedCoupon?.productList.collection.url ?? ""
        
        let fileName = urlString
        let fileArray = fileName.components(separatedBy: "/")
        let firstString = fileArray.first
        let secondString = fileArray[2]

        let barcodeUrl = String(format: "%@//%@/qrval/%@/%@",firstString!,secondString,purchasedCoupon?.productList.collection.code ?? "",purchasedCoupon?.productList.collection.session ?? "")
        
        barcodeImageView.image = generateBarcode(from:barcodeUrl,qrCodeImage: barcodeImageView)
        discountValueLabel.text = purchasedCoupon?.productList.collection.value ?? ""//String(format: "%@ one purchase", claimedCoupon?.value ?? "")
        couponCodeLabel1.text = String(format: "Promo Code: %@", purchasedCoupon?.productList.collection.value ?? "")
        
        let description = purchasedCoupon?.productList.collection.collectionDescription ?? ""
        
        let str = description.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        descriptionLabel.text = str
        let expiryDate = purchasedCoupon?.productList.collection.expirydate
        
        let convertDate = self.convertDateFormater(expiryDate ?? "")

        validDateLabel.text = String(format: "Valid until %@",convertDate )
        
        gemCountLabel.text = "\(String(describing: purchasedCoupon!.gem))"
        
        
    }
    
    func configureClaimedUI() {
        navigationBarView.delegate = self
        navigationBarView.title = "Coupon Details"
        deleteOfferBtn.titleLabel?.text = "Remove"
        deleteOfferBtn .setTitle("Remove" , for: .normal)

        deleteOfferBtn.layer.cornerRadius = 10
        companyLogoImage.imageFromURL(urlString: claimedCoupon?.productList.logoURL ?? "")
        
        let urlString = claimedCoupon?.productList.collection.url ?? ""
        
        let fileName = urlString
        let fileArray = fileName.components(separatedBy: "/")
        let firstString = fileArray.first
        let secondString = fileArray[2]

        let barcodeUrl = String(format: "%@//%@/qrval/%@/%@",firstString!,secondString,claimedCoupon?.productList.collection.code ?? "",claimedCoupon?.productList.collection.session ?? "")
        
        barcodeImageView.image = generateBarcode(from:barcodeUrl,qrCodeImage: barcodeImageView)
        discountValueLabel.text = claimedCoupon?.productList.collection.value ?? ""//String(format: "%@ one purchase", claimedCoupon?.value ?? "")
        couponCodeLabel1.text = String(format: "Promo Code: %@", claimedCoupon?.productList.collection.value ?? "")
        
        let description = claimedCoupon?.productList.collection.collectionDescription ?? ""
        
        let str = description.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        descriptionLabel.text = str
        let expiryDate = claimedCoupon?.productList.collection.expirydate
        
        let convertDate = self.convertDateFormater(expiryDate ?? "")

        validDateLabel.text = String(format: "Valid until %@",convertDate )
        
        gemCountLabel.text = "\(String(describing: claimedCoupon!.gem))"
        
        
    }
    func generateBarcode(from string: String,qrCodeImage: UIImageView) -> UIImage? {
        
        let data = string.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)

        let filter = CIFilter(name: "CIQRCodeGenerator")

        filter?.setValue(data, forKey: "inputMessage")
        filter?.setValue("Q", forKey: "inputCorrectionLevel")

        let qrcodeImages = filter?.outputImage

        //qrImageView is a IBOutlet of UIImageView
        let scaleX = qrCodeImage.frame.size.width / (qrcodeImages?.extent.size.width)!
        let scaleY = qrCodeImage.frame.size.height / (qrcodeImages?.extent.size.height)!

        let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)

        
        if let output = qrcodeImages?.transformed(by: transform) {
            return UIImage(ciImage: output)
        }
  
        return nil
    }
    
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "E d MMM yyyy"
            return  dateFormatter.string(from: date!)

        }
  
}

// MARK: - CustomNavigationBarDelegate methods

extension QRCodeGenerateViewController: CustomNavigationBarDelegate {
    func customNavigationBarDidTapLeftButton(_ navigationBar: CustomNavigationBar) {
        if HarmonySingleton.shared.isSecondChallange {
            delegate?.backFunctionTapped()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func customNavigationBarDidTapRightButton(_ navigationBar: CustomNavigationBar) {
        
    }
}

//MARK: - DeleteChallangeViewControllerDelegate Methods

extension QRCodeGenerateViewController: RemoveCouponViewControllerDelegate {
    func didTapThumbsDownButton(deleteChallangeViewController: RemoveCouponAlertViewController) {
        deleteChallangeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapThumbsUpButton(deleteChallangeViewController: RemoveCouponAlertViewController,memberId:String) {
       
        if isPurchased {
        self.claimedCouponApimethod(couponOrderId: purchasedCoupon?.orderID ?? "",isRemovedClicked: false)
        } else {
            self.claimedCouponApimethod(couponOrderId: claimedCoupon?.orderID ?? "",isRemovedClicked: false)

        }
            deleteChallangeViewController.dismiss(animated: true) {

            }
    }
}

