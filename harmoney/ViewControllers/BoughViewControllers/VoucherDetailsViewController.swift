//
//  VoucherDetailsViewController.swift
//  Harmoney
//
//  Created by INQ Projects on 22/12/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol VoucherOfferDetailsDelegate {
    func backFunctionTapped()
}


class VoucherDetailsViewController: UIViewController {

    @IBOutlet weak var choreUITable: UITableView!
    @IBOutlet weak var shadowLayer: UIView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var claimButton: UIButton!
    var voucherData: VoucherData?
    var delegate : VoucherOfferDetailsDelegate?
    var rewardObj = RewardsProgressVm.shared

    var datasArray = [String]()
    var titleArray = [String]()

    
    @IBOutlet weak var customNavigationbar: CustomNavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
            let value = "\(self.voucherData?.voucherValue ?? 0)"

            datasArray = [self.voucherData?.voucherName ?? "",
                          "Voucher from Level Up",
                          value,
                          self.voucherData?.voucherCode ?? ""
                          ]
            titleArray = ["Voucher Name","Voucher From","Voucher Value","Voucher Code"]
        
        
        // Do any additional setup after loading the view.
        self.shadowLayer.clipsToBounds = false;
        self.shadowLayer.layer.masksToBounds = false;
        shadowLayer.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.3659284607)
        shadowLayer.layer.shadowOffset = CGSize.init(width: 1, height: 1)
        shadowLayer.layer.shadowRadius = 5
        shadowLayer.layer.shadowOpacity = 1
        
        tableViewHeight.constant = 490
        customNavigationbar.delegate = self
        customNavigationbar.title = "Voucher Details"

    }
    
    @IBAction func closeClick(_ sender: Any) {
//        rewardParentVC?.choreProgressContainerView.isHidden = true
    }
    @IBAction func claimButtonAction(_ sender: Any) {
        
                VouchersManager.sharedInstance.claimVoucher(voucherData: voucherData!) { (result) in
                    switch result {
                    case .success(_):
                        self.view.toast("Voucher Successfully claimed")

                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if HarmonySingleton.shared.isSecondChallange {
                                self.delegate?.backFunctionTapped()
                            }
                            self.dismiss(animated: true, completion: nil)
                        }
                    case .failure(let error):
                        self.view.toast(error.localizedDescription)
                    }
                }
     
    }
    
}

extension VoucherDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChoresHeaderCell", for: indexPath) as! ChoresHeaderCell
            cell.choreTitle.text = voucherData?.voucherName ?? ""
            if voucherData?.voucherImage == "" || voucherData?.voucherImage != nil {
                cell.choreImg.imageFromURL(urlString: voucherData?.voucherImage ?? "")

            }else {
                cell.choreImg.image = UIImage(named: "ProfilePlaceholder")

            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChoreNameCell", for: indexPath) as! ChoreNameCell
            cell.choreTskHeading.text = titleArray[indexPath.row]
            cell.choreNameLabel.text = datasArray[indexPath.row]
            cell.choreNameLabel.lineBreakMode = .byTruncatingTail
            cell.choreNameLabel.textColor = #colorLiteral(red: 0.9725490196, green: 0.6549019608, blue: 0.6196078431, alpha: 1)
            cell.choreTskHeading.adjustsFontSizeToFitWidth = true
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {return 1}
        if section == 1 {return datasArray.count}
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {return 135}
        if indexPath.section == 1 {return 50}
       
        return 0
    }
}
// MARK: - CustomNavigationBarDelegate methods

extension VoucherDetailsViewController: CustomNavigationBarDelegate {
    func customNavigationBarDidTapLeftButton(_ navigationBar: CustomNavigationBar) {
        if HarmonySingleton.shared.isSecondChallange {
            delegate?.backFunctionTapped()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func customNavigationBarDidTapRightButton(_ navigationBar: CustomNavigationBar) {
        
    }
}

