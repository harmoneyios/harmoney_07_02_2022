//
//  HealthPermissionVC.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Toast_Swift
import CoreMotion

class HealthPermissionVC: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var allowBtn: UIButton!
    @IBOutlet weak var dontAllowBtn: UIButton!


    var username = String()
    var categoryChoosing = String()
    var pinCode = String()
    var guid: String!
    var schoolName = String()
    var isPushEnabled: Bool = false


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setupInitial()
        setContent()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
//    func reqguestForMotionSensor() {
//        let pedometer = CMPedometer()
//               pedometer.startUpdates(from: Calendar.current.startOfDay(for: Date())) {
//                pedometerData, error in
//                guard let _ = pedometerData, error == nil else {
//                       return
//                }
//            }
//    }
    
    func setContent()  {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        if let pageData = appdelegate.cmsModelObject?.data?.Motion{
            titleLabel.text = pageData.title1
            descLabel.text = pageData.desc1
            allowBtn.setTitle(pageData.button2, for: .normal)
            dontAllowBtn.setTitle(pageData.button1, for: .normal)
        }
    }
    func setupInitial() {
         
         allowBtn.backgroundColor = ConstantString.buttonBackgroundColor
         allowBtn.titleLabel?.font = ConstantString.buttonTitleFont
         allowBtn.setTitleColor(ConstantString.buttontextColor, for: .normal)

         titleLabel.font = ConstantString.titleFont
         descLabel.font = ConstantString.descriptionFont
         
         titleLabel.textColor = ConstantString.textColor
         descLabel.textColor = ConstantString.textColor
        
        dontAllowBtn.setTitle("No way !\nI don't want access my health kit data", for: .normal)
        
        
        dontAllowBtn.layer.borderColor = ConstantString.borderColor.cgColor
        dontAllowBtn.layer.borderWidth = 1

    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func pushToSeperateView() {
        if (self.categoryChoosing == "2" || self.categoryChoosing == "3")
        {
            
      /*      let bankaccountLinkVC = GlobalStoryBoard().bankAccountVC
            bankaccountLinkVC.username = self.username
            bankaccountLinkVC.categoryChoosing = self.categoryChoosing
            bankaccountLinkVC.guid = self.guid
            bankaccountLinkVC.zipCode = self.pinCode
            bankaccountLinkVC.allowPushNotifications = isPushEnabled
            self.navigationController?.pushViewController(bankaccountLinkVC, animated: true)*/
            
           // payment gate way  over come 
            let earnHarmoneyBucksVC = GlobalStoryBoard().earnHarmoneyVC
            earnHarmoneyBucksVC.username = self.username
            earnHarmoneyBucksVC.categoryChoosing = self.categoryChoosing
            earnHarmoneyBucksVC.pinCode = self.pinCode
            earnHarmoneyBucksVC.schoolName = self.schoolName
            earnHarmoneyBucksVC.guid = self.guid
            earnHarmoneyBucksVC.allowPushNotifications = isPushEnabled
            earnHarmoneyBucksVC.parentLinkPayment = false
            self.navigationController?.pushViewController(earnHarmoneyBucksVC, animated: true)
        }
        else
        {
            let earnHarmoneyBucksVC = GlobalStoryBoard().earnHarmoneyVC
            earnHarmoneyBucksVC.username = self.username
            earnHarmoneyBucksVC.categoryChoosing = self.categoryChoosing
            earnHarmoneyBucksVC.pinCode = self.pinCode
            earnHarmoneyBucksVC.schoolName = self.schoolName
            earnHarmoneyBucksVC.guid = self.guid
            earnHarmoneyBucksVC.allowPushNotifications = isPushEnabled
            earnHarmoneyBucksVC.parentLinkPayment = false
            self.navigationController?.pushViewController(earnHarmoneyBucksVC, animated: true)
        }
    }

    @IBAction func allowClick(_ sender: Any){
//        reqguestForMotionSensor()
        
        let healthService = HealthService()
        healthService.requestAccess { [weak self](success, error) in
            guard error == nil, success else {
                print("Some error occurend when fetching permission...")
                return
            }
            DispatchQueue.main.async {
                self?.pushToSeperateView()
            }
        }
    }
    @IBAction func donwtAllowClick(_ sender: Any){
        pushToSeperateView()
    }
    

    
}
