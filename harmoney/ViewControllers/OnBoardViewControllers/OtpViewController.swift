//
//  OtpViewController.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit
import Alamofire
import TextFieldEffects
import Toast_Swift
import SVProgressHUD
import ObjectMapper
class OtpViewController: UIViewController,UITextFieldDelegate{
    
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var bottomBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var hTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var reSendBtn: UIButton!
    @IBOutlet weak var changeNumberBtn: UIButton!
    @IBOutlet weak var reSend: UILabel!
    @IBOutlet weak var tfConstraint: NSLayoutConstraint!
    var sendOtpModelObject : sendOtpModelData?
    
    var enteredOTP: String?
    var guid: String!
    var mobileNumber: String!
    var comeFrom: String!
    var schools = [Any]()
    var runfor = [Any]()
    var otpCount = 0
    var otp = String()
    
    var islocationisCalled = false
    var globalData = [String : Any]()
    
    var secondTime = false
    
    //MARK: - Function Start
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        loginFunctionality()
        setupInitial()
        setContent()
        otpTextField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setContent()  {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        if let pageData = appdelegate.cmsModelObject?.data?.OTP {
            titleLabel.text = pageData.title1
            nextBtn.setTitle(pageData.button1, for: .normal)
            descLabel.text =  pageData.desc1
            reSendBtn.setTitle(pageData.link1, for: .normal)
            changeNumberBtn.setTitle(pageData.link2, for: .normal)
        }else{
            self.descLabel.text = "Please key in your verificaton code"
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        otpTextField.becomeFirstResponder()
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXX-XXX-XXXX"
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            bottomBtnConstraint.constant = keyboardSize.height + 10
            // tfConstraint.constant = 0
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        bottomBtnConstraint.constant = 70
        // tfConstraint.constant = 65
    }
    
    func setupInitial() {
        nextBtn.backgroundColor = ConstantString.buttonBackgroundColor
        nextBtn.titleLabel?.font = ConstantString.buttonTitleFont
        nextBtn.titleLabel?.textColor = ConstantString.buttontextColor
        
        titleLabel.font = ConstantString.titleFont
        descLabel.font = ConstantString.descriptionFont
        
        titleLabel.textColor = ConstantString.textColor
        descLabel.textColor = ConstantString.textColor
    }
    
    
    @IBAction func ChangeNumberBtnAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    //MARK: - Resend OTP Functionality
    
    func loginFunctionality() {
        
        let strings = mobileNumber
        let trimmedString = strings!.replacingOccurrences(of: " ", with: "")
       // let data = ["phone": trimmedString]
        let data = ["mno": getEncryptedString(plainText: trimmedString)]
        var (url, method, param) = APIHandler().getOTP(params: data)
       // let ph = (data["phone"]!).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
     //   print(ph ?? "sample")
    //   url = url + "/" + ph!
        print("resend Data")
        print(url, method, param)
        
        
        AF.request(url, method: method, parameters: param, encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                if let value = value as? [String:Any] {
                    if let sucessMessage = value["sucMsg"] as? [String:Any], let message = sucessMessage["message"] {
                        print(message)
                    }
                    if let data = value["data"] as? [String:Any] {
                        print(data)
                        UserDefaultConstants().sessionToken = data["sessionToken"] as? String
                        if let guid = data["guid"] as? String {
                            self.guid = guid
                            UserDefaultConstants().guid = self.guid
                        }
                        if let otp = data["otp"] as? String{
                            self.otp = otp
                        }
                       
                    }
                    
                    if let sucessMessage = value["errMsg"] as? [String:Any], let message = sucessMessage["message"] {
                        self.view.toast(message as! String)
                        print(message)
                    }
                    //if let sucessMessage = value["message"] {
                    //    self.view.toast(sucessMessage as! String)
                    //}
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func reSendOTP(_ sender: Any) {
        otpTextField.text = ""
        loginFunctionality()
    }
    
    @IBAction func verifyAction(_ sender: Any) {
        guard otpTextField.text?.count != 0 && (otpTextField.text?.count == 8) || (otpTextField.text?.count == 6) else {
            view.toast("Enter verificiation code.")
            
            let alertController = UIAlertController(title: nil , message: nil, preferredStyle: .actionSheet)
            let otpActn = UIAlertAction.init(title: "Enter valid digits", style: .default) { (action) in
                // self.selectPictureFromLibraryMtd()
            }
            let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
            alertController.addAction(otpActn)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
            return
        }
        registerVerify()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
              let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                  return false
              }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 8
    }
    
    func pushToProfileCreationView() {
        let profileCreation = GlobalStoryBoard().inputVC
        profileCreation.guid = guid
        self.navigationController?.pushViewController(profileCreation, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func registerVerify() {
        guard Reachability.isConnectedToNetwork() else {
            SVProgressHUD.showError(withStatus: "No network access")
            return
        }
        var data = [String : Any]()
        data.updateValue(guid ?? "", forKey: "guid")
        data.updateValue("1", forKey: "otpType")
        data.updateValue(otpTextField.text ?? "", forKey: "otp")
        let url = APIService.sendOtp
        let headers: HTTPHeaders = [
            "Authorization": UserDefaultConstants().sessionToken ?? "",
            "Accept": "application/json"
        ]
        print("sessionToken:\(headers)")
        /*
        let operation = NetworkOperation(method: .post, parameters: data, headers: headers, encoder: JSONEncoding.default, url: APIService.sendOtp)
        
        BackendManager.sharedInstance.startRequestForJSON(operation: operation) {  (result) in
            switch result {
            case .success(let sendOtpModel):
                print(sendOtpModel)
                if  let json = sendOtpModel as? [String: Any] {
                    let dashBoardData = Mapper<sendOtpModel>().map(JSON: json)
                    
                    
                }
                
            case .failure(let error):
             print(error)
            }
        */
        
        
        let (_, method, param) = APIHandler().sendOtp(params: data)
        print(url)
        print(method)
        print(param)
        SVProgressHUD.show(withStatus: "Verifying code")
        
        AF.request(url, method: method, parameters: param,encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                if  let value = value as? [String:Any] {
                    if let status = value["message"] as? String {
                        self.view.toast(status)
                    }
                    
                    if let data = value["data"] as? [String:Any] {
                        print(data)
                        if let status = value["status"] as? NSNumber {
                            
                            if status == 200{
                                
                                if let guid = data["guid"] as? String {
                                    self.guid = guid
                                }
                                
                                UserDefaultConstants().clearDefaults()
                                print("AuthorizationOTP: \(UserDefaultConstants.shared.deviceToken)")
                                
                               
                                UserDefaultConstants().sessionKey = self.guid
                                
                                UserDefaultConstants().mobileNumberKey = self.mobileNumber
                                
                                // Nelson Start
                                if let mobileNumber = UserDefaultConstants().userPhoneNumber {
                                    var mobilenumber = [String]()
                                    mobilenumber = mobileNumber
                                    mobilenumber.append(self.mobileNumber)
                                    UserDefaultConstants().userPhoneNumber = mobileNumber
                                } else {
                                    var mobilenumber = [String]()
                                    mobilenumber.append(self.mobileNumber)
                                    UserDefaultConstants().userPhoneNumber = mobilenumber
                                }
                                
                                // Nelson End
                                if let profile = data["profileUpdateStatus"] as? NSNumber  {
                                    if profile == 1 {
                                        
                                        let tabBar =  GlobalStoryBoard().launchTabBAr
                                        let homeController = GlobalStoryBoard().earnTabVC
                                        tabBar.selectedIndex = 0
                                        self.navigationController?.pushViewController(tabBar, animated: true)
                                        UserDefaultConstants().login = true
                                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                            appDelegate.registerForPushNotifications(content: HarmoneyBucksConfirmationVC())
                                        }
                                    }else{
                                        self.pushToProfileCreationView()
                                    }
                                }
                            }
                        }
                    }
                }
                SVProgressHUD.dismiss()
                
            case .failure(let error):
                print(error)
                SVProgressHUD.showError(withStatus: "Error in verifying code:" + "\(error.localizedDescription)" )
            }
        }
 
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

