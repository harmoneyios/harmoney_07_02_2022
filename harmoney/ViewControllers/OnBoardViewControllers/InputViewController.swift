//
//  InputViewController.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit

class InputViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var enterSchoolNameContainerView: UIView!
    @IBOutlet weak var schoolNameTextfield: UITextField!
    @IBOutlet weak var inputtextField: UITextField!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var bottomBtnConstraint: NSLayoutConstraint!

    var location = Bool()
    var username = String()
    var categoryChoosing = String()
    var guid: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
       
        setupInitial()
      //Nelson
        inputtextField.keyboardDistanceFromTextField = 60
        
        //
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if location
            {
                bottomBtnConstraint.constant = keyboardSize.height + 10
            }
            else
            {
                if view.bounds.height <= 700 {
                    bottomBtnConstraint.constant = keyboardSize.height
                } else {
                    bottomBtnConstraint.constant = keyboardSize.height + 10
                }
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
         bottomBtnConstraint.constant = 150

    }
    func setupInitial() {
        
        nextBtn.backgroundColor = ConstantString.buttonBackgroundColor
        nextBtn.titleLabel?.font = ConstantString.buttonTitleFont
        nextBtn.titleLabel?.textColor = ConstantString.buttontextColor
        
        titleLabel.font = ConstantString.titleFont
        descLabel.font = ConstantString.buttonTitleFont
        
        titleLabel.textColor = ConstantString.textColor
        descLabel.textColor = ConstantString.textColor
       
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        

       if location
       {
           
           inputtextField.keyboardType = UIKeyboardType.numberPad
           backgroundImageView.image = UIImage(named: "location")
           inputtextField.placeholder = "Enter your zip code"
        
            if let pageData = appdelegate.cmsModelObject?.data?.Zip{
                titleLabel.text = pageData.title1
                nextBtn.setTitle(pageData.button1, for: .normal)
                descLabel.text = pageData.desc1
                self.enterSchoolNameContainerView.isHidden = true
                            
            }else{
                titleLabel.text = "We're almost done!"
                nextBtn.setTitle("Yay! Let's go", for: .normal)
                descLabel.text = "Tell us your zip code to connect with your friends"
                self.enterSchoolNameContainerView.isHidden = true
            }

       }
       else
       {
        
        if let pageData = appdelegate.cmsModelObject?.data?.Name {
            titleLabel.text = pageData.title1
            descLabel.text = pageData.desc1
            nextBtn.setTitle(pageData.button1, for: .normal)
        }else {
            titleLabel.text = "Let's get started!"
            descLabel.text = "Harmoney is a fun place where you can get rewarded doing the good stuff you do everyday\n\nSo, how do I call you?"
            nextBtn.titleLabel?.text  = "Start"
            nextBtn.setTitle("Start", for: .normal)
            self.enterSchoolNameContainerView.isHidden = true
        }
       }
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    @IBAction func StartBtnAction(_ sender: Any) {
//        pushToHarmoneyBucksVC()
//        return
        
        guard location else {
            proceedToCategoryView()
            return
        }
        guard inputtextField.text?.isBlank == false else {
            return view.toast("Enter zip code")
        }
        guard inputtextField.text?.count == 5 else {
            return view.toast("Enter valid zip code")
        }
//
        let harmoneyBucksConfirmationVC = GlobalStoryBoard().harmoneyBucksVC
        harmoneyBucksConfirmationVC.username = username
        harmoneyBucksConfirmationVC.categoryChoosing = categoryChoosing
        harmoneyBucksConfirmationVC.pinCode = inputtextField.text ?? "62775"
        harmoneyBucksConfirmationVC.guid = guid
        harmoneyBucksConfirmationVC.schoolName = "A"
        navigationController?.pushViewController(harmoneyBucksConfirmationVC, animated: true)
    }

    private func pushToHarmoneyBucksVC(){
        guard inputtextField.text?.isBlank == false else {
            view.toast("Enter name")
            return
        }
        let harmoneyBucksConfirmationVC = GlobalStoryBoard().harmoneyBucksVC
        harmoneyBucksConfirmationVC.username = inputtextField.text!
        harmoneyBucksConfirmationVC.categoryChoosing = "2"
        harmoneyBucksConfirmationVC.pinCode = ""
        harmoneyBucksConfirmationVC.guid = guid
        harmoneyBucksConfirmationVC.schoolName = "A"
        navigationController?.pushViewController(harmoneyBucksConfirmationVC, animated: true)
    }
    private func proceedToCategoryView() {
        guard inputtextField.text?.isBlank == false else {
            view.toast("Enter name")
            return
        }
        let categoryViewController = GlobalStoryBoard().categoryVC
        categoryViewController.username = inputtextField.text!
        categoryViewController.guid = guid
        navigationController?.pushViewController(categoryViewController, animated: true)
    }

        
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.placeholder == "Your name" {
            guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
               let substringToReplace = textFieldText[rangeOfTextToReplace]
               let count = textFieldText.count - substringToReplace.count + string.count
           
            do {
               let regex = try NSRegularExpression(pattern: "[^A-Za-z ]", options: [])
               if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                   return false
               }
           }
           catch {
               print("ERROR")
           }
            
            return count <= 25
            
        }
        
        guard location else {
            return true
        }
        guard let text = textField.text else {
            return false
        }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = formattedNumber(number: newString)
        return false
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXXXX"

        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}
