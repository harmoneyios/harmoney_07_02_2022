//
//  CategoryViewController.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var kidsBtn: UIButton!
    @IBOutlet weak var parentBtn: UIButton!
    @IBOutlet weak var adultBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!

    var username = String()
    var chooseCategory = false
    var categoryChoosing = String()
    var guid: String!
    var selectedTag: Int = 0
    var userTypeData : UserType?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setupInitial()
        setContent()
    }

    func setupPlaid() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveNotification(_:)), name: NSNotification.Name(rawValue: "PLDPlaidLinkSetupFinished"), object: nil)

    }
    
    @objc func didReceiveNotification(_ notification: NSNotification) {
        if notification.name.rawValue == "PLDPlaidLinkSetupFinished" {
            NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        }
    }

    func setContent() {
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        if let pageData = appdelegate.cmsModelObject?.data?.UserType {
            titleLabel.text = "Hi " + username + "!"
            descLabel.text = pageData.desc1
            kidsBtn.setTitle("Student",for: .normal)
            parentBtn.setTitle("Parent/Adult",for: .normal)
           // kidsBtn.setTitle(pageData.button1,for: .normal)
            //parentBtn.setTitle(pageData.button2,for: .normal)
            adultBtn.setTitle(pageData.button3,for: .normal)
            nextBtn.setTitle(pageData.button4,for: .normal)
        }else{
            descLabel.text = "How would you like to use this app?"
            nextBtn.setTitle("Next",for: .normal)

        }
    }
    func setupInitial() {
        nextBtn.backgroundColor = ConstantString.buttonBackgroundColor
        nextBtn.titleLabel?.font = ConstantString.buttonTitleFont
        nextBtn.setTitleColor(ConstantString.buttontextColor, for: .normal)

        titleLabel.font = ConstantString.titleFont
        descLabel.font = ConstantString.descriptionFont
         
        titleLabel.textColor = ConstantString.textColor
        descLabel.textColor = ConstantString.textColor
        
        kidsBtn.layer.borderColor = ConstantString.buttonSelectedBackgroundColor.cgColor
        parentBtn.layer.borderColor = ConstantString.buttonSelectedBackgroundColor.cgColor
        adultBtn.layer.borderColor = ConstantString.buttonSelectedBackgroundColor.cgColor
        
        kidsBtn.layer.borderWidth = 1
        parentBtn.layer.borderWidth = 1
        adultBtn.layer.borderWidth = 1

        adultBtn.titleLabel?.font = ConstantString.buttonTitleFont
        adultBtn.setTitleColor(ConstantString.textColor, for: .normal)
        
        kidsBtn.titleLabel?.font = ConstantString.buttonTitleFont
        kidsBtn.setTitleColor(ConstantString.textColor, for: .normal)

        parentBtn.titleLabel?.font = ConstantString.buttonTitleFont
        parentBtn.setTitleColor(ConstantString.textColor, for: .normal)

        kidsBtn.layer.cornerRadius  = 25
        parentBtn.layer.cornerRadius  = 25
        adultBtn.layer.cornerRadius  = 25
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }


    @IBAction func categoryBtnAction(_ sender: UIButton) {
        
        chooseCategory = true
        categoryChoosing = String(sender.tag + 1)
        selectedTag = sender.tag
        
        switch sender.tag {
        case 0:
            kidsBtn.backgroundColor = ConstantString.buttonSelectedBackgroundColor
            parentBtn.backgroundColor = UIColor.clear
            adultBtn.backgroundColor = UIColor.clear
            kidsBtn.setTitleColor(ConstantString.buttontextColor, for: .normal)
            parentBtn.setTitleColor(ConstantString.textColor, for: .normal)
            adultBtn.setTitleColor(ConstantString.textColor, for: .normal)

            break
        case 1:
            kidsBtn.backgroundColor = UIColor.clear
            parentBtn.backgroundColor = ConstantString.buttonSelectedBackgroundColor
            adultBtn.backgroundColor = UIColor.clear
            kidsBtn.setTitleColor(ConstantString.textColor, for: .normal)
            parentBtn.setTitleColor(ConstantString.buttontextColor, for: .normal)
            adultBtn.setTitleColor(ConstantString.textColor, for: .normal)

            break
          
        default:
            kidsBtn.backgroundColor = UIColor.clear
            parentBtn.backgroundColor = UIColor.clear
            adultBtn.backgroundColor = ConstantString.buttonSelectedBackgroundColor
            kidsBtn.setTitleColor(ConstantString.textColor, for: .normal)
            parentBtn.setTitleColor(ConstantString.textColor, for: .normal)
            adultBtn.setTitleColor(ConstantString.buttontextColor, for: .normal)

            break
        }
        
    }
    @IBAction func nextBtnAction(_ sender: UIButton) {
        guard (chooseCategory) else {
            view.toast("Choose category")
            return
        }
        navigateToProfileCreation()
    }
    
    func navigateToProfileCreation() {
        //Zipcode viewcontroller hidden
        
        let harmoneyBucksConfirmationVC = GlobalStoryBoard().harmoneyBucksVC
        harmoneyBucksConfirmationVC.username = username
        harmoneyBucksConfirmationVC.categoryChoosing = categoryChoosing
      //  harmoneyBucksConfirmationVC.pinCode = inputtextField.text ?? "62775"
        harmoneyBucksConfirmationVC.guid = guid
        harmoneyBucksConfirmationVC.schoolName = "A"
        navigationController?.pushViewController(harmoneyBucksConfirmationVC, animated: true)
        
       /* let profileCreation = GlobalStoryBoard().inputVC
            profileCreation.location = true //hide zipcode
            profileCreation.username = username
            profileCreation.categoryChoosing = categoryChoosing
            profileCreation.guid = guid
        self.navigationController?.pushViewController(profileCreation, animated: true)*/
    }
}
