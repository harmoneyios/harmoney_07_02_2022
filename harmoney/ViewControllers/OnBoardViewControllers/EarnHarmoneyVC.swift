//
//  EarnHarmoneyVC.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Toast_Swift
import Lottie
import SVProgressHUD

class EarnHarmoneyVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var earnLabel: UILabel!
    @IBOutlet weak var harmoneyLabel: UILabel!

    @IBOutlet weak var startBtn: UIButton!
    var username = String()
    var categoryChoosing = String()
    var pinCode = String()
    var guid: String!
    var allowPushNotifications = false
    var parentLinkPayment = false
    var isFromEndGoalVC = false
    var schoolName = String()

    @IBOutlet weak var animatetdbaseView: UIView!

    var animationView =  AnimationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true

        setupInitial()
        setContent()
        
        self.view.addSubview(animationView)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.leadingAnchor.constraint(equalTo: animatetdbaseView.leadingAnchor,constant: 0).isActive = true
        animationView.trailingAnchor.constraint(equalTo: animatetdbaseView.trailingAnchor,constant: 0).isActive = true
        
        animationView.topAnchor.constraint(equalTo: animatetdbaseView.topAnchor,constant: 0).isActive = true
        animationView.bottomAnchor.constraint(equalTo: animatetdbaseView.bottomAnchor,constant: 0).isActive = true

        let path = Bundle.main.path(forResource: "successAnimation",
                                    ofType: "json") ?? ""
        animationView.animation = Animation.filepath(path)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.isUserInteractionEnabled = false
        animationView.play()
        self.navigationController?.navigationBar.isHidden = true

    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
    }
    
    func setContent()  {
         let appdelegate = UIApplication.shared.delegate as! AppDelegate
        if let pageData = appdelegate.cmsModelObject?.data?.Congrats{
            titleLabel.text = pageData.title1
            earnLabel.text = pageData.subTitle1
            descLabel.text = "Complete your first challenge to earn more rewards."
            startBtn .setTitle(pageData.button1, for: .normal)
        }
                          
     }
    func setupInitial() {
        //self.webservice()
        startBtn.backgroundColor = ConstantString.buttonBackgroundColor
        startBtn.titleLabel?.font = ConstantString.buttonTitleFont

         titleLabel.font = ConstantString.titleFont
         descLabel.font = ConstantString.descriptionFont
         
        
        titleLabel.text = "CONGRATULATIONS!"

        descLabel.text = "Complete your first challenge to earn more rewards."
        

    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    @IBAction func StartBtnAction(_ sender: Any){

        
                if Reachability.isConnectedToNetwork(){
                    var data = [String : Any]()
                    data.updateValue(guid, forKey: "guid")
                    data.updateValue(getEncryptedString(plainText: username), forKey: "name")
                    data.updateValue("", forKey: "email")
                    data.updateValue(pinCode, forKey: "zip")
                    data.updateValue(categoryChoosing, forKey: "userType")
                    data.updateValue("", forKey: "schoolId")
                    data.updateValue(self.schoolName, forKey: "schoolName")
                    data.updateValue(allowPushNotifications, forKey: "allowPushNotifications")
                    data.updateValue(parentLinkPayment, forKey: "parentLinkPayment")
                    
                    
                    let headers: HTTPHeaders = [
                        "Authorization": UserDefaultConstants().sessionToken ?? ""
                       
                    ]
                    print("Authorization: \(headers)")
                    UserDefaultConstants().choiceCategory = Int(categoryChoosing) ?? 1
//                    Defaults.saveChoiceCategory(Int(categoryChoosing) ?? 1)
                    let (url, method, param) = APIHandler().profile(params: data)

                    SVProgressHUD.show()

                    AF.request(url, method: method, parameters: param, encoding: JSONEncoding.default,headers: headers).validate().responseJSON { response in
                        switch response.result {
                        case .success(let value):
                            if  let value = value as? [String:Any] {


                                 if let status = value["status"] as? NSNumber {
                                    
                                    if (status == 200)
                                    {
                                        UserDefaultConstants().login = true
                                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                        appdelegate.presentDailyViewController()

                                    }
                                }

                                if let message = value["message"] as? String
                                 {
                                    self.view.toast(message)
                                 }

                                if let data = value["data"] as? [String:Any] {
                                    print(data)

                                    //
//                                    self.PushToProfileCreationView()

                                }

                            }
                            SVProgressHUD.dismiss()

                        case .failure(let error):
                            print(error)
                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                        }
                    }
                }else{
                    self.view.toast("Internet Connection not Available!")
                }
            }
    
    
   
}
