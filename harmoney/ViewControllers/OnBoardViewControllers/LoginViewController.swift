//
//  LoginViewController.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit
import Foundation
import UIKit
import TextFieldEffects
import Alamofire
import SVProgressHUD

class LoginViewController: UIViewController,UITextFieldDelegate,TermsAndConditionsUpdateDelegate {
    
    //Outlet
    @IBOutlet weak var viewForTextField: UIView!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var termAndConditionLabel: UILabel!
    @IBOutlet weak var bottomTermsAndConditionLabel: NSLayoutConstraint!
    @IBOutlet weak var byProceedingLabel: UILabel!
    @IBOutlet weak var signupView: UIView!
    
    
    @IBOutlet weak var btnCheckBox: UIButton!
    
    @IBOutlet weak var btnloginBottomConstraints: NSLayoutConstraint!
    var mobileNumber: String!
    var schools = [Any]()
    var runfor = [Any]()
    var isUserTappedTermsAndConditions = false
    var guid: String?
    var otp = String()
    var loginData : Login?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setupInitial()
        setContent()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCheckBox.isSelected = false
        loginBtn.isEnabled = false
        
        if let mobileNumber = UserDefaultConstants().userPhoneNumber {
            mobileTextField.text = formattedNumber(number: mobileNumber.last ?? "")
        }
        mobileTextField.becomeFirstResponder()
        self.navigationController?.isNavigationBarHidden = true
        
        mobileTextField.keyboardDistanceFromTextField = 100
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        btnloginBottomConstraints.constant = keyboardSize.height - 30
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        btnloginBottomConstraints.constant = 50
    }

    @objc func tappedLabel(tapGestureRecognizer: UITapGestureRecognizer) {
        self.isUserTappedTermsAndConditions = true
        self.navigateToTermandCondition()
    }

    func navigateToTermandCondition() {
     
        let terms = GlobalStoryBoard().termsVC
        terms.isUserTappedTermsAndConditions = self.isUserTappedTermsAndConditions
        terms.modalPresentationStyle = .fullScreen
        
        terms.delegate = self as TermsAndConditionsUpdateDelegate
        self.present(terms, animated: true, completion: nil)
        self.isUserTappedTermsAndConditions = false
    }
    
    func setContent()  {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        if let pageData = appdelegate.cmsModelObject?.data?.Login{
            titleLabel.text = pageData.title1! + "\n" + pageData.subTitle1!
            descLabel.text = pageData.desc1
            loginBtn .setTitle(pageData.button1, for: .normal)
           // byProceedingLabel.text =  pageData.footer1
            termAndConditionLabel.text =  pageData.footer2
            
        }else {
            titleLabel.text = "Hey there!\nWelcome to Harmoney!"
            descLabel.text = "Your mobile number is important\nto earning rewarding Harmoney Bucks.\nPlease enter your mobile number"
        }
        
        descLabel.text = "Please enter your mobile number to \n earn & share harmoney bucks"
    }
    func setupInitial() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedLabel(tapGestureRecognizer:)))
        termAndConditionLabel.isUserInteractionEnabled = true
        termAndConditionLabel.addGestureRecognizer(tapGesture)
        
        mobileTextField.delegate = self
       
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: "StringWithUnderLine", attributes: underlineAttribute)
       termAndConditionLabel.attributedText = underlineAttributedString
        loginBtn.titleLabel?.font = ConstantString.buttonTitleFont
        loginBtn.titleLabel?.textColor = ConstantString.buttontextColor
        
        titleLabel.font = ConstantString.titleFont
        descLabel.font = ConstantString.descriptionFont
        
        titleLabel.textColor = ConstantString.textColor
        descLabel.textColor = ConstantString.textColor
        
    }
    
    @IBAction func onClickBtnChkboc(_ sender: Any) {
        btnCheckBox.isSelected.toggle()
        if btnCheckBox.isSelected == true{
            loginBtn.isEnabled = true
            loginBtn.backgroundColor = ConstantString.buttonBackgroundColor
        }
        if btnCheckBox.isSelected == false
        {
            loginBtn.isEnabled = false
            loginBtn.backgroundColor = UIColor.systemGray
        }
    }
    
    @IBAction func login(_ sender: Any) {
        
        mobileTextField.resignFirstResponder()
        if Reachability.isConnectedToNetwork(){
            guard !(mobileTextField.text?.isBlank)! else {
                return self.view.toast("Enter mobile number")
            }
            guard (mobileTextField.text?.count == 12) else {
                return self.view.toast("Enter valid mobile number")
            }
        }else{
            self.view.toast("Internet Connection not Available!")
            return
        }
        if loginBtn.isEnabled == true{
        otpPage()
        }
        //loginFunctionality()
        //self.navigateToTermandCondition()
    }
    

    func otpPage() {
        mobileTextField.resignFirstResponder()
        guard Reachability.isConnectedToNetwork() else {
            self.view.toast("Internet Connection not Available!")
            return
        }
        guard !(mobileTextField.text?.isBlank)! else {
            view.toast("Enter mobile number")
            return
        }
        guard (mobileTextField.text?.count == 12) else {
            view.toast("Enter valid mobile number")
            return
        }
      
               let strings = mobileTextField.text
               let trimmedString = strings!.replacingOccurrences(of: " ", with: "")
               mobileNumber = trimmedString
               loginFunctionality()
    }
    
    
    func loginFunctionality() {
//
//
//             let strings = mobileNumber
//             let trimmedString = strings!.replacingOccurrences(of: " ", with: "")
//             let data = ["phone": trimmedString]
//             var (url, method, param) = APIHandler().getOTP(params: data)
//             url = url + "/" + trimmedString
//
//             Alamofire.request(url, method: method, parameters: param).validate().responseJSON { response in
//                 switch response.result {
//                     case .success(let value):
//                         if let value = value as? [String:Any] {
//                             if let sucessMessage = value["sucMsg"] as? [String:Any], let message = sucessMessage["message"] {
//                                        print(message)
//                                 }
//                             if let data = value["data"] as? [String:Any] {
//                                 print(data)
//
//                                 if let guid = data["guid"] as? String {
//                                     self.guid = guid
//                                    UserDefaultConstants().guid = self.guid
//                                 }
//                                if let profile = data["firstTimeUser"] as? NSNumber  {
//                                if profile == 1 {
//                                //self.navigateToTermandCondition()
//
//
//                                }else{
                                   self.moveToOtpScreen()
//                                }
//
//                             }
//                            }
//                             if let sucessMessage = value["errMsg"] as? [String:Any], let message = sucessMessage["message"] {
//                                 self.view.toast(message as! String)
//                             }
//                            }
//                     case .failure(let error):
//                         print(error)
//                 }
//             }

         }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = formattedNumber(number: newString)
        return false
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXX-XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func updateTermsAndConditions() {
    moveToOtpScreen()
       // self.otpPage()
    }
    
    func moveToOtpScreen(){
        let otpView = GlobalStoryBoard().otpVC
         otpView.guid = guid
                otpView.comeFrom = "SignUp"
                otpView.schools = schools
                otpView.runfor = runfor
                otpView.otp = otp
                let strings = mobileTextField.text
                let trimmedString = strings!.replacingOccurrences(of: " ", with: "")
                otpView.mobileNumber = trimmedString
                self.navigationController?.pushViewController(otpView, animated: true)
        
        
    }
    
}

