//
//  SplashVC.swift
//  Harmoney
//
//  Created by Prema Ravikumar on 19/12/19.
//  Copyright © 2019 Prema Ravikumar. All rights reserved.
//
import UIKit
import Foundation
class SplashScreenVC: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate{
    
    var pageIndex = 0

    @IBOutlet weak var parentScrollView: UIScrollView!
    @IBOutlet weak var getdashboardButton: UIButton!

    @IBOutlet weak var stepOneTitleLabel: UILabel!
    @IBOutlet weak var stepOneDescLabel: UILabel!
    @IBOutlet weak var stepTwoTitleLabel: UILabel!
    @IBOutlet weak var stepTwoDescLabel: UILabel!
    @IBOutlet weak var stepThreeTitleLabel: UILabel!
    @IBOutlet weak var stepThreeDescLabel: UILabel!
    @IBOutlet weak var tapView: UIView!

    @IBOutlet weak var bottomOneConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomTwoConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomThreeConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageControl1: UIImageView!
    @IBOutlet weak var pageControl2: UIImageView!
    @IBOutlet weak var pageControl3: UIImageView!

    private var pageScrollTimer: Timer!
    private let isUserAlreadyLoggedIn: Bool = UserDefaultConstants().login ?? false

    override func viewDidLoad() {
        super.viewDidLoad()

        if isUserAlreadyLoggedIn {
            if let userCategory = UserDefaultConstants().choiceCategory {
                pageIndex = userCategory - 1
            }
        }
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: Selector(("respondToSwipeGesture:")))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        swipeRight.delegate = self

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: Selector(("respondToSwipeGesture:")))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        swipeLeft.delegate = self

        setupInitial()

        guard !isUserAlreadyLoggedIn else {
            return
        }

        tapView.addGestureRecognizer(swipeRight)
        tapView.addGestureRecognizer(swipeLeft)
    }

    func setupInitial() {
        stepOneTitleLabel.font = ConstantString.titleFont
        stepOneDescLabel.font = ConstantString.descriptionFont
        stepTwoTitleLabel.font = ConstantString.titleFont
        stepTwoDescLabel.font = ConstantString.descriptionFont
        stepThreeTitleLabel.font = ConstantString.titleFont
        stepThreeDescLabel.font = ConstantString.descriptionFont

        stepOneTitleLabel.textColor = ConstantString.textColor
        stepOneDescLabel.textColor = ConstantString.textColor
        stepTwoTitleLabel.textColor = ConstantString.textColor
        stepTwoDescLabel.textColor = ConstantString.textColor
        stepThreeTitleLabel.textColor = ConstantString.textColor
        stepThreeDescLabel.textColor = ConstantString.textColor


        stepOneTitleLabel.text = "Never have enough money\nto keep up with the trends?"
        stepOneDescLabel.text = "Use Harmoney bucks\nto buy cool stuff everyday"
        stepTwoTitleLabel.text = "Parents - Challenge your kids"
        stepTwoDescLabel.text = "Nudge your kids in the right direction\nby rewarding them"
        stepThreeTitleLabel.text = "All that you do everyday\nis valuable!"
        stepThreeDescLabel.text = "Earn Harmoney bucks\nby doing what you do everyday"
    }


    @objc func moveToHomeScreen() {
        pageScrollTimer.invalidate()
        (UIApplication.shared.delegate as? AppDelegate)?.initialView()
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.stepOneTitleLabel.alpha = 0.3
        self.stepOneDescLabel.alpha = 0.3
        self.stepTwoTitleLabel.alpha = 0.3
        self.stepTwoDescLabel.alpha = 0.3
        self.stepThreeTitleLabel.alpha = 0.3
        self.stepThreeDescLabel.alpha = 0.3
        self.getdashboardButton.alpha = 0.3

        guard !isUserAlreadyLoggedIn else {
            DispatchQueue.main.async {
                self.showSplashForUser()
            }
            return
        }
        self.fadeIn(completion: { (finished: Bool) -> Void in
        })

        UIView.animate(withDuration: 0.7, animations: { () -> Void in
            self.bottomOneConstraint.constant = 60
            self.view.layoutIfNeeded()
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isUserAlreadyLoggedIn {
            parentScrollView.isScrollEnabled = false
            pageScrollTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(moveToHomeScreen), userInfo: nil, repeats: false)
        } else {
            pageScrollTimer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        }
    }

    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if gesture.isKind(of: UISwipeGestureRecognizer.self){        
            switch gesture.state {
            case .cancelled, .ended, .failed:
                pageScrollTimer?.invalidate()
            default:
                print("Gesture in progress")
            }
        }
    }

    @objc func showSplashForUser() {
        let counter = CGFloat(pageIndex)
        let pageWidth = parentScrollView.frame.width
        let minX = counter * pageWidth
        let scrollRect = CGRect(x: minX, y: 0, width: pageWidth, height: parentScrollView.frame.height)
        parentScrollView.scrollRectToVisible(scrollRect, animated: true)
        fadeIn(0.0, delay: 0.1) { (success) in
            UIView.animate(withDuration: 0.1) {
                if self.pageIndex == 0 {
                    self.pageControl1.isHidden = true
                    self.bottomOneConstraint.constant = 20
                } else if self.pageIndex == 1 {
                    self.pageControl2.isHidden = true
                    self.bottomTwoConstraint.constant = 20
                } else {
                    self.pageControl3.isHidden = true
                    self.bottomThreeConstraint.constant = 0
                }
            }
        }
    }
   
    @objc func moveToNextPage() {
            
        let pageWidth:CGFloat = self.parentScrollView.frame.width
        let maxWidth:CGFloat = parentScrollView.frame.width * 3
        let contentOffset:CGFloat = self.parentScrollView.contentOffset.x

        let slideToX = contentOffset + pageWidth

        if  contentOffset + pageWidth == maxWidth {
            pageScrollTimer?.invalidate()
        }
        let rect = CGRect(x:slideToX, y:0, width:pageWidth, height: parentScrollView.frame.height)
        self.parentScrollView.scrollRectToVisible(rect, animated: true)
        pageIndex += 1
        self.fadeIn(completion: { (finished: Bool) -> Void in

        })
        if pageIndex == 1 {
            UIView.animate(withDuration: 0.8, animations: { () -> Void in
                self.bottomTwoConstraint.constant = 60
                self.view.layoutIfNeeded()
            })
        } else if pageIndex == 2 {
            UIView.animate(withDuration: 0.8, animations: { () -> Void in
                self.bottomThreeConstraint.constant = 20
                self.view.layoutIfNeeded()
                let when = DispatchTime.now() + 3
                DispatchQueue.main.asyncAfter(deadline: when) {
                    //self.nextBtnAction(self)
                }
            })
        }
       
    }
    
    func fadeIn(_ duration: TimeInterval = 0.0, delay: TimeInterval = 0.7, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.transitionFlipFromTop, animations: {
            if self.pageIndex == 0 {
                self.stepOneTitleLabel.alpha = 1.0
                self.stepOneDescLabel.alpha = 1.0
            } else if self.pageIndex == 1 {
                self.stepTwoTitleLabel.alpha = 1.0
                self.stepTwoDescLabel.alpha = 1.0
            } else if self.pageIndex == 2 {
                self.stepThreeTitleLabel.alpha = 1.0
                self.stepThreeDescLabel.alpha = 1.0
                self.getdashboardButton.alpha = 1.0
            }
     }, completion: completion)  }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageIndex = Int(round(scrollView.contentOffset.x / scrollView.frame.size.width))
        
        pageScrollTimer.invalidate()
        self.pageIndex += 1
        self.fadeIn(completion: {
             (finished: Bool) -> Void in
            })

        if pageIndex == 1 {
            UIView.animate(withDuration: 0.7, animations: { () -> Void in
                self.bottomTwoConstraint.constant = 60
                self.view.layoutIfNeeded()
                self.pageScrollTimer.fire()
            })
        } else if pageIndex == 2 {
            UIView.animate(withDuration: 0.7, animations: { () -> Void in
                self.bottomTwoConstraint.constant = 60
                self.stepTwoTitleLabel.alpha = 1.0
                self.stepTwoDescLabel.alpha = 1.0

                self.bottomThreeConstraint.constant = 20
                self.view.layoutIfNeeded()
                self.pageScrollTimer.fire()
            })
        } else if pageIndex == 3 {
          //  nextBtnAction(self)
        }
    }

    @IBAction func nextBtnAction(_ sender: Any) {        
        UserDefaultConstants().launchedBefore = true
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.initialView()
        
    }

    @IBAction func dismissBtnAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func onClickbtnGetStarted(_ sender: Any) {
        nextBtnAction(self)
    }
}

