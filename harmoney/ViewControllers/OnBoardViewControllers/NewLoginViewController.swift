//
//  NewLoginViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/7/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class NewLoginViewController: UIViewController {
    @IBOutlet weak var mobileNumberTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            mobileNumberTextField.leftView = paddingView
            mobileNumberTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 14)
            ]

            mobileNumberTextField.attributedPlaceholder = NSAttributedString(string: "Enter your mobile number", attributes:attributes)
        }
    }
    
//MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configuteUI()
        
        if let mobileNumber = UserDefaultConstants().newuserPhoneNumber {
            mobileNumberTextField.text = formattedNumber(number: mobileNumber.last ?? "")
        }
        mobileNumberTextField.becomeFirstResponder()
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXX-XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: UI Methods
extension NewLoginViewController {
    func configuteUI() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func presentLoginViewController() {
        UserDefaultConstants().guid = nil
        
        let loginViewController = GlobalStoryBoard().splashVC
        self.navigationController?.pushViewController(loginViewController, animated: true)
    }
    
    func presentOtpViewController() {
        let otpViewController = GlobalStoryBoard().otpVC
        let strings = mobileNumberTextField.text
        let trimmedString = strings!.replacingOccurrences(of: " ", with: "")
        otpViewController.mobileNumber = trimmedString
        
        self.navigationController?.pushViewController(otpViewController, animated: true)
    }
}

//MARK: Action Methods
extension NewLoginViewController {
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        if mobileNumberTextField.text?.count ?? 0 > 0 {
            guard (mobileNumberTextField.text?.count == 12) else {
                return self.view.toast("Enter valid mobile number")
            }
            self.presentOtpViewController()

        } else {
            view.toast("Enter your mobile number")
        }
    }
    
    @IBAction func letsGetStartedButtonTapped(_ sender: UIButton) {
        presentLoginViewController()
    }
}

//MARK: UITextFieldDelegate Methods
extension NewLoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumberTextField {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Utilities.formattedMobileNumber(number: newString)
            
            return false
        }
        return true
    }
}

