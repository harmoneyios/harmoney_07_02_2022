//
//  HarmoneyBucksConfirmationVC.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit

class HarmoneyBucksConfirmationVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var informationBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var nextBtn: UIButton!

    var username = String()
    var categoryChoosing = String()
    var pinCode = String()
    var guid: String!
    var schoolName = String()

    var shouldAddBankAccount: Bool {
        return self.categoryChoosing == "2" || self.categoryChoosing == "3"
    }
    
    func notificationAction() {
        proceedToGetHealthKitAccess(pushEnabled: true)
    }

    
    private func proceedToGetHealthKitAccess(pushEnabled: Bool) {
        let healthKitPermissionVC = GlobalStoryBoard().healthpermissionVC
        healthKitPermissionVC.username = username
        healthKitPermissionVC.categoryChoosing = categoryChoosing
        healthKitPermissionVC.pinCode = pinCode
        healthKitPermissionVC.guid = guid
        healthKitPermissionVC.schoolName = "A"
        healthKitPermissionVC.isPushEnabled = pushEnabled
        navigationController?.pushViewController(healthKitPermissionVC, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setupInitial()
        setContent()
    }
    
    func setupInitial() {
         
         nextBtn.backgroundColor = ConstantString.buttonBackgroundColor
         nextBtn.titleLabel?.font = ConstantString.buttonTitleFont
         nextBtn.setTitleColor(ConstantString.buttontextColor, for: .normal)

         titleLabel.font = ConstantString.titleFont
         descLabel.font = ConstantString.descriptionFont
         
         titleLabel.textColor = ConstantString.textColor
         descLabel.textColor = ConstantString.textColor
        
        informationBtn.setTitle("No way !\nI don't want Harmoney bucks", for: .normal)

        informationBtn.layer.borderColor = ConstantString.borderColor.cgColor
        informationBtn.layer.borderWidth = 1

    }
    
    func setContent()  {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
        if let pageData = appdelegate.cmsModelObject?.data?.Notification {
                titleLabel.text = pageData.title1
                descLabel.text = pageData.desc1
                nextBtn .setTitle(pageData.button1, for: .normal)
                informationBtn.setTitle(pageData.notificationText, for: .normal)
            }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }


    @IBAction func informationBtnClick(_ sender: Any) {
        proceedToGetHealthKitAccess(pushEnabled: false)
    }
    
    @IBAction func StartBtnAction(_ sender: Any) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.registerForPushNotifications(content: self)
        }
    }
}
