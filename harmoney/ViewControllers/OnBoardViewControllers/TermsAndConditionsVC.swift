//
//  TermsAndConditionsVC.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit


protocol TermsAndConditionsUpdateDelegate {
    func updateTermsAndConditions()
}

class TermsAndConditionsVC: UIViewController {

    var comefrom = String()
    var delegate: TermsAndConditionsUpdateDelegate?
    var isUserTappedTermsAndConditions = Bool()

    @IBAction func GotItBtnAction(_ sender: Any) {
        if !self.isUserTappedTermsAndConditions{
          self.delegate?.updateTermsAndConditions()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func HomeBtnAction(_ sender: Any) {
        
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.setupSplash()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func sideMenuBtnAction(_ sender: Any) {
        
        if comefrom == "Profile"
        {
            self.navigationController?.popViewController(animated: true)
        }
    }

}
