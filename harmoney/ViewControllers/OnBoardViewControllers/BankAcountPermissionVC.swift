//
//  BankAcountPermissionVC.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit
import Foundation
import LinkKit
import SVProgressHUD
import  Alamofire

class BankAcountPermissionVC: UIViewController {

    @IBOutlet weak var bankcDescLabel: UILabel!
    var username = String()
    var categoryChoosing = String()
    var guid: String!
    var allowPushNotifications = false
    var parentLinkPayment = false
    var parentLinkPaymentSuccess = false
    var zipCode = String()
    var data = [String : Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if categoryChoosing == "2"{
            self.bankcDescLabel.text = "You and your kids can earn Harmoney bucks as you complete each challange"
        }else if categoryChoosing == "3"{
            self.bankcDescLabel.text = "Link your bank account to earn and spend Harmoney bucks once a challenge has been completed"
        }
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func procededBtnAction(_ sender: Any) {
        //depricated Framework tool
     //   presentPlaidLinkWithCustomConfiguration()
    }

    @IBAction func skipBtnAction(_ sender: Any) {
        parentLinkPayment = false
        pushToCongratualtion()
    }
    
    func pushToCongratualtion()
    {
     

        let profileCreation = GlobalStoryBoard().earnHarmoneyVC
        
        profileCreation.username = self.username
        profileCreation.categoryChoosing = self.categoryChoosing
        profileCreation.pinCode = self.zipCode
        profileCreation.schoolName = ""
        profileCreation.guid = self.guid
        profileCreation.allowPushNotifications = self.allowPushNotifications
        profileCreation.parentLinkPayment = self.parentLinkPayment
        self.navigationController?.pushViewController(profileCreation, animated: true)

        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let ProfileCreation = storyboard.instantiateViewController(withIdentifier: "CongratualtionViewController") as! CongratualtionViewController
//            ProfileCreation.username = self.username
//            ProfileCreation.categoryChoosing = self.categoryChoosing
//            ProfileCreation.pinCode = self.zipCode
//            ProfileCreation.guid = self.guid
//            ProfileCreation.allowPushNotifications = false
//            ProfileCreation.parentLinkPayment = parentLinkPayment
//            self.navigationController?.pushViewController(ProfileCreation, animated: true)
    }
    
    func handleSuccessWithToken(_ publicToken: String, metadata: [String : Any]?) {
            parentLinkPaymentSuccess = true
            presentAlertViewWithTitle("Payment success", message: "Your payment successfull")

//            presentAlertViewWithTitle("Success", message: "token: \(publicToken)\nmetadata: \(metadata ?? [:])")
          data.updateValue(true, forKey: "parentLinkPayment")

        if let account_id = metadata?["account_id"] as? String {
             data.updateValue(account_id, forKey: "bankAccountNumber")
         }
        if let institution = metadata?["institution"] as? [String : Any] {
             if let name = institution["name"] as? String {
                 data.updateValue(name, forKey: "bankName")

             }
         }
        
         data.updateValue(metadata, forKey: "bankLinkResponse")
         data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
         updateUserProfile()

        }
    
    func updateUserProfile() {
            
            if Reachability.isConnectedToNetwork() {
                SVProgressHUD.show()
                let (url, method, param) = APIHandler().updateProfile(params: data)
                let headers: HTTPHeaders = [
                    "Authorization": UserDefaultConstants().sessionToken ?? ""
                   
                ]
                AF.request(url, method: method, parameters: param, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
                    switch response.result {

                    case .success(let value):
                        if  value is [String:Any] {
                        
                        }
                    case .failure(let error):
                        print(error)
                    }
                    SVProgressHUD.dismiss()
                }
            } else {
                SVProgressHUD.dismiss()
            }
            
        }

        func handleError(_ error: Error, metadata: [String : Any]?) {
            
            presentAlertViewWithTitle("Payment failure", message: "Your payment failure")

    //        presentAlertViewWithTitle("Failure", message: "error: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
        }
        
        func handleExitWithMetadata(_ metadata: [String : Any]?) {
           // presentAlertViewWithTitle("Payment Cancel", message: "You have cancelled your payment")
        }

    func presentAlertViewWithTitle(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            if self.parentLinkPaymentSuccess
            {
                self.parentLinkPayment = true
                self.pushToCongratualtion()
            }
        }

        alert.addAction(OKAction)
        present(alert, animated: true, completion: nil)
    }
 /*   //depricated Framework tool
    // MARK: Plaid Link setup with shared configuration from Info.plist
    func presentPlaidLinkWithSharedConfiguration() {
        // <!-- SMARTDOWN_PRESENT_SHARED -->
        // With shared configuration from Info.plist
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet
        }
        present(linkViewController, animated: true)
        // <!-- SMARTDOWN_PRESENT_SHARED -->
    }

    // MARK: Plaid Link setup with custom configuration
    func presentPlaidLinkWithCustomConfiguration() {
        // <!-- SMARTDOWN_PRESENT_CUSTOM -->
        // With custom configuration
        let linkConfiguration = PLKConfiguration(key: "adced9ed28e0da390c03521ee07faf", env: .sandbox, product: .auth)
        linkConfiguration.clientName = "Harmoney Inc"
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(configuration: linkConfiguration, delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet
        }
        present(linkViewController, animated: true)
        // <!-- SMARTDOWN_PRESENT_CUSTOM -->
    }

    // MARK: Start Plaid Link in update mode
    func presentPlaidLinkInUpdateMode() {
        // <!-- SMARTDOWN_UPDATE_MODE -->
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(publicToken: "5e130922b1398b0011fa9e62", delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet
        }
        present(linkViewController, animated: true)
        // <!-- SMARTDOWN_UPDATE_MODE -->
    }
    */

}
/* depricated Framework tool
// MARK: - PLKPlaidLinkViewDelegate Protocol
// <!-- SMARTDOWN_PROTOCOL -->
extension BankAcountPermissionVC : PLKPlaidLinkViewDelegate
// <!-- SMARTDOWN_PROTOCOL -->
{

// <!-- SMARTDOWN_DELEGATE_SUCCESS -->
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didSucceedWithPublicToken publicToken: String, metadata: [String : Any]?) {
        dismiss(animated: true) {
            // Handle success, e.g. by storing publicToken with your service
            NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
            self.handleSuccessWithToken(publicToken, metadata: metadata)
        }
    }
// <!-- SMARTDOWN_DELEGATE_SUCCESS -->

// <!-- SMARTDOWN_DELEGATE_EXIT -->
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didExitWithError error: Error?, metadata: [String : Any]?) {
        dismiss(animated: true) {
            if let error = error {
                NSLog("Failed to link account due to: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
                self.handleError(error, metadata: metadata)
            }
            else {
                NSLog("Plaid link exited with metadata: \(metadata ?? [:])")
                self.handleExitWithMetadata(metadata)
            }
        }
    }
// <!-- SMARTDOWN_DELEGATE_EXIT -->
    
// <!-- SMARTDOWN_DELEGATE_EVENT -->
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didHandleEvent event: String, metadata: [String : Any]?) {
        NSLog("Link event: \(event)\nmetadata: \(metadata ?? [:])")
    }
// <!-- SMARTDOWN_DELEGATE_EVENT -->
}
*/
