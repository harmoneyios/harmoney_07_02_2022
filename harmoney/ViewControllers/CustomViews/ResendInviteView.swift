//
//  ResendInviteView.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 22/06/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import iOSDropDown

protocol ResendInviteViewDelegate: class {
    func didTapResendButton(resendInviteView: ResendInviteView, inviteData: InviteData)
    func closeResendView()

}
class ResendInviteView: UIView {
    
    var type: FamilyViewControllerType = .invite

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var fatherButton: UIButton!
    @IBOutlet weak var motherButton: UIButton!
    @IBOutlet weak var guardianButton: UIButton!
    @IBOutlet weak var dropDown: UIPickerView!
    var resendData: InviteData?
    @IBOutlet weak var parentNameTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            parentNameTextField.leftView = paddingView
            parentNameTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            parentNameTextField.attributedPlaceholder = NSAttributedString(string: "Enter parent name", attributes:attributes)
        }
    }
    
    @IBOutlet weak var mobileNumberTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            mobileNumberTextField.leftView = paddingView
            mobileNumberTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            mobileNumberTextField.attributedPlaceholder = NSAttributedString(string: "Enter mobile number", attributes:attributes)
        }
    }
    @IBOutlet weak var emailAddressTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            emailAddressTextField.leftView = paddingView
            emailAddressTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            emailAddressTextField.attributedPlaceholder = NSAttributedString(string: "Enter email address", attributes:attributes)
        }
    }
    
    @IBOutlet weak var relationshipTextField: DropDown!{
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            relationshipTextField.leftView = paddingView
            relationshipTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            relationshipTextField.attributedPlaceholder = NSAttributedString(string: "Select Relationship", attributes:attributes)
        }
    }
  
    
    var memberRelation: String? = ""
    weak var delegate: ResendInviteViewDelegate?
    
//MARK: - Lifecycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
        setupDropDowns()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("ResendInviteView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    func setupDropDowns() {
      
        relationshipTextField.optionArray = ["Mom","Dad","Guradian"]
        relationshipTextField.text = ""
        relationshipTextField.selectedRowColor =  #colorLiteral(red: 0.9294117647, green: 0.937254902, blue: 0.9529411765, alpha: 1)
        relationshipTextField.checkMarkEnabled = false
        relationshipTextField.arrowColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        relationshipTextField.isSearchEnable = false
        relationshipTextField.adjustsFontSizeToFitWidth = true
        relationshipTextField.minimumFontSize = 12
        relationshipTextField.didSelect { (string, int1, int2) in
            self.relationshipTextField.text = "\(string)"
            self.memberRelation = string
        }
        
    }
    
    func loadResendView() {
        parentNameTextField.text = resendData?.memberName
        emailAddressTextField.text = resendData?.memberEmail
        relationshipTextField.text = resendData?.memberRelation
        mobileNumberTextField.text = resendData?.memberMobile
        self.memberRelation = resendData?.memberRelation
    }
}

//MARK: UI Methods
extension ResendInviteView {
    func buttonStateSelected(button: UIButton) {
        button.backgroundColor = UIColor.harmoneyDarkBlueColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.futuraPTBookFont(size: 16)
        button.borderWidth = 0
        button.borderColor = UIColor.clear
    }
    
    func buttonStateNormal(button: UIButton) {
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5), for: .normal)
        button.titleLabel?.font = UIFont.futuraPTLightFont(size: 16)
        button.borderWidth = 1
        button.borderColor = UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5)
    }
}

//MARK: Action methods
extension ResendInviteView {
    @IBAction func fatherButtonTapped(_ sender: UIButton) {
        memberRelation = "Father"
        
        buttonStateSelected(button: sender)
        buttonStateNormal(button: motherButton)
        buttonStateNormal(button: guardianButton)
        self.endEditing(true)
    }
    
    @IBAction func motherButtonTapped(_ sender: UIButton) {
        memberRelation = "Mother"
        
        buttonStateSelected(button: sender)
        buttonStateNormal(button: fatherButton)
        buttonStateNormal(button: guardianButton)
        self.endEditing(true)

    }
    
    @IBAction func guardianButtonTapped(_ sender: UIButton) {
        memberRelation = "Guardian"
        
        buttonStateSelected(button: sender)
        buttonStateNormal(button: motherButton)
        buttonStateNormal(button: fatherButton)
        self.endEditing(true)

    }
    
    @IBAction func closeBtnAct(_ sender: UIButton) {
        delegate?.closeResendView()
    }
    
    @IBAction func sendInviteButtonTapped(_ sender: UIButton) {
        let enteredmobulenumber = AddKidCustomView().formattedNumber(number: mobileNumberTextField.text!)
        let loginedMobileNumber = AddKidCustomView().formattedNumber(number: (UserDefaultConstants().userPhoneNumber?.last)!)

        if parentNameTextField.text?.count ?? 0 > 0 && mobileNumberTextField.text?.count ?? 0 > 0 && memberRelation?.count ?? 0 > 0 {
            self.endEditing(true)
//            let isMobileNumberContainedAlready = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?.contains(where:{$0.memberMobile == enteredmobulenumber})
//            if  isMobileNumberContainedAlready == true {
//                self.toast(notifications.mobileNumberAlreadyUser)
//                return
//            }
            if emailAddressTextField.text?.count ?? 0 > 0 {
                if Utilities.isValidEmail(emailString: emailAddressTextField?.text ?? "") == false {
                    self.toast("Please enter valid email address")
                }else if (enteredmobulenumber == loginedMobileNumber){
                    let errorMessage = notifications.nothavePermissiontoUseSameNumber
                    self.toast(errorMessage)
                }else {
                    let inviteData = InviteData(memberPhoto: "", memberName_encrypted: getEncryptedString(plainText: parentNameTextField.text), memberMobile_encrypted: getEncryptedString(plainText:mobileNumberTextField.text), memberRelation_encrypted: getEncryptedString(plainText: memberRelation), memberEmail_encrypted: getEncryptedString(plainText: emailAddressTextField.text), inviteStatus: 0)
                    
                 
                    delegate?.didTapResendButton(resendInviteView: self, inviteData: inviteData)
                }

            }else {
                if (enteredmobulenumber == loginedMobileNumber){
                    let errorMessage = notifications.nothavePermissiontoUseSameNumber
                    self.toast(errorMessage)
                    return
                }
                
                let inviteData = InviteData(memberPhoto: "", memberName_encrypted: getEncryptedString(plainText: parentNameTextField.text), memberMobile_encrypted: getEncryptedString(plainText:mobileNumberTextField.text), memberRelation_encrypted: getEncryptedString(plainText: memberRelation), memberEmail_encrypted: getEncryptedString(plainText: emailAddressTextField.text), inviteStatus: 0)

                delegate?.didTapResendButton(resendInviteView: self, inviteData: inviteData)
            }
        } else if parentNameTextField.text?.count == 0 {
            self.toast("Please enter parent name")
        } else if mobileNumberTextField.text?.count == 0 {
            self.toast("Please enter parent mobile number")
        } else if memberRelation?.count == 0 {
            self.toast("Please select a relationship")
        }
        
//        delegate?.didTapResendButton(resendInviteView: self, inviteData: resendData!)
    }
    
    func reloadInviteParentViewData()  {
        parentNameTextField.text = ""
        mobileNumberTextField.text = ""
        memberRelation = ""
        buttonStateNormal(button: fatherButton)
        buttonStateNormal(button: motherButton)
        buttonStateNormal(button: guardianButton)
    }
    
    @IBAction func addFromContactButtonTapped(_ sender: Any) {
    
    }
}

//MARK: UITextFieldDelegate Methods
extension ResendInviteView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumberTextField {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Utilities.formattedMobileNumber(number: newString)
            
            return false
        }
        return true
    }
}
