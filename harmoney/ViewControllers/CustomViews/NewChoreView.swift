//
//  NewChoreView.swift
//  Harmoney
//
//  Created by Mac on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit


class NewChoreView: UIView {

    @IBOutlet weak var newChoreButton: UIButton!
    @IBOutlet var contentView: UIView!


    
        
    //MARK: - Lifecycle methods
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            commonInit()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            commonInit()
        }
        


    
        func commonInit() {
            Bundle.main.loadNibNamed("NewChoreView", owner: self, options: nil)
            addSubview(contentView)
            contentView.frame = self.bounds
            contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    }
    
}
