//
//  ChallangeAcceptView.swift
//  Harmoney
//
//  Created by Prema Ravikumar on 08/03/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import Lottie
import SVProgressHUD

final class ChallangeAcceptView: UIView {

        @IBOutlet weak var parentView: UIView!

        @IBOutlet weak var walkTextStaticLabel: UILabel!
        @IBOutlet weak var acceptChallangeBtn: UIButton!
        @IBOutlet weak var completeLabel: UILabel!
        @IBOutlet weak var dateLabel: UILabel!
        @IBOutlet weak var walkLabel: UILabel!
        @IBOutlet weak var xpLabel: UILabel!
        @IBOutlet weak var jemLabel: UILabel!
        @IBOutlet weak var jemLabelNew: UILabel!
        @IBOutlet weak var borderView: UIView!
        @IBOutlet weak var shadowView: UIView!
        @IBOutlet weak var parentScrollView: UIScrollView!
        @IBOutlet weak var typeImageView: UIImageView!
        @IBOutlet weak var leftDescriptionLabel: UILabel!
        @IBOutlet weak var harmoneyImageView: UIImageView!
        @IBOutlet weak var closeBtn: UIButton!
        @IBOutlet weak var navTitleLabel: UILabel!
        @IBOutlet weak var charityDonationLabel: UILabel!
        @IBOutlet weak var additionalLabel: UILabel!

        var shadowAcceptChallangeView: UIView = UIView()
        var WhiteParentView: UIView = UIView()
        var cornerView: UIView = UIView()
        var stepImageView: UIImageView = UIImageView()
        var titleLabel: UILabel = UILabel()
        var descriptionLabel: UILabel = UILabel()
        var ChallangeborderView: UIView = UIView()
        var okayButton: UIButton = UIButton()
        var challange = Challenge()
        var globalHomeViewcontroller = HomeViewController()
        var ChallangeAcceptViewControl = ChallangeAcceptViewcontroller()
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
        override func draw(_ rect: CGRect) {
               commonSetup()
           }
        
        override init(frame: CGRect) {
             // Call super init
             super.init(frame: frame)
             
             // 3. Setup view from .xib file
             configureXIB()
         }
         
         required init?(coder aDecoder: NSCoder) {
             // 1. setup any properties here
             
             // 2. call super.init(coder:)
             super.init(coder: aDecoder)
             
             // 3. Setup view from .xib file
             configureXIB()
         }
         
         //MARK: - Custom Methods
         func configureXIB() {
             parentView = configureNib()
             parentView.frame = bounds
            parentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
             addSubview(parentView)
         }
         
         func configureNib() -> UIView {
             let bundle = Bundle(for: type(of: self))
             let nib = UINib(nibName: "ChallangeAcceptView", bundle: bundle)
             let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
             return view
         }
     
        
       func commonSetup() {
//            getChallengesData()
            let today = Date()
            let modifiedDate = Calendar.current.date(byAdding: .day, value: 1, to: today)!
            let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "MMM dd, yyyy"
            self.dateLabel.text = dateFormatterPrint.string(from: modifiedDate as Date)
        CreateChallanageAcceptedView()

        }
        
        @objc func handleTapOne(_ sender: UITapGestureRecognizer? = nil) {
            shadowAcceptChallangeView.isHidden = true

        }
        func CreateChallanageAcceptedView() {
            shadowView.layer.cornerRadius = 10.0
            shadowView.layer.shadowColor =  UIColor(red: 106/250, green: 162/250, blue: 172/250, alpha: 0.2).cgColor
            shadowView.layer.shadowPath = UIBezierPath(rect: shadowView.bounds).cgPath
            shadowView.layer.shadowRadius = 5
            shadowView.layer.shadowOffset = .zero
            shadowView.layer.shadowOpacity = 1

            borderView.layer.borderColor = UIColor(red: 220/250, green: 225/250, blue: 241/250, alpha: 1.0).cgColor
            borderView.layer.borderWidth = 1.0
            borderView.layer.cornerRadius = 35.0

            if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
                
                window.addSubview(shadowAcceptChallangeView)
                shadowAcceptChallangeView.isHidden = true
                shadowAcceptChallangeView.backgroundColor = UIColor(red: 0.0/250, green: 0.0/250, blue: 0.0/250, alpha: 0.7)
                shadowAcceptChallangeView.translatesAutoresizingMaskIntoConstraints = false
                shadowAcceptChallangeView.topAnchor.constraint(equalTo: window.topAnchor).isActive = true
                shadowAcceptChallangeView.leadingAnchor.constraint(equalTo: window.leadingAnchor).isActive = true
                shadowAcceptChallangeView.trailingAnchor.constraint(equalTo: window.trailingAnchor).isActive = true
                shadowAcceptChallangeView.bottomAnchor.constraint(equalTo: window.bottomAnchor).isActive = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOne(_:)))
                shadowAcceptChallangeView.addGestureRecognizer(tap)

            }
            
            shadowAcceptChallangeView.addSubview(WhiteParentView)
            WhiteParentView.layer.cornerRadius = 12
            WhiteParentView.backgroundColor = UIColor.white
            WhiteParentView.translatesAutoresizingMaskIntoConstraints = false
            WhiteParentView.centerYAnchor.constraint(equalTo: shadowAcceptChallangeView.centerYAnchor).isActive = true
            WhiteParentView.centerXAnchor.constraint(equalTo: shadowAcceptChallangeView.centerXAnchor).isActive = true
            WhiteParentView.widthAnchor.constraint(equalToConstant:  self.frame.size.width * 0.8).isActive = true

            WhiteParentView.addSubview(cornerView)
            cornerView.backgroundColor = UIColor.white
            cornerView.layer.cornerRadius = 40
            cornerView.translatesAutoresizingMaskIntoConstraints = false

            cornerView.widthAnchor.constraint(equalToConstant:  80).isActive = true
            cornerView.heightAnchor.constraint(equalToConstant:  80).isActive = true
            cornerView.centerXAnchor.constraint(equalTo: WhiteParentView.centerXAnchor).isActive = true
            cornerView.centerYAnchor.constraint(equalTo: WhiteParentView.topAnchor).isActive = true

            cornerView.addSubview(stepImageView)
            stepImageView.translatesAutoresizingMaskIntoConstraints = false
            stepImageView.image = UIImage.init(named: "footsteps")
            stepImageView.centerXAnchor.constraint(equalTo: cornerView.centerXAnchor).isActive = true
            stepImageView.centerYAnchor.constraint(equalTo: cornerView.centerYAnchor).isActive = true
           
            WhiteParentView.addSubview(titleLabel)
            titleLabel.textAlignment = .center
            titleLabel.translatesAutoresizingMaskIntoConstraints = false
            titleLabel.numberOfLines = 0
            titleLabel.text = "Start Walking and your steps will be automatically tracked"//"Yay! You have accepted this challenge!"

//            titleLabel.text = "Yay! You have accepted this challenge! Your mobile will notify us when you are done!"
            titleLabel.font =  UIFont(name: "FuturaPT-Medium", size: 20)
            titleLabel.textColor = UIColor.black
            titleLabel.topAnchor.constraint(equalTo: cornerView.bottomAnchor ,constant: 3).isActive = true
            titleLabel.trailingAnchor.constraint(equalTo: WhiteParentView.trailingAnchor,constant:-15).isActive = true
            titleLabel.leadingAnchor.constraint(equalTo: WhiteParentView.leadingAnchor,constant:15).isActive = true

            WhiteParentView.addSubview(descriptionLabel)
            descriptionLabel.textAlignment = .center
            descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
       //     descriptionLabel.text = "Your mobile will notify us when you are done!"

//            descriptionLabel.text = "From now on your activities will captured through mobile or paired wearbles."
            descriptionLabel.font =  UIFont(name: "FuturaPT-Book", size: 18)
            descriptionLabel.textColor = UIColor.black
            descriptionLabel.numberOfLines = 0

            titleLabel.bottomAnchor.constraint(equalTo: descriptionLabel.topAnchor ,constant: -7).isActive = true
            descriptionLabel.trailingAnchor.constraint(equalTo: WhiteParentView.trailingAnchor,constant:-15).isActive = true
            descriptionLabel.leadingAnchor.constraint(equalTo: WhiteParentView.leadingAnchor,constant:15).isActive = true

            WhiteParentView.addSubview(ChallangeborderView)
            ChallangeborderView.backgroundColor = UIColor(red: 112/250, green: 112/250, blue: 112/250, alpha: 1.0)
            ChallangeborderView.alpha = 0.35
            ChallangeborderView.translatesAutoresizingMaskIntoConstraints = false
            descriptionLabel.bottomAnchor.constraint(equalTo: ChallangeborderView.topAnchor ,constant: -20).isActive = true
            ChallangeborderView.trailingAnchor.constraint(equalTo: WhiteParentView.trailingAnchor).isActive = true
            ChallangeborderView.leadingAnchor.constraint(equalTo: WhiteParentView.leadingAnchor).isActive = true
            ChallangeborderView.heightAnchor.constraint(equalToConstant: 1).isActive = true

            WhiteParentView.addSubview(okayButton)
            okayButton .addTarget(self, action: #selector(ChallanageAccetOkayBtnAction), for: .touchUpInside)
            okayButton.translatesAutoresizingMaskIntoConstraints = false
            okayButton.titleLabel?.font = UIFont(name: "FuturaPT-Medium", size: 21)
            okayButton.setTitleColor(UIColor(red: 38/250, green: 128/250, blue: 235/250, alpha: 1.0), for: .normal)
            okayButton.setTitle("Got it", for: .normal)
            ChallangeborderView.bottomAnchor.constraint(equalTo: okayButton.topAnchor).isActive = true
            okayButton.bottomAnchor.constraint(equalTo: WhiteParentView.bottomAnchor).isActive = true
            okayButton.trailingAnchor.constraint(equalTo: WhiteParentView.trailingAnchor).isActive = true
            okayButton.leadingAnchor.constraint(equalTo: WhiteParentView.leadingAnchor).isActive = true
            okayButton.heightAnchor.constraint(equalToConstant: 50).isActive = true

        }

        @IBAction func closeBtnAction(_ sender: Any) {
         
        ChallangeAcceptViewControl.closeBtnAction()
        }

        @IBAction func AcceptChallengeBtnAction(_ sender: Any) {
        if let cId = challange["cId"] as? String
            {
                if cId == "2"
                {
                    stepImageView.image = UIImage.init(named: "selectedBought")
                    titleLabel.text = "You have accepted the challenge!"
                      ChallangeAcceptViewControl.acceptChallenge(challenge: challange)
                }
                else if cId == "3"
                {
//                okayButton.setTitle("Pay Now", for: .normal)
                stepImageView.image = UIImage.init(named: "shareOutfilled2")
                    titleLabel.text = "You have accepted the challenge!"
                    descriptionLabel.text = "Save Money to your first saving goal to unlock Mysetry chest"

//                descriptionLabel.text = "Save Money to your wallet to unlock Mysetry chest"
                }
                if cId != "2"{
                    shadowAcceptChallangeView.isHidden = false
                }
            }
            
            
        }
        @objc func ChallanageAccetOkayBtnAction(_ sender: UIButton) {

        ChallangeAcceptViewControl.acceptChallenge(challenge: challange)
//            walkTextStaticLabel.isHidden = true
            shadowAcceptChallangeView.isHidden = true
//            self.shadowView.isHidden = true
        }
        
       func getChallengesData(){

            var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")

            if Reachability.isConnectedToNetwork(){
                
                var (url, method, param) = APIHandler().getChallenges(params: data)
                
                url = url + "/" + UserDefaultConstants().sessionKey!

                SVProgressHUD.show()
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            
                            if let data = value["data"] as? [String:Any] {
                                self.challange = data
                                self.getChallangeData(challengesData: data)
                            }
                        }
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.showError(withStatus: error.localizedDescription)

                    }
                }

            }else{
                self.toast("Internet Connection not Available!")
            }
        }
        
        func getChallangeData(challengesData: [String:Any]) {
            
            self.challange = challengesData

//            globalHomeViewcontroller.Parenttasklisttableview.isHidden = false
             var cIdstr =  "1"
            if let cId = challengesData["cId"] as? String
            {
                cIdstr = cId
            }
            
//            if let cInfo = challengesData["cInfo"] as? [String:Any] {
//                globalHomeViewcontroller.ParenttasklisttableviewHeightConstraint.constant
//              self.shadowView.isHidden = false
                charityDonationLabel.isHidden = false
//                if var cCompletedCount = cInfo["taskCount"] as? String {
                    
//                    walkTextStaticLabel.isHidden = false
            var cCompletedCount = ""
            if let cInfo = challengesData["cInfo"] as? [String:Any] {
                if let cc = cInfo["taskCount"] as? String {
                    cCompletedCount = cc
                }
            }
            if(cCompletedCount.count == 0){
                cCompletedCount = challengesData["taskCount"] as? String ?? ""
            }
                    var step = " Steps"
                    if cIdstr == "1"
                    {
                        
                        walkTextStaticLabel.text = "Walk"
                        navTitleLabel.text = "Walk"
                        self.walkLabel.text = cCompletedCount
                        self.walkLabel.isHidden = true
                        self.leftDescriptionLabel.isHidden = true
                        self.additionalLabel.isHidden = false
                        charityDonationLabel.text = "Kickstart your day with a small walk"
                        let noOFsteps = NSAttributedString(string: "No. of Steps  ", attributes: [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Demi", size: 16)!, NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.1119422093, green: 0.1536023319, blue: 0.2179811597, alpha: 1)])
                        let stepsCount = NSAttributedString(string: "\(cCompletedCount)", attributes: [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Bold", size: 16)!, NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.9836915135, green: 0.7184832096, blue: 0.6810647845, alpha: 1)])
                        let finalStr = NSMutableAttributedString(attributedString: noOFsteps)
                        finalStr.append(stepsCount)
                        additionalLabel.attributedText = finalStr

                    }
                    else if cIdstr == "2"
                    {
                        walkTextStaticLabel.text = "Offer"
                        navTitleLabel.text = "Offer"
                        self.walkLabel.isHidden = true
                        self.leftDescriptionLabel.isHidden = true
                        self.additionalLabel.isHidden = false
                        self.walkLabel.text =  nil//"1.30" + "Min"

//                        self.walkLabel.text = cCompletedCount + "L"
                        leftDescriptionLabel.text = "Task"
                        walkLabel.text = challengesData["cName"] as? String
                        
                        
                        let noOFsteps = NSAttributedString(string: "Task  ", attributes: [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Demi", size: 16)!, NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.1119422093, green: 0.1536023319, blue: 0.2179811597, alpha: 1)])
                        let stepsCount = NSAttributedString(string: challengesData["cName"] as? String ?? "", attributes: [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Bold", size: 16)!, NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.9836915135, green: 0.7184832096, blue: 0.6810647845, alpha: 1)])
                        let finalStr = NSMutableAttributedString(attributedString: noOFsteps)
                        finalStr.append(stepsCount)
                        additionalLabel.attributedText = finalStr

                        
                        charityDonationLabel.text = ""
                        typeImageView.image = UIImage.init(named: "selectedBought")
                        if let cName = challengesData["cName"] as? String
                        {
                            cCompletedCount = cName
                        }
                        
                        cCompletedCount = "This"

                        step = ""
                        acceptChallangeBtn.setTitle("Let's do it", for: .normal)
                    }
                    else if cIdstr == "3"
                    {
                        additionalLabel.isHidden = false
                        harmoneyImageView.isHidden = false
                        walkTextStaticLabel.text = "Share"
                        navTitleLabel.text = "Share"
                        let earnString = NSAttributedString(string: "Earn ", attributes: [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Bold", size: 16)!])
//                        leftDescriptionLabel.attributedText = earnString
                        leftDescriptionLabel.textColor = #colorLiteral(red: 0.9836915135, green: 0.7184832096, blue: 0.6810647845, alpha: 1)
                        additionalLabel.textColor = #colorLiteral(red: 0.9836915135, green: 0.7184832096, blue: 0.6810647845, alpha: 1)
                        typeImageView.image = UIImage.init(named: "shareOutfilled2")

                        let amountImageAttachment = NSTextAttachment()
                        amountImageAttachment.image = UIImage(named: "h-r")
                        amountImageAttachment.bounds = CGRect(x: 0, y: 0, width: 14, height: 14)

                        let amountImageAttachmentString = NSAttributedString(attachment: amountImageAttachment)
                        let amountAttributedString = NSMutableAttributedString(attributedString: earnString)
                        amountAttributedString.append(amountImageAttachmentString)
                        let amountAdditionalAttributedStrring = NSAttributedString(string: "\(cCompletedCount) and give to a charity", attributes: [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Bold", size: 16)!])
                        amountAttributedString.append(amountAdditionalAttributedStrring)
                        
                        leftDescriptionLabel.isHidden = true
                        walkLabel.isHidden = true
//                        leftDescriptionLabel.attributedText = amountAttributedString
//                        self.walkLabel.text = ""
                        additionalLabel.attributedText = amountAttributedString
                        self.walkLabel.removeFromSuperview()
                        let imageAttachment = NSTextAttachment()
                        imageAttachment.image = UIImage.init(named: "h-Text")!

                        imageAttachment.bounds = CGRect(x: 0, y: 0, width: 10, height: 12)
                        let attachmentString = NSAttributedString(attachment: imageAttachment)

                        let attributedText = NSMutableAttributedString(string: "Harmoney will donate ", attributes: [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Book", size: 17.0)!])
                        attributedText.append(attachmentString)
                        let secondAttributedText = NSAttributedString(string: "1 on your behalf.", attributes: [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Book", size: 17.0)!])
                        attributedText.append(secondAttributedText)
//                        attributedText.setAttributes([NSAttributedString.Key.font: UIFont(name: "FuturaPT-Book", size: 17.0)!], range: NSRange(location: 0, length: attributedText.length))

                        self.charityDonationLabel.attributedText = attributedText
                        self.charityDonationLabel.isHidden = false

                        cCompletedCount = "the Share challenge"

                        step = ""
                        
                    }
            let sentence = "Complete " + cCompletedCount + step
                    let regularAttributes = [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Bold", size: 17)]
                    let largeAttributes = [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Book", size: 17)]
                    let attributedSentence = NSMutableAttributedString(string: sentence, attributes: regularAttributes as [NSAttributedString.Key : Any])

                    attributedSentence.setAttributes(largeAttributes as [NSAttributedString.Key : Any], range: NSRange(location: 0, length: 8))
                    if cIdstr == "2"{
                        attributedSentence.setAttributes(largeAttributes as [NSAttributedString.Key : Any], range: NSRange(location: 9, length: cCompletedCount.count))
                    }else{
                        attributedSentence.setAttributes(regularAttributes as [NSAttributedString.Key : Any], range: NSRange(location: 9, length: cCompletedCount.count))
                    }
            attributedSentence.setAttributes(largeAttributes as [NSAttributedString.Key : Any], range: NSRange(location: 9 + cCompletedCount.count, length: step.count))
                    completeLabel.attributedText = attributedSentence
//                }
//            }

                if let points = challengesData["points"] as? [String:Any] {
                    if let xp = points["xp"] {
                        let xNSNumber = xp as! NSNumber
                        self.xpLabel.text = xNSNumber.stringValue
                    }
                    if let jem = points["jem"] {
                        let xNSNumber = jem as! NSNumber
                        self.jemLabelNew.text = xNSNumber.stringValue
                    }
    //                if let xp = points["jem"] as? Int {
    //                let xNSNumber = xp as NSNumber
    //                    self.jemLabel.text = xNSNumber.stringValue
    //                }

                }
        }
    }

