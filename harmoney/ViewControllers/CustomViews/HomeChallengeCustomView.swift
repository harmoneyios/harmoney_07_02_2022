//
//  HomeChallengeCustomView.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/25/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class HomeChallengeCustomView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    var homeChallenge: HomeChallenge? {
        didSet {
            configureUI()
        }
    }
    
//MARK: - Lifecycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
            
        commonInit()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
            
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("HomeChallengeCustomView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}

//MARK: - UI methods
extension HomeChallengeCustomView {
    func configureUI() {
        imageView.imageFromURL(urlString: homeChallenge?.imageUrl ?? "")
        imageView.borderColor = UIColor.homeScreenBorderColor
        
        switch homeChallenge?.typeName {
        case "Walk":
            titleLabel.text = homeChallenge?.typeName
            subtitleLabel.text = String(format: "Task: %@ Steps", homeChallenge?.task ?? 0)
        case "Run":
            titleLabel.text = homeChallenge?.typeName
            subtitleLabel.text = String(format: "Task: %@ Miles", homeChallenge?.task ?? 0)
        case "Swim":
            titleLabel.text = homeChallenge?.typeName
            subtitleLabel.text = String(format: "Task: %@ Laps", homeChallenge?.task ?? 0)
        case "Bike":
            titleLabel.text = homeChallenge?.typeName
            subtitleLabel.text = String(format: "Task: %@ Miles", homeChallenge?.task ?? 0)
        case .none:
            break
        case .some(_):
            break
        }
    }
    
    func clearView() {
        imageView.borderColor = UIColor.clear
        imageView.image = nil
        titleLabel.text = ""
        subtitleLabel.text = ""
    }
    
    func setupHarmoneyAttributedLabel() {
        let harmoneyString = String(format: "%d", homeChallenge?.task ?? 0)
        
        let xpAttachment = NSTextAttachment()
        xpAttachment.image = UIImage(named: "home_harmoney_icon")
        let font = UIFont.futuraPTBookFont(size: 12)
        
        let imageSize = xpAttachment.image!.size
        xpAttachment.bounds = CGRect(x: CGFloat(0), y: (font.capHeight - imageSize.height) / 2, width: imageSize.width, height: imageSize.height)
        
        let attributedText = NSMutableAttributedString(string: "Task: Share ")
        let xpImageString = NSAttributedString(attachment: xpAttachment)
        attributedText.append(xpImageString)
        attributedText.append(NSAttributedString(string: harmoneyString))
        
        let lightRange = (attributedText.string as NSString).range(of: attributedText.string)
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBookFont(size: 12)], range:lightRange)
        
        subtitleLabel.attributedText = attributedText
    }
}

