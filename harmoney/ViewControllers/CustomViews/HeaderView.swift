//
//  HeaderView.swift
//  Harmoney
//
//  Created by GOPI K on 21/03/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import AlamofireObjectMapper
import LinearProgressView
import SVProgressHUD
import ObjectMapper

class HeaderView: UIView {
    @IBOutlet weak var harmonyBucksContainerView: UIView!
    @IBOutlet weak var bellView: UIView!
    @IBOutlet weak var progressView: CircularProgress!
    @IBOutlet weak var xpCountLabel: AnimatedLabel!
    @IBOutlet weak var harmonyBucksLabel: AnimatedLabel!
    @IBOutlet weak var notificationsCountLabel: UILabel!
    @IBOutlet weak var levelLabel: AnimatedLabel!
    @IBOutlet weak var diamondLabel: AnimatedLabel!
   
    @IBOutlet weak var levelButton: UIButton!
    @IBOutlet weak var hbView: UIView!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var soccerButton: UIButton!
    
    @IBOutlet weak var diamondimageview: UIImageView!
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.getDashboardData()
        self.getConfigData()
        self.setupViews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updateHeaderViewAfterLevelRewardClaimed(lastDiamondValue: Float) {
        let gemNumber = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
        let updatedGems = gemNumber + 10
        
        diamondLabel.count(from: Float(HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0), to: Float(updatedGems), duration: .brisk)
        
        UIView.animate(withDuration: 1.5, animations: {
            self.diamondLabel.transform = CGAffineTransform(scaleX: 2, y: 2)
        }) { (finished) in
            UIView.animate(withDuration: 1.5, animations: {
                self.diamondLabel.transform = CGAffineTransform.identity
            })
        }
    }
    
    func updateHeaderViewAfterCouponClaimed(lastDiamondValue: Float) {
        diamondLabel.count(from: Float(lastDiamondValue), to: Float(HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0), duration: .brisk)
        
        UIView.animate(withDuration: 1.5, animations: {
            self.diamondLabel.transform = CGAffineTransform(scaleX: 2, y: 2)
        }) { (finished) in
            UIView.animate(withDuration: 1.5, animations: {
                self.diamondLabel.transform = CGAffineTransform.identity
            })
        }
    }

    func updateHeaderViewWithNewData() {
        xpCountLabel.countFromZero(to: Float(HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0), duration: .brisk)
        diamondLabel.countFromZero(to: Float(HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0), duration: .brisk)
        harmonyBucksLabel.countFromZero(to: Float(HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0), duration: .brisk)
        levelLabel.countFromZero(to: Float(HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0), duration: .brisk)
        
        UIView.animate(withDuration: 2, animations: {
            self.xpCountLabel.transform = CGAffineTransform(scaleX: 2, y: 2)
            self.diamondLabel.transform = CGAffineTransform(scaleX: 2, y: 2)
            self.harmonyBucksLabel.transform = CGAffineTransform(scaleX: 2, y: 2)
        }) { (finished) in
            UIView.animate(withDuration: 2, animations: {
                self.xpCountLabel.transform = CGAffineTransform.identity
                self.diamondLabel.transform = CGAffineTransform.identity
                self.harmonyBucksLabel.transform = CGAffineTransform.identity
            })
        }
    }
    
    func registerNotifications() {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.clearAllNotifications), name: Notification.Name(rawValue: notifications.clearAllNotification), object: nil)
    }
    
   @objc func clearAllNotifications(){
        self.notificationsCountLabel.text = ""
        self.bellView.isHidden = true
        self.getDashboardData()
    }
    
    func setupViews() {
        self.progressView.setProgressWithAnimation(duration: 0.0, value: 0)
        self.progressView.progressColor =  UIColor(red: 248 / 255.0, green: 167 / 255.0, blue: 158 / 255.0, alpha: 1.0)
        self.progressView.trackColor = UIColor(red: 187.0/255.0, green: 194.0/255.0, blue: 214.0/255.0, alpha: 1.0)
        self.progressView.layer.cornerRadius = self.bellView.frame.size.width/2
        self.progressView.layer.masksToBounds = true
        
        self.bellView.layer.cornerRadius = self.bellView.frame.size.width/2
        self.bellView.layer.masksToBounds = true
        
        self.harmonyBucksContainerView.layer.cornerRadius = 15.0
        self.harmonyBucksContainerView.layer.masksToBounds = true
        self.harmonyBucksContainerView.backgroundColor = UIColor.homelightgrayColor
        
        xpCountLabel.adjustsFontSizeToFitWidth = true
        diamondLabel.adjustsFontSizeToFitWidth = true
        harmonyBucksLabel.adjustsFontSizeToFitWidth = true
        levelLabel.adjustsFontSizeToFitWidth = true
    }
    
    @IBAction func levelsButtonTapped(_ sender: UIButton) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        
        if let topController = keyWindow?.rootViewController {
            let levelsVC = GlobalStoryBoard().levelsVC
            topController.present(levelsVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func diamondButtonTapped(_ sender: UIButton) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        
        if let topController = keyWindow?.rootViewController {
//            let gemsVC = GlobalStoryBoard().gemsVC
//            topController.present(gemsVC, animated: true, completion: nil)

             
            let gemsVC = GlobalStoryBoard().CouponList
            gemsVC.isfromSecondChallange = false
            topController.present(gemsVC, animated: true, completion: nil)
            

            
        }
    }
    
    @IBAction func notificationClickAction(_ sender: Any) {
        self.bellView.isHidden = true
        self.notificationsCountLabel.text = ""
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
       

        if let topController = keyWindow?.rootViewController {
            let vc = GlobalStoryBoard().notificationsVC
            topController.present(vc, animated: true, completion: nil)
        }
    }
    
    func updateUI(){
        guard HarmonySingleton.shared.dashBoardData != nil else {
            return
        }
        DispatchQueue.main.async {
            let value = HarmonySingleton.shared.dashBoardData.toJSON()
            if value.keys.count == 0{
                return
            }
            if let status = value["status"] as? String, status == "Failure" {
                SVProgressHUD.showError(withStatus: "HeaderView get dashboard data operation failed")
                return
            }
            
            if let sucessMessage = value["sucMsg"] as? [String:Any], let message = sucessMessage["message"] {
                print(message)
            }
            
            if let data = value["data"] as? [String:Any] {
                let keyedArch = NSKeyedArchiver.archivedData(withRootObject: data)
                UserDefaults.standard.set(keyedArch, forKey: "DashBoardData")
                
                if let notificationArray = data["notification"] as? [Any] {
                    //self.notificationArray = notificationArray
                }
                var statusCount = -1
                
                if let challengesArrray = data["challenges"] as? [Any] {
                    if challengesArrray.count > 0  {
                        for index in 0 ... challengesArrray.count-1 {
                            if let data = challengesArrray[index] as? [String:Any] {
                                if let cStatus = data["cStatus"] as? String {
                                    if cStatus == "1" {
                                        statusCount = index
                                    }
                                }
                            }
                        }
                    } else {
                        
                    }
                    
                    if statusCount == 0 {
                        self.progressView.setProgressWithAnimation(duration: 0.0, value: 0.3)
                    } else if statusCount == 1 {
                        self.progressView.setProgressWithAnimation(duration: 0.0, value: 0.6)
                    } else if statusCount == 2 {
                        self.progressView.setProgressWithAnimation(duration: 0.0, value: 1)
                    }
                }
                if let xp = data["newNotificationCount"] as? Int {
                    let xNSNumber = xp as NSNumber
                    
                    if xNSNumber.stringValue == "0"{
                        self.bellView.isHidden = true
                        self.notificationsCountLabel.text = ""
                    } else {
                        self.notificationsCountLabel.text = xNSNumber.stringValue
                        self.bellView.isHidden = false
                    }
                   // self.notificationView.isHidden = false
                }
                
                if let points = data["points"] as? [String:Any] {
                    if let xp = points["xp"] as? Int {
                        let xNSNumber = xp as NSNumber
                        self.xpCountLabel.text = xNSNumber.stringValue
                    }
                    
                    if let xp = points["hBucks"] as? Double {
                        let xNSNumber = Int(xp)
                        self.harmonyBucksLabel.text = String(xNSNumber)
                    }else{
                        self.harmonyBucksLabel.text = "0"
                    }
                    
                    if let xp = points["jem"] as? Int {
                        let xNSNumber = xp as NSNumber
                        self.diamondLabel.text = xNSNumber.stringValue
                    }
                    
                    if let level = points["level"] as? Int {
                        let xNSNumber = level as NSNumber
                        self.levelLabel.text = xNSNumber.stringValue
                    }
                }
            }
        }
    }
    
    func getDashboardData() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey ?? "", forKey: "session_token")
        if Reachability.isConnectedToNetwork(){
            var (url, method, param) = APIHandler().dashboard(params: data)
            url = url + "/" + (UserDefaultConstants().sessionKey ?? "")
            print("getDashboardData in Top Bar")
            print((url, method, param))
            SVProgressHUD.show()
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                DispatchQueue.main.async {
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: value)
                            self.updateUI()
                        }
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                        self.getDashboardData()
                    }
                }
            }
        } else{
            self.toast("Internet Connection not Available!")
        }
    }

    func getConfigData() {
        if Reachability.isConnectedToNetwork() {
        let (url, method, param) = APIHandler().getConfig(params: [:])
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                    
                    switch response.result {
                       case .success(let value):
                        if let value = value as? [String:Any] {
                            let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                            let configModel = try? JSONDecoder().decode(ConfigModel.self, from: jsonData!)
                            HarmonySingleton.shared.cofigModel = configModel
                            
                            let creditType = configModel?.data.creditType
                            
                            if creditType == 1 {
                                self.diamondimageview.image = UIImage(named: "soccer")

                            } else if creditType == 2 {
                                self.diamondimageview.image = UIImage(named: "diamond")

                            } else {
                                self.diamondimageview.image = UIImage(named: "touchdown")

                            }

                        }
                        break
                        case .failure(let error):
                            self.getConfigData()
                        break
                       }
            }
        }
    }
    

}


