//
//  RemoveMemberView.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 23/06/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
protocol RemoveMemberViewDelegate: class {
    func didTapRemoveMemberButton(resendInviteView: RemoveMemberView, inviteData: InviteData)
    func closeRemoveMemberView()

}
class RemoveMemberView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var closeMemberDetailView: UIButton!
    @IBOutlet weak var removeMemberBtn: UIButton!
    @IBOutlet weak var memberDetailTableView: UITableView!
    @IBOutlet weak var memberProfile: UIImageView!
    var resendData: InviteData?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    weak var delegate: RemoveMemberViewDelegate?
    func commonInit() {
        Bundle.main.loadNibNamed("RemoveMemberView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        memberDetailTableView.register(UINib.init(nibName: "RemoveMemberDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "RemoveMemberDetailTableViewCell")
        
        memberDetailTableView.register(UINib.init(nibName: "PrimaryCheckTableViewCell", bundle: nil), forCellReuseIdentifier: "PrimaryCheckTableViewCell")
        
        if resendData?.memberPhoto?.count ?? 0 == 0

        {
      
            self.setProfilePic(frstName: resendData?.memberName, lstName: nil, profileImageView: memberProfile)

          
        }else{
            
            memberProfile?.sd_setImage(with: URL.init(string: resendData?.memberPhoto ?? ""))
         
        }
        
    }
    func setProfilePic(frstName: String?, lstName: String?, profileImageView: UIImageView){
//        let menuBackSystem =  UIColor(red:  41.0/255.0, green:  72.0/255.0, blue:  100.0/255.0, alpha: 1.0)
         if let firstName = frstName, let lastName = lstName {
             profileImageView.setImageWith(firstName + " " + lastName, color: ConstantString.menuBackSystemProfilepic)
         } else {
             if let dispName = frstName {
                 profileImageView.setImageWith(dispName, color: ConstantString.menuBackSystemProfilepic)
             }
         }
    }

    @IBAction func removeMemberAct(_ sender: Any) {
        if let memberData = resendData{
            delegate?.didTapRemoveMemberButton(resendInviteView: self, inviteData: memberData)

        }
        
    }
    @IBAction func closeMemberViewAct(_ sender: Any) {
        delegate?.closeRemoveMemberView()
    }
    
    func onLoadSetUp()  {
        if HarmonySingleton.shared.dashBoardData.data?.invitedRequest?.count ?? 0 > 0{
            memberDetailTableView.isHidden = false
               }else{
                memberDetailTableView.isHidden = true
               }
        memberDetailTableView.reloadData()
    }
    func reloadData()  {
        memberDetailTableView.reloadData()
    }
}

extension RemoveMemberView : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrimaryCheckTableViewCell", for: indexPath) as! PrimaryCheckTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "RemoveMemberDetailTableViewCell", for: indexPath) as! RemoveMemberDetailTableViewCell
//        cell.parentVC = self
        cell.borderView.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.8823529412, blue: 0.9450980392, alpha: 1)
        if indexPath.row == 0 {
            cell.memberDetailLabel.text = ConstantString.firstName
            cell.memberDetailValuelabel.text = resendData?.memberName
        }
        if indexPath.row == 1{
            cell.memberDetailLabel.text = ConstantString.mobileNumber
            cell.memberDetailValuelabel.text = resendData?.memberMobile
        }
        if indexPath.row == 2{
            cell.memberDetailLabel.text = ConstantString.memberRelationship
            cell.memberDetailValuelabel.text = resendData?.memberRelation
        }
        return cell
    }
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      
    }
    
}
