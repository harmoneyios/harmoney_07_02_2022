//
//  LevelsCouponCustomView.swift
//  Harmoney
//
//  Created by Csongor Korosi on 8/5/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class LevelsCouponCustomView: UIView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var contentView: UIView!
    
//MARK: - Lifecycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
                
        commonInit()
    }
            
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
            
        commonInit()
    }
        
    func commonInit() {
        Bundle.main.loadNibNamed("LevelsCouponCustomView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}
