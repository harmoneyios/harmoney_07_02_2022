//
//  OptionsCollectionViewCell.swift
//  MyInterest
//
//  Created by Dineshkumar kothuri on 29/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ObjectMapper
import Toast_Swift


class OptionsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var optionBtn: UIButton!
    var optionItem : InterestItem?
    var parentVC : InterestViewController?
    var parentComponent : MyInterestCellTableViewCell?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func awakeFromNib() {
        super.awakeFromNib()
        self.optionBtn.setTitle(nil, for: .normal)
        self.layer.cornerRadius = 15
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.systemGray.cgColor
        optionBtn.titleLabel?.font = ConstantString.intButtonFont
    }
    @IBAction func optionClick(sender:UIButton){
        let text = parentComponent?.countLabel.text
        let number = text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "()")) ?? "0"
        var countNum = Int(number) ?? 0
        
        if sender.backgroundColor ==  #colorLiteral(red: 0.6745098039, green: 0.6980392157, blue: 0.7764705882, alpha: 1){
            sender.backgroundColor = .white
            sender.setTitleColor(#colorLiteral(red: 0.6745098039, green: 0.6980392157, blue: 0.7764705882, alpha: 1), for: .normal)
            countNum -= 1
        }else{
           sender.backgroundColor = #colorLiteral(red: 0.6745098039, green: 0.6980392157, blue: 0.7764705882, alpha: 1)
            sender.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            countNum += 1
        }
                     
        parentComponent?.countLabel.text = "(\(countNum > 0 ? countNum : 0))"
        updateInterestsToBackend()
    }
    
    func appendData(obj:InterestItem, parentScreen:InterestViewController)  {
        self.optionItem = obj
        self.parentVC = parentScreen
        self.optionBtn.setTitle(obj.ItemName, for: .normal)
        
        if let arr = parentVC?.currentInterests?.response?.filter({$0.item_code == obj.ItemID}), arr.count > 0 {
            optionBtn.backgroundColor = #colorLiteral(red: 0.6745098039, green: 0.6980392157, blue: 0.7764705882, alpha: 1)
            optionBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        }else{
            optionBtn.backgroundColor = .white
            optionBtn.setTitleColor(#colorLiteral(red: 0.6745098039, green: 0.6980392157, blue: 0.7764705882, alpha: 1), for: .normal)
        }
    }
    
    func updateInterestsToBackend() {

        if Reachability.isConnectedToNetwork() {
//         SVProgressHUD.show()
            var data = [String : Any]()
            data.updateValue(UserDefaultConstants().guid!, forKey: "guid")
            data.updateValue((self.optionItem?.ItemID)!, forKey: "item_code")
            data.updateValue((self.optionItem?.CategoryCode)!, forKey: "cat_code")
        var (url, method, param) = APIHandler().updateInterest(params: data)
//            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in                    
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            print(value)
                            self.parentVC?.getUserInterests(reload: false)
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
    }

}
