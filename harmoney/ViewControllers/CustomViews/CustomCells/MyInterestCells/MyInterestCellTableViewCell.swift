//
//  MyInterestCell.swift
//  MyInterest
//
//  Created by Dineshkumar kothuri on 28/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

class MyInterestCellTableViewCell: UITableViewCell{
    
    

    @IBOutlet weak var dropImageView: UIImageView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    
    var interestCategoryObj : InterestCategory?
    var parentVC : InterestViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpUI()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setUpUI()  {
        titleLabel.font = ConstantString.titleFontEdit
        
//        optionsCollectionView.register(UINib.init(nibName: "OptionsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OptionsCollectionViewCell")
        optionsCollectionView.delegate = self
        optionsCollectionView.dataSource = self
        bgView.layer.cornerRadius = 10
        bgView.layer.borderWidth = 1
        bgView.layer.borderColor = #colorLiteral(red: 0.6745098039, green: 0.6980392157, blue: 0.7764705882, alpha: 0.64)
        
        bgView.layer.shadowColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 0.5)
        bgView.layer.shadowOpacity = 1
        bgView.layer.shadowOffset = .zero
        bgView.layer.shadowRadius = 2
                
        
        self.optionsCollectionView.reloadData()
        self.optionsCollectionView.layoutIfNeeded()
        
    }
    
    func appendData(obj:InterestCategory, parentScreen: InterestViewController)  {
        titleLabel.text = obj.CategoryName
        titleLabel.font = ConstantString.intFieldFont
        self.interestCategoryObj = obj
        self.parentVC = parentScreen
        optionsCollectionView.reloadData()
        
        let ary = parentScreen.expandCell.filter({$0.tag == self.tag})
        if ary.count > 0{
            dropImageView?.isHighlighted = true
        }else{
            dropImageView?.isHighlighted = false
        }
        
        let alreadyInterests = parentScreen.currentInterests?.response?.filter({$0.cat_code == obj.CategoryCode})
        countLabel.text = "(\(alreadyInterests?.count ?? 0))"
        
    }

}

extension MyInterestCellTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.interestCategoryObj?.Item?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionsCollectionViewCell", for: indexPath) as! OptionsCollectionViewCell
        cell.parentComponent = self
        if let arr = self.interestCategoryObj?.Item{
            cell.appendData(obj: arr[indexPath.row], parentScreen: self.parentVC!)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = interestCategoryObj?.Item?[indexPath.row].ItemName ?? ""
        let cellWidth = text.size(withAttributes:[.font: UIFont.systemFont(ofSize:12.0)]).width + 20
        return CGSize(width: cellWidth, height: 32.0)
    }
}
