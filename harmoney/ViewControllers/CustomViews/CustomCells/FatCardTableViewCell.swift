//
//  FatCardTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 30/05/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class FatCardTableViewCell: UITableViewCell {

    @IBOutlet weak var fatCardBottom: UIView!
    @IBOutlet weak var fatcardTop: UIView!
    @IBOutlet weak var factImgeView: UIImageView!
    @IBOutlet weak var fatLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        fatCardBottom.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        fatcardTop.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
