//
//  CustomRewardTableViewCell.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/16/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import UICircularProgressRing

protocol CustomRewardTableViewCellDelegate: class {
    func didTapClaimButton(customRewardTableViewCell: CustomRewardTableViewCell)
}


class CustomRewardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var smallClaimButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var customRewardLabel: UILabel!
    @IBOutlet weak var rewardImageView: UIImageView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var levelCustomView: UIView!
    @IBOutlet weak var circularProgressRing: UICircularProgressRing!
    @IBOutlet weak var vouchersImageView: UIImageView!
    @IBOutlet weak var circleImageView: UIImageView!
    
    @IBOutlet weak var tabButton: UIButton!
    
    var voucherData: VoucherData? {
        didSet {
            circleImageView.isHidden = true
            circularProgressRing.isHidden = true
            rewardImageView.isHidden = true
            vouchersImageView.isHidden = false
            levelCustomView.isHidden = true
            titleLabel.text = voucherData?.voucherName ?? ""
            taskLabel.text = "Voucher from Level Up"
            customRewardLabel.text = String(format: "%@", voucherData?.voucherCode ?? "")
            if voucherData?.voucherImage == "" || voucherData?.voucherImage != nil {
                vouchersImageView.imageFromURL(urlString: voucherData?.voucherImage ?? "")

            }else {
                vouchersImageView.image = UIImage(named: "ProfilePlaceholder")

            }
            vouchersImageView.contentMode = .scaleAspectFit
        }
    }
    
    var claimedCoupon: Datum? {
        didSet {
            levelCustomView.isHidden = true
            vouchersImageView.isHidden = true
            rewardImageView.isHidden = false
            titleLabel.text = claimedCoupon?.strProductTitle ?? ""
            taskLabel.text = claimedCoupon?.strProductOffValueText ?? ""
            customRewardLabel.text = String(format: "Claim to get %@", claimedCoupon?.strProductOffValueText ?? "")
            rewardImageView.imageFromURL(urlString: claimedCoupon?.strTaskThumbnailImage ?? "")
            rewardImageView.contentMode = .scaleAspectFit
        }
    }
    
    
    var customReward: RewardsData? {
        didSet {
            vouchersImageView.isHidden = true
            
            if customReward?.taskType == 3 {
                circularProgressRing.style = .ontop
                rewardImageView.isHidden = true
                levelCustomView.isHidden = false
                levelLabel.text = String(format: "%d", customReward?.level ?? "")
                titleLabel.text = String(format: "%@ %d", customReward?.taskName ?? "", customReward?.level ?? 0)
                customRewardLabel.text = customReward?.rewardName ?? ""
                configureTaskLabel(customReward: customReward!)
            } else {
                rewardImageView.isHidden = false
                levelCustomView.isHidden = true
                titleLabel.text = customReward?.taskName ?? ""
                configureTaskLabel(customReward: customReward!)
                customRewardLabel.text = customReward?.rewardName ?? ""
            }
        }
    }
    
    weak var delegate: CustomRewardTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

//MARK: UI Methods
extension CustomRewardTableViewCell {
    func configureTaskLabel(customReward: RewardsData) {
        switch customReward.taskName {
        case "Walk":
            taskLabel.text = String(format: "Task: %@ steps", customReward.task )
        case "Run":
            taskLabel.text = String(format: "Task: %@ miles", customReward.task )
        case "Swim":
            taskLabel.text = String(format: "Task: %@ laps", customReward.task )
        case "Bike":
            taskLabel.text = String(format: "Task: %@ miles", customReward.task )
            //Sabarish
//        case .none:
//            break
//        case .some(_):
//            taskLabel.text = customReward.task
        default:
            taskLabel.text = customReward.task
        }
        
        rewardImageView.imageFromURL(urlString: customReward.taskImage ?? "")
    }
}

//MARK: Action Methods
extension CustomRewardTableViewCell {
    @IBAction func claimButtonTapped(_ sender: UIButton) {
        delegate?.didTapClaimButton(customRewardTableViewCell: self)
    }
}
