//
//  NotificationTableViewCell.swift
//  harmoney
//
//  Created by Prema Ravikumar on 07/02/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var indicatorImageView: UIImageView!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBAction func onClickbtnDelete(_ sender: Any) {
        
        print("delete")
    }
   
    
    var notification: NotificationDash? {
        didSet {
            configureNotification()
        }
    }
    
    //MARK: - Lifecycle methods
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    //MARK: - Private methods
    
    private func configureNotification() {
        
        if let url = URL(string: notification?.profilePictureURL ?? "") {
            profileImageView.imageFromURL(urlString: url.absoluteString)
        } else {
            switch notification?.type {
            case 1:
                profileImageView.image = UIImage(named: "footsteps")
            case 2:
                profileImageView.image = UIImage(named: "selectedBought")
            case 3:
                profileImageView.image = UIImage(named: "shareOutfilled2")
            case 4:
                profileImageView.image = UIImage(named: "harmoneycircle")
            case 5:
                profileImageView.image = UIImage(named: "harmoneycircle")
            case 6:
                let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
                
                if creditType == 1 {
                    profileImageView.image = UIImage(named: "soccer")

                } else if creditType == 2 {
                    profileImageView.image = UIImage(named: "diamond")

                } else {
                    profileImageView.image = UIImage(named: "touchdown")
                }
//                profileImageView.image = UIImage(named: "diamond")
            default:
                profileImageView.image = UIImage(named: "harmoneycircle")
                break
            }
            
            profileImageView.image = UIImage(named: "footsteps")
        }
        
        indicatorImageView.isHidden = true

        if notification?.type == 10 || notification?.type == 11 || notification?.type == 12 {
            if notification?.request?.status == 1 {
                indicatorImageView.isHidden = true
            } else {
                indicatorImageView.isHidden = true//false
            }
        }
        var message = notification?.message
        
        if let emojiUnicode = notification?.emoji?.uniCode {
            message = "\(message ?? "") \(emojiUnicode.decodeEmoji())"
        }
        
        titleLabel.text = message
        let str = notification?.createdDateTime?.UTCToLocal(incomingFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outGoingFormat: "MMM d, yyyy h:mm a")
        dateLabel.text = str
        
        titleLabel.font = UIFont.futuraPTMediumFont(size: 17)
        titleLabel.textColor = UIColor.black
        
        if notification?.isRead == 1{
            titleLabel.font = UIFont.futuraPTLightFont(size: 17)
            titleLabel.textColor = UIColor.darkGray
        }
    }
}

extension String {
    subscript(_ range: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: max(0, range.lowerBound))
        let end = index(start, offsetBy: min(self.count - range.lowerBound,
                                             range.upperBound - range.lowerBound))
        return String(self[start..<end])
    }

    subscript(_ range: CountablePartialRangeFrom<Int>) -> String {
        let start = index(startIndex, offsetBy: max(0, range.lowerBound))
         return String(self[start...])
    }
    
    //MARK:- Convert UTC To Local Date by passing date formats value
    func UTCToLocal(incomingFormat: String, outGoingFormat: String) -> String {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = incomingFormat
      dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

      let dt = dateFormatter.date(from: self)
      dateFormatter.timeZone = TimeZone.current
      dateFormatter.dateFormat = outGoingFormat

      return dateFormatter.string(from: dt ?? Date())
    }

    
}
