//
//  EditGenderTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/27/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class EditGenderTableViewCell: UITableViewCell {

    @IBOutlet weak var genderLbl: UILabel!

    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleBtn: UIButton!
    var parentVC : EditSettingsViewController?
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        // Initialization code
    }
    func setUI(){
        genderLbl.font = ConstantString.editTitleFont
        maleBtn.titleLabel?.font = ConstantString.editTextFont
        femaleBtn.titleLabel?.font = ConstantString.editTextFont
        maleBtn.layer.cornerRadius = 8.0
        femaleBtn.layer.cornerRadius = 8.0
        maleBtn.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        maleBtn.layer.borderWidth = 1
        femaleBtn.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        femaleBtn.layer.borderWidth = 1
        maleBtn.addTarget(self, action: #selector(genderClick(sender:)), for: .touchUpInside)
        femaleBtn.addTarget(self, action: #selector(genderClick(sender:)), for: .touchUpInside)
    }
   @objc func handleGenderButtons(sender:UIButton)  {
        maleBtn.backgroundColor = .white
    maleBtn.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
    
        femaleBtn.backgroundColor = .white
    femaleBtn.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
    
        sender.backgroundColor = #colorLiteral(red: 0.06666666667, green: 0.1294117647, blue: 0.2, alpha: 1)
    sender.setTitleColor(.white, for: .normal)
    parentVC?.profileUpdate.gender = sender.titleLabel?.text
    }
    
    @objc func genderClick(sender:UIButton){
        handleGenderButtons(sender: sender)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
