//
//  EditTfTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/27/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class EditTfTableViewCell: UITableViewCell{

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var editTextField: UITextField!
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        // Initialization code
    }
    func setUI(){
       
        nameLbl.font = ConstantString.editTitleFont
        editTextField.font = ConstantString.editTextFont
       // profileCompletnLbl.font = ConstantString.descriptionFont
       // editButton.titleLabel?.font = ConstantString.descriptionFont
       // profilePentgLbl.font = ConstantString.descriptionFont
        
        //profileImgView?.layer.cornerRadius = 60
        //  profileImgView?.layer.borderWidth = 1.0
         // profileImgView?.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 1)
        
    }
    


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
