//
//  EditDateTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/27/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class EditDateTableViewCell: UITableViewCell{
    
    
    var profileUpdate = ProfileUpdateObject()
    @IBOutlet weak var dateLbl: UILabel!
    var parentVc : EditSettingsViewController?

    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var monthTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    let datePickerView = UIDatePicker()
    var stringDate : String?{
        didSet{
            if self.stringDate != nil && self.stringDate?.count ?? 0 > 0 {
                self.splitDateToTF()
            }
        }
    }
   // let datePicker = UIDatePicker()
    
   
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        
       //showDatePicker()
        //monthTextField.inputView = datePicker
//        monthTextField.addTarget(self, action: #selector(dp(_:)), for: .allEvents)
//        dateTextField.addTarget(self, action: #selector(dp(_:)), for: .allEvents)
//        yearTextField.addTarget(self, action: #selector(dp(_:)), for: .allEvents)
        datePickerView.datePickerMode = .date
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        var components = DateComponents()
        components.year = -100
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = Date()
        if #available(iOS 13.4, *) {
            datePickerView.preferredDatePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        monthTextField.inputView = datePickerView
        yearTextField.inputView = datePickerView
        dateTextField.inputView = datePickerView
        
        self.monthTextField.tintColor = .clear
        self.yearTextField.tintColor = .clear
        self.dateTextField.tintColor = .clear
        
    }
   

    func setUI() {
        dateLbl.font = ConstantString.editTitleFont
        dateLbl.text = ConstantString.dateofBirth
        monthTextField.font = ConstantString.editTextFont
        dateTextField.font = ConstantString.editTextFont
        yearTextField.font = ConstantString.editTextFont
        
    }


       @objc func handleDatePicker(sender: UIDatePicker) {
            let fulDate = DateFormatter()
            fulDate.dateFormat = "MMM dd YYYY"
            stringDate = fulDate.string(from: sender.date)
       }
    
    func splitDateToTF()  {
        
        let arr : [String] = (stringDate?.components(separatedBy: " "))!
        monthTextField.text = arr[0]
        yearTextField.text = arr[2]
        dateTextField.text = arr[1]
        parentVc?.profileUpdate.dateOfBirth = stringDate
    }
   
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
