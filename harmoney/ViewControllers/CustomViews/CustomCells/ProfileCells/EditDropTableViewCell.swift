//
//  EditDropTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/27/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class EditDropTableViewCell: UITableViewCell {

    @IBOutlet weak var editTf: UITextField!
    @IBOutlet weak var bankNameLbl: UILabel!
   
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        // Initialization code
    }
    func setUI(){
        bankNameLbl.font = ConstantString.editTitleFont
        bankNameLbl.text = ConstantString.bankName
        editTf.font = ConstantString.editTextFont
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
