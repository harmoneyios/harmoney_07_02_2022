//
//  LogoutTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/29/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class LogoutTableViewCell: UITableViewCell {
    @IBOutlet weak var logoutLbl: UILabel!
    @IBOutlet weak var appVersionLbl: UILabel!
    
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        // Initialization code
    }
    func setUI(){
        //logoutLbl.font = ConstantString.descriptionFont
        appVersionLbl.font = ConstantString.smallTitle
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
