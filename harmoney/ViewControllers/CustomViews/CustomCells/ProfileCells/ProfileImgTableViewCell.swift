//
//  ProfileImgTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/24/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ProfileImgTableViewCell: UITableViewCell,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var editButton: UIButton!

    var parentVC : MyPfrofileViewController?
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUI(){
        //profileImgView.layer.cornerRadius = 45.0
        editButton.layer.cornerRadius = 15.0
       // profileCompletnLbl.font = ConstantString.descriptionFont
        editButton.titleLabel?.font = ConstantString.descriptionFont
       // profilePentgLbl.font = ConstantString.descriptionFont
        
        profileImgView?.layer.cornerRadius = profileImgView.frame.size.width/2
          profileImgView?.layer.borderWidth = 1.0
          profileImgView?.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 0.5)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
