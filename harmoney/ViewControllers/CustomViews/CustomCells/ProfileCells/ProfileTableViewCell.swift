//
//  ProfileTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/24/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var customLbl: UILabel!
    @IBOutlet weak var customTextField: UITextField!
    @IBOutlet weak var bgView: UIView!
       
      var parentVC : MyPfrofileViewController?
       override func awakeFromNib() {
           setUI()
           super.awakeFromNib()
           // Initialization code
//        bgView.layer.cornerRadius = 5
//        bgView.layer.borderWidth = 1
//        bgView.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 0.5)
        
    }
       func setUI(){
           customLbl.font = ConstantString.labelTitileFont
           customTextField.font = ConstantString.editTextFont18
        
        
//         maleButton.titleLabel?.font = ConstantString.descriptionFont
//           femaleButton.titleLabel?.font = ConstantString.descriptionFont
//           maleButton.layer.cornerRadius = 15.0
//          femaleButton.layer.cornerRadius = 15.0
//           maleButton.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 1)
//           femaleButton.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 1)
//           maleButton.layer.borderWidth = 1.0
//           femaleButton.layer.borderWidth = 1.0
  
            
       }
    func enableEditProfileUI()  {
        customTextField.layer.borderWidth = 1
        customTextField.layer.borderColor = #colorLiteral(red: 0.862745098, green: 0.8823529412, blue: 0.9450980392, alpha: 1)
        customTextField.isEnabled = true
       // maleButton.isEnabled = true
       // femaleButton.isEnabled = true
        paddingView()
    }
    
//    @IBAction func maleClick(_ sender: Any) {
//        selectMaleOrfemale(isMale: true)
//       
//    }
//    @IBAction func femaleClick(_ sender: Any) {
//        selectMaleOrfemale(isMale: false)
//    }
//    func selectMaleOrfemale(isMale male:Bool) {
//        if(male){
//            maleButton.backgroundColor = #colorLiteral(red: 0.1509042382, green: 0.3234898448, blue: 0.4761670828, alpha: 1)
//            maleButton.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            
//            femaleButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            femaleButton.titleLabel?.textColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 1)
//            parentVC?.profileUpdate.gender = "Male"
//        }else{
//           femaleButton.backgroundColor = #colorLiteral(red: 0.1509042382, green: 0.3234898448, blue: 0.4761670828, alpha: 1)
//           femaleButton.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//           
//           maleButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//           maleButton.titleLabel?.textColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 1)
//             parentVC?.profileUpdate.gender = "FeMale"
//        }
//    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func paddingView() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: customTextField.frame.height))
        customTextField.leftView = paddingView
        customTextField.leftViewMode = .always
    }

}


class ShadowView: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }

    private func setupShadow() {
        self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
