//
//  FyiDataTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/29/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class FyiDataTableViewCell: UITableViewCell {

    @IBOutlet weak var fyiTextView: UILabel!
    @IBOutlet weak var fyiNameLbl: UILabel!
    @IBOutlet weak var fyiSwitch: UIView!
    @IBOutlet weak var motionSwitch: UISwitch!
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        // Initialization code
        
    }
    func setUI() {
        fyiNameLbl.font = ConstantString.labelTitileFont
        fyiTextView.font = ConstantString.editTitleFont
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
