//
//  EditDoneTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/27/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class EditDoneTableViewCell: UITableViewCell {

    @IBOutlet weak var doneBtn: UIButton!
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        // Initialization code
    }
    func setUI(){
        doneBtn.titleLabel?.text = ConstantString.done
        doneBtn.titleLabel?.font = ConstantString.editTextFont
        
    }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
