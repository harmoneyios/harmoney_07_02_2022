//
//  SaveTableViewCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 24/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class SaveTableViewCell: UITableViewCell {

    @IBOutlet weak var saveProfileButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
