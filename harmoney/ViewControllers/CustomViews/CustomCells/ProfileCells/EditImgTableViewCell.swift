//
//  EditImgTableViewCell.swift
//  Harmoney
//
//  Created by sramika mangalapurapu on 5/27/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class EditImgTableViewCell: UITableViewCell, UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    var parentVC : EditSettingsViewController?
    @IBOutlet weak var editLbl: UILabel!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var editImgView: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!
    override func awakeFromNib() {
        setUI()
        super.awakeFromNib()
        // Initialization code
    }
    func setUI(){
        //profileImgView.layer.cornerRadius = 45.0
       // editButton.layer.cornerRadius = 15.0
       // profileCompletnLbl.font = ConstantString.descriptionFont
      //  editButton.titleLabel?.font = ConstantString.descriptionFont
       editLbl.font = ConstantString.descriptionFont
        editLbl.text = ConstantString.editProfile
        editLbl.font = ConstantString.titleFontEdit
        editImgView?.layer.cornerRadius = editImgView.frame.size.width/2
          editImgView?.layer.borderWidth = 1.0
          editImgView?.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.8549019608, blue: 0.862745098, alpha: 1)
        
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
      
        @IBAction func cameraBtnClick(_ sender: Any) {
            showUploadImageOptions()
        }
        
        func showUploadImageOptions() {
            let alert = UIAlertController.init(title: "Upload Image", message: "", preferredStyle: .actionSheet)
            let action1 = UIAlertAction.init(title: "Select Picture", style: .default) { (action) in
                self.selectPictureFromLibraryMtd()
            }
            let action2 = UIAlertAction.init(title: "Capture Image", style: .default) { (action) in
                self.captureProfileImageMtd()
            }
            let action3 = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(action1)
            alert.addAction(action2)
            alert.addAction(action3)
            parentVC?.present(alert, animated: true, completion: nil)
        }
        
        func selectPictureFromLibraryMtd() {
           let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
                  
           imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
           
           imagePicker.allowsEditing = true 
           
           parentVC!.present(imagePicker, animated:true){
               
           }
        }
        
        func captureProfileImageMtd() {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    
            imagePicker.allowsEditing = true
            
            parentVC!.present(imagePicker, animated:true){
                
            }
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
               
            var image : UIImage!

            if let img = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as? UIImage
            {
                image = img

            }
            else if let img = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage
            {
                image = img
            }
            parentVC?.profileUpdate.profileImage = image
            parentVC?.settingsTableView.reloadData()
            picker.dismiss(animated: true,completion: nil)
     }
    
//               if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
//                   parentVC?.profileUpdate.profileImage = image
//                   parentVC?.settingsTableView.reloadData()
//               }
//                picker.dismiss(animated: true) {
//
//                }
//           }
           
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

            var image : UIImage!

        if let img = info[UIImagePickerController.InfoKey.editedImage.rawValue] as? UIImage
            {
                image = img

            }
        else if let img = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage
            {
                image = img
            }
            parentVC?.profileUpdate.profileImage = image
            parentVC?.settingsTableView.reloadData()
            picker.dismiss(animated: true,completion: nil)
     }

    }



