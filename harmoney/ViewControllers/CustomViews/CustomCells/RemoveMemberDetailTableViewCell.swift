//
//  RemoveMemberDetailTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 23/06/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class RemoveMemberDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var memberDetailValuelabel: UILabel!
    @IBOutlet weak var memberDetailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
