//
//  shareContentCell.swift
//  Harmoney
//
//  Created by Mac on 21/04/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ShareContentCell: UITableViewCell {
    
    @IBOutlet weak var shareBorderView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var inviteBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor(red: 220/250, green: 225/250, blue: 241/250, alpha: 1.0).cgColor
        
        borderView.layer.cornerRadius = 10.0
        
        let shadowPath = UIBezierPath(rect: borderView.bounds)
        borderView.layer.masksToBounds = false
        borderView.layer.shadowColor =  UIColor(red: 106/250, green: 162/250, blue: 172/250, alpha: 0.9).cgColor
        borderView.layer.shadowOpacity = 0.09
        borderView.layer.shadowPath = shadowPath.cgPath

        
        shareBorderView.layer.borderWidth = 1
        shareBorderView.layer.borderColor = UIColor(red: 220/250, green: 225/250, blue: 241/250, alpha: 1.0).cgColor
        
        shareBorderView.layer.cornerRadius = 25.0
        
        let shadowPathTwo = UIBezierPath(rect: shareBorderView.bounds)
        shareBorderView.layer.masksToBounds = false
        shareBorderView.layer.shadowColor =  UIColor(red: 106/250, green: 162/250, blue: 172/250, alpha: 0.9).cgColor
        shareBorderView.layer.shadowOpacity = 0.09
        shareBorderView.layer.shadowPath = shadowPathTwo.cgPath

        
        inviteBtn.layer.cornerRadius = 12
        
        let fullString = NSMutableAttributedString(string: "Post it on Social Media and earn ")
        var myMutableString = NSMutableAttributedString()
        let secondiconImage = UIImage(named: "h-black")!
        let secondicon = NSTextAttachment()
        secondicon.bounds = CGRect(x: 0, y: (descriptionLabel.font.capHeight - 12).rounded() / 2, width: 12, height: 12)
        secondicon.image = secondiconImage
        let hrString = NSAttributedString(attachment: secondicon)
        fullString.append(hrString)
        myMutableString = NSMutableAttributedString(string: "1\n")
        myMutableString.setAttributes([NSAttributedString.Key.font : UIFont(name: "FuturaPT-Bold", size: 15)!
            , NSAttributedString.Key.foregroundColor : UIColor(red: 22 / 255.0, green: 29 / 255.0, blue: 42 / 255.0, alpha: 1.0)], range: NSRange(location:0,length:1))
        fullString.append(myMutableString)
        
        myMutableString = NSMutableAttributedString(string: "Share your winning moments and earnings to the world!")

        myMutableString.setAttributes([NSAttributedString.Key.font : UIFont(name: "FuturaPT-Book", size: 14)!
            , NSAttributedString.Key.foregroundColor : UIColor(red: 143 / 255.0, green: 152 / 255.0, blue: 179 / 255.0, alpha: 1.0)], range: NSRange(location:0,length:53))

        fullString.append(myMutableString)
        descriptionLabel.attributedText = fullString
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

    func animatePostButton() {
        if inviteBtn.isHidden == false {
            self.inviteBtn.alpha = 1
            UIView.animate(withDuration: 1, delay: 0, options: [.allowUserInteraction, .curveEaseInOut,.autoreverse, .repeat], animations: {
                self.inviteBtn.alpha = 0.2
            }, completion: nil)
        }
    }
}
