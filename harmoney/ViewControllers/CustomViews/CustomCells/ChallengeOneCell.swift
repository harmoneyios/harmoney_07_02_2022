//
//  ChallengeOneCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 16/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Lottie

enum ChallengeState {
    case notaccepted
    case accepted
    case completed
    case updated
}

class ChallengeOneCell: UITableViewCell {
    
    @IBOutlet weak var remainingStepsLabel: UILabel!
    @IBOutlet weak var taskTypeLabel: UILabel!
    @IBOutlet weak var taskCountLabel: UILabel!
    @IBOutlet weak var xpLabel: UILabel!
    @IBOutlet weak var taskTypeImageView: UIImageView!
    @IBOutlet weak var circleProgressView: CircularProgress!
    @IBOutlet weak var xpImageView: UIImageView!

    //Outlet
    @IBOutlet weak var borderView: UIView!

    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var tickImageView: UIImageView!
    @IBOutlet weak var stepsLabel: UILabel!

    var animationView =  AnimationView()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        circleProgressView.addSubview(animationView)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.leadingAnchor.constraint(equalTo: circleProgressView.leadingAnchor,constant: 5).isActive = true
        animationView.trailingAnchor.constraint(equalTo: circleProgressView.trailingAnchor,constant: 5).isActive = true
        animationView.topAnchor.constraint(equalTo: circleProgressView.topAnchor,constant:  -5).isActive = true
        animationView.bottomAnchor.constraint(equalTo: circleProgressView.bottomAnchor, constant: -5).isActive = true


        let path = Bundle.main.path(forResource: "steps",
                                           ofType: "json") ?? ""
               animationView.animation = Animation.filepath(path)
               animationView.contentMode = .scaleAspectFit
               animationView.loopMode = .loop
               animationView.isHidden = true
        
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor(red: 220/250, green: 225/250, blue: 241/250, alpha: 1.0).cgColor
        
        borderView.layer.cornerRadius = 10.0
        
        let shadowPath = UIBezierPath(rect: borderView.bounds)
        borderView.layer.masksToBounds = false
        borderView.layer.shadowColor =  UIColor(red: 106/250, green: 162/250, blue: 172/250, alpha: 0.9).cgColor
        borderView.layer.shadowOpacity = 0.09
        borderView.layer.shadowPath = shadowPath.cgPath

        doneBtn.layer.cornerRadius = 12
        doneBtn.backgroundColor = #colorLiteral(red: 0.151170969, green: 0.3193956912, blue: 0.4761708975, alpha: 1)
        
        self.circleProgressView.progressColor = UIColor(red: 146.0/255.0, green: 190.0/255.0, blue: 145.0/255.0, alpha: 1.0)
    }
    
    func showChallengeData(obj:ChallengeData) {
        xpLabel.text = "\(obj.points?.xp ?? 0)"
        switch obj.cId {
        case "1":
            taskCountLabel.text = "Task: \(obj.cInfo?.taskCount ?? "") Steps"
        case "2":
            taskCountLabel.text = "Task: Dance and Workout"
        case "3":
            taskCountLabel.text = "Task: Earn h1 and give to a charity"
            taskCountLabel.adjustsFontSizeToFitWidth = true
        default:
            print("")
            
        }
                
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

    func hideContents() {
        taskTypeLabel.alpha = 0
        taskCountLabel.alpha = 0
        stepsLabel.alpha = 0
        doneBtn.alpha = 0
        tickImageView.alpha = 0
        xpLabel.alpha = 0
        remainingStepsLabel.alpha = 0
        xpImageView.alpha = 0
    }

    func showContents() {
            taskTypeLabel.alpha = 1
            taskCountLabel.alpha = 1
            stepsLabel.alpha = 1
            tickImageView.alpha = 1
            xpLabel.alpha = 1
            remainingStepsLabel.alpha = 1
            xpImageView.alpha = 1
        }

    func animateButton() {
            if doneBtn.isHidden == false {
                self.doneBtn.alpha = 1
                UIView.animate(withDuration: 1, delay: 0, options: [.allowUserInteraction, .curveEaseInOut,.autoreverse, .repeat], animations: {
                    self.doneBtn.alpha = 0.2
                }, completion: nil)
            }
        }
    
    //MARK: -ChallengeOne
    func challengeOneDefaultSetUp(challengeState:ChallengeState, isNextChallengeViewed :Bool) {
        
            
            remainingStepsLabel.text = ""
            stepsLabel.text = ""
            animateButton()
            taskTypeImageView.image = UIImage.init(named: "footsteps_small")
            circleProgressView.trackColor = UIColor(red: 22.0/255.0, green: 97.0/255.0, blue: 20.0/255.0, alpha: 0.2)
            circleProgressView.progressColor = UIColor(red: 187.0/255.0, green: 194.0/255.0, blue: 214.0/255.0, alpha: 1.0)
            taskTypeLabel.text = "Walk"
        taskTypeLabel.textColor = UIColor(red: 146/250, green: 190/250, blue: 145/250, alpha: 1.0)
        
        
        switch challengeState {
        case .accepted:
                tickImageView.isHidden = true
                doneBtn.isHidden = true
                taskTypeImageView.isHidden = true
                animationView.play()
                animationView.isHidden = false
            
        case .completed:
                animationView.isHidden = true
                animationView.stop()
                tickImageView.isHidden = false
                taskTypeImageView.isHidden = false
                if(isNextChallengeViewed){
                    doneBtn.isHidden = true
                }else{
                    doneBtn.isHidden = false
                    doneBtn.setTitle("What's Next", for: .normal)
                    animateButton()
                }
        default:
            print("Challenge 1")
            
        }
               
        
        
    }
    
    
    //MARK: -ChallengeTwo
    func challengeTwoDefaultSetUp(challengeState:ChallengeState, isNextChallengeViewed :Bool) {
        
         taskTypeLabel.text = "Dancercize"
         remainingStepsLabel.text = ""
         stepsLabel.text = ""
         taskTypeImageView.image = UIImage.init(named: "girl-dancing_small")
         circleProgressView.trackColor = UIColor(red: 187.0/255.0, green: 194.0/255.0, blue: 214.0/255.0, alpha: 1.0)
         circleProgressView.progressColor = UIColor(red: 187.0/255.0, green: 194.0/255.0, blue: 214.0/255.0, alpha: 1.0)
        taskTypeLabel.textColor = UIColor(red: 71/250, green: 122/250, blue: 192/250, alpha: 1.0)
        animateButton()
        
        switch challengeState {
        case .accepted:
                doneBtn.setTitle("Play video", for: .normal)
                tickImageView.isHidden = true
        case .completed:
                tickImageView.isHidden = false
                doneBtn.isHidden = true
                circleProgressView.setProgressWithAnimation(duration: 0.0, value: 100)
                tickImageView.isHidden = false
                if(isNextChallengeViewed){
                    doneBtn.isHidden = true
                }else{
                    doneBtn.isHidden = false
                    doneBtn.setTitle("What's Next", for: .normal)
                }
        default:
            print("Challenge 2")
        }
        
    }
    
    
    //MARK: -ChallengeThree
    func challengeThreeDefaultSetUp(challengeState:ChallengeState, isNextChallengeViewed :Bool) {

        
             taskTypeLabel.text =  "Share"
             remainingStepsLabel.text = ""
             stepsLabel.text = ""
             taskTypeImageView.image = UIImage.init(named: "shareOutfilled2")
             circleProgressView.trackColor = UIColor(red: 187.0/255.0, green: 194.0/255.0, blue: 214.0/255.0, alpha: 1.0)
             circleProgressView.progressColor = UIColor(red: 187.0/255.0, green: 194.0/255.0, blue: 214.0/255.0, alpha: 1.0)
            remainingStepsLabel.text = ""
            stepsLabel.text = ""
            taskTypeLabel.textColor = UIColor(red: 131/250, green: 134/250, blue: 142/250, alpha: 1.0)
            animateButton()
        switch challengeState {
        case .accepted:
             doneBtn.setTitle("What's Next", for: .normal)
            
        case .completed:
             doneBtn.isHidden = true
             circleProgressView.setProgressWithAnimation(duration: 0.0, value: 100)
             tickImageView.isHidden = false
                
        default:
            print("Challenge 3")
        }
        
    }
    
    
 
    
}
