//
//  MysteryChesttableviewcell.swift
//  harmoney
//
//  Created by Prema Ravikumar on 07/02/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class MysteryChesttableviewcell: UITableViewCell {
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var openBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor(red: 220/250, green: 225/250, blue: 241/250, alpha: 1.0).cgColor
        
        borderView.layer.cornerRadius = 10.0
        
        let shadowPath = UIBezierPath(rect: borderView.bounds)
        borderView.layer.masksToBounds = false
        borderView.layer.shadowColor =  UIColor(red: 106/250, green: 162/250, blue: 172/250, alpha: 0.9).cgColor
        borderView.layer.shadowOpacity = 0.09
        borderView.layer.shadowPath = shadowPath.cgPath

        openBtn.layer.cornerRadius = 12

        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

    func animateButton() {
        if openBtn.isHidden == false {
            self.openBtn.alpha = 1
            UIView.animate(withDuration: 1, delay: 0, options: [.allowUserInteraction, .curveEaseInOut,.autoreverse, .repeat], animations: {
                self.openBtn.alpha = 0.2
            }, completion: nil)
        }
    }
}
