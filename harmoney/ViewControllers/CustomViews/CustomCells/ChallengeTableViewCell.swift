//
//  ChallengeTableViewCell.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/20/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import UICircularProgressRing

protocol ChallengeTableViewCellDelegate: class {
    func didTapChallengeTableViewCellWhatsNextButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String)
    func didTapChallengeTableViewCellStartButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String)
    func didTapChallengeTableViewCellVideoButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String)
    func didTapChallengeTableViewCellRemoveButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String)
    func didTapChallengeTableViewCellCustomButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String)
    func didTapChallengeTableViewCellNotAprovedButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String)
}

class ChallengeTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var vwStackHBucks: UIStackView!
    @IBOutlet weak var challengeImageView: UIImageView!
    @IBOutlet weak var challengeTitleLabel: UILabel!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var experianceLabel: UILabel!
    @IBOutlet weak var diamondLabel: UILabel!
    @IBOutlet weak var completedCheckMarkImageVIew: UIImageView!
    @IBOutlet weak var whatsNextButton: UIButton!
    @IBOutlet weak var harmoneyImageView: UIImageView!
    @IBOutlet weak var harmoneyBucksLabel: UILabel!
    @IBOutlet weak var circleProgressView: UICircularProgressRing!
    @IBOutlet weak var challengeNotAprovedButton: UIButton!
    @IBOutlet weak var challengeNotAprovedLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    
    @IBOutlet weak var deciedeBtn: UIButton!
    @IBOutlet weak var assingedBy: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var progressValueLabel: UILabel!
    @IBOutlet weak var diamondImageview: UIImageView!
    
    @IBOutlet weak var toUserImage: UIImageView!
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var challengesLbl: UILabel!
    @IBOutlet weak var dailyChllegeTopView: UIView!
    //MARK: - Properties
    
    weak var delegate: ChallengeTableViewCellDelegate?
    var playVideo = false
    var isButtonSelected:Bool = false
    var dailyChallegeTableView:Bool = false
    var fitnessTableview:Bool = false
    var isFromController:String = "viewcontrollerName"
    public var onboardingChallenge: ChallengeData? {
        didSet {
            dailyChllegeTopView.isHidden = true
            topViewHeightConstraint.constant = 0
            let challenge = onboardingChallenge
            let taskCount = Int(onboardingChallenge?.taskCount ?? "0") ?? 0
            print(challenge)
            playVideo = false
            removeButton.isHidden = true
            whatsNextButton.isHidden = false
            completedCheckMarkImageVIew.isHidden = true
            circleProgressView.style = .ontop
            circleProgressView.value = 0.0
            circleProgressView.maxValue = CGFloat(taskCount)
            challengeNotAprovedButton.isHidden = true
            challengeNotAprovedLabel.isHidden = true
            experianceLabel.text = "\(onboardingChallenge?.points?.xp ?? 0)"
            harmoneyBucksLabel.text = "\(onboardingChallenge?.points?.hBucks ?? 0)"
            diamondLabel.text = "\(onboardingChallenge?.points?.jem ?? 0)"
            HarmonySingleton.tid = onboardingChallenge?.cId ?? "0"
            challengeTitleLabel.textColor = .black
            assingedBy.isHidden = true
            deciedeBtn.isHidden = true
            let creditType = HarmonySingleton.shared.cofigModel?.data.creditType

            if creditType == 1 {
                self.diamondImageview.image = UIImage(named: "soccer")

            } else if creditType == 2 {
                self.diamondImageview.image = UIImage(named: "diamond")

            } else {
                self.diamondImageview.image = UIImage(named: "touchdown")

            }
//            diamondImageview.image = UIImage(named: "diamond")

            //Walk
            if onboardingChallenge?.cType == "1" {
                challengeImageView.image = UIImage(named: "walk_icon")
                challengeTitleLabel.text = "Walk"
                taskLabel.text = "Task: \(onboardingChallenge?.cName ?? "")"
            //Dance
            } else if onboardingChallenge?.cType == "2" {
                challengeImageView.image = UIImage(named: "selectedBought")
                challengeTitleLabel.text = "Offer"
                taskLabel.text = "Task: \(onboardingChallenge?.cName ?? "")"
            //Save goal
            } else if onboardingChallenge?.cType == "3" {
                let str1 = NSAttributedString.init(string: "Task: Earn ")
                let amountImageAttachment = NSTextAttachment()
                amountImageAttachment.image = UIImage(named: "harmoney")
                amountImageAttachment.bounds = CGRect(x: 0, y: 0, width: 14, height: 14)
                let amountImageAttachmentString = NSAttributedString(attachment: amountImageAttachment)
                let str2 = NSAttributedString(string: " 1", attributes: [NSAttributedString.Key.font: UIFont(name: "FuturaPT-Bold", size: 16)!])
                let str3 = NSAttributedString.init(string: " and give to a charity")
            
                let finalStr = NSMutableAttributedString(attributedString: str1)
                finalStr.append(amountImageAttachmentString)
                finalStr.append(str2)
                finalStr.append(str3)
               
                challengeImageView.image = UIImage(named: "shareOutfilled2")
                challengeTitleLabel.text = "Share"
                taskLabel.attributedText = finalStr //"Task: Earn h1 and give to a charity"
                taskLabel.adjustsFontSizeToFitWidth = true
            }

            //Not Started
            if onboardingChallenge?.cStatus == nil {
                whatsNextButton.setTitle("Let's do it", for: .normal)
            //In Progress
            } else if onboardingChallenge?.cStatus == 0 {
                progressValueLabel.isHidden = false
                challengeImageView.isHidden = true
                whatsNextButton.isHidden = true
                isButtonSelected = false
                if onboardingChallenge?.cType == "1" {
//                    let completedCount = PedometerManager.sharedInstance.completedSteps
                    if completedChallangeFootSteps >= taskCount {
                        completedCheckMarkImageVIew.isHidden = false
                        progressValueLabel.isHidden = true
                        challengeImageView.isHidden = false
                        whatsNextButton.isHidden = false
                        whatsNextButton.setTitle("What's next", for: .normal)
                        
                        progressValueLabel.text = String(taskCount)
                        circleProgressView.value = CGFloat(taskCount)
                    } else {
                        completedCheckMarkImageVIew.isHidden = true
                        whatsNextButton.isHidden = true
                        progressValueLabel.isHidden = false
                        challengeImageView.isHidden = true
                        progressValueLabel.text = String(completedChallangeFootSteps)
                        circleProgressView.value = CGFloat(completedChallangeFootSteps)
                    }
                
                } else if onboardingChallenge?.cType == "2" {
//                    playVideo = true
//                    whatsNextButton.isHidden = false
//                    whatsNextButton.setTitle("Play Video", for: .normal)
                    if onboardingChallenge?.cType == "2" && onboardingChallenge?.cStatus == 0 {
                        whatsNextButton.isHidden = false
                        progressValueLabel.isHidden = true
                        challengeImageView.isHidden = false
                        whatsNextButton.setTitle("What's next", for: .normal)
                    }
                    
                } else if onboardingChallenge?.cType == "3" {
                    whatsNextButton.isHidden = false
                    progressValueLabel.isHidden = true
                    challengeImageView.isHidden = false
                    whatsNextButton.setTitle("What's next", for: .normal)
                }
            //Completed
            } else if onboardingChallenge?.cStatus == 1 || onboardingChallenge?.cStatus == 2{
                if onboardingChallenge?.cType == "2" && onboardingChallenge?.cStatus == 1 {
                    whatsNextButton.isHidden = false
                    whatsNextButton.setTitle("What's next", for: .normal)
                    isButtonSelected = false

                }else{
                    whatsNextButton.isHidden = true
                    completedCheckMarkImageVIew.isHidden = false
                    challengeTitleLabel.textColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.value = 0.0
                    isButtonSelected = false

                }
            }
            vwStackHBucks.isHidden = false
            if harmoneyBucksLabel.text == "0" || harmoneyBucksLabel.text == "" || harmoneyBucksLabel.text?.first == "0"{
                vwStackHBucks.isHidden = true
            }
//            whatsNextButton.isHidden = false
//            whatsNextButton.setTitle("What's next", for: .normal)
        }
    }
    
    
    public var dailyChallengeCompleted: DailyChallenge? {
        didSet {
       
            dailyChllegeTopView.isHidden = true
            topViewHeightConstraint.constant = 0
            let userDefaultsBackGroundColor = UserDefaults.standard
            let backColor = userDefaultsBackGroundColor.string(forKey: "setBackGroundDaily")
            if backColor == "yes"{
                contentView.backgroundColor = UIColor(red: 236/255.0, green: 243/255.0, blue: 251/255.0, alpha: 1)
                dailyChllegeTopView.isHidden = false
                topViewHeightConstraint.constant = 33
                challengesLbl.text = "Daily Challenge"
                dailyChllegeTopView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                mainView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            }

            assingedBy.isHidden = true
            deciedeBtn.isHidden = true
            let userDefaults = UserDefaults.standard
            let userDefaultsCompledailytChallengeCompleteexperianceLabel = userDefaults.string(forKey: "completeDailyChallengeChatboatexperianceLabel")
            let userDefaultsCompledailytChallengeCompletediamondLabel = userDefaults.string(forKey: "completeDailyChallengeChatboatdiamondLabel")
            let userDefaultsCompledailytChallengeCompletechallengeTitleLabel = userDefaults.string(forKey: "completeDailyChallengeChatboatchallengeTitleLabel")
            let userDefaultsCompledailytChallengeCompletetaskLabel = userDefaults.string(forKey: "completeDailyChallengeChatboattaskLabel")
            let userDefaultsCompledailytChallengeharmoneyBucksLabel = userDefaults.string(forKey: "CompledailytChallengeChatboatharmoneyBucksLabel")
            whatsNextButton.isHidden = true
            challengeNotAprovedButton.isHidden = true
            challengeNotAprovedLabel.isHidden = true
            completedCheckMarkImageVIew.isHidden = true
            circleProgressView.style = .ontop
            circleProgressView.value = 0.0
//            circleProgressView.maxValue = CGFloat(taskCount)
            experianceLabel.text = userDefaultsCompledailytChallengeCompleteexperianceLabel
            harmoneyBucksLabel.text = userDefaultsCompledailytChallengeharmoneyBucksLabel
            diamondLabel.text = userDefaultsCompledailytChallengeCompletediamondLabel
            challengeTitleLabel.text = userDefaultsCompledailytChallengeCompletechallengeTitleLabel
            if let taskValue = userDefaultsCompledailytChallengeCompletetaskLabel{
                taskLabel.text = "Task: \(taskValue)"
            }
            
            challengeTitleLabel.textColor = .black

//            let creditType = HarmonySingleton.shared.cofigModel?.data.creditType

//            if creditType == 1 {
                self.diamondImageview.image = UIImage(named: "soccer")
//
//            } else if creditType == 2 {
//                self.diamondImageview.image = UIImage(named: "diamond")
//
//            } else {
//                self.diamondImageview.image = UIImage(named: "touchdown")
//
//            }
////            diamondImageview.image = UIImage(named: "diamond")
//            //Walk
//            if dailyChallenge?.challegeTaskType == 1 {
//                challengeImageView.image = UIImage(named: "walk_icon")
//            //Dance
//            } else if dailyChallenge?.challegeTaskType == 3 {
//                challengeImageView.image = UIImage(named: "girl-dancing_big")
//            }
//
//            //Not started
//            if dailyChallenge?.userCompletionStatus == .notStarted {
//                whatsNextButton.setTitle("Let's do it", for: .normal)
//            //Accepted
//            } else if dailyChallenge?.userCompletionStatus == .accepted {
//
//                //Walk
//                if dailyChallenge?.challegeTaskType == 1 {
//                    if(completedChallangeFootSteps >= Int(dailyChallenge!.taskValue)!){
//                      whatsNextButton.setTitle("What's next", for: .normal)
//                    }else{
//                        whatsNextButton.isHidden = true
//                        progressValueLabel.text = String(completedChallangeFootSteps)
//                        circleProgressView.value = CGFloat(completedChallangeFootSteps)
//                    }
//                //Dance
//                }else if dailyChallenge?.challegeTaskType == 2 {
//                    whatsNextButton.setTitle("What's next", for: .normal)
//                }else if dailyChallenge?.challegeTaskType == 3 {
//                    playVideo = true
//                    whatsNextButton.isHidden = false
//                    whatsNextButton.setTitle("Play video", for: .normal)
//                }
//
//            //Completed
//            } else if dailyChallenge?.userCompletionStatus == .completed {
//
//            //Rewards
//            } else if dailyChallenge?.userCompletionStatus == .rewards {
                whatsNextButton.isHidden = true
            progressValueLabel.isHidden = true
            challengeImageView.isHidden = false
                completedCheckMarkImageVIew.isHidden = false
//                //What's next
//            } else if dailyChallenge?.userCompletionStatus == .whatsNext {
//                whatsNextButton.setTitle("What's next", for: .normal)
//                completedCheckMarkImageVIew.isHidden = false
//                challengeTitleLabel.textColor = UIColor.harmoneyLightGreenColor
//                circleProgressView.value = 0.0
//            }
            
            vwStackHBucks.isHidden = false
            if harmoneyBucksLabel.text == "0" || harmoneyBucksLabel.text == "" || harmoneyBucksLabel.text?.first == "0"{
                vwStackHBucks.isHidden = true
            }
        }
    }
    
    public var dailyChallenge: DailyChallenge? {
        didSet {
            dailyChllegeTopView.isHidden = true
            topViewHeightConstraint.constant = 0

            let userDefaultsBackGroundColor = UserDefaults.standard
            let backColor = userDefaultsBackGroundColor.string(forKey: "setBackGroundDaily")
            if backColor == "yes"{
                contentView.backgroundColor = UIColor(red: 236/255.0, green: 243/255.0, blue: 251/255.0, alpha: 1)
                dailyChllegeTopView.isHidden = false
                topViewHeightConstraint.constant = 33
                challengesLbl.text = "Daily Challenge"
                dailyChllegeTopView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                mainView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]

            }
            let challenge = dailyChallenge
            let taskCount = Int(dailyChallenge?.taskValue ?? "0") ?? 0
            let completedCount = dailyChallenge?.taskCurrentValue ?? 0
            playVideo = false
            removeButton.isHidden = true
            whatsNextButton.isHidden = false
            challengeNotAprovedButton.isHidden = true
            challengeNotAprovedLabel.isHidden = true
            completedCheckMarkImageVIew.isHidden = true
            circleProgressView.style = .ontop
            circleProgressView.value = 0.0
            circleProgressView.maxValue = CGFloat(taskCount)
            experianceLabel.text = "\(dailyChallenge?.xp ?? 0)"
            harmoneyBucksLabel.text = "\(dailyChallenge?.hBucks ?? 0)"
            diamondLabel.text = "\(dailyChallenge?.gem ?? "0")"
            challengeTitleLabel.text = dailyChallenge?.title
            taskLabel.text = "Task: \(dailyChallenge?.subTitle ?? "")"
            HarmonySingleton.tid = dailyChallenge?.challengeID ?? "0"
            challengeTitleLabel.textColor = .black
            assingedBy.isHidden = true
            deciedeBtn.isHidden = true
            let creditType = HarmonySingleton.shared.cofigModel?.data.creditType

            if creditType == 1 {
                self.diamondImageview.image = UIImage(named: "soccer")

            } else if creditType == 2 {
                self.diamondImageview.image = UIImage(named: "diamond")

            } else {
                self.diamondImageview.image = UIImage(named: "touchdown")

            }
//            diamondImageview.image = UIImage(named: "diamond")
            //Walk
            if dailyChallenge?.challegeTaskType == 1 {
                challengeImageView.image = UIImage(named: "walk_icon")
            //Dance
            } else if dailyChallenge?.challegeTaskType == 3 {
                challengeImageView.image = UIImage(named: "girl-dancing_big")
            }else if dailyChallenge?.challegeTaskType == 2{
                challengeImageView.image = UIImage(named: "hydratebig")
            }
            
//            challengeImageView.set_image(dailyChallenge?.image ?? "")
            //Not started
            if dailyChallenge?.userCompletionStatus == .notStarted   {
                if dailyChallenge?.challegeTaskType == 1 { //&& HarmonySingleton.shared.dashBoardData.data?.userType == .parent
                
                    whatsNextButton.setTitle("Let's do it", for: .normal)
                    whatsNextButton.isHidden = true
                }else{
                    whatsNextButton.setTitle("Let's do it", for: .normal)
                    whatsNextButton.isHidden = false
                }
              
                
                
            //Accepted
            } else if dailyChallenge?.userCompletionStatus == .accepted {
                
                //Walk
                if dailyChallenge?.challegeTaskType == 1 {
                    if(completedChallangeFootSteps >= Int(dailyChallenge!.taskValue)!){
                        completedCheckMarkImageVIew.isHidden = false
                        progressValueLabel.isHidden = true
                        challengeImageView.isHidden = false
                        whatsNextButton.isHidden = false
                        whatsNextButton.setTitle("What's next", for: .normal)
                        
                        progressValueLabel.text = String(taskCount)
                        circleProgressView.value = CGFloat(taskCount)
                    }else{
                        completedCheckMarkImageVIew.isHidden = true
                        whatsNextButton.isHidden = true
                        progressValueLabel.isHidden = false
                        challengeImageView.isHidden = true
                        progressValueLabel.text = String(completedChallangeFootSteps)
                        circleProgressView.value = CGFloat(completedChallangeFootSteps)
                    }
                //Dance
                }else if dailyChallenge?.challegeTaskType == 2 {
                    whatsNextButton.setTitle("What's next", for: .normal)
                }else if dailyChallenge?.challegeTaskType == 3 {
                    playVideo = true
                    whatsNextButton.isHidden = false
                    whatsNextButton.setTitle("Play video", for: .normal)
                }
                
            //Completed
            } else if dailyChallenge?.userCompletionStatus == .completed {
                whatsNextButton.isHidden = true
                completedCheckMarkImageVIew.isHidden = false
            //Rewards
            } else if dailyChallenge?.userCompletionStatus == .rewards {
                whatsNextButton.isHidden = true
                completedCheckMarkImageVIew.isHidden = false
                //What's next
            } else if dailyChallenge?.userCompletionStatus == .whatsNext {
                whatsNextButton.setTitle("What's next", for: .normal)
                completedCheckMarkImageVIew.isHidden = false
                challengeTitleLabel.textColor = UIColor.harmoneyLightGreenColor
                circleProgressView.value = 0.0
            }
            
            vwStackHBucks.isHidden = false
            if harmoneyBucksLabel.text == "0" || harmoneyBucksLabel.text == "" || harmoneyBucksLabel.text?.first == "0"{
                vwStackHBucks.isHidden = true
            }
        }
    }
    
    public var personalChallengeChatboat: PersonalChallenge? {
        didSet {
//            if modeofChallenge == 1 {
//                BaseViewController().locationmanage.startUpdatingLocation()
//            } else {
//                BaseViewController().locationmanage.stopUpdatingLocation()
//            }
            dailyChllegeTopView.isHidden = true
            topViewHeightConstraint.constant = 0

//            let userDefaultsBackGroundColor = UserDefaults.standard
//            let backColor = userDefaultsBackGroundColor.string(forKey: "setBackGround")
//            if backColor == "yes"{
                contentView.backgroundColor = UIColor(red: 236/255.0, green: 243/255.0, blue: 251/255.0, alpha: 1)
                dailyChllegeTopView.isHidden = false
                topViewHeightConstraint.constant = 33
                challengesLbl.text = "Fitness Challenge"
                dailyChllegeTopView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                mainView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]

//            }
            
            let formatter = Utilities.addNumberFormatter()
            let formatedChallengeTask = formatter.string(from: (personalChallenge?.data.challengeTask ?? 0) as NSNumber)
            challengeTitleLabel.textColor = .black
            assingedBy.isHidden = true
            deciedeBtn.isHidden = true
            let userDefaults = UserDefaults.standard
            let myString = userDefaults.string(forKey: "completeFitnessChatboat")
            if myString == "completed"{
                let userDefaults = UserDefaults.standard
                let challengeType = userDefaults.string(forKey: "challengeType")
                let challengeTask = userDefaults.string(forKey: "challengeTask")
                let challengeexperiance = userDefaults.string(forKey: "experiance")
                let challengeharmoneyBucks = userDefaults.string(forKey: "harmoneyBucks")
                let challengeharmoneydiamond = userDefaults.string(forKey: "diamond")
                completedCheckMarkImageVIew.isHidden = false
                if challengeType == "walk"{
                    challengeImageView.image = UIImage(named: "walk_icon")
                    challengeTitleLabel.text = "Walk"
                    taskLabel.text =  String(format: "Task: \(challengeTask ?? "") steps")
                    experianceLabel.text = challengeexperiance
                    progressValueLabel.textColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.outerRingColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.innerRingColor = UIColor.harmoneyLightGreenColor
                    refreshButton.isHidden = true
                    whatsNextButton.isHidden = true
                    self.diamondImageview.image = UIImage(named: "soccer")
                    challengeNotAprovedButton.isHidden = true
                    challengeNotAprovedLabel.isHidden = true
                    harmoneyBucksLabel.text = challengeharmoneyBucks
                    
                    diamondLabel.text = challengeharmoneydiamond
                    return;
                }
                if challengeType == "swim"{
                    challengeImageView.image = UIImage(named: "swim_icon")
                    challengeTitleLabel.text = "Swim"
                    taskLabel.text =  String(format: "Task: \(challengeTask ?? "") steps")
                    experianceLabel.text = challengeexperiance
                    progressValueLabel.textColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.outerRingColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.innerRingColor = UIColor.harmoneyLightGreenColor
                    refreshButton.isHidden = true
                    whatsNextButton.isHidden = true
                    self.diamondImageview.image = UIImage(named: "soccer")
                    challengeNotAprovedButton.isHidden = true
                    challengeNotAprovedLabel.isHidden = true
                    harmoneyBucksLabel.text = challengeharmoneyBucks
                    
                    diamondLabel.text = challengeharmoneydiamond
                    return;
                    
                }
                if challengeType == "run"{
                    challengeImageView.image = UIImage(named: "run_icon")
                    challengeTitleLabel.text = "Run"
                    taskLabel.text =  String(format: "Task: \(challengeTask ?? "") steps")
                    experianceLabel.text = challengeexperiance
                    progressValueLabel.textColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.outerRingColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.innerRingColor = UIColor.harmoneyLightGreenColor
                    refreshButton.isHidden = true
                    whatsNextButton.isHidden = true
                    self.diamondImageview.image = UIImage(named: "soccer")
                    challengeNotAprovedButton.isHidden = true
                    challengeNotAprovedLabel.isHidden = true
                    harmoneyBucksLabel.text = challengeharmoneyBucks
                    
                    diamondLabel.text = challengeharmoneydiamond
                }
                if challengeType == "bike"{
                    challengeImageView.image = UIImage(named: "bike_icon")
                    challengeTitleLabel.text = "Bike"
                    taskLabel.text =  String(format: "Task: \(challengeTask ?? "") steps")
                    experianceLabel.text = challengeexperiance
                    progressValueLabel.textColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.outerRingColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.innerRingColor = UIColor.harmoneyLightGreenColor
                    refreshButton.isHidden = true
                    whatsNextButton.isHidden = true
                    self.diamondImageview.image = UIImage(named: "soccer")
                    challengeNotAprovedButton.isHidden = true
                    challengeNotAprovedLabel.isHidden = true
                    harmoneyBucksLabel.text = challengeharmoneyBucks
                    
                    diamondLabel.text = challengeharmoneydiamond
                    
                }
            }
            circleProgressView.style = .ontop
            circleProgressView.maxValue = CGFloat(personalChallenge?.data.challengeTask ?? 0)

//            let formatter = Utilities.addNumberFormatter()
//            let formatedChallengeTask = formatter.string(from: (personalChallenge?.data.challengeTask ?? 0) as NSNumber)
            challengeTitleLabel.textColor = .black
            harmoneyBucksLabel.text = formatter.string(from: (personalChallenge?.data.harmoneyBucks ?? 0) as NSNumber)
            
            diamondLabel.text = "\(personalChallenge?.data.diamond ?? 0)"
            let creditType = HarmonySingleton.shared.cofigModel?.data.creditType

            if creditType == 1 {
                self.diamondImageview.image = UIImage(named: "soccer")

            } else if creditType == 2 {
                self.diamondImageview.image = UIImage(named: "diamond")

            } else {
                self.diamondImageview.image = UIImage(named: "touchdown")

            }
//            diamondImageview.image = UIImage(named: "diamond")

            if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue == RegistrationType.child.rawValue {
                
                if personalChallenge?.data.harmoneyBucks ?? 0 > 0 {
                    if personalChallenge?.data.challengeAcceptType?.rawValue == ChallengeAcceptType.padding.rawValue || personalChallenge?.data.challengeAcceptType?.rawValue == ChallengeAcceptType.decline.rawValue {
                        challengeNotAprovedButton.isHidden = true
                        progressValueLabel.isHidden = false
                        challengeImageView.isHidden = true
                        whatsNextButton.isHidden = true
                        challengeNotAprovedLabel.isHidden = false
                    } else {
                        challengeNotAprovedButton.isHidden = false
                        progressValueLabel.isHidden = true
                        challengeImageView.isHidden = false
                        whatsNextButton.isHidden = false
                        challengeNotAprovedLabel.isHidden = true
                    }
                } else {
                    challengeNotAprovedButton.isHidden = true
                    progressValueLabel.isHidden = true
                    challengeImageView.isHidden = false
                    whatsNextButton.isHidden = false
                    challengeNotAprovedLabel.isHidden = true
                }
            } else {
                challengeNotAprovedButton.isHidden = true
                progressValueLabel.isHidden = true
                challengeImageView.isHidden = false
                whatsNextButton.isHidden = false
                challengeNotAprovedLabel.isHidden = true
            }
            
            if personalChallenge?.data.isCompleted != 0 {
                whatsNextButton.setTitle("What's next", for: .normal)
                refreshButton.isHidden = true
                completedCheckMarkImageVIew.isHidden = false
                removeButton.isHidden = true
                challengeTitleLabel.textColor = UIColor.harmoneyLightGreenColor
                progressValueLabel.text = String(personalChallenge?.data.challengeTask ?? 0)
                circleProgressView.value = CGFloat(personalChallenge?.data.challengeTask ?? 0)
            } else {
                progressValueLabel.isHidden = false
                refreshButton.isHidden = true
                challengeImageView.isHidden = true
                switch personalChallenge?.data.challengeType {
                
                    case .walk:
                        progressValueLabel.text = isButtonSelected == true ? String(format: "%.0f", personalChallenge?.data.challengeProgress ?? 0) : String(format: "%.0f", personalChallenge?.data.challengeProgress ?? 0)
                        break;
                    case .swim:
                        progressValueLabel.text = isButtonSelected == true ? String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0) : String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0)
                        break;
                    case .run:
                        progressValueLabel.text = isButtonSelected == true ? String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0) : String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0)
                        break;
                    case .bike:
                        progressValueLabel.text = isButtonSelected == true ? String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0) : String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0)
                        break;
                    case .none:
                        break;
                }
                isButtonSelected = false
                circleProgressView.value = CGFloat(personalChallenge?.data.challengeProgress ?? 0)
                print("get progress level-->",(CGFloat(personalChallenge?.data.challengeProgress ?? 0)))
                removeButton.isHidden = false
                completedCheckMarkImageVIew.isHidden = true
                
                whatsNextButton.isHidden = true
            }
            
            switch personalChallenge?.data.challengeType {
                case .walk:
                    challengeImageView.image = UIImage(named: "walk_icon")
                    challengeTitleLabel.text = personalChallenge?.data.challengeType.rawValue
                    taskLabel.text =  String(format: "Task: %@ steps", formatedChallengeTask ?? 0)
                    experianceLabel.text = String(format: "%d", personalChallenge?.data.experiance ?? 0)
                    progressValueLabel.textColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.outerRingColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.innerRingColor = UIColor.harmoneyLightGreenColor
                    
                    break;
                case .swim:
                    challengeImageView.image = UIImage(named: "swim_icon")
                    challengeTitleLabel.text = personalChallenge?.data.challengeType.rawValue
                    taskLabel.text = String(format: "Task: %@ laps", formatedChallengeTask ?? 0)
                    experianceLabel.text = String(format: "%d", personalChallenge?.data.experiance ?? 0)
                    progressValueLabel.textColor = UIColor.harmoneySwimBlueColor
                    circleProgressView.outerRingColor = UIColor.harmoneySwimBlueColor
                    circleProgressView.innerRingColor = UIColor.harmoneySwimBlueColor
                    
                    break;
                case .run:
                    challengeImageView.image = UIImage(named: "run_icon")
                    challengeTitleLabel.text = personalChallenge?.data.challengeType.rawValue
                    taskLabel.text = String(format: "Task: %@ miles", formatedChallengeTask ?? 0)
                    experianceLabel.text = String(format: "%d", personalChallenge?.data.experiance ?? 0)
                    
                    progressValueLabel.textColor = UIColor.harmoneyRunGrayColor

                    circleProgressView.outerRingColor = UIColor.harmoneyRunGrayColor
                    circleProgressView.innerRingColor = UIColor.harmoneyRunGrayColor
                    
                    break;
                case .bike:
                    challengeImageView.image = UIImage(named: "bike_icon")
                    challengeTitleLabel.text = personalChallenge?.data.challengeType.rawValue
                    taskLabel.text = String(format: "Task: %@ mile", formatedChallengeTask ?? 0)
                    experianceLabel.text = String(format: "%d", personalChallenge?.data.experiance ?? 0)
                    
                    progressValueLabel.textColor = UIColor.harmoneyBikeBrownColor

                    circleProgressView.outerRingColor = UIColor.harmoneyBikeBrownColor
                    circleProgressView.innerRingColor = UIColor.harmoneyBikeBrownColor
                    
                    break;
                case .none:
                    break;
            }
            vwStackHBucks.isHidden = false
            if harmoneyBucksLabel.text == "0" || harmoneyBucksLabel.text == "" || harmoneyBucksLabel.text?.first == "0"{
                vwStackHBucks.isHidden = true
            }
        }
    }
    
    public var personalChallenge: PersonalChallenge? {
        didSet {
            
            dailyChllegeTopView.isHidden = true
            topViewHeightConstraint.constant = 0

            let userDefaultsBackGroundColor = UserDefaults.standard
            let backColor = userDefaultsBackGroundColor.string(forKey: "setBackGround")
            if backColor == "yes"{
                contentView.backgroundColor = UIColor(red: 236/255.0, green: 243/255.0, blue: 251/255.0, alpha: 1)
                dailyChllegeTopView.isHidden = false
                topViewHeightConstraint.constant = 33
                challengesLbl.text = "Fitness Challenge"
                dailyChllegeTopView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                mainView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]

            }
//            if modeofChallenge == 1 {
//                BaseViewController().locationmanage.startUpdatingLocation()
//            } else {
//                BaseViewController().locationmanage.stopUpdatingLocation()
//            }
            circleProgressView.style = .ontop
            circleProgressView.maxValue = CGFloat(personalChallenge?.data.challengeTask ?? 0)

            let formatter = Utilities.addNumberFormatter()
            let formatedChallengeTask = formatter.string(from: (personalChallenge?.data.challengeTask ?? 0) as NSNumber)
            challengeTitleLabel.textColor = .black
            harmoneyBucksLabel.text = formatter.string(from: (personalChallenge?.data.harmoneyBucks ?? 0) as NSNumber)
            HarmonySingleton.tid = personalChallenge?.data.challengeId ?? "0"
            diamondLabel.text = "\(personalChallenge?.data.diamond ?? 0)"
            let creditType = HarmonySingleton.shared.cofigModel?.data.creditType

            if creditType == 1 {
                self.diamondImageview.image = UIImage(named: "soccer")

            } else if creditType == 2 {
                self.diamondImageview.image = UIImage(named: "diamond")

            } else {
                self.diamondImageview.image = UIImage(named: "touchdown")

            }
//            diamondImageview.image = UIImage(named: "diamond")

            if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue == RegistrationType.child.rawValue {
                
                if personalChallenge?.data.harmoneyBucks ?? 0 > 0 {
                    if personalChallenge?.data.challengeAcceptType?.rawValue == ChallengeAcceptType.padding.rawValue || personalChallenge?.data.challengeAcceptType?.rawValue == ChallengeAcceptType.decline.rawValue {
                        challengeNotAprovedButton.isHidden = false
                        progressValueLabel.isHidden = false
                        challengeImageView.isHidden = true
                        whatsNextButton.isHidden = true
                        challengeNotAprovedLabel.isHidden = false
                    } else {
                        challengeNotAprovedButton.isHidden = true
                        progressValueLabel.isHidden = true
                        challengeImageView.isHidden = false
                        whatsNextButton.isHidden = false
                        challengeNotAprovedLabel.isHidden = true
                    }
                } else {
                    challengeNotAprovedButton.isHidden = true
                    progressValueLabel.isHidden = true
                    challengeImageView.isHidden = false
                    whatsNextButton.isHidden = false
                    challengeNotAprovedLabel.isHidden = true
                }
            } else {
                challengeNotAprovedButton.isHidden = true
                progressValueLabel.isHidden = true
                challengeImageView.isHidden = false
                whatsNextButton.isHidden = false
                challengeNotAprovedLabel.isHidden = true
            }
            
            if personalChallenge?.data.isCompleted != 0 {
                whatsNextButton.setTitle("What's next", for: .normal)
                completedCheckMarkImageVIew.isHidden = false
                removeButton.isHidden = true
                challengeTitleLabel.textColor = UIColor.harmoneyLightGreenColor
                progressValueLabel.text = String(personalChallenge?.data.challengeTask ?? 0)
                circleProgressView.value = CGFloat(personalChallenge?.data.challengeTask ?? 0)
                if personalChallenge?.data.isCompleted == 2{
                    whatsNextButton.isHidden = true
                    refreshButton.isHidden = true
                }
            } else {
                progressValueLabel.isHidden = false
                challengeImageView.isHidden = true
                switch personalChallenge?.data.challengeType {
                
                    case .walk:
                        progressValueLabel.text = isButtonSelected == true ? String(format: "%.0f", personalChallenge?.data.challengeProgress ?? 0) : String(format: "%.0f", personalChallenge?.data.challengeProgress ?? 0)
                        break;
                    case .swim:
                        progressValueLabel.text = isButtonSelected == true ? String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0) : String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0)
                        break;
                    case .run:
                        progressValueLabel.text = isButtonSelected == true ? String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0) : String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0)
                        break;
                    case .bike:
                        progressValueLabel.text = isButtonSelected == true ? String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0) : String(format: "%.2f", personalChallenge?.data.challengeProgress ?? 0)
                        break;
                    case .none:
                        break;
                }
                isButtonSelected = false
                circleProgressView.value = CGFloat(personalChallenge?.data.challengeProgress ?? 0)
                print("get progress level-->",(CGFloat(personalChallenge?.data.challengeProgress ?? 0)))
                removeButton.isHidden = false
                completedCheckMarkImageVIew.isHidden = true
                whatsNextButton.isHidden = false
                if personalChallenge?.data.isStarted == Harmoney.ChallengeStatus.started
                {
                    whatsNextButton.isHidden = true
                }
                
            }
            
            switch personalChallenge?.data.challengeType {
                case .walk:
                    challengeImageView.image = UIImage(named: "walk_icon")
                    challengeTitleLabel.text = personalChallenge?.data.challengeType.rawValue
                    taskLabel.text =  String(format: "Task: %@ steps", formatedChallengeTask ?? 0)
                    experianceLabel.text = String(format: "%d", personalChallenge?.data.experiance ?? 0)
                    progressValueLabel.textColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.outerRingColor = UIColor.harmoneyLightGreenColor
                    circleProgressView.innerRingColor = UIColor.harmoneyLightGreenColor
                    
                    break;
                case .swim:
                    challengeImageView.image = UIImage(named: "swim_icon")
                    challengeTitleLabel.text = personalChallenge?.data.challengeType.rawValue
                    taskLabel.text = String(format: "Task: %@ laps", formatedChallengeTask ?? 0)
                    experianceLabel.text = String(format: "%d", personalChallenge?.data.experiance ?? 0)
                    progressValueLabel.textColor = UIColor.harmoneySwimBlueColor
                    circleProgressView.outerRingColor = UIColor.harmoneySwimBlueColor
                    circleProgressView.innerRingColor = UIColor.harmoneySwimBlueColor
                    
                    break;
                case .run:
                    challengeImageView.image = UIImage(named: "run_icon")
                    challengeTitleLabel.text = personalChallenge?.data.challengeType.rawValue
                    taskLabel.text = String(format: "Task: %@ miles", formatedChallengeTask ?? 0)
                    experianceLabel.text = String(format: "%d", personalChallenge?.data.experiance ?? 0)
                    
                    progressValueLabel.textColor = UIColor.harmoneyRunGrayColor

                    circleProgressView.outerRingColor = UIColor.harmoneyRunGrayColor
                    circleProgressView.innerRingColor = UIColor.harmoneyRunGrayColor
                    
                    break;
                case .bike:
                    challengeImageView.image = UIImage(named: "bike_icon")
                    challengeTitleLabel.text = personalChallenge?.data.challengeType.rawValue
                    taskLabel.text = String(format: "Task: %@ mile", formatedChallengeTask ?? 0)
                    experianceLabel.text = String(format: "%d", personalChallenge?.data.experiance ?? 0)
                    
                    progressValueLabel.textColor = UIColor.harmoneyBikeBrownColor

                    circleProgressView.outerRingColor = UIColor.harmoneyBikeBrownColor
                    circleProgressView.innerRingColor = UIColor.harmoneyBikeBrownColor
                    
                    break;
                case .none:
                    break;
            }
         
            if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue == RegistrationType.child.rawValue {
                if personalChallenge?.data.isAccepted == 0 && personalChallenge?.data.isCompleted == 1
                {
                    challengeNotAprovedButton.isHidden = false
                    challengeNotAprovedLabel.isHidden = false
                }
            }
            assingedBy.isHidden = true
            deciedeBtn.isHidden = true
            if personalChallenge?.data.noAction == true
            {
                whatsNextButton.isHidden = true
                refreshButton.isHidden = true
                removeButton.isHidden = true
                toUserImage.isHidden = false
                assingedBy.isHidden = true
                completedCheckMarkImageVIew.isHidden = false
                self.completedCheckMarkImageVIew.image = UIImage(named: "pending")
                toUserImage.imageFromURL(urlString: personalChallenge?.data.taskTo?.profilePic ?? "")
                
                toUserImage.layer.cornerRadius = toUserImage.frame.size.height/2
                toUserImage.layer.borderWidth = 0
        //        answerViewProfileImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
                toUserImage.clipsToBounds = true
//                if let assignedBy = personalChallenge?.data.taskTo?.name{
//
//                    assingedBy.isHidden = false
//                    assingedBy.text = "Assigned To: \(assignedBy)"
               
//                decideBtn.isHidden = false
//
//                }
                assingedBy.isHidden = true
                deciedeBtn.isHidden = true
                if personalChallenge?.data.isAccepted == 0 && personalChallenge?.data.isCompleted == 1
                {
                    assingedBy.isHidden = false
                    assingedBy.text = "Your kid has completed the challenge"
                    deciedeBtn.isHidden = false
                }
            }else{
                deciedeBtn.isHidden = true
                assingedBy.isHidden = true
//                whatsNextButton.isHidden = false
                refreshButton.isHidden = false
//                removeButton.isHidden = false
                toUserImage.isHidden = true
                completedCheckMarkImageVIew.isHidden = true
                if personalChallenge?.data.noAction == false
                {
                    if let assignedBy = personalChallenge?.data.taskFrom?.name{
                       
                        if HarmonySingleton.shared.dashBoardData.data?.userType == .child{
                            assingedBy.isHidden = true // child him self assign no need
                            assingedBy.text = "Assigned By: \(assignedBy)"
                          

                        }else{
                            assingedBy.isHidden = true
                            assingedBy.text = "Assigned By: \(assignedBy)"

                        }
                    }
                }
            }
            vwStackHBucks.isHidden = false
            
            if harmoneyBucksLabel.text == "0" || harmoneyBucksLabel.text == "" || harmoneyBucksLabel.text?.first == "0"{
                vwStackHBucks.isHidden = true
            }
            if personalChallenge?.data.isCompleted == 2{
                whatsNextButton.isHidden = true
                refreshButton.isHidden = true
            }
            
            if (personalChallenge?.data.isAccepted == 0 && personalChallenge?.data.isCompleted == 1) || (personalChallenge?.data.isAccepted == 1 && personalChallenge?.data.isCompleted == 2) || (personalChallenge?.data.isAccepted == 0 && personalChallenge?.data.isCompleted == 2) || (personalChallenge?.data.isAccepted == 1 && personalChallenge?.data.isCompleted == 1)
            {
                completedCheckMarkImageVIew.isHidden = true
                refreshButton.isHidden = true
            }else{
                
                if let isStarted = personalChallenge?.data.isStarted{
                    if isStarted == .started{
                        completedCheckMarkImageVIew.image = UIImage(named: "pending")
                        completedCheckMarkImageVIew.isHidden = false
                    }else{
                        refreshButton.isHidden = true
                    }
                }
                
            }
            
            
            
        }
    }
    
    var challengeCanStart: Bool = false {
        didSet {
            if challengeCanStart {
                progressValueLabel.isHidden = true
                challengeImageView.isHidden = false
                if personalChallenge?.data.noAction == true
                {
                    whatsNextButton.isHidden = true
                }else{
                    whatsNextButton.isHidden = false
                }
                
                whatsNextButton.setTitle("Let's do it", for: .normal)
                isButtonSelected = false
            }
        }
    }
    
    var challengeCanStartChatBoat: Bool = false {
        didSet {
            if challengeCanStartChatBoat {
                let userDefaults = UserDefaults.standard
                let myString = userDefaults.string(forKey: "completeFitnessChatboat")
                if myString == "completed"{
                    progressValueLabel.isHidden = true
                    challengeImageView.isHidden = true
                    whatsNextButton.isHidden = true
                    whatsNextButton.setTitle("Let's do it", for: .normal)
                    isButtonSelected = true
                    refreshButton.isHidden = true
                }
               

            }
        }
    }
    
    
    
    //MARK: - Lifecycle Methods

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - Action Methods
    
    func animateButton() {
        if whatsNextButton.isHidden == false {
            self.whatsNextButton.alpha = 1
            UIView.animate(withDuration: 1, delay: 0, options: [.allowUserInteraction, .curveEaseInOut,.autoreverse, .repeat], animations: {
                self.whatsNextButton.alpha = 0.2
            }, completion: nil)
        }
    }

    
    @IBAction func whatsNextButtonTapped(_ sender: UIButton) {
        if playVideo {
            delegate?.didTapChallengeTableViewCellVideoButton(challengeTableViewCell: self, fromController: isFromController)
            isButtonSelected = false

        } else {
            if challengeCanStart {
                if !isButtonSelected {
                    isButtonSelected = true
                    checkChallengeStatus()
//                    delegate?.didTapChallengeTableViewCellStartButton(challengeTableViewCell: self, fromController: isFromController)
                    
                }else{
                    isButtonSelected = false

                }
            } else {
                delegate?.didTapChallengeTableViewCellWhatsNextButton(challengeTableViewCell: self, fromController: isFromController)
                isButtonSelected = false

            }
        }
    }
    
    func checkChallengeStatus() {
        HarmonySingleton.shared.taskType = "letsdoit"
        let challengeStatusData = ChallengeStatusList(isTaskAlreadyRunning: false, taskType: 0, taskId: HarmonySingleton.tid, taskTitle: "", taskSubTitle: "", taskImage: "", displayMessage: "")
        PersonalFitnessManager.sharedInstance.checkChallengeStatus(challengeCheckdata: challengeStatusData) { (result) in
                   switch result {
                   case .success(_):
                    if HarmonySingleton.shared.checkChallengeStatus?.isTaskAlreadyRunning == false
                    {
                        self.delegate?.didTapChallengeTableViewCellStartButton(challengeTableViewCell: self, fromController: self.isFromController)
                    }else{
                        
                        if HarmonySingleton.shared.checkChallengeStatus?.taskType == 2
                        {
                            self.delegate?.didTapChallengeTableViewCellRemoveButton(challengeTableViewCell: self, fromController: self.isFromController)
                            self.isButtonSelected = false
                        }else{
                            self.delegate?.didTapChallengeTableViewCellCustomButton(challengeTableViewCell: self, fromController: HarmonySingleton.shared.checkChallengeStatus?.displayMessage ?? "")
                            self.isButtonSelected = false
                        }
                    }
                    print("success")
                   case .failure(let error):
//                       self.view.toast(error.localizedDescription)
                    
                   print(error)
                   }
               }
        
        
    }
    
    
    @IBAction func challengeNotAprovedButtonTapped(_ sender: UIButton) {
        delegate?.didTapChallengeTableViewCellNotAprovedButton(challengeTableViewCell: self, fromController: isFromController)
        isButtonSelected = false

    }
    
    @IBAction func removeButtonTapped(_ sender: UIButton) {
        HarmonySingleton.shared.taskType = "buttonTap"
        delegate?.didTapChallengeTableViewCellRemoveButton(challengeTableViewCell: self, fromController: isFromController)
        isButtonSelected = false

    }
}
