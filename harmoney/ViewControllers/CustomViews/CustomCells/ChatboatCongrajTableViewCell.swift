//
//  ChatboatCongrajTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 30/05/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class ChatboatCongrajTableViewCell: UITableViewCell {

    @IBOutlet weak var completedLabel: UILabel!
    @IBOutlet weak var harmoneyCountLabel: UILabel!
    @IBOutlet weak var gemCountLabel: UILabel!
    @IBOutlet weak var xpCountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var homeModel: HomeModel? {
        return HomeManager.sharedInstance.homeModel
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let userName = homeModel?.data.name{
            nameLabel.text = "Congrats \(userName),"

        }else{
            nameLabel.text = "Congrats"

        }
    }
    func choreCompleted()
    {
        let userDefaults = UserDefaults.standard
        let myString = userDefaults.string(forKey: "completeChoreChatboat")
        if myString == "completedChore"{
            
            let userDefaults = UserDefaults.standard
            let experianceStringChore = userDefaults.string(forKey: "experianceString")
            xpCountLabel.text = experianceStringChore
            var harmoneyStringChore = userDefaults.string(forKey: "harmoneyString")
            if harmoneyStringChore == ""{
                harmoneyStringChore = "0"
            }
            harmoneyCountLabel.text = harmoneyStringChore
            gemCountLabel.text = "0"
            let userDefaultstaskName = UserDefaults.standard
            if let experianceStringChoreCompletetaskName = userDefaultstaskName.string(forKey: "ChoreCompletetaskName"){
                completedLabel.text = "completing the \(experianceStringChoreCompletetaskName.replacingOccurrences(of: "Challenge", with: "")) challenge"

            }
        }
    }
  
    func fitnessCompleted()
    {
        let userDefaults = UserDefaults.standard
        let myString = userDefaults.string(forKey: "completeFitnessChatboat")
        if myString == "completed"{
            let userDefaults = UserDefaults.standard
            let challengeexperiance = userDefaults.string(forKey: "experiance")
            xpCountLabel.text = challengeexperiance
            let challengeharmoneyBucks = userDefaults.string(forKey: "harmoneyBucks")
            harmoneyCountLabel.text = challengeharmoneyBucks

            let challengeharmoneydiamond = userDefaults.string(forKey: "diamond")
            gemCountLabel.text = challengeharmoneydiamond
            
            let userDefaultschallengeType = UserDefaults.standard
            if let challengeType = userDefaultschallengeType.string(forKey: "challengeType"){
                completedLabel.text = "completing the \(challengeType.replacingOccurrences(of: "Challenge", with: "")) challenge"

            }
     
//            if challengeType == "walk"{
//                completedLabel.text = "completing the \(challengeType) challenge"
//
//            }
//           else if challengeType == "swim"{
//
//
//            }
//          else  if challengeType == "run"{
//
//            }
//          else  if challengeType == "bike"{
//
//
//            }
            
        }
    }
    
    func dailyCompleted()
    {
        let userDefaults = UserDefaults.standard
        if let userDefaultsCompledailytChallengeCompleteTitleLabel = userDefaults.string(forKey: "completeDailyChallengeChatboatchallengeTitleLabel"){
            completedLabel.text = "completing the \(userDefaultsCompledailytChallengeCompleteTitleLabel.replacingOccurrences(of: "Challenge", with: "")) challenge"

        }
        
        let userDefaultsCompledailytChallengeCompleteexperianceLabel = userDefaults.string(forKey: "completeDailyChallengeChatboatexperianceLabel")
        xpCountLabel.text = userDefaultsCompledailytChallengeCompleteexperianceLabel
        let userDefaultsCompledailytChallengeCompletediamondLabel = userDefaults.string(forKey: "completeDailyChallengeChatboatdiamondLabel")
        gemCountLabel.text = userDefaultsCompledailytChallengeCompletediamondLabel
        let userDefaultsCompledailytChallengeharmoneyBucksLabel = userDefaults.string(forKey: "CompledailytChallengeChatboatharmoneyBucksLabel")
        
        harmoneyCountLabel.text = userDefaultsCompledailytChallengeharmoneyBucksLabel
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
