//
//  choreAcceptAndPendingCell.swift
//  Harmoney
//
//  Created by Mac on 19/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class choreAcceptAndPendingCell: UITableViewCell {

    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var parentBorderView: UIView!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var attributeLabel: UILabel!
    @IBOutlet weak var pendingApprovalView: UIView!
    @IBOutlet weak var completedLabel: UILabel!

    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var assignToImg: UIImageView!
    
    @IBOutlet weak var decideBtn: UIButton!
    @IBOutlet weak var assignTo: UILabel!
    @IBOutlet weak var rmvNwBtn: UIButton!
    //    var attributedString = NSMutableAttributedString(string: "")
 

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUI()
    
    }
    func setUI(){
        
        pendingApprovalView.alpha = 0.8
        
        attributeLabel.textColor = UIColor(red: 22/250, green: 29/250, blue: 42/250, alpha: 1.0)
        
        parentBorderView.layer.borderColor = UIColor(red: 220/250, green: 225/250, blue: 241/250, alpha: 1.0).cgColor
        parentBorderView.layer.shadowColor = UIColor(red: 106/250, green: 162/250, blue: 172/250, alpha: 0.09).cgColor
        
        borderView.layer.borderColor = UIColor(red: 133/250, green: 191/250, blue: 142/250, alpha: 1.0).cgColor
        borderView.layer.borderWidth = 0.5
        borderView.layer.cornerRadius = 30.0

        removeBtn.layer.cornerRadius = 14.0
        doneBtn.layer.cornerRadius = 14.0

        doneBtn.layer.borderColor = UIColor(red: 30/250, green: 63/250, blue: 102/250, alpha: 1.0).cgColor
        doneBtn.layer.borderWidth = 1.0
        
//        decideBtn.layer.cornerRadius = 10.0
//
//        decideBtn.layer.borderColor = UIColor(red: 30/250, green: 63/250, blue: 102/250, alpha: 1.0).cgColor
//        decideBtn.layer.borderWidth = 1.0

    }
    
    
    func cellForRowChoreComplete( indexPath:ChoresListModel?, indexpath:IndexPath)  {

        pendingApprovalView.isHidden = true
        let userDefaults = UserDefaults.standard
        let myString = userDefaults.string(forKey: "completeChoreChatboat")
        if myString == "completedChore"{
            
            let userDefaults = UserDefaults.standard
            let ChoreCompletetaskName = userDefaults.string(forKey: "ChoreCompletetaskName")
            let experianceStringChore = userDefaults.string(forKey: "experianceString")
            let harmoneyStringChore = userDefaults.string(forKey: "harmoneyString")
           //            let indexData = indexPath?.data?[indexpath.row]
            doneBtn.isHidden = false
            removeBtn.isHidden = true
            removeBtn.setTitle("Remove", for: .normal)
            self.removeBtn.alpha = 1
        
         
        titleLabel.text = ChoreCompletetaskName
            if let ChoreCompleteDescription = userDefaults.string(forKey: "ChoreCompleteDescription"){
                descriptionLabel.text = "Task: \(ChoreCompleteDescription)"

            }

                
            let experianceString = experianceStringChore ?? ""
        var harmoneyString = harmoneyStringChore
            let diamondString = "1"
        
        if harmoneyString == "" || harmoneyString?.trim().count == 0{
            harmoneyString = "0"
        }
        let harmoneyAttachment = NSTextAttachment()
        
        harmoneyAttachment.image = UIImage(named: "harmoney")
        let boldFont = UIFont.futuraPTBoldFont(size: 16)
        
        let harmoneySize = harmoneyAttachment.image!.size
        
        harmoneyAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - harmoneySize.height) / 2, width: harmoneySize.width, height: harmoneySize.height)
        
        let harmoneyImageString = NSAttributedString(attachment: harmoneyAttachment)
        
        let attributedText = NSMutableAttributedString(string: "")
            
            if (harmoneyString != "0" && harmoneyString?.first != "0"){
                attributedText.append(harmoneyImageString)
                attributedText.append(NSAttributedString(string: " "))
                attributedText.append(NSAttributedString(string: harmoneyString ?? "" + " "))
            }

        let lightRange = (attributedText.string as NSString).range(of: attributedText.string)
        let boldRangeExperiance = (attributedText.string as NSString).range(of: experianceString)
        let boldRangeDiamond = (attributedText.string as NSString).range(of: diamondString)
        
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBoldFont(size: 14)], range:lightRange)
        attributedText.addAttributes([NSAttributedString.Key.font: boldFont], range:boldRangeExperiance)
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBoldFont(size: 16)], range:boldRangeDiamond)
            
        attributeLabel.attributedText = attributedText;
            
            if let harmoneyStringChore = userDefaults.string(forKey: "choreImgUrl"){
                typeImageView.imageFromURL(urlString: harmoneyStringChore)
            }
        }
    }
    
    func cellForRow( indexPath:ChoresListModel, indexpath:IndexPath, backgroundColor:String)  {

        let indexData = indexPath.data![indexpath.row]
        doneBtn.isHidden = false
        removeBtn.isHidden = true
        removeBtn.setTitle("Remove", for: .normal)
        self.removeBtn.alpha = 1
    
     
        
    titleLabel.text = indexData.task_name
    descriptionLabel.text = "Task: " + (indexData.task_subName ?? "Drink 3L water")
            
    let experianceString = indexData.task_xp_value ?? "200"
    var harmoneyString = indexData.harmoney_bucks  ?? "10"
    let diamondString = indexData.task_gems_value ?? "1"
    
    if harmoneyString == "" {
        harmoneyString = "0"
    }
//    let xpAttachment = NSTextAttachment()
    let harmoneyAttachment = NSTextAttachment()
//    let diamondAttachment = NSTextAttachment()
    
//    xpAttachment.image = UIImage(named: "xp")
    harmoneyAttachment.image = UIImage(named: "harmoney")
//    diamondAttachment.image = UIImage(named: "diamond")
    let boldFont = UIFont.futuraPTBoldFont(size: 16)
    
//    let imageSize = xpAttachment.image!.size
    let harmoneySize = harmoneyAttachment.image!.size
//    let image2Size = diamondAttachment.image!.size
    
//    xpAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - (imageSize.height - 8)) / 2, width: imageSize.width - 8, height: imageSize.height - 8)
    harmoneyAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - harmoneySize.height) / 2, width: harmoneySize.width, height: harmoneySize.height)
//    diamondAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - (image2Size.height - 5)) / 2, width: image2Size.width - 5, height: image2Size.height - 5)
    
//    let xpImageString = NSAttributedString(attachment: xpAttachment)
    let harmoneyImageString = NSAttributedString(attachment: harmoneyAttachment)
//    let diamondImageString = NSAttributedString(attachment: diamondAttachment)
    
    let attributedText = NSMutableAttributedString(string: "")
//    attributedText.append(NSAttributedString(string: experianceString + " "))
//    attributedText.append(xpImageString)
    
//    attributedText.append(NSAttributedString(string: "  "))
        
        if harmoneyString != "0" && harmoneyString.first != "0"{
            
    attributedText.append(harmoneyImageString)
    attributedText.append(NSAttributedString(string: " "))
    attributedText.append(NSAttributedString(string: harmoneyString + " "))
        }
//    attributedText.append(NSAttributedString(string: "  "))
//    attributedText.append(diamondImageString)
//    attributedText.append(NSAttributedString(string: diamondString + " "))
    
    let lightRange = (attributedText.string as NSString).range(of: attributedText.string)
    let boldRangeExperiance = (attributedText.string as NSString).range(of: experianceString)
    let boldRangeDiamond = (attributedText.string as NSString).range(of: diamondString)
    
    attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBoldFont(size: 14)], range:lightRange)
//    attributedText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.harmoneyLightOrangeColor], range:boldRangeExperiance)
    attributedText.addAttributes([NSAttributedString.Key.font: boldFont], range:boldRangeExperiance)
    attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBoldFont(size: 16)], range:boldRangeDiamond)
//    attributedText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.harmoneyLightOrangeColor], range:boldRangeDiamond)
    
    attributeLabel.attributedText = attributedText;
        typeImageView.imageFromURL(urlString: indexData.task_image_url ?? "")
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let dayObj = indexData.taskList?.filter({$0.date == formatter.string(from: Date())}).last {
            if dayObj.status == 1{
                doneBtn.isHidden = true
            }else{
                removeBtn.isHidden = true
                doneBtn.isHidden = false
            }
        }
        if HarmonySingleton.shared.dashBoardData.data?.userType == .child{
            if indexData.isAccepted == 1 && indexData.isCompleted == 1{
                removeBtn.isHidden = false
                doneBtn.isHidden = true
                removeBtn.setTitle("What's Next", for: .normal)
                animateButton()
            }
        }else{
            if indexData.isCompleted == 1{
                removeBtn.isHidden = false
                
                statusIcon.isHidden = true
                doneBtn.isHidden = true
                removeBtn.setTitle("What's Next", for: .normal)
                animateButton()
            }
        }
       
        if indexData.isCompleted == 2{
            removeBtn.isHidden = true
            doneBtn.isHidden = true
        }
        
        
          if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child {
             let acptd = (indexData.isAccepted == 0 && indexData.isCompleted == 1)
             let pendingApproval = indexData.isApproved ?? false
             if !pendingApproval || acptd {
                     pendingApprovalView.isHidden = false
                hideUnhideBtns(doneB: true, removeB: true)
                 
                 print("Accepted child chores")
              
                 }else{
                     pendingApprovalView.isHidden = true
                     print(" Rejected child chores")
                 }
      
             }else{
                 pendingApprovalView.isHidden = true
                 print(" Rejected child chores")
             }

        if harmoneyString == "0"{
            pendingApprovalView.isHidden = true
            
        }
        decideBtn.isHidden = true
        if indexData.noAction == true
        {
            
            doneBtn.isHidden = true
            removeBtn.isHidden = true
            rmvNwBtn.isHidden = true
            assignToImg.isHidden = false
            statusIcon.isHidden = false
            assignToImg.imageFromURL(urlString: indexData.taskTo?.profilePic ?? "")
            assignToImg.layer.cornerRadius = assignToImg.frame.size.width/2
            assignToImg.layer.borderWidth = 0
    //        answerViewProfileImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
            assignToImg.clipsToBounds = true
            assignTo.isHidden = true
//            if let assignedBy = indexData.taskTo?.name{
//                assignTo.isHidden = false
//                assignTo.text = "Assigned To: \(assignedBy)"
//
//            }
            if indexData.isAccepted == 0 && indexData.isCompleted == 1
            {
                assignTo.isHidden = false
                assignTo.text = "Your kid has completed the chore"
                decideBtn.isHidden = false
            }
        }else{
            decideBtn.isHidden = true
            statusIcon.isHidden = true
//            doneBtn.isHidden = false
//            removeBtn.isHidden = false
//            rmvNwBtn.isHidden = false
            assignToImg.isHidden = true
            assignTo.isHidden = true
            if indexData.noAction == false
            {
                if let assignedBy = indexData.taskFrom?.name{
                    if HarmonySingleton.shared.dashBoardData.data?.userType == .child{
                        assignTo.isHidden = false
                        assignTo.text = "Assigned By: \(assignedBy)"

                    }else{
                        assignTo.isHidden = true
                        assignTo.text = "Assigned By: \(assignedBy)"

                    }
                }
            }
        }
        
        if (indexData.isAccepted == 0 && indexData.isCompleted == 1) || (indexData.isAccepted == 1 && indexData.isCompleted == 2) || (indexData.isAccepted == 1 && indexData.isCompleted == 1)
        {
            statusIcon.isHidden = true
        }
        
    }
    
    func hideUnhideBtns(doneB:Bool,removeB:Bool)  {
        doneBtn.isHidden = doneB ? true : false
        removeBtn.isHidden = true // removeB ? true : false
    }
    func animateButton() {
        if removeBtn.isHidden == false {
            self.removeBtn.alpha = 1
            UIView.animate(withDuration: 1, delay: 0, options: [.allowUserInteraction, .curveEaseInOut,.autoreverse, .repeat], animations: {
                self.removeBtn.alpha = 0.2
            }, completion: nil)
        }
    }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
