//
//  choreListCell.swift
//  Harmoney
//
//  Created by Mac on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class choreListCell: UITableViewCell {

    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var parentBorderView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUI()

    
    }
    func setUI(){
        
        parentBorderView.layer.borderColor = UIColor(red: 220/250, green: 225/250, blue: 241/250, alpha: 1.0).cgColor
        parentBorderView.layer.borderWidth = 1.0
        parentBorderView.layer.shadowColor = UIColor(red: 106/250, green: 162/250, blue: 172/250, alpha: 0.09).cgColor
        parentBorderView.layer.cornerRadius = 4.0
        
        parentBorderView.layer.shadowPath = UIBezierPath(rect: parentBorderView.bounds).cgPath
        parentBorderView.layer.shadowRadius = 10
        parentBorderView.layer.shadowOffset = .zero
        parentBorderView.layer.shadowOpacity = 1

        


    }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
