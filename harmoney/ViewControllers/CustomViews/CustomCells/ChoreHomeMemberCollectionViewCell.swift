//
//  ChoreHomeMemberCollectionViewCell.swift
//  FitKet
//
//  Created by vaish navi on 19/04/21.
//  Copyright © 2021 Fitket. All rights reserved.
//

import UIKit

class ChoreHomeMemberCollectionViewCell: UICollectionViewCell {

    static let identifier = "ChoreHomeMemberCollectionViewCell"

    var plusMinus = false

    @IBOutlet weak var tickIcon: UIImageView!
    @IBOutlet weak var taskCountLbl: UILabel!
    @IBOutlet var memberName: UILabel!
    @IBOutlet var memberImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
      self.layoutIfNeeded()
         memberImageView.layoutIfNeeded()

         memberImageView.isUserInteractionEnabled = true
         let square = memberImageView.frame.size.width < memberImageView.frame.height ? CGSize(width: memberImageView.frame.size.width, height: memberImageView.frame.size.width) : CGSize(width: memberImageView.frame.size.height, height:  memberImageView.frame.size.height)
         memberImageView.layer.cornerRadius = square.width/2
         memberImageView.clipsToBounds = true;
        
        taskCountLbl.isUserInteractionEnabled = true
        let squarelbl = taskCountLbl.frame.size.width < taskCountLbl.frame.height ? CGSize(width: taskCountLbl.frame.size.width, height: taskCountLbl.frame.size.width) : CGSize(width: taskCountLbl.frame.size.height, height:  taskCountLbl.frame.size.height)
        taskCountLbl.layer.cornerRadius = squarelbl.width/2
        taskCountLbl.clipsToBounds = true;
        
        
    }

    @IBAction func tickButtonAct(_ sender: Any) {
        
        if plusMinus{
            plusMinus = false
            tickIcon.isHidden = false
            
        }else {
            
            plusMinus = true
            tickIcon.isHidden = true
        }
    }
}
