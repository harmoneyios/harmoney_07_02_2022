//
//  dailyMychallengeTableviewCell.swift
//  Harmoney
//
//  Created by INQ Projects on 08/02/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
protocol myChallenegeDelegate: class {
    func didTapOnMychallenegeCell(challengeTableViewCell: dailyMychallengeTableviewCell,selectedIndex:Int)
   
}
class dailyMychallengeTableviewCell: UITableViewCell {

    @IBOutlet weak var myChallengeCollectionView: UICollectionView!
    
    let identifier = "HomeCollectionViewCell"
    var homeModel : HomeModel?
    weak var delegate: myChallenegeDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        myChallengeCollectionView.delegate = self
        myChallengeCollectionView.dataSource = self
        myChallengeCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: identifier)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
//MARK: UICollectionViewDataSource Methods
extension dailyMychallengeTableviewCell: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
            if(collectionView == myChallengeCollectionView){
                if homeModel?.data.challenges.count ?? 0 < 3  && homeModel?.data.challenges.count ?? 0 >= 0{
                    let count = homeModel?.data.challenges.count ?? 0
                    
                    if homeModel?.data.challenges != nil{
                        return count + 1

                    } else {
                        return homeModel?.data.challenges.count ?? 0
                    }

                } else {
                    return homeModel?.data.challenges.count ?? 0
                }
                
            }
        return 0
        }
       
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath as IndexPath) as! HomeCollectionViewCell
        var trending : Trending?
        if(collectionView == myChallengeCollectionView){
            
            let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)

                if homeModel?.data.challenges.count ?? 0 < 3  && homeModel?.data.challenges.count ?? 0 >= 0{
                    if (indexPath.row == lastRowIndex - 1) {
                        if homeModel?.data.challenges != nil {
                            trending = nil

                        } else {
                            trending = homeModel?.data.challenges[indexPath.row]

                        }
                    } else {
                    trending = homeModel?.data.challenges[indexPath.row]
                    }
                } else {
                    trending = homeModel?.data.challenges[indexPath.row]
                    
                }
        }
        cell.trending = trending
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)
           
                if homeModel?.data.challenges.count ?? 0 < 3  && homeModel?.data.challenges.count ?? 0 >= 0{
                    if (indexPath.row == lastRowIndex - 1) {
                        
                        myChallengeType = 1

                    } else {
                        let trending = homeModel?.data.challenges[indexPath.row]
                        myChallengeType = trending?.type ?? 0
                    }
                } else {
                    let trending = homeModel?.data.challenges[indexPath.row]
                    myChallengeType = trending?.type ?? 0
                }
        isFromMyChallange = true
        delegate?.didTapOnMychallenegeCell(challengeTableViewCell: self,selectedIndex: indexPath.row)
    }


}
