//
//  ChorePopupCellOne.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 29/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ChorePopupCellOne: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descpLable: UILabel!
    
    @IBOutlet weak var bucksView: UIView!
    @IBOutlet weak var bucksTF: UITextField!
    
    
    @IBOutlet weak var rewardsView: UIView!
    @IBOutlet weak var rewardsTF: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleUI(hidedscLbl:Bool,hidebucksVw:Bool,hiderewardVw:Bool)  {
        bucksView.isHidden = hidebucksVw
        rewardsView.isHidden = hiderewardVw
        descpLable.isHidden = hidedscLbl
    }

}
