//
//  AddAccountCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 30/07/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
protocol AddBankAccountDelegate {
    func addBankAccountCallBack()
}
class AddAccountCell: UITableViewCell {
    var parentVC : AcceptChildChorePopUp?
    var delegate : AddBankAccountDelegate?
    @IBOutlet weak var adaccountClick: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        parentVC?.dismiss(animated: true, completion: nil)
        // Configure the view for the selected state
    }

    @IBAction func addbankAccountClick(_ sender: Any) {
        delegate?.addBankAccountCallBack()
    }
}
