//
//  ChorePopUpAcceptCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 29/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Toast_Swift
class ChorePopUpAcceptCell: UITableViewCell {
    var parentVC : AcceptChildChorePopUp?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func acceptClick(_ sender: Any) {
        // suba start
        /* // bank accout adding feature temparly hide 
        if (HarmonySingleton.shared.dashBoardData.data?.bankAccount?.isEmpty ?? true) && (Int(parentVC?.choreViewData?.harmoney_bucks ?? "0") ?? 0 > 0){
            parentVC?.view.toast("Please add bank account")
        }else{  */
      
            parentVC?.getChoreDetails(getChore: false, accept: true, reject: false)
        //}
        // suba end
        parentVC?.view.endEditing(true)
    }
    @IBAction func rejectClick(_ sender: Any) {
        parentVC?.getChoreDetails(getChore: false, accept: false, reject: true)
        parentVC?.view.endEditing(true)
    }
}
