//
//  LevelsTableViewCell.swift
//  Harmoney
//
//  Created by Csongor Korosi on 8/4/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol LevelsTableViewCellDelegate: class {
    func didTapAddRewardButton(levelsTableViewCell: LevelsTableViewCell)
    func didTapAddCouponButton(levelsTableViewCell: LevelsTableViewCell)
}

class LevelsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var xpImage: UIImageView!
    @IBOutlet weak var gemsCountLabel: UILabel!
    @IBOutlet weak var xpCountLabel: UILabel!
    
    @IBOutlet weak var seconfdPlusBtn: UIButton!
    @IBOutlet weak var rewardClaimedLabel: UILabel!
//    @IBOutlet weak var addCouponButton: UIButton!
//    @IBOutlet weak var addRewardButton: UIButton!
    @IBOutlet weak var couponCustomView: LevelsCouponCustomView!
    @IBOutlet weak var requieredExperianceLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var viewLeadingConstraint: NSLayoutConstraint!
//    @IBOutlet weak var couponButtonLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var completedTick: UIButton!
    
    @IBOutlet weak var diamondImageview: UIImageView!
    
//    @IBOutlet weak var rewardTextField: UITextField! {
//        didSet {
//            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
//            rewardTextField.leftView = paddingView
//            rewardTextField.leftViewMode = .always
//        }
//    }
    
    var levelReward: String?
    let currerntLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
    var levelInfo: LevelInfo? {
        didSet {
            let creditType = HarmonySingleton.shared.cofigModel?.data.creditType

            if creditType == 1 {
                self.diamondImageview.image = UIImage(named: "soccer")

            } else if creditType == 2 {
                self.diamondImageview.image = UIImage(named: "diamond")

            } else {
                self.diamondImageview.image = UIImage(named: "touchdown")

            }
//            diamondImageview.image = UIImage(named: "diamond")

            levelLabel.text = String(format: "Level %d", levelInfo?.level ?? 0)
            let requieredExperiance = (levelInfo?.levelReachXP ?? 0) //levelReachXp
            gemsCountLabel.text = "\(levelInfo?.jem ?? 0)"
            xpCountLabel.text = "\(levelInfo?.levelXP ?? 0)"
            requieredExperianceLabel.text = String(format: "%d", requieredExperiance)
            if (requieredExperiance <= 0){
                requieredExperianceLabel.isHidden = true
                xpImage.isHidden = true
            }else {
                requieredExperianceLabel.isHidden = false
                xpImage.isHidden = false
            }
            let lev = levelInfo?.level ?? 0
    
            if currerntLevel > lev {
//                rewardTextField.isUserInteractionEnabled = false
//                addRewardButton.isUserInteractionEnabled = false
//                addCouponButton.isUserInteractionEnabled = false
                rewardClaimedLabel.isHidden = false
                rewardClaimedLabel.backgroundColor = .clear
                completedTick.isHidden = true
            }else if currerntLevel == lev {
//                rewardTextField.isUserInteractionEnabled = true
//                addRewardButton.isUserInteractionEnabled = true
//                addCouponButton.isUserInteractionEnabled = true
                rewardClaimedLabel.isHidden = false
                rewardClaimedLabel.backgroundColor = .clear
                completedTick.isHidden = true
            }else if currerntLevel < lev {
//                rewardTextField.isUserInteractionEnabled = true
//                addRewardButton.isUserInteractionEnabled = true
//                addCouponButton.isUserInteractionEnabled = true
                rewardClaimedLabel.isHidden = false
                rewardClaimedLabel.backgroundColor = UIColor.init(displayP3Red: 0, green: 0, blue: 0, alpha: 0.7)
                completedTick.isHidden = true
            }
            
            if levelInfo?.reward?.rewardName?.count ?? 0 > 0 {
//                rewardTextField.text = levelInfo?.reward?.rewardName ?? ""
            } else {
//                rewardTextField.text = ""
            }
            
            if levelInfo?.giftCard?.giftCardID ?? 0 > 0 {
                couponCustomView.isHidden = false
//                viewLeadingConstraint.constant = 25
//                couponButtonLeadingConstraint.constant = 60
                couponCustomView.imageView.imageFromURL(urlString: levelInfo?.giftCard?.giftCardImageURL ?? "")
            } else {
                couponCustomView.isHidden = true
//                viewLeadingConstraint.constant = 10
//                couponButtonLeadingConstraint.constant = 10
            }
            rewardClaimedLabel.text = ""
            if ((levelInfo?.giftCard) != nil) {
                seconfdPlusBtn.isHidden = false
            }else{
                seconfdPlusBtn.isHidden = true
            }
        }
    }
    
    weak var delegate: LevelsTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
//        viewLeadingConstraint.constant = 25
//        couponButtonLeadingConstraint.constant = 60
    }
    @IBAction func rewardTextfieldEditingChanged(_ sender: UITextField) {
        levelReward = sender.text
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func addCouponButtonTapped(_ sender: UIButton) {
        self.delegate?.didTapAddCouponButton(levelsTableViewCell: self)
    }
    
    @IBAction func addRewardButtonTapped(_ sender: UIButton) {
        self.delegate?.didTapAddRewardButton(levelsTableViewCell: self)
    }
}
