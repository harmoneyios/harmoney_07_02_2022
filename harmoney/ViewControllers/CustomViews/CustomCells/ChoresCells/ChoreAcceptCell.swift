//
//  ChoreAcceptCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Toast_Swift
import ObjectMapper
import IQKeyboardManagerSwift
import SDWebImage
import CRRefresh

class ChoreAcceptCell: UITableViewCell {
    
    var getInformationForPaymentObject : getInformationForPaymentObject?
    let parentRef = CreateChoreVM.shared.parentRef
    var walletPresent:Bool = false
    @IBOutlet weak var acceptChorebtn: UIButton!
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUp()
    }
    func initialSetUp()  {
        acceptChorebtn.isUserInteractionEnabled = true
        getInformation()
        let dat = HarmonySingleton.shared.dashBoardData.data
        let isParentInvited = dat?.invitedRequest?.filter({$0.memberAccept == 1}).count ?? 0 > 0 ? true : false
        if dat?.userType == RegistrationType.child {
            if(!isParentInvited || isParentInvited) && dat?.parentApproval == ParentApproval.notApproved{
                acceptChorebtn.isEnabled = false
                acceptChorebtn.backgroundColor = #colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1)
                if let assigntomemberDataList = dat?.invitedRequest{
                    for objMember in assigntomemberDataList{
                        if objMember.memberAccept == 1
                        {
                            acceptChorebtn.isEnabled = true
                            acceptChorebtn.backgroundColor = #colorLiteral(red: 0.1607843137, green: 0.2823529412, blue: 0.4274509804, alpha: 1)
                        }
                    }
                }

            }else if dat?.parentApproval == ParentApproval.approved && isParentInvited{
                acceptChorebtn.isEnabled = true
                acceptChorebtn.backgroundColor = #colorLiteral(red: 0.1607843137, green: 0.2823529412, blue: 0.4274509804, alpha: 1)
            }
        }
//        else if (dat?.userType != RegistrationType.child) && (dat?.bankAccount?.isEmpty ?? true){
//            acceptChorebtn.isEnabled = false
//            acceptChorebtn.backgroundColor = #colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1)
//        }
        else{
            acceptChorebtn.isEnabled = true
            acceptChorebtn.backgroundColor = #colorLiteral(red: 0.1607843137, green: 0.2823529412, blue: 0.4274509804, alpha: 1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func choreAcceptClick(_ sender: Any) {
        print("choreAcceptClick")
        acceptChorebtn.isUserInteractionEnabled = false
        parentRef?.view.endEditing(true)
        guard let customTask = parentRef?.vmObj.choreCustomText, !customTask.isEmpty, customTask != "Custom" else{
        parentRef?.view.toast("Please Enter 'Chore Name'")
        return
    }
            guard let when = parentRef?.vmObj.choreDay, !when.isEmpty, when != "Select" else{
            parentRef?.view.toast("Please select 'When'")
            return
        }
        if(!(when == "Just One Time")){
            guard let dateC = parentRef?.vmObj.choreDate, !dateC.isEmpty else{
                parentRef?.view.toast("Please select 'Date'")
                   return
            }
        }
//        guard let hBucks = Int(parentRef?.vmObj.choreBucksvalue ?? "0") , hBucks >= 1 else {
//            parentRef?.view.toast("Please enter Minimum 1 Harmoney Bucks")
//            return
//        }
       acceptChore()
        
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        initialSetUp()
    }
    func findTaskDuration()-> Bool {
    if parentRef!.vmObj.choreDay == "Every Week"{
        let obj = parentRef!.vmObj
        let df = DateFormatter.init()
        df.dateFormat = "yyyy-MM-dd"
        let d1 = Date()
        let d2Str = obj.choreDate.getDateasDMY
        let d2 = df.date(from: d2Str)
        let calendar = Calendar.current
        let compare = calendar.dateComponents([.day], from: d1, to: d2!)
//        ["Everyday", "Every Week","Just One Time"]
            if (compare.day! + 2 < 14){
                parentRef?.view.toast("Please select Just this time")
                return false
            }
        }
        return true
    }
    
    
    func getInformation(){
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show()
            var data = [String : Any]()
            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            var (url, method, param) = APIHandler().getInformationForPayment(params: data)
            
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        self.getInformationForPaymentObject = Mapper<getInformationForPaymentObject>().map(JSON: value)
                        print(self.getInformationForPaymentObject!)
                        if self.getInformationForPaymentObject?.data?.kidWallet == nil
                        {
                            self.walletPresent = false
                            
                        }else{
                            HarmonySingleton.shared.getKidWalletList = self.getInformationForPaymentObject?.data?.kidWallet
                            
                            self.walletPresent = true
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}


extension ChoreAcceptCell {
    
    func acceptChore() {
        guard findTaskDuration() else{
            acceptChorebtn.isUserInteractionEnabled = true
            return
        }
        var data = [String : Any]()
        var memberId:[String] = []
        if let assigntomemberDataList = HarmonySingleton.shared.assigntomemberDataList{
            for objMember in assigntomemberDataList{
                if objMember.isSelectAvathar ?? false {
                    if let fromguid = objMember.fromGuid{
                        memberId.append(fromguid)
                    }
                }
            }
        }
        if HarmonySingleton.shared.assigntomemberDataList?.count == 1{
            memberId.append(HarmonySingleton.shared.assigntomemberDataList?.first?.memberId ?? "")

        }
        if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
        }else{
            if memberId.count == 0
            {
                self.toast("Please select member to assign Chore")
                acceptChorebtn.isUserInteractionEnabled = true
                return
            }
        }
      
        let obj = parentRef!.vmObj
 
        
        let taskendDate = obj.choreDate.getDateasDMY.count > 0 ? obj.choreDate.getDateasDMY :  obj.choreDate.getTodayDateAsDMY
        data.updateValue(obj.defaultChoreObj?.chorseTaskId ?? "", forKey: "task_id")
       
        if obj.defaultChoreObj?.chorserSubTitle == "Custom"
        {
            data.updateValue(obj.choreCustomText, forKey: "task_name")
            data.updateValue("Custom Chores!" , forKey: "task_subName")
        }else{
            data.updateValue(obj.defaultChoreObj?.chorserName ?? "", forKey: "task_name")
            data.updateValue(obj.defaultChoreObj?.chorserSubTitle ?? "", forKey: "task_subName")

        }
        data.updateValue(UserDefaultConstants().guid!, forKey: "guid")
        data.updateValue("", forKey: "invited_guid")
        data.updateValue(obj.choreDay, forKey: "task_when")
        data.updateValue(taskendDate, forKey: "task_until")
        data.updateValue(obj.choreDate.getTodayDateAsDMY, forKey: "task_start_date")
        data.updateValue(taskendDate, forKey: "task_end_date")
        data.updateValue(obj.choreBucksvalue, forKey: "harmoney_bucks")
        data.updateValue(obj.choreRewardMsg, forKey: "task_reward")
        data.updateValue(/*obj.choreXpvalue*/0, forKey: "task_xp_value")
        data.updateValue(/*obj.choreGemValue*/0, forKey: "task_gems_value")
        data.updateValue(1, forKey: "isActive")
        data.updateValue(memberId, forKey: "assignedTo")
//        if memberId.count > 0{
//           data.updateValue(0, forKey: "tIsAccepted")
//            data.updateValue(1, forKey: "tIsApproved")
//        }
        data.updateValue(0, forKey: "tIsAccepted")
         data.updateValue(1, forKey: "tIsApproved")
        if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
            if obj.choreBucksvalue == "" || obj.choreBucksvalue == "0"
            {
                data.updateValue(1, forKey: "tIsApproved")
                data.updateValue(1, forKey: "tIsAccepted")
            }
        }
          data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")
        if Reachability.isConnectedToNetwork(){
            SVProgressHUD.show()
            var (url, method, param) = APIHandler().acceptChoreAPI(params: data)           
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        let vc  = self.parentRef?.addChorePopUp
                        vc!.modalPresentationStyle = .overFullScreen
                        self.parentRef?.present(vc!, animated: true, completion: nil)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }else{
            CreateChoreVM.shared.parentRef!.view.toast("Internet Connection not Available!")
        }
        
    }
}
