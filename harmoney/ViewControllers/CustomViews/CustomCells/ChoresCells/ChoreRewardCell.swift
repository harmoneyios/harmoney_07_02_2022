//
//  ChoreRewardCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ChoreRewardCell: UITableViewCell {
    @IBOutlet weak var choreNameLabel: UILabel!
    
    @IBOutlet weak var rewardAttributeLabel: UILabel!
    @IBOutlet weak var rewrdLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setXPandGemCount(xp: "0", gem: "0" )
    }
    
    func setXPandGemCount(xp:String,gem:String) {
        let experianceString = xp
             let diamondString = gem
        
             let xpAttachment = NSTextAttachment()
             let diamondAttachment = NSTextAttachment()
             xpAttachment.image = UIImage(named: "xp")
        let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
        
        if creditType == 1 {
            diamondAttachment.image = UIImage(named: "soccer")

        } else if creditType == 2 {
            diamondAttachment.image = UIImage(named: "diamond")

        } else {
            diamondAttachment.image = UIImage(named: "touchdown")
        }
//             diamondAttachment.image = UIImage(named: "diamond")
             let boldFont = UIFont.futuraPTBoldFont(size: 16)
             
        let imageSize = xpAttachment.image?.size
        let image2Size = diamondAttachment.image?.size
             xpAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - imageSize!.height) / 2, width: imageSize!.width, height: imageSize!.height)
             diamondAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - image2Size!.height) / 2, width: image2Size!.width, height: image2Size!.height)
             
             let xpImageString = NSAttributedString(attachment: xpAttachment)
             let diamondImageString = NSAttributedString(attachment: diamondAttachment)
        let attributedText = NSMutableAttributedString(string: "to get ")
               attributedText.append(NSAttributedString(string: experianceString + " "))
               attributedText.append(xpImageString)
               attributedText.append(NSAttributedString(string: "  "))
               attributedText.append(diamondImageString)
               attributedText.append(NSAttributedString(string:"  " + diamondString + " "))
               attributedText.append(NSAttributedString(string: " and start leveling up"))
               
               let lightRange = (attributedText.string as NSString).range(of: attributedText.string)
               let boldRangeExperiance = (attributedText.string as NSString).range(of: experianceString)
               let boldRangeDiamond = (attributedText.string as NSString).range(of: diamondString)
               
               attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBookFont(size: 16)], range:lightRange)
               attributedText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.harmoneyLightOrangeColor], range:boldRangeExperiance)
               attributedText.addAttributes([NSAttributedString.Key.font: boldFont], range:boldRangeExperiance)
               attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBoldFont(size: 16)], range:boldRangeDiamond)
               attributedText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.harmoneyLightOrangeColor], range:boldRangeDiamond)
               
               rewardAttributeLabel.attributedText = attributedText
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
