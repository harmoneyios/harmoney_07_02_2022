//
//  AssignToTableViewCell.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 08/07/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class AssignToTableViewCell: UITableViewCell {

    var homeModel: HomeModel? {
        return HomeManager.sharedInstance.homeModel
    }
    @IBOutlet weak var memberCollectionViewWidthConstrain: NSLayoutConstraint!
    @IBOutlet weak var assignToLabel: UILabel!
    var parentRef : ChoreCreateVC?
    @IBOutlet weak var memberCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        memberCollectionView.dataSource = self
       memberCollectionView.delegate = self
        memberCollectionView.register(UINib(nibName: ChoreHomeMemberCollectionViewCell.identifier, bundle: .main), forCellWithReuseIdentifier: ChoreHomeMemberCollectionViewCell.identifier)
    }

  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setProfilePic(frstName: String?, lstName: String?, profileImageView: UIImageView){
     
         if let firstName = frstName, let lastName = lstName {
             profileImageView.setImageWith(firstName + " " + lastName, color: ConstantString.menuBackSystemProfilepic)
         } else {
             if let dispName = frstName {
                 profileImageView.setImageWith(dispName, color: ConstantString.menuBackSystemProfilepic)
             }
         }
     }
    
}

extension AssignToTableViewCell: UICollectionViewDelegate {
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        let width = (self.frame.size.width - 12 * 3) / 3 //some width
//        let height = width * 1.5 //ratio
//        return CGSize(width: width, height: height)
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let isSelectedAvathar = !(HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].isSelectAvathar ?? false)
        HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].isSelectAvathar = isSelectedAvathar
        
        collectionView.reloadItems(at: [indexPath])
        CreateChoreVM.shared.parentRef?.choreUITable.reloadData()
        
    }
}
extension AssignToTableViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}
extension AssignToTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return HarmonySingleton.shared.assigntomemberDataList?.count ?? 0
//        return 2
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChoreHomeMemberCollectionViewCell", for: indexPath) as! ChoreHomeMemberCollectionViewCell
        cell.contentView.isUserInteractionEnabled = true

        cell.memberName.text = HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].memberName ?? "No name"
        
        if HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].memberPhoto == "" {
            self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: cell.memberImageView)
        } else {
            cell.memberImageView.imageFromURL(urlString: HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].memberPhoto ?? "")
        }
        if HarmonySingleton.shared.assigntomemberDataList?.count == 1{

            cell.tickIcon.isHidden = false
        }else{
            cell.tickIcon.isHidden = !(HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].isSelectAvathar ?? false)

        }
//        cell.tickIcon.isHidden = !(HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].isSelectAvathar ?? false)
        
        if indexPath.row == 0{
            if(cell.tickIcon.isHidden){
            
                HarmonySingleton.shared.choreFor = "others"
                
            }else{
                HarmonySingleton.shared.choreFor = "own"
            }
        }
        
        return cell
        
    }
    
    
   
}


