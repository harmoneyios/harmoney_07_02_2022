
//
//  ChoresHeaderCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ChoresHeaderCell: UITableViewCell {
    @IBOutlet weak var choreTitle: UILabel!
    @IBOutlet weak var choreImg: UIImageView!
    @IBOutlet weak var halfWhiteView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uiSetup()
    }
    
    func uiSetup() {        
        halfWhiteView.roundCorners([.topLeft,.topRight], radius: 5)        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
