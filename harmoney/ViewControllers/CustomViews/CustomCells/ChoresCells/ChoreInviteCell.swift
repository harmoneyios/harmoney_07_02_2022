//
//  ChoreInviteCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ChoreInviteCell: UITableViewCell {

    @IBOutlet weak var inviteLbl: UILabel!
    @IBOutlet weak var choreInviteBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUP()
    }
    func initialSetUP() {
        let dat = HarmonySingleton.shared.dashBoardData.data
        let isParentInvited = dat?.invitedRequest?.count ?? 0 > 0 ? true : false
        
        if dat?.userType == RegistrationType.child && isParentInvited && dat?.parentApproval == ParentApproval.notApproved {
            inviteLbl.text = "Waiting for your parent to accept your Invite"
            choreInviteBtn.isEnabled = false
         
        }else {
           if dat?.userType == RegistrationType.child {
            inviteLbl.text = "Invite a parent to earn Harmoney Bucks or a Reward"
            choreInviteBtn.isEnabled = true
           }else {
         //   inviteLbl.isHidden = true
          //  choreInviteBtn.isEnabled = false
           }
        }
        //suba start
        //if (dat?.userType == RegistrationType.parent ||  dat?.userType == RegistrationType.adult){
        if (dat?.userType == RegistrationType.parent){
           
          //  if (dat?.bankAccount?.isEmpty ?? true){
           //     inviteLbl.text = "Please add a bank account to accept chore"
                choreInviteBtn.isEnabled = true
          //  }
            //suba end
        }
    }
    @IBAction func inviteClick(_ sender: Any) {
        if inviteLbl.text == "Invite a parent to earn Harmoney Bucks or a Reward"{
            let profileNavigationViewController =   CreateChoreVM.shared.parentRef?.tabBarController?.viewControllers?[3] as! UINavigationController
            let profileViewController = profileNavigationViewController.viewControllers[0] as! ProfileTabViewController
            profileViewController.inviteState = (true, .invite)
        }
        CreateChoreVM.shared.parentRef?.tabBarController?.selectedIndex = 3
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        initialSetUP()
    }

}
