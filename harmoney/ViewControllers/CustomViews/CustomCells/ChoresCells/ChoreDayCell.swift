//
//  ChoreDayCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import iOSDropDown

class ChoreDayCell: UITableViewCell {

    @IBOutlet weak var dayDropDown: DropDown!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        initialUI()
        dayDropDown.text = ""
        dayDropDown.optionArray = ["Everyday", "Every Week","Just One Time"]
        dayDropDown.selectedRowColor =  #colorLiteral(red: 0.9294117647, green: 0.937254902, blue: 0.9529411765, alpha: 1)
        dayDropDown.checkMarkEnabled = false
        dayDropDown.arrowColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        dayDropDown.isSearchEnable = false
        dayDropDown.adjustsFontSizeToFitWidth = true
        dayDropDown.minimumFontSize = 12
        
        
        dayDropDown.didSelect{(selectedText , index ,id) in
            CreateChoreVM.shared.daySelected = true
            CreateChoreVM.shared.choreDay = selectedText
            if index != 2{
                ChoreDataCells.shared.ifdaySelectedUI()
                CreateChoreVM.shared.choreDate = ""
                CreateChoreVM.shared.choreXpvalue = "0"
                CreateChoreVM.shared.choreGemValue = "0"
            }else{
                ChoreDataCells.shared.defaultSections()
                CreateChoreVM.shared.choreDate = ""
                CreateChoreVM.shared.choreXpvalue = "200"
                CreateChoreVM.shared.choreGemValue = "1"
            }
            
            CreateChoreVM.shared.parentRef?.choreUITable.reloadData()
        }

    }
    func initialUI() {
        let dat = HarmonySingleton.shared.dashBoardData.data
        let isParentInvited = dat?.invitedRequest?.count ?? 0 > 0 ? true : false
        if dat?.userType == .child {
            if (!isParentInvited || isParentInvited) && dat?.parentApproval == ParentApproval.notApproved{
                dayDropDown.isEnabled = false
            }else if isParentInvited && dat?.parentApproval == ParentApproval.approved{
               dayDropDown.isEnabled = true
            }
        }
        else {
          dayDropDown.isEnabled = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
//        initialUI()
        self.dayDropDown.text = CreateChoreVM.shared.choreDay
        self.dayDropDown.selectedIndex = nil
    }

}
