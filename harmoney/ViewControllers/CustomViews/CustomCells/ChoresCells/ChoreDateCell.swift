//
//  ChoreDateCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ChoreDateCell: UITableViewCell {
    let datePickerView = UIDatePicker()
    var stringDate : String = ""
    @IBOutlet weak var choreDateTF: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        choreDateTF.adjustsFontSizeToFitWidth = true
        choreDateTF.minimumFontSize = 14
        calendarPicker()
    }

    
    func calendarPicker() {
         datePickerView.datePickerMode = .date
               datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .allEvents)
        let now = Calendar.current.dateComponents(in: .current, from: Date())
        /*Today date need to show
         let tomorrow = DateComponents(year: now.year, month: now.month, day: now.day! + 1)
         */
        let tomorrow = DateComponents(year: now.year, month: now.month, day: now.day)
        let dateTomorrow = Calendar.current.date(from: tomorrow)!
        datePickerView.minimumDate = dateTomorrow
        if #available(iOS 13.4, *) {
            datePickerView.preferredDatePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        choreDateTF.inputView = datePickerView

                
    }
    func calculateDatesDifference() -> Int{
        let today = Date()
        let when = datePickerView.date
        let totalDays = Calendar.current.dateComponents([.day], from: today, to: when).day!
        return totalDays + 2

    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
         let fulDate = DateFormatter()
         fulDate.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
         fulDate.dateFormat = "MMM dd, yyyy"
         stringDate = fulDate.string(from: sender.date)
    }
    @IBAction func calendarClick(_ sender: Any) {
        choreDateTF.becomeFirstResponder()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        choreDateTF.text = CreateChoreVM.shared.choreDate
    }
    func indexPath(row:Int) -> IndexPath {
        return IndexPath.init(item: row, section: 0)
    }
}


extension ChoreDateCell : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if !(stringDate.count > 0){
            handleDatePicker(sender: datePickerView)
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.text = stringDate
        CreateChoreVM.shared.choreDate = stringDate
        CreateChoreVM.shared.choreXpvalue = "\(200 * calculateDatesDifference())"
        CreateChoreVM.shared.choreGemValue = "\(calculateDatesDifference())"
        let vc = CreateChoreVM.shared.parentRef
        let index = vc?.choreUIhandler.rows.firstIndex(of: .reward)
        vc?.choreUITable.reloadRows(at: [indexPath(row: index ?? 0)], with: .none)
        
        return true
    }
}
