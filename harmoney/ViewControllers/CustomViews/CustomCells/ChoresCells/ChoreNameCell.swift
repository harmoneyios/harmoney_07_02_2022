//
//  ChoreNameCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ChoreNameCell: UITableViewCell {
    @IBOutlet weak var choreNameLabel: UILabel!
    @IBOutlet weak var choreTskHeading: UILabel!
    
    @IBOutlet weak var choreNameTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if choreNameTextField != nil{
            initialState()
        }
        
    }

    @objc func initialState(){
        choreNameTextField.delegate = self
        choreNameTextField.text = ""
        choreNameTextField.addTarget(self, action: #selector(textChange), for: .editingChanged)
    }
    @objc func textChange()  {
        if let customText = choreNameTextField.text, !customText.isEmpty {
            CreateChoreVM.shared.choreCustomText = customText
//            CreateChoreVM.shared.choreXpvalue = "\(Int(bucks )! * 100)"
        }else{
            CreateChoreVM.shared.choreCustomText = ""
//            CreateChoreVM.shared.choreXpvalue = "0"
        }
//       let vc = CreateChoreVM.shared.parentRef
//        let index = vc?.choreUIhandler.rows.firstIndex(of: .reward)
//        vc?.choreUITable.reloadRows(at: [indexPath(row: index ?? 0)], with: .none)
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ChoreNameCell: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == choreNameTextField){
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 10
        }
       
        return true
    }
}
