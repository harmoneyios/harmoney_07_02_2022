//
//  ChoreBucksCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 17/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ChoreBucksCell: UITableViewCell {
    @IBOutlet weak var bucksLbl: UILabel!
    
    @IBOutlet weak var himgView: UIImageView!
    @IBOutlet weak var rewardsLbl: UILabel!
    @IBOutlet weak var rewardsTFView: UIView!
    @IBOutlet weak var harmoneyBucksView: UIView!
    @IBOutlet weak var bucksImage: UIImageView!
    @IBOutlet weak var choreRewardsTF: UITextField!
//    @IBOutlet weak var rewardsImg: UIImageView!
    @IBOutlet weak var choreBucksTF: UITextField!
    
    @IBOutlet weak var hbucksInfoLabel: UILabel!
    @IBOutlet weak var bucksTFBGView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialState()
    }
    
    func initialState()  {
        let dat = HarmonySingleton.shared.dashBoardData.data
        let isParentInvited = dat?.invitedRequest?.count ?? 0 > 0 ? true : false
        choreRewardsTF.delegate = self
        choreBucksTF.delegate = self
        choreBucksTF.addTarget(self, action: #selector(calculateXP), for: .editingChanged)
        choreBucksTF.keyboardType = .decimalPad
        if dat?.userType == .child {
            if (!isParentInvited || isParentInvited) && dat?.parentApproval == ParentApproval.notApproved{
                disableState()
                if let assigntomemberDataList = dat?.invitedRequest{
                    for objMember in assigntomemberDataList{
                        if objMember.memberAccept == 1
                        {
                            enableState()
                        }
                    }
                }
            }else if isParentInvited && dat?.parentApproval == ParentApproval.approved{
               
                    enableState()

            }
        }else if dat?.userType == .adult {
            enableState()
        }else if dat?.userType != .child{
//            if (dat?.bankAccount?.isEmpty ?? true){
//               disableStateForNoBank()
//            }else{
               enableState()
//            }
        }
        if dat?.userType == .child{
            if dat?.invitedRequest?.count == 0
            {
                disableState()
            }
        }
      
//        if dat?.userType == .child{
//            if dat?.invite == nil{
//                disableState()
//            }
//        }
        choreRewardsTF.addTarget(self, action: #selector(collectRewardsText), for: .editingChanged)
    }
    @objc func collectRewardsText(){
        CreateChoreVM.shared.choreRewardMsg = choreRewardsTF.text ?? ""
    }
    @objc func calculateXP()  {
        if let bucks = choreBucksTF.text, !bucks.isEmpty {
            CreateChoreVM.shared.choreBucksvalue = bucks
//            CreateChoreVM.shared.choreXpvalue = "\(Int(bucks )! * 100)"
        }else{
            CreateChoreVM.shared.choreBucksvalue = "0"
//            CreateChoreVM.shared.choreXpvalue = "0"
        }
//       let vc = CreateChoreVM.shared.parentRef
//        let index = vc?.choreUIhandler.rows.firstIndex(of: .reward)
//        vc?.choreUITable.reloadRows(at: [indexPath(row: index ?? 0)], with: .none)
        
    }
    func indexPath(row:Int) -> IndexPath {
        return IndexPath.init(item: row, section: 0)
    }
    func disableState()  {
        himgView.isHighlighted = true
        bucksImage.isHighlighted = true
//        rewardsImg.isHighlighted = true
        harmoneyBucksView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        bucksLbl.isEnabled = false
        choreRewardsTF.isEnabled = false
        choreBucksTF.isEnabled = false
        rewardsTFView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        rewardsLbl.isHidden = false
        rewardsLbl.isEnabled = true
        bucksTFBGView.backgroundColor = .clear
        choreBucksTF.text = "0"
        choreBucksTF.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        choreRewardsTF.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
       
    }
    func disableStateForNoBank()  {
        himgView.isHighlighted = true
        bucksImage.isHighlighted = true
        harmoneyBucksView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        bucksLbl.isEnabled = false
        choreBucksTF.isEnabled = false
        choreBucksTF.text = "0"
        bucksTFBGView.backgroundColor = .clear
        
        
        rewardsLbl.isHidden = false
//        rewardsImg.isHighlighted = false
        rewardsTFView.backgroundColor = .white
        choreRewardsTF.isEnabled = true
        choreRewardsTF.backgroundColor = .white
    }
    
    func enableState()  {
        himgView.isHighlighted = false
        rewardsLbl.isHidden = false
        bucksImage.isHighlighted = false
//        rewardsImg.isHighlighted = false
        harmoneyBucksView.backgroundColor = #colorLiteral(red: 0.862745098, green: 0.8823529412, blue: 0.9450980392, alpha: 1)
        rewardsTFView.backgroundColor = .white
        bucksLbl.isEnabled = true
        choreRewardsTF.isEnabled = true
        choreBucksTF.isEnabled = true
        choreRewardsTF.backgroundColor = .white
        choreBucksTF.backgroundColor = .white
        bucksTFBGView.backgroundColor = .white
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        initialState()
    }

}

extension ChoreBucksCell: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == choreBucksTF){
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 2
        }
        if(textField == choreRewardsTF){
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 100
        }
        return true
    }
}
