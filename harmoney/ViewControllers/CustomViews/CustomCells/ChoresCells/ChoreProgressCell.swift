//
//  ChoreProgressCell.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 19/06/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class ChoreProgressCell: UITableViewCell {

    @IBOutlet weak var desclbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var topLine: UIView!
    @IBOutlet weak var bottomLine: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
