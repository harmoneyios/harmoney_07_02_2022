//
//  ClaimListTableViewCell.swift
//  Harmoney
//
//  Created by INQ Projects on 09/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class ClaimListTableViewCell: UITableViewCell {
    @IBOutlet weak var smallClaimButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var customRewardLabel: UILabel!
    @IBOutlet weak var levelCustomView: UIView!
    @IBOutlet weak var vouchersImageView: UIImageView!
    @IBOutlet weak var circleImageView: UIImageView!
    @IBOutlet weak var rewardImageView: UIImageView!

    
    var purchasedCouponTools: CouponPurchasedDatum? {
        didSet {
            levelCustomView.isHidden = true
            vouchersImageView.isHidden = true
            titleLabel.text = purchasedCouponTools?.productList.name ?? ""
            taskLabel.text = purchasedCouponTools?.productList.collection.value ?? ""
            let convertDate = self.convertDateFormater(purchasedCouponTools?.productList.collection.expirydate ?? "")
            customRewardLabel.text = String(format: "Valid : %@",convertDate)
            rewardImageView.imageFromURL(urlString: purchasedCouponTools?.productList.logoURL ?? "")
            rewardImageView.contentMode = .scaleAspectFit
        }
    }
    var claimedCouponTools: DatumClaimed? {
        didSet {
            levelCustomView.isHidden = true
            vouchersImageView.isHidden = true
            titleLabel.text = claimedCouponTools?.productList.name ?? ""
            taskLabel.text = claimedCouponTools?.productList.collection.value ?? ""
            let convertDate = self.convertDateFormater(claimedCouponTools?.productList.collection.expirydate ?? "")
            customRewardLabel.text = String(format: "Valid : %@",convertDate)
            rewardImageView.imageFromURL(urlString: claimedCouponTools?.productList.logoURL ?? "")
            rewardImageView.contentMode = .scaleAspectFit
        }
    }
    
    weak var delegate: CustomRewardTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "E d MMM yyyy"
            return  dateFormatter.string(from: date!)

        }
}


