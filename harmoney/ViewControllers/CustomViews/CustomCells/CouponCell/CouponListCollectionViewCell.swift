//
//  CouponListCollectionViewCell.swift
//  Harmoney
//
//  Created by INQ Projects on 08/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
protocol CouponGemProductCollectionViewCellDelegate: class {
    func didTapBuyButton(coupon: CouponInfo, couponGemProductCollectionViewCell: CouponListCollectionViewCell)
}
class CouponListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var diamondImage: UIImageView!
    
    weak var delegate: CouponGemProductCollectionViewCellDelegate?
    
    var coupon: CouponInfo? {
        didSet {
            configureUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//MARK: UI Methods

extension CouponListCollectionViewCell {
    func configureUI() {
        imageView.imageFromURL(urlString: coupon?.logoURL ?? "")
        discountLabel.text = coupon?.title ?? ""
        priceLabel.text = coupon?.poweredby ?? ""
        let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
        
        if creditType == 1 {
            self.diamondImage.image = UIImage(named: "soccer")

        } else if creditType == 2 {
            self.diamondImage.image = UIImage(named: "diamond")

        } else {
            self.diamondImage.image = UIImage(named: "touchdown")

        }
//        diamondImage.image = UIImage(named: "diamond")
    }
}

//MARK: Action Methods

extension CouponListCollectionViewCell {
    @IBAction func buyButtonTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 0.5
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }) { (finished) in
            UIView.animate(withDuration: 0.5, animations: {
                sender.alpha = 1
                sender.transform = CGAffineTransform.identity
            })
        }
        delegate?.didTapBuyButton(coupon: coupon!, couponGemProductCollectionViewCell: self)
    }
}







