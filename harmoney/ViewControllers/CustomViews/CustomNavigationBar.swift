//
//  CustomNavigationBar.swift
//  Harmoney
//
//  Created by Norbert Korosi on 22/06/2020.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol CustomNavigationBarDelegate: class {
    func customNavigationBarDidTapLeftButton(_ navigationBar: CustomNavigationBar)
    func customNavigationBarDidTapRightButton(_ navigationBar: CustomNavigationBar)
}

class CustomNavigationBar: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    @IBOutlet weak var navImage: UIImageView!
    weak var delegate: CustomNavigationBarDelegate?
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var rightButtonTitle: String? {
        didSet {
            rightButton.setTitle(rightButtonTitle, for: .normal)
        }
    }
    
    //MARK: - Lifecycle methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
           
        commonInit()
    }
       
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
           
        commonInit()
    }
       
    func commonInit() {
        Bundle.main.loadNibNamed("CustomNavigationBar", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}

//MARK: - Action methods

extension CustomNavigationBar {
    @IBAction func leftButtonTapped(_ sender: UIButton) {
        delegate?.customNavigationBarDidTapLeftButton(self)
    }
       
    @IBAction func rightButtonTapped(_ sender: UIButton) {
        delegate?.customNavigationBarDidTapRightButton(self)
    }
}
