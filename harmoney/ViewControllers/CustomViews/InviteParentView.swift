//
//  InviteParentView.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/9/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import iOSDropDown

protocol InviteParentViewDelegate: class {
    func didTapInviteButton(inviteParentView: InviteParentView, inviteData: InviteData)
}

class InviteParentView:  UIView {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var fatherButton: UIButton!
    @IBOutlet weak var motherButton: UIButton!
    @IBOutlet weak var guardianButton: UIButton!
    @IBOutlet weak var dropDown: UIPickerView!
    @IBOutlet weak var parentNameTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            parentNameTextField.leftView = paddingView
            parentNameTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            parentNameTextField.attributedPlaceholder = NSAttributedString(string: "Enter parent name", attributes:attributes)
        }
    }
    
    @IBOutlet weak var mobileNumberTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            mobileNumberTextField.leftView = paddingView
            mobileNumberTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            mobileNumberTextField.attributedPlaceholder = NSAttributedString(string: "Enter mobile number", attributes:attributes)
        }
    }
    @IBOutlet weak var emailAddressTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            emailAddressTextField.leftView = paddingView
            emailAddressTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            emailAddressTextField.attributedPlaceholder = NSAttributedString(string: "Enter email address", attributes:attributes)
        }
    }
    
    @IBOutlet weak var relationshipTextField: DropDown!{
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            relationshipTextField.leftView = paddingView
            relationshipTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            relationshipTextField.attributedPlaceholder = NSAttributedString(string: "Select Relationship", attributes:attributes)
        }
    }
  
    
    var memberRelation: String? = ""
    weak var delegate: InviteParentViewDelegate?
    
//MARK: - Lifecycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
        setupDropDowns()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("InviteParentView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    func setupDropDowns() {
      
        relationshipTextField.optionArray = ["Mom","Dad","Guradian"]
        relationshipTextField.text = ""
        relationshipTextField.selectedRowColor =  #colorLiteral(red: 0.9294117647, green: 0.937254902, blue: 0.9529411765, alpha: 1)
        relationshipTextField.checkMarkEnabled = false
        relationshipTextField.arrowColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        relationshipTextField.isSearchEnable = false
        relationshipTextField.adjustsFontSizeToFitWidth = true
        relationshipTextField.minimumFontSize = 12
        relationshipTextField.didSelect { (string, int1, int2) in
            self.relationshipTextField.text = "\(string)"
            self.memberRelation = string
        }
        
    }
}

//MARK: UI Methods
extension InviteParentView {
    func buttonStateSelected(button: UIButton) {
        button.backgroundColor = UIColor.harmoneyDarkBlueColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.futuraPTBookFont(size: 16)
        button.borderWidth = 0
        button.borderColor = UIColor.clear
    }
    
    func buttonStateNormal(button: UIButton) {
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5), for: .normal)
        button.titleLabel?.font = UIFont.futuraPTLightFont(size: 16)
        button.borderWidth = 1
        button.borderColor = UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5)
    }
}

//MARK: Action methods
extension InviteParentView {
    @IBAction func fatherButtonTapped(_ sender: UIButton) {
        memberRelation = "Father"
        
        buttonStateSelected(button: sender)
        buttonStateNormal(button: motherButton)
        buttonStateNormal(button: guardianButton)
        self.endEditing(true)
    }
    
    @IBAction func motherButtonTapped(_ sender: UIButton) {
        memberRelation = "Mother"
        
        buttonStateSelected(button: sender)
        buttonStateNormal(button: fatherButton)
        buttonStateNormal(button: guardianButton)
        self.endEditing(true)

    }
    
    @IBAction func guardianButtonTapped(_ sender: UIButton) {
        memberRelation = "Guardian"
        
        buttonStateSelected(button: sender)
        buttonStateNormal(button: motherButton)
        buttonStateNormal(button: fatherButton)
        self.endEditing(true)

    }
    
    @IBAction func sendInviteButtonTapped(_ sender: UIButton) {
        let enteredmobulenumber = AddKidCustomView().formattedNumber(number: mobileNumberTextField.text!)
        let loginedMobileNumber = AddKidCustomView().formattedNumber(number: (UserDefaultConstants().userPhoneNumber?.last)!)
       
        if parentNameTextField.text?.count ?? 0 > 0 && mobileNumberTextField.text?.count ?? 0 > 0 && memberRelation?.count ?? 0 > 0 {
            self.endEditing(true)
            if mobileNumberTextField.text?.count ?? 0 > 0 {
                guard (mobileNumberTextField.text?.count == 12) else {
                    return self.toast("Enter valid mobile number")
                }
            }
            
            let isMobileNumberContainedAlready = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?.contains(where:{$0.memberMobile == enteredmobulenumber})
            if  isMobileNumberContainedAlready == true {
                self.toast(notifications.mobileNumberAlreadyUser)
                return
            }
            if emailAddressTextField.text?.count ?? 0 > 0 {
                if Utilities.isValidEmail(emailString: emailAddressTextField?.text ?? "") == false {
                    self.toast("Please enter valid email address")
                }else if (enteredmobulenumber == loginedMobileNumber){
                    let errorMessage = notifications.nothavePermissiontoUseSameNumber
                    self.toast(errorMessage)
                }else {
                    
                    let inviteData = InviteData(memberPhoto: "", memberName_encrypted: getEncryptedString(plainText: parentNameTextField.text), memberMobile_encrypted: getEncryptedString(plainText: mobileNumberTextField.text), memberRelation_encrypted: memberRelation , memberEmail_encrypted: getEncryptedString(plainText: emailAddressTextField.text), inviteStatus: 0)
               
                    
                    delegate?.didTapInviteButton(inviteParentView: self, inviteData: inviteData)
                }
              
            }else {
                if (enteredmobulenumber == loginedMobileNumber){
                    let errorMessage = notifications.nothavePermissiontoUseSameNumber
                    self.toast(errorMessage)
                    return
                }
                let inviteData = InviteData(memberPhoto: "", memberName_encrypted: getEncryptedString(plainText: parentNameTextField.text), memberMobile_encrypted: getEncryptedString(plainText: mobileNumberTextField.text), memberRelation_encrypted: memberRelation , memberEmail_encrypted: getEncryptedString(plainText: emailAddressTextField.text), inviteStatus: 0)
        
            
            delegate?.didTapInviteButton(inviteParentView: self, inviteData: inviteData)
            }
        } else if parentNameTextField.text?.count == 0 {
            self.toast("Please enter parent name")
        } else if mobileNumberTextField.text?.count == 0 {
            self.toast("Please enter parent mobile number")
        } else if memberRelation?.count == 0 {
            self.toast("Please select a relationship")
        }
    }
    
    func reloadInviteParentViewData()  {
        parentNameTextField.text = ""
        mobileNumberTextField.text = ""
        memberRelation = ""
        buttonStateNormal(button: fatherButton)
        buttonStateNormal(button: motherButton)
        buttonStateNormal(button: guardianButton)
    }
    
    @IBAction func addFromContactButtonTapped(_ sender: Any) {
    
    }
}

//MARK: UITextFieldDelegate Methods
extension InviteParentView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumberTextField {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Utilities.formattedMobileNumber(number: newString)
            
            return false
        }else if textField == parentNameTextField{
            
            guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
               let substringToReplace = textFieldText[rangeOfTextToReplace]
               let count = textFieldText.count - substringToReplace.count + string.count
           
            do {
               let regex = try NSRegularExpression(pattern: "[^A-Za-z ]", options: [])
               if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                   return false
               }
           }
           catch {
               print("ERROR")
           }
            
            return count <= 30
        }
        return true
    }
}


