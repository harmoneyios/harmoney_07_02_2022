//
//  AddKidCustomView.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/12/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import iOSDropDown

protocol AddKidCustomViewDelegate: class {
    func didTapProfileImageView(addKidCustomView: AddKidCustomView)
    func didTapAddKidButton(addKidCustomView: AddKidCustomView, inviteData: InviteData)
}

class AddKidCustomView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var sonButton: UIButton!
    
    @IBOutlet weak var daughterButton: UIButton!
    
    @IBOutlet weak var guardianButton: UIButton!
    
    var memberRelation: String? = ""

    @IBOutlet weak var nameTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            nameTextField.leftView = paddingView
            nameTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            nameTextField.attributedPlaceholder = NSAttributedString(string: "Enter kid name", attributes:attributes)
        }
    }
    @IBOutlet weak var mobileNumberTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            mobileNumberTextField.leftView = paddingView
            mobileNumberTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            mobileNumberTextField.attributedPlaceholder = NSAttributedString(string: "Enter mobile number", attributes:attributes)
        }
    }
    @IBOutlet weak var emailAddressTextField: UITextField! {
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            emailAddressTextField.leftView = paddingView
            emailAddressTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            emailAddressTextField.attributedPlaceholder = NSAttributedString(string: "Enter email address", attributes:attributes)
        }
    }
    
    @IBOutlet weak var relationshipTextField: DropDown!{
        didSet {
            let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
            relationshipTextField.leftView = paddingView
            relationshipTextField.leftViewMode = .always
            
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5),
                NSAttributedString.Key.font : UIFont.futuraPTBookFont(size: 18)
            ]

            relationshipTextField.attributedPlaceholder = NSAttributedString(string: "Select Relationship", attributes:attributes)
        }
    }
    
    weak var delegate: AddKidCustomViewDelegate?
    var invitedRequest: InvitedRequest? {
        didSet {
            
        }
    }
    
//MARK: - Lifecycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
        setupDropDowns()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
        setupDropDowns()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("AddKidCustomView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        nameTextField.delegate = self
    }
    func setupDropDowns() {
      
        relationshipTextField.optionArray = ["Son", "Daughter", "Others"]
        relationshipTextField.text = ""
        relationshipTextField.selectedRowColor =  #colorLiteral(red: 0.9294117647, green: 0.937254902, blue: 0.9529411765, alpha: 1)
        relationshipTextField.checkMarkEnabled = false
        relationshipTextField.arrowColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        relationshipTextField.isSearchEnable = false
        relationshipTextField.adjustsFontSizeToFitWidth = true
        relationshipTextField.minimumFontSize = 12
        relationshipTextField.delegate = self
        relationshipTextField.didSelect { (string, int1, int2) in
            self.relationshipTextField.text = "\(string)"
            self.memberRelation = string
        }
        
    }
    @IBAction func sonButtonTabbed(_ sender: UIButton) {
        memberRelation = "Son"
        
        buttonStateSelected(button: sender)
        buttonStateNormal(button: daughterButton)
        buttonStateNormal(button: guardianButton)
        self.endEditing(true)

    }
    @IBAction func daughterButtonTabbed(_ sender: UIButton) {
        memberRelation = "Daughter"
        
        buttonStateSelected(button: sender)
        buttonStateNormal(button: sonButton)
        buttonStateNormal(button: guardianButton)
        self.endEditing(true)

    }
    @IBAction func guardianButtonTabbed(_ sender: UIButton) {
        
        memberRelation = "Guardian"
        
        buttonStateSelected(button: sender)
        buttonStateNormal(button: sonButton)
        buttonStateNormal(button: daughterButton)
        self.endEditing(true)

    }
    
}
//MARK: UI Methods
extension AddKidCustomView {
    func buttonStateSelected(button: UIButton) {
        button.backgroundColor = UIColor.harmoneyDarkBlueColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.futuraPTBookFont(size: 16)
        button.borderWidth = 0
        button.borderColor = UIColor.clear
    }
    
    func buttonStateNormal(button: UIButton) {
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5), for: .normal)
        button.titleLabel?.font = UIFont.futuraPTLightFont(size: 16)
        button.borderWidth = 1
        button.borderColor = UIColor.harmoneyOpacityGrayColor.withAlphaComponent(0.5)
    }
}

//MARK: UI Methods
extension AddKidCustomView {
    func setupUI() {
    
    }
}

//MARK: Action Methods
extension AddKidCustomView {
    @IBAction func addKidButtonTapped(_ sender: UIButton) {
        let enteredmobulenumber = formattedNumber(number: mobileNumberTextField.text!)
        let loginedMobileNumber = formattedNumber(number: (UserDefaultConstants().userPhoneNumber?.last)!)
       
        if nameTextField.text?.count ?? 0 > 0 && mobileNumberTextField.text?.count ?? 0 > 0 && memberRelation?.count ?? 0 > 0 {
            self.endEditing(true)
            if mobileNumberTextField.text?.count ?? 0 > 0 {
                guard (mobileNumberTextField.text?.count == 12) else {
                    return self.toast("Enter valid mobile number")
                }
            }
            
            let isMobileNumberContainedAlready = HarmonySingleton.shared.dashBoardData.data?.invitedRequest?.contains(where:{$0.memberMobile == enteredmobulenumber})
            if  isMobileNumberContainedAlready == true {
                self.toast(notifications.mobileNumberAlreadyUser)
                return
            }
            if emailAddressTextField.text?.count ?? 0 > 0 {
                if Utilities.isValidEmail(emailString: emailAddressTextField?.text ?? "") == false {
                    self.toast("Please enter valid email address")
                }else if (enteredmobulenumber == loginedMobileNumber){
                    let errorMessage = notifications.nothavePermissiontoUseSameNumber
                    self.toast(errorMessage)
                }else {
                    
                    let inviteData = InviteData(memberPhoto: "", memberName_encrypted: getEncryptedString(plainText: nameTextField.text), memberMobile_encrypted: getEncryptedString(plainText: mobileNumberTextField.text), memberRelation_encrypted: memberRelation , memberEmail_encrypted: getEncryptedString(plainText: emailAddressTextField.text), inviteStatus: 0)
                
                    delegate?.didTapAddKidButton(addKidCustomView: self, inviteData: inviteData)
                }
              
            } else {
                
                let inviteData = InviteData(memberPhoto: "", memberName_encrypted: getEncryptedString(plainText: nameTextField.text), memberMobile_encrypted: getEncryptedString(plainText: mobileNumberTextField.text), memberRelation_encrypted: memberRelation , memberEmail_encrypted: getEncryptedString(plainText: emailAddressTextField.text), inviteStatus: 0)
            
                
                delegate?.didTapAddKidButton(addKidCustomView: self, inviteData: inviteData)
            }
            
        } else if nameTextField.text?.count == 0 {
            self.toast("Please enter kid name")
        } else if mobileNumberTextField.text?.count == 0 {
            self.toast("Please enter mobile number")
        } else if memberRelation?.count ?? 0 == 0 {
            self.toast(notifications.relationshipMust)
        }
        
        
        /*
         else if emailAddressTextField?.text?.count == 0 {
             self.toast("Please enter email address")
         } else if Utilities.isValidEmail(emailString: emailAddressTextField?.text ?? "") == false{
             self.toast("Please enter valid email address")
         }
         */
//        delegate?.didTapAddKidButton(addKidCustomView: self)
    }
    func reloadInviteKidViewData()  {
        nameTextField.text = ""
        mobileNumberTextField.text = ""
        emailAddressTextField.text = ""
        memberRelation = ""
        buttonStateNormal(button: sonButton)
        buttonStateNormal(button: daughterButton)
        buttonStateNormal(button: guardianButton)
    }
    @IBAction func ProfileImageButtonTapped(_ sender: UIButton) {
        delegate?.didTapProfileImageView(addKidCustomView: self)
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXX-XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
}

//MARK: UITextFieldDelegate Methods
extension AddKidCustomView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumberTextField {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Utilities.formattedMobileNumber(number: newString)
            
            return false
        } else if textField == nameTextField{

            guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
               let substringToReplace = textFieldText[rangeOfTextToReplace]
               let count = textFieldText.count - substringToReplace.count + string.count
           
            do {
               let regex = try NSRegularExpression(pattern: "[^A-Za-z ]", options: [])
               if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                   return false
               }
           }
           catch {
               print("ERROR")
           }
            
            return count <= 30
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool { // return NO to disallow editing.
        print("textFieldShouldBeginEditing")
        relationshipTextField.isUserInteractionEnabled = false
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        print("textFieldShouldEndEditing")
        relationshipTextField.isUserInteractionEnabled = true
        return true
    }
}

