//
//  HomeCollectionViewCell.swift
//  Harmoney
//
//  Created by Csongor Korosi on 6/26/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var addChallenegeLabel: UILabel!
    
    @IBOutlet weak var plusImageView: UIImageView!
    
    var trending: Trending? {
        didSet {
            configureUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//MARK: UI Methods
extension HomeCollectionViewCell {
    func configureUI() {
        if trending == nil {
            
            plusImageView.image = UIImage(named: "no_challenge_placeholder_icon")
            plusImageView.contentMode = .scaleAspectFill
            subtitleLabel.isHidden = true
            plusImageView.isHidden = false
            titleLabel.isHidden = true
            imageView.isHidden = true
            addChallenegeLabel.isHidden = false
            addChallenegeLabel.text = "Add\nChallenge"

        } else{
            plusImageView.isHidden = true
            imageView.isHidden = false
            subtitleLabel.isHidden = false
            addChallenegeLabel.isHidden = true
            titleLabel.isHidden = false

        if trending?.type == 1 {
            titleLabel.text = trending?.typeName
            
            if trending?.typeName == "Walk" {
                imageView.image = UIImage(named: "home_walk_icon")
                imageView.contentMode = .center
                subtitleLabel.text = String(format: "Task: 2000 Steps", trending?.task ?? "")
            } else if trending?.typeName == "Run" {
                imageView.image = UIImage(named: "home_run_icon")
                subtitleLabel.text = String(format: "Task: 2 Miles", trending?.task ?? "")
            }else if trending?.typeName == "Bike" {
               imageView.image = UIImage(named: "bike_icon")
               subtitleLabel.text = String(format: "Task: 2 Miles", trending?.task ?? "")
           } else if trending?.typeName == "Swim" {
            imageView.image = UIImage(named: "home_swim_icon")
            subtitleLabel.text = String(format: "Task: 2 Laps", trending?.task ?? "")
        }
        } else if trending?.type == 2 {
//            imageView.imageFromURL(urlString: trending?.imageURL ?? "")
            imageView!.sd_setImage(with: URL.init(string: trending?.imageURL ?? ""), completed: nil)
//            imageView.contentMode = .scaleAspectFit
//            imageView.contentMode = .scaleAspectFill
            imageView.contentMode = .scaleAspectFit
            titleLabel.text = trending?.typeName
            subtitleLabel.text = String(format: "%@", trending?.task ?? "")
        }
        }
    }
}
