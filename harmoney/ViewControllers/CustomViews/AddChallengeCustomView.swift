//
//  AddChallengeCustomView.swift
//  Harmoney
//
//  Created by Csongor Korosi on 5/21/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import Foundation
import UIKit
import iOSDropDown
import Toast_Swift
import CRRefresh

protocol AddChallengeCustomViewDelegate: class {
    func didTapAddChallengeButton(addChallengeCustomView: AddChallengeCustomView, personalChallenge: PersonalChallenge)
    func didTapCloseButton(addChallengeCustomView: AddChallengeCustomView)
    func didTapInviteParentButton(addChallengeCustomView: AddChallengeCustomView)
    func pullToRefresh()

}

class AddChallengeCustomView: UIView {
    var homeModel: HomeModel? {
        return HomeManager.sharedInstance.homeModel
    }
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var acceptChallengeBtn: UIButton!
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var taskCounterLabel: UILabel!
    @IBOutlet weak var experianceLabel: UILabel!
    @IBOutlet weak var challengeImageView: UIImageView!
    @IBOutlet weak var stepsTextField: UITextField!
    @IBOutlet weak var actionsDropDown: DropDown!
    @IBOutlet weak var cardsDropDown: DropDown!
    @IBOutlet weak var harmoneyBucksTextField: UITextField!
    @IBOutlet weak var autoSpendLabel: UILabel!
    @IBOutlet weak var harmoneyImageView: UIImageView!
    @IBOutlet weak var starsImageView: UIImageView!
    @IBOutlet weak var secondStarsImageView: UIImageView!
    @IBOutlet weak var harmoneyBucksLabel: UILabel!
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var acceptInviteView: UIView!
    @IBOutlet weak var customRewardTextField: UITextField!
    @IBOutlet weak var harmoneyBucksView: UIView!
    @IBOutlet weak var inviteLabel: UILabel!
    @IBOutlet weak var acceptChallengeButtonBottomConstrait: NSLayoutConstraint!
    
    @IBOutlet weak var acceptinviteViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var hbucksinfoLabel: UILabel!
    @IBOutlet weak var innerViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var assignToMainViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var assignToHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var memberAddCollectionViewWidthConstrain: NSLayoutConstraint!
    @IBOutlet weak var memberAddCollectionView: UICollectionView!
    @IBOutlet weak var assignToMainView: UIView!
    @IBOutlet weak var initeViewHightConstraint: NSLayoutConstraint!
    weak var delegate: AddChallengeCustomViewDelegate?
    override var isHidden: Bool {
            get {
                return super.isHidden
            }
            set(newValue) {
                super.isHidden = newValue
                if newValue == false{
                    self.getAssignedMemberdata()
                }
            }
        }
    var personalChallenge : PersonalChallenge? {
        didSet {
//            commonInit()
            setupUI()
//            commonInit()
            self.updateCollectionviewSize()
            setupRefreshControll()
           // getAssignedMemberdata()
        }
    }
    
    
    
//MARK: - Lifecycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
        
    }
    
    
//    private func getAssignedMemberdata() {
//        PersonalFitnessManager.sharedInstance.assingToList { (result) in
//            DispatchQueue.main.async {
//                switch result {
//                case .success(_):
//                    self.memberAddCollectionView.reloadData()
//                case .failure(let error):
//                    print(error.debugDescription)
//                }
//            }
//
//        }
//    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("AddChallengeCustomView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        memberAddCollectionView.dataSource = self
        memberAddCollectionView.delegate = self
        memberAddCollectionView.register(UINib(nibName: ChoreHomeMemberCollectionViewCell.identifier, bundle: .main), forCellWithReuseIdentifier: ChoreHomeMemberCollectionViewCell.identifier)
        self.getAssignedMemberdata()
    }
    
    private func getAssignedMemberdata() {
        PersonalFitnessManager.sharedInstance.assingToList { (result) in
            switch result {
            case .success(_):
//                self.addChallengeCustomView.personalChallenge = self.personalChallenge
                DispatchQueue.main.async {
                    self.updateCollectionviewSize()
                }
            case .failure(let error):
                self.toast(error.localizedDescription)
            }
        }
    }
    
    
    func updateCollectionviewSize(){
        print(HarmonySingleton.shared.assigntomemberDataList as Any)
//                self.choreUITable.reloadData()
        var widthForCollectionView:CGFloat = 80
        
        if HarmonySingleton.shared.assigntomemberDataList?.count ?? 0 > 1{
            widthForCollectionView = CGFloat((HarmonySingleton.shared.assigntomemberDataList?.count ?? 0) * 80 + 20)
        }
        if widthForCollectionView > 400
        {
            self.memberAddCollectionViewWidthConstrain.constant = 400
        }else{
            self.memberAddCollectionViewWidthConstrain.constant = widthForCollectionView
        }
        self.memberAddCollectionView.reloadData()
    }
    
//MARK: UI Methods
    func setupUI() {
        configureHarmoneyBucks()
        acceptChallengeBtn.isUserInteractionEnabled = true
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
//        self.addGestureRecognizer(tapGesture)
        
        //setupDropDowns()
        setupTextFields()
        titleLabel.text = personalChallenge?.data.challengeType.rawValue
        harmoneyImageView.roundCorners([.topLeft, .bottomLeft], radius: 11)
       
    
        
        setupExperianceAttributedLabel()
        switch personalChallenge?.data.challengeType {
        case .walk:
            challengeImageView.image = UIImage(named: "walk_icon")
            taskNameLabel.text = "No. of Steps*"
            setupTaskCounterAttributedLabel(challengeTypeString: " Steps")
            break
        case .swim:
            challengeImageView.image = UIImage(named: "swim_icon")
            setupTaskCounterAttributedLabel(challengeTypeString: " Laps")
            taskNameLabel.text = "No. of Laps*"
            break
        case .run:
            challengeImageView.image = UIImage(named: "run_icon")
            taskNameLabel.text = "No. of Miles*"
            setupTaskCounterAttributedLabel(challengeTypeString: " Miles")
            break
        case .bike:
            challengeImageView.image = UIImage(named: "bike_icon")
            taskNameLabel.text = "No. of Miles*"
            setupTaskCounterAttributedLabel(challengeTypeString: " Miles")
            break
        case .none:
            break
        }
    }
    
    func setupRefreshControll() {
        
        self.memberAddCollectionView.reloadData()
        scrollView.alwaysBounceVertical = true
        scrollView.bounces  = true
        _ = RamotionAnimator(frame: CGRect(x: 0, y: 0, width: scrollView.frame.size.width, height: 100))
        
        let normalAnimator = NormalHeaderAnimator()
        normalAnimator.loadingDescription = "Syncing data .."
        normalAnimator.pullToRefreshDescription = "Drag down to sync"
        normalAnimator.backgroundColor = UIColor(red: 238/255, green: 245/255, blue: 255/255, alpha: 1.0)
        scrollView.cr.addHeadRefresh(animator:normalAnimator) { [weak self] in
            // start refresh
            self?.delegate?.pullToRefresh()
        }
        
    }
    
    func disableHarmoneyBucksAndCustomReward() {
        acceptInviteView.isHidden = false
        acceptinviteViewHeightConstrain.constant = 44
        assignToMainView.isHidden = true
        assignToMainViewHeightConstrain.constant = 0
        innerViewHeightConstrain.constant = 613
       // rewardLabel.textColor = UIColor.harmoneyGrayColor
        harmoneyImageView.image = UIImage(named: "harmoney_disabled_icon")
        harmoneyImageView.backgroundColor = UIColor.harmoneyGrayColor
        starsImageView.image = UIImage(named: "stars_desabled_icon")
        //secondStarsImageView.image = UIImage(named: "stars_desabled_icon")
        harmoneyBucksTextField.backgroundColor = UIColor.harmoneyGrayColor
        harmoneyBucksTextField.textColor = UIColor.harmoneyGrayColor
        harmoneyBucksTextField.isUserInteractionEnabled = false
        if self.hbucksinfoLabel.isHidden == true
        {
            harmoneyBucksTextField.isUserInteractionEnabled = true
        }else{
            harmoneyBucksTextField.isUserInteractionEnabled = false
        }
        harmoneyBucksLabel.textColor = UIColor.harmoneyBlueColor
      //  customRewardTextField.isUserInteractionEnabled = false
      //  customRewardTextField.backgroundColor = UIColor.harmoneyGrayColor
        harmoneyBucksView.backgroundColor = UIColor.white
    }
    
    func enableHarmoneyBucksAndCustomReward() {
        acceptInviteView.isHidden = true
        acceptinviteViewHeightConstrain.constant = 0
        assignToMainView.isHidden = true
        assignToMainViewHeightConstrain.constant = 0
        if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue ?? "2" == RegistrationType.parent.rawValue {
            assignToMainView.isHidden = false
            assignToMainViewHeightConstrain.constant = 130
        }
        innerViewHeightConstrain.constant = 613
        rewardLabel.textColor = UIColor.harmoneyBlueColor
        harmoneyImageView.image = UIImage(named: "logo-1")
        harmoneyImageView.backgroundColor = UIColor.harmoneyLightGrayColor
        starsImageView.image = UIImage(named: "harmoneystar")
        secondStarsImageView.image = UIImage(named: "harmoneystar")
        harmoneyBucksTextField.backgroundColor = UIColor.white
        harmoneyBucksTextField.textColor = UIColor.harmoneyLightOrangeColor
        harmoneyBucksTextField.isUserInteractionEnabled = true
        if self.hbucksinfoLabel.isHidden == true
        {
            harmoneyBucksTextField.isUserInteractionEnabled = true
        }else{
            harmoneyBucksTextField.isUserInteractionEnabled = false
        }
        harmoneyBucksLabel.textColor = UIColor.harmoneyBlueColor
        customRewardTextField.isUserInteractionEnabled = true
        customRewardTextField.backgroundColor = UIColor.white
        harmoneyBucksView.backgroundColor = UIColor.white
    }
    
    func configureHarmoneyBucks() {
        if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue ?? "1" == RegistrationType.child.rawValue && HarmonySingleton.shared.dashBoardData.data?.invite == nil {
            inviteLabel.text = "Invite a parent to earn Harmoney Bucks"
            assignToMainView.isHidden = true
            assignToMainViewHeightConstrain.constant = 0
            innerViewHeightConstrain.constant = 613
            disableHarmoneyBucksAndCustomReward()
        } else if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue ?? "1" == RegistrationType.child.rawValue && (HarmonySingleton.shared.dashBoardData.data?.invite?.inviteStatus == 2 || HarmonySingleton.shared.dashBoardData.data?.invite?.inviteStatus == 0) {
//            acceptChallengeButtonBottomConstrait.constant = 60
            acceptInviteView.isHidden = false
            acceptinviteViewHeightConstrain.constant = 44
            assignToMainView.isHidden = true
            assignToMainViewHeightConstrain.constant = 0
            innerViewHeightConstrain.constant = 613
            disableHarmoneyBucksAndCustomReward()
            
            if HarmonySingleton.shared.dashBoardData.data?.invite?.memberRelation == "Father" {
                inviteLabel.text = "Waiting for your Dad to accept your invite"
            } else if HarmonySingleton.shared.dashBoardData.data?.invite?.memberRelation == "Mother" {
                inviteLabel.text = "Waiting for your Mom to accept your invite"
            } else if HarmonySingleton.shared.dashBoardData.data?.invite?.memberRelation == "Guardian" {
                inviteLabel.text = "Waiting for your Guardian to accept your invite"
            }
        } else if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue ?? "2" == RegistrationType.parent.rawValue && HarmonySingleton.shared.dashBoardData.data?.bankAccount?.isEmpty == nil {
            initeViewHightConstraint.constant = 0
//            acceptChallengeButtonBottomConstrait.constant = 162
            enableHarmoneyBucksAndCustomReward()
            assignToMainView.isHidden = false
            assignToMainViewHeightConstrain.constant = 130
            innerViewHeightConstrain.constant = 800
            //suba start
          //  disableHarmoneyBucksAndCustomReward()
          //  inviteLabel.text = "Please add a bank account to add Harmoney bucks"
            //suba end 
        }  else if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue ?? "3" == RegistrationType.adult.rawValue && HarmonySingleton.shared.dashBoardData.data?.bankAccount?.isEmpty == nil {
            assignToMainView.isHidden = false
            assignToMainViewHeightConstrain.constant = 130
            innerViewHeightConstrain.constant = 800
            //suba start
            //disableHarmoneyBucksAndCustomReward()
            //inviteLabel.text = "Please add a bank account to add Harmoney bucks"
            //suba end
        }else if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue ?? "3" == RegistrationType.adult.rawValue {
            assignToMainView.isHidden = false
            assignToMainViewHeightConstrain.constant = 130
            innerViewHeightConstrain.constant = 800
            initeViewHightConstraint.constant = 0
//            acceptChallengeButtonBottomConstrait.constant = 162
            enableHarmoneyBucksAndCustomReward()
        }
        else {
//            acceptChallengeButtonBottomConstrait.constant = 162
            enableHarmoneyBucksAndCustomReward()
        }
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer?) {
        stepsTextField.resignFirstResponder()
//        actionsDropDown.resignFirstResponder()
//        cardsDropDown.resignFirstResponder()
        harmoneyBucksTextField.resignFirstResponder()
    }
    
    func setupDropDowns() {
        let paddingView2: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
        actionsDropDown.leftView = paddingView2
        actionsDropDown.leftViewMode = .always
        actionsDropDown.optionArray = ["Save", "Share", "Buy"]
        actionsDropDown.text = "Select"
        actionsDropDown.didSelect { (string, int1, int2) in
            self.autoSpendLabel.isHidden = false
        }
        
        let paddingView3: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
        cardsDropDown.leftView = paddingView3
        cardsDropDown.leftViewMode = .always
        cardsDropDown.optionArray = ["$25 Amazon Card", "$50 Amazon Card", "$100 Amazon Card"]
        cardsDropDown.text = "Select"
        cardsDropDown.didSelect { (string, int1, int2) in
            self.autoSpendLabel.isHidden = false
        }
    }
    
    func setupTextFields() {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
        stepsTextField.leftView = paddingView
        stepsTextField.leftViewMode = .always
        stepsTextField.tintColor = UIColor.harmoneyLightOrangeColor
        
        let formatter = Utilities.addNumberFormatter()
        stepsTextField.text = formatter.string(from: (personalChallenge?.data.challengeTask ?? 0) as NSNumber)
        
        harmoneyBucksTextField.tintColor = UIColor.harmoneyBlueColor
        
        var harmoneyBucks = "\(personalChallenge?.data.harmoneyBucks ?? 0)"
        if harmoneyBucks == "0" {
            harmoneyBucks = ""
        }
        harmoneyBucksTextField.text = harmoneyBucks
        
        customRewardTextField.text = "\(personalChallenge?.data.customReward ?? "")"
    }
    
    func setupTaskCounterAttributedLabel(challengeTypeString: String) {
        let formatter = Utilities.addNumberFormatter()
        
        let challengeTaskString = formatter.string(from: (personalChallenge?.data.challengeTask ?? 0) as NSNumber)
        let text = "Complete " + (challengeTaskString ?? "") + challengeTypeString
        let attributedText = NSMutableAttributedString(string: text)
        
        let boldRange = (attributedText.string as NSString).range(of: (challengeTaskString ?? ""))
        let lightRange = (attributedText.string as NSString).range(of: text)

        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBookFont(size: 16)], range:lightRange)
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBoldFont(size: 16)], range:boldRange)
        
        taskCounterLabel.attributedText = attributedText
    }
    
    func setupExperianceAttributedLabel() {
        let experianceString = "\(self.personalChallenge?.data.experiance ?? 0) "
        let diamondString = String(format: "%d", self.personalChallenge?.data.diamond ?? 0)
        
        let xpAttachment = NSTextAttachment()
        let diamondAttachment = NSTextAttachment()
        xpAttachment.image = UIImage(named: "xp")
        let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
        
        if creditType == 1 {
            diamondAttachment.image = UIImage(named: "soccer")

        } else if creditType == 2 {
            diamondAttachment.image = UIImage(named: "diamond")

        } else {
            diamondAttachment.image = UIImage(named: "touchdown")
        }
//        diamondAttachment.image = UIImage(named: "diamond")
        let boldFont = UIFont.futuraPTBoldFont(size: 16)
        
        let imageSize = xpAttachment.image!.size
        let image2Size = diamondAttachment.image!.size
        xpAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - imageSize.height) / 2, width: imageSize.width, height: imageSize.height)
        diamondAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - image2Size.height) / 2, width: image2Size.width, height: image2Size.height)
        
        let xpImageString = NSAttributedString(attachment: xpAttachment)
        let diamondImageString = NSAttributedString(attachment: diamondAttachment)
        
        let attributedText = NSMutableAttributedString(string: "to get ")
        attributedText.append(NSAttributedString(string: experianceString + " "))
        attributedText.append(xpImageString)
        attributedText.append(NSAttributedString(string: "  "))
        attributedText.append(diamondImageString)
        attributedText.append(NSAttributedString(string:"  " + diamondString + " "))
        attributedText.append(NSAttributedString(string: " and start leveling up"))
        
        let lightRange = (attributedText.string as NSString).range(of: attributedText.string)
        let boldRangeExperiance = (attributedText.string as NSString).range(of: experianceString)
        let boldRangeDiamond = (attributedText.string as NSString).range(of: diamondString)
        
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBookFont(size: 16)], range:lightRange)
        attributedText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.harmoneyLightOrangeColor], range:boldRangeExperiance)
        attributedText.addAttributes([NSAttributedString.Key.font: boldFont], range:boldRangeExperiance)
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBoldFont(size: 16)], range:boldRangeDiamond)
        attributedText.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.harmoneyLightOrangeColor], range:boldRangeDiamond)
        
        experianceLabel.attributedText = attributedText
    }
    func setProfilePic(frstName: String?, lstName: String?, profileImageView: UIImageView){
     
         if let firstName = frstName, let lastName = lstName {
             profileImageView.setImageWith(firstName + " " + lastName, color: ConstantString.menuBackSystemProfilepic)
         } else {
             if let dispName = frstName {
                 profileImageView.setImageWith(dispName, color: ConstantString.menuBackSystemProfilepic)
             }
         }
     }
    func checkChallengeTaskIncrement(divideValue: Float, errorMessage: String) -> (Bool) {
        let challengeTask = Float(stepsTextField.text ?? "")
        if challengeTask == 0
        {
            self.makeToast(errorMessage, duration: 3, position: .top, style: ToastStyle())
            stepsTextField.text = String(format: "%0.f", divideValue)
            let challengeTaskInt  = Int(stepsTextField.text ?? "") ?? 0
            personalChallenge?.data.challengeTask = challengeTaskInt
            let experience = experianceBasedOnChallengeTask(challengeTask: challengeTaskInt)
            personalChallenge?.data.experiance = experience
            return false
        }else if challengeTask?.truncatingRemainder(dividingBy: divideValue) != 0.0 {
            self.makeToast(errorMessage, duration: 3, position: .top, style: ToastStyle())
            stepsTextField.text = String(format: "%0.f", divideValue)
            let challengeTaskInt  = Int(stepsTextField.text ?? "") ?? 0
            personalChallenge?.data.challengeTask = challengeTaskInt
            let experience = experianceBasedOnChallengeTask(challengeTask: challengeTaskInt)
            personalChallenge?.data.experiance = experience
            
            return false
        }
        
        return true
    }
    
    func experianceBasedOnChallengeTask(challengeTask: Int) -> Int {
        let experiance = 200
        var numberOfTasks = Int()
        let type = personalChallenge?.data.challengeType
        
        switch type {
        case .walk:
            numberOfTasks = challengeTask / 2000
            break
        case .swim:
            numberOfTasks = challengeTask / 2
            break
        case .bike:
            numberOfTasks = challengeTask / 2
            break
        case .run:
            numberOfTasks = challengeTask / 2
            break
        case .none:
            break
        }
        
        let experianceFromChallengeTask = experiance * Int(numberOfTasks)
        
        return experianceFromChallengeTask
    }
}

//MARK: Action Methods
extension AddChallengeCustomView {
    @IBAction func addChallengeButtonTapped(_ sender: UIButton) {
        dismissKeyboard(nil)
        sender.isUserInteractionEnabled = false
        var memberId:[String] = []
        if let assigntomemberDataList = HarmonySingleton.shared.assigntomemberDataList{
            for objMember in assigntomemberDataList{
                if objMember.isSelectAvathar ?? false {
                    if let fromguid = objMember.fromGuid{
                        memberId.append(fromguid)
                    }
                }
            }
        }
        if HarmonySingleton.shared.assigntomemberDataList?.count == 1{
            memberId.append(HarmonySingleton.shared.assigntomemberDataList?.first?.memberId ?? "")

        }
        
        if memberId.count == 0
        {
            if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
            }else{
                self.toast("Please select member to assign Challenge")
                sender.isUserInteractionEnabled = true
                return
            }
            
        }
        delegate?.didTapAddChallengeButton(addChallengeCustomView: self, personalChallenge: personalChallenge!)
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismissKeyboard(nil)
        delegate?.didTapCloseButton(addChallengeCustomView: self)
    }
    
    @IBAction func inviteParentButtonTapped(_ sender: UIButton) {
        dismissKeyboard(nil)

        delegate?.didTapInviteParentButton(addChallengeCustomView: self)
    }
}

//MARK: UITextFieldDelegate Methods
extension AddChallengeCustomView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == actionsDropDown {
           actionsDropDown.showList()
        } else if textField == cardsDropDown {
            cardsDropDown.showList()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == harmoneyBucksTextField {
            guard let textFieldText = harmoneyBucksTextField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 2
        }
     
        return true
    }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == stepsTextField {
            let challengeTask  = Int(textField.text ?? "") ?? 0
            
            switch personalChallenge?.data.challengeType {
            case .walk:
                
                if checkChallengeTaskIncrement(divideValue: 2000, errorMessage: ConstantString.stepsIcrementsString) {
                    personalChallenge?.data.challengeTask = challengeTask
                    let experience = experianceBasedOnChallengeTask(challengeTask: challengeTask)
                    personalChallenge?.data.experiance = experience
                }
                break
            case .swim:
                if checkChallengeTaskIncrement(divideValue: 2, errorMessage: ConstantString.swimIcrementsString) {
                    personalChallenge?.data.challengeTask = challengeTask
                    let experience = experianceBasedOnChallengeTask(challengeTask: challengeTask)
                    personalChallenge?.data.experiance = experience
                }
                break
            case .bike:
                if checkChallengeTaskIncrement(divideValue: 2, errorMessage: ConstantString.bikeIcrementsString) {
                    personalChallenge?.data.challengeTask = challengeTask
                    let experience = experianceBasedOnChallengeTask(challengeTask: challengeTask)
                    personalChallenge?.data.experiance = experience
                }
                break
            case .run:
                if checkChallengeTaskIncrement(divideValue: 2, errorMessage: ConstantString.runIcrementsString) {
                    personalChallenge?.data.challengeTask = challengeTask
                    let experience = experianceBasedOnChallengeTask(challengeTask: challengeTask)
                    personalChallenge?.data.experiance = experience
                }
                break
            case .none:
                break
            }
        } else if textField == harmoneyBucksTextField {
            let harmoneyBuks = textField.text
            
            personalChallenge?.data.harmoneyBucks = (Int(harmoneyBuks ?? "") ?? 0)
            
            
        } else if textField == customRewardTextField {
            if textField.text?.count ?? 0 <= 50 {
                personalChallenge?.data.customReward = customRewardTextField.text ?? ""
            } else {
                self.toast("Custom reward is too long, we accept max. 50 characters")
            }
        }
    }
    
    @IBAction func customRewardTextfieldEditingChanged(_ sender: UITextField) {
        personalChallenge?.data.customReward = customRewardTextField.text ?? ""
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == actionsDropDown {
           return false
        } else if textField == cardsDropDown {
            return false
        }
        
        return true
    }
}

extension AddChallengeCustomView: UICollectionViewDelegate {
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        let width = (self.frame.size.width - 12 * 3) / 3 //some width
//        let height = width * 1.5 //ratio
//        return CGSize(width: width, height: height)
//    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let isSelectedAvathar = !(HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].isSelectAvathar ?? false)
        HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].isSelectAvathar = isSelectedAvathar
        
        collectionView.reloadItems(at: [indexPath])
//        collectionView.reloadData()
    }
}

extension AddChallengeCustomView: UICollectionViewDataSource {
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return HarmonySingleton.shared.assigntomemberDataList?.count ?? 0
//        return 2
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChoreHomeMemberCollectionViewCell", for: indexPath) as! ChoreHomeMemberCollectionViewCell
       //cell.contentView.isUserInteractionEnabled = true
        cell.memberName.text = HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].memberName ?? "No name"
        
        if HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].memberPhoto == "" {
            self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: cell.memberImageView)
        } else {
            cell.memberImageView.imageFromURL(urlString: HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].memberPhoto ?? "")
        }
        
        if HarmonySingleton.shared.assigntomemberDataList?.count == 1{

            cell.tickIcon.isHidden = false
        }else{
            cell.tickIcon.isHidden = !(HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].isSelectAvathar ?? false)

        }
        
        if indexPath.row == 0{
            if(cell.tickIcon.isHidden){
                self.harmoneyBucksTextField.isUserInteractionEnabled = true
                self.hbucksinfoLabel.isHidden = true
            }else{
                self.harmoneyBucksTextField.text = ""
                self.harmoneyBucksTextField.isUserInteractionEnabled = false
                if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
                    self.hbucksinfoLabel.isHidden = true
                }else{
                    self.hbucksinfoLabel.isHidden = false
                }
            }
        }
        
//        cell.tickIcon.isHidden = !(HarmonySingleton.shared.assigntomemberDataList?[indexPath.row].isSelectAvathar ?? false)


        
        return cell
        
    }
    
    
   
}
