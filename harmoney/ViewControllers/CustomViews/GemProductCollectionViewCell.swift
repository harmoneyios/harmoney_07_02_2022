//
//  GemProductCollectionViewCell.swift
//  Harmoney
//
//  Created by Csongor Korosi on 7/14/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit

protocol GemProductCollectionViewCellDelegate: class {
    func didTapBuyButton(coupon: NonPromoList, gemProductCollectionViewCell: GemProductCollectionViewCell)
}

class GemProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    
    weak var delegate: GemProductCollectionViewCellDelegate?
    
    var coupon: NonPromoList? {
        didSet {
            configureUI()
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//MARK: UI Methods

extension GemProductCollectionViewCell {
    func configureUI() {
        imageView.imageFromURL(urlString: coupon?.strTaskThumbnailImage ?? "")
        priceLabel.text = String(format: "%d", coupon?.intProductClaimValue ?? 0)
        discountLabel.text = coupon?.strProductOffValueText ?? ""
    }
}

//MARK: Action Methods

extension GemProductCollectionViewCell {
    @IBAction func buyButtonTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 0.5
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }) { (finished) in
            UIView.animate(withDuration: 0.5, animations: {
                sender.alpha = 1
                sender.transform = CGAffineTransform.identity
            })
        }
        
        delegate?.didTapBuyButton(coupon: coupon!, gemProductCollectionViewCell: self)
    }
}





