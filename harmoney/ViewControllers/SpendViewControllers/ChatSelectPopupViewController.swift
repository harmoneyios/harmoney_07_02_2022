//
//  ChatSelectPopupViewController.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 02/06/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class ChatSelectPopupViewController: UIViewController {

//    var delegate : AddChorePopupReceived?
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var taskFourLabel: UIButton!
    @IBOutlet weak var taskTwoLabel: UIButton!
    @IBOutlet weak var taskThreeLabel: UIButton!
    @IBOutlet weak var taskOneLabel: UIButton!
    @IBOutlet weak var choreImg: UIImageView!
    var taskTotalCount:String = ""
    
    @IBOutlet weak var imgVw: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        choreImg.imageFromURL(urlString: CreateChoreVM.shared.defaultChoreObj?.choserImage ?? "")
              titleLbl.adjustsFontSizeToFitWidth = true
//              if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child {
//                  titleLbl.text = "Yay! Chore has been added! Waiting for your parent's approval"
//              }else{
//                  titleLbl.text = "Yay! Chore has been successfully added!"
//              }
    }
    
    @IBAction func taskOneAct(_ sender: Any) {
        taskOneLabel.backgroundColor =   UIColor(red: 41.0/255.0, green: 72.0/255.0, blue: 109.0/255.0, alpha: 1.0)
        taskTwoLabel.backgroundColor = UIColor.white
        taskThreeLabel.backgroundColor = UIColor.white
      //  taskFourLabel.backgroundColor = UIColor.white
        taskTotalCount = "1"
        
    }
    @IBAction func tasktwoAct(_ sender: Any) {
        taskOneLabel.backgroundColor = UIColor.white
        taskTwoLabel.backgroundColor =   UIColor(red: 41.0/255.0, green: 72.0/255.0, blue: 109.0/255.0, alpha: 1.0)
        taskThreeLabel.backgroundColor = UIColor.white
    //    taskFourLabel.backgroundColor = UIColor.white
        taskTotalCount = "2"
    }
    @IBAction func taskthreeAct(_ sender: Any) {
        taskOneLabel.backgroundColor = UIColor.white
        taskTwoLabel.backgroundColor = UIColor.white
        taskThreeLabel.backgroundColor =  UIColor(red: 41.0/255.0, green: 72.0/255.0, blue: 109.0/255.0, alpha: 1.0)
     //   taskFourLabel.backgroundColor = UIColor.white
        taskTotalCount = "3"
    }
    @IBAction func taskfourAct(_ sender: Any) {
        taskOneLabel.backgroundColor = UIColor.white
        taskTwoLabel.backgroundColor = UIColor.white
        taskThreeLabel.backgroundColor = UIColor.white
      //  taskFourLabel.backgroundColor =  UIColor(red: 41.0/255.0, green: 72.0/255.0, blue: 109.0/255.0, alpha: 1.0)
        taskTotalCount = "4"
    }
    
    
    @IBAction func gotItClick(_ sender: Any) {
//        delegate?.newChoreAdded()
        UserDefaults.standard.set(taskTotalCount, forKey: "taskTotalCount")
        self.dismiss(animated: true, completion: nil)
    }
}



//
//import UIKit
//protocol AddChorePopupReceived {
//    func newChoreAdded()
//}
