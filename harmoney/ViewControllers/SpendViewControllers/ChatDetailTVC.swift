//
//  ChatDetailTVC.swift
//  ChatUIDemo
//
//  Created by Mrunal Thanki on 12/06/18.
//  Copyright © 2018 Mrunal Thanki. All rights reserved.
//

import UIKit
class ChatDetailCell: UITableViewCell {
    
    @IBOutlet var lblSender : UILabel?
    @IBOutlet var lblDate : UILabel?
    @IBOutlet var lblContent : UILabel?
    @IBOutlet var viewContainer : UIView?
    @IBOutlet var imgChecked : UIImageView?

}
