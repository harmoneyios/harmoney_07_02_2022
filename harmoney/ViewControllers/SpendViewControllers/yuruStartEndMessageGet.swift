//
//  yuruStartEndMessageGet.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 21/06/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation

struct yuruMessage: Codable {
    var data: yuruMessageList
}

struct yuruMessageList: Codable {
    
    let type: Int
    let guid: String
    let date: String
    let time: String
    let msg: String

    enum CodingKeys: String, CodingKey {
        case type, guid, date, time, msg
    }
}

