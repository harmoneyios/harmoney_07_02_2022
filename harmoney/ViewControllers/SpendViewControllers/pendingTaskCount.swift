//
//  pendingTaskCount.swift
//  Harmoney
//
//  Created by Ravikumar Narayanan on 06/06/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation



struct pendingTaskCount: Codable {
    var data: pendingTaskList
}

struct pendingTaskList: Codable {
   
    let dailyChallengeTaskCompletionCount, totalCompletion, totalPending: Int
   
    let personalFitnessTaskCompletionCount: Int
    let choresTaskCompletionCount: Int
    let msg: String

    enum CodingKeys: String, CodingKey {
    
        
        case dailyChallengeTaskCompletionCount, totalCompletion, totalPending, personalFitnessTaskCompletionCount, choresTaskCompletionCount, msg
    }
}
