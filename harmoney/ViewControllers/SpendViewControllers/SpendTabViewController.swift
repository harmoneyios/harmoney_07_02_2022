//
//  SpendTabViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 23/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SVProgressHUD
import AVFoundation
import iRecordView
import ObjectMapper
import IQKeyboardManagerSwift

public enum ChatPreviousVC {
    case home
    case profile
    case aboutHarmoney
    case privacyPolicy
    case termsConditions
    case aboutUs
    case earn
    case fitness
    case sideMenu
    case myStuf
    case none
}



class SpendTabViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,RecordViewDelegate,AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    @IBOutlet weak var lblTimeMorningQuestion: UILabel!
    @IBOutlet weak var lblTimeMorningAnswer: UILabel!
    @IBOutlet weak var lblTimeMorningReply: UILabel!
    @IBOutlet weak var lblTimeMorningBarChart: UILabel!
    
    @IBOutlet weak var lblTimeInfo: UILabel!
    
    @IBOutlet weak var lblTimeNightQuestion: UILabel!
    @IBOutlet weak var lblTimeNightAnswer: UILabel!
    @IBOutlet weak var lblTimeNightReply: UILabel!
    @IBOutlet weak var lblTimeNightBarChart: UILabel!
    
    @IBOutlet weak var vwGoalAchivement: UIView!
    @IBOutlet weak var vwTopGoalInfo: UIView!
    
    @IBOutlet weak var lblGoalRemainInfo: UILabel!
    
    @IBOutlet weak var vwGoalStack: UIStackView!
    var initialHealthKitCall = false
    var foreGroundHealthKitCall = false
    var timerSteps : Timer?
    var ch1Obj : ChallengeData?
    
    func setInitialValueInGoalview(){
        vwTopGoalInfo.topCorners(radius: 20)
        for i in 1...6{
            if let vw = vwGoalStack.viewWithTag(i){
                
                if let btn = vw.viewWithTag(666) as? UIButton{
                    btn.isSelected = false
                }
                if let vwLeft = vw.viewWithTag(777){
                    vwLeft.backgroundColor =  UIColor.harmoneyGrayColor
                }
                if let vwRight = vw.viewWithTag(888){
                    vwRight.backgroundColor =  UIColor.harmoneyGrayColor
                }
            }
        }
    }
  
    func setGoalViewBasedOnGoalCount(cnt: Int){
        
        setInitialValueInGoalview()
        
        let remainCount = 5 - cnt
        if remainCount <= 0{
            // Success
            lblGoalRemainInfo.text = "Congratulations! You have achieved all your goals!"
        }else{
           var goalText = "goals"
            if remainCount == 1{
                goalText = "goal"
            }
            lblGoalRemainInfo.text = "Only \(remainCount) more \(goalText) to go!"
        }
        for i in 1...6{
            if let vw = vwGoalStack.viewWithTag(i){
                if i <= cnt || cnt > 5{
                    if let btn = vw.viewWithTag(666) as? UIButton{
                        btn.isSelected = true
                    }
                    if let vwLeft = vw.viewWithTag(777){
                        vwLeft.backgroundColor =  UIColor.hflBlue
                    }
                    if let vwRight = vw.viewWithTag(888) {
                        if i != cnt || cnt >= 5{
                            vwRight.backgroundColor =  UIColor.hflBlue
                        }
                    }
                    }
            }
        }
    }
    func onStart() {
        
        let userDefaultsYuruTypeCheck = UserDefaults.standard
            let userAge = userDefaultsYuruTypeCheck.integer(forKey: "yuruTypeIdPersonalNightMessage")
        if userAge == 5 {
            
    //        stateLabel.text = "onStart"
            nightReplyRecordView.isHidden = false
            nightReplyTextField.isHidden = true
            if audionightRecorder?.isRecording == false{
    //               playButton.isEnabled = false
    //               stopButton.isEnabled = true
                audionightRecorder?.record()
               }
            print("onStartNight")
            return
        }
//        stateLabel.text = "onStart"
        RecordView.isHidden = false
        txtfield.isHidden = true
        if audioRecorder?.isRecording == false{
//               playButton.isEnabled = false
//               stopButton.isEnabled = true
               audioRecorder?.record()
           }
        print("onStart")
    }
    
    func onCancel() {
//        stateLabel.text = "onCancel"
        let userDefaultsYuruTypeCheck = UserDefaults.standard
            let userAge = userDefaultsYuruTypeCheck.integer(forKey: "yuruTypeIdPersonalNightMessage")
        if userAge == 5 {
            print("onCancel")
            nightReplyView.isHidden = false
            nightReplyTextField.isHidden = false
            nightReplyRecordView.isHidden = true
            return
        }
        print("onCancel")
        replyView.isHidden = false
        txtfield.isHidden = false
        RecordView.isHidden = true

    }
    
    func onFinished(duration: CGFloat) {
        let userDefaultsYuruTypeCheck = UserDefaults.standard
            let userAge = userDefaultsYuruTypeCheck.integer(forKey: "yuruTypeIdPersonalNightMessage")
        if userAge == 5 {
            if audionightRecorder?.isRecording == true{
                audionightRecorder?.stop()
            }else{
                audionightPlayer?.stop()
            }
            if duration > 15.0{
                
            
    //        stateLabel.text = "onFinished duration: \(duration)"
            print("onFinished \(duration)")
    //        stopButton.isEnabled = false
    //          playButton.isEnabled = true
    //          recordButton.isEnabled = true

            nightAnswerView.isHidden = false
                nightAnswerViewHeightConstrain.constant = 50
                nightProfileImage.isHidden = false
                nightProfileImageConstrain.constant = 30
                
                
     if homeModel?.data.profilePic == "" {
                 self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: nightProfileImage)
             } else {
              
                 if let url = URL(string: homeModel?.data.profilePic ?? "") {

                     if url.absoluteString.range(of: "jpg") != nil {

                         self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: nightProfileImage)
                     }

                 }else{
                    nightProfileImage.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
                 }

                 
             }

                
                nightProfileImage.layer.cornerRadius = nightProfileImage.frame.size.height/2
                nightProfileImage.layer.borderWidth = 0
        //        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
                nightProfileImage.clipsToBounds = true
        //        self.view.addSubview(answerImage)
                nightAnswerLabel.isHidden = true
            nightReplyView.isHidden = true
                nightReplyTextField.isHidden = true
                nightReplyRecordView.isHidden = true
                nightReplyRecordView.isHidden = true
    //            replyViewHeightConstrain.constant = 0
                    nightPlayBtn.isHidden = false
            SVProgressHUD.show()
            var text:String = txtfield.text ?? ""
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": "Bearer \(self.accessToken)",
            ]
            
            AF.upload(multipartFormData: { (datavalue) in
                datavalue.append("Yes happy day looking great and awesome day thanks for the day".data(using: .utf8)!, withName: "text")
                datavalue.append("2020-11-03T19:38:27+0300".data(using: .utf8)!, withName: "recorded_at")
                datavalue.append("en-US".data(using: .utf8)!, withName: "language_code")
                
            }, to: "https://api.dev.cogapps.net/simplified/", headers: headers).response { response in
                SVProgressHUD.dismiss()
                if let error = response.error {
                    print(error.localizedDescription)
                    return
                }
                
                if let data = response.data {
                    do {
                        updateGoals(goalType: .hbotMorning) { (countValue) in
                            self.setGoalViewBasedOnGoalCount(cnt: countValue)
                        }
                        self.showGoalSuccessPopup()
                        if let serializationObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]{
                            self.saveHbotToPlist(dictData: serializationObj,isMorning: false)
                            self.getReplyNightView.isHidden = false
                            self.getReplyNightViewHeightConstrain.constant = 80
                            self.nightReplyImg.isHidden = false
                            self.nightReplyImageHeightConstrain.constant = 30
                            self.getReplyNightLabel.isHidden = false
                            if let emotion = serializationObj["emotion"] as? String {
                                if emotion == "negative"{
                                    self.getReplyNightLabel.text = "Looks like you are feeling sad today and we hope you feel better. 😔"
                                }else{
                                    self.getReplyNightLabel.text = "Great to see that you feel happy today 😀"
                                }
                                
                            }
                            if let pairs = serializationObj["pairs"] as? [[String:Any]]{
                                self.chartValuesforGraphNight .removeAll()
                                self.chartDateArrayforGraphNight.removeAll()
                                for object in pairs {
                                    if let emotion = object["emotion"] as? String {
                                        self.chartDateArrayforGraphNight.append(emotion)
                                        for (index, element) in self.chartDateArrayforGraphNight.enumerated() {
                                            self.chartDateArrayforGraphNight[index] = element.capitalized
                                        }
                                    }
                                    if let value = object["value"] as? CGFloat {
                                        self.chartValuesforGraphNight.append(value)
                                    }
                                }
                                print(self.chartValuesforGraphNight)
                                print(self.chartDateArrayforGraphNight)
                                self.nightBarChartMainView.isHidden = false
                                self.nightBarChartMainHeightConstrain.constant = 329
                                self.nightBarChartProfileImg.isHidden = false
                                self.nightBarChartImageHeight.constant = 30
                                self.addSingleLineGrapghGraphNight()
                                let userDefaults = UserDefaults.standard
                                userDefaults.set("playAudioNight", forKey: "adioRecordNight")
                                self.addSingleLineGrapghGraph()
                                let now = Date()
                                let formatter = DateFormatter()
                                formatter.dateFormat = "yyyyMMdd"
                                let nowString = formatter.string(from: now)
                                UserDefaults.standard.set(nowString, forKey: "savedDate_hbotData")

                            }
                        }
                        
                    } catch let error {
                        print(error.localizedDescription)
                    }
    //                print(String.init(data: data, encoding: .utf8)!)

                }
                print(response)
                
            }
            }else{
                nightReplyView.isHidden = false
                getReplyNightView.isHidden = false
                self.getReplyNightViewHeightConstrain.constant = 80
                self.nightReplyImg.isHidden = false
                self.nightReplyImageHeightConstrain.constant = 30
                getReplyNightLabel.text = "Please take a minimum of 15 seconds to record your experience for an effective assessment ⚠️"
                nightAnswerView.isHidden = false
                nightAnswerLabel.isHidden = true
//                nightLabel.text = ""
                nightPlayBtn.isHidden = false
                nightAnswerViewHeightConstrain.constant = 50
                nightProfileImage.isHidden = false
                nightProfileImageConstrain.constant = 30
//                answerImageHeightConstrain.constant = 30
            
                if homeModel?.data.profilePic == "" {
                            self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: nightProfileImage)
                        } else {
                         
                            if let url = URL(string: homeModel?.data.profilePic ?? "") {

                                if url.absoluteString.range(of: "jpg") != nil {

                                    self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: nightProfileImage)
                                }

                            }else{
                               nightProfileImage.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
                            }

                            
                        }
                
                nightProfileImage.layer.cornerRadius = nightProfileImage.frame.size.height/2
                nightProfileImage.layer.borderWidth = 0
        //        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
                nightProfileImage.clipsToBounds = true
        //        self.view.addSubview(answerImage)
                nightReplyView.isHidden = false
                nightReplyTextField.isHidden = false
                nightReplyRecordView.isHidden = true
            }
            return
        }
        if audioRecorder?.isRecording == true{
            audioRecorder?.stop()
        }else{
            audioPlayer?.stop()
        }
        if duration > 15.0{
            
        
//        stateLabel.text = "onFinished duration: \(duration)"
        print("onFinished \(duration)")
//        stopButton.isEnabled = false
//          playButton.isEnabled = true
//          recordButton.isEnabled = true

        answerView.isHidden = false
            answerViewHeightConstrain.constant = 50
            answerImage.isHidden = false
//            answerImageHeightConstrain.constant = 30
            
            if homeModel?.data.profilePic == "" {
                        self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                    } else {
                     
                        if let url = URL(string: homeModel?.data.profilePic ?? "") {

                            if url.absoluteString.range(of: "jpg") != nil {

                                self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                            }

                        }else{
                            answerImage.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
                        }

                        
                    }
            answerImage.layer.cornerRadius = answerImage.frame.size.height/2
            answerImage.layer.borderWidth = 0
    //        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
            answerImage.clipsToBounds = true
    //        self.view.addSubview(answerImage)
        answerLbl.isHidden = true
        replyView.isHidden = true
            RecordView.isHidden = true
//            replyViewHeightConstrain.constant = 0
                playBtn.isHidden = false
        SVProgressHUD.show()
        var text:String = txtfield.text ?? ""
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(self.accessToken)",
        ]
        
        AF.upload(multipartFormData: { (datavalue) in
            datavalue.append("Yes happy day looking great and awesome day thanks for the day".data(using: .utf8)!, withName: "text")
            datavalue.append("2020-11-03T19:38:27+0300".data(using: .utf8)!, withName: "recorded_at")
            datavalue.append("en-US".data(using: .utf8)!, withName: "language_code")
            
        }, to: "https://api.dev.cogapps.net/simplified/", headers: headers).response { response in
            SVProgressHUD.dismiss()
            if let error = response.error {
                print(error.localizedDescription)
                return
            }
            
            if let data = response.data {
                do {
                    
                    if let serializationObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]{
                        updateGoals(goalType: .hbotMorning){ (countValue) in
                            self.setGoalViewBasedOnGoalCount(cnt: countValue)
                        }
                        self.showGoalSuccessPopup()
                        self.saveHbotToPlist(dictData: serializationObj)
                        if let dictMorningData = HarmonySingleton.dictHbotData?["morningMessageData"] as? [String: Any]{
                            self.showMorningMessageData(serializationObj: dictMorningData)
                        }
                        
                        let userDefaults = UserDefaults.standard
                        userDefaults.set("playAudio", forKey: "adioRecord")
                        let now = Date()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyyMMdd"
                        let nowString = formatter.string(from: now)
                        UserDefaults.standard.set(nowString, forKey: "savedDate_hbotData")
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                }
//                print(String.init(data: data, encoding: .utf8)!)

            }
            let userDefaults = UserDefaults.standard
            userDefaults.set(text, forKey: "replyKey")
            print(response)
            
        }
        }else{
            getreplyView.isHidden = false
            self.getReplyHeightConstrain.constant = 80
            self.getReplyViewProfileImage.isHidden = false
            self.getReplyViewProfileImageHeightConstrain.constant = 30
            getReplyLbl.text = "Please take a minimum of 15 seconds to record your experience for an effective assessment ⚠️"
            answerView.isHidden = false
            answerLbl.isHidden = true
            answerLbl.text = ""
            playBtn.isHidden = false
            answerViewHeightConstrain.constant = 50
            answerImage.isHidden = false
//            answerImageHeightConstrain.constant = 30
            
         
            
            if homeModel?.data.profilePic == "" {
                        self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                    } else {
                     
                        if let url = URL(string: homeModel?.data.profilePic ?? "") {

                            if url.absoluteString.range(of: "jpg") != nil {

                                self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                            }

                        }else{
                            answerImage.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
                        }

                        
                    }
            
            answerImage.layer.cornerRadius = answerImage.frame.size.height/2
            answerImage.layer.borderWidth = 0
    //        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
            answerImage.clipsToBounds = true
    //        self.view.addSubview(answerImage)
            replyView.isHidden = false
            txtfield.isHidden = false
            RecordView.isHidden = true
        }
    }
    
    func onAnimationEnd() {
//        stateLabel.text = "onAnimation End"
        print("onAnimationEnd")
    }
    
    @IBAction func playAudioBtn(_ sender: Any) {
        
        
        if audioRecorder?.isRecording == false{
//               stopButton.isEnabled = true
//               recordButton.isEnabled = false

               var error : NSError?
               do {
                   let player = try AVAudioPlayer(contentsOf: audioRecorder!.url)
                   try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
                    audioPlayer = player
                } catch {
                    print(error)
                }

               audioPlayer?.delegate = self

               if let err = error{
                   print("audioPlayer error: \(err.localizedDescription)")
               }else{
                   audioPlayer?.play()
               }
           }
        
    }
    var audioPlayer : AVAudioPlayer?
    var audioRecorder : AVAudioRecorder?
    
    var audionightPlayer : AVAudioPlayer?
    var audionightRecorder : AVAudioRecorder?
    @IBOutlet weak var dailychallengeCompletedTablevie: UITableView!
    @IBOutlet weak var dailyChallengeTableview: UITableView!
    @IBOutlet weak var chatTableviewHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var completeFitnessTableHeightconstrain: NSLayoutConstraint!
    @IBOutlet weak var choreCompleteHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var choreTablevieHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var RecordView: RecordView!

    @IBOutlet weak var nightViewHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var nightBarChartImageHeight: NSLayoutConstraint!
    @IBOutlet weak var nightBarChartViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var nightReplyImageHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var getReplyNightViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var nightProfileImageConstrain: NSLayoutConstraint!
    @IBOutlet weak var nightAnswerViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var nightViewImageHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var completeGoalWidthConstrain: NSLayoutConstraint!
    @IBOutlet weak var completeGoal: UIImageView!
    @IBOutlet weak var disableGoalWidthConstrain: NSLayoutConstraint!
    @IBOutlet weak var RecordBtn: RecordButton!
    @IBOutlet weak var getReplyLbl: UILabel!
    @IBOutlet weak var answerLbl: UILabel!
    @IBOutlet weak var getreplyView: UIView!
    @IBOutlet weak var answerView: UIView!
    @IBOutlet weak var replyView: UIView!
   @IBOutlet weak var goodMorninView: UIView!
    
//    @IBOutlet weak var goodMorninView: LeftTriangleView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var receiverRplyView: UIView!
    @IBOutlet weak var receiverTimeLbl: UILabel!
    @IBOutlet weak var receiverTxtMessageLbl: UILabel!
    @IBOutlet weak var receiverNameLbl: UILabel!
    @IBOutlet weak var barChartView: BarChartView!
//    @IBOutlet weak var navBar: UIView!
    
    @IBOutlet weak var barChartMainView: UIView!
    @IBOutlet weak var navBar: CustomNavigationBar!
    @IBOutlet weak var chatTableView: UITableView!
    
    @IBOutlet weak var disableGoalImage: UIImageView!
    @IBOutlet weak var textBtn: UIButton!
    @IBOutlet weak var goalView: UIView!
    @IBOutlet weak var choreTableView: UITableView!
    @IBOutlet weak var playBtn: UIButton!
    
    @IBOutlet weak var profileAnswerView: UIImageView!
    @IBOutlet weak var backViewHeighConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var infoviewProfileImageHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var infoViewProfileImage: UIImageView!
    @IBOutlet weak var barchatprofileImageHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var barChatProfileImage: UIImageView!
    @IBOutlet weak var getReplyViewProfileImageHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var getReplyViewProfileImage: UIImageView!
    @IBOutlet weak var GMProfileImageHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var GMProfileImage: UIImageView!
    
    @IBOutlet weak var answerImage: UIImageView!
    @IBOutlet weak var bottomViewBottomConstrain: NSLayoutConstraint!
    @IBOutlet weak var addChallengeImage: UIView!
    var isShowChatTableview:Bool = false
    var isShowCompleteFitnessTableview:Bool = false
    var isShowchoreTableview:Bool = false
    var isShowChoreCompleteTableview:Bool = false
    

    
    var homeModel: HomeModel? {
        return HomeManager.sharedInstance.homeModel
    }

    var chartValuesforGraph = [CGFloat]()
    var chartDateArrayforGraph = [String]()
    var chartValuesforGraphNight = [CGFloat]()
    var chartDateArrayforGraphNight = [String]()
    var chartDateArrayforGraphWithPercentage = [String]()
    var accessToken : String = ""
    let marker = FPL_BalloonMarker(color: UIColor(white: 180/255, alpha: 1),
                                        font: .boldSystemFont(ofSize: 10),
                                        textColor: .white,
                                        insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
    
    
    let cellReuseIdentifier = "challengeCell"
    
        
    var personalChallengeList: [PersonalChallenge] {
        return PersonalFitnessManager.sharedInstance.personalChallengeList
    }
    var localPersonalChallenge : [PersonalChallenge] = []
    var personalChallengeFitness : PersonalChallenge?
    var activeChallenge: PersonalChallenge? {
        return PersonalFitnessManager.sharedInstance.getActiveChallenge()
    }
    var startedChallenge: PersonalChallenge? {
        return PersonalFitnessManager.sharedInstance.getStartedChallenge()
    }
    var levelsModel: LevelsModel? {
          return LevelsManager.sharedInstance.levelsModel
      }
    let healthService = HealthService()
    let refresh = UIRefreshControl()
    var progressCount :Float = 0.0
    var stepCountTimer = Timer()
    var oldStepCountProgress = false
    var reloadPedometerUpdates = true
    var debugView: PedometerDebugView?
    var isCompleteFitness = false
    var challengeActionsViewController: ChallengeActionsViewController?
    var addChallengeViewController: AddChallengeViewController?
    var completedChallengeViewController: CompletedChallengeViewController?
    var deleteChallengeViewController: DeleteChallangeViewController?
    var customAlertViewController: CustomAlertViewController?
    var challengeDetailsViewController: ChallengeDetailsViewController?
    var restartChallengeViewController: RestartChallageViewController?
    var ChatSelectPopupViewController: ChatSelectPopupViewController?
    
    @IBOutlet weak var goalInforLabel: UILabel!
    @IBOutlet weak var replyViewHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var barChartHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var getReplyHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var answerViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var completFitnessTableView: UITableView!
    
    //chores
    var ChoresListModel : ChoresListModel?
    var choresListModelCompleted : ChoresListModel?
    var choreAcceptAndPendingCell = "choreAcceptAndPendingCell"
    let challengeCellReuseIdentifier = "challengeCell"
    let factCardCellReuseIdentifier = "FatCardTableViewCell"
    let chatboatCongrazReuseIdentifier = "ChatboatCongrajTableViewCell"
    
//    let greetingCellIdentifier = "greetingTableViewCell"
//    let greetinReplyCellIdentifier = "greetinReplyTableViewCell"
//    let youroResponseCellIdentifier = "youroResponseTableViewCell"

    
    var wellnessParentVC : WellnessViewController?
    var removeChoreViewController: RemoveChoreViewController?
    var doneChoreViewController: DoneChoreViewController?
    var selectedTag = 0
    
    //daily challenges
    
    @IBOutlet weak var infoviewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var dailyChallengeTableViewHeightConstrain: NSLayoutConstraint!
    var onboardingChallenges: [ChallengeData] {
        return DailyChallengeManager.sharedInstance.onboardingChallenges.filter{ value in
            return value.cName != nil
        }
    }
//    public static var previousVC:ChatPreviousVC = .none
    
    var dailyChallenges: [DailyChallenge] {
        return DailyChallengeManager.sharedInstance.dailyChallenges
    }
    var dailyChallengeChatboat : DailyChallenge?
    
    var activeOnboardingWalkChallenge: ChallengeData? {
        return DailyChallengeManager.sharedInstance.getActiveOnboardingWalkChallenge()
    }
    
    var startedOnboardingWalkChallenge: ChallengeData? {
        DailyChallengeManager.sharedInstance.getStartedOnboardingWalkChallenge()
    }
    
    var activeDailyWalkChallenge: DailyChallenge? {
        return DailyChallengeManager.sharedInstance.getActiveDailyWalkChallenge()
    }
    
    var startedDailyWalkChallenge: DailyChallenge? {
        DailyChallengeManager.sharedInstance.getStartedDailyWalkChallenge()
    }
    
//    var levelsModel: LevelsModel? {
//          return LevelsManager.sharedInstance.levelsModel
//      }
//    
    var challengeAcceptViewController : ChallangeAcceptViewcontroller?
//    var completedChallengeViewController: CompletedChallengeViewController?
//    var debugView: PedometerDebugView?
    var getFootCountUpdates = OSDataPermissions()
    //MARK: - Lifecycle methods
    var sectionArray = [String]()
    
    //Task Completed View
    
    @IBOutlet weak var taskCompletedView: UIView!
    
    @IBOutlet weak var taskCompletedLabel: UILabel!
    @IBOutlet weak var choreCompleteTableview: UITableView!
    
    @IBOutlet var mainContainerview: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var infoView: UIView!
    
    
    var greeting = ""
    
    
    @IBOutlet weak var holeChatTableView: UITableView!
    
    
    @IBOutlet weak var bottonViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var nightView: UIView!
    
    @IBOutlet weak var nightLabel: UILabel!
    
    @IBOutlet weak var nightQuestionLabel: UILabel!
    
    @IBOutlet weak var nightAnswerView: UIView!
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var nightViewImage: UIImageView!
    @IBOutlet weak var nightBarChartProfileImg: UIImageView!
    @IBOutlet weak var nightBarChart: BarChartView!
    @IBOutlet weak var nightBarChartMainView: UIView!
    @IBOutlet weak var nightReplyImg: UIImageView!
    @IBOutlet weak var getReplyNightLabel: UILabel!
    @IBOutlet weak var getReplyNightView: UIView!
    @IBOutlet weak var nightProfileImage: UIImageView!
    @IBOutlet weak var nightPlayBtn: UIButton!
    @IBAction func playnightBtn(_ sender: Any) {
        
        
        if audionightRecorder?.isRecording == false{
//               stopButton.isEnabled = true
//               recordButton.isEnabled = false

               var error : NSError?
               do {
                   let player = try AVAudioPlayer(contentsOf: audionightRecorder!.url)
                    audionightPlayer = player
                } catch {
                    print(error)
                }

            audionightPlayer?.delegate = self

               if let err = error{
                   print("audioPlayer error: \(err.localizedDescription)")
               }else{
                audionightPlayer?.play()
               }
           }
        
    }
    var enableNightView:Bool = false
    @IBOutlet weak var nightAnswerLabel: UILabel!
  
    @IBOutlet weak var nightBarChartMainHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var nightBarChartHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var txtfield: UITextField!
    @IBOutlet weak var nightReplyView: UIView!
    
    @IBOutlet weak var nightReplyRecordView: RecordView!
    @IBOutlet weak var nightReplyMicBtn: RecordButton!
 
    @IBOutlet weak var nightReplyTextField: UITextField!
    @IBOutlet weak var nightReplySendBtn: UIButton!
    
    @IBOutlet weak var noContentLabel: UILabel!
    @IBOutlet weak var noContentView: UIView!
    
    var userAgeDailyChallenge:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialValueInGoalview()
        nightReplyTextField.placeholder = "Minimum 30 Characters Required"
        txtfield.placeholder = "Minimum 30 Characters Required"
        infoLabel.text = "Let's see if you can achieve your target of 5 Goals for Today (Complete challenges and chores to earn goals). 🎉"
        IQKeyboardManager.shared.enable = true
        RecordBtn.recordView = RecordView
        
        nightReplyMicBtn.recordView = nightReplyRecordView
        
        RecordView.delegate = self
        getFootCountUpdates.delegate = self
        
        nightReplyRecordView.delegate = self
        nightReplySendBtn.isEnabled = false
        textBtn.isEnabled = false
        // Do any additional setup after loading the view.
     
      
        // getting URL path for audio
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docDir = dirPath[0]
        let soundFilePath = (docDir as NSString).appendingPathComponent("sound.caf")
        let soundFileURL = NSURL(fileURLWithPath: soundFilePath)
        print(soundFilePath)

        //Setting for recorder
        let recordSettings = [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
            AVEncoderBitRateKey: 16,
            AVNumberOfChannelsKey : 2,
            AVSampleRateKey: 44100.0] as [String : Any] as [String : Any] as [String : Any] as [String : Any]
        var error : NSError?
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord)
            audioRecorder = try AVAudioRecorder(url: soundFileURL as URL, settings: recordSettings as [String : AnyObject])
        } catch _ {
            print("Error")
        }

        if let err = error {
            print("audioSession error: \(err.localizedDescription)")
        }else{
            audioRecorder?.prepareToRecord()
        }
        
        
          // getting URL path for audio
          let dirPathnight = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
          let docDirnight = dirPathnight[0]
          let soundFilePathnight = (docDirnight as NSString).appendingPathComponent("soundnight.caf")
          let soundFileURLnight = NSURL(fileURLWithPath: soundFilePathnight)
          print(soundFileURLnight)

          //Setting for recorder
          let recordSettingsnight = [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
              AVEncoderBitRateKey: 16,
              AVNumberOfChannelsKey : 2,
              AVSampleRateKey: 44100.0] as [String : Any] as [String : Any] as [String : Any] as [String : Any]
          var errornight : NSError?
          let audioSessionnight = AVAudioSession.sharedInstance()
          do {
              try audioSessionnight.setCategory(AVAudioSession.Category.playAndRecord)
            audionightRecorder = try AVAudioRecorder(url: soundFileURLnight as URL, settings: recordSettingsnight as [String : AnyObject])
          } catch _ {
              print("Error")
          }

          if let err = error {
              print("audioSession error: \(err.localizedDescription)")
          }else{
            audionightRecorder?.prepareToRecord()
          }
        
        setupTableView()
        initialSetUp()
        getLevels()
     
        
    
        goodMorninView.layer.cornerRadius = 20
        goodMorninView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        
            nightView.layer.cornerRadius = 20
            nightView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        answerView.layer.cornerRadius = 20
        answerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        nightAnswerView.layer.cornerRadius = 20
        nightAnswerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        getreplyView.layer.cornerRadius = 20
        getreplyView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        getReplyNightView.layer.cornerRadius = 20
        getReplyNightView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        infoView.layer.cornerRadius = 20
        infoView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        navBar.delegate = self
        navBar.title = "Bot"
        navBar.navImage.isHidden = false
        
  
//        goodMorninView.addSubview(leftTailView)
////        leftTailView.anchor(topAnchor, left: nil, bottom: nil, right: goodMorninView.leftAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 9, heightConstant: 6)

        greetingLogic()
        if let userName = homeModel?.data.name{
            let userString = userName.prefix(20)
            receiverNameLbl.text = "\(greeting)\(userString) 🌞"

        }else{
            receiverNameLbl.text = "\(greeting) 🌞"

        }
        
        
        if homeModel?.data.profilePic == "" {
                    self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                } else {
                 
                    if let url = URL(string: homeModel?.data.profilePic ?? "") {

                        if url.absoluteString.range(of: "jpg") != nil {

                            self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                        }

                    }else{
                        answerImage.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
                    }

                    
                }
        answerImage.layer.cornerRadius = answerImage.frame.size.height/2
        answerImage.layer.borderWidth = 0
//        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        answerImage.clipsToBounds = true
        
        enableNightView = true
        
    }
    
    func setInitialTime(){
        lblTimeMorningQuestion.text = ""
        lblTimeMorningAnswer.text = ""
        lblTimeMorningReply.text = ""
        lblTimeMorningBarChart.text = ""
        lblTimeInfo.text = ""
        lblTimeNightQuestion.text = ""
        lblTimeNightAnswer.text = ""
        lblTimeNightReply.text = ""
        lblTimeNightBarChart.text = ""
        txtfield.text = ""
        nightReplyTextField.text = ""
    }
    @IBAction func nightReplyMicBtn(_ sender: Any) {
       
    }
    @IBAction func nightReplySendAct(_ sender: Any) {
        if nightReplyTextField.text?.count == 0{
            return
        }
        if nightReplyTextField.text?.count ?? 0 < 30
        {
            
            self.getReplyNightLabel.text = "Please enter a minimum of 30 characters for an effective assessment ⚠️"
            nightReplyView.isHidden = false
            nightAnswerView.isHidden = false
           
            nightAnswerViewHeightConstrain.constant = 70
//            answerViewHeightConstrain.constant = 50
            nightProfileImage.isHidden = false
            nightProfileImageConstrain.constant = 30
            
//            answerImageHeightConstrain.constant = 30
            nightPlayBtn.isHidden = true
            var text:String = nightReplyTextField.text ?? ""
            nightAnswerLabel.text = text
            nightAnswerLabel.isHidden = false
            getReplyNightLabel.isHidden = false
            self.getReplyNightView.isHidden = false
            self.getReplyNightViewHeightConstrain.constant = 80
//            getReplyHeightConstrain.constant = 50
            self.nightReplyImg.isHidden = false
            self.nightReplyImageHeightConstrain.constant = 30
//            self.getReplyViewProfileImageHeightConstrain.constant = 30
            nightReplyTextField.text = ""
            nightReplyTextField.becomeFirstResponder()
            return
        }
       
        nightReplyView.isHidden = true
//        replyViewHeightConstrain.constant = 0
        nightAnswerView.isHidden = false
        nightAnswerViewHeightConstrain.constant = 70
//        answerViewHeightConstrain.constant = 50
        nightProfileImage.isHidden = false
        nightProfileImageConstrain.constant = 30
//        answerImageHeightConstrain.constant = 30
        nightPlayBtn.isHidden = true
        SVProgressHUD.show()
        var text:String = nightReplyTextField.text ?? ""
        nightAnswerLabel.text = text
        nightAnswerLabel.isHidden = false
        let userDefaults = UserDefaults.standard
        userDefaults.set(text, forKey: "replyKeyNight")
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let nowString = formatter.string(from: now)
        UserDefaults.standard.set(nowString, forKey: "savedDate")
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(self.accessToken)",
        ]
        
        AF.upload(multipartFormData: { (datavalue) in
            datavalue.append(text.data(using: .utf8)!, withName: "text")
            datavalue.append("2020-11-03T19:38:27+0300".data(using: .utf8)!, withName: "recorded_at")
            datavalue.append("en-US".data(using: .utf8)!, withName: "language_code")
            
        }, to: "https://api.dev.cogapps.net/simplified/", headers: headers).response { response in
            SVProgressHUD.dismiss()
            if let error = response.error {
                print(error.localizedDescription)
                return
            }
            
            if let data = response.data {
                do {
                    
                    if let serializationObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]{
                        updateGoals(goalType: .hbotEvening){ (countValue) in
                            self.setGoalViewBasedOnGoalCount(cnt: countValue)
                        }
                        self.showGoalSuccessPopup()
                        self.saveHbotToPlist(dictData: serializationObj,isMorning: false)
                        self.getReplyNightView.isHidden = false
                        self.getReplyNightViewHeightConstrain.constant = 80
                        
//                        self.getReplyHeightConstrain.constant = 80
                        self.nightReplyImg.isHidden = false
                        self.nightReplyImageHeightConstrain.constant = 30
//                        self.getReplyViewProfileImageHeightConstrain.constant = 30
                        self.getReplyNightLabel.isHidden = false
                        if let emotion = serializationObj["emotion"] as? String {
                            if emotion == "negative"{
                                self.getReplyNightLabel.text = "Looks like you are feeling sad today and we hope you feel better. 😔"
                            }else{
                                self.getReplyNightLabel.text = "Great to see that you feel happy today 😀"
                            }
                         
                            
                        }
                        self.chartValuesforGraphNight .removeAll()
                        self.chartDateArrayforGraphNight.removeAll()
                        if let pairs = serializationObj["pairs"] as? [[String:Any]]{
                          
                            for object in pairs {
                                if let emotion = object["emotion"] as? String {
                                    self.chartDateArrayforGraphNight.append(emotion)
                                    for (index, element) in self.chartDateArrayforGraphNight.enumerated() {
                                        self.chartDateArrayforGraphNight[index] = element.capitalized
                                    }
                                }
                                if let value = object["value"] as? CGFloat {
                                    self.chartValuesforGraphNight.append(value)
                                }
                            }
                            print(self.chartValuesforGraphNight)
                            print(self.chartDateArrayforGraphNight)
                            self.nightBarChartMainView.isHidden = false
                            self.nightBarChartMainHeightConstrain.constant = 329
                            self.nightBarChartProfileImg.isHidden = false
                            self.nightBarChartImageHeight.constant = 30
                            self.addSingleLineGrapghGraphNight()
                        }
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                }
//                print(String.init(data: data, encoding: .utf8)!)

            }
            print(response)
            
        }
    }
    
    
    @IBOutlet weak var addChallengeViewConstrain: NSLayoutConstraint!
    @IBOutlet weak var addChallengeView: UIView!
    var bottomHeight: CGFloat {
        guard #available(iOS 11.0, *),
            let window = UIApplication.shared.keyWindow else {
                return 0
        }
        return window.safeAreaInsets.bottom
    }
    @IBAction func addChallengeAction(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
     
        let myStringcompleteChoreChatboat = UserDefaults.standard.string(forKey: "completeChoreChatboat")
        let myStringCompleteFitnessChallengae = UserDefaults.standard.string(forKey: "completeFitnessChatboat")
        
        if (myStringcompleteChoreChatboat == "completedChore" && myStringCompleteFitnessChallengae == "completed") ||  myStringCompleteFitnessChallengae != "completed" {
            isFromAddChallenge = true
            challengeHbotType = 1
        }else{
            isFromAddChallenge = true
            challengeHbotType = 2
        }
        self.tabBarController?.selectedIndex = 1
    }
    func back(sender: UIBarButtonItem) {
           // Perform your custom actions
           // ...
           // Go back to the previous ViewController
        self.navigationController?.popViewController(animated: true)
       }
   
    func greetingLogic() {
         let date = NSDate()
         let calendar = NSCalendar.current
         let currentHour = calendar.component(.hour, from: date as Date)
         let hourInt = Int(currentHour.description)!

         if hourInt >= 12 && hourInt <= 16 {
             greeting = "Hello "
         }
         else if hourInt >= 5 && hourInt <= 12 {
             greeting = "Hello "
         }
         else if hourInt >= 16 && hourInt <= 20 {
             greeting = "Hello "
         }
         else if hourInt >= 20 && hourInt <= 24 {
             greeting = "Hello "
         }
         else if hourInt >= 0 && hourInt <= 5 {
             greeting = "Hello "
         }
        
  

     }
    private func initialSetUp()  {
        
        sectionArray = ["Daily Challenge","My Challenges"]

        dailyChallengeTableview.register(UINib.init(nibName: "ChallengeTableViewCell", bundle: nil), forCellReuseIdentifier: challengeCellReuseIdentifier)
        dailyChallengeTableview.register(UINib.init(nibName: "FatCardTableViewCell", bundle: nil), forCellReuseIdentifier: factCardCellReuseIdentifier)
        chatTableView.register(UINib.init(nibName: "FatCardTableViewCell", bundle: nil), forCellReuseIdentifier: factCardCellReuseIdentifier)
        choreTableView.register(UINib.init(nibName: "FatCardTableViewCell", bundle: nil), forCellReuseIdentifier: factCardCellReuseIdentifier)
        
        dailyChallengeTableview.register(UINib.init(nibName: "ChatboatCongrajTableViewCell", bundle: nil), forCellReuseIdentifier: chatboatCongrazReuseIdentifier)
        completFitnessTableView.register(UINib.init(nibName: "ChatboatCongrajTableViewCell", bundle: nil), forCellReuseIdentifier: chatboatCongrazReuseIdentifier)
        choreCompleteTableview.register(UINib.init(nibName: "ChatboatCongrajTableViewCell", bundle: nil), forCellReuseIdentifier: chatboatCongrazReuseIdentifier)
        
        
    }
    
    func setMoringinitalTime(){
        if let morningInitalTime = HarmonySingleton.dictHbotData?["morningInitalTime"] as? String{
            self.lblTimeMorningQuestion.text = morningInitalTime
        }else{
            let currentTime = Date().getDateStrFrom(format: DateFormatType.hBotTime)
            self.lblTimeMorningQuestion.text = currentTime
            if HarmonySingleton.dictHbotData == nil
            {
                HarmonySingleton.dictHbotData = [String:Any]()
            }
            HarmonySingleton.dictHbotData?["morningInitalTime"] = currentTime
            saveHbotData()
    }
    }
    func getLevels() {
           LevelsManager.sharedInstance.getLevels { (result) in
           }
    }
    
    func getAllChallenges() {
        
        self.getOnboardingChallenges()
        //Get Started Onboarding Challenges
        
        /*
        DailyChallengeManager.sharedInstance.getDashboardData { (result) in
            switch result {
             //Get not started onboarding challenges
            case .success(_):
                self.dailyChallengeTableview.reloadData()
//                self.setupPedometerUpdates()
                if DailyChallengeManager.sharedInstance.showMisteryBoxScreen {
                    self.showOnboardCompleteVC()
                }
                
//                self.getDailyChallenges()
//                if self.onboardingChallenges.count == 0{
                    self.getOnboardingChallenges()
//                }
                
            case .failure(let error):
                print(error.localizedDescription)
//                if self.onboardingChallenges.count == 0{
//                    self.getOnboardingChallenges()
//                }
            }
        }
         */
    }
    func presentCustomAlertViewController() {
        customAlertViewController = GlobalStoryBoard().customAlertVC
        customAlertViewController?.alertTitle = "Invite Sent!"
        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
            customAlertViewController?.alertMessage = "Yay! You can take the challenges upon\nyour parent's approval for the invite!"

        } else {
            customAlertViewController?.alertMessage = "Yay! You can take the challenges upon\nyour Kid's approval for the invite!"
        }
        customAlertViewController?.delegate = self
        tabBarController?.present(customAlertViewController!, animated: true, completion: nil)
    }
    func showOnboardCompleteVC()  {
        let vc = GlobalStoryBoard().onBoardCompleteVC
        vc.modalPresentationStyle = .overFullScreen
//        vc.parentVC = self
//        self.present(vc, animated: true, completion: nil)
    }
    
    private func getOnboardingChallenges() {
        DailyChallengeManager.sharedInstance.getOnboardingChallenges { (result) in
            switch result {
            
           

            case .success(_):
//                for onboardingChallengeObj in self.onboardingChallenges {
//                    if onboardingChallengeObj.cName != nil {
//
//                        self.onboardingChallesCnameCheck.append(onboardingChallengeObj)
//                    }
//
//                }
//                DailyChallengeManager.sharedInstance.onboardingChallenges.removeAll()
//                DailyChallengeManager.sharedInstance.onboardingChallenges = self.onboardingChallesCnameCheck
                self.dailyChallengeTableview.reloadData()
                self.getDailyChallenges()
            case .failure(let error):
                self.getDailyChallenges()
            }
        }
    }
    
    private func getDailyChallenges() {
            
                guard DailyChallengeManager.sharedInstance.onboardingChallengesFinished else {
                    getpersonalchallenges()
                    return
                    
                }
        if HarmonySingleton.shared.dashBoardData.data?.challenges?.count == 0
        {
            
        
                DailyChallengeManager.sharedInstance.getDailyChallenges { (result) in
                    switch result {
                        
                    case .success(_):
                        
                        if self.dailyChallenges.count > 0
                        {
                            self.dailyChallengeTableview.isHidden = false
                            self.dailyChallengeTableViewHeightConstrain.constant = 350
                            self.dailyChallengeTableview.reloadData()
                            self.chatTableView.isHidden = true
                            self.chatTableviewHeightConstrain.constant = 0
                            self.completFitnessTableView.isHidden = true
                            self.completeFitnessTableHeightconstrain.constant = 0
                            self.choreTableView.isHidden = true
                            self.choreTablevieHeightConstrain.constant = 0
                            self.choreCompleteTableview.isHidden = true
                            self.choreTablevieHeightConstrain.constant = 0
                            let DailyChallengeChatboatuserDefaults = UserDefaults.standard
                            let completeDailyChallengeChatboat = DailyChallengeChatboatuserDefaults.string(forKey: "completeDailyChallengeChatboat")
                            if completeDailyChallengeChatboat == "dailyChallengCompleted"{
                                self.dailyChallengeTableview.isHidden = false
                                self.dailyChallengeTableViewHeightConstrain.constant = 300
                                self.dailyChallengeTableview.reloadData()
                                self.getpersonalchallenges()
                             
                            }else{
                                self.updateNewFuction()
                                self.startTimer()
                            }
                            
                        }else{
                            let DailyChallengeChatboatuserDefaults = UserDefaults.standard
                            let completeDailyChallengeChatboat = DailyChallengeChatboatuserDefaults.string(forKey: "completeDailyChallengeChatboat")
                            if completeDailyChallengeChatboat == "dailyChallengCompleted"{
                                self.dailyChallengeTableview.isHidden = false
                                self.dailyChallengeTableViewHeightConstrain.constant = 300
                                self.dailyChallengeTableview.reloadData()
                            }else{
                                self.dailyChallengeTableview.isHidden = true
                                self.dailyChallengeTableViewHeightConstrain.constant = 0
                                self.dailyChallengeTableview.reloadData()
                            }
                           
                            self.getpersonalchallenges()
                        }
                        
        //                self.dailyChallengeTableview.reloadData()
        //                self.setupPedometerUpdates()
                    case .failure(let error):
                        print(error.debugDescription)
                        break
                    }
                }
        }else{
            getpersonalchallenges()
        }
        
    }
    
    
    @objc func getpersonalchallenges()  {
  
         
                getPersonalChallenges(isfromRefresh: true)
         
//        let userDefaultsYuruTypeCheck = UserDefaults.standard
//            let userAge = userDefaultsYuruTypeCheck.integer(forKey: "yuruTypeIdPersonalNightMessage")
//        if userAge == 5 {
//            nightView.isHidden = false
//            nightViewImage.isHidden = false
//            nightViewHeightConstrain.constant = 60
//            nightViewImageHeightConstrain.constant = 30
////            nightViewHeightConstrain.constant = 30
//            replyView.isHidden = true
//            nightReplyView.isHidden = false
//            let userDefaultsreplyKeyNight = UserDefaults.standard
//            let myStringreplyKeyNight = userDefaultsreplyKeyNight.string(forKey: "replyKeyNight")
//            if myStringreplyKeyNight?.count ?? 0 > 0
//            {
//                nightReplyView.isHidden = true
//            }
//
//            showNight()
//            showProfilePic()
//
//        }else{
//            nightView.isHidden = true
//            nightViewHeightConstrain.constant = 0
//            nightViewImage.isHidden = true
//            nightViewHeightConstrain.constant = 0
//            nightAnswerView.isHidden = true
//            nightProfileImage.isHidden = true
//        }
      
//        setupPedometerUpdates()
    }
    
    
    private func getPersonalChallenges(isfromRefresh:Bool) {
//        SVProgressHUD.show()
   
            
            PersonalFitnessManager.sharedInstance.getPersonalChallengesHbot { (result) in
                switch result {
                case .success(_):
                    self.refresh.endRefreshing()
                    SVProgressHUD.dismiss()
                    
                    self.chatTableView.reloadData()
                    if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                        print(startedChallenge)
                        self.progressCount = startedChallenge.data.challengeRunStep ?? 0
                        let challengeId = startedChallenge.data.challengeId
                        let curentStep = startedChallenge.data.challengeRunStep
                        
                        UserDefaults.standard.set(curentStep, forKey: "current_stepCount")
                        
                        let guid =  UserDefaultConstants().guid ?? ""
                        UserDefaults.standard.set(guid, forKey: "current_userGuid")
                        UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                        
                        self.getCurrentTimeAndDate()
                        self.timerMethod()
                    }
                    
                  if self.personalChallengeList.count > 0
                    {
                    
    //                self.dailyChallengeTableview.isHidden = true
    //                self.dailyChallengeTableViewHeightConstrain.constant = 0
    //                let DailyChallengeChatboatuserDefaults = UserDefaults.standard
    //                let completeDailyChallengeChatboat = DailyChallengeChatboatuserDefaults.string(forKey: "completeDailyChallengeChatboat")
    //                if completeDailyChallengeChatboat == "dailyChallengCompleted"{
    //                    self.dailyChallengeTableview.isHidden = false
    //                    self.dailyChallengeTableViewHeightConstrain.constant = 300
    //                }
                    self.chatTableView.isHidden = false
                    self.chatTableviewHeightConstrain.constant = 300
                    if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue == RegistrationType.child.rawValue {
                        let personalChallengeKid = self.personalChallengeList.first
                        if personalChallengeKid?.data.harmoneyBucks ?? 0 > 0 {
                            if personalChallengeKid?.data.challengeAcceptType?.rawValue == ChallengeAcceptType.padding.rawValue || personalChallengeKid?.data.challengeAcceptType?.rawValue == ChallengeAcceptType.decline.rawValue {
                                self.chatTableView.isHidden = true
                                self.chatTableviewHeightConstrain.constant = 0
                            }else{
                                self.chatTableView.isHidden = false
                                self.chatTableviewHeightConstrain.constant = 300
                            }
                        }else{
                            self.chatTableView.isHidden = false
                            self.chatTableviewHeightConstrain.constant = 300
                        }
                       
                    }
                    self.completFitnessTableView.isHidden = true
                    self.completeFitnessTableHeightconstrain.constant = 0
                    self.addChallengeView.isHidden = true
                    self.addChallengeViewConstrain.constant = 0
                    let userDefaults = UserDefaults.standard
                    let myString = userDefaults.string(forKey: "completeFitnessChatboat")
                    if myString == "completed"{
                        self.chatTableView.isHidden = true
                        self.chatTableviewHeightConstrain.constant = 0
                        self.completFitnessTableView.isHidden = false
                        self.completeFitnessTableHeightconstrain.constant = 300
                        self.completFitnessTableView.reloadData()
                        self.getAllSubCategory()
                    }
                    let completeChoreChatboat = UserDefaults.standard
                    let myStringcompleteChoreChatboat = completeChoreChatboat.string(forKey: "completeChoreChatboat")
                    if myStringcompleteChoreChatboat == "completedChore"{
                        self.getAllSubCategory()
                    }
                 
                    
                }else{
                    
    //                let DailyChallengeChatboatuserDefaults = UserDefaults.standard
    //                let completeDailyChallengeChatboat = DailyChallengeChatboatuserDefaults.string(forKey: "completeDailyChallengeChatboat")
    //                if completeDailyChallengeChatboat == "dailyChallengCompleted"{
    //                    self.dailyChallengeTableview.isHidden = false
    //                    self.dailyChallengeTableViewHeightConstrain.constant = 300
    //                }else{
    //                    self.dailyChallengeTableview.isHidden = true
    //                    self.dailyChallengeTableViewHeightConstrain.constant = 0
    //                }
                    self.chatTableView.isHidden = true
                    self.chatTableviewHeightConstrain.constant = 0
                    self.addChallengeView.isHidden = false
                    self.addChallengeViewConstrain.constant = 80
                    let userDefaults = UserDefaults.standard
                    let myString = userDefaults.string(forKey: "completeFitnessChatboat")
                    if myString == "completed"{
                        self.chatTableView.isHidden = true
                        self.chatTableviewHeightConstrain.constant = 0
                        self.completFitnessTableView.isHidden = false
                        self.completeFitnessTableHeightconstrain.constant = 300
                        self.completFitnessTableView.reloadData()
                        self.addChallengeView.isHidden = false
                        self.addChallengeViewConstrain.constant = 80
                    }
                    self.getAllSubCategory()
                }
                    
    //                self.checkActivityType()
                case .failure(let error):
                    self.refresh.endRefreshing()
                    SVProgressHUD.dismiss()
                }
            }
        
    }
    
    @objc func getAllSubCategory()  {
        //        SVProgressHUD.show()
   
            
                if Reachability.isConnectedToNetwork() {
                    var data = [String : Any]()
                    data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                    data.updateValue(DateUtils.format(date: Date(), in: .yearMonthDay) ?? "", forKey: "date")
                    
                    let (url, method, param) = APIHandler().getChorslistHBot(params: data)
                    
                    AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                        SVProgressHUD.dismiss()
                        switch response.result {
                        case .success(let value):
                            
                            if let value = value as? [String:Any] {
                                let personalChallenge = Mapper<ChoresListModel>().map(JSON: value)
                                self.ChoresListModel = personalChallenge
                                //                                    DispatchQueue.main.async {
                                self.choreTableView.reloadData()
                                if self.ChoresListModel?.data?.count ?? 0 > 0{
                                    self.addChallengeView.isHidden = true
                                    self.addChallengeViewConstrain.constant = 0
                                    self.choreTableView.isHidden = false
                                    self.choreTablevieHeightConstrain.constant = 140
                                    let indexData = self.ChoresListModel?.data?.first
                                    if HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child {
                                        let pendingApproval = indexData?.isApproved ?? false
                                        if  pendingApproval == false {
                                            self.choreTableView.isHidden = true
                                            self.choreTablevieHeightConstrain.constant = 0
                                        }else{
                                            self.choreTableView.isHidden = false
                                            self.choreTablevieHeightConstrain.constant = 140
                                        }
                                    }else{
                                        self.choreTableView.isHidden = false
                                        self.choreTablevieHeightConstrain.constant = 140
                                    }
                                    self.choreCompleteTableview.isHidden = true
                                    self.choreCompleteHeightConstrain.constant = 0
                                    
                                    let completeChoreChatboat = UserDefaults.standard
                                    let myStringcompleteChoreChatboat = completeChoreChatboat.string(forKey: "completeChoreChatboat")
                                    if myStringcompleteChoreChatboat == "completedChore"{
                                        let userDefaults = UserDefaults.standard
                                        let myString = userDefaults.string(forKey: "completeFitnessChatboat")
                                        if myString == "completed"{
                                            self.addChallengeView.isHidden = false
                                            self.addChallengeViewConstrain.constant = 80
                                        }
                                        self.choreCompleteTableview.isHidden = false
                                        self.choreCompleteHeightConstrain.constant = 270
                                        self.choreTableView.isHidden = true
                                        self.choreTablevieHeightConstrain.constant = 0
                                        self.choreCompleteTableview.reloadData()
                                    }
                                    
                                    
                                }else{
                                    
                                    
                                   
                                    let completeChoreChatboat = UserDefaults.standard
                                    let myStringcompleteChoreChatboat = completeChoreChatboat.string(forKey: "completeChoreChatboat")
                                    if myStringcompleteChoreChatboat == "completedChore"{
                                        self.choreCompleteTableview.isHidden = false
                                        self.choreCompleteHeightConstrain.constant = 270
                                        self.choreTableView.isHidden = true
                                        self.choreTablevieHeightConstrain.constant = 0
                                        self.choreCompleteTableview.reloadData()
                                        
                                        let userDefaults = UserDefaults.standard
                                        let myString = userDefaults.string(forKey: "completeFitnessChatboat")
                                        if myString == "completed"{
                                            self.addChallengeView.isHidden = false
                                            self.addChallengeViewConstrain.constant = 80
                                        }
                                        
                                    }else{
                                        self.addChallengeView.isHidden = false
                                        self.addChallengeViewConstrain.constant = 80
                                        self.choreTableView.isHidden = true
                                        self.choreTablevieHeightConstrain.constant = 0
                                    }
                                    
                                    if self.wellnessParentVC?.chorePendingAndApprovalContainerView.isHidden == false
                                    {
                                        self.wellnessParentVC?.handleViewControllersUI(nwCHoreVw: false, nwChoreVC: true, chorePendingVC: true, createChoreVC: true, choreProgressVC: true)
                                    }
                                    
                                }
                                //                                    }
                                self.refresh.endRefreshing()
                            }
                            break
                        case .failure(let error):
                            self.refresh.endRefreshing()
                            break
                        }
                    }
                }else{
                    self.refresh.endRefreshing()
                }
           
        
        let userDefaultsYuruTypeCheck = UserDefaults.standard
            let userAge = userDefaultsYuruTypeCheck.integer(forKey: "yuruTypeIdPersonalNightMessage")
        if userAge == 5 {
            nightView.isHidden = false
            nightViewImage.isHidden = false
            nightViewHeightConstrain.constant = 60
            nightViewImageHeightConstrain.constant = 30
//            nightViewHeightConstrain.constant = 30
            replyView.isHidden = true
            nightReplyView.isHidden = false
            let userDefaultsreplyKeyNight = UserDefaults.standard
            let myStringreplyKeyNight = userDefaultsreplyKeyNight.string(forKey: "replyKeyNight")
            let userDefaultsadioRecordNight = UserDefaults.standard
            let myStringreplyadioRecordNight = userDefaultsadioRecordNight.string(forKey: "adioRecordNight")
            if myStringreplyKeyNight?.count ?? 0 > 0
            {
                nightReplyView.isHidden = true
            }
            if myStringreplyadioRecordNight?.count ?? 0 > 0
            {
                nightReplyView.isHidden = true
            }
            showNight()
            showProfilePic()
            
        }else{
            nightView.isHidden = true
            nightViewHeightConstrain.constant = 0
            nightViewImage.isHidden = true
            nightViewHeightConstrain.constant = 0
            nightAnswerView.isHidden = true
            nightProfileImage.isHidden = true
        }
    }

    func setupNavBar(){
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = navBar.bounds
        navBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        stopTimer()
    }
    
    func showMorningMessageData(serializationObj : [String : Any]){
        if let timeMorning = HarmonySingleton.dictHbotData?["morningTime"] as? String{
            lblTimeInfo.text = timeMorning
            lblTimeMorningReply.text = timeMorning
            lblTimeMorningAnswer.text = timeMorning
            lblTimeMorningBarChart.text = timeMorning
        }else if let timeNight = HarmonySingleton.dictHbotData?["nightTime"] as? String{
            lblTimeNightQuestion.text = timeNight
            lblTimeNightReply.text = timeNight
            lblTimeNightAnswer.text = timeNight
            lblTimeNightBarChart.text = timeNight            
        }
        self.getreplyView.isHidden = false
        self.getReplyHeightConstrain.constant = 80
        self.getReplyLbl.isHidden = false
        self.getReplyViewProfileImage.isHidden = false
        self.getReplyViewProfileImageHeightConstrain.constant = 30
      
        if let pairs = serializationObj["pairs"] as? [[String:Any]]{
          
            for object in pairs {
                if let emotion = object["emotion"] as? String {
                    self.chartDateArrayforGraph.append(emotion)
                    for (index, element) in self.chartDateArrayforGraph.enumerated() {
                        self.chartDateArrayforGraph[index] = element.capitalized
                    }
                }
                if let value = object["value"] as? CGFloat {
                    self.chartValuesforGraph.append(value)
                }
            }
            print(self.chartValuesforGraph)
            print(self.chartDateArrayforGraph)
            self.barChartMainView.isHidden = false
            self.barChartHeightConstrain.constant = 329
            self.barChatProfileImage.isHidden = false
            self.barchatprofileImageHeightConstrain.constant = 30
            self.addSingleLineGrapghGraph()
        }
        if let emotion = serializationObj["emotion"] as? String {

            if emotion == "negative"{
                self.getReplyLbl.text = "Looks like you are feeling sad today and we hope you feel better. 😔"
            }else{
                self.getReplyLbl.text = "Great to see that you feel happy today 😀"
            }
        }
        self.scrollView.scrollsToBottom(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        setInitialValueInGoalview()
        super.viewWillAppear(animated)
        setInitialTime()
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        greetingLogic()
        if let userName = homeModel?.data.name{
            let userString = userName.prefix(20)
            receiverNameLbl.text = "\(greeting)\(userString) 🌞"

        }else{
            receiverNameLbl.text = "\(greeting) 🌞"

        }
        IQKeyboardManager.shared.enable = true
        enableNightView = true
        self.tabBarController?.tabBar.isHidden = true

        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

        txtfield.isEnabled = false
        self.chartValuesforGraph .removeAll()
        self.chartDateArrayforGraph.removeAll()
//        setupNavBar()
        getTokenValue()
        let userDefaultsAdioRecord = UserDefaults.standard
        let myStringAdioRecord = userDefaultsAdioRecord.string(forKey: "adioRecord")
        let userDefaultsyuruNotificationContent = UserDefaults.standard
            let yuruNotificationContent = userDefaultsyuruNotificationContent.string(forKey: "yuruNotificationContent")
        
        let userDefaultsYuruTypeCheck = UserDefaults.standard
        userAgeDailyChallenge = userDefaultsYuruTypeCheck.integer(forKey: "yuruTypeIdDailyChallenge")
        
        if yuruNotificationContent?.count == nil || yuruNotificationContent?.count == 0{
            if let yuruInitalMessage = HarmonySingleton.dictHbotData?["yuruInitialMessage"] as? String{
                self.receiverTxtMessageLbl.text = yuruInitalMessage
                self.userAgeDailyChallenge = 1
            }else{
                self.getYuruMessageForLoad()
                self.userAgeDailyChallenge = 1
            }
        }else{
            self.receiverTxtMessageLbl.text = yuruNotificationContent

        }
        if userAgeDailyChallenge == 1{

//            getYuruStartMessage
          
            goodMorninView.isHidden = false
            GMProfileImage.isHidden = false
            replyView.isHidden = false
            bottomView.isHidden = false
//            noContentView.isHidden = true

        }else{
//            noContentView.isHidden = false
            goodMorninView.isHidden = true
            GMProfileImage.isHidden = true
            replyView.isHidden = true
            bottomView.isHidden = true
        }
        let userDefaults = UserDefaults.standard
        let myString = userDefaults.string(forKey: "replyKey")
        if myString == nil && myStringAdioRecord == nil
        {
                replyView.isHidden = false
    //            replyViewHeightConstrain.constant = 44
    //            backViewHeighConstrain.constant = 527
//            scrollView.isScrollEnabled = false
                answerView.isHidden = true
                answerViewHeightConstrain.constant = 0
                answerImage.isHidden = true
//                answerImageHeightConstrain.constant = 30
                getreplyView.isHidden = true
                getReplyHeightConstrain.constant = 0
                self.getReplyViewProfileImage.isHidden = true
                self.getReplyViewProfileImageHeightConstrain.constant = 0
                barChartMainView.isHidden = true
                barChartHeightConstrain.constant = 0
                barChatProfileImage.isHidden = true
                barchatprofileImageHeightConstrain.constant = 0
                infoView.isHidden = true
                infoviewHeightConstrain.constant = 0
                infoViewProfileImage.isHidden = true
                infoviewProfileImageHeightConstrain.constant = 0
                chatTableView.isHidden = true
                completFitnessTableView.isHidden = true
                choreTableView.isHidden = true
                choreCompleteTableview.isHidden = true
                dailyChallengeTableview.isHidden = true
                goalView.isHidden = true
            vwGoalAchivement.isHidden = true
            
            setMoringinitalTime()
            
        }else{
            
            let now = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyyMMdd"
            let nowString = formatter.string(from: now)

            if let lastTime = UserDefaults.standard.string(forKey: "savedDate_hbotData"), lastTime == nowString {
                // Already notified today, skip
                print("same date - get action")
               
            }else{
                print(" not same date - no action")
                replyView.isHidden = false
//                replyViewHeightConstrain.constant = 44
//                backViewHeighConstrain.constant = 527
                answerView.isHidden = true
                answerViewHeightConstrain.constant = 0
                answerImage.isHidden = true
//                answerImageHeightConstrain.constant = 30
                getreplyView.isHidden = true
                getReplyHeightConstrain.constant = 0
                self.getReplyViewProfileImage.isHidden = true
                self.getReplyViewProfileImageHeightConstrain.constant = 0
                barChartMainView.isHidden = true
                barChartHeightConstrain.constant = 0
                barChatProfileImage.isHidden = true
                barchatprofileImageHeightConstrain.constant = 0
                infoView.isHidden = true
                infoviewHeightConstrain.constant = 0
                infoViewProfileImage.isHidden = true
                infoviewProfileImageHeightConstrain.constant = 0
                chatTableView.isHidden = true
                completFitnessTableView.isHidden = true
                choreTableView.isHidden = true
                choreCompleteTableview.isHidden = true
                dailyChallengeTableview.isHidden = true
                UserDefaults.standard.removeObject(forKey: "completeChoreChatboat")
                UserDefaults.standard.removeObject(forKey: "completeDailyChallengeChatboat")
                UserDefaults.standard.removeObject(forKey: "completeFitnessChatboat")
                UserDefaults.standard.removeObject(forKey: "yuruTypeIdDailyChallenge")
                UserDefaults.standard.removeObject(forKey: "yuruTypeIdPersonalNightMessage")
                UserDefaults.standard.removeObject(forKey: "yuruTypeIdPersonalFitness")
                UserDefaults.standard.removeObject(forKey: "yuruTypeIdPersonalChore")
                HarmonySingleton.dictHbotData?.removeAll()
                saveHbotData()
                goalView.isHidden = true
                vwGoalAchivement.isHidden = true
                setMoringinitalTime()
                return
            }

            
//        let userDefaultsYuruTypeCheck = UserDefaults.standard
//             userAgeDailyChallenge = userDefaultsYuruTypeCheck.integer(forKey: "yuruTypeIdDailyChallenge")
            if userAgeDailyChallenge == 1{
            
            
            if myString?.count ?? 0 > 0
            {
                
                replyView.isHidden = true
//            replyViewHeightConstrain.constant = 0
            answerViewHeightConstrain.constant = 50
                answerView.isHidden = false
            answerImage.isHidden = false
//            answerImageHeightConstrain.constant = 30
                
                if homeModel?.data.profilePic == "" {
                            self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                        } else {
 
                        self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                            
                        }
                
                answerImage.layer.cornerRadius = answerImage.frame.size.height/2
                answerImage.layer.borderWidth = 0
        //        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
                answerImage.clipsToBounds = true
        //        self.view.addSubview(answerImage)
                playBtn.isHidden = true
                var text:String = myString ?? ""
                answerLbl.isHidden = false
                answerLbl.text = myString
            let userDefaultsaccessTokenyouro = UserDefaults.standard
                if   let accessTokenyouro = userDefaultsaccessTokenyouro.string(forKey: "accessTokenyouro"){
                    if let dictMorningData = HarmonySingleton.dictHbotData?["morningMessageData"] as? [String: Any]{
                        self.showMorningMessageData(serializationObj: dictMorningData)
                    }else{
                        self.doInitialAPICall(accessTokenyouro: accessTokenyouro, text: text)
                    }
                   
                }
            }else{
                
                replyView.isHidden = true
//            replyViewHeightConstrain.constant = 0
            answerViewHeightConstrain.constant = 50
                answerView.isHidden = false
                playBtn.isHidden = false
                answerImage.isHidden = false
//                answerImageHeightConstrain.constant = 30
                
                if homeModel?.data.profilePic == "" {
                            self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                        } else {
                         
                            if let url = URL(string: homeModel?.data.profilePic ?? "") {

                                if url.absoluteString.range(of: "jpg") != nil {

                                    self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                                }

                            }else{
                                answerImage.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
                            }

                            
                        }
                
                answerImage.layer.cornerRadius = answerImage.frame.size.height/2
                answerImage.layer.borderWidth = 0
        //        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
                answerImage.clipsToBounds = true
        //        self.view.addSubview(answerImage)
                answerLbl.isHidden = true
                
                var text:String = myString ?? ""
                answerLbl.text = myString
            let userDefaultsaccessTokenyouro = UserDefaults.standard
            if   let accessTokenyouro = userDefaultsaccessTokenyouro.string(forKey: "accessTokenyouro"){
                
                if let dictMorningData = HarmonySingleton.dictHbotData?["morningMessageData"] as? [String: Any]{
                    self.showMorningMessageData(serializationObj: dictMorningData)
                }else{
                self.doInitialAPICall(accessTokenyouro: accessTokenyouro, text: "Yes happy day looking great and awesome day thanks for the day")
                }
            }
            }
        }
        }
    }
    func doInitialAPICall(accessTokenyouro: String,text: String){
        SVProgressHUD.show()
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(accessTokenyouro)",
        ]
        AF.upload(multipartFormData: { (datavalue) in
            datavalue.append(text.data(using: .utf8)!, withName: "text")
            datavalue.append("2020-11-03T19:38:27+0300".data(using: .utf8)!, withName: "recorded_at")
            datavalue.append("en-US".data(using: .utf8)!, withName: "language_code")
            
        }, to: "https://api.dev.cogapps.net/simplified/", headers: headers).response { response in
            SVProgressHUD.dismiss()
            if let error = response.error {
                print(error.localizedDescription)
                return
            }
            if let data = response.data {
                do {
                  
                    if let serializationObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]{
                        self.saveHbotToPlist(dictData: serializationObj)
                        self.showMorningMessageData(serializationObj: serializationObj)
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            print(response)
        }
}
    
    func saveHbotToPlist(dictData: [String: Any], isMorning: Bool = true){
        let dictResult = dictData.filter { !($0.value is NSNull) }
        if HarmonySingleton.dictHbotData == nil{
            HarmonySingleton.dictHbotData = [String: Any]()
        }
        
        var saveDKey = "morningMessageData"
        var timeKey = ""
        if isMorning{
            saveDKey = "morningMessageData"
            timeKey = "morningTime"
        }else{
            saveDKey = "nightMessageData"
            timeKey = "nightTime"
        }
        
        HarmonySingleton.dictHbotData?[saveDKey] = dictResult
        HarmonySingleton.dictHbotData?[timeKey] = Date().getDateStrFrom(format: DateFormatType.hBotTime)
        saveHbotData()
    }
    
    func showNight(){
        
        greetingLogic()
        if let userName = homeModel?.data.name{
            let userString = userName.prefix(20)
            nightLabel.text = "\(greeting)\(userString) 🌞"

        }else{
            nightLabel.text = "\(greeting) 🌞"

        }
    }
    
    func showProfilePic() {
     
        
if homeModel?.data.profilePic == "" {
         self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: nightProfileImage)
     } else {
      
         if let url = URL(string: homeModel?.data.profilePic ?? "") {

             if url.absoluteString.range(of: "jpg") != nil {

                 self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: nightProfileImage)
             }

         }else{
             nightProfileImage.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
         }

         
     }
        
        nightProfileImage.layer.cornerRadius = nightProfileImage.frame.size.height/2
        nightProfileImage.layer.borderWidth = 0
//        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        nightProfileImage.clipsToBounds = true
//        self.view.addSubview(answerImage)
        
    }
   
    
    @objc func adjustForKeyboard(notification: Notification) {
        self.scrollView.isScrollEnabled = true
        var info : NSDictionary = notification.userInfo! as NSDictionary
        var keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)

          self.scrollView.contentInset = contentInsets
          self.scrollView.scrollIndicatorInsets = contentInsets

          var aRect : CGRect = self.view.frame
          aRect.size.height -= keyboardSize!.height
          if let activeFieldPresent = txtfield
          {
            if (!aRect.contains(txtfield!.frame.origin))
              {
                  self.scrollView.scrollRectToVisible(txtfield!.frame, animated: true)
              }
          }

    }
    
    func setProfilePic(frstName: String?, lstName: String?, profileImageView: UIImageView){
     
         if let firstName = frstName, let lastName = lstName {
             profileImageView.setImageWith(firstName + " " + lastName, color: ConstantString.menuBackSystemProfilepic)
         } else {
             if let dispName = frstName {
                let formattedString = dispName.replacingOccurrences(of: " ", with: "")
                 profileImageView.setImageWith(formattedString, color: ConstantString.menuBackSystemProfilepic)
             }
         }
     }

    
    @IBAction func backBtnAct(_ sender: Any) {
     
        
    }
   
    @IBAction func txtBtnAction(_ sender: Any) {
   
        if txtfield.text?.count == 0{
            return
        }
        if txtfield.text?.count ?? 0 < 30
        {
            self.getReplyLbl.text = "Please enter a minimum of 30 characters for an effective assessment ⚠️"
            replyView.isHidden = false
//            replyViewHeightConstrain.constant = 44
          //  backViewHeighConstrain.constant = 600
            answerView.isHidden = false
            answerViewHeightConstrain.constant = 50
            answerImage.isHidden = false
//            answerImageHeightConstrain.constant = 30
            
            
 if homeModel?.data.profilePic == "" {
             self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
         } else {
          
             if let url = URL(string: homeModel?.data.profilePic ?? "") {

                 if url.absoluteString.range(of: "jpg") != nil {

                     self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
                 }

             }else{
                answerImage.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
             }

             
         }
            
            answerImage.layer.cornerRadius = answerImage.frame.size.height/2
            answerImage.layer.borderWidth = 0
    //        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
            answerImage.clipsToBounds = true
    //        self.view.addSubview(answerImage)
            playBtn.isHidden = true
            var text:String = txtfield.text ?? ""
            answerLbl.text = text
            getReplyLbl.isHidden = false
            self.getreplyView.isHidden = false
            getReplyHeightConstrain.constant = 80
            self.getReplyViewProfileImage.isHidden = false
            self.getReplyViewProfileImageHeightConstrain.constant = 30
            txtfield.text = ""
//            txtfield.becomeFirstResponder()
            return
        }
        replyView.isHidden = true
//        replyViewHeightConstrain.constant = 0
        answerView.isHidden = false
        answerViewHeightConstrain.constant = 50
        answerImage.isHidden = false
//        answerImageHeightConstrain.constant = 30
        
        
if homeModel?.data.profilePic == "" {
         self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
     } else {
      
         if let url = URL(string: homeModel?.data.profilePic ?? "") {

             if url.absoluteString.range(of: "jpg") != nil {

                 self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: answerImage)
             }

         }else{
            answerImage.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
         }

         
     }

        
        answerImage.layer.cornerRadius = answerImage.frame.size.height/2
        answerImage.layer.borderWidth = 0
//        answerImage.layer.borderColor = = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        answerImage.clipsToBounds = true
//        self.view.addSubview(answerImage)
        playBtn.isHidden = true
        SVProgressHUD.show()
        var text:String = txtfield.text ?? ""
        answerLbl.text = text
        let userDefaults = UserDefaults.standard
        userDefaults.set(text, forKey: "replyKey")
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let nowString = formatter.string(from: now)
        UserDefaults.standard.set(nowString, forKey: "savedDate_hbotData")
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(self.accessToken)",
        ]
        
        AF.upload(multipartFormData: { (datavalue) in
            datavalue.append(text.data(using: .utf8)!, withName: "text")
            datavalue.append("2020-11-03T19:38:27+0300".data(using: .utf8)!, withName: "recorded_at")
            datavalue.append("en-US".data(using: .utf8)!, withName: "language_code")
            
        }, to: "https://api.dev.cogapps.net/simplified/", headers: headers).response { response in
            SVProgressHUD.dismiss()
            if let error = response.error {
                print(error.localizedDescription)
                return
            }
            
            if let data = response.data {
                do {
                    
                    if let serializationObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]{
                        updateGoals(goalType: .hbotMorning){ (countValue) in
                            self.setGoalViewBasedOnGoalCount(cnt: countValue)
                        }
                        self.showGoalSuccessPopup()
                        self.saveHbotToPlist(dictData: serializationObj)
                        
                        if let dictMorningData = HarmonySingleton.dictHbotData?["morningMessageData"] as? [String: Any]{
                            self.showMorningMessageData(serializationObj: dictMorningData)
                        }
                        
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
//                print(String.init(data: data, encoding: .utf8)!)

            }
            print(response)
            
        }
    }
    
    @IBAction func recordBtnAction(_ sender: Any) {
        
        
        
        
    }
    
    func getTokenValue()  {
        if let accessTokenyouro = UserDefaults.standard.string(forKey: "accessTokenyouro"){
            self.accessToken = accessTokenyouro
            txtfield.isEnabled = true
            return
        }
        SVProgressHUD.show()
        CouponManager.sharedInstance.getYouroTokens(email: "sakthivel.inq@gmail.com", password: "Password54%") { [self] (result) in
           
            txtfield.isEnabled = true
            switch result {
            case .success(let coupons):
               
                self.accessToken = coupons.access
                let userDefaultsCompletChallengeComplete = UserDefaults.standard
                userDefaultsCompletChallengeComplete.set(accessToken, forKey: "accessTokenyouro")
              print(accessToken)
                SVProgressHUD.dismiss()
            case .failure(let error):
                
                SVProgressHUD.dismiss()
            }
        }
        

    }
  
    
    func addSingleLineGrapghGraphNight(){
        self.nightBarChart.reloadInputViews()
        var chartData1 = [BarChartDataEntry]()
        for value in chartValuesforGraphNight {
            let percentageValue = value as NSNumber
            var percentageString = percentageValue.getPercentage()
            self.chartDateArrayforGraphWithPercentage.append(percentageString)
            
        }
        for (index,value) in chartValuesforGraphNight.enumerated(){
            let chartdataEntry = BarChartDataEntry(x: Double(index), y: Double(value))

            chartData1.append(chartdataEntry)
        }
       
        
        
        let chartDataSet = BarChartDataSet(entries: chartData1, label:"")
        chartDataSet.drawValuesEnabled = false
//        chartDataSet.colors = [UIColor(red: 44/255, green: 195/255, blue: 250/255, alpha: 1.0)]
        chartDataSet.colors = ChartColorTemplates.joyful()
        chartDataSet.highlightColor = UIColor.orange.withAlphaComponent(0.3)
        chartDataSet.highlightAlpha = 1
        let chartData = BarChartData(dataSet: chartDataSet)
        
        nightBarChart.data = chartData
        
        nightBarChart.xAxis.granularityEnabled = true
        nightBarChart.xAxis.granularity = 1
        nightBarChart.xAxis.drawAxisLineEnabled = false
        nightBarChart.xAxis.enabled = true
//        barChartView.YAxis.enabled = true
        nightBarChart.leftAxis.labelTextColor = UIColor.clear
        nightBarChart.leftAxis.enabled = true
        nightBarChart.rightAxis.enabled = true
        nightBarChart.drawBordersEnabled = false
//        chartView.leftAxis.drawAxisLineEnabled = false
        nightBarChart.xAxis.drawGridLinesEnabled = false
        nightBarChart.xAxis.labelFont = UIFont.boldSystemFont(ofSize: 9.0)
        nightBarChart.xAxis.labelTextColor = UIColor.black
        nightBarChart.xAxis.labelPosition = .bottom
  
//        barChartView.startAtZeroEnabled = true
//        barChartView.xAxis.avoidFirstLastClippingEnabled = false
        nightBarChart.xAxis.labelRotationAngle = -90
//        barChartView.xAxis.startAtZeroEnabled = true

    
        nightBarChart.xAxis.axisMaximum = 7
//        barChartView.leftAxis.valueFormatter = IndexAxisValueFormatter(values: chartDateArrayforGraphWithPercentage)
        nightBarChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: chartDateArrayforGraphNight)
        if nightBarChart.xAxis.labelPosition == .bottom {
            nightBarChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: chartDateArrayforGraphNight)
        }
        
        nightBarChart.barData?.setValueFormatter(MyValueFormatter())
        nightBarChart.barData?.setDrawValues(true)
        
        // Right axis configurations
        nightBarChart.rightAxis.drawAxisLineEnabled = false
        nightBarChart.rightAxis.drawGridLinesEnabled = false
        nightBarChart.rightAxis.drawLabelsEnabled = false
        nightBarChart.leftAxis.drawGridLinesEnabled = false
        nightBarChart.leftAxis.drawAxisLineEnabled = false

        
        // Other configurations
        nightBarChart.highlightPerDragEnabled = false
        nightBarChart.chartDescription?.text = ""
        nightBarChart.legend.enabled = false
        nightBarChart.scaleYEnabled = false
        
        nightBarChart.drawMarkers = false

        let l = nightBarChart.legend
        l.horizontalAlignment = .center
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 9
        l.font = UIFont(name: "HelveticaNeue-Light", size: 7)!
        l.xEntrySpace = 4
        marker.chartView = nightBarChart
        nightBarChart.marker = marker
        self.infoView.isHidden = false
        self.infoviewHeightConstrain.constant = 120
        infoViewProfileImage.isHidden = false
        infoviewProfileImageHeightConstrain.constant = 30
        self.goalView.isHidden = true
        vwGoalAchivement.isHidden = false
        getTaskCountsForLoad()
        
 //   self.showAddChallengeView()
//        self.addChallengeView.isHidden = false
//        self.addChallengeViewConstrain.constant = 80
//
    }
    func showAddChallengeView()
    {
            let DailyChallengeChatboatuserDefaults = UserDefaults.standard
            let completeDailyChallengeChatboat = DailyChallengeChatboatuserDefaults.string(forKey: "completeDailyChallengeChatboat")
            if completeDailyChallengeChatboat == "dailyChallengCompleted"{
                
                   self.addChallengeView.isHidden = false
                   self.addChallengeViewConstrain.constant = 80
            }
       
            
        
        let userDefaults = UserDefaults.standard
        let myString = userDefaults.string(forKey: "completeFitnessChatboat")
        if myString == "completed"{
            
               self.addChallengeView.isHidden = false
               self.addChallengeViewConstrain.constant = 80
        
          
        }
        let completeChoreChatboat = UserDefaults.standard
        let myStringcompleteChoreChatboat = completeChoreChatboat.string(forKey: "completeChoreChatboat")
        if myStringcompleteChoreChatboat == "completedChore"{
            self.addChallengeView.isHidden = false
            self.addChallengeViewConstrain.constant = 80
        }
    }
    
    func addSingleLineGrapghGraph(){
        self.barChartView.reloadInputViews()
        var chartData1 = [BarChartDataEntry]()
        for value in chartValuesforGraph {
            let percentageValue = value as NSNumber
            var percentageString = percentageValue.getPercentage()
            self.chartDateArrayforGraphWithPercentage.append(percentageString)
            
        }
        for (index,value) in chartValuesforGraph.enumerated(){
            let chartdataEntry = BarChartDataEntry(x: Double(index), y: Double(value))

            chartData1.append(chartdataEntry)
        }
       
        
        
        let chartDataSet = BarChartDataSet(entries: chartData1, label:"")
        chartDataSet.drawValuesEnabled = false
//        chartDataSet.colors = [UIColor(red: 44/255, green: 195/255, blue: 250/255, alpha: 1.0)]
        chartDataSet.colors = ChartColorTemplates.joyful()
        chartDataSet.highlightColor = UIColor.orange.withAlphaComponent(0.3)
        chartDataSet.highlightAlpha = 1
        let chartData = BarChartData(dataSet: chartDataSet)
        
        barChartView.data = chartData
        
        barChartView.xAxis.granularityEnabled = true
        barChartView.xAxis.granularity = 1
        barChartView.xAxis.drawAxisLineEnabled = false
        barChartView.xAxis.enabled = true
//        barChartView.YAxis.enabled = true
        barChartView.leftAxis.labelTextColor = UIColor.clear
        barChartView.leftAxis.enabled = true
        barChartView.rightAxis.enabled = true
                barChartView.drawBordersEnabled = false
//        chartView.leftAxis.drawAxisLineEnabled = false
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.xAxis.labelFont = UIFont.boldSystemFont(ofSize: 9.0)
        barChartView.xAxis.labelTextColor = UIColor.black
        barChartView.xAxis.labelPosition = .bottom
  
//        barChartView.startAtZeroEnabled = true
//        barChartView.xAxis.avoidFirstLastClippingEnabled = false
        barChartView.xAxis.labelRotationAngle = -90
//        barChartView.xAxis.startAtZeroEnabled = true

    
        barChartView.xAxis.axisMaximum = 7
//        barChartView.leftAxis.valueFormatter = IndexAxisValueFormatter(values: chartDateArrayforGraphWithPercentage)
        barChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: chartDateArrayforGraph)
        if barChartView.xAxis.labelPosition == .bottom {
            barChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: chartDateArrayforGraph)
        }
        
        barChartView.barData?.setValueFormatter(MyValueFormatter())
        barChartView.barData?.setDrawValues(true)
        
        // Right axis configurations
       barChartView.rightAxis.drawAxisLineEnabled = false
        barChartView.rightAxis.drawGridLinesEnabled = false
        barChartView.rightAxis.drawLabelsEnabled = false
        barChartView.leftAxis.drawGridLinesEnabled = false
        barChartView.leftAxis.drawAxisLineEnabled = false

        
        // Other configurations
        barChartView.highlightPerDragEnabled = false
        barChartView.chartDescription?.text = ""
        barChartView.legend.enabled = false
        barChartView.scaleYEnabled = false
        
        barChartView.drawMarkers = false

        let l = barChartView.legend
        l.horizontalAlignment = .center
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 9
        l.font = UIFont(name: "HelveticaNeue-Light", size: 7)!
        l.xEntrySpace = 4
        marker.chartView = barChartView
        barChartView.marker = marker
        self.infoView.isHidden = false
        self.infoviewHeightConstrain.constant = 120
        infoViewProfileImage.isHidden = false
        infoviewProfileImageHeightConstrain.constant = 30
        self.goalView.isHidden = true
        vwGoalAchivement.isHidden = false
        getTaskCountsForLoad()
        self.getAllChallenges()
        
    }
    
    func showGoalSuccessPopup(){
        DispatchQueue.main.async {
            let title = NSMutableAttributedString(string: "")
            let subTitle = NSMutableAttributedString(string: "You have achieved a daily goal!")
            AlertManager.shared.showSuccesAlertView(controller: self, title: title, subTitle: subTitle, imgLogo: "goal_ball", btnTitle: "Close") {
            }
        }
    }
    private func getTaskCountsForLoad() {
      
        checkGoalCount()
        
        /*
        self.getTaskCount{ (result) in
            switch result {
                
            case .success(_):
                print("Suceess")
                
                self.goalView.isHidden = false

            case .failure(let error):
                break
            }
        }
         */
        
    }
    
    //saravanan checking
    
    
    func checkGoalCount(){
        getGoals { (goalCount) in
            DispatchQueue.main.async {
                self.setGoalViewBasedOnGoalCount(cnt: goalCount)
            }
          
            /*
            self.disableGoalImage.isHidden = false
            self.disableGoalWidthConstrain.constant = 152
            self.completeGoal.isHidden = true
            self.completeGoalWidthConstrain.constant = 0
            self.goalInforLabel.text = ""

            
            if goalCount == 0{
                self.disableGoalImage.image = UIImage(named: "disableGoal")
            }else if  goalCount == 1{
                self.disableGoalImage.image = UIImage(named: "oneGoal")
            }
            else if  goalCount == 2{
                self.disableGoalImage.image = UIImage(named: "twoGoal")
            }
            else if  goalCount == 3{
                self.disableGoalImage.image = UIImage(named: "threeGoal")
            }
            else if  goalCount == 4{
                self.disableGoalImage.image = UIImage(named: "fourGoal")
            }else{
                self.completeGoal.image = UIImage(named: "fiveGoal")
                self.disableGoalImage.isHidden = true
                self.disableGoalWidthConstrain.constant = 0
                self.completeGoal.isHidden = false
                self.completeGoalWidthConstrain.constant = 19
                self.goalInforLabel.text = "Congratulation! you achieved today goals!!"
            }
 */
        }
    }
        func getTaskCount(handler: @escaping (Swift.Result<pendingTaskCount, NSError>) -> Void) {
            let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                       "date": DateUtils.format(date: Date(), in: .yearMonthDay) ?? ""]
            
            let operation = NetworkOperation(method: .post, parameters: params, headers: nil, encoder: JSONEncoding.default, url: APIService.getTaskCountInfo)
            
            BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
                switch result {
                case .success(let data):
                    do {
                        let pendingTaskListValue = try JSONDecoder().decode(pendingTaskCount.self, from: data as! Data)
                        let labelMessage = pendingTaskListValue.data.msg
                        self.goalInforLabel.text = "\(labelMessage)"
                        self.disableGoalImage.isHidden = false
                        self.disableGoalWidthConstrain.constant = 152
                        self.completeGoal.isHidden = true
                        self.completeGoalWidthConstrain.constant = 0
                        
                        if labelMessage.contains("5")
                        {
                            self.disableGoalImage.image = UIImage(named: "disableGoal")
                            
                        }else  if labelMessage.contains("4"){
                            self.disableGoalImage.image = UIImage(named: "oneGoal")
                           
                        }else if labelMessage.contains("3"){
                            self.disableGoalImage.image = UIImage(named: "twoGoal")
                            
                        }else if labelMessage.contains("2"){
                            self.disableGoalImage.image = UIImage(named: "threeGoal")
                        }else if labelMessage.contains("1"){
                            self.disableGoalImage.image = UIImage(named: "fourGoal")
                        }else{
                            
                            self.completeGoal.image = UIImage(named: "fiveGoal")
                            self.disableGoalImage.isHidden = true
                            self.disableGoalWidthConstrain.constant = 0
                            self.completeGoal.isHidden = false
                            self.completeGoalWidthConstrain.constant = 19
                        }
                        
                        handler(.success(pendingTaskListValue))
                    } catch {
                        handler(.failure(error as NSError))
                    }
                case .failure(let error):
                    handler(.failure(error as NSError))
                }
            }
        }
    
    private func getYuruMessageForLoad() {
      
        self.getYuruStartMessage{ (result) in
            switch result {
                
            case .success(_):
                self.userAgeDailyChallenge = 1
//                self.goodMorninView.isHidden = false
//                self.GMProfileImage.isHidden = false
//                self.replyView.isHidden = false
//                self.bottomView.isHidden = false
//                self.noContentView.isHidden = true
                print("Suceess")

            case .failure(let error):
                self.presentCustomAlertViewController()
                break
            }
        }
    }
    
        func getYuruStartMessage(handler: @escaping (Swift.Result<yuruMessage, NSError>) -> Void) {
            let params : Parameters = ["guid": UserDefaultConstants().guid ?? "",
                                       "date": DateUtils.format(date: Date(), in: .yearMonthDay) ?? "",
                                       "time": DateUtils.format(date: Date(), in: .hourAndMinuteAndSecond) ?? "",
                                       "type": 1]
            
            
            let operation = NetworkOperation(method: .post, parameters: params, headers: nil, encoder: JSONEncoding.default, url: APIService.getYuruMorningMessage)
            
            BackendManager.sharedInstance.startRequest(operation: operation) { (result) in
                switch result {
                case .success(let data):
                    do {
                        let yuruMessageListValue = try JSONDecoder().decode(yuruMessage.self, from: data as! Data)
                        self.receiverTxtMessageLbl.text = yuruMessageListValue.data.msg
                        HarmonySingleton.dictHbotData?["yuruInitialMessage"] = yuruMessageListValue.data.msg
                        saveHbotData()
                        handler(.success(yuruMessageListValue))
                    } catch {
                        handler(.failure(error as NSError))
                        print(error)
                    }
                case .failure(let error):
                    handler(.failure(error as NSError))
                    print(error)
                }
            }
        }

    func restartAnimation() {
        self.chatTableView.reloadData()
    }
    
    func configureNoChallengeUI() {
//        if personalChallengeList.count == 0 {
//            noChallengeContainerView.isHidden = false
//            addNewChallengeButton.isHidden = true
//        } else {
//            addNewChallengeButton.isHidden = false
//            noChallengeContainerView.isHidden = true
//            challengeTableVIew.isHidden = false
//            addChallengeContainerView.isHidden = true
//        }
    }
    
    func setupTableView() {
        completFitnessTableView.register(UINib.init(nibName: "ChallengeTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        chatTableView.register(UINib.init(nibName: "ChallengeTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        chatTableView.refreshControl = refresh
        refresh.addTarget(self, action: #selector(getpersonalchallenges), for: .valueChanged)
        chatTableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 80, right: 0)
//        chatTableView.contentInset.bottom = addNewChallengeButton.frame.height
    }
    
//    func setupHoleChatTableView() {
//        chatTableView.register(UINib.init(nibName: "ChallengeTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
//
//
//    }
    
    public func getCurrentTimeAndDate(){
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let startTime = cal.startOfDay(for: date)
       
        let startTimedate = UserDefaults.standard.value(forKey: "startDate_Time")
        if startTimedate == nil {
            UserDefaults.standard.set(startTime, forKey: "startDate_Time")
            UserDefaults.standard.set(startTime, forKey: "updateDate_Time")

        }else{
            UserDefaults.standard.set(startTime, forKey: "startDate_Time")

        }
        
        let todayDate = "\(UserDefaults.standard.value(forKey: "startDate_Time") ?? "")"
        let tommorrowDate = "\(UserDefaults.standard.value(forKey: "updateDate_Time") ?? "")"
        
        if todayDate == tommorrowDate {
            UserDefaults.standard.set(0, forKey: "final_Stepcount")

        } else {
            let nextDay = UserDefaults.standard.bool(forKey: "nextDay_Progress")
            if nextDay {
                UserDefaults.standard.set(false, forKey: "nextDay_Progress")
                let sTotal = UserDefaults.standard.value(forKey: "total_Stepcount")
                UserDefaults.standard.set(sTotal, forKey: "final_Stepcount")
            }

        }
       

        
    }
    
    func timerMethod()  {
        self.stepCountTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.updateHealthKitDatappp), userInfo: nil, repeats: true)
    }
    
    func removeUserDefaults()  {
        UserDefaults.standard.removeObject(forKey: "startDate_Time")
        UserDefaults.standard.removeObject(forKey: "updateDate_Time")
        UserDefaults.standard.removeObject(forKey: "starting_StepCount")
        UserDefaults.standard.removeObject(forKey: "update_StepCount")
    }
    
    @objc func updateHealthKitDatappp() {
        if Reachability.isConnectedToNetwork(){
        }else{
        self.view.toast("Internet Connection not Available!")
        return
    }
    
        let oldStepCount = UserDefaults.standard.value(forKey: "starting_StepCount")
        let newStepCount = UserDefaults.standard.value(forKey: "update_StepCount")
        
        let oldValue = "\(oldStepCount ?? "")"
        let newValue = "\(newStepCount ?? "")"

        let float1 = Float(oldValue) ?? 0
        let float2 = Float(newValue) ?? 0
        
        var stepCount = Float()

        if  oldStepCount != nil {
            
            self.getCurrentTimeAndDate()

            let todayDate = "\(UserDefaults.standard.value(forKey: "startDate_Time") ?? "")"
            let tommorrowDate = "\(UserDefaults.standard.value(forKey: "updateDate_Time") ?? "")"

            if todayDate == tommorrowDate {
                stepCount = float2 - float1
                let sCounts =  String(format: "%.2f", stepCount)
                stepCount = Float(sCounts) ?? 0
                UserDefaults.standard.removeObject(forKey: "final_Stepcount")

                if oldStepCountProgress == false && progressCount > 0 && stepCount == 0 {
                    oldStepCountProgress = true
                    if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                        switch startedChallenge.data.challengeType {
                        case .walk:
                            let progressStepCount =  (progressCount)
                            stepCount = progressStepCount
                            let oldStep = float1 - (progressCount)
                            let newStep = oldStep + stepCount

                            UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
                            UserDefaults.standard.set(newStep, forKey: "update_StepCount")

                            progressCount = 0.0
                            break
                        case .swim:
                            //distance swim or pool swim
                            if modeofChallenge == 4 {
                                HomeViewController().healthService.getswmmingDistance { (distance, error) in
                                    DispatchQueue.main.async {
                                        let stringValue = String(format: "%.2f", distance ?? 0.0)
                                        stepCount = Float(distance!)
                                    }
                                }
                            }else {
                                stepCount = Float(travelleddistance)
                            }
                            let progressStepCount = (progressCount)
                            stepCount = progressStepCount
                            let oldStep = float1 - (progressCount)
                            let newStep = oldStep + stepCount

                            UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
                            UserDefaults.standard.set(newStep, forKey: "update_StepCount")

                            progressCount = 0.0
                            break
                        case .run:
                            let progressStepCount = (progressCount)
                            stepCount = progressStepCount
                            let oldStep = float1 - (progressCount)
                            let newStep = oldStep + stepCount

                            UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
                            UserDefaults.standard.set(newStep, forKey: "update_StepCount")

                            progressCount = 0.0
                            break
                        case .bike:
                            if modeofChallenge == 2 {
                                HomeViewController().healthService.getCyclingDistance { (distance, error) in
                                    DispatchQueue.main.async {
                                        
                                        let stringValue = String(format: "%.2f", distance ?? 0.0)
                                        stepCount = Float(distance!)
                                        print("cycling distance \(stringValue)")
                                        //  self.KmsLabel.text = stringValue
                                    }
                                }
                            }else if modeofChallenge == 1 {
                                stepCount = Float(travelleddistance)
                            }
                            let progressStepCount = (progressCount)
                            stepCount = progressStepCount
                            let oldStep = float1 - (progressCount)
                            let newStep = oldStep + stepCount

                            UserDefaults.standard.set(oldStep, forKey: "starting_StepCount")
                            UserDefaults.standard.set(newStep, forKey: "update_StepCount")

                            progressCount = 0.0
                            break
                        }
                    }
                }
                
                UserDefaults.standard.set(stepCount, forKey: "total_Stepcount")

            } else {

                if float1 == 0{

                    let sTotal = UserDefaults.standard.value(forKey: "final_Stepcount")
                    let totalS = "\(sTotal ?? "")"

                    let totalSteps = Float(totalS) ?? 0
                    let progressStepCount = totalSteps

                    stepCount = float2 + progressStepCount
                    
                    UserDefaults.standard.set(stepCount, forKey: "total_Stepcount")

                   
                } else {
                    stepCount = float2 - float1
                    UserDefaults.standard.set(stepCount, forKey: "total_Stepcount")
                    UserDefaults.standard.set(0, forKey: "starting_StepCount")
                    UserDefaults.standard.set(0, forKey: "update_StepCount")
                    UserDefaults.standard.set(true, forKey: "nextDay_Progress")

                }
                let sCounts =  String(format: "%.2f", stepCount)
                stepCount = Float(sCounts) ?? 0

            }
            
        } else if oldStepCount == nil && progressCount > 0 {
            
            if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {

                switch startedChallenge.data.challengeType {
                    case .walk:
    
                        let progressStepCount = (progressCount)
                        stepCount = progressStepCount
                        
                        break
                    case .swim:
                        if modeofChallenge == 3 {
                            let progressStepCount = travelleddistance // (progressCount)
                            stepCount = Float (progressStepCount)
                        }else {
                        let progressStepCount = (progressCount)
                        stepCount = progressStepCount
                        }
                       break
                    case .run:
                        let progressStepCount = (progressCount)
                        stepCount = progressStepCount
                        break
                    case .bike:
                        if modeofChallenge == 1 {
                            let progressStepCount = travelleddistance // (progressCount)
                            stepCount = Float (progressStepCount)
                        }else {
                            let progressStepCount = (progressCount)
                            stepCount = progressStepCount
                        }
                        break
                    }
                
                
            }
        } else {
            
        }
        
        self.setupforHealthKitData()
  
        print("current step count-->",stepCount)
        
       let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge()
        
        let startDateAndTime = UserDefaults.standard.value(forKey: "startDate_Time")
        let today = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let currentTime = formatter .string(from: today as Date)
        
        let count = startedChallenge?.data.challengeRunStep ?? 0
        
        let currentGuid = UserDefaults.standard.value(forKey: "current_userGuid")
        let challengeId = UserDefaults.standard.value(forKey: "challenge_id")
        
        let totalStep = stepCount == 0 ? count : stepCount
        let reminingValue = Float((startedChallenge?.data.challengeTask) ?? 0) - stepCount
        if modeofChallenge == 1 || modeofChallenge == 3 {
            let distance = BaseViewController().travelledDistance(locations: locations1)
            stepCount = Float(travelleddistance)
        }else {
        
        if stepCount != 0  {
        PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: Float(stepCount)) { (result) in
            switch result {
            case .success(let newActiveChallenge):
                        if self.reloadPedometerUpdates {
                            self.chatTableView.reloadData()
                        }
            case .failure(let error):
                print(error)
            }
        }
        }
        
        if personalChallengeList.count == 0{
            stepCountTimer.invalidate()
//            debugTextview.text = ""
        }
    }
    
    }
    
    private func setupforHealthKitData() {
        
        if self.activeChallenge != nil && !initialHealthKitCall{

            
            var serviceTye = HealthServiceType.stepCount
            
            if self.activeChallenge?.data.challengeType == .run{
                serviceTye = HealthServiceType.distanceWalked
            }else if self.activeChallenge?.data.challengeType == .swim{
                serviceTye = HealthServiceType.distanceSwam
            }else if self.activeChallenge?.data.challengeType == .bike{
                serviceTye = HealthServiceType.distanceCycled
            }

     
            let startDateAndTime = UserDefaults.standard.value(forKey: "startDate_Time")
            let currentTime = NSDate()
            print("today-->",currentTime)
           
            if self.activeChallenge?.data.challengeType != .bike{
                healthService.sampleDataforHealthkit(forServiceType: serviceTye,
                                                     sampleStartTime: startDateAndTime as! Date,
                                                     sampleEndTime: currentTime as Date) { (value, error) in
                    guard error == nil else {
                        print("health service error-->",error ?? "")
                        return
                    }
                } onUpdate: { (updateValue, updateError) in
                    guard updateError == nil else {
                        return
                    }
                }
                
            }
        } else {
//            PedometerManager.sharedInstance.stopUpdates()
            self.debugView?.challengeSource = nil
            self.debugView?.challengeID = nil
            self.debugView?.startDate = nil
            self.debugView?.endDate = nil
            self.debugView?.nrOfSteps = nil
            self.debugView?.distance = nil
//            self.debugView?.activityType = nil
            self.debugView?.challengeType = nil
            self.debugView?.progress = nil
        }
    }
//    func presentChallengeActionsViewController() {
//        if challengeActionsViewController == nil {
//            challengeActionsViewController = GlobalStoryBoard().challengeActionsVC
//            challengeActionsViewController?.delegate = self
//        }
//        tabBarController?.present(challengeActionsViewController!, animated: false, completion: nil)
//    }
    private func updateDailyChallenge(challenge: DailyChallenge, status: DailyChallengeStatus, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        SVProgressHUD.show()
        DailyChallengeManager.sharedInstance.updateDailyChallenge(dailyChallenge: challenge, status: status) { (result) in
            SVProgressHUD.dismiss()
            switch result {
            case .success(let dailyChallenge):
                self.dailyChallengeTableview.reloadData()
                handler(.success(dailyChallenge))
            case .failure(let error):
                self.dailyChallengeTableview.reloadData()
                 handler(.failure(error as NSError))
            }
        }
    }
    
    func presentDeleteChallengeViewController(personalChallenge: PersonalChallenge, indexPath: IndexPath) {
        deleteChallengeViewController = GlobalStoryBoard().deleteChallengeVC
        deleteChallengeViewController?.indexPath = indexPath
        deleteChallengeViewController?.personalChallenge = personalChallenge
        deleteChallengeViewController?.delegate = self
        tabBarController?.present(deleteChallengeViewController!, animated: true, completion: nil)
    }
    func presentRestartChallengeViewController(personalChallenge: PersonalChallenge, indexPath: IndexPath) {
        restartChallengeViewController = GlobalStoryBoard().restartChallengeVC
        restartChallengeViewController?.indexPath = indexPath
        restartChallengeViewController?.personalChallenge = personalChallenge
        restartChallengeViewController?.delegate = self
        tabBarController?.present(restartChallengeViewController!, animated: true, completion: nil)
    }
    func presentCustomAlertViewController(isMessage: String) {
        customAlertViewController = GlobalStoryBoard().customAlertVC
        if HarmonySingleton.shared.checkChallengeStatus?.taskType == 3
        {
            customAlertViewController?.alertTitle = "Onboarding challenge"
        }else{
            customAlertViewController?.alertTitle = "Daily challenge"
        }
        
//        if  HarmonySingleton.shared.dashBoardData.data?.userType == RegistrationType.child{
//            customAlertViewController?.alertMessage = "Yay! The invitation has been sent! Waiting for your parent to accept your invite."
//
//        } else {
            customAlertViewController?.alertMessage = isMessage
//        }
        customAlertViewController?.delegate = self
        tabBarController?.present(customAlertViewController!, animated: true, completion: nil)
    }
    func presentCompletedChallengeViewControllerForDailyChallenge(dailyChallenge: DailyChallenge, indexPath: IndexPath?) {
        completedChallengeViewController = GlobalStoryBoard().completedChallengeVC
        completedChallengeViewController?.dailyChallenge = dailyChallenge
        dailyChallengeChatboat = dailyChallenge
        completedChallengeViewController?.indexPath = indexPath
        completedChallengeViewController?.delegate = self

        tabBarController?.present(completedChallengeViewController!, animated: true, completion: nil)
    }

    func presentDeleteChallengeViewControllerDailly(personalChallenge: DailyChallenge) {
        deleteChallengeViewController = GlobalStoryBoard().deleteChallengeVC
//        deleteChallengeViewController?.indexPath = indexPath
//        deleteChallengeViewController?.personalChallenge = personalChallenge
        deleteChallengeViewController?.daillyChallenge = personalChallenge
        deleteChallengeViewController?.delegate = self
        tabBarController?.present(deleteChallengeViewController!, animated: true, completion: nil)
    }
    
    func presentCompletedChallengeViewController(personalChallenge: PersonalChallenge, indexPath: IndexPath) {
        completedChallengeViewController = GlobalStoryBoard().completedChallengeVC
        completedChallengeViewController?.personalChallenge = personalChallenge
        personalChallengeFitness = personalChallenge
        
        completedChallengeViewController?.indexPath = indexPath
        completedChallengeViewController?.delegate = self
      
        tabBarController?.present(completedChallengeViewController!, animated: true, completion: nil)
        
    }
    
    func removeTableViewCell(indexPathToRemove: IndexPath) {
        do {
            let index = try indexPathToRemove
            UIView.animate(withDuration: 1) {
                self.chatTableView.performBatchUpdates({
                    self.chatTableView.deleteRows(at: [index], with: .fade)
                }){(completed) in
                    self.reloadPedometerUpdates = true
                    self.restartAnimation()
                    self.configureNoChallengeUI()
                }
            }
        } catch {
            
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView.tag == 5{
         
            if personalChallengeList.count > 0{
                
                return personalChallengeList.count + 1
            }
        
         
        }
        else if tableView.tag == 25{
           
            if dailyChallenges.count > 0{
                let dailyChallengesValue = dailyChallenges[0]
                
                if dailyChallengesValue.title == "Drink Water"
                {
                    if dailyChallengesValue.userCompletionStatus == .rewards{
                        return dailyChallenges.count + 1
                    }
                    return dailyChallenges.count
                }
                return dailyChallenges.count + 1
            }else{
                
                    let DailyChallengeChatboatuserDefaults = UserDefaults.standard
                    let completeDailyChallengeChatboat = DailyChallengeChatboatuserDefaults.string(forKey: "completeDailyChallengeChatboat")
                if completeDailyChallengeChatboat == "dailyChallengCompleted"{
                    return 2
                }
            }
           
        }
        else if tableView.tag == 15{
         return 2
        }else if tableView.tag == 20{
            return 2
        }
        else{
            if  ChoresListModel?.data?.count ?? 0 > 0 {
                var choreCount = ChoresListModel?.data?.count ?? 0
                return choreCount
            }
            
        }
      
        return 0
       
    }

    func numberOfSections(in tableView: UITableView) -> Int {
//        if tableView.tag == 25
//        {
//            if dailyChallenges.count > 0{
//                return sectionArray.count
//            }
//        }
        return 1
    }

    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if tableView.tag == 100
//        {
//            if indexPath.row == 0{
//            let cell = tableView.dequeueReusableCell(withIdentifier: greetingCellIdentifier, for: indexPath) as! greetingTableViewCell
//                return cell
//        }else if indexPath.row == 1{
//            let cell = tableView.dequeueReusableCell(withIdentifier: greetinReplyCellIdentifier, for: indexPath) as! greetinReplyTableViewCell
//            return cell
//        }else if indexPath.row == 2{
//            let cell = tableView.dequeueReusableCell(withIdentifier: youroResponseCellIdentifier, for: indexPath) as! youroResponseTableViewCell
//            return cell
//        }
//        }
//
        
        if tableView.tag == 10
        {
//            if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: choreAcceptAndPendingCell, for: indexPath) as! choreAcceptAndPendingCell
            
            
            cell.cellForRow(indexPath: self.ChoresListModel!, indexpath: indexPath, backgroundColor: "Yes")
            cell.doneBtn.tag = indexPath.row
            cell.doneBtn.addTarget(self, action: #selector(presentDonePopup), for: .touchUpInside)
            
            cell.removeBtn.tag = indexPath.row
            cell.removeBtn.addTarget(self, action: #selector(presentConfirmPopup), for: .touchUpInside)
            cell.rmvNwBtn.tag = indexPath.row
            cell.rmvNwBtn.addTarget(self, action: #selector(presentConfirmPopup), for: .touchUpInside)
            cell.rmvNwBtn.isHidden = true
            cell.decideBtn.tag = indexPath.row
//            cell.decideBtn.addTarget(self, action: #selector(decideBtnAct), for: .touchUpInside)
            return cell
//        }else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: factCardCellReuseIdentifier, for: indexPath) as! FatCardTableViewCell
//            return cell
//        }
        }
        else if tableView.tag == 20
        {
            if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: choreAcceptAndPendingCell, for: indexPath) as! choreAcceptAndPendingCell
            
            cell.cellForRowChoreComplete(indexPath: self.ChoresListModel, indexpath: indexPath)
            cell.doneBtn.tag = indexPath.row
            cell.doneBtn.addTarget(self, action: #selector(presentDonePopup), for: .touchUpInside)
            cell.doneBtn.isHidden = true
            cell.removeBtn.tag = indexPath.row
            cell.removeBtn.addTarget(self, action: #selector(presentConfirmPopup), for: .touchUpInside)
            cell.removeBtn.isHidden = true
            cell.rmvNwBtn.tag = indexPath.row
            cell.rmvNwBtn.addTarget(self, action: #selector(presentConfirmPopup), for: .touchUpInside)
            cell.rmvNwBtn.isHidden = true
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: chatboatCongrazReuseIdentifier, for: indexPath) as! ChatboatCongrajTableViewCell
            cell.choreCompleted()
            return cell
        }
        }
        else  if tableView.tag == 15{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! ChallengeTableViewCell
                
                cell.delegate = self
                cell.isFromController = "FitnessTableview"
                cell.personalChallengeChatboat = personalChallengeFitness
                
                let userDefaultsCompledailytChallengeComplete = UserDefaults.standard
                userDefaultsCompledailytChallengeComplete.set("yes", forKey: "setBackGround")
                cell.challengeCanStartChatBoat = false
                cell.refreshButton.isHidden = true
                cell.removeButton.isHidden = true
                //            cell.animateButton()
                return cell
            }else{

            let cell = tableView.dequeueReusableCell(withIdentifier: chatboatCongrazReuseIdentifier, for: indexPath) as! ChatboatCongrajTableViewCell
            cell.fitnessCompleted()
            return cell
        }
        }
        
        else  if tableView.tag == 25{
            
            let userDefaultsCompledailytChallengeComplete = UserDefaults.standard
            userDefaultsCompledailytChallengeComplete.set("yes", forKey: "setBackGroundDaily")
            if indexPath.row == 0{
                if dailyChallenges.count > 0 {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: challengeCellReuseIdentifier, for: indexPath) as! ChallengeTableViewCell
                  
                    cell.delegate = self
                    cell.isFromController = "dailytableview"
                    cell.removeButton.isHidden = true
                    //                    var dailyChallegeTableView:Bool = true
                    //                    var fitnessTableview:Bool = false
    //                if onboardingChallenges.count > 0{
    //                    cell.onboardingChallenge = onboardingChallenges[indexPath.row]
    //                    cell.challengeCanStart = onboardingChallenges[indexPath.row].cStatus == nil
    //                } else
                    
                        let DailyChallengeChatboatuserDefaults = UserDefaults.standard
                        let completeDailyChallengeChatboat = DailyChallengeChatboatuserDefaults.string(forKey: "completeDailyChallengeChatboat")
                    if completeDailyChallengeChatboat == "dailyChallengCompleted"{
                        cell.dailyChallengeCompleted = dailyChallenges[indexPath.row]
                    }else{
                        if dailyChallenges.count > 0{
                            cell.dailyChallenge = dailyChallenges[indexPath.row]
//                            let userDefaults = UserDefaults.standard
//                            let myString = userDefaults.string(forKey: "userCompletionStatus")
//                            if myString == "alreadyUpdated"{
//                                cell.challengeCanStart = false
//                            }else{
//                                userDefaults.set("alreadyUpdated", forKey: "userCompletionStatus")
                        
                                cell.challengeCanStart = dailyChallenges[indexPath.row].userCompletionStatus == .notStarted
                            
                            if dailyChallenges[indexPath.row].challegeTaskType == 1{
                                if dailyChallenges[indexPath.row].userCompletionStatus == .notStarted{
                                    cell.delegate?.didTapChallengeTableViewCellStartButton(challengeTableViewCell: cell, fromController: "dailytableview")
//                                        cell.whatsNextButton.isHidden = true
                                }
                            }
                            
//                            }
                        }
                    }
                   
                    cell.animateButton()
                    cell.refreshButton.isHidden = true
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: challengeCellReuseIdentifier, for: indexPath) as! ChallengeTableViewCell
                    cell.delegate = self
                    cell.isFromController = "dailytableview"
                    cell.removeButton.isHidden = true
                    //                    var dailyChallegeTableView:Bool = true
                    //                    var fitnessTableview:Bool = false
    //                if onboardingChallenges.count > 0{
    //                    cell.onboardingChallenge = onboardingChallenges[indexPath.row]
    //                    cell.challengeCanStart = onboardingChallenges[indexPath.row].cStatus == nil
    //                } else
                    
                        let DailyChallengeChatboatuserDefaults = UserDefaults.standard
                        let completeDailyChallengeChatboat = DailyChallengeChatboatuserDefaults.string(forKey: "completeDailyChallengeChatboat")
                    if completeDailyChallengeChatboat == "dailyChallengCompleted"{
                        cell.dailyChallengeCompleted = dailyChallengeChatboat
                    }else{
                        if dailyChallenges.count > 0{
                            cell.dailyChallenge = dailyChallenges[indexPath.row]
//                            let userDefaults = UserDefaults.standard
//                            let myString = userDefaults.string(forKey: "userCompletionStatus")
//                            if myString == "alreadyUpdated"{
//                                cell.challengeCanStart = false
//                            }else{
//                                userDefaults.set("alreadyUpdated", forKey: "userCompletionStatus")
                                cell.challengeCanStart = dailyChallenges[indexPath.row].userCompletionStatus == .notStarted
//                            }
                        }
                    }
                   
                    cell.animateButton()
                    cell.refreshButton.isHidden = true
                    return cell
                }
            }else{
                let DailyChallengeChatboatuserDefaults = UserDefaults.standard
                let completeDailyChallengeChatboat = DailyChallengeChatboatuserDefaults.string(forKey: "completeDailyChallengeChatboat")
                if completeDailyChallengeChatboat == "dailyChallengCompleted"{
                    let cell = tableView.dequeueReusableCell(withIdentifier: chatboatCongrazReuseIdentifier, for: indexPath) as! ChatboatCongrajTableViewCell
                    cell.dailyCompleted()
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: factCardCellReuseIdentifier, for: indexPath) as! FatCardTableViewCell
//                    let cell = tableView.dequeueReusableCell(withIdentifier: chatboatCongrazReuseIdentifier, for: indexPath) as! ChatboatCongrajTableViewCell
                    
                    return cell
                }
             
            }
       
            
        }
        else  if tableView.tag == 5{
            if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! ChallengeTableViewCell
            
            let personalChallenge = personalChallengeList[indexPath.row]
            cell.delegate = self
            //        self.dailyChallegeTableView:Bool = false
            //        self.fitnessTableview:Bool = true
                
                let userDefaultsCompledailytChallengeComplete = UserDefaults.standard
                userDefaultsCompledailytChallengeComplete.set("yes", forKey: "setBackGround")
            cell.personalChallenge = personalChallenge
            cell.isFromController = "FitnessTableview"
                
            cell.challengeCanStart = PersonalFitnessManager.sharedInstance.challengeCanStart(challenge: personalChallenge)
            cell.refreshButton.addTarget(self, action: #selector(getpersonalchallengesInfoFromHealthKit), for: .touchUpInside)
            cell.animateButton()
                cell.removeButton.isHidden = true
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: factCardCellReuseIdentifier, for: indexPath) as! FatCardTableViewCell
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 10
        {
            wellnessParentVC?.choreProgressContainerView.isHidden = false
    //        wellnessParentVC?.handleViewControllersUI(nwCHoreVw: true, nwChoreVC: true, chorePendingVC: true, createChoreVC: true, choreProgressVC: false)
            ChoreProgressVm.shared.dataObj = ChoresListModel?.data![indexPath.row]
            wellnessParentVC?.choreProgressvc?.choreUITable.reloadData()
        }
    }
    
//        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//            if tableView.tag == 10{
//                return 170.0
//
//            }else if tableView.tag == 20{
//                return 170.0
//            }
//            return 150.0
//        }
    @objc func presentDonePopup(_ sender: UIButton) {
        okayButton(sender: sender)
    }
    func showchoreDonePopUp(sender: UIButton) {
        selectedTag = sender.tag
        doneChoreViewController = GlobalStoryBoard().doneChoreVC
        doneChoreViewController?.selectedTag = sender.tag
        doneChoreViewController?.choresListModel = ChoresListModel
        doneChoreViewController?.delegate = self
        tabBarController?.present(doneChoreViewController!, animated: true, completion: nil)
     
    }
    @objc func presentConfirmPopup(_ sender: UIButton) {
        if sender.titleLabel?.text == "What's Next"{
            let challengeVc = GlobalStoryBoard().completedChallengeVC
            let indexData = self.ChoresListModel!.data![sender.tag]
            challengeVc.delegateChore = self
            challengeVc.choreTask = indexData
            challengeVc.isFromChoresVC = true
            self.present(challengeVc, animated: true, completion: nil)
            let userDefaultsCompletChallengeComplete = UserDefaults.standard
            userDefaultsCompletChallengeComplete.set("completedChore", forKey: "completeChoreChatboat")
//            let indexData = ChoresListModel?.data![selectedTag]
            
            let taskName = indexData.task_name
            let userDefaultstaskName = UserDefaults.standard
            userDefaultstaskName.set(taskName, forKey: "ChoreCompletetaskName")
            let Description = indexData.task_subName
            let userDefaultsDescription = UserDefaults.standard
            userDefaultsDescription.set(Description, forKey: "ChoreCompleteDescription")
            let experianceString = indexData.task_xp_value ?? "200"
            let userDefaultsexperianceString = UserDefaults.standard
            userDefaultsexperianceString.set(experianceString, forKey: "experianceString")
            var harmoneyString = indexData.harmoney_bucks  ?? "10"
            let userDefaultsharmoneyString = UserDefaults.standard
            userDefaultsharmoneyString.set(harmoneyString, forKey: "harmoneyString")
            var ImgUrl = indexData.task_image_url
            let userDefaultsImgUrlString = UserDefaults.standard
            userDefaultsImgUrlString.set(ImgUrl, forKey: "choreImgUrl")
            
//            self.choresListModelCompleted = self.ChoresListModel
//            self.choreTableView.isHidden = true
//            self.choreTablevieHeightConstrain.constant = 0
//            self.choreCompleteTableview.isHidden = false
//            self.choreCompleteHeightConstrain.constant = 170
//            choreCompleteTableview.reloadData()
        }else{
            selectedTag = sender.tag
            removeChoreViewController = GlobalStoryBoard().removeChoreVC
            removeChoreViewController?.selectedTag = sender.tag
            removeChoreViewController?.choresListModel = ChoresListModel
            removeChoreViewController?.delegate = self
            tabBarController?.present(removeChoreViewController!, animated: true, completion: nil)
        }
    }
    
    @objc func getpersonalchallengesInfoFromHealthKit()  {
        //self.setupforHealthKitData()
        getPersonalChallenges(isfromRefresh: true)
        if modeofChallenge == 2 {
            HomeViewController().healthService.getCyclingDistance { (distance, error) in
                DispatchQueue.main.async {
                    
                    let stringValue = String(format: "%.2f", distance ?? 0.0)
                    print("cycling distance \(stringValue)")
                    self.progressCount = Float(distance ?? 0)
                    //  self.KmsLabel.text = stringValue
                }
            }
        }else  if modeofChallenge == 4 {
            HomeViewController().healthService.getswmmingDistance { (distance, error) in
                
                let stringValue = String(format: "%.2f", distance ?? 0.0)
                print("cycling distance \(stringValue)")
                self.progressCount = Float(distance ?? 0)
            }
        }
        //setupPedometerUpdates()
    }
    
    
    private func startChallenge(challenge: PersonalChallenge?) {
        SVProgressHUD.show()
        if startedChallenge != nil {
            //Stop the currently active challenge
            changeChallengeStatus(challenge: startedChallenge, status: .stopped) { (result) in
                switch result {
                    
                case .success(_):
                    self.changeChallengeStatus(challenge: challenge, status: .started) { (result) in
                        switch result {
                            
                        case .success(_):
                            SVProgressHUD.dismiss()
                            self.chatTableView.reloadData()
                            if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                                print(startedChallenge)
                                self.progressCount = startedChallenge.data.challengeRunStep ?? 0
                            }
                            
                            let challengeId = self.startedChallenge?.data.challengeId
                            let guid =  UserDefaultConstants().guid ?? ""
                            UserDefaults.standard.set(guid, forKey: "current_userGuid")
                            UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                            
                            self.getCurrentTimeAndDate()
                            self.timerMethod()
                            self.setupforHealthKitData()
//                            self.setupPedometerUpdates()
                        case .failure(_):
                            print("error")
                        }
                    }
                case .failure(_):
                    SVProgressHUD.dismiss()
                }
            }
            
        } else {
            changeChallengeStatus(challenge: challenge, status: .started) { (result) in
                switch result {
                    
                case .success(_):
                    SVProgressHUD.dismiss()
                    self.chatTableView.reloadData()
                    if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                        print(startedChallenge)
                        self.progressCount = startedChallenge.data.challengeRunStep ?? 0
                    }
                    let challengeId = self.startedChallenge?.data.challengeId
                    let guid =  UserDefaultConstants().guid ?? ""
                    UserDefaults.standard.set(guid, forKey: "current_userGuid")
                    UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                    
                    self.getCurrentTimeAndDate()
                    self.timerMethod()
                    self.setupforHealthKitData()
//                    self.setupPedometerUpdates()
                case .failure(_):
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    private func changeChallengeStatus(challenge: PersonalChallenge?, status: ChallengeStatus, handler: @escaping (Swift.Result<Any, NSError>) -> Void) {
        PersonalFitnessManager.sharedInstance.changePersonalChallengeStatus(challenge: challenge, status: status) { (result) in
            switch result {
                
            case .success(let response):
                handler(.success(response))
            case .failure(let error):
                handler(.failure(error as NSError))
            }
        }
    }
    
    private func setupPedometerUpdates() {
        
    }
}


extension SpendTabViewController: ChallengeTableViewCellDelegate {
    func didTapChallengeTableViewCellStartButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        
        if fromController == "dailytableview"{
            print(onboardingChallenges.count,dailyChallenges.count)
//              if let onboardingChallenge = challengeTableViewCell.onboardingChallenge,onboardingChallenges.count > 0 {
//      //            presentAcceptOnboardingChallengeScreen(onboardingChallenge: onboardingChallenge)
//                  let challengeDictionary = onboardingChallenge.toJSON()
//
//                  if challengeDictionary["cType"] as! String == "1"{
//                     letsDoitFlowForOnboarding(onboardingChallange: onboardingChallenge, challenge: challengeDictionary)
//                  }else if challengeDictionary["cType"] as! String == "2" {
//                      isSecondOnboardingChallange = true
//
//                      let gemsViewController = GlobalStoryBoard().CouponList
//      //                gemsViewController.delegate = self
//                      gemsViewController.isfromSecondChallange = true
//                      present(gemsViewController, animated: true, completion: nil)
//                  }
//
//              }else
              
               if var dailyChallenge = challengeTableViewCell.dailyChallenge {
                  dailyChallenge.taskCurrentValue = 1
                  if dailyChallenge.challengeType == 1 && dailyChallenge.challegeTaskType == 1 { //Walk Challange
                      completedChallangeFootSteps = 0
                      getFootCountUpdates.updateFootCount(whenSteps: Int(dailyChallenge.taskValue)!)
                  }
                  updateDailyChallenge(challenge: dailyChallenge, status: .accepted) { (result) in
                      self.dailyChallengeTableview.reloadData()
                  }
              }
        }else{
            
            if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                if startedChallenge.data.isCompleted == 0{
                    if let indexPath = chatTableView.indexPath(for: challengeTableViewCell)  {
                        presentRestartChallengeViewController(personalChallenge: challengeTableViewCell.personalChallenge! ,indexPath: indexPath)
                    }

    //            self.showAlertView(challenge: challengeTableViewCell.personalChallenge)
                }else {
                    self.stepCountTimer.invalidate()
                    self.removeUserDefaults()
    //                self.oldStepCountProgress = false
                    let challengeId = challengeTableViewCell.personalChallenge?.data.challengeId ?? ""

                    UserDefaults.standard.set(true, forKey: "newChallenge_Created")
                    UserDefaults.standard.set(challengeId, forKey: "challenge_id")

                    self.startChallenge(challenge: challengeTableViewCell.personalChallenge)
                }
            } else {
                switch challengeTableViewCell.personalChallenge?.data.challengeType {

                    case .walk:
                        BaseViewController().locationmanage.stopUpdatingLocation()
                        modeofChallenge  = 0
                        break;
                    case .swim:
                        if isPairedWithWatch {
                            //BaseViewController().locationmanage.stopUpdatingLocation()
                                AlertView().showAlertwithTwoActionWithSwimType(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeSwimmingDescription)
                        }else {
                            //gps enable and track the location
                            modeofChallenge = 3
                            BaseViewController().locationPermissionRequest()
                        }
                        break;
                    case .run:
                        BaseViewController().locationmanage.stopUpdatingLocation()
                        modeofChallenge  = 0
                        break;
                    case .bike:
                        if isPairedWithWatch {
                            BaseViewController().locationmanage.stopUpdatingLocation()
                                AlertView().showAlertwithTwoAction(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeDescription)
                        }else {
                            //gps enable and track the location
                            modeofChallenge = 1
                            BaseViewController().locationPermissionRequest()
                        }
                        break;
                    case .none:
                        break;
                }
                self.stepCountTimer.invalidate()
                self.removeUserDefaults()
    //            self.oldStepCountProgress = false
                let challengeId = challengeTableViewCell.personalChallenge?.data.challengeId ?? ""

                UserDefaults.standard.set(true, forKey: "newChallenge_Created")
                UserDefaults.standard.set(challengeId, forKey: "challenge_id")

                self.startChallenge(challenge: challengeTableViewCell.personalChallenge)
            }

        }
        
    }

    func didTapChallengeTableViewCellNotAprovedButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        
        if fromController == "dailytableview"{
            
        }else{
            
            if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
    //            challengeDetailsContainerView.isHidden = false

                challengeDetailsViewController?.personalChallenge = challengeTableViewCell.personalChallenge!
        }
        
        }
    }

    func didTapChallengeTableViewCellWhatsNextButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        
        if fromController == "dailytableview"{
            //Show Complete Daily Challenge Screen
            
            let userDefaultsCompledailytChallengeComplete = UserDefaults.standard
            userDefaultsCompledailytChallengeComplete.set("dailyChallengCompleted", forKey: "completeDailyChallengeChatboat")
            
            if let dailyChallenge = challengeTableViewCell.dailyChallenge, let indexPath = dailyChallengeTableview.indexPath(for: challengeTableViewCell)  {
                
                let userDefaultsCompledailytChallengeCompleteexperianceLabel = UserDefaults.standard
                userDefaultsCompledailytChallengeCompleteexperianceLabel.set(dailyChallenge.xp , forKey: "completeDailyChallengeChatboatexperianceLabel")
                let userDefaultsCompledailytChallengeCompleteharmoneyBucksLabel = UserDefaults.standard
                userDefaultsCompledailytChallengeCompleteharmoneyBucksLabel.set(dailyChallenge.hBucks , forKey: "CompledailytChallengeChatboatharmoneyBucksLabel")
                let userDefaultsCompledailytChallengeCompletediamondLabel = UserDefaults.standard
                userDefaultsCompledailytChallengeCompletediamondLabel.set(dailyChallenge.gem , forKey: "completeDailyChallengeChatboatdiamondLabel")
                let userDefaultsCompledailytChallengeCompletechallengeTitleLabel = UserDefaults.standard
                userDefaultsCompledailytChallengeCompletechallengeTitleLabel.set(dailyChallenge.title , forKey: "completeDailyChallengeChatboatchallengeTitleLabel")
                let userDefaultsCompledailytChallengeCompletetaskLabel = UserDefaults.standard
                userDefaultsCompledailytChallengeCompletetaskLabel.set(dailyChallenge.subTitle , forKey: "completeDailyChallengeChatboattaskLabel")
              
              
                self.presentCompletedChallengeViewControllerForDailyChallenge(dailyChallenge: dailyChallenge, indexPath: indexPath)
             //Show Complete Onboarding Challenge Screen
            }
//            else if let onboardingChallenge = challengeTableViewCell.onboardingChallenge, let indexPath = challengeTableView.indexPath(for: challengeTableViewCell) {
//                presentCompletedChallengeViewControllerForOnboardingChallenge(onboardinChallenge: onboardingChallenge, indexPath: indexPath)
//            }
        }else{
            
             if let personalChallenge = challengeTableViewCell.personalChallenge, let indexPath = chatTableView.indexPath(for: challengeTableViewCell)  {
                self.reloadPedometerUpdates = false
                let userDefaultsCompletChallengeComplete = UserDefaults.standard
                userDefaultsCompletChallengeComplete.set("completed", forKey: "completeFitnessChatboat")
                      
                      let userDefaultsCompletChallenge = UserDefaults.standard
                      if personalChallenge.data.challengeType == Harmoney.ChallengeType.walk
                      {
                          userDefaultsCompletChallenge.set("walk", forKey: "challengeType")
                      }else if personalChallenge.data.challengeType == Harmoney.ChallengeType.bike{
                        userDefaultsCompletChallenge.set("bike", forKey: "challengeType")
                      }else if personalChallenge.data.challengeType == Harmoney.ChallengeType.swim{
                        userDefaultsCompletChallenge.set("swim", forKey: "challengeType")
                      }else if personalChallenge.data.challengeType == Harmoney.ChallengeType.run{
                        userDefaultsCompletChallenge.set("run", forKey: "challengeType")
                      }
                      let userDefaultsCompletexperiance = UserDefaults.standard
                      userDefaultsCompletexperiance.set(personalChallenge.data.experiance, forKey: "experiance")
                      let userDefaultschallengeTask = UserDefaults.standard
                      userDefaultschallengeTask.set(personalChallenge.data.challengeTask, forKey: "challengeTask")
                      let userDefaultsCompletChallengeharmoneyBucks = UserDefaults.standard
                      userDefaultsCompletChallengeharmoneyBucks.set(personalChallenge.data.harmoneyBucks, forKey: "harmoneyBucks")
                      let userDefaultsCompletChallengediamond = UserDefaults.standard
                      userDefaultsCompletChallengediamond.set(personalChallenge.data.diamond, forKey: "diamond")
                self.presentCompletedChallengeViewController(personalChallenge: personalChallenge, indexPath: indexPath)
            }
        }
        
    }

    func didTapChallengeTableViewCellRemoveButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        
        if fromController == "dailytableview"{
            
            if var dailyChallenge = challengeTableViewCell.dailyChallenge {
                HarmonySingleton.shared.taskTitle = "Daily"
                self.presentDeleteChallengeViewControllerDailly(personalChallenge: dailyChallenge)
            }
            
        }else{
            if let indexPath = chatTableView.indexPath(for: challengeTableViewCell)  {
                presentDeleteChallengeViewController(personalChallenge: challengeTableViewCell.personalChallenge! ,indexPath: indexPath)

            }
        }
        
       
    }

    func didTapChallengeTableViewCellCustomButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
//        if let indexPath = challengeTableVIew.indexPath(for: challengeTableViewCell)  {
//            presentDeleteChallengeViewController(personalChallenge: challengeTableViewCell.personalChallenge! ,indexPath: indexPath)
//            
//        }
        if fromController == "dailytableview"{
            
        }else{
            presentCustomAlertViewController(isMessage: fromController)
            
        }

    }
    
    func didTapChallengeTableViewCellVideoButton(challengeTableViewCell: ChallengeTableViewCell,fromController: String) {
        if fromController == "dailytableview"{
//            if let dailyChallenge = challengeTableViewCell.dailyChallenge {
//                presentVideoPlayerControllerForDailyChallenge(dailyChallenge: dailyChallenge)
//            } else if let onboardingChallenge = challengeTableViewCell.onboardingChallenge {
//                presentVideoPlayerControllerForOnboardingChallenge(onboardingChallenge: onboardingChallenge)
//            }
        }else{
            
        }
    }
}

//MARK: - ChallengeActionsViewControllerDelegate Methods
//
//extension SpendTabViewController: ChallengeActionsViewControllerDelegate {
//    func challengeActionsViewControllerDidTapWalk(_ challengeActionsViewController: ChallengeActionsViewController) {
//        reloadPedometerUpdates = false
//        addChallengeContainerView.isHidden = false
//
//        let walkData = PersonalChallengeData(challengeId: "", challengeType: .walk, challengeTask: 2000, experiance: 200, harmoneyBucks: 0, diamond: 1, isCompleted: 0, challengeProgress: 0, aprovedId: "", createdDate: "", isClaimed: 0, challengeAcceptType: .padding)
//
//        let walkChallenge = PersonalChallenge(data: walkData)
//
//        addChallengeViewController?.personalChallenge = walkChallenge
//    }
//
//    func challengeActionsViewControllerDidTapRun(_ challengeActionsViewController: ChallengeActionsViewController) {
//        reloadPedometerUpdates = false
//        addChallengeContainerView.isHidden = false
//
//        let runData = PersonalChallengeData(challengeId: "", challengeType: .run, challengeTask: 2, experiance: 200, harmoneyBucks: 0, diamond: 1, isCompleted: 0, challengeProgress: 0, aprovedId: "", createdDate: "", isClaimed: 0, challengeAcceptType: .padding)
//
//        let runChallenge = PersonalChallenge(data: runData)
//
//        addChallengeViewController?.personalChallenge = runChallenge
//    }
//
//    func challengeActionsViewControllerDidTapSwim(_ challengeActionsViewController: ChallengeActionsViewController) {
//        reloadPedometerUpdates = false
//        addChallengeContainerView.isHidden = false
//
//        let swimData = PersonalChallengeData(challengeId: "", challengeType: .swim, challengeTask: 2, experiance: 200, harmoneyBucks: 0, diamond: 1, isCompleted: 0, challengeProgress: 0, aprovedId: "", createdDate: "", isClaimed: 0, challengeAcceptType: .padding)
//
//        let swimChallenge = PersonalChallenge(data: swimData)
//
//        addChallengeViewController?.personalChallenge = swimChallenge
//    }
//
//    func challengeActionsViewControllerDidTapBike(_ challengeActionsViewController: ChallengeActionsViewController) {
//        reloadPedometerUpdates = false
//        addChallengeContainerView.isHidden = false
//
//        let bikeData = PersonalChallengeData(challengeId: "", challengeType: .bike, challengeTask: 2, experiance: 200, harmoneyBucks: 0, diamond: 1, isCompleted: 0, challengeProgress: 0, aprovedId: "", createdDate: "", isClaimed: 0, challengeAcceptType: .padding)
//
//        let bikeChallenge = PersonalChallenge(data: bikeData)
//
//        addChallengeViewController?.personalChallenge = bikeChallenge
//    }
//}

//MARK: - AddChallengeViewControllerDelegate Methods

extension SpendTabViewController: AddChallengeViewControllerDelegate {
    func didTapInviteParentButton(addChallengeViewController: AddChallengeViewController) {
        if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue == RegistrationType.child.rawValue {
            let profileNavigationViewController = tabBarController?.viewControllers?[3] as! UINavigationController
            let profileViewController = profileNavigationViewController.viewControllers[0] as! ProfileTabViewController
            profileViewController.inviteState = (true, .invite)
            
            tabBarController?.selectedIndex = 3
        } else {
            tabBarController?.selectedIndex = 3
        }
    }
    
    func didTapCloseAddChallenge(addChallengeViewController: AddChallengeViewController) {
        reloadPedometerUpdates = true
//        addChallengeContainerView.isHidden = true
    }
    
    func didTapAddChallenge(addChallengeViewController: AddChallengeViewController, personalChallenge: PersonalChallenge) {
        configureNoChallengeUI()
        self.view .endEditing(true)
        UIView.animate(withDuration: 1) {
            self.chatTableView.performBatchUpdates({
                self.chatTableView.insertRows(at: [IndexPath(row: self.personalChallengeList.count - 1, section: 0)], with: .right)
            }) { (finished) in
                self.chatTableView.setContentOffset(.zero, animated: true)
                self.reloadPedometerUpdates = true
            }
        }
    }
}

//MARK: CompletedChallengeViewControllerDelegate Methods

extension SpendTabViewController: CompletedChallengeViewControllerDelegate {
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, personalChallenge: PersonalChallenge) {
        
        PersonalFitnessManager.sharedInstance.completePersonalChallenge(personalChallenge: personalChallenge, userLevel: completedChallengeViewController.levelDiference) { (result) in
                 switch result {
                     case .success( _):
                       completedChallengeViewController.dismiss(animated: true) {
                        
                           UIView.animate(withDuration: 1) {
                               if let indexPath = completedChallengeViewController.indexPath {
                                updateGoals(goalType: .fitnessChallenge){ (countValue) in
                                    self.setGoalViewBasedOnGoalCount(cnt: countValue)
                                }
                                
                                self.reloadPedometerUpdates = true
//                                  self.removeTableViewCell(indexPathToRemove: indexPath)
                                self.chatTableView.reloadData()
                                self.getPersonalChallenges(isfromRefresh: false)

                                
                               }
                           }
                       }

                    HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
                     case .failure(let error):
                        print("error")
                 }
            }
    }
    
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, dailyChallenge: DailyChallenge) {
        updateDailyChallenge(challenge: dailyChallenge, status: .rewards) { (result) in
            switch result {
            case .success(_):
                updateGoals(goalType: .dailyChallenge){ (countValue) in
                    self.setGoalViewBasedOnGoalCount(cnt: countValue)
                }
                UserDefaults.standard.removeObject(forKey: "CurrentDate")
                UserDefaults.standard.removeObject(forKey: "ChallangeSteps")
                UserDefaults.standard.removeObject(forKey: "InitalChallangeSteps")
                PersonalFitnessManager.sharedInstance.checkActiveChallenge = true
                HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
            case .failure(_):
                break
            }
        }

    }
    
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, onboardingChallenge: ChallengeData) {}
    func didTapNextButtonLevelupPopup(completedChallengeViewController: CompletedChallengeViewController){
        let levelInfo = self.levelsModel?.data.levelInfo[1]
        self.presentClaimLevelRewardViewController(levelInfo: levelInfo!)
    }
    func presentClaimLevelRewardViewController(levelInfo: LevelInfo) {
        let claimLevelRewardViewController = GlobalStoryBoard().claimLevelRewardVC
        claimLevelRewardViewController.levelInfo = levelInfo
        claimLevelRewardViewController.isFromOnboarding = true
        claimLevelRewardViewController.delegate = self
        present(claimLevelRewardViewController, animated: true, completion: nil)
    }
}
extension SpendTabViewController : CollectLevelRewardsDelegate{
    func leveleRegardsCollected(){
        let levelsViewController = GlobalStoryBoard().levelsVC
        levelsViewController.isFromOnboarding = true
        LevelsManager.sharedInstance.levelRewardClaimed = true
        present(levelsViewController, animated: true, completion: nil)
    }
}
//MARK: - DeleteChallangeViewControllerDelegate Methods

extension SpendTabViewController: DeleteChallangeViewControllerDelegate {
    func didTapThumbsUpButtonOnPersonalChallenge(deleteChallangeViewController: DeleteChallangeViewController, challenge: PersonalChallenge) {
        
    }
    
    func didTapThumbsUpButtonChallengeDeleteDaillyChalleng(deleteChallangeViewController: DeleteChallangeViewController, daillyChallenge: DailyChallenge?) {
        
    }
    
    func didTapThumbsUpButtonChallengeDeleteOnboarding(deleteChallangeViewController: DeleteChallangeViewController, onboarding: ChallengeData) {
        
    }
    
    func didTapThumbsDownButton(deleteChallangeViewController: DeleteChallangeViewController) {
        deleteChallangeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapThumbsUpButton(deleteChallangeViewController: DeleteChallangeViewController,challenge:PersonalChallenge) {
        SVProgressHUD.show()
            deleteChallangeViewController.dismiss(animated: true) {
                UIView.animate(withDuration: 0.5) {
                    if let indexPath = deleteChallangeViewController.indexPath {
                        self.reloadPedometerUpdates = false
                        let currentActiveChallengeId = self.activeChallenge?.data.challengeId
                        if currentActiveChallengeId == challenge.data.challengeId{
                            self.stepCountTimer .invalidate()
                        }
                        PersonalFitnessManager.sharedInstance.deletePersonalChallenge(personalChallenge: deleteChallangeViewController.personalChallenge!) { (result) in
                            SVProgressHUD.dismiss()
                            switch result {
                            case .success(_):
                                self.removeTableViewCell(indexPathToRemove: indexPath)
                                
                                if currentActiveChallengeId == deleteChallangeViewController.personalChallenge?.data.challengeId {
                                    self.setupPedometerUpdates()
                                }
                                self.getDailyChallenges()
                            case .failure(let error):
                                self.reloadPedometerUpdates = true
                            }
                        }
                    }
                }
            }
    }
    
}


//MARK: - RestartChallangeViewControllerDelegate Methods

extension SpendTabViewController: RestartChallangeViewControllerDelegate {
    func didTapThumbsDownButton(restartChallangeViewController: RestartChallageViewController) {
        restartChallangeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapThumbsUpButton(restartChallangeViewController: RestartChallageViewController,challenge:PersonalChallenge) {
        
        restartChallangeViewController.dismiss(animated: true) {
            UIView.animate(withDuration: 1) {
                let challengeId = challenge.data.challengeId
                
                self.startChallenge(challenge: challenge)
                
                UserDefaults.standard.set(true, forKey: "newChallenge_Created")
                UserDefaults.standard.set(challengeId, forKey: "challenge_id")
                
                self.stepCountTimer.invalidate()
                self.removeUserDefaults()
    //            self.oldStepCountProgress = false
                if let startedChallenge = PersonalFitnessManager.sharedInstance.getStartedChallenge() {
                    if startedChallenge.data.isCompleted == 0{
                    PersonalFitnessManager.sharedInstance.updateActiveChallengeProgress(progress: Float(0)) { (result) in
                        switch result {
                        case .success(let newActiveChallenge):
                            travelleddistance = 0
                            if challenge.data.challengeType.rawValue == "Bike"{
                                if isPairedWithWatch {
                                    BaseViewController().locationmanage.stopUpdatingLocation()
                                        AlertView().showAlertwithTwoAction(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeDescription)
                                }else {
                                    //gps enable and track the location
                                    modeofChallenge = 1
                                    BaseViewController().locationPermissionRequest()
                                    BaseViewController().locationmanage.startUpdatingLocation()
                                }
                            }else if challenge.data.challengeType.rawValue == "Swim"{
                                if isPairedWithWatch {
                                    AlertView().showAlertwithTwoActionWithSwimType(view: self, title: notifications.challengeAcceptingType, description: notifications.challengeSwimmingDescription)
                                }else {
                                    //gps enable and track the location
                                    modeofChallenge = 3
                                    BaseViewController().locationPermissionRequest()
                                }
                            } else {
                                BaseViewController().locationmanage.stopUpdatingLocation()
                                modeofChallenge  = 0
                            }
                                    if self.reloadPedometerUpdates {
                                        self.chatTableView.reloadData()
                                    }
                        case .failure(let error):
                            print(error)
                        }
                    }
                } else {
                    
                }
            }
            }
        }
            
    }
}

//MARK: - ChallengeDetailsViewControllerDelegate Methods

extension SpendTabViewController: ChallengeDetailsViewControllerDelegate {
    func didTapCloseButton(challengeDetailsViewController: ChallengeDetailsViewController) {
//        challengeDetailsContainerView.isHidden = true
//        self.stepCountTimer.invalidate()
    }
}




extension SpendTabViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool { // return NO to disallow editing.
        print("textFieldShouldBeginEditing")
        answerLbl.isHidden = false
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) { // became first responder
        print("textFieldDidBeginEditing")
        if !(textField.text?.isEmpty ?? false){
            

           } else {
            
           }

    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        print("textFieldShouldEndEditing")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)  {// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("textFieldDidEndEditing")

    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {// if implemented, called in place of textFieldDidEndEditing:
        print("textFieldDidEndEditing")

        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {// return NO to not change text
   
        if textField == nightReplyTextField{
            let currentString: NSString = (nightReplyTextField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
                nightReplySendBtn.isEnabled = false
                if (newString.length >= 30){
                    nightReplySendBtn.isEnabled = true
                }
        }else if textField == txtfield{
            let currentString: NSString = (txtfield.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            textBtn.isEnabled = false
                if (newString.length >= 30){
                    textBtn.isEnabled = true
                }
        }
   
        if textField == txtfield
        {
            if (range.location == 0 && string == " ") {
                       return false
                   }
         
            let oldStr = txtfield.text! as NSString
            let newStr = oldStr.replacingCharacters(in: range, with: string) as NSString
            if newStr.length == 0
            {
                RecordBtn.isHidden = false
                textBtn.isHidden = true
            }else
            {
                RecordBtn.isHidden = true
                textBtn.isHidden = false
                let maxLength = 500
                  let currentString: NSString = (txtfield.text ?? "") as NSString
                  let newString: NSString =
                      currentString.replacingCharacters(in: range, with: string) as NSString
                  return newString.length <= maxLength
            }
       
           
        }
      
        
        
        if textField == nightReplyTextField
        {
            if (range.location == 0 && string == " ") {
                       return false
                   }
         
            let oldStr = nightReplyTextField.text! as NSString
            let newStr = oldStr.replacingCharacters(in: range, with: string) as NSString
            if newStr.length == 0
            {
                nightReplyMicBtn.isHidden = false
                nightReplySendBtn.isHidden = true
            }else
            {
                nightReplyMicBtn.isHidden = true
                nightReplySendBtn.isHidden = false
                let maxLength = 500
                  let currentString: NSString = (nightReplyTextField.text ?? "") as NSString
                  let newString: NSString =
                      currentString.replacingCharacters(in: range, with: string) as NSString
                  return newString.length <= maxLength
            }
        }
        

        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {// called when clear button pressed. return NO to ignore (no notifications)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {// called when 'return' key pressed. return NO to ignore.
         textField.resignFirstResponder()
        return true
    }
}


extension SpendTabViewController {
    func okayButton(sender:UIButton) {
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        
            let indexData = ChoresListModel?.data![sender.tag]
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        print("clicked on tag \(sender.tag), Chore Name : \(indexData?.task_name)\(indexData?.chorse_id)")
            
        guard let dayObj = indexData?.taskList?.filter({$0.date == formatter.string(from: Date())}) else{
            return
        }

        var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            data.updateValue(indexData?.chorse_id ?? "1", forKey: "chorse_id")
            data.updateValue((dayObj.last?.day) ?? "1", forKey: "taskDay")
            
        let (url, method, param) = APIHandler().dailyCompleteChore(params: data)
            
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                self.showchoreDonePopUp(sender: sender)
                    switch response.result {
                               case .success(let value):
                                if let value = value as? [String:Any] {
                                    if let message = value["message"] as? String{
                                  }
                                }
                                   break
                               case .failure(let error):

                                   break
                               }
            }
        }


    }
}


extension SpendTabViewController: RemoveChoreViewControllerDelegate {
    
    func didTapThumbsDownButton(removeChoreViewController: RemoveChoreViewController ) {
        
        removeChoreViewController.dismiss(animated: true, completion: nil)

        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        
        let data = [String : Any]()
            
            let indexData = ChoresListModel?.data![selectedTag]

        var (url, method, param) = APIHandler().deleteChore(params: data)
            url = url + (indexData?.chorse_id ?? "1")
            
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                               case .success(let value):
                                
                                if let value = value as? [String:Any] {
                                    self.ChoresListModel?.data?.remove(at: self.selectedTag)
                                    self.getAllSubCategory()
                                    if let message = value["message"] as? String{
                                        
                                  }
                                }
                                   break
                               case .failure(let error):

                                   break
                               }
            }
        }

    }
    
    func didTapThumbsUpButton(removeChoreViewController: RemoveChoreViewController) {
        removeChoreViewController.dismiss(animated: true, completion: nil)

    }
    
    }
    
extension SpendTabViewController : CompletedChallengeViewControllerDelegateForChore{
    func didTapNextButton(completedChallengeViewController: CompletedChallengeViewController, chore: ChoresModel) {
        clollectChoreReward(chore: chore)
    }

    
    func clollectChoreReward(chore:ChoresModel) {
        SVProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            data.updateValue(chore.chorse_id ?? "1", forKey: "chorse_id")

        let (url, method, param) = APIHandler().completeChoreEarnPoints(params: data)
            
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                               case .success(let value):
                                if let value = value as? [String:Any] {
                                    if let message = value["message"] as? String{
                                        HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
                                        self.getAllSubCategory()
                                  }
                                }
                                   break
                               case .failure(let error):
                                   break
                               }
            }
        }
    }
}
extension SpendTabViewController : DoneChoreViewControllerDelegate{
    func okayButton(doneChoreViewController: DoneChoreViewController) {
        DispatchQueue.main.async {
            self.getAllSubCategory()
        }
    }
    
    
}


//MARK: CustomAlertViewControllerDelegate Methods
extension SpendTabViewController: CustomAlertViewControllerDelegate {
    func didTapOkayButton(customAlertViewController: CustomAlertViewController) {
        customAlertViewController.dismiss(animated: true, completion: nil)

    }
}

// MARK: - CustomNavigationBarDelegate methods

extension SpendTabViewController: CustomNavigationBarDelegate {
    func customNavigationBarDidTapLeftButton(_ navigationBar: CustomNavigationBar) {
//        dismiss(animated: true, completion: nil)
//        self.tabBarController?.selectedIndex = 2
//        
//        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        if HarmonySingleton.previousVC == .sideMenu{
            let vc = GlobalStoryBoard().localWebVC
            vc.push = true
            vc.titletext = "About Harmoney"
            vc.websiteUrl = "http://harmoney.ai/app-pages/about-harmoney.html"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if HarmonySingleton.previousVC == .earn{
            self.tabBarController?.selectedIndex = 1
        }else if HarmonySingleton.previousVC == .home{
            self.tabBarController?.selectedIndex = 0
        }else if HarmonySingleton.previousVC == .profile{
            self.tabBarController?.selectedIndex = 3
        }else if HarmonySingleton.previousVC == .myStuf{
            self.tabBarController?.selectedIndex = 2
        }
    }
    
    func customNavigationBarDidTapRightButton(_ navigationBar: CustomNavigationBar) {
//        clearALlNotifications()
    }
}


extension NSNumber {
    func getPercentage() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 0 // You can set what you want
        return formatter.string(from: self)!
    }
}


class MyValueFormatter: IValueFormatter {
    var xValueForToday: Double?  // Set a value

    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        if entry.x == xValueForToday {
            return "Today"
        } else {
            let percentageValue = value as NSNumber
            var percentageString = percentageValue.getPercentage()
            return percentageString
        }
    }
}


class LeftTriangleView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }

        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        context.closePath()

        context.setFillColor(UIColor.gray.cgColor)
        context.fillPath()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



class RightTriangleView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }

        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        context.closePath()


        context.setFillColor(UIColor.blue.cgColor)
        context.fillPath()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension UIScrollView {
    func scrollsToBottom(animated: Bool) {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height)
        setContentOffset(bottomOffset, animated: animated)
    }
}
extension SpendTabViewController{
    private func setupPedometerUpdatesNew() {
        let progress = completedChallangeFootSteps
        if ((self.ch1Obj?.cStatus == 0 && self.ch1Obj?.cId == "1") || (self.activeOnboardingWalkChallenge != nil) && onboardingChallenges.count > 0){
            DailyChallengeManager.sharedInstance.updateActiveOnboardingChallengeProgress(progress: progress) { (result) in
                    switch result {
                    case .success(let _):
                        self.dailyChallengeTableview.reloadData()
                    case .failure(let error):
                        print(error)
                    }
            }
        }else if dailyChallenges.count > 0/*self.activeDailyWalkChallenge != nil*/ {
                DailyChallengeManager.sharedInstance.updateActiveDailyChallengeProgress(progress: progress) { (result) in
                    switch result {
                             
                    case .success(let newActiveDailyChallenge):
                        self.dailyChallengeTableview.reloadData()
                    case .failure(let error):
                        print(error)
                    }
            }
        }
    }
}
extension SpendTabViewController : FootStepsDelegates{
    //This Method calls on Final Update
    func footStepsReading(footSteps: Int) {
        completedChallangeFootSteps = footSteps
        setupPedometerUpdatesNew()
        DispatchQueue.main.async {
            self.dailyChallengeTableview.reloadData()
        }
    }
    //This Method calls on every stepcount
    func footStepsContinuousUpdate(footSteps: Int) {
        if onboardingChallenges.count > 0 || dailyChallenges.count > 0
        {
        completedChallangeFootSteps = footSteps
        setupPedometerUpdatesNew()
            DispatchQueue.main.async {
                self.dailyChallengeTableview.reloadData()
            }
        }
    }
}
extension SpendTabViewController{
    private func startTimer(){
        stopTimer()
        //foregroundCall
        timerSteps = Timer.scheduledTimer(timeInterval: 10,
                                   target: self,
                                 selector: #selector(self.updateNewFuction),
                                 userInfo: nil,
                                  repeats: true)
      
    }
    private func stopTimer(){
        if timerSteps != nil{
            timerSteps?.invalidate()
            timerSteps = nil
        }
    }
    
    @objc func updateNewFuction(){
        self.healthService.getTodaysStepsCollectionCumulitive { (stepCount, error) in
            DispatchQueue.main.async {
                let stringValue = String(format: "%.0f", stepCount ?? 0)
                if let intStepsCount = Int(stringValue){
                    UserDefaults.standard.setValue(intStepsCount, forKey: "InitalChallangeSteps")
                    self.footStepsReading(footSteps: intStepsCount)
                }
            }
    }
    }
}

