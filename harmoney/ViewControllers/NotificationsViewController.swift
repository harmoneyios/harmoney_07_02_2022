//
//  NotificationsViewController.swift
//  Harmoney
//
//  Created by Dineshkumar kothuri on 27/05/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Toast_Swift
import SVProgressHUD
import ObjectMapper
import IQKeyboardManagerSwift

enum NotificationScreens:Int {
    case inviteParent = 10
    case acceptChore = 11
}

class NotificationsViewController: UIViewController {
    
    let notificationsTableViewCellIdentifier = "NotificationTableViewCell"
    
    let tableViewHeaderHeight: CGFloat = 40.0
    let tableViewHeaderX: CGFloat = 20.0
    
    var childInvitePopUp : ChildInviteParentRequestPopUp?
    var alreadyAccept : Bool = false
    
    @IBOutlet weak var notificationsTableView: UITableView!
    @IBOutlet weak var emptyNotificationsView: UIView!
    @IBOutlet weak var navigationBarView: CustomNavigationBar!
    var getGuidForMobileNumber : guidForMobileNumberObject?
    var transfertoUserObjectValue:  transfertoUserObject?
    var getWalletObj : getwalletObject?
    var addHarmoneyWalletValue : addHarmoneyWalletDataObject?
    let refresh = UIRefreshControl()

    var notifications: [NotificationDash] {
        return NotificationsManager.sharedInstance.notifications
    }
    var  thumbsAlertViewController: ThumbsAlertViewController?
    var approveChallengeViewController: ApproveChallengeViewController?
    var customAlertViewController: CustomAlertViewController?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        getAllNotifications()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
    }

    //MARK: - UI methods
    
    private func configureUI() {
        notificationsTableView.register(UINib(nibName: notificationsTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: notificationsTableViewCellIdentifier)
        notificationsTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        notificationsTableView.refreshControl = refresh
        refresh.addTarget(self, action: #selector(getNotificationList), for: .valueChanged)
        
        navigationBarView.delegate = self
        navigationBarView.title = "Notifications"
        navigationBarView.rightButtonTitle = "Clear all"
    }
    @objc func getNotificationList()  {
        getAllNotifications()
    }
    private func setupNotificationsUI() {
        if notifications.count > 0 {
            notificationsTableView.isHidden = false
            notificationsTableView.reloadData()
        } else {
            notificationsTableView.isHidden = true
            emptyNotificationsView.isHidden = false
        }
    }
    
    func presentThumbsAlertViewController(alertTitle: String) {
        thumbsAlertViewController = GlobalStoryBoard().thumbsAlertVC
        thumbsAlertViewController?.alertTitle = alertTitle
        thumbsAlertViewController?.delegate = self
        present(thumbsAlertViewController!, animated: true, completion: nil)
    }
    
    func presentApproveChallengeViewController(personalChallenge: PersonalChallenge, kidName: String) {
        approveChallengeViewController = GlobalStoryBoard().approveChallengeVC
        approveChallengeViewController?.personalChallenge = personalChallenge
        approveChallengeViewController?.kidName = kidName
        approveChallengeViewController?.delegate = self

        present(approveChallengeViewController!, animated: true, completion: nil)
    }
    
    func presentApproveLevelUpCustomRewardViewController(invitedRequest: InvitedRequest) {
        approveChallengeViewController = GlobalStoryBoard().approveChallengeVC
        approveChallengeViewController?.invitedRequest = invitedRequest
        approveChallengeViewController?.delegate = self

        present(approveChallengeViewController!, animated: true, completion: nil)
    }
    
    func presentCustomAlertViewController(alertMessage: String, alertTitle: String) {
        customAlertViewController = GlobalStoryBoard().customAlertVC
        customAlertViewController?.alertTitle = alertTitle
        customAlertViewController?.alertMessage = alertMessage
        customAlertViewController?.delegate = self
        present(customAlertViewController!, animated: true, completion: nil)
    }
    
    func refreshNotification() {
        
        self.getAllNotifications()
    }
    
    //MARK: - Private methods
    private func readAllNotifications(){
        NotificationsManager.sharedInstance.markNotificationsRead { (result) in
            switch result {
            case .success(_):
                print("Success")
            case .failure(_):
                print("Failed")
            }
        }
    }
    private func getAllNotifications() {
        NotificationsManager.sharedInstance.getAllNotifications { (result) in
            DispatchQueue.main.async {
                switch result {
                    case .success(_):
                        self.refresh.endRefreshing()
                        self.setupNotificationsUI()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.readAllNotifications()
                        }
                    case .failure(let error):
                        self.refresh.endRefreshing()
                        self.view.toast(error.localizedDescription)
                }
            }
        }
    }
    
    private func clearALlNotifications() {
        guard notifications.count > 0 else { return }
        NotificationsManager.sharedInstance.removeAllNotifications { (result) in
            switch result {
                case .success(_):
                    self.setupNotificationsUI()
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
            }
        }
    }
}

// MARK: - TableView methods

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
        return notifications.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: notificationsTableViewCellIdentifier, for: indexPath) as! NotificationTableViewCell
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(NotificationsViewController.deletebuttontapped(_:)), for: .touchUpInside)
        let notification = notifications[(notifications.count-1) - indexPath.row]
        cell.notification = notification
        return cell
    }
    @objc func deletebuttontapped(_ sender: UIButton){
       
      let alert = UIAlertController(title: "Delete", message: "Are you sure\n you want to delete this notification?", preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "Delete", style: .default) { alertAct in
            self.deleteNotification(index: sender.tag)
        }
        alert.addAction(alertAction)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
      
   }
    
    
    func deleteNotification(index : Int){
        self.showLoader()
      let notificationObjc = self.notifications[index]
        NotificationsManager.sharedInstance.deleteNotification(notificationObject: notificationObjc) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                    case .success(_):
                        NotificationsManager.sharedInstance.notifications.remove(at: index)
                        self.setupNotificationsUI()
                    case .failure(let error):
                        self.view.toast(error.localizedDescription)
                }
            }
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let notification = notifications[(notifications.count-1) - indexPath.row]
            if notification.request?.status ?? 0 == 0 {
                if notification.status == 1{
                    
                }else{
                    if notification.type == 10 {
                        presentThumbsAlertViewController(alertTitle: notification.message ?? "")
                    } else if notification.type == 11 || notification.type == 12 || notification.type == 15 {
                        if notification.request?.taskFor == 2 || notification.request?.taskFor == 15 {
                            PersonalFitnessManager.sharedInstance.getPersonalChallenge(taskId: notification.request?.taskId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "") { (result) in
                                switch result {
                                case .success(let personalChallenge):
                                    self.presentApproveChallengeViewController(personalChallenge: personalChallenge, kidName: notification.request?.kidName ?? "")
                                case .failure(let error):
                                    self.view.toast(error.localizedDescription)
                                }
                            }
                        } else {
                            
        //                    if UserDefaults.standard.bool(forKey: notification.request?.taskId ?? "") == false{

        //                    if alreadyAccept == false{
                                let vc = GlobalStoryBoard().childChoreAcceptPopUp
                                vc.modalPresentationStyle = .overFullScreen
                                vc.notificationsViewController = self
                                vc.notificationData = notifications[(notifications.count-1) - indexPath.row]
                                vc.delegate = self
                                UserDefaults.standard.set(true, forKey: notification.request?.taskId ?? "")

                                self.alreadyAccept = true
                                self.present(vc, animated: true, completion: nil)
        //                    }else{
        //                        view.toast("Already accepted/rejected this chore request.")
        //
        //                    }
                           
                        }
                        
                    } else if notification.type == 13 {
                        presentApproveLevelUpCustomRewardViewController(invitedRequest: notification.request!)
                    }
                }
               
            } else {
                if notification.type == 10 {
                    view.toast("Already accepted/rejected this invite request.")
                } else if notification.type == 11 {
                    view.toast("Already accepted/rejected this challenge.")
                }
            }
    }
}

// MARK: - CustomNavigationBarDelegate methods

extension NotificationsViewController: CustomNavigationBarDelegate {
    func customNavigationBarDidTapLeftButton(_ navigationBar: CustomNavigationBar) {
        dismiss(animated: true, completion: nil)
    }
    
    func customNavigationBarDidTapRightButton(_ navigationBar: CustomNavigationBar) {
        clearALlNotifications()
    }
}

// MARK: - ThumbsAlertViewControllerDelegate methods

extension NotificationsViewController: ThumbsAlertViewControllerDelegate {
    func didTapThumbsUpButton(thumbsAlertViewController: ThumbsAlertViewController) {
        if let indexPath = notificationsTableView.indexPathForSelectedRow {
            let notification = notifications[(notifications.count-1) - indexPath.row]
            
            guard let request  = notification.request else {
                return
            }
            
            PersonalFitnessManager.sharedInstance.acceptInvite(invitedRequest: request) { (result) in
                switch result {
                case .success(_):
                    thumbsAlertViewController.dismiss(animated: true, completion: nil)
                    self.getAllNotifications()
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
            }
        }
    }
    
    func didTapThumbsDownButton(thumbsAlertViewController: ThumbsAlertViewController) {
        if let indexPath = notificationsTableView.indexPathForSelectedRow {
            let notification = notifications[(notifications.count-1) - indexPath.row]
    
            guard let request  = notification.request else {
                return
            }
    
        PersonalFitnessManager.sharedInstance.rejectInvite(invitedRequest: request) { (result) in
            switch result {
            case .success(_):
                thumbsAlertViewController.dismiss(animated: true, completion: nil)
                self.getAllNotifications()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
                }
            }
        }
    }
}

//MARK: - ApproveChallengeViewControllerDelegate Methods

extension NotificationsViewController: ApproveChallengeViewControllerDelegate {
    func didTapAcceptChallengeButton(approveChallengeViewController: ApproveChallengeViewController) {
        guard let indexPath = notificationsTableView.indexPathForSelectedRow else { return }
        
//        approveChallengeViewController.personalChallenge?.data.challengeAcceptType = .accept
        
        let notification = notifications[(notifications.count-1) - indexPath.row]
        
        let harmoneyBucks = Int(approveChallengeViewController.harmoneyTextfield.text ?? "")
        let customReward = approveChallengeViewController.customRewardTextfield.text
        
        var walletPrivateKeyKid:String = "[]"
        if let assigntomemberDataList = HarmonySingleton.shared.getKidWalletList{
            for objMember in assigntomemberDataList{
                if objMember.kidGuid == notification.request?.requestUserGuid {
                    if let fromguid = objMember.walletAddress{
                        walletPrivateKeyKid = fromguid
                    }
                }
            }
        }
        
        if notification.type == 15{
            if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
            }else{
                if self.approveChallengeViewController?.harmoneyTextfield.text ?? "" != "0" || self.approveChallengeViewController?.harmoneyTextfield.text ?? "" != ""
                {
                    if HarmonySingleton.shared.getKidWalletList == nil && HarmonySingleton.shared.isHideForNewFlow == false
                    {
                        SVProgressHUD.dismiss()
                        AlertView.shared.showAlert(view: self, title: "Wallet Create", description: "Create sub Wallet to transfer amount")
                        AlertView.shared.delegate = self
                        return
                    }
                }
            }
            PersonalFitnessManager.sharedInstance.acceptPersonalChallenge(taskId: approveChallengeViewController.personalChallenge?.data.challengeId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "", harmoneyBucks: harmoneyBucks ?? 0, customReward: customReward ?? "") { (result) in
                switch result {
                case .success(_):
                   
                        self.transactionToUser(transferGuid: notification.request?.requestUserGuid ?? "", harmoneyBucks: approveChallengeViewController.harmoneyTextfield.text ?? "",cryptoAddresskid: walletPrivateKeyKid)
                       
                        
                        PersonalFitnessManager.sharedInstance.transferFitnessAmount(taskId: approveChallengeViewController.personalChallenge?.data.challengeId ?? "", type: 15) { (result) in
                            switch result {
                            case .success(_):
                                self.getAllNotifications()
                                approveChallengeViewController.dismiss(animated: true, completion: nil)
                                self.presentCustomAlertViewController(alertMessage: String(format: "Yay! You have transfer Amount to kid.", notification.request?.kidName ?? ""), alertTitle: "Amount Approved")
                            case .failure(let error):
                                self.view.toast(error.localizedDescription)
                            }
                        }
                    
                    
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
            }

          
        }else{
            PersonalFitnessManager.sharedInstance.acceptPersonalChallenge(taskId: approveChallengeViewController.personalChallenge?.data.challengeId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "", harmoneyBucks: harmoneyBucks ?? 0, customReward: customReward ?? "") { (result) in
                switch result {
                case .success(_):
                    self.getAllNotifications()
                    approveChallengeViewController.dismiss(animated: true, completion: nil)
                    self.presentCustomAlertViewController(alertMessage: String(format: "Yay! You have approved kid's task.", notification.request?.kidName ?? ""), alertTitle: "Challenge Approved")
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
            }

        }
        
    }
    
    func addHarmoneyAmount(transferGuid:String, harmoneyBucks:String){
        self.reduceHarmoneyWallet(amount: harmoneyBucks )
        self.addHarmoneyWallet(amount: harmoneyBucks, requestUserGuid: transferGuid )
    }
    func transactionToUser(transferGuid:String, harmoneyBucks:String, cryptoAddresskid:String) {
        
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
        data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
        data.updateValue(harmoneyBucks, forKey: "amount")
        data.updateValue(transferGuid , forKey: "transferGuid")
        let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
        data.updateValue(cryptoAddresskid, forKey: "walletAddress")
        var (url, method, param) = APIHandler().transferAmountToSila(params: data)
        print(url, method, param)
        AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                
                if  let value = value as? [String:Any] {
                    self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                    if self.transfertoUserObjectValue?.data?.status == "SUCCESS" {
                        self.view.toast(self.transfertoUserObjectValue?.data?.message ?? "")
                        self.reduceHarmoneyWallet(amount: harmoneyBucks )
//                        self.addHarmoneyWallet(amount: harmoneyBucks, requestUserGuid: transferGuid )
                    }else{
                        
                        if Reachability.isConnectedToNetwork() {
                            SVProgressHUD.show()
                            var data = [String : Any]()
                            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                            
                            data.updateValue("535-354-5454", forKey: "moblieNumber")
                            var (url, method, param) = APIHandler().getGuidForMobileNumber(params: data)
                            
                            AF.request(url, method: method, parameters: param,headers: self.headers).validate().responseJSON { response in
                                SVProgressHUD.dismiss()
                                switch response.result {
                                case .success(let value):
                                    if  let value = value as? [String:Any] {
                                        self.getGuidForMobileNumber = Mapper<guidForMobileNumberObject>().map(JSON: value)
                                        let guid = self.getGuidForMobileNumber?.data?.last?.guid
                                        let userName = self.getGuidForMobileNumber?.data?.last?.name
                                        if guid == nil
                                        {
                                            
                                        }else{
                                            
                                            var data = [String : Any]()
                                            //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                                            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                                            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
                                            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
                                            data.updateValue(harmoneyBucks , forKey: "amount")
                                            data.updateValue(guid ?? "", forKey: "transferGuid")
                                            let cryptoAddress = UserDefaults.standard.value(forKey: "cryptoAddress")
                                            data.updateValue(cryptoAddress ?? "", forKey: "walletAddress")
                                            var (url, method, param) = APIHandler().transferAmountToSila(params: data)
                                            
                                            AF.request(url, method: method, parameters: param,headers: self.headers).validate().responseJSON { response in
                                                SVProgressHUD.dismiss()
                                                switch response.result {
                                                case .success(let value):
                                                    
                                                    if  let value = value as? [String:Any] {
                                                        self.transfertoUserObjectValue = Mapper<transfertoUserObject>().map(JSON: value)
                                                        self.view.toast(self.transfertoUserObjectValue?.data?.message ?? "")
                                                        self.reduceHarmoneyWallet(amount: harmoneyBucks )
                                                        self.addHarmoneyWallet(amount: harmoneyBucks, requestUserGuid: transferGuid )
                                                    }
                                                case .failure(let error):
                                                    print(error)
                                                }
                                            }
                                        }
                                        
                                    }
                                case .failure(let error):
                                    print(error)
                                }
                            }
                        }
                    }
                    
                    //                                                        self.transferAmountTxt.text = ""
                    //                                                        self.transferMobileNumberTxt.text = ""
                    //                                                        DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
                    //                                                            self.getWallet()
                    //                                                        }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
    func addHarmoneyWallet(amount: String, requestUserGuid: String){
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(requestUserGuid, forKey: "guid")
            
        data.updateValue(amount, forKey: "amount")
      
        var (url, method, param) = APIHandler().addHarmoneyWallet(params: data)
            print(url,method,param)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
//                            self.view.toast(self.addHarmoneyWalletValue?.message ?? "Wallet amount added to harmoney account")
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    
    func reduceHarmoneyWallet(amount: String){
        if Reachability.isConnectedToNetwork() {
         SVProgressHUD.show()
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
        data.updateValue(amount, forKey: "amount")
      
        var (url, method, param) = APIHandler().reduceHarmoneyWallet(params: data)
            print(url, method, param)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
//                            self.view.toast(self.addHarmoneyWalletValue?.message ?? "Wallet amount added to harmoney account")
                            
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    func didTapDeclineChallengeButton(approveChallengeViewController: ApproveChallengeViewController) {
        guard let indexPath = notificationsTableView.indexPathForSelectedRow else { return }
        
        approveChallengeViewController.personalChallenge?.data.challengeAcceptType = .decline
        
        let notification = notifications[(notifications.count-1) - indexPath.row]
        
        PersonalFitnessManager.sharedInstance.rejectPersonalChallenge(taskId: approveChallengeViewController.personalChallenge?.data.challengeId ?? "", requestUserGuid: notification.request?.requestUserGuid ?? "") { (result) in
            switch result {
            case .success(_):
                self.getAllNotifications()
                approveChallengeViewController.dismiss(animated: true, completion: nil)
                self.presentCustomAlertViewController(alertMessage: String(format: "You have rejected the task", notification.request?.kidName ?? ""), alertTitle: "Challenge Rejected")
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func didTapCloseButton(approveChallengeViewController: ApproveChallengeViewController) {
        approveChallengeViewController.dismiss(animated: true, completion: nil)
    }
    
    func didTapAcceptLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController) {
        LevelsManager.sharedInstance.acceptLevelReward(invitedRequest: approveChallengeViewController.invitedRequest!) { (result) in
            switch result {
            case .success(_):
                approveChallengeViewController.dismiss(animated: true, completion: nil)
                self.view.toast("Level reward accepted.")
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func didTapDeclineLevelRewardButton(approveChallengeViewController: ApproveChallengeViewController) {
        LevelsManager.sharedInstance.rejectLevelReward(invitedRequest: approveChallengeViewController.invitedRequest!) { (result) in
            switch result {
            case .success(_):
                approveChallengeViewController.dismiss(animated: true, completion: nil)
                self.view.toast("Level reward rejected.")
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
}

//MARK: - CustomAlertViewControllerDelegate Methods
extension NotificationsViewController : AlertCallBack{
    func clickedOnOk() {
        self.dismiss(animated: true, completion: nil)
    }
}
extension NotificationsViewController: CustomAlertViewControllerDelegate {
    func didTapOkayButton(customAlertViewController: CustomAlertViewController) {
        customAlertViewController.dismiss(animated: true, completion: nil)
        
        self.getAllNotifications()
    }
}

extension NotificationsViewController: AddBankAccountDelegate {
    func addBankAccountCallBack() {
        self.dismiss(animated: false, completion: nil)
        HarmonySingleton.shared.mainTabBar?.selectedIndex = 3
    }
}
