//
//  TableViewCell.swift
//  Harmoney
//
//  Created by Thirukumaran on 30/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class EnterCodeTableviewcell: UITableViewCell {
    static let identifier = "EnterCodeTableviewcell"
    static func nib()->UINib{
       return UINib(nibName: EnterCodeTableviewcell.identifier, bundle: nil)
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var btnArrowWidthConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
