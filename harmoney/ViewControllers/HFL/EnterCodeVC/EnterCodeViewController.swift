//
//  EnterCodeViewController.swift
//  tableviewcell
//
//  Created by Mohanraj on 07/10/21.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD
import IQKeyboardManagerSwift
protocol EnterCodeDelegate:NSObject {
    func showClubListVC(leagueInfo : BannerLeagueInfo?, arrAPI : [Any]?)
}

class EnterCodeViewController: UIViewController, UITableViewDelegate{
    //Outlets
//    @IBOutlet weak var vwName: UIView!
//    @IBOutlet weak var vwGrade: UIView!
//    @IBOutlet weak var vwPassCode: UIView!
    @IBOutlet weak var imgLegueBanner: UIImageView!
//    @IBOutlet weak var txtName: UITextField!
//    @IBOutlet weak var txtEventcode: UITextField!
//    @IBOutlet weak var txtgrade: UITextField!
    @IBOutlet weak var btnPrivacy: UIButton!
    @IBOutlet weak var listTableview: UITableView!
    @IBOutlet weak var listTableviewheightConstraint: NSLayoutConstraint!

    weak var deleagte : EnterCodeDelegate? = nil
    
    //Properties
    var bannerLeagueInfo : BannerLeagueInfo?
    var leagueRegistrationInfo = [LeagueRegistrationInfo]()
    var gradePicker: UIPickerView! = UIPickerView()
    var arrGrades :[String] = []
    var txtgrade = UITextField()
    var arrAPI = [Any]()

    //View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enable = true
    }
}

//Action methods
extension EnterCodeViewController {
    @objc func onGrade(_ sender: UIButton) {
        let indexPath = IndexPath(item: sender.tag, section: 0)
        if let cell = listTableview.cellForRow(at: indexPath) as? EnterCodeTableviewcell{
            txtgrade = cell.inputTextField
            txtgrade.becomeFirstResponder()
            arrGrades.removeAll()
            arrGrades = self.leagueRegistrationInfo[sender.tag].dropdownValues ?? []
        }

    }
    
    @IBAction func onClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitbtn(_ sender: Any) {
        if isValidateFields(){
            doSubmit()
        }
    }
    @IBAction func privacyPolicy(_ sender: Any) {
        
        let privacypolicy = GlobalStoryBoard().localWebVC
        privacypolicy.push = true
        privacypolicy.titletext =  HarmonySingleton.shared.dashBoardData.data?.profileMenu?.privacyPolicy?.menuName
        privacypolicy.websiteUrl =  HarmonySingleton.shared.dashBoardData.data?.profileMenu?.privacyPolicy?.link
        print("pushed")
        self.navigationController?.pushViewController(privacypolicy, animated: true)
        
    }
}

//User defiend methods
extension EnterCodeViewController {
    func initalSetup(){
     
        if let registerInfo = bannerLeagueInfo?.leagueRegistrationInfo{
            if registerInfo.count > 0{
                for item in registerInfo {
                    if item.isActive == true{
                        self.leagueRegistrationInfo.append(item)
                    }
                }
            }
        }
        
        if (leagueRegistrationInfo.count) > 0{
            listTableview.reloadData()
        }
        DispatchQueue.main.async {
            self.listTableviewheightConstraint.constant = CGFloat(self.leagueRegistrationInfo.count * 73)
        }
        showTextFieldBasedonLeageInfo()
        initalUI()
        setPickerViewData()
        registerCells()
    }
    func registerCells(){
        listTableview.separatorStyle = .none
        listTableview.register(EnterCodeTableviewcell.nib(), forCellReuseIdentifier: EnterCodeTableviewcell.identifier)
        listTableview.delegate = self
        listTableview.dataSource = self
        listTableview.reloadData()
    }

    
    func initalUI(){
       
        if let imgName = bannerLeagueInfo?.leagueLogo {
            imgLegueBanner.set_image(imgName)
        }
        btnPrivacy.underline()
//        setPading()
    }
    
//    func showTextFieldBasedonLeageInfo(){
       /*
        if let leagueInfo = self.bannerLeagueInfo{
            vwName.isHidden = true
            vwGrade.isHidden = true
            vwPassCode.isHidden = true
            
            if leagueInfo.leagueRegistrationInfo?.name?.isActive ?? false{
                vwName.isHidden = false
            }
            if leagueInfo.leagueRegistrationInfo?.grade?.isActive ?? false{
                vwGrade.isHidden = false
                if let grades = leagueInfo.leagueRegistrationInfo?.grade?.dropdownValues{
                    self.arrGrades = grades
                }
            }
            if leagueInfo.leagueRegistrationInfo?.passcode?.isActive ?? false{
                vwPassCode.isHidden = false
            }
        }
 */
//    }
    func showTextFieldBasedonLeageInfo(){
          /*
           if let leagueInfo = self.bannerLeagueInfo{
               vwName.isHidden = true
               vwGrade.isHidden = true
               vwPassCode.isHidden = true
               
               if leagueInfo.leagueRegistrationInfo?.name?.isActive ?? false{
                   vwName.isHidden = false
               }
               if leagueInfo.leagueRegistrationInfo?.grade?.isActive ?? false{
                   vwGrade.isHidden = false
                   if let grades = leagueInfo.leagueRegistrationInfo?.grade?.dropdownValues{
                       self.arrGrades = grades
                   }
               }
               if leagueInfo.leagueRegistrationInfo?.passcode?.isActive ?? false{
                   vwPassCode.isHidden = false
               }
           }
    */
      
//           if let leagueInfoDetails = self.bannerLeagueInfo,
//              let leagueRegistrationInfoDetails = leagueInfoDetails.leagueRegistrationInfo{
//               for registrationDetail in leagueRegistrationInfoDetails {
//                   if let formkey = registrationDetail.formKey {
//                       if formkey.lowercased() == "grade".lowercased(){
//                           if let grades = registrationDetail.dropdownValues{
//                               self.arrGrades = grades
//                           }
//                       }
//                   }
//               }
//           }
        
       }
//    func setPading(){
//        txtName.setLeftPaddingPoints(10)
//        txtEventcode.setLeftPaddingPoints(10)
//        txtgrade.setLeftPaddingPoints(10)
//    }
    func setPickerViewData(){
        gradePicker.delegate = self
        gradePicker.dataSource = self
    }
    
    func doSubmit(){
        print("all are validated: Validation Success!")
        if leagueRegistrationInfo.count > 0 {
                for i in 0..<leagueRegistrationInfo.count {
                    let index = leagueRegistrationInfo[i]
                    if index.labelName == "Passcode"{
                        let indexPath = IndexPath(item: i, section: 0)
                        if let cell = listTableview.cellForRow(at: indexPath) as? EnterCodeTableviewcell{
                            doPasscodeValidate(passcode: cell.inputTextField.text ?? "")
                            break
                        }
                    }
                }
        }
    }
    
    //validating every  textfields  only if view is active
    
    func isValidateFields()->Bool{
        if leagueRegistrationInfo.count > 0 {
                for i in 0..<leagueRegistrationInfo.count {
                    let index = leagueRegistrationInfo[i]
                        let indexPath = IndexPath(item: i, section: 0)
                        if let cell = listTableview.cellForRow(at: indexPath) as? EnterCodeTableviewcell{
                            if cell.inputTextField.text == ""{
                                AlertView.shared.showAlert(view: self, title: index.labelName ?? "", description: "Please enter your " + (index.labelName ?? ""))
                                arrAPI.removeAll()
                                return false
                            }else{
                                var data = [String : Any]()
                                data["Key"] = index.formKey
                                data["Value"] = cell.inputTextField.text
                                arrAPI.append(data)
                            }
                        }
                }
        }

        return true
    }
}
//picker view for grade in textfields
extension EnterCodeViewController : UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        txtgrade.text = arrGrades[row]
    }
}

extension EnterCodeViewController: UIPickerViewDataSource{
    // returns the number of 'columns' to display.
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return arrGrades.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrGrades[row]
    }
}

extension EnterCodeViewController: UITextFieldDelegate {
    //checking textfield has a valid
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let indexPath = IndexPath(item: textField.tag, section: 0)
        if let cell = listTableview.cellForRow(at: indexPath) as? EnterCodeTableviewcell{
            txtgrade = cell.inputTextField
            txtgrade.becomeFirstResponder()
            arrGrades.removeAll()
            arrGrades = self.leagueRegistrationInfo[textField.tag].dropdownValues ?? []
        }

    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if (textField == txtName)  {
//            guard let textFieldText = textField.text,
//                  let rangeOfTextToReplace = Range(range, in: textFieldText) else {
//                return false
//            }
//            let substringToReplace = textFieldText[rangeOfTextToReplace]
//            let count = textFieldText.count - substringToReplace.count + string.count
//
//            do {
//                let regex = try NSRegularExpression(pattern: ".*[^A-Za-z].*", options: [])
//                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
//                    return false
//                }
//            }
//            catch {
//                print("ERROR")
//            }
//            return count <= 30
//        }
//        return true
//
//    }
}

extension EnterCodeViewController{
    
    func doPasscodeValidate(passcode:String){
        showLoader()
        if Reachability.isConnectedToNetwork() {
            var data = [String : Any]()
            data["guid"] = UserDefaultConstants().sessionKey!
            data["passcode"] = passcode
            data["leagueId"] = bannerLeagueInfo?.leagueID
            
            let (url, method, param) = APIHandler().validateEventPassword(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                //providing response from api
                
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            let commonResponse = Mapper<CommonResponse>().map(JSON: value)
                            if commonResponse?.status == 200{
                                if self.bannerLeagueInfo?.isSingleTeam == true{
                                    self.showSubTeamDirectly(leagueInfo: self.bannerLeagueInfo!)
                                   
                                }else{
                                    self.pushToClubList()
                                }
                               
                            }else{
                                AlertView.shared.showAlert(view: self, title: "Error", description: commonResponse?.message ?? "")
                            }
                        }
                    case .failure(let error):
                        print(error)
                        AlertView.shared.showAlert(view: self, title: "Error", description: error.localizedDescription)
                    }
                }
            }
        }else{
            self.hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
        
    }
    
    private func pushToClubList(){
        self.navigationController?.popViewController(animated: false)
        self.deleagte?.showClubListVC(leagueInfo: self.bannerLeagueInfo, arrAPI: arrAPI)
    }
    
    func showSubTeamDirectly(leagueInfo : BannerLeagueInfo){
        let subTeamVC = GlobalStoryBoard().subTeamListViewController
        var arrClub = [TeamResult]()
        let teamResult = TeamResult(clubName: leagueInfo.strTeamName, clubLogo: leagueInfo.strTeamFlag, clubId: leagueInfo.strTeamID)
        arrClub.append(teamResult)
        subTeamVC.arrClubList = arrClub
        subTeamVC.leagueInfo = leagueInfo
        subTeamVC.currentIndex = 0
        subTeamVC.arrAPI = arrAPI
        subTeamVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(subTeamVC, animated: true)
    }
}


extension EnterCodeViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leagueRegistrationInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EnterCodeTableviewcell.identifier) as?EnterCodeTableviewcell else {
            return UITableViewCell()
        }
        if let index = leagueRegistrationInfo[indexPath.item] as? LeagueRegistrationInfo{
            cell.lblTitle.text = index.labelName
            if index.isMandatory == true {
                cell.lblTitle.text = (cell.lblTitle.text ?? "") + "*"
            }
                
            cell.btnArrow.isHidden = true
            cell.btnArrow.tag = indexPath.item
            cell.inputTextField.tag = indexPath.item
            cell.inputTextField.delegate = self
            
            cell.btnArrowWidthConstraint.constant = 0
            if index.isEditOrDropdown == 2{
                cell.btnArrow.isHidden = false
                cell.btnArrowWidthConstraint.constant = 40
                cell.inputTextField.inputView = gradePicker
                cell.btnArrow.addTarget(self, action:#selector(onGrade), for:.touchUpInside)
            }else{
                if index.inputType == 1{
                    cell.inputTextField.keyboardType = .default
                }else if index.inputType == 2{
                    cell.inputTextField.keyboardType = .numberPad
                }else if index.inputType == 3{
                    cell.inputTextField.keyboardType = .emailAddress
                }else if index.inputType == 4{
                    cell.inputTextField.keyboardType = .numberPad
                }else if index.inputType == 5{
                    cell.inputTextField.keyboardType = .default
                }
            }
        }
        cell.lblTitle.textColor = .black
        cell.vwBackground.backgroundColor = .white
        cell.selectionStyle = .none
    return cell
}

}
