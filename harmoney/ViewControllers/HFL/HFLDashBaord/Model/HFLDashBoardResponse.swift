//
//  HFLDashBoardResponse.swift
//  Harmoney
//
//  Created by Saravanan on 18/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct HFLDashBoardResponse : Mappable {
    var status : Int?
    var response : HFLResponse?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        response <- map["response"]
    }

}
