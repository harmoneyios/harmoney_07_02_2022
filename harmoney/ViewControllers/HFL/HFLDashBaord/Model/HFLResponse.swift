//
//  HFLResponse.swift
//  Harmoney
//
//  Created by Saravanan on 18/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct HFLResponse : Mappable {
    var userProfile : HFLUserProfile?
    var allTeam : [HFLTopUserDetails]?
    var myClub : [HFLTopUserDetails]?
    var myTeam : [HFLTopUserDetails]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        userProfile <- map["userProfile"]
        allTeam <- map["allTeam"]
        myClub <- map["myClub"]
        myTeam <- map["myTeam"]
    }

}
