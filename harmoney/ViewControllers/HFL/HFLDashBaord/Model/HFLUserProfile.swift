//
//  HFLUserProfile.swift
//  Harmoney
//
//  Created by Saravanan on 18/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct HFLUserProfile : Mappable {
    var strProfilePicLink : String?
    var strDisplayName_encrypted : String?
    var strMyTeamName : String?
    var strMyTeamLogo : String?
    var strClubName : String?
    var strClubLogo : String?
    var intTotalSteps : String?
    var intTotalKms : String?
    var intTotalGoal : String?
    var intTotalDonation : String?
    var strDisplayName : String?{
        return getDecryptedString(cipherText: strDisplayName_encrypted)
    }

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        strProfilePicLink <- map["strProfilePicLink"]
        strDisplayName_encrypted <- map["strDisplayName"]
        strMyTeamName <- map["strMyTeamName"]
        strMyTeamLogo <- map["strMyTeamLogo"]
        strClubName <- map["strClubName"]
        strClubLogo <- map["strClubLogo"]
        intTotalSteps <- map["intTotalSteps"]
        intTotalKms <- map["intTotalKms"]
        intTotalGoal <- map["intTotalGoal"]
        intTotalDonation <- map["intTotalDonation"]
    }

}
