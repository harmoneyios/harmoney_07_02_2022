//
//  HFLTopUserDetails.swift
//  Harmoney
//
//  Created by Saravanan on 18/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct HFLTopUserDetails : Mappable {
    var strProfilePicLink : String?
    var strDisplayName_encrypted : String?
    var intTotalSteps : String?
    var intTotalGoals : String?
    var intPosition : Int?
    var strTeamId : String?
    var strTeamFlagLink : String?
    var strTeamName : String?
    var strDisplayName : String?{
        return getDecryptedString(cipherText: strDisplayName_encrypted)
    }
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        strProfilePicLink <- map["strProfilePicLink"]
        strDisplayName_encrypted <- map["strDisplayName"]
        intTotalSteps <- map["intTotalSteps"]
        intTotalGoals <- map["intTotalGoals"]
        intPosition <- map["intPosition"]
        strTeamId <- map["strTeamId"]
        strTeamFlagLink <- map["strTeamFlagLink"]
        strTeamName <- map["strTeamName"]
    }

}
