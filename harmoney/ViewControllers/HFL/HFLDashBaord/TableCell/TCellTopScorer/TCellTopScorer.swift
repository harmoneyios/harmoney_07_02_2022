//
//  TCellTopScorer.swift
//  Harmoney
//
//  Created by Saravanan on 05/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class TCellTopScorer: UITableViewCell {

    @IBOutlet weak var vwTopTitle: UIView!
    @IBOutlet weak var imgTeamLogo: UIImageView!
    
    @IBOutlet weak var lblDataNotAvailble: UILabel!
    @IBOutlet weak var btnLeaderBoard: UIButton!
    @IBOutlet weak var lblHeaderName: UILabel!
    
    
    @IBOutlet weak var imgProfile_2: UIImageView!
    @IBOutlet weak var lblTeamName_2: UILabel!
    @IBOutlet weak var lblUserName_2: UILabel!
    @IBOutlet weak var lblGoalCount_2: UILabel!
    @IBOutlet weak var lblStepsCount_2: UILabel!
    
    
    @IBOutlet weak var imgProfile_1: UIImageView!
    @IBOutlet weak var lblTeamName_1: UILabel!
    @IBOutlet weak var lblUserName_1: UILabel!
    @IBOutlet weak var lblGoalCount_1: UILabel!
    @IBOutlet weak var lblStepsCount_1: UILabel!
    
    
    @IBOutlet weak var imgProfile_3: UIImageView!
    @IBOutlet weak var lblTeamName_3: UILabel!
    @IBOutlet weak var lblUserName_3: UILabel!
    @IBOutlet weak var lblGoalCount_3: UILabel!
    @IBOutlet weak var lblStepsCount_3: UILabel!
    
    //290
    //280
    
    @IBOutlet weak var vw_Details_1: UIView!
    
    @IBOutlet weak var vw_Details_2: UIView!
    
    @IBOutlet weak var vw_Details_3: UIView!
    
    static let identifier = "TCellTopScorer"
    
    
    enum cellType: Int {
        case myTeam = 0, myClub, myLeague
    }
    static func nib()->UINib{
       return UINib(nibName: TCellTopScorer.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension TCellTopScorer{
    func upDateCell(index: Int, leagueInfo : BannerLeagueInfo?,HFL_Response : HFLResponse?){
        guard let type = cellType(rawValue: index) else {
            return
        }
        self.imgTeamLogo.isHidden = false
        if index == 1{
            self.imgTeamLogo.isHidden = true
        }
        var arrUserDetails : [HFLTopUserDetails]?
        var headerText = ""
        var logoUrl = ""
        
        lblTeamName_1.isHidden = false
        lblTeamName_2.isHidden = false
        lblTeamName_3.isHidden = false
        vwTopTitle.backgroundColor = .clear
        switch type {
        case .myTeam:
            arrUserDetails = HFL_Response?.myTeam
            if let hText = HFL_Response?.userProfile?.strMyTeamName{
                headerText = "\(hText) - Top 3"
            }
            
            if let logoText = HFL_Response?.userProfile?.strMyTeamLogo{
                logoUrl = logoText
            }
           // vwTopTitle.backgroundColor = UIColor.init(hex: 0x1E3F66)
            lblTeamName_1.isHidden = true
            lblTeamName_2.isHidden = true
            lblTeamName_3.isHidden = true
        case .myClub:
            arrUserDetails = HFL_Response?.myClub
            
            if leagueInfo?.isSingleTeam == false{
                if let hText = HFL_Response?.userProfile?.strClubName{
                    headerText = "\(hText) - Top 3"
                }
            }else{
                headerText = "Overall - Top 3"
//                if let hText = leagueInfo?.leagueName{
//                    headerText = "\(hText) - Top 3"
//                }
            }
           
            if leagueInfo?.isSingleTeam == false{
            if let logoText = HFL_Response?.userProfile?.strClubLogo{
                logoUrl = logoText
            }
            }else{
                if let logoText = leagueInfo?.leagueRunningImageURL{
                    logoUrl = logoText
                }
            }           
            
        case .myLeague:
            arrUserDetails = HFL_Response?.allTeam
            

            if let hText = leagueInfo?.leagueName{
                headerText = "\(hText) - Top 3"
            }
            
            if let logoText = leagueInfo?.leagueRunningImageURL{
                logoUrl = logoText
            }
        }
        
        
        
        // Empty goal and empty steps users remove from top team
        if HarmonySingleton.shared.isHideForNewFlow {
            arrUserDetails?.removeAll(where: { (hflTopuserDetails) -> Bool in
             return Int(hflTopuserDetails.intTotalGoals ?? "0") == 0 &&   Int(hflTopuserDetails.intTotalSteps ?? "0") == 0
             })
        }
      
    
        imgTeamLogo.backgroundColor = .white
        lblHeaderName.text = headerText
        imgTeamLogo.set_image(logoUrl)
        //useravatar
        
        setEmptyAvatar()
        vw_Details_1.isHidden = false
        vw_Details_2.isHidden = false
        vw_Details_3.isHidden = false
        
        lblDataNotAvailble.isHidden = true
        if arrUserDetails?.count == 0{
            vw_Details_1.isHidden = true
            vw_Details_2.isHidden = true
            vw_Details_3.isHidden = true
          //  lblDataNotAvailble.isHidden = false
        }else if arrUserDetails?.count == 1{
            setTop_1_Details(topUserDetails: arrUserDetails?.first)
            vw_Details_2.isHidden = true
            vw_Details_3.isHidden = true
        }else if arrUserDetails?.count == 2{
            setTop_1_Details(topUserDetails: arrUserDetails?.first)
            setTop_2_Details(topUserDetails: arrUserDetails?.last)
            vw_Details_3.isHidden = true
        }else if arrUserDetails?.count == 3{
            setTop_1_Details(topUserDetails: arrUserDetails?.first)
            setTop_2_Details(topUserDetails: arrUserDetails?[1])
            setTop_3_Details(topUserDetails: arrUserDetails?.last)
        }
       
    }
    
    func setEmptyAvatar(){
        
        imgProfile_1.image = UIImage(named: "useravatar")
        imgProfile_2.image = UIImage(named: "useravatar")
        imgProfile_3.image = UIImage(named: "useravatar")
    }
    
    func setTop_1_Details(topUserDetails: HFLTopUserDetails?){
        guard let topuserInfo = topUserDetails else{
            return
        }
       
        imgProfile_1.imageFromURL(urlString: topuserInfo.strProfilePicLink ?? "")
        lblTeamName_1.text = topuserInfo.strTeamName
        lblUserName_1.text = topuserInfo.strDisplayName?.capitalized
        lblGoalCount_1.text = topuserInfo.intTotalGoals
        lblStepsCount_1.text = topuserInfo.intTotalSteps?.formatedValue()
    }
    func setTop_2_Details(topUserDetails: HFLTopUserDetails?){
        guard let topuserInfo = topUserDetails else{
            return
        }
        imgProfile_2.imageFromURL(urlString: topuserInfo.strProfilePicLink ?? "")
        lblTeamName_2.text = topuserInfo.strTeamName
        lblUserName_2.text = topuserInfo.strDisplayName?.capitalized
        lblGoalCount_2.text = topuserInfo.intTotalGoals
        lblStepsCount_2.text = topuserInfo.intTotalSteps?.formatedValue()
    }
    func setTop_3_Details(topUserDetails: HFLTopUserDetails?){
        guard let topuserInfo = topUserDetails else{
            return
        }
        imgProfile_3.imageFromURL(urlString: topuserInfo.strProfilePicLink ?? "")
        lblTeamName_3.text = topuserInfo.strTeamName
        lblUserName_3.text = topuserInfo.strDisplayName?.capitalized
        lblGoalCount_3.text = topuserInfo.intTotalGoals
        lblStepsCount_3.text = topuserInfo.intTotalSteps?.formatedValue()
    }
}
