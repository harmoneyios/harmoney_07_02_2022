//
//  HFLDashBoardVC.swift
//  Harmoney
//
//  Created by Saravanan on 13/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

class HFLDashBoardVC: UIViewController {
    
    @IBOutlet weak var vwTopInfo: UIView!
    @IBOutlet weak var lblFilterDays: UILabel!
    @IBOutlet weak var vwNavBar: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblTotalGoals: UILabel!
    @IBOutlet weak var lblTotalSteps: UILabel!
    @IBOutlet weak var lblTotalDonation: UILabel!
    @IBOutlet weak var lblOrganized: UILabel!
    @IBOutlet weak var imgSponserLogo: UIImageView!
    @IBOutlet weak var imgLeagueLogo: UIImageView!
    @IBOutlet weak var tblVw: UITableView!
    
    
    @IBOutlet weak var vwDonation: UIStackView!
    @IBOutlet weak var btnViewDonation: UIButton!
    
    var selectedDayRow : Int = 0
    var leagueInfo : BannerLeagueInfo?
    var HFL_Response : HFLResponse?{
        didSet{
            self.updateUI()
        }
    }
    var toolBarVW = UIToolbar()
    var pickerVw  = UIPickerView()
    var selectedFilterDay : FilterDays = .today
    var arrFilterDays : [String]?
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        setUpNavBar()
    }
    
    @IBAction func onViewDonation(_ sender: Any) {
        let hflDonationVC =  GlobalStoryBoard().hfl_DonationViewContoller
        hflDonationVC.leagueInfo = self.leagueInfo
        hflDonationVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(hflDonationVC, animated: true)
        
    }
    
    @IBAction func onDateFilter(_ sender: Any) {
        showPickerView()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onBack(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onUserProfile(_ sender: Any) {
        
        let hflStatics =  GlobalStoryBoard().leagueStatiticsVc
        hflStatics.leagueInfo = self.leagueInfo
        hflStatics.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(hflStatics, animated: true)
        
        /*
        let hflStatics =  GlobalStoryBoard().hfl_StatisticsViewController
        hflStatics.leagueInfo = self.leagueInfo
        hflStatics.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(hflStatics, animated: true)
         */
    }
}

extension HFLDashBoardVC{
    func initalSetup(){
       // vwDonation.isHidden = !(leagueInfo?.isShowDonation ?? true)
       // btnViewDonation.isHidden = !(leagueInfo?.isShowDonation ?? true)
        btnViewDonation.isHidden = true
        
        vwTopInfo.isHidden = true
        lblFilterDays.text = "Today"
        arrFilterDays = FilterDays.allCases.map { $0.rawValue }
        registerCells()
        getDashBoardDetails()
    }
        private func setUpNavBar(){
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = CGRect(x: vwNavBar.bounds.origin.x, y: vwNavBar.bounds.origin.y, width: UIScreen.main.bounds.width, height: vwNavBar.bounds.height)
        vwNavBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
    }
    
    func registerCells(){
        tblVw.register(TCellTopScorer.nib(), forCellReuseIdentifier: TCellTopScorer.identifier)
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.tableFooterView = UIView()
        tblVw.reloadData()
        tblVw.bouncesZoom = false
    }
    
    
    func showPickerView(){
        
        if (pickerVw.superview != nil){
            pickerVw.removeFromSuperview()
        }
        
        if (toolBarVW.superview != nil){
            toolBarVW.removeFromSuperview()
        }
        
        pickerVw = UIPickerView.init()
        pickerVw.delegate = self
        pickerVw.dataSource = self
        pickerVw.backgroundColor = UIColor.white
        pickerVw.setValue(UIColor.black, forKey: "textColor")
        pickerVw.autoresizingMask = .flexibleWidth
        pickerVw.contentMode = .center
        pickerVw.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(pickerVw)
                
        toolBarVW = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBarVW.barStyle = .default
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)

       let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))
        
        let cancelButton = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(onCancelButtonTapped))
        
        toolBarVW.items = [cancelButton, spaceButton, doneButton]
        
        
        if let selecteIndex = self.arrFilterDays?.firstIndex(of: lblFilterDays.text ?? ""){
            pickerVw.selectRow(selecteIndex, inComponent: 0, animated: false)
            selectedDayRow = selecteIndex
        }
        
        self.view.addSubview(toolBarVW)
    }
    
    
    @objc func onDoneButtonTapped() {
        
        selectedDayRow = pickerVw.selectedRow(inComponent: 0)
        
        if let selectDay = FilterDays(rawValue: arrFilterDays?[selectedDayRow] ?? ""){
            selectedFilterDay = selectDay
        }
        lblFilterDays.text = arrFilterDays?[selectedDayRow]
        toolBarVW.removeFromSuperview()
        pickerVw.removeFromSuperview()
        getDashBoardDetails()
    }
    @objc func onCancelButtonTapped() {
        toolBarVW.removeFromSuperview()
        pickerVw.removeFromSuperview()
    }
}


extension HFLDashBoardVC : UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
}


extension HFLDashBoardVC: UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrFilterDays?.count ?? 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrFilterDays?[row]
    }
    
}


extension HFLDashBoardVC{
    
    func updateUI(){
        vwTopInfo.isHidden = false
        imgUserProfile.imageFromURL(urlString: HFL_Response?.userProfile?.strProfilePicLink ?? "")
        lblUserName.text = HFL_Response?.userProfile?.strDisplayName?.capitalized
        
        lblTotalGoals.text = HFL_Response?.userProfile?.intTotalGoal?.formatedValue()
        lblTotalSteps.text = HFL_Response?.userProfile?.intTotalSteps?.formatedValue()
        lblTotalDonation.text = "$\(HFL_Response?.userProfile?.intTotalDonation?.formatedValue() ?? "0")"
        lblOrganized.text = leagueInfo?.organizedText
        
        imgLeagueLogo.set_image(leagueInfo?.leagueLogo ?? "")
        imgSponserLogo.set_image(leagueInfo?.leagueSponsorLogo ?? "")
        
        tblVw.reloadData()
    }
    
    
    
    
    
    func getDashBoardDetails(){
        showLoader()
        if Reachability.isConnectedToNetwork() {
  
            //Params
          //  let (fromDate, toDate) = self.selectedFilterDay.getFromAndToDate(leagueStartDate: leagueInfo?.leagueStartDate ?? "")
            let (fromDate, toDate) = (leagueInfo?.leagueStartDate ?? "" , Date().getFilterDateFormat())
            
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.currentLeague?.leagueId
            data["teamId"] = leagueInfo?.currentLeague?.teamId
            data["subteamId"] = leagueInfo?.currentLeague?.subteamId
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getHFLDashBoardDetails(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    self.hideLoader()
                        switch response.result {
                        case .success(let value):
                            if let value = value as? [String:Any] {
                                //Update UI after API Success
                                let hflDashResponse = Mapper<HFLDashBoardResponse>().map(JSON: value)
                                self.HFL_Response = hflDashResponse?.response
                            }
                        case .failure(let error):
                            print(error)
                            AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                        }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
    }
}


extension HFLDashBoardVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 223
        }
        return 240

//        if indexPath.row == 0{
//            return 270
//        }
//        return 280
        
    }
}

extension HFLDashBoardVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.HFL_Response == nil ? 0 : self.leagueInfo?.isSingleTeam == false ? 3 : 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TCellTopScorer.identifier) as? TCellTopScorer else{
            return UITableViewCell()
        }
        cell.btnLeaderBoard.tag = indexPath.row
        cell.selectionStyle = .none
        cell.upDateCell(index: indexPath.row, leagueInfo: self.leagueInfo, HFL_Response: self.HFL_Response)
        cell.btnLeaderBoard.addTarget(self, action: #selector(onClickLeaderBoard(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func onClickLeaderBoard(sender: UIButton){
        let hflDonationVC =  GlobalStoryBoard().hfl_LeaderBoardViewContoller
        hflDonationVC.leagueInfo = self.leagueInfo
        hflDonationVC.dashBoardType = sender.tag
        hflDonationVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(hflDonationVC, animated: true)
    }
    
}
