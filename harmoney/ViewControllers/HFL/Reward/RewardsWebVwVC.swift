//
//  RewardsWebVwVC.swift
//  Harmoney
//
//  Created by Saravanan on 03/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import WebKit

class RewardsWebVwVC: UIViewController {

    @IBOutlet weak var webVw: WKWebView!
    var websiteUrl : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        setupWebVw()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func onClkBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupWebVw(){
        webVw.navigationDelegate = self
        let link = URL(string:websiteUrl ?? "" )!
        let request = URLRequest(url: link)
        webVw.contentMode = .scaleAspectFill
        webVw.load(request)
        webVw.scrollView.contentInsetAdjustmentBehavior = .never;
    }


}

extension RewardsWebVwVC : WKNavigationDelegate,WKUIDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideLoader()
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showLoader()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        hideLoader()
    }
}
