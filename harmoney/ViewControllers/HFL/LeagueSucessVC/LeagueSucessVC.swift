//
//  LeagueSucessVC.swift
//  Harmoney
//
//  Created by Saravanan on 16/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Lottie

class LeagueSucessVC: UIViewController {
    var animationView =  AnimationView()
    var completion :(()->())?
    var imageName : String?
    @IBOutlet weak var imgVw: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        completion?()
    }
    
    func initalSetup(){
        if let imgName = imageName{
            imgVw.set_image(imgName)
        }
        addAnimationView()
    }
    func addAnimationView(){
        animationView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        animationView.isUserInteractionEnabled = false
        self.view.addSubview(animationView)
          let path = Bundle.main.path(forResource: "HFL_successAnimation",
                                    ofType: "json") ?? ""
        animationView.animation = Animation.filepath(path)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.isUserInteractionEnabled = false
        animationView.play()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
