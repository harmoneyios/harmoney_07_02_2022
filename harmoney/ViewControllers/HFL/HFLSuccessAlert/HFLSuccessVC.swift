//
//  HFLSuccessVC.swift
//  Harmoney
//
//  Created by Saravanan on 13/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Lottie

class HFLSuccessVC: UIViewController {

    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    var animationView =  AnimationView()
    var alertinfo: (NSAttributedString?,NSAttributedString?,String?)?
    var completion :(()->())?
    var btnTitle : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        // Do any additional setup after loading the view.
    }
    
    
    func initalSetup(){
        
        if let (mainTitle,subTitle,img) = alertinfo{
            addAnimationView()
            if mainTitle?.length == 0{
                lblTitle.isHidden = true
            }
            lblTitle.attributedText = mainTitle
            lblSubTitle.attributedText = subTitle
            imgVw.set_image(img ?? "")
            
            if let titleBtn = btnTitle{
                btnOk.setTitle(titleBtn, for: .normal)
                imgVw.image = UIImage(named: img ?? "")
            }
        }
        
    }
    
    func addAnimationView(){
   
        animationView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        animationView.isUserInteractionEnabled = false
        self.view.addSubview(animationView)
          let path = Bundle.main.path(forResource: "HFL_successAnimation",
                                    ofType: "json") ?? ""
        animationView.animation = Animation.filepath(path)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.isUserInteractionEnabled = false
        animationView.play()
        
    }
    @IBAction func onOkClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        completion?()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



class AlertManager:NSObject{
    static let shared = AlertManager()
    func showSuccesAlertView(controller: UIViewController?, title : NSAttributedString? = nil, subTitle: NSAttributedString? = nil, imgLogo : String?,btnTitle: String? = nil, completionHandler :(()->())?){
        let subTeamVC = GlobalStoryBoard().successAlertVC
        subTeamVC.alertinfo = (title, subTitle, imgLogo)
        subTeamVC.completion = completionHandler
        subTeamVC.btnTitle =  btnTitle
        subTeamVC.modalPresentationStyle = .overCurrentContext
        if let controller = controller {
            controller.present(subTeamVC, animated: false, completion: nil)
        } else {
             showINTopVc(vc: subTeamVC)
        }
    }
  
    func show2000SuccesAlertView(controller: UIViewController?, imgName : String?, completionHandler :(()->())?){
        let leagueSuccessVC = GlobalStoryBoard().leagueSucess2000StepsVC
        leagueSuccessVC.completion = completionHandler
        leagueSuccessVC.imageName = imgName
        leagueSuccessVC.modalPresentationStyle = .overCurrentContext
        showINTopVc(vc: leagueSuccessVC)
    }
     
    func showHFLEarnPopup(controller : UIViewController?,img: String?){
       
        let hfl_Popup = GlobalStoryBoard().HFL_EarnPopup
        hfl_Popup.imgLogoUrl = img
        hfl_Popup.modalPresentationStyle = .overCurrentContext
        hfl_Popup.hidesBottomBarWhenPushed = true
        if let controller = controller {
            controller.present(hfl_Popup, animated: false, completion: nil)
        } else {
             showINTopVc(vc: hfl_Popup)
        }

    }
    
    func takeScreenshot(_ shouldSave: Bool = false) -> UIImage? {
            var screenshotImage :UIImage?
            let layer = UIApplication.shared.keyWindow!.layer
            let scale = UIScreen.main.scale
            UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
            guard let context = UIGraphicsGetCurrentContext() else {return nil}
            layer.render(in:context)
            screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            if let image = screenshotImage, shouldSave {
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            }
            return screenshotImage
        }
    
    func shareScreenToSocialMedia(controller : UIViewController?){
        guard let imgToShare = takeScreenshot() else{
            return
        }
        var shareAll = [Any]()
        let text = "Hi there,\n\nI am excited to share my earnings in Harmoney app with you all.\n\nIf you would like to earn, you can download and join the app with the link\n"
        
        if let myWebsite = NSURL(string:"https://apps.apple.com/us/app/harmoney-digital-wallet/id1483085835"){
            shareAll = [imgToShare ] as [Any]
        }
        
        let activityViewController = UIActivityViewController(activityItems: shareAll , applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = controller?.view
        if let controller = controller {
            controller.present(activityViewController, animated: false, completion: nil)
        } else {
             showINTopVc(vc: activityViewController)
        }
    }
}
