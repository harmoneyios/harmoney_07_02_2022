//
//  LBTeamWiseResponse.swift
//  Harmoney
//
//  Created by Saravanan on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct LBTeamWiseResponse : Mappable {
    var status : Int?
    var result : LBResult?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        result <- map["result"]
    }

}



struct LBResult : Mappable {
    var stepWise : [LBUserInfo]?
    var goalWise : [LBUserInfo]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        stepWise <- map["stepWise"]
        goalWise <- map["goalWise"]
    }

}



struct LBUserInfo : Mappable {
    var strTeamId : String?
    var strTeamFlagLink : String?
    var strTeamDisplayName : String?
    var intTotalUsers : Int?
    var intTotalSteps : Int?
    var intTotalKms : String?
    var intTotalGoals : Int?
    var intTotalDonation : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        strTeamId <- map["strTeamId"]
        strTeamFlagLink <- map["strTeamFlagLink"]
        strTeamDisplayName <- map["strTeamDisplayName"]
        intTotalUsers <- map["intTotalUsers"]
        intTotalSteps <- map["intTotalSteps"]
        intTotalKms <- map["intTotalKms"]
        intTotalGoals <- map["intTotalGoals"]
        intTotalDonation <- map["intTotalDonation"]
    }

}
