//
//  ClubWiseResponse.swift
//  Harmoney
//
//  Created by Mohanraj on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation

import ObjectMapper

struct ClubWiseResponse : Mappable {
    var status : Int?
    var result : ClubWiseResult?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        result <- map["result"]
    }

}

struct ClubWiseResult : Mappable {
    var stepsWise : [ClubWiseInfo]?
    var goalWise : [ClubWiseInfo]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        stepsWise <- map["stepWise"]
        goalWise <- map["goalWise"]
    }

}


struct ClubWiseInfo : Mappable {
    var strTeamId : String?
    var strTeamFlagLink : String?
    var strTeamDisplayName : String?
    var intTotalUsers : Int?
    var intTotalSteps : Int?
    var intTotalKms : String?
    var intTotalGoals : Int?
    var intTotalDonation : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        
        strTeamId <- map["strTeamId"]
        strTeamFlagLink <- map["strTeamFlagLink"]
        strTeamDisplayName <- map["strTeamDisplayName"]
        intTotalUsers <- map["intTotalUsers"]
        intTotalSteps <- map["intTotalSteps"]
        intTotalKms <- map["intTotalKms"]
        intTotalGoals <- map["intTotalGoals"]
        intTotalDonation <- map["intTotalDonation"]
    }

}


