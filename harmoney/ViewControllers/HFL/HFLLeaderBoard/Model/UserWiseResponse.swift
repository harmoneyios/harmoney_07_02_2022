//
//  UserWiseResponse.swift
//  Harmoney
//
//  Created by Mohanraj on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserWiseResponse : Mappable {
    var status : Int?
    var result : UserWiseResult?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        result <- map["result"]
    }

}

struct UserWiseResult : Mappable {
    var stepsWise : [UserWiseInfo]?
    var goalWise : [UserWiseInfo]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        stepsWise <- map["stepsWise"]
        goalWise <- map["goalWise"]
    }

}


struct UserWiseInfo : Mappable {
    var strGuid : String?
    var strProfilePicLink : String?
    var strDisplayName_encrypted : String?
    var strTotalSteps : Int?
    var strTotalKms : String?
    var strTotalGoals : Int?
    var strTotalDonation : String?
    var intPosition : Int?
    var strTeamFlagLink : String?
    var strTeamName : String?
    var strDisplayName : String?{
        return getDecryptedString(cipherText: self.strDisplayName_encrypted)
    }
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        strGuid <- map["strGuid"]
        strProfilePicLink <- map["strProfilePicLink"]
        strDisplayName_encrypted <- map["strDisplayName"]
        strTotalSteps <- map["strTotalSteps"]
        strTotalKms <- map["strTotalKms"]
        strTotalGoals <- map["strTotalGoals"]
        strTotalDonation <- map["strTotalDonation"]
        intPosition <- map["intPosition"]
        strTeamFlagLink <- map["strTeamFlagLink"]
        strTeamName <- map["strTeamName"]
    }

}
