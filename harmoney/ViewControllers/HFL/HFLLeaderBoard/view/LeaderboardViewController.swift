//
//  LeaderboardViewController.swift
//  Harmoney
//
//  Created by Mohanraj on 09/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import ObjectMapper
import SVProgressHUD


class LeaderboardViewController: UIViewController{
    @IBOutlet weak var lblTopTeamTItle: UILabel!
    @IBOutlet weak var vwTeamImageMyposition: UIView!
    
    @IBOutlet weak var lblMyTeamName: UILabel!
    @IBOutlet weak var imgVwTeamMyPosition: UIImageView!
    @IBOutlet weak var imgSponcorLogo: UIImageView!
    @IBOutlet weak var btnTeamList: UIButton!
    @IBOutlet weak var btnDay: UIButton!
    @IBOutlet weak var btnClubList: UIButton!
    @IBOutlet weak var leaderboard: UITableView!
    @IBOutlet weak var topheadvw: UIView!
    @IBOutlet weak var headvw: UIView!
    @IBOutlet weak var btnclub: UIButton!
    @IBOutlet weak var btnteam: UIButton!
    @IBOutlet weak var btnusers: UIButton!
    @IBOutlet weak var navBar: UIView!
    
    
    @IBOutlet weak var constHtMyPosition: NSLayoutConstraint! // 85
    @IBOutlet weak var lblMyPos_Donations: UILabel!
    @IBOutlet weak var lblMyPos_GoalOrStepsTitle: UILabel!
    @IBOutlet weak var lblMyPos_GoalORSteps: UILabel!
    @IBOutlet weak var img_MyPos_GoalOrStep: UIImageView!
    @IBOutlet weak var imgMyPosition: UIImageView!
    @IBOutlet weak var lblMyPosition: UILabel!
    @IBOutlet weak var vwMYPosition: UIView!
    
    @IBOutlet weak var lblMyPositionTitle: UILabel!
    
    @IBOutlet weak var vwDonation: UIStackView!
    var selectedDayRow : Int = 0
    var clubSelectRow : Int = 0
    var teamSelectRow : Int = 0
    
    var leagueInfo : BannerLeagueInfo?
    var clubListResponse : TeamListResponse?
    var subTeamListResponse : SubTeamListResponse?
    var ownSubTeamListResponse : SubTeamListResponse?
    
    var selectedFilterDay : FilterDays = .overall
    var arrFilterDays : [String]?
    
    var currentResultListType : ResultListType = .steps
    var currentListType : ListType = .users
    
    var toolBarVW = UIToolbar()
    var pickerVw  = UIPickerView()
    var dashBoardType : Int = 0
    var lbTeamWiseRespnse : LBTeamWiseResponse?{
        didSet{
            self.updateUI()
        }
    }
    var clubWiseResponse : ClubWiseResponse?{
        didSet{
            self.updateUI()
        }
    }
    var userWiseResponse : UserWiseResponse?{
        didSet{
            self.updateUI()
        }
    }
    
   
    
    enum ListType:Int{
        case club = 0
        case teams
        case users
    }
    enum ResultListType:Int{
        case goals = 1
        case steps
    }
    
    
    @IBAction func backbtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func onClkFilterDays(_ sender: UIButton) {
        self.showPickerView(type: 1)
    }
    @IBAction func onClubList(_ sender: UIButton) {
        self.showPickerView(type: 2)
    }
    @IBAction func onTeamList(_ sender: UIButton) {
        if clubSelectRow == 0{
            self.view.toast("Please Choose Any Club")
        }else{
            self.showPickerView(type: 3)
        }
        
    }
    
    @IBAction func onRadioButton(_ sender: UIButton) {
        removePickerView()
        chooseType(type: sender.tag)
    }
    
    
    func defaultButtonBehaviour(){
        btnClubList.isHidden = true
        btnTeamList.isHidden = true
        btnDay.isHidden = true
        btnClubList.isUserInteractionEnabled = true
    }
    func chooseType(type : Int){
        switch type {
        case 0:
            if btnclub.isSelected{
                return
            }
            defaultButtonBehaviour()
            currentListType = ListType(rawValue: type) ?? .club
            print("Club")
           // btnClubList.isHidden = false
            btnClubList.setTitle("All Clubs", for: .normal)
            clubSelectRow = 0
            btnClubList.isUserInteractionEnabled = false
            btnclub.isSelected = true
            btnteam.isSelected =  false
            btnusers.isSelected = false
            
            break
        case 1:
            if btnteam.isSelected{
                return
            }
            
            defaultButtonBehaviour()
            currentListType = ListType(rawValue: type) ?? .teams
            btnClubList.isHidden = false
            setClubName()
            print("Team")
            btnclub.isSelected = false
            btnteam.isSelected =  true
            btnusers.isSelected = false
            
            break
            
        case 2:
            
            if btnusers.isSelected{
                return
            }
            defaultButtonBehaviour()
            currentListType = ListType(rawValue: type) ?? .users
            btnClubList.isHidden = false
            btnTeamList.isHidden = false
            setClubName()
            self.subTeamListResponse = self.ownSubTeamListResponse
            setSubTeamName()
            print("Users")
            btnclub.isSelected = false
            btnteam.isSelected =  false
            btnusers.isSelected = true
            
            
            break
            
        default:
            print("none")
            
        }
        doAPICall()
    }
    
    func setSubTeamName(){
        var subTeamName : String?
        
        if let indexValue = self.subTeamListResponse?.arrSubTeam?.firstIndex(where: { (teamResult) -> Bool in
            if teamResult._id == self.leagueInfo?.currentLeague?.subteamId{
                subTeamName = teamResult.name
                return true
            }else{
                return false
            }
        }){
            self.btnTeamList.setTitle(subTeamName, for: .normal)
            self.teamSelectRow = indexValue + 1
        }
    }
    func setClubName(){
        var clubName : String?
        if let indexValue = self.clubListResponse?.result?.firstIndex(where: { (teamResult) -> Bool in
            if teamResult.strTeamId == self.leagueInfo?.currentLeague?.teamId{
                clubName = teamResult.strDisplayName
                return true
            }else{
                return false
            }
        }){
            self.btnClubList.setTitle(clubName, for: .normal)
            self.clubSelectRow = indexValue + 1
        }
    }
    func updateUI(){
        leaderboard.delegate = self
        leaderboard.dataSource = self
        leaderboard.reloadData()
        showUserPosition()
    }
    
    
    func showUserPosition(){
        vwMYPosition.isHidden = true
        constHtMyPosition.constant = 0
        
        vwTeamImageMyposition.isHidden = true
        lblMyTeamName.isHidden = true
        
        if currentListType == .users{
            var arrResponse : [UserWiseInfo]?
            if currentResultListType == .goals{
                arrResponse = self.userWiseResponse?.result?.goalWise
            }else{
                arrResponse = self.userWiseResponse?.result?.stepsWise
            }
            guard let userInformation = arrResponse?.first(where: { (userinfo) -> Bool in
                return userinfo.strGuid == UserDefaultConstants().guid
            })else{
                return
            }
            if currentResultListType == .goals{
                lblMyPos_GoalOrStepsTitle.text = "Total Goals"
                img_MyPos_GoalOrStep.image = UIImage(named: "goal_ball")
                lblMyPos_GoalORSteps.text = "\(userInformation.strTotalGoals?.formatedValue() ?? "0")"
            }else{
                lblMyPos_GoalOrStepsTitle.text = "Total Steps"
                img_MyPos_GoalOrStep.image = UIImage(named: "steps_new")
                lblMyPos_GoalORSteps.text = "\(userInformation.strTotalSteps?.formatedValue() ?? "0")"
            }
            
            lblMyPos_Donations.text = "$\(userInformation.strTotalDonation?.formatedValue() ?? "0")"
            lblMyPositionTitle.text =  userInformation.strDisplayName?.capitalized
            imgMyPosition.imageFromURL(urlString: userInformation.strProfilePicLink ?? "")
            imgVwTeamMyPosition.set_image(userInformation.strTeamFlagLink ?? "")
            lblMyPosition.text = "\(userInformation.intPosition ?? 0)"
            lblMyTeamName.text = userInformation.strTeamName
            
            vwDonation.isHidden = false
            if teamSelectRow == 0{
                vwDonation.isHidden = true
                vwTeamImageMyposition.isHidden = false
                lblMyTeamName.isHidden = false
            }
            vwMYPosition.isHidden = false
            constHtMyPosition.constant = 85
            
        }
        
    }
    
    func removePickerView(){
        if (pickerVw.superview != nil){
            pickerVw.removeFromSuperview()
        }
        
        if (toolBarVW.superview != nil){
            toolBarVW.removeFromSuperview()
        }
    }
    func showTeamSelectionPage(type : Int){
        var selectIndex = 0
        var isClub = true
        if type == 3{
            isClub = false
            selectIndex = teamSelectRow
        }else{
            selectIndex = clubSelectRow
        }
        let teamSelectVC =  GlobalStoryBoard().TeamSelection
        teamSelectVC.clubListResponse = self.clubListResponse
        teamSelectVC.subTeamListResponse = self.subTeamListResponse
        teamSelectVC.isClub  = isClub
        teamSelectVC.selectedIndex = selectIndex
        teamSelectVC.delegate = self
        teamSelectVC.modalPresentationStyle = .overCurrentContext
        self.present(teamSelectVC, animated: true, completion: nil)
        
    }
    
    func showPickerView(type : Int){
        /*Type denotes the tag */
        if type == 2 || type == 3{
            self.showTeamSelectionPage(type: type)
            return
        }
        removePickerView()
        pickerVw = UIPickerView.init()
        pickerVw.delegate = self
        pickerVw.tag = type
        pickerVw.dataSource = self
        pickerVw.backgroundColor = UIColor.white
        pickerVw.setValue(UIColor.black, forKey: "textColor")
        pickerVw.autoresizingMask = .flexibleWidth
        pickerVw.contentMode = .center
        pickerVw.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(pickerVw)
        
        toolBarVW = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBarVW.barStyle = .default
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped(sender:)))
        doneButton.tag = type
        let cancelButton = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(onCancelButtonTapped))
        
        toolBarVW.items = [cancelButton, spaceButton, doneButton]
        self.view.addSubview(toolBarVW)
    }
    
    
    @objc func onDoneButtonTapped(sender : UIButton) {
      
        if sender.tag == 1{
            selectedDayRow = pickerVw.selectedRow(inComponent: 0)
            if arrFilterDays?.count ?? 0 > selectedDayRow{
                btnDay.setTitle(arrFilterDays?[selectedDayRow], for: .normal)
            }
            
        }else if sender.tag == 2{
            clubSelectRow = pickerVw.selectedRow(inComponent: 0)
            if clubSelectRow == 0{
                btnClubList.setTitle("All Clubs", for: .normal)
                
            }else{
                if self.clubListResponse?.result?.count ?? 0 > (clubSelectRow - 1){
                    btnClubList.setTitle(self.clubListResponse?.result?[clubSelectRow - 1].strDisplayName, for: .normal)
                    self.getSubTeamList()
                }
            }
            
            btnTeamList.setTitle("All", for: .normal)
            teamSelectRow = 0
            
        }else{
            teamSelectRow = pickerVw.selectedRow(inComponent: 0)
            if teamSelectRow == 0{
                btnTeamList.setTitle("All", for: .normal)
            }else{
                if self.subTeamListResponse?.arrSubTeam?.count ?? 0 > (teamSelectRow - 1){
                    btnTeamList.setTitle(self.subTeamListResponse?.arrSubTeam?[teamSelectRow - 1].name, for: .normal)
                }
            }
        }
        removePickerView()
        doAPICall()
    }
    func doProcessTeamSelection(type: Int){
        if type == 2{
         
           if clubSelectRow == 0{
               btnClubList.setTitle("All Clubs", for: .normal)
               
           }else{
               if self.clubListResponse?.result?.count ?? 0 > (clubSelectRow - 1){
                   btnClubList.setTitle(self.clubListResponse?.result?[clubSelectRow - 1].strDisplayName, for: .normal)
                   self.getSubTeamList()
               }
           }
           
           btnTeamList.setTitle("All", for: .normal)
           teamSelectRow = 0
           
       }else{
         
           if teamSelectRow == 0{
               btnTeamList.setTitle("All", for: .normal)
           }else{
               if self.subTeamListResponse?.arrSubTeam?.count ?? 0 > (teamSelectRow - 1){
                   btnTeamList.setTitle(self.subTeamListResponse?.arrSubTeam?[teamSelectRow - 1].name, for: .normal)
               }
           }
       }
       doAPICall()
    }
    
    @objc func onCancelButtonTapped() {
        toolBarVW.removeFromSuperview()
        pickerVw.removeFromSuperview()
    }
    
    func setupNavBar(){
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = navBar.bounds
        navBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
    }
    
    func initalSetup(){
        vwMYPosition.isHidden = true
        initialUI()
        if HarmonySingleton.shared.isHideForNewFlow == false{
            setSwiperGestrues()
        }
        registerCells()
        self.getTeamList()
    }
    
    func initialUI(){
        
        if leagueInfo?.isSingleTeam == true{
            btnclub.isHidden = true
        }
        if HarmonySingleton.shared.isHideForNewFlow == false{
            vwDonation.isHidden = !(leagueInfo?.isShowDonation ?? true)
        }
        
        vwMYPosition.topCorners(radius: 10)
        arrFilterDays = FilterDays.allCases.compactMap{ $0.rawValue }
        if HarmonySingleton.shared.isHideForNewFlow{
            selectedDayRow = 0
        }else{
            selectedDayRow = arrFilterDays!.count  - 1
        }
      
        btnDay.setTitle(arrFilterDays?[selectedDayRow], for: .normal)
//        topheadvw.topCorners(radius: 25)
//        headvw.topCorners(radius: 25)
        imgSponcorLogo.set_image(leagueInfo?.leagueLogo ?? "")
    }
    func registerCells(){
        self.leaderboard.separatorStyle = .none
        self.leaderboard.register(UINib(nibName: "MembersTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "MembersTableViewCell")
        self.leaderboard.register(UINib(nibName: "ClubTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ClubTableViewCell")
        self.leaderboard.register(UINib(nibName: "MemberheaderTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "MemberheaderTableViewCell")
    }
    
    func setSwiperGestrues(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
            swipeRight.direction = .right
          self.leaderboard.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeLeft.direction = .left
          self.leaderboard.addGestureRecognizer(swipeLeft)
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                if currentResultListType == .steps {
                    selectType(selectType: ResultListType.goals.rawValue)
                }
            case .down:
                print("Swiped down")
            case .left:
                print("Swiped left")
                if currentResultListType == .goals {
                    selectType(selectType: ResultListType.steps.rawValue)
                }
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavBar()
    }
    
    
    
}
extension LeaderboardViewController: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        /*
        if pickerView.tag == 1{
            selectedDayRow = row
        }else if pickerView.tag == 2{
            clubSelectRow = row
        }else{
            teamSelectRow = row
        }
         */
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1{
            return arrFilterDays?.count ?? 0
        }else if pickerView.tag == 2{
            if let arrClubList = self.clubListResponse?.result{
                return arrClubList.count + 1
            }
            return 1
        }else{
            if let arrSubTeam = self.subTeamListResponse?.arrSubTeam{
                return arrSubTeam.count + 1
            }
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.white
        imageView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        
        //Text Label
        let textLabel = UILabel()
        textLabel.font = ConstantString.editTextFont
        textLabel.textColor = UIColor.black
        textLabel.widthAnchor.constraint(equalToConstant: vw.frame.size.width ).isActive = true
        textLabel.textAlignment = .left
        
        //Stack View
        let stackView   = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.horizontal
        stackView.distribution  = UIStackView.Distribution.fill
        stackView.alignment = UIStackView.Alignment.center
        stackView.spacing   = 20.0
        var text = ""
        
        if pickerView.tag == 1{
            textLabel.textAlignment = .center
            text =  arrFilterDays?[row] ?? ""
            stackView.addArrangedSubview(textLabel)
        }else if pickerView.tag == 2{
            stackView.addArrangedSubview(imageView)
            stackView.addArrangedSubview(textLabel)
            if row == 0{
                text = "All"
            }else{
                text =  self.clubListResponse?.result?[row - 1].strDisplayName ?? ""
                imageView.set_image(self.clubListResponse?.result?[row - 1].strProfilePicLink ?? "")
            }
            
            
        }else{
            stackView.addArrangedSubview(imageView)
            stackView.addArrangedSubview(textLabel)
            if row == 0{
                text = "All"
            }else{
                text =  self.subTeamListResponse?.arrSubTeam?[row - 1].name ?? ""
                imageView.set_image( self.subTeamListResponse?.arrSubTeam?[row - 1].banner_img_url ?? "")
            }
            
        }
        
        textLabel.text = text
        vw.addSubview(stackView)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.centerYAnchor.constraint(equalTo: vw.centerYAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: vw.leadingAnchor, constant: 10).isActive = true
        stackView.leadingAnchor.constraint(equalTo: vw.trailingAnchor, constant: 10).isActive = true
        
        return vw
        
    }
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //
    //        if pickerView.tag == 1{
    //            return arrFilterDays?[row]
    //        }else if pickerView.tag == 2{
    //            return self.clubListResponse?.result?[row].strDisplayName
    //        }else{
    //            return self.subTeamListResponse?.arrSubTeam?[row].name
    //        }
    //    }
    
}


extension LeaderboardViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if HarmonySingleton.shared.isHideForNewFlow{
            return UIView()
        }
        let headerView = UIView()
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "MemberheaderTableViewCell") as! MemberheaderTableViewCell
        headerCell.delegate = self
        if currentResultListType == .goals{
            headerCell.changeButtonState(type: 1)
        }else{
            headerCell.changeButtonState(type: 2)
        }
        headerCell.frame.size.width = tableView.frame.size.width
        headerView.addSubview(headerCell)
        return headerView
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
         
        var rowCunt = 0
        let noAvailalbeText = "Be the first person to\ncomplete your challenges!!"
        switch currentListType {
        
        case .club:
            if currentResultListType == .goals{
                rowCunt = self.clubWiseResponse?.result?.goalWise?.count ?? 0
            }else{
                rowCunt = self.clubWiseResponse?.result?.stepsWise?.count ?? 0
            }
            
        case .teams:
            if currentResultListType == .goals{
                rowCunt = self.lbTeamWiseRespnse?.result?.goalWise?.count ?? 0
            }else{
                rowCunt = self.lbTeamWiseRespnse?.result?.stepWise?.count ?? 0
            }
        case .users:
            if currentResultListType == .goals{
                rowCunt = self.userWiseResponse?.result?.goalWise?.count ?? 0
            }else{
                rowCunt = self.userWiseResponse?.result?.stepsWise?.count ?? 0
            }
        }
        
        var numOfSections: Int = 0
        if rowCunt == 0
        {
            numOfSections            = 1
            
            //Background view
            let noDataView:UIView = UIView(frame: CGRect(x: 20, y: 0, width: (tableView.bounds.size.width - 40), height: tableView.bounds.size.height))
            
            //Imageview
            let imgVw = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 100))
            imgVw.image = UIImage(named: "icon_befirstperson")
            imgVw.contentMode = .scaleAspectFit
            imgVw.center.x = noDataView.center.x
            imgVw.frame.origin.y = (noDataView.frame.size.height / 2) - 60
            noDataView.addSubview(imgVw)
            
            //Label
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 20, y: imgVw.frame.maxY , width: (noDataView.bounds.size.width - 40), height: 45))
            noDataLabel.text          = noAvailalbeText
            noDataLabel.font           = ConstantString.intFieldFont
            noDataLabel.textColor   = UIColor.init(hex: 0x7a7a7a)
            noDataLabel.numberOfLines = 0
            noDataLabel.lineBreakMode = .byWordWrapping
            noDataLabel.center.x = noDataView.center.x
            noDataLabel.textAlignment = .center
            noDataView.addSubview(noDataLabel)
        
            tableView.backgroundView  = noDataView
           
        }
        else
        {
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        return numOfSections
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if HarmonySingleton.shared.isHideForNewFlow{
            return 10
        }
        return 44
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch currentListType {
        
        case .club:
            if currentResultListType == .goals{
                return self.clubWiseResponse?.result?.goalWise?.count ?? 0
            }else{
                return self.clubWiseResponse?.result?.stepsWise?.count ?? 0
            }
            
        case .teams:
            if currentResultListType == .goals{
                return self.lbTeamWiseRespnse?.result?.goalWise?.count ?? 0
            }else{
                return self.lbTeamWiseRespnse?.result?.stepWise?.count ?? 0
            }
        case .users:
            if currentResultListType == .goals{
                return self.userWiseResponse?.result?.goalWise?.count ?? 0
            }else{
                return self.userWiseResponse?.result?.stepsWise?.count ?? 0
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MembersTableViewCell") as! MembersTableViewCell
        
        cell.selectionStyle = .none
        
        if currentResultListType == .goals{
            cell.imgStepOrGoal.image = UIImage(named: "goal_ball")
            cell.lblGoalOrStepTitle.text = "Total Goals"
        }else{
            cell.imgStepOrGoal.image = UIImage(named: "steps_new")
            cell.lblGoalOrStepTitle.text = "Total Steps"
        }
        
        cell.snoLbl.text = "\(indexPath.row + 1)"
        cell.vwDonation.isHidden = false
        cell.lblTeamName.isHidden = true
        cell.vwTeamLogo.isHidden = true
        cell.vwDonation.isHidden = !(leagueInfo?.isShowDonation ?? true)
        
        switch currentListType {
        
        case .club:
            var clubInfo : ClubWiseInfo?
            if currentResultListType == .goals{
                clubInfo = self.clubWiseResponse?.result?.goalWise?[indexPath.row]
                cell.lblGoalOrStep.text = "\(clubInfo?.intTotalGoals?.formatedValue() ?? "0")"
            }else{
                clubInfo = self.clubWiseResponse?.result?.stepsWise?[indexPath.row]
                cell.lblGoalOrStep.text = "\(clubInfo?.intTotalSteps?.formatedValue() ?? "0")"
            }
            
            
            cell.usernameLbl.text = clubInfo?.strTeamDisplayName
            cell.profilevw.set_image(clubInfo?.strTeamFlagLink ?? "")
            cell.lblDonation.text = "$\(clubInfo?.intTotalDonation?.formatedValue() ?? "0")"
            
        case .teams:
            var userInfo : LBUserInfo?
            
            if currentResultListType == .goals{
                userInfo = self.lbTeamWiseRespnse?.result?.goalWise?[indexPath.row]
                cell.lblGoalOrStep.text = "\(userInfo?.intTotalGoals?.formatedValue() ?? "0")"
            }else{
                userInfo = self.lbTeamWiseRespnse?.result?.stepWise?[indexPath.row]
                cell.lblGoalOrStep.text = "\(userInfo?.intTotalSteps?.formatedValue() ?? "0")"
            }
            
            cell.usernameLbl.text = userInfo?.strTeamDisplayName
            cell.profilevw.set_image(userInfo?.strTeamFlagLink ?? "")
            cell.lblDonation.text = "$\(userInfo?.intTotalDonation?.formatedValue() ?? "0")"
            
        case .users:
            var userInfo : UserWiseInfo?
            if currentResultListType == .goals{
                userInfo = self.userWiseResponse?.result?.goalWise?[indexPath.row]
                cell.lblGoalOrStep.text = "\(userInfo?.strTotalGoals?.formatedValue() ?? "0")"
            }else{
                userInfo = self.userWiseResponse?.result?.stepsWise?[indexPath.row]
                cell.lblGoalOrStep.text = "\(userInfo?.strTotalSteps?.formatedValue() ?? "0")"
            }
            cell.usernameLbl.text = userInfo?.strDisplayName?.capitalized
            cell.profilevw.imageFromURL(urlString: userInfo?.strProfilePicLink ?? "")
            cell.lblDonation.text = "$\(userInfo?.strTotalDonation?.formatedValue() ?? "0")"
            
            if teamSelectRow == 0{
                cell.vwDonation.isHidden = true
                cell.lblTeamName.isHidden = false
                cell.vwTeamLogo.isHidden = false
                cell.lblTeamName.text = userInfo?.strTeamName
                cell.imgTeamLogo.set_image(userInfo?.strTeamFlagLink ?? "")
            }
        }
       
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }
}


extension LeaderboardViewController{
    
    
    func doAPICall(){
        
        switch currentListType {
        case .club:
            getListClubWise()
        case .teams:
            getListTeamWise()
        case .users:
            getListUserWise()
        }
        showHideTopPerfromerLabel()
    }
    
    func showHideTopPerfromerLabel(){
        
        lblTopTeamTItle.text = "Top Performers"
        
        if currentListType == .users{
            if btnTeamList.titleLabel?.text == leagueInfo?.strSubTeamName{
                lblTopTeamTItle.text = "My Team - \(leagueInfo?.strSubTeamName ?? "")"
            }
        }
    }

    
    func getListUserWise(){
        
        showLoader()
        if Reachability.isConnectedToNetwork() {
            
            //Params
            if let selectDay = FilterDays(rawValue: arrFilterDays?[selectedDayRow] ?? ""){
                selectedFilterDay = selectDay
            }
         //  let (fromDate, toDate) = self.selectedFilterDay.getFromAndToDate(leagueStartDate: leagueInfo?.leagueStartDate ?? "")
            let (fromDate, toDate) = (leagueInfo?.leagueStartDate ?? "" , Date().getFilterDateFormat())
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
            
            if clubSelectRow == 0{
                data["teamId"] = ""
            }else{
                if self.clubListResponse?.result?.count ?? 0 > (self.clubSelectRow - 1){
                    if let clubInfo = self.clubListResponse?.result?[self.clubSelectRow - 1]{
                        data["teamId"] = clubInfo.strTeamId
                    }
                }
            }
            
            
            if teamSelectRow == 0{
                data["subteamId"] = ""
            }else{
                if self.subTeamListResponse?.arrSubTeam?.count ?? 0 > (self.teamSelectRow - 1){
                    if let teamInfo = self.subTeamListResponse?.arrSubTeam?[self.teamSelectRow - 1]{
                        data["subteamId"] = teamInfo._id
                    }
                }
            }
            
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getUserWiseLeaderBoard(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            
                           // self.userWiseResponse = Mapper<UserWiseResponse>().map(JSON: value)
                            var userResponse = Mapper<UserWiseResponse>().map(JSON: value)
                            
                            if HarmonySingleton.shared.isHideForNewFlow == false{
                                userResponse?.result?.goalWise?.removeAll(where: { (userWiseInfo) -> Bool in
                                    return (userWiseInfo.strTotalGoals == 0) ||  (userWiseInfo.strTotalGoals == nil)
                                })
                                
                            
                                userResponse?.result?.stepsWise?.removeAll(where: { (userWiseInfo) -> Bool in
                                    return (userWiseInfo.strTotalSteps == 0) ||  (userWiseInfo.strTotalSteps == nil)
                                })
                            }
                           
                            
                            self.userWiseResponse = userResponse
                           /*
                            var totalSteps = 0
                            if let userResp = userResponse{
                                if let arrUser = userResp.result?.stepsWise{
                                    for userWise in arrUser{
                                        if let strSteps = userWise.strTotalSteps{
                                            totalSteps =  totalSteps + strSteps
                                        }
                                       
                                    }
                                }
                            }
                            print("Total Steps")
                            print(totalSteps)
                            */
                        }else{
                            
                        }
                    case .failure(let error):
                        print(error)
                        
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
    
    
    func getListTeamWise(){
        showLoader()
        if Reachability.isConnectedToNetwork() {
            //Params
            if let selectDay = FilterDays(rawValue: arrFilterDays?[selectedDayRow] ?? ""){
                selectedFilterDay = selectDay
            }
           // let (fromDate, toDate) = self.selectedFilterDay.getFromAndToDate(leagueStartDate: leagueInfo?.leagueStartDate ?? "")
            let (fromDate, toDate) = (leagueInfo?.leagueStartDate ?? "" , Date().getFilterDateFormat())
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
            
            if clubSelectRow == 0{
                data["teamId"] = ""
            }else{
                if self.clubListResponse?.result?.count ?? 0 > (self.clubSelectRow - 1){
                    if let clubInfo = self.clubListResponse?.result?[self.clubSelectRow - 1]{
                        data["teamId"] = clubInfo.strTeamId
                    }
                }
            }
            
            data["subteamId"] = ""
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getTeamWiseLeaderBoard(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            
                           // self.lbTeamWiseRespnse = Mapper<LBTeamWiseResponse>().map(JSON: value)
                            var resultResponse = Mapper<LBTeamWiseResponse>().map(JSON: value)
                            
                            resultResponse?.result?.goalWise?.removeAll(where: { (resultInfo) -> Bool in
                                return (resultInfo.intTotalGoals == 0) ||  (resultInfo.intTotalGoals == nil)
                            })
                            
                        
                            resultResponse?.result?.stepWise?.removeAll(where: { (resultInfo) -> Bool in
                                return (resultInfo.intTotalSteps == 0) ||  (resultInfo.intTotalSteps == nil)
                            })
                            
                            self.lbTeamWiseRespnse = resultResponse
                            
                        }else{
                            
                        }
                    case .failure(let error):
                        print(error)
                        
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
    }
    func getListClubWise(){
        
        showLoader()
        if Reachability.isConnectedToNetwork() {
            //Params
            if let selectDay = FilterDays(rawValue: arrFilterDays?[selectedDayRow] ?? ""){
                selectedFilterDay = selectDay
            }
           // let (fromDate, toDate) = self.selectedFilterDay.getFromAndToDate(leagueStartDate: leagueInfo?.leagueStartDate ?? "")
            let (fromDate, toDate) = (leagueInfo?.leagueStartDate ?? "" , Date().getFilterDateFormat())
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
            data["teamId"] = ""
            data["subteamId"] = ""
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getClubWiseLeaderBoard(params: data)
            print((url, method, param))
          
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                DispatchQueue.main.async {
                    self.hideLoader()
                   
                    switch response.result {
                     
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            print("Club List Response")
                            print(value)
                           // self.clubWiseResponse = Mapper<ClubWiseResponse>().map(JSON: value)
                            
                            var resultResponse = Mapper<ClubWiseResponse>().map(JSON: value)
                            
                            resultResponse?.result?.goalWise?.removeAll(where: { (resultInfo) -> Bool in
                                return resultInfo.intTotalGoals == 0 || resultInfo.intTotalGoals == nil
                            })
                            
                            resultResponse?.result?.stepsWise?.removeAll(where: { (resultInfo) -> Bool in
                                return resultInfo.intTotalSteps == 0 ||  resultInfo.intTotalSteps == nil
                            })
                            
                            self.clubWiseResponse = resultResponse
                            
                        }else{
                            
                        }
                    case .failure(let error):
                        print(error)
                        
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
    func getTeamList(){
        showLoader()
        if Reachability.isConnectedToNetwork() {
            //Params
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getClubListOfLeague(params: data)
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
               
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            self.clubListResponse = Mapper<TeamListResponse>().map(JSON: value)
                            self.setClubName()
                            self.getSubTeamList(isOwnTeam: true)
                        }
                    case .failure(let error):
                        print(error)
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
    
    func  getSubTeamList(isOwnTeam: Bool = false){
        showLoader()
        if Reachability.isConnectedToNetwork() {
            //Params
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
//            data["teamId"] = self.clubListResponse?.result?[self.clubSelectRow - 1].strTeamId
            data["teamId"] = leagueInfo?.strTeamID

            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getSubteamList(params: data)
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            self.subTeamListResponse = Mapper<SubTeamListResponse>().map(JSON: value)
                            
                            if isOwnTeam && self.dashBoardType == 1{
                                self.ownSubTeamListResponse = self.subTeamListResponse
                                self.btnclub.isSelected = false
                                self.btnteam.isSelected =  false
                                self.btnusers.isSelected = true
                                self.teamSelectRow = 0
                                self.doProcessTeamSelection(type: 3)
                            }
                            else if isOwnTeam{
                                self.ownSubTeamListResponse = self.subTeamListResponse
                                self.chooseType(type: self.currentListType.rawValue)
                            }
                        }
                    case .failure(let error):
                        print(error)
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
    }
}

extension LeaderboardViewController : HeaderTableDeleage{
    func selectType(selectType: Int) {
        currentResultListType = ResultListType(rawValue: selectType) ?? .goals
        leaderboard.reloadData()
        showUserPosition()
    }
}


extension LeaderboardViewController : TeamSelectionDelegate{
    func selectIndex(index: Int, isClub: Bool) {
        var type = 0
        if isClub{
            clubSelectRow = index
            type = 2
        }else{
            
         teamSelectRow = index
            type = 3
        }
        self.doProcessTeamSelection(type: type)
    }
}
