//
//  MemberheaderTableViewCell.swift
//  Harmoney
//
//  Created by Mohanraj on 09/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

protocol HeaderTableDeleage : NSObject{
    func selectType(selectType : Int)
}

class MemberheaderTableViewCell: UITableViewCell {

    weak var delegate : HeaderTableDeleage?
    @IBOutlet weak var btnsteps: UIButton!
    @IBOutlet weak var btngoals: UIButton!
    @IBOutlet weak var goalsvw: UIView!
    @IBOutlet weak var stepsvw: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onChooseType(_ sender: UIButton) {
        self.delegate?.selectType(selectType: sender.tag)
        
    }
    
    func changeButtonState(type : Int){
        btnsteps.isSelected = false
        btngoals.isSelected = false
        
        goalsvw.isHidden = true
        stepsvw.isHidden = true
        
        if type == 1{
            btngoals.isSelected = true
            goalsvw.isHidden = false
        }else{
            btnsteps.isSelected = true
            stepsvw.isHidden = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
