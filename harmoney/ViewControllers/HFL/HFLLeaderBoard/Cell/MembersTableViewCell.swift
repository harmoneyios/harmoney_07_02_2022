//
//  MembersTableViewCell.swift
//  Harmoney
//
//  Created by Mohanraj on 09/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class MembersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var snoLbl: UILabel!

    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var profilevw: UIImageView!
    
    @IBOutlet weak var imgStepOrGoal: UIImageView!
    @IBOutlet weak var lblGoalOrStep: UILabel!
    
    @IBOutlet weak var lblGoalOrStepTitle: UILabel!
    @IBOutlet weak var lblDonation: UILabel!
    
    @IBOutlet weak var vwDonation: UIStackView!
    
    @IBOutlet weak var imgTeamLogo: UIImageView!
    @IBOutlet weak var vwTeamLogo: UIView!
    @IBOutlet weak var lblTeamName: UILabel!
    static let identifer = "MembersTableviewCell"
    
    //goal_ball
    //steps_blud
    
    //Total Goals
    //Total Steps
    
    static func nib()->UINib{
        return UINib(nibName: "MembersTableViewCell", bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        vwBottom.bottomCorners(radius: 10)
        topview.topCorners(radius: 10)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    
    
}
}
