//
//  TeamListViewController.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD
import Lottie
class TeamListViewController: UIViewController {

    // Outlets
    @IBOutlet weak var vwAnimation: UIView!
    
    @IBOutlet weak var txtLegueDescription: UITextView!
    @IBOutlet weak var imgOrganizedLogo: UIImageView!
    @IBOutlet weak var lblOrganized: UILabel!
    @IBOutlet weak var imgLeagueLogo: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var vwNavBar: UIView!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var btnGrid: UIButton!
    @IBOutlet weak var collVwTeams: UICollectionView!
    
    var animationView =  AnimationView()
    
    // Properties
    var clubListResponse : TeamListResponse?
    var leagueInfo : BannerLeagueInfo?
    var arrAPI = [Any]()

    var viewType : TeamViewType?{
        didSet{
            chanageButtonsState()
        }
    }
    
    enum TeamViewType: Int{
        case gridView = 1
        case listView = 2
    }
    
    // ViewLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        vwAnimation.isHidden = true
        initalSetup()
        getTeamList()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavBar()
    }
    private func chanageButtonsState(){
        btnGrid.tintColor = .lightGray
        btnList.tintColor = .lightGray
        if  viewType == .gridView{
            btnGrid.tintColor = .darkGray
        }else{
            btnList.tintColor = .darkGray
        }
        self.collVwTeams.performBatchUpdates({
                            let indexSet = IndexSet(integersIn: 0...0)
                            self.collVwTeams.reloadSections(indexSet)
                        }, completion: nil)

    }
    
    // Show view type based on user selection
    @IBAction func onGridListClick(_ sender: UIButton) {
        //Tag 1 Grid
        //Tag 2 List
        viewType = TeamViewType(rawValue: sender.tag)
      
    }
    
    
    // Back button action
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TeamListViewController{
    
    private func updateUI(){
        txtLegueDescription.text = clubListResponse?.league?.description
        lblOrganized.text = clubListResponse?.leagueInfo?.organizedText
        imgLeagueLogo.set_image(clubListResponse?.league?.league_img_url ?? "")
        imgOrganizedLogo.set_image(clubListResponse?.league?.sponsor ?? "")
        self.collVwTeams.performBatchUpdates({
                            let indexSet = IndexSet(integersIn: 0...0)
                            self.collVwTeams.reloadSections(indexSet)
                        }, completion: nil)
       
    }
    
    // Get club list from API
    func getTeamList(){
        showLoader()
        if Reachability.isConnectedToNetwork() {
            
            //Params
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getClubListOfLeague(params: data)
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    self.hideLoader()
                        switch response.result {
                        case .success(let value):
                            if let value = value as? [String:Any] {
                                //Update UI after API Success
                                self.clubListResponse = Mapper<TeamListResponse>().map(JSON: value)
                                self.updateUI()
                            }
                        case .failure(let error):
                            print(error)
                            AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                        }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
    }
}

// MARK: - Private Methods

extension TeamListViewController{
    private func initalSetup(){
        viewType = .gridView
        registerCells()
        
    }
    private func registerCells(){
        collVwTeams.register(CollCellTeamListView.nib(), forCellWithReuseIdentifier: CollCellTeamListView.identifier)
        collVwTeams.register(CollCellTeamGridView.nib(), forCellWithReuseIdentifier: CollCellTeamGridView.identifier)
        collVwTeams.delegate = self
        collVwTeams.dataSource = self
        
    }
    private func setUpNavBar(){
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = CGRect(x: vwNavBar.bounds.origin.x, y: vwNavBar.bounds.origin.y, width: UIScreen.main.bounds.width, height: vwNavBar.bounds.height)
        vwNavBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
    }
    
}

// MARK: - Collectionview delegate Methods
extension TeamListViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //showAniamtionView()
        let subTeamVC = GlobalStoryBoard().subTeamListViewController
        subTeamVC.arrClubList = self.clubListResponse?.result
        subTeamVC.leagueInfo = self.leagueInfo
        subTeamVC.currentIndex = indexPath.row
        subTeamVC.arrAPI = arrAPI
        subTeamVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(subTeamVC, animated: true)

    }
    
    func showAniamtionView(){
        vwAnimation.isHidden = false
        vwAnimation.backgroundColor = .black
        animationView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        vwAnimation.addSubview(animationView)
        
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.leadingAnchor.constraint(equalTo: vwAnimation.leadingAnchor,constant: 0).isActive = true
        animationView.trailingAnchor.constraint(equalTo: vwAnimation.trailingAnchor,constant: 0).isActive = true
        
        animationView.topAnchor.constraint(equalTo: vwAnimation.topAnchor,constant: 0).isActive = true
        animationView.bottomAnchor.constraint(equalTo: vwAnimation.bottomAnchor,constant: 0).isActive = true

        let path = Bundle.main.path(forResource: "Event_Join_Animation",
                                    ofType: "json") ?? ""
        animationView.animation = Animation.filepath(path)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.isUserInteractionEnabled = false
       
        animationView.play()
        //successAnimationFour
        //successAnimationThree
        //successAnimationTw0
        //steps
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        vwAnimation.isHidden = true
    }
}

// MARK: - Collectionview datasource Methods
extension TeamListViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.clubListResponse?.result?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if viewType == .gridView{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollCellTeamGridView.identifier, for: indexPath) as? CollCellTeamGridView else {
                return UICollectionViewCell()
            }
            
            if let result = self.clubListResponse?.result?[indexPath.row]{
                cell.lblClubName.text = result.strDisplayName
                cell.imgClubLogo.imageFromURL(urlString: result.strProfilePicLink ?? "")
            }
            //strDisplayName
            return cell
        }else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollCellTeamListView.identifier, for: indexPath) as? CollCellTeamListView else {
                return UICollectionViewCell()
            }
            if let result = self.clubListResponse?.result?[indexPath.row]{
                cell.lblClubName.text = result.strDisplayName
                cell.imgVwClub.imageFromURL(urlString: result.strProfilePicLink ?? "")
            }
            return cell
        }
    }
    
}

// MARK: - Collectionview flowlayout Methods
extension TeamListViewController: UICollectionViewDelegateFlowLayout{
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if viewType == .gridView{
            let cellSize = (UIScreen.main.bounds.width - 60) / 3
            return CGSize(width: cellSize, height: cellSize + 25)
        }else{
            let cellSize = (UIScreen.main.bounds.width - 40)
            return CGSize(width: cellSize, height: 80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if viewType == .gridView{
        return 20
        }else{
            return 5
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }*/
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if viewType == .gridView{
            let yourWidth = collectionView.bounds.width/3.0
            let yourHeight = yourWidth

            return CGSize(width: yourWidth, height: yourHeight)

        }else{
            let cellSize = (UIScreen.main.bounds.width - 40)
            return CGSize(width: cellSize, height: 80)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
