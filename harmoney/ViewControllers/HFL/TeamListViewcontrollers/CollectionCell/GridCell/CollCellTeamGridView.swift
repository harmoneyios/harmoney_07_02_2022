//
//  CollCellTeamGridView.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class CollCellTeamGridView: UICollectionViewCell {
    static let identifier = "CollCellTeamGridView"
    static func nib()->UINib{
       return UINib(nibName: CollCellTeamGridView.identifier, bundle: nil)
    }
    @IBOutlet weak var lblClubName: UILabel!
    @IBOutlet weak var imgClubLogo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
