//
//  CollCellTeamListView.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class CollCellTeamListView: UICollectionViewCell {
static let identifier = "CollCellTeamListView"
    
    @IBOutlet weak var lblClubName: UILabel!
    @IBOutlet weak var imgVwClub: UIImageView!
    static func nib()->UINib{
       return UINib(nibName: CollCellTeamListView.identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
