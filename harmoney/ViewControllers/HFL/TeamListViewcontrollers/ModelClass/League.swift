//
//  League.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.


import Foundation
import ObjectMapper

struct League : Mappable {
	var _id : String?
	var name : String?
	var short_name : String?
	var sponsor : String?
	var description : String?
	var bannerImgUrl : String?
	var league_img_url : String?
	var fromDate : String?
	var toDate : String?
	var leagueThroughKm : String?
	var km : String?
	var notification : String?
	var singleTeam : String?
	var organizedText : String?
	var status : String?
	var teamId : TeamId?
	var created : String?
	var teamDetails : TeamDetails?
	var sponsors : Sponsors?
	var ngo : [Ngo]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		name <- map["name"]
		short_name <- map["short_name"]
		sponsor <- map["sponsor"]
		description <- map["description"]
		bannerImgUrl <- map["bannerImgUrl"]
		league_img_url <- map["league_img_url"]
		fromDate <- map["fromDate"]
		toDate <- map["toDate"]
		leagueThroughKm <- map["leagueThroughKm"]
		km <- map["km"]
		notification <- map["notification"]
		singleTeam <- map["singleTeam"]
		organizedText <- map["organizedText"]
		status <- map["status"]
		teamId <- map["teamId"]
		created <- map["created"]
		teamDetails <- map["teamDetails"]
		sponsors <- map["sponsors"]
		ngo <- map["ngo"]
	}

}
