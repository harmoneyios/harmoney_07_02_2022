//
//  Ngo.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.


import Foundation
import ObjectMapper

struct Ngo : Mappable {
	var _id : String?
	var name : String?
	var short_name : String?
	var flag : String?
	var banner_img_url : String?
	var type : String?
	var status : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		name <- map["name"]
		short_name <- map["short_name"]
		flag <- map["flag"]
		banner_img_url <- map["banner_img_url"]
		type <- map["type"]
		status <- map["status"]
	}

}
