//
//  LeagueInfo.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.


import Foundation
import ObjectMapper

struct LeagueInfo : Mappable {
	var organizedText : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		organizedText <- map["organizedText"]
	}

}
