//
//  TeamResult.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.


import Foundation
import ObjectMapper

struct TeamResult : Mappable {
	var strProfilePicLink : String?
	var strDisplayName : String?
	var strTeamId : String?
	var strTeamFullName : String?
	var strDate : String?
	var strTeamShortName : String?
	var strDescription : String?
	var strSponsorFlagLink : String?
	var strNgoFlagLink : String?
	var strSponsorBannerUrl : String?
	var strNgoBannerUrl : String?
	var intTotalUsers : Int?
	var intTotalRun : Int?
	var intAvgRuns : Int?
	var intMaxRuns : Int?
	var intTotalkarmaPoints : Int?
	var intAvgkarmaPoints : Int?

	init?(map: Map) {

	}
    init(clubName: String,clubLogo:String,clubId:String){
        self.strDisplayName = clubName
        self.strProfilePicLink = clubLogo
        self.strTeamId = clubId
         }
	mutating func mapping(map: Map) {

		strProfilePicLink <- map["strProfilePicLink"]
		strDisplayName <- map["strDisplayName"]
		strTeamId <- map["strTeamId"]
		strTeamFullName <- map["strTeamFullName"]
		strDate <- map["strDate"]
		strTeamShortName <- map["strTeamShortName"]
		strDescription <- map["strDescription"]
		strSponsorFlagLink <- map["strSponsorFlagLink"]
		strNgoFlagLink <- map["strNgoFlagLink"]
		strSponsorBannerUrl <- map["strSponsorBannerUrl"]
		strNgoBannerUrl <- map["strNgoBannerUrl"]
		intTotalUsers <- map["intTotalUsers"]
		intTotalRun <- map["intTotalRun"]
		intAvgRuns <- map["intAvgRuns"]
		intMaxRuns <- map["intMaxRuns"]
		intTotalkarmaPoints <- map["intTotalkarmaPoints"]
		intAvgkarmaPoints <- map["intAvgkarmaPoints"]
	}

}
