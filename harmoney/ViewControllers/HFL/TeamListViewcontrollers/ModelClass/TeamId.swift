//
//  TeamId.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.


import Foundation
import ObjectMapper

struct TeamId : Mappable {
	var team : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		team <- map["team"]
	}

}
