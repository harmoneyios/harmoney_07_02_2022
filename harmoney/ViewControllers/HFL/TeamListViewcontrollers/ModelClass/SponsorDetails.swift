//
//  SponsorDetails.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.


import Foundation
import ObjectMapper

struct SponsorDetails : Mappable {
	var _id : String?
	var name : String?
	var short_name : String?
	var ngo_name : String?
	var code : String?
	var logo_image_url : String?
	var banner_image_url : String?
	var ngo_logo_image_url : String?
	var status : String?
	var created : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		name <- map["name"]
		short_name <- map["short_name"]
		ngo_name <- map["ngo_name"]
		code <- map["code"]
		logo_image_url <- map["logo_image_url"]
		banner_image_url <- map["banner_image_url"]
		ngo_logo_image_url <- map["ngo_logo_image_url"]
		status <- map["status"]
		created <- map["created"]
	}

}
