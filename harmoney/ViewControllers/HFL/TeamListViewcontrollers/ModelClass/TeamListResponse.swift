//
//  TeamListResponse.swift
//  Harmoney
//
//  Created by Saravanan on 07/10/21.
//  Copyright © 2021 harmoney. All rights reserved.

import Foundation
import ObjectMapper

struct TeamListResponse : Mappable {
	var status : Int?
	var leagueInfo : LeagueInfo?
	var result : [TeamResult]?
	var league : League?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		status <- map["status"]
		leagueInfo <- map["leagueInfo"]
		result <- map["result"]
		league <- map["league"]
	}

}
