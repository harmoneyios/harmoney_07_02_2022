//
//  HFLDonationViewContoller.swift
//  Harmoney
//
//  Created by Saravanan on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

class HFLDonationViewContoller: UIViewController {

    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var lblTotalDonations: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var vwTopUserInfo: UIView!
    
    
    @IBOutlet weak var vwDonation: UIStackView!
    
    
    var leagueInfo : BannerLeagueInfo?
    var hflDashBoardResponse : HFLDashBoardResponse?
    var isReloadUI : Bool?{
        didSet{
            if isReloadUI ?? false{
                self.updateUI()
            }
        }
    }
    var lbTeamWiseRespnse : LBTeamWiseResponse?
    var isLoading : Bool?{
        didSet{
            self.updateLoadingstatus()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        
        // Do any additional setup after loading the view.
    }
    
    func updateLoadingstatus(){
        if isLoading == true{
            self.showLoader()
        }else{
            hideLoader()
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    @IBAction func onDonation(_ sender: Any) {
        
    }
    @IBAction func onShare(_ sender: Any) {
        AlertManager.shared.shareScreenToSocialMedia(controller: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HFLDonationViewContoller {
    func initalSetup(){
        vwTopUserInfo.isHidden = true
        registerCells()
        getDonationDetails()
    }
    func registerCells(){
        tblVw.register(TCellDonations.nib(), forCellReuseIdentifier: TCellDonations.identifier)
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.reloadData()
    }
    
}


extension HFLDonationViewContoller: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension HFLDonationViewContoller: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.lbTeamWiseRespnse?.result?.stepWise?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TCellDonations.identifier) as? TCellDonations else{
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        
        if let userInfo = self.lbTeamWiseRespnse?.result?.stepWise?[indexPath.row]{
            cell.lblClubName.text = self.hflDashBoardResponse?.response?.userProfile?.strMyTeamName
            cell.imgTeamLogo.set_image(userInfo.strTeamFlagLink ?? "")
            cell.lblTeamName.text = userInfo.strTeamDisplayName
            cell.lblTotalUsers.text = "\(userInfo.intTotalUsers ?? 0)"
            cell.totalDonations.text =  "\(userInfo.intTotalDonation?.formatedValue() ?? "0")"
        }
        return cell
    }
}

extension HFLDonationViewContoller{

    func updateUI(){
        vwTopUserInfo.isHidden = false
        lblTotalDonations.text = self.hflDashBoardResponse?.response?.userProfile?.intTotalDonation
        imgUserProfile.imageFromURL(urlString: self.hflDashBoardResponse?.response?.userProfile?.strProfilePicLink ?? "")
        lblUserName.text = self.hflDashBoardResponse?.response?.userProfile?.strDisplayName?.capitalized
        tblVw.reloadData()
    }
    
    func getDonationDetails(){
        
        DispatchQueue.global().async {
            self.isLoading = true
            let dispatchGroup = DispatchGroup()
            // Get Teamwise info
            dispatchGroup.enter()
            self.getTeamWise {
                dispatchGroup.leave()
            } failed: {
                dispatchGroup.suspend()
            }
            dispatchGroup.wait()
            
            //Get Dashboard details
            dispatchGroup.enter()
            self.getDashBoardDetails {
                dispatchGroup.leave()
            } failed: {
                dispatchGroup.suspend()
            }
            dispatchGroup.wait()
            
            dispatchGroup.notify(queue: .main) {
                self.isLoading = false
                self.isReloadUI = true
            }
        }
    }
    func getDashBoardDetails(success: @escaping () -> Void,failed: @escaping () -> Void){
        
        if Reachability.isConnectedToNetwork() {
            //Params
            let (fromDate, toDate) =  (leagueInfo?.leagueStartDate ?? "",Date().getFilterDateFormat())
            
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.currentLeague?.leagueId
            data["teamId"] = leagueInfo?.currentLeague?.teamId
            data["subteamId"] = leagueInfo?.currentLeague?.subteamId
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getHFLDashBoardDetails(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                   
                        switch response.result {
                        case .success(let value):
                            if let value = value as? [String:Any] {
                                //Update UI after API Success
                                self.hflDashBoardResponse = Mapper<HFLDashBoardResponse>().map(JSON: value)
                                success()
                            }else{
                                failed()
                            }
                        case .failure(let error):
                            print(error)
                            failed()
                            AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                        }
                }
            }
        }else{
            failed()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
    func getTeamWise(success: @escaping () -> Void,failed: @escaping () -> Void){
    
        if Reachability.isConnectedToNetwork() {
            
            //Params
            let (fromDate, toDate) =  (leagueInfo?.leagueStartDate ?? "",Date().getFilterDateFormat())
            
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.currentLeague?.leagueId
            data["teamId"] = leagueInfo?.currentLeague?.teamId
            data["subteamId"] = leagueInfo?.currentLeague?.subteamId
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getTeamWiseLeaderBoard(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                DispatchQueue.main.async {
                  
                        switch response.result {
                        case .success(let value):
                            if let value = value as? [String:Any] {
                                //Update UI after API Success
                                self.lbTeamWiseRespnse = Mapper<LBTeamWiseResponse>().map(JSON: value)
                                success()
                            }else{
                                failed()
                            }
                        case .failure(let error):
                            print(error)
                            failed()
                            AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                        }
                }
            }
        }else{
            failed()
            self.view.toast("Internet Connection not Available!")
        }
    }
}
