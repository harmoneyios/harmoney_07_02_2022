//
//  TCellDonations.swift
//  Harmoney
//
//  Created by Saravanan on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class TCellDonations: UITableViewCell {

    @IBOutlet weak var totalDonations: UILabel!
    @IBOutlet weak var lblTotalUsers: UILabel!
    static let identifier = "TCellDonations"
    @IBOutlet weak var lblClubName: UILabel!
    
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var imgTeamLogo: UIImageView!
    
    @IBOutlet weak var vwDonation: UIStackView!
    static func nib()->UINib{
       return UINib(nibName: TCellDonations.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
