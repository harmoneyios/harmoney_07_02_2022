//
//  SubTeamListViewController.swift
//  Harmoney
//
//  Created by Saravanan on 11/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

class SubTeamListViewController: UIViewController {
    
    @IBOutlet weak var vwNavBar: UIView!
    @IBOutlet weak var collVwTeamList: UICollectionView!
    @IBOutlet weak var tblVwSubTeam: UITableView!
    @IBOutlet weak var lblClubName: UILabel!
    @IBOutlet weak var lblTotalCount: UILabel!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPervious: UIButton!
    var currentIndex : Int = 0
    var arrClubList :  [TeamResult]?
    var leagueInfo : BannerLeagueInfo?
    
    var arrAPI = [Any]()

    var subTeamListResponse : SubTeamListResponse?{
        didSet{
            self.updateUI()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavBar()
    }
    @IBAction func onPerviousTeam(_ sender: Any) {
        if currentIndex != 0{
            currentIndex = currentIndex - 1
            changeClub(position: .right, animation: true)
        }
    }
    
    func changeClub(position : UICollectionView.ScrollPosition, animation : Bool = false){
        collVwTeamList.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at: position, animated: animation)
            btnPervious.isEnabled = true
            btnNext.isEnabled = true
        if currentIndex == 0{
            btnPervious.isEnabled = false
        }
        
        if currentIndex == (self.arrClubList!.count - 1){
            btnNext.isEnabled = false
        }
        getSubTeamList()
   
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onNextTeam(_ sender: Any) {
        if currentIndex != arrClubList!.count - 1{
            currentIndex = currentIndex + 1
            self.changeClub(position: .left, animation: true)
            
        }
    }
 
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SubTeamListViewController{
    func initalSetup(){
        if arrClubList?.count == 1{
            btnNext.isHidden = true
            btnPervious.isHidden = true
        }
        registerCells()
       
    }
    
    private func setUpNavBar(){
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = CGRect(x: vwNavBar.bounds.origin.x, y: vwNavBar.bounds.origin.y, width: UIScreen.main.bounds.width, height: vwNavBar.bounds.height)
        vwNavBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
    }
    
    func registerCells(){
        
        collVwTeamList.register(CollCellClub.nib(), forCellWithReuseIdentifier: CollCellClub.identifier)
        collVwTeamList.delegate = self
        collVwTeamList.dataSource = self
        collVwTeamList.isPagingEnabled = true
        collVwTeamList.isUserInteractionEnabled = false
       
        tblVwSubTeam.register(TCellSubTeam.nib(), forCellReuseIdentifier: TCellSubTeam.identifier)
        tblVwSubTeam.delegate = self
        tblVwSubTeam.dataSource = self
        tblVwSubTeam.tableFooterView = UIView()
        changeClub(position: .left, animation:  false)
    }
    
}

extension SubTeamListViewController:UICollectionViewDelegate{
    
}

extension SubTeamListViewController:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return arrClubList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollCellClub.identifier, for: indexPath) as? CollCellClub{
            
            if let result = arrClubList?[indexPath.row]{
                //cell.imgClubLogo.set_image(result.strProfilePicLink ?? "")
                cell.imgClubLogo.imageFromURL(urlString: result.strProfilePicLink ?? "")
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 67
    }
    
    
}

extension SubTeamListViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 113, height: 113)
    }
}
extension SubTeamListViewController{
    
    func updateUI(){
        if let currentClubInfo = self.arrClubList?[currentIndex]{
            self.lblClubName.text = currentClubInfo.strDisplayName
            
         if let totUsers = self.subTeamListResponse?.arrSubTeam?.reduce(0, { (result, teaminfo) -> Int in
            return result + (teaminfo.max_member_count ?? 0)
         }){
            lblTotalCount.text = "\(totUsers)"
         }
    
            
        }
        self.tblVwSubTeam.reloadData()
        
    }
    func getSubTeamList(){
        showLoader()
        if Reachability.isConnectedToNetwork() {
            //Params
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
            data["teamId"] = self.arrClubList?[self.currentIndex].strTeamId
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getSubteamList(params: data)
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    self.hideLoader()
                        switch response.result {
                        case .success(let value):
                            if let value = value as? [String:Any] {
                                //Update UI after API Success
                                self.subTeamListResponse = Mapper<SubTeamListResponse>().map(JSON: value)
                            }
                        case .failure(let error):
                            print(error)
                            AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                        }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
    }
}


extension SubTeamListViewController : UITableViewDelegate{
    
}

extension SubTeamListViewController: UITableViewDataSource{
   
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        if (self.subTeamListResponse?.arrSubTeam?.count ?? 0 > 0)
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "Sub team not available"
            noDataLabel.font          = ConstantString.labelTitileFont
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.subTeamListResponse?.arrSubTeam?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TCellSubTeam.identifier) as? TCellSubTeam else{
            return UITableViewCell()
        }
        let subTeamInfo = self.subTeamListResponse?.arrSubTeam?[indexPath.row]
        cell.lblSubTeamName.text = subTeamInfo?.name
        //cell.imgVwSubTeam.set_image(subTeamInfo?.banner_img_url ?? "")
        cell.imgVwSubTeam.imageFromURL(urlString: subTeamInfo?.banner_img_url ?? "")
        cell.selectionStyle = .none
        if let maxCount = subTeamInfo?.max_member_count, let registrationCount = subTeamInfo?.registrationCount, let remainingCount = subTeamInfo?.availableCount{
            if maxCount == registrationCount {
                cell.lblSlot.text = "(all slots filled)"
                cell.lblSubTeamName.textColor = UIColor.init(hex: 0x7D7D7D)
                cell.lblSlot.textColor = UIColor.init(hex: 0xA8A8A8)
                cell.vwBg.backgroundColor = UIColor.init(hex: 0xF3F3F3)
                
                cell.btnJoin.isHidden = true
            }else{
                cell.lblSlot.text = "(\(remainingCount) of \(maxCount) slots are left)"
                cell.lblSubTeamName.textColor = UIColor.init(hex: 0x1E3F66)
                cell.lblSlot.textColor = UIColor.init(hex: 0x565656)
                cell.vwBg.backgroundColor = UIColor.init(hex: 0xF2F6FB)
                
                cell.btnJoin.isHidden = false
                cell.btnJoin.tag = indexPath.row
                cell.btnJoin.addTarget(self, action: #selector(onJoinTeam(sender:)), for: .touchUpInside)
                cell.btnJoin.backgroundColor = UIColor.harmoneyDarkBlueColor
            }
        }
        
        return cell
    }
    
   @objc func onJoinTeam(sender : UIButton){
    if let subTeamInfo = self.subTeamListResponse?.arrSubTeam?[sender.tag]{
            doJoinTeam(subTeamInfo: subTeamInfo)
        }
    }
    
    func doJoinTeam(subTeamInfo : SubTeamInfo){
        
        guard let subTeamId = subTeamInfo._id else{
            return
        }
        
        showLoader()
        if Reachability.isConnectedToNetwork() {
            //Params
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
            data["teamId"] = self.arrClubList?[self.currentIndex].strTeamId
            data["subteamId"] = subTeamId
            data["guid"] = UserDefaultConstants().guid ?? ""
            data["dateTime"] = Date().getDateStrFrom(format: DateFormatType.joinTeam)
            //Params, API TYPE, API URL
            
            if arrAPI.count > 0 {
                for i in 0..<arrAPI.count - 1 {
                    if let index = arrAPI[i] as? [String : Any]{
                        data[index["Key"] as! String] = index["Value"]
                    }
                }
            }

            let (url, method, param) = APIHandler().joinTeam(params: data)
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    self.hideLoader()
                        switch response.result {
                        case .success(let value):
                            if let dictResponse = value as? [String:Any] {
                                //Update UI after API Success
                                let commonResponse = Mapper<CommonResponse>().map(JSON: dictResponse)
                                if commonResponse?.status == 200{
                                    self.showSuccessJoin(subTeamInfo : subTeamInfo)
                                }
                            }
                        case .failure(let error):
                            print(error)
                            AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                        }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
        
    }
    
    private func showSuccessJoin(subTeamInfo : SubTeamInfo){
        
        guard let currentClubInfo = self.arrClubList?[currentIndex], let titleText = currentClubInfo.strDisplayName, let subTitleText = subTeamInfo.name else{
            return
        }
        let title = getAttributedText(fullText: "Welcome to the \(titleText)", boldtext: titleText)
        let subTitle = getAttributedText(fullText: " You have joined \(subTitleText) team. Let's get cracking !!", boldtext: subTitleText)
        AlertManager.shared.showSuccesAlertView(controller: self, title: title, subTitle: subTitle, imgLogo: subTeamInfo.banner_img_url) {
            DispatchQueue.main.async {
                let currentLeagueInfo = CurrentLeagueInfo(leagueId: self.leagueInfo?.leagueID ?? "", teamId: currentClubInfo.strTeamId ?? "", subteamId: subTeamInfo._id ?? "")
                self.leagueInfo?.setSubTeamValue(teamId: subTeamInfo._id, teamName: subTeamInfo.name, teamFlag: subTeamInfo.flag)
                self.leagueInfo?.setCurrentLeagueInfo(currentLeague: currentLeagueInfo)
                
                let hflDashBoard = GlobalStoryBoard().hflDashBoardVC
                hflDashBoard.leagueInfo = self.leagueInfo
                hflDashBoard.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(hflDashBoard, animated: true)
            }
        }
    }
    
    
   
}
