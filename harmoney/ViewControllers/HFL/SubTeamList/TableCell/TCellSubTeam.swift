//
//  TCellSubTeam.swift
//  Harmoney
//
//  Created by Saravanan on 11/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class TCellSubTeam: UITableViewCell {
    static let identifier = "TCellSubTeam"
    
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var lblSlot: UILabel!
    @IBOutlet weak var lblSubTeamName: UILabel!
    @IBOutlet weak var imgVwSubTeam: UIImageView!
    static func nib()->UINib{
       return UINib(nibName: TCellSubTeam.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
