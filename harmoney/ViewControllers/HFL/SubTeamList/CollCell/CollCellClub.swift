//
//  CollCellClub.swift
//  Harmoney
//
//  Created by Saravanan on 11/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class CollCellClub: UICollectionViewCell {

    static let identifier = "CollCellClub"
    
    static func nib()->UINib{
       return UINib(nibName: CollCellClub.identifier, bundle: nil)
    }
    
    @IBOutlet weak var lblClubName: UILabel!
    @IBOutlet weak var imgClubLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
