//
//  SubTeamInfo.swift
//  Harmoney
//
//  Created by Saravanan on 12/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct SubTeamInfo : Mappable {
    var _id : String?
    var teamId : String?
    var name : String?
    var short_name : String?
    var flag : String?
    var sub_title : String?
    var description : String?
    var banner_img_url : String?
    var sponsor_id : String?
    var max_member_count : Int?
    var status : String?
    var created : String?
    var updatedDate : String?
    var registrationCount : Int?
    var availableCount : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        teamId <- map["teamId"]
        name <- map["name"]
        short_name <- map["short_name"]
        flag <- map["flag"]
        sub_title <- map["sub_title"]
        description <- map["description"]
        banner_img_url <- map["banner_img_url"]
        sponsor_id <- map["sponsor_id"]
        max_member_count <- map["max_member_count"]
        status <- map["status"]
        created <- map["created"]
        updatedDate <- map["updatedDate"]
        registrationCount <- map["registrationCount"]
        availableCount <- map["availableCount"]
    }

}
