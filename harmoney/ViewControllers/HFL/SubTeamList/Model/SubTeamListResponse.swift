//
//  SubTeamListResponse.swift
//  Harmoney
//
//  Created by Saravanan on 12/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct SubTeamListResponse : Mappable {
    var status : Int?
    var arrSubTeam : [SubTeamInfo]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        status <- map["status"]
        arrSubTeam <- map["result"]
    }
}
