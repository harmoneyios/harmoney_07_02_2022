//
//  CommonResponse.swift
//  Harmoney
//
//  Created by Saravanan on 12/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct CommonResponse: Mappable {
       
        var status : Int?
        var message : String?
        
        init?(map: Map) {
        }
        
        mutating func mapping(map: Map) {
            status    <- map["status"]
            message    <- map["message"]
        }
    }
