//
//  CharityImageVC.swift
//  Harmoney
//
//  Created by Saravanan on 16/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class CharityImageVC: UIViewController {
    var imageName : String?
    @IBOutlet weak var imgVw: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initalSetup(){
        if let imgName = imageName{
            imgVw.set_image(imgName)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

