//
//  HFLChartResponse.swift
//  Harmoney
//
//  Created by Saravanan on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct HFLChartResponse : Mappable {
    var status : Int?
    var result : [HFLChartResult]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        result <- map["result"]
    }

}

struct HFLChartResult : Mappable {
    var date : String?
    var intTotalSteps : Int?
    var intTotalKms : Int?
    var intTotalGoals : Int?
    var intTotalDonation : Int?

    init?(map: Map) {

    }
    var formatedDate: String? {
        if let dat = date{
            let fulDate = DateFormatter()
            fulDate.dateFormat = "yyyy-MM-dd"
            if let date = fulDate.date(from: dat){
                fulDate.dateFormat = "MMdd"
                return fulDate.string(from: date)
            }
        }
        return nil
    }
    mutating func mapping(map: Map) {

        date <- map["date"]
        intTotalSteps <- map["intTotalSteps"]
        intTotalKms <- map["intTotalKms"]
        intTotalGoals <- map["intTotalGoals"]
        intTotalDonation <- map["intTotalDonation"]
    }

}
