//
//  HFLStatisticsResponse.swift
//  Harmoney
//
//  Created by Saravanan on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import Foundation
import ObjectMapper

struct HFLStatisticsResponse : Mappable {
    var status : Int?
    var result : HFLStatisticsResult?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        result <- map["result"]
    }

}


struct HFLStatisticsResult : Mappable {
    var teamInfo : TeamInfo?
    var todayInfo : TodayInfo?
    var overallInfo : OverallInfo?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        teamInfo <- map["teamInfo"]
        todayInfo <- map["todayInfo"]
        overallInfo <- map["overallInfo"]
    }

}


struct TeamInfo : Mappable {
    var strSubTeamFlagLink : String?
    var strSubTeamName : String?
    var intTotalUsers : String?
    var intTotalSteps : String?
    var intTotalKms : String?
    var intTotalGoals : String?
    var intTotalDonation : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        strSubTeamFlagLink <- map["strSubTeamFlagLink"]
        strSubTeamName <- map["strSubTeamName"]
        intTotalUsers <- map["intTotalUsers"]
        intTotalSteps <- map["intTotalSteps"]
        intTotalKms <- map["intTotalKms"]
        intTotalGoals <- map["intTotalGoals"]
        intTotalDonation <- map["intTotalDonation"]
    }

}

struct TodayInfo : Mappable {
    var intTotalSteps : String?
    var intTotalKms : String?
    var intTotalGoals : String?
    var intTotalDonation : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        intTotalSteps <- map["intTotalSteps"]
        intTotalKms <- map["intTotalKms"]
        intTotalGoals <- map["intTotalGoals"]
        intTotalDonation <- map["intTotalDonation"]
    }

}



struct OverallInfo : Mappable {
    var intTotalSteps : String?
    var intTotalKms : String?
    var intTotalGoals : String?
    var intTotalDonation : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        intTotalSteps <- map["intTotalSteps"]
        intTotalKms <- map["intTotalKms"]
        intTotalGoals <- map["intTotalGoals"]
        intTotalDonation <- map["intTotalDonation"]
    }

}
