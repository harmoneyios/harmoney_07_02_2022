//
//  LeagueStatisticsVc.swift
//  Harmoney
//
//  Created by Thirukumaran on 17/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD


class LeagueStatisticsVc: UIViewController{
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var btnMycharity: UIButton!
    @IBOutlet weak var tblVwstatitics: UITableView!
    
    var chartType = 1
    var leagueInfo : BannerLeagueInfo?
    var hflDashBoardResponse : HFLDashBoardResponse?
    var hflStatisticsResponse : HFLStatisticsResponse?
    var hflChartResponse : HFLChartResponse?
    
    var isReloadUI : Bool?{
        didSet{
            if isReloadUI ?? false{
                self.updateUI()
            }
        }
    }
    
    var isLoading : Bool?{
        didSet{
            self.updateLoadingstatus()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
       
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    //MARK: Action methods
    
    @IBAction func btnShare(_ sender: Any) {
        AlertManager.shared.shareScreenToSocialMedia(controller: self)
    }
    
    func getStepsCount()->Int?{
        var countValue = self.hflStatisticsResponse?.result?.todayInfo?.intTotalSteps ?? "0"
        countValue = countValue.replacingOccurrences(of: ",", with: "")
        return Int(countValue)
    }
    @IBAction func btnMyCharity(_ sender: Any) {
        var imageName = ""
     /*   if let joinedLeague = self.leagueInfo{
            imageName = getJoinedImageName(teamName: joinedLeague.strSubTeamName ?? "")
            if joinedLeague.leagueStartDate.getDateFromString() > Date(){
                // League Not start let
            }else if joinedLeague.leagueStartDate == Date().getFilterDateFormat(){
                
                if UserDefaultConstants.shared.isUserReached2000Steps == true{
                    imageName = CharityImage.afterJoin2K
                }
            }
        }else{
            imageName = CharityImage.beforeJoin
        } */
        let charityImageVC = GlobalStoryBoard().charityImageVC
        charityImageVC.imageName = imageName
        charityImageVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(charityImageVC, animated: true)
    }
    
    
    @IBAction func onClickbtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
}
extension LeagueStatisticsVc {
    func initalSetup(){
        lblUserName.text = "User Name"
        tblVwstatitics.isHidden = true
        //   vwTopUserInfo.isHidden = true
        registerCells()
        getStatictisDeatils()
        //changeButtonState(type: 1)
    }
    /* func setSwiperGestrues(){
     let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
     swipeRight.direction = .right
     self.vwBarChat.addGestureRecognizer(swipeRight)
     
     let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
     swipeLeft.direction = .left
     self.vwBarChat.addGestureRecognizer(swipeLeft)
     }*/
    
    
    /* @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
     
     if let swipeGesture = gesture as? UISwipeGestureRecognizer {
     switch swipeGesture.direction {
     case .right:
     print("Swiped right")
     if chartType ==  2{
     changeButtonState(type: 1)
     }
     case .down:
     print("Swiped down")
     case .left:
     print("Swiped left")
     
     if chartType ==  1{
     changeButtonState(type: 2)
     }
     
     case .up:
     print("Swiped up")
     default:
     break
     }
     }
     }*/
    
    /* func findDates()->(String,String)  {
     let fromvalue = (currentLoop*7) - 1
     let toValue = currentLoop == 1 ? 0:(( (currentLoop-1)*7 ))
     
     let fromDate = Calendar.current.date(byAdding: .day, value: -fromvalue, to: Date()) ?? Date()
     let toDate = Calendar.current.date(byAdding: .day, value: -toValue, to: Date()) ?? Date()
     
     getDatesBetween(fromDate: fromDate, toDate: toDate)
     
     return (fromDate.getFilterDateFormat(),toDate.getFilterDateFormat())
     }*/
    
    
    /*  func getDatesBetween(fromDate : Date, toDate : Date){
     var arrDateStr = [String]()
     var date = fromDate // first date
     let endDate = toDate // last date
     
     // Formatter for printing the date, adjust it according to your needs:
     let fmt = DateFormatter()
     fmt.dateFormat = "MMdd"
     
     while date <= endDate {
     arrDateStr.append(fmt.string(from: date))
     print(fmt.string(from: date))
     date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
     }
     arrDate.removeAll()
     arrDate = arrDateStr.reversed()
     
     if let leagueStartDate = leagueInfo?.leagueStartDate{
     if arrDate.contains(fmt.string(from: leagueStartDate.getDateFromString())) {
     btnRightArrow.isEnabled = false
     }
     }
     
     }*/
    func registerCells(){
        
        tblVwstatitics.register(TCellTeamStatistics.nib(), forCellReuseIdentifier: TCellTeamStatistics.identifier)
        tblVwstatitics.register(TCellLeagueMystatitics.nib(), forCellReuseIdentifier: TCellLeagueMystatitics.identifier)
        tblVwstatitics.delegate = self
        tblVwstatitics.dataSource = self
    }
    
}
extension LeagueStatisticsVc: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 200
        }
        return 175
    }
}

extension LeagueStatisticsVc: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hflStatisticsResponse == nil ? 0 : 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            guard  let cell = tableView.dequeueReusableCell(withIdentifier:TCellLeagueMystatitics.identifier) as? TCellLeagueMystatitics else {
                return UITableViewCell()
            }
            
            cell.selectionStyle = .none
          
            cell.lblTeamName.text = self.hflDashBoardResponse?.response?.userProfile?.strMyTeamName
            cell.imgVwUserProfile.set_image(self.hflDashBoardResponse?.response?.userProfile?.strMyTeamLogo ?? "")
            cell.lblSteps.text = self.hflDashBoardResponse?.response?.userProfile?.intTotalSteps?.formatedValue()
                    
            cell.lblDonations.text = "$\(self.hflDashBoardResponse?.response?.userProfile?.intTotalDonation?.formatedValue() ?? "0")"
            
            return cell
        }
        
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: TCellTeamStatistics.identifier) as? TCellTeamStatistics else {
            return UITableViewCell()
        }
        cell.lblSteps.text = self.hflStatisticsResponse?.result?.teamInfo?.intTotalSteps?.formatedValue()
        cell.lblTotalPlayers.text = self.hflStatisticsResponse?.result?.teamInfo?.intTotalUsers
        cell.lblTotalDonations.text = "$\(self.hflStatisticsResponse?.result?.teamInfo?.intTotalDonation?.formatedValue() ?? "0")"
        
        cell.selectionStyle = .none
        return cell
    }
    
    
}


extension LeagueStatisticsVc {
    
    func updateUI(){
        
        DispatchQueue.main.async {
            self.tblVwstatitics.isHidden = false
            //       self.vwTopUserInfo.isHidden = false
          //self.lblTotalDonations.text = self.hflDashBoardResponse?.response?.userProfile?.intTotalDonation
            self.imgUserProfile.imageFromURL(urlString: self.hflDashBoardResponse?.response?.userProfile?.strProfilePicLink ?? "")
            self.lblUserName.text = self.hflDashBoardResponse?.response?.userProfile?.strDisplayName?.capitalized
            // self.loadBarChart()
            self.tblVwstatitics.reloadData()
        }
        
        
    }
    /*   func changeButtonState(type : Int){
     chartType=type
     btnsteps.isSelected = false
     btngoals.isSelected = false
     
     goalsvw.isHidden = true
     stepsvw.isHidden = true
     
     if type == 1{
     btngoals.isSelected = true
     goalsvw.isHidden = false
     }else{
     btnsteps.isSelected = true
     stepsvw.isHidden = false
     }
     createChart()
     }*/
    
    func updateLoadingstatus(){
        if isLoading == true{
            self.showLoader()
        }else{
            hideLoader()
        }
    }
    
    
    func getStatictisDeatils(){
        
        DispatchQueue.global().async {
            self.isLoading = true
            let dispatchGroup = DispatchGroup()
            // Get Teamwise info
            dispatchGroup.enter()
            self.getUserStatictisDetails {
                dispatchGroup.leave()
            } failed: {
                dispatchGroup.suspend()
            }
            dispatchGroup.wait()
            
            //Get Dashboard details
            dispatchGroup.enter()
            self.getDashBoardDetails {
                dispatchGroup.leave()
            } failed: {
                dispatchGroup.suspend()
            }
            dispatchGroup.wait()
            
            
            
            dispatchGroup.notify(queue: .main) {
                self.isLoading = false
                self.isReloadUI = true
            }
        }
    }
  
    
    
    func getDashBoardDetails(success: @escaping () -> Void,failed: @escaping () -> Void){
        
        if Reachability.isConnectedToNetwork() {
            //Params
            let (fromDate, toDate) =  (leagueInfo?.leagueStartDate ?? "",Date().getFilterDateFormat())
            
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.currentLeague?.leagueId
            data["teamId"] = leagueInfo?.currentLeague?.teamId
            data["subteamId"] = leagueInfo?.currentLeague?.subteamId
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getHFLDashBoardDetails(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            self.hflDashBoardResponse = Mapper<HFLDashBoardResponse>().map(JSON: value)
                            success()
                        }else{
                            failed()
                        }
                    case .failure(let error):
                        print(error)
                        failed()
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            failed()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
    /*func getChartDetails(success: @escaping () -> Void,failed: @escaping () -> Void){
     
     if Reachability.isConnectedToNetwork() {
     //Params
     let (fromDate, toDate) = findDates()
     
     var data = [String : Any]()
     data["leagueId"] = leagueInfo?.currentLeague?.leagueId
     data["teamId"] = leagueInfo?.currentLeague?.teamId
     data["subteamId"] = leagueInfo?.currentLeague?.subteamId
     data["fromDate"] = fromDate
     data["toDate"] = toDate
     data["guid"] = UserDefaultConstants().guid ?? ""
     
     //Params, API TYPE, API URL
     let (url, method, param) = APIHandler().getHFLChartDeatails(params: data)
     print((url, method, param))
     AF.request(url, method: method, parameters: param).validate().responseJSON { response in
     
     DispatchQueue.main.async {
     
     switch response.result {
     case .success(let value):
     if let value = value as? [String:Any] {
     //Update UI after API Success
     self.hflChartResponse = Mapper<HFLChartResponse>().map(JSON: value)
     success()
     }else{
     failed()
     }
     case .failure(let error):
     print(error)
     failed()
     AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
     }
     }
     }
     }else{
     failed()
     self.view.toast("Internet Connection not Available!")
     }
     }*/
    
    func getUserStatictisDetails(success: @escaping () -> Void,failed: @escaping () -> Void){
        
        if Reachability.isConnectedToNetwork() {
            //Params
            let (fromDate, toDate) =  (leagueInfo?.leagueStartDate ?? "",Date().getFilterDateFormat())
            
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.currentLeague?.leagueId
            data["teamId"] = leagueInfo?.currentLeague?.teamId
            data["subteamId"] = leagueInfo?.currentLeague?.subteamId
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getStatisticsDetails(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            self.hflStatisticsResponse = Mapper<HFLStatisticsResponse>().map(JSON: value)
                            success()
                        }else{
                            failed()
                        }
                    case .failure(let error):
                        print(error)
                        failed()
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            failed()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
}

