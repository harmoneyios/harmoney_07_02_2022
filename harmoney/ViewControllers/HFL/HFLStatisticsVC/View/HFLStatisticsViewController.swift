//
//  HFLStatisticsViewController.swift
//  Harmoney
//
//  Created by Saravanan on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD
import Charts

var arrDate = [String]()

class HFLStatisticsViewController: UIViewController {
    
    var currentLoop = 1
   
    @IBOutlet weak var vwBarChat: BarChartView!
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var lblTotalDonations: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var vwTopUserInfo: UIView!
    @IBOutlet weak var btnsteps: UIButton!
    @IBOutlet weak var btngoals: UIButton!
    @IBOutlet weak var stepsvw: UIView!
    @IBOutlet weak var goalsvw: UIView!
    @IBOutlet weak var btnLeftArrow: UIButton!
    @IBOutlet weak var lblNoDataAvailable: UILabel!
    @IBOutlet weak var btnRightArrow: UIButton!
    
    
    @IBOutlet weak var vwDonation: UIStackView!
    //100 170
    var chartType = 1
    var leagueInfo : BannerLeagueInfo?
    var hflDashBoardResponse : HFLDashBoardResponse?
    var hflStatisticsResponse : HFLStatisticsResponse?
    var hflChartResponse : HFLChartResponse?
    var isReloadUI : Bool?{
        didSet{
            if isReloadUI ?? false{
                self.updateUI()
            }
        }
    }
  
    var isLoading : Bool?{
        didSet{
            self.updateLoadingstatus()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        createChart()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func onLeftArrow(_ sender: Any) {
        btnRightArrow.isEnabled = true
        currentLoop=currentLoop-1
        if(currentLoop == 1){
            btnLeftArrow.isEnabled=false
        }
        onGetChartInfo()
    }
    @IBAction func onRightArrow(_ sender: Any) {
        currentLoop=currentLoop+1
        btnLeftArrow.isEnabled=true
        onGetChartInfo()
    }
    @IBAction func onChooseType(_ sender: UIButton) {
        // self.delegate?.selectType(selectType: (sender as AnyObject).tag)
        
        changeButtonState(type: sender.tag)
    }
    @IBAction func onHowTOEarnGoal(_ sender: Any) {
        AlertManager.shared.showHFLEarnPopup(controller: nil, img: leagueInfo?.leagueLogo ?? "")
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onDonation(_ sender: Any) {
        
    }
    @IBAction func onShare(_ sender: Any) {
        AlertManager.shared.shareScreenToSocialMedia(controller: self)
    }
    @IBAction func onRedeemCoupons(_ sender: Any) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        if let topController = keyWindow?.rootViewController {
            let gemsVC = GlobalStoryBoard().CouponList
            gemsVC.isfromSecondChallange = false
            topController.present(gemsVC, animated: true, completion: nil)
        }
    }
}

extension HFLStatisticsViewController {
    func initalSetup(){
        vwDonation.isHidden = !(leagueInfo?.isShowDonation ?? true)
        setSwiperGestrues()
        tblVw.isHidden = true
        vwTopUserInfo.isHidden = true
        registerCells()
        getStatictisDeatils()
        btnLeftArrow.isEnabled=false
        onGetChartInfo()
        changeButtonState(type: 1)
    }
    func setSwiperGestrues(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
            swipeRight.direction = .right
          self.vwBarChat.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeLeft.direction = .left
          self.vwBarChat.addGestureRecognizer(swipeLeft)
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                if chartType ==  2{
                    changeButtonState(type: 1)
                }
            case .down:
                print("Swiped down")
            case .left:
                print("Swiped left")
                
                if chartType ==  1{
                    changeButtonState(type: 2)
                }
               
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func findDates()->(String,String)  {
        let fromvalue = (currentLoop*7) - 1
        let toValue = currentLoop == 1 ? 0:(( (currentLoop-1)*7 ))
        
        let fromDate = Calendar.current.date(byAdding: .day, value: -fromvalue, to: Date()) ?? Date()
        let toDate = Calendar.current.date(byAdding: .day, value: -toValue, to: Date()) ?? Date()
        
        getDatesBetween(fromDate: fromDate, toDate: toDate)
        
        return (fromDate.getFilterDateFormat(),toDate.getFilterDateFormat())
    }
    
    
    func getDatesBetween(fromDate : Date, toDate : Date){
        var arrDateStr = [String]()
        var date = fromDate // first date
        let endDate = toDate // last date

        // Formatter for printing the date, adjust it according to your needs:
        let fmt = DateFormatter()
        fmt.dateFormat = "MMdd"

        while date <= endDate {
            arrDateStr.append(fmt.string(from: date))
            print(fmt.string(from: date))
            date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
        }
        arrDate.removeAll()
        arrDate = arrDateStr.reversed()
        
        if let leagueStartDate = leagueInfo?.leagueStartDate{
            if arrDate.contains(fmt.string(from: leagueStartDate.getDateFromString())) {
                btnRightArrow.isEnabled = false
            }
        }
  
    }
    func registerCells(){
        tblVw.register(TCellStatictsTodayInfo.nib(), forCellReuseIdentifier: TCellStatictsTodayInfo.identifier)
        tblVw.register(TcellStatictsTeamInfo.nib(), forCellReuseIdentifier: TcellStatictsTeamInfo.identifier)
        tblVw.delegate = self
        tblVw.dataSource = self
    }
    
}
extension HFLStatisticsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 170
        }
        return 120
    }
}

extension HFLStatisticsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hflStatisticsResponse == nil ? 0 : 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            guard  let cell = tableView.dequeueReusableCell(withIdentifier:TcellStatictsTeamInfo.identifier) as? TcellStatictsTeamInfo else {
                return UITableViewCell()
            }

            cell.selectionStyle = .none
            cell.lblTeamName.text = self.hflStatisticsResponse?.result?.teamInfo?.strSubTeamName
            cell.lblTotalUsers.text = self.hflStatisticsResponse?.result?.teamInfo?.intTotalUsers ?? "0"
            cell.lblClubName.text = self.hflDashBoardResponse?.response?.userProfile?.strClubName
            cell.imgClubLogo.set_image(self.hflDashBoardResponse?.response?.userProfile?.strClubLogo ?? "")
            return cell
        }
        
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: TCellStatictsTodayInfo.identifier) as? TCellStatictsTodayInfo else {
            return UITableViewCell()
        }
        var steps = ""
        var goals = ""
        var donations = ""
        var titleText = ""
        
        if indexPath.row == 1{
            titleText = "Today"
            steps = self.hflStatisticsResponse?.result?.todayInfo?.intTotalSteps?.formatedValue() ?? "0"
            goals = self.hflStatisticsResponse?.result?.todayInfo?.intTotalGoals ?? "0"
            donations = self.hflStatisticsResponse?.result?.todayInfo?.intTotalDonation?.formatedValue() ?? "0"
        }else{
            steps = self.hflStatisticsResponse?.result?.overallInfo?.intTotalSteps?.formatedValue() ?? "0"
            goals = self.hflStatisticsResponse?.result?.overallInfo?.intTotalGoals ?? "0"
            donations = self.hflStatisticsResponse?.result?.overallInfo?.intTotalDonation?.formatedValue() ?? "0"
            titleText = "Overall Statistics"
        }
        cell.vwDonation.isHidden = !(leagueInfo?.isShowDonation ?? true)
        cell.lblTitle.text = titleText
        cell.lblTotalSteps.text = steps
        cell.lblTotalGoals.text = goals
        cell.lblTotalDonations.text = "$\(donations)"
        
        cell.selectionStyle = .none
        return cell
    }
    
}


extension HFLStatisticsViewController {
    
    func updateUI(){
        
        DispatchQueue.main.async {
            self.tblVw.isHidden = false
            self.vwTopUserInfo.isHidden = false
            self.lblTotalDonations.text = "$\(self.hflDashBoardResponse?.response?.userProfile?.intTotalDonation?.formatedValue() ?? "0")"
            self.imgUserProfile.imageFromURL(urlString: self.hflDashBoardResponse?.response?.userProfile?.strProfilePicLink ?? "")
            self.lblUserName.text = self.hflDashBoardResponse?.response?.userProfile?.strDisplayName?.capitalized
            // self.loadBarChart()
            self.tblVw.reloadData()
        }
        

    }
    func changeButtonState(type : Int){
        chartType=type
        btnsteps.isSelected = false
        btngoals.isSelected = false
        
        goalsvw.isHidden = true
        stepsvw.isHidden = true
        
        if type == 1{
            btngoals.isSelected = true
            goalsvw.isHidden = false
        }else{
            btnsteps.isSelected = true
            stepsvw.isHidden = false
        }
        createChart()
    }
    
    func updateLoadingstatus(){
        if isLoading == true{
            self.showLoader()
        }else{
            hideLoader()
        }
    }
    
    
    func getStatictisDeatils(){
        
        DispatchQueue.global().async {
            self.isLoading = true
            let dispatchGroup = DispatchGroup()
            // Get Teamwise info
            dispatchGroup.enter()
            self.getUserStatictisDetails {
                dispatchGroup.leave()
            } failed: {
                dispatchGroup.suspend()
            }
            dispatchGroup.wait()
            
            //Get Dashboard details
            dispatchGroup.enter()
            self.getDashBoardDetails {
                dispatchGroup.leave()
            } failed: {
                dispatchGroup.suspend()
            }
            dispatchGroup.wait()
            
            
            
            dispatchGroup.notify(queue: .main) {
                self.isLoading = false
                self.isReloadUI = true
            }
        }
    }
    func onGetChartInfo()  {
        //Get Dashboard details
        self.showLoader()
        self.getChartDetails {
            self.hideLoader()
            self.createChart()
        } failed: {
            self.hideLoader()
            
        }
        
    }
    
    
    func getDashBoardDetails(success: @escaping () -> Void,failed: @escaping () -> Void){
        
        if Reachability.isConnectedToNetwork() {
            //Params
            let (fromDate, toDate) =  (leagueInfo?.leagueStartDate ?? "",Date().getFilterDateFormat())
            
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.currentLeague?.leagueId
            data["teamId"] = leagueInfo?.currentLeague?.teamId
            data["subteamId"] = leagueInfo?.currentLeague?.subteamId
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getHFLDashBoardDetails(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            self.hflDashBoardResponse = Mapper<HFLDashBoardResponse>().map(JSON: value)
                            success()
                        }else{
                            failed()
                        }
                    case .failure(let error):
                        print(error)
                        failed()
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            failed()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
    func getChartDetails(success: @escaping () -> Void,failed: @escaping () -> Void){
        
        if Reachability.isConnectedToNetwork() {
            //Params
            let (fromDate, toDate) = findDates()
            
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.currentLeague?.leagueId
            data["teamId"] = leagueInfo?.currentLeague?.teamId
            data["subteamId"] = leagueInfo?.currentLeague?.subteamId
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getHFLChartDeatails(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            self.hflChartResponse = Mapper<HFLChartResponse>().map(JSON: value)
                            success()
                        }else{
                            failed()
                        }
                    case .failure(let error):
                        print(error)
                        failed()
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            failed()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
    func getUserStatictisDetails(success: @escaping () -> Void,failed: @escaping () -> Void){
        
        if Reachability.isConnectedToNetwork() {
            //Params
            let (fromDate, toDate) =  (leagueInfo?.leagueStartDate ?? "",Date().getFilterDateFormat())
            
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.currentLeague?.leagueId
            data["teamId"] = leagueInfo?.currentLeague?.teamId
            data["subteamId"] = leagueInfo?.currentLeague?.subteamId
            data["fromDate"] = fromDate
            data["toDate"] = toDate
            data["guid"] = UserDefaultConstants().guid ?? ""
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getStatisticsDetails(params: data)
            print((url, method, param))
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    
                    switch response.result {
                    case .success(let value):
                        if let value = value as? [String:Any] {
                            //Update UI after API Success
                            self.hflStatisticsResponse = Mapper<HFLStatisticsResponse>().map(JSON: value)
                            success()
                        }else{
                            failed()
                        }
                    case .failure(let error):
                        print(error)
                        failed()
                        AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                    }
                }
            }
        }else{
            failed()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
}


extension HFLStatisticsViewController {
    
    private func createChart(){
        vwBarChat.dragEnabled = false
        vwBarChat.dragXEnabled = false
        vwBarChat.dragYEnabled = false
     //   vwBarChat.delegate = self
        let xAxis:XAxis = vwBarChat.xAxis;
        xAxis.drawAxisLineEnabled = false;
        xAxis.drawGridLinesEnabled = false
        //supply data
        var entries = [BarChartDataEntry]()
        var arrValue = [Int]()
    
        for checkDate in arrDate{
            if let hflChatRes = self.hflChartResponse?.result?.first(where: {$0.formatedDate == checkDate}){
                if  chartType == 1{
                    arrValue.append(hflChatRes.intTotalGoals ?? 0)
                }else{
                    arrValue.append(hflChatRes.intTotalSteps ?? 0)
                }
            }else{
                arrValue.append(0)
            }
        }
                
        
//        arrDate.removeAll()
//        arrDate=self.hflChartResponse?.result?.compactMap({ (hflchartResult) -> String? in
//            return hflchartResult.date
//        }) ?? [String]()
        
        
        
        lblNoDataAvailable.isHidden=true
        if arrValue.reduce(0, +) == 0{
            lblNoDataAvailable.isHidden=false
            return
            
        }
      
      for x in  0..<arrValue.count {

                entries.append(BarChartDataEntry(x: Double(x) , y: Double(arrValue[x])))
            
        }
      
        self.vwBarChat.noDataText=""
        let set = BarChartDataSet(entries: entries, label: "steps")
        self.vwBarChat.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.vwBarChat.xAxis.drawGridLinesEnabled = false
        self.vwBarChat.rightAxis.drawGridLinesEnabled = false
        self.vwBarChat.leftAxis.drawLabelsEnabled = false
        self.vwBarChat.rightAxis.drawLabelsEnabled = false
        self.vwBarChat.legend.enabled = false
        self.vwBarChat.xAxis.labelCount = 7;
        self.vwBarChat.xAxis.removeAllLimitLines()
        self.vwBarChat.rightAxis.removeAllLimitLines()
        self.vwBarChat.leftAxis.removeAllLimitLines()
        self.vwBarChat.leftAxis.drawGridLinesEnabled = false
        self.vwBarChat.rightAxis.enabled=false
        self.vwBarChat.leftAxis.enabled=false
        self.vwBarChat.leftAxis.axisMinimum = 0
        self.vwBarChat.xAxis.labelFont = UIFont.futuraPTBookFont(size: 8)
      

        
   let formato:BarChartFormatter = BarChartFormatter()
        let xaxis:XAxis = XAxis()
        formato.stringForValue(Double(), axis: xaxis)
        xaxis.valueFormatter = formato
        vwBarChat.xAxis.valueFormatter = xaxis.valueFormatter
        vwBarChat.xAxis.granularity = 1
        
      set.colors = ChartColorTemplates.joyful()
        //[NSUIColor(cgColor:UIColor.init(red: 41, green: 72, blue: 109).cgColor)]

        let data = BarChartData(dataSet: set)
        data.setValueFont(ConstantString.smallTitle!)
        data.setValueFormatter(MyValueFormatter2())
        
        vwBarChat.data = data
        
    }
    /*   func loadBarChart(){
     vwBarChat.delegate = self
     vwBarChat.chartDescription?.enabled =  false
     vwBarChat.pinchZoomEnabled = false
     vwBarChat.drawBarShadowEnabled = false
     //        let marker = BalloonMarker(color: UIColor(white: 180/255, alpha: 1), font: .systemFont(ofSize: 12), textColor: .white, insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
     //        marker.chartView = vwBarChat
     //        marker.minimumSize = CGSize(width: 80, height: 40)
     //        vwBarChat.marker = marker
     
     let l = vwBarChat.legend
     l.horizontalAlignment = .right
     l.verticalAlignment = .top
     l.orientation = .vertical
     l.drawInside = true
     l.font = .systemFont(ofSize: 8, weight: .light)
     l.yOffset = 10
     l.xOffset = 10
     l.yEntrySpace = 0
     //        chartView.legend = l
     
     let xAxis = vwBarChat.xAxis
     xAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
     xAxis.granularity = 1
     xAxis.centerAxisLabelsEnabled = true
     //        xAxis.valueFormatter = IndexAxisValueFormatter(values: ["0","1","3"])
     
     let leftAxisFormatter = NumberFormatter()
     leftAxisFormatter.maximumFractionDigits = 1
     
     let leftAxis = vwBarChat.leftAxis
     leftAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
     //        leftAxis.valueFormatter = LargeValueFormatter() as! IAxisValueFormatter
     leftAxis.drawTopYLabelEntryEnabled = false
     leftAxis.spaceTop = 0.35
     leftAxis.axisMinimum = 0
     
     vwBarChat.rightAxis.enabled = false
     self.setDataCount()
     
     
     }*/
    
    func setDataCount() {
        
        var arrSteps = [Int]()
        var arrGoals = [Int]()
        
        if let arrStepsValue = hflChartResponse?.result?.compactMap({ (hflRest) -> Int? in
            return hflRest.intTotalSteps
        }){
            arrSteps = arrStepsValue
        }
        if let arrGoalsValue = hflChartResponse?.result?.compactMap({ (hflRest) -> Int? in
            return hflRest.intTotalGoals
        }){
            arrGoals = arrGoalsValue
        }
        
        let groupSpace = 0.08
        let barSpace = 0.03
        let barWidth = 0.2
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"
        
        let groupCount = arrSteps.count + 1
        let startYear = 0
        let endYear = arrSteps.count
        
        
        
        let block1: (Int) -> BarChartDataEntry = { (i) -> BarChartDataEntry in
            return BarChartDataEntry(x: Double(i), y: Double(arrSteps[i]))
        }
        
        let block2: (Int) -> BarChartDataEntry = { (i) -> BarChartDataEntry in
            return BarChartDataEntry(x: Double(i), y: Double(arrGoals[i]))
        }
        
        
        let yVals1 = (startYear ..< endYear).map(block1)
        let yVals2 = (startYear ..< endYear).map(block2)
        
        let set1 = BarChartDataSet(entries: yVals1, label: "Steps")
        set1.setColor(UIColor.blue)
        
        let set2 = BarChartDataSet(entries: yVals2, label: "Goals")
        set2.setColor(UIColor.green)
        
        let data: BarChartData = BarChartData(dataSets: [set1, set2])
        data.setValueFont(.systemFont(ofSize: 10, weight: .light))
        data.setValueFormatter(MyValueFormatter2())
        
        // specify the width each bar should have
        data.barWidth = barWidth
        
        // restrict the x-axis range
        vwBarChat.xAxis.axisMinimum = Double(startYear)
        
        // groupWidthWithGroupSpace(...) is a helper that calculates the width each group needs based on the provided parameters
        vwBarChat.xAxis.axisMaximum = Double(startYear) + data.groupWidth(groupSpace: groupSpace, barSpace: barSpace) * Double(groupCount)
        
        data.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        
        vwBarChat.data = data
    }
}

extension HFLStatisticsViewController : ChartViewDelegate {
    
}

class MyValueFormatter2: IValueFormatter {
    var xValueForToday: Double?  // Set a value
    
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        let percentageValue = value as NSNumber
        let percentageString = String(describing: percentageValue)
        return percentageString
    }
}
@objc(BarChartFormatter)
public class BarChartFormatter: NSObject, IAxisValueFormatter
{
    var months: [String]! = arrDate
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
        return months[Int(value)]
    }
}
