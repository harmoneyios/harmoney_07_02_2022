//
//  TCellStatictsTodayInfo.swift
//  Harmoney
//
//  Created by Saravanan on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class TCellStatictsTodayInfo: UITableViewCell {
    static let identifier = "TCellStatictsTodayInfo"
    static func nib()->UINib{
       return UINib(nibName: TCellStatictsTodayInfo.identifier, bundle: nil)
    }
    
    @IBOutlet weak var lblTotalDonations: UILabel!
    @IBOutlet weak var lblTotalGoals: UILabel!
    @IBOutlet weak var lblTotalSteps: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    @IBOutlet weak var vwDonation: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
