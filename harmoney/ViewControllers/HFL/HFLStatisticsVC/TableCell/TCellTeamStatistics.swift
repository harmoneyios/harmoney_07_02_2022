//
//  TCellTeamStatistics.swift
//  Harmoney
//
//  Created by Thirukumaran on 17/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class TCellTeamStatistics: UITableViewCell {
    static let identifier = "TCellTeamStatistics"
    static func nib()->UINib{
       return UINib(nibName: TCellTeamStatistics.identifier, bundle: nil)
    }
    

    @IBOutlet weak var lblSteps: UILabel!
    
    @IBOutlet weak var lblAvgSteps: UILabel!
    
    @IBOutlet weak var lblTotalDonations: UILabel!
    @IBOutlet weak var lblTotalPlayers: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
