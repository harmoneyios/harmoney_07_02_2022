//
//  TcellStatictsTeamInfo.swift
//  Harmoney
//
//  Created by Saravanan on 19/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class TcellStatictsTeamInfo: UITableViewCell {

    static let identifier = "TcellStatictsTeamInfo"
    static func nib()->UINib{
       return UINib(nibName: TcellStatictsTeamInfo.identifier, bundle: nil)
    }
    
    
    @IBOutlet weak var lblTotalUsers: UILabel!
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblClubName: UILabel!
    @IBOutlet weak var imgClubLogo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
