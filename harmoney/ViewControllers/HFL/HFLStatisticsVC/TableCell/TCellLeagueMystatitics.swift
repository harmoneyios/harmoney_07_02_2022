//
//  TCellLeagueMystatitics.swift
//  Harmoney
//
//  Created by Thirukumaran on 17/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class TCellLeagueMystatitics: UITableViewCell {
    static let identifier = "TCellLeagueMystatitics"
    static func nib()->UINib{
       return UINib(nibName: TCellLeagueMystatitics.identifier, bundle: nil)
    }
    
    @IBOutlet weak var lblTeamName: UILabel!
    
    @IBOutlet weak var imgVwUserProfile: UIImageView!
    @IBOutlet weak var lblDonations: UILabel!
    @IBOutlet weak var lblSteps: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
