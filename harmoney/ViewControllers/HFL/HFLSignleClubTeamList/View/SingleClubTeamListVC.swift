//
//  SingleClubTeamListVC.swift
//  Harmoney
//
//  Created by Saravanan on 29/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//
import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD
import Lottie
class SingleClubTeamListVC: UIViewController {

    // Outlets
    
    @IBOutlet weak var txtLegueDescription: UITextView!
    @IBOutlet weak var imgOrganizedLogo: UIImageView!
    @IBOutlet weak var lblOrganized: UILabel!
    @IBOutlet weak var imgLeagueLogo: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var vwNavBar: UIView!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var btnGrid: UIButton!
    @IBOutlet weak var collVwTeams: UICollectionView!
    
    @IBOutlet weak var lblLeagueInfo: UILabel!
    
    // Properties
    var leagueInfo : BannerLeagueInfo?
    var subTeamListResponse : SubTeamListResponse?
    var arrAPI = [Any]()

    var viewType : TeamViewType?{
        didSet{
            chanageButtonsState()
        }
    }
    
    enum TeamViewType: Int{
        case gridView = 1
        case listView = 2
    }
    
    // ViewLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        getSubTeamList(strTeamId: self.leagueInfo?.strTeamID ?? "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavBar()
    }
    private func chanageButtonsState(){
        btnGrid.tintColor = .lightGray
        btnList.tintColor = .lightGray
        if  viewType == .gridView{
            btnGrid.tintColor = .darkGray
        }else{
            btnList.tintColor = .darkGray
        }
        self.collVwTeams.performBatchUpdates({
                            let indexSet = IndexSet(integersIn: 0...0)
                            self.collVwTeams.reloadSections(indexSet)
                        }, completion: nil)

    }
    
    // Show view type based on user selection
    @IBAction func onGridListClick(_ sender: UIButton) {
        //Tag 1 Grid
        //Tag 2 List
        viewType = TeamViewType(rawValue: sender.tag)
    }
    
    
    // Back button action
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onRewards(_ sender: Any) {
        let privacypolicy = GlobalStoryBoard().rewardWebVC
        privacypolicy.websiteUrl = "https://harmoney.ai/app-pages/bonobos/charity1.html"
        self.navigationController?.pushViewController(privacypolicy, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SingleClubTeamListVC{
    
    @objc func imageTapped(sender: UITapGestureRecognizer) {
                if sender.state == .ended {
                    showNewBonobosPage()
                }
        }
    func showNewBonobosPage(){
                let privacypolicy = GlobalStoryBoard().rewardWebVC
                privacypolicy.websiteUrl = "https://www.bonobos.org/"
                privacypolicy.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(privacypolicy, animated: true)
    }
    private func updateUI(){
        txtLegueDescription.text = self.leagueInfo?.startInformation
        lblLeagueInfo.text = self.leagueInfo?.startInformation
        lblOrganized.text = self.leagueInfo?.organizedText
        //imgLeagueLogo.set_image(self.leagueInfo?.leagueLogo ?? "")
       //imgOrganizedLogo.set_image(self.leagueInfo?.leagueSponsorLogo ?? "")
        imgLeagueLogo.imageFromURL(urlString: self.leagueInfo?.leagueLogo ?? "")
        imgOrganizedLogo.imageFromURL(urlString: self.leagueInfo?.leagueSponsorLogo ?? "")
        self.collVwTeams.performBatchUpdates({
                            let indexSet = IndexSet(integersIn: 0...0)
                            self.collVwTeams.reloadSections(indexSet)
                        }, completion: nil)
       
    }
    
    func getSubTeamList(strTeamId : String){
        showLoader()
        if Reachability.isConnectedToNetwork() {
            //Params
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
            data["teamId"] = strTeamId
            
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().getSubteamList(params: data)
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                
                DispatchQueue.main.async {
                    self.hideLoader()
                        switch response.result {
                        case .success(let value):
                            if let value = value as? [String:Any] {
                                //Update UI after API Success
                                self.subTeamListResponse = Mapper<SubTeamListResponse>().map(JSON: value)
                                self.updateUI()
                            }
                        case .failure(let error):
                            print(error)
                            AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                        }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
    }
    
    // Get club list from API
    
}

// MARK: - Private Methods

extension SingleClubTeamListVC{
    
    
    private func initalSetup(){
        viewType = .gridView
        registerCells()
        setTapGesture()
    }
    private func setTapGesture(){
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(sender:)))
        imgOrganizedLogo.addGestureRecognizer(tapGR)
        imgOrganizedLogo.isUserInteractionEnabled = true
    }
    private func registerCells(){
        
        collVwTeams.register(CollCellSingleTeamListView.nib(), forCellWithReuseIdentifier: CollCellSingleTeamListView.identifier)
        collVwTeams.register(CollCellSingleTeamGridView.nib(), forCellWithReuseIdentifier: CollCellSingleTeamGridView.identifier)
        collVwTeams.delegate = self
        collVwTeams.dataSource = self
        
    }
    private func setUpNavBar(){
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = CGRect(x: vwNavBar.bounds.origin.x, y: vwNavBar.bounds.origin.y, width: UIScreen.main.bounds.width, height: vwNavBar.bounds.height)
        vwNavBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
    }
    
    private func showJoinPermissionAlert(index : Int){
        
        guard let subTeamInfo = self.subTeamListResponse?.arrSubTeam?[index], let teamName = subTeamInfo.name else{
            return
        }
            
        let alert = UIAlertController(title: "", message: "Do you want to join \(teamName) team?", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "Join",
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                            self.doJoinTeam(subTeamInfo: subTeamInfo)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    private func showSuccessJoin(subTeamInfo : SubTeamInfo){
        
        guard let titleText = self.leagueInfo?.strTeamName, let subTitleText = subTeamInfo.name else{
            return
        }
        var title = NSMutableAttributedString(string: "")
        if titleText.count > 0{
            title = getAttributedText(fullText: "Welcome to the \(titleText)", boldtext: titleText)
        }
        //You have joined the 'ape team name' and donated $5 to Friends of Bonobos
        let subTitle = getAttributedText(fullText: "You have joined the \(subTitleText) team and donated $5 to Friends of Bonobos", boldtext: subTitleText)
        AlertManager.shared.showSuccesAlertView(controller: self, title: title, subTitle: subTitle, imgLogo: subTeamInfo.banner_img_url) {
            DispatchQueue.main.async {
                
                var isPushtoDashboard = false
                if self.leagueInfo?.isSingleTeam == false{
                    isPushtoDashboard = true
                }else{
                    if let eventDate = self.leagueInfo?.leagueStartDate.getDateFromString(){
                        if eventDate > Date(){
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            isPushtoDashboard = true
                        }
                    }
                }
                if isPushtoDashboard{
                    let currentLeagueInfo = CurrentLeagueInfo(leagueId: self.leagueInfo?.leagueID ?? "", teamId: self.leagueInfo?.strTeamID ?? "", subteamId: subTeamInfo._id ?? "")
                    self.leagueInfo?.setSubTeamValue(teamId: subTeamInfo._id, teamName: subTeamInfo.name, teamFlag: subTeamInfo.flag)
                    self.leagueInfo?.setCurrentLeagueInfo(currentLeague: currentLeagueInfo)
                    let hflDashBoard = GlobalStoryBoard().hflDashBoardVC
                    hflDashBoard.leagueInfo = self.leagueInfo
                    hflDashBoard.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(hflDashBoard, animated: true)
                }
                
            }
        }
    }
    
}

// MARK: - Collectionview delegate Methods
extension SingleClubTeamListVC: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //showAniamtionView()
        if let result = self.subTeamListResponse?.arrSubTeam?[indexPath.row]{
            if let availableCount = result.availableCount{
                if availableCount == 0 {
                    self.view.toast("Sorry, slots are filled.Please join to another Team")
                }else{
                    showJoinPermissionAlert(index: indexPath.row)
                }
                
            }
        }
    }
}



// MARK: - Collectionview datasource Methods
extension SingleClubTeamListVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.subTeamListResponse?.arrSubTeam?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if viewType == .gridView{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollCellSingleTeamGridView.identifier, for: indexPath) as? CollCellSingleTeamGridView else {
                return UICollectionViewCell()
            }
            
            if let result = self.subTeamListResponse?.arrSubTeam?[indexPath.row]{
                cell.lblClubName.text = result.name
                cell.imgClubLogo.contentMode = .scaleAspectFill
                //cell.imgClubLogo.set_image(result.banner_img_url ?? "")
                cell.imgClubLogo.imageFromURL(urlString: result.banner_img_url ?? "")
            }
            //strDisplayName
            return cell
        }else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollCellSingleTeamListView.identifier, for: indexPath) as? CollCellSingleTeamListView else {
                return UICollectionViewCell()
            }
            if let result = self.subTeamListResponse?.arrSubTeam?[indexPath.row]{
                cell.lblClubName.text = result.name
                //cell.imgVwClub.set_image(result.banner_img_url ?? "")
                cell.imgVwClub.imageFromURL(urlString: result.banner_img_url ?? "")
                cell.btnJoin.tag = indexPath.row
                cell.btnJoin.addTarget(self, action: #selector(onJoinClick(sender:)), for: .touchUpInside)
                
                if let maxCount = result.max_member_count, let availableCount = result.availableCount{
                    if availableCount == 0 {
                        cell.lblSlot.text = "(all slots filled)"
                        cell.lblClubName.textColor = UIColor.init(hex: 0x7D7D7D)
                        cell.lblSlot.textColor = UIColor.init(hex: 0xA8A8A8)
                       // cell.vwBg.backgroundColor = UIColor.init(hex: 0xF3F3F3)
                        
                        cell.btnJoin.isHidden = true
                    }else{
                       
                            cell.lblSlot.text = "(\(availableCount) of \(maxCount) slots are left)"
                            cell.lblClubName.textColor = UIColor.init(hex: 0x1E3F66)
                            cell.lblSlot.textColor = UIColor.init(hex: 0x565656)
                            
                            cell.btnJoin.isHidden = false
                            cell.btnJoin.tag = indexPath.row
                            cell.btnJoin.backgroundColor = UIColor.harmoneyDarkBlueColor
                        
                    }
                }
                
            }
            
            return cell
        }
    }
    
    @objc func onJoinClick(sender : UIButton){
        if let subTeamInfo = self.subTeamListResponse?.arrSubTeam?[sender.tag]{
                doJoinTeam(subTeamInfo: subTeamInfo)
            }
    }
    
    func doJoinTeam(subTeamInfo : SubTeamInfo){
        
        guard let subTeamId = subTeamInfo._id,let availableCount = subTeamInfo.availableCount  else{
            return
        }
        if availableCount == 0{
            self.view.toast("Sorry, slots are filled.Please join to another Team")
            return
        }
        showLoader()
        if Reachability.isConnectedToNetwork() {
            //Params
            var data = [String : Any]()
            data["leagueId"] = leagueInfo?.leagueID
            data["teamId"] = self.leagueInfo?.strTeamID
            data["subteamId"] = subTeamId
            data["guid"] = UserDefaultConstants().guid ?? ""
            data["dateTime"] = Date().getDateStrFrom(format: DateFormatType.joinTeam)
            
            if arrAPI.count > 0 {
                for i in 0..<arrAPI.count - 1 {
                    if let index = arrAPI[i] as? [String : Any]{
                        data[index["Key"] as! String] = index["Value"]
                    }
                }
            }
            //Params, API TYPE, API URL
            let (url, method, param) = APIHandler().joinTeam(params: data)
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                print(url, method, param)
                DispatchQueue.main.async {
                    self.hideLoader()
                        switch response.result {
                        case .success(let value):
                            if let dictResponse = value as? [String:Any] {
                                //Update UI after API Success
                                let commonResponse = Mapper<CommonResponse>().map(JSON: dictResponse)
                                if commonResponse?.status == 200{
                                    self.showSuccessJoin(subTeamInfo : subTeamInfo)
                                }
                            }
                        case .failure(let error):
                            print(error)
                            AlertView.shared.showAlert(view: self, title: "Alert", description: "Internal Server Error!")
                        }
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
        
    }
    
}

// MARK: - Collectionview flowlayout Methods
extension SingleClubTeamListVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if viewType == .gridView{
            let cellSize = (UIScreen.main.bounds.width - 60) / 3
            return CGSize(width: cellSize, height: cellSize + 25)
        }else{
            let cellSize = (UIScreen.main.bounds.width - 40)
            return CGSize(width: cellSize, height: 80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if viewType == .gridView{
        return 20
        }else{
            return 5
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
