//
//  CollCellSingleTeamListView.swift
//  Harmoney
//
//  Created by Saravanan on 29/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class CollCellSingleTeamListView: UICollectionViewCell {
    static let identifier = "CollCellSingleTeamListView"
        static func nib()->UINib{
           return UINib(nibName: CollCellSingleTeamListView.identifier, bundle: nil)
        }
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var lblClubName: UILabel!
    @IBOutlet weak var imgVwClub: UIImageView!
    @IBOutlet weak var lblSlot: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblSlot.isHidden = true
    }

}
