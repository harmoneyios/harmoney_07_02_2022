//
//  CollCellSingleTeamGridView.swift
//  Harmoney
//
//  Created by Saravanan on 29/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class CollCellSingleTeamGridView: UICollectionViewCell {
    static let identifier = "CollCellSingleTeamGridView"
    static func nib()->UINib{
       return UINib(nibName: CollCellSingleTeamGridView.identifier, bundle: nil)
    }
    @IBOutlet weak var vwImageBG: UIView!
    
    @IBOutlet weak var lblClubName: UILabel!
    @IBOutlet weak var imgClubLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.initialSetup()
    }
    
    func initialSetup(){
        let padding : CGFloat = 10
        let imgPadding : CGFloat = 40
        let cellSize = ((UIScreen.main.bounds.width - 60) / 3) - padding
        vwImageBG.layer.cornerRadius = cellSize / 2
        imgClubLogo.layer.cornerRadius = (cellSize - imgPadding) / 2
    }

}
