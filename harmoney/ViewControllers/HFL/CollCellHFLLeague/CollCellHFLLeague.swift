//
//  CollCellHFLLeague.swift
//  Harmoney
//
//  Created by Saravanan on 18/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
import Foundation

enum CharityImage{
    static let  orangutans = "Charity/after_join_orangutans"
    static let  humans = "Charity/after_join_humans"
    static let  gorillas = "Charity/after_join_gorillas"
    static let  chimpanzees = "Charity/after_join_chimpanzees"
    static let  bonobos = "Charity/after_join_bonobos"
    static let  beforeJoin = "Charity/before_join"
    static let  afterJoin2K = "Charity/after_fin_2k_steps"
}

protocol CollCellHFLLeagueDelegate: NSObject {
    func show2000StepsSuccessPopup()
}
class CollCellHFLLeague: UICollectionViewCell {
    
    @IBOutlet weak var vwStepProgress: UIView!
    @IBOutlet weak var constVwStepProgressHeight: NSLayoutConstraint! //80
    @IBOutlet weak var vwLeagueInfoBottom: UIView!
    weak var delegate : CollCellHFLLeagueDelegate? = nil
    //30
    @IBOutlet weak var constEventDateHeight: NSLayoutConstraint!
    static let identifier = "CollCellHFLLeague"
    static func nib()->UINib{
        return UINib(nibName: CollCellHFLLeague.identifier, bundle: nil)
    }
    @IBOutlet weak var imgLeagueLogo_Join: UIImageView!
    @IBOutlet weak var vwBefore_Joined: UIStackView!
    
    @IBOutlet weak var vw_After_joined: UIView!
    @IBOutlet weak var lblClubName_Joined: UILabel!
    @IBOutlet weak var imgClubLogo_Joined: UIImageView!
    @IBOutlet weak var imgVwSponcerLogo_Joined: UIImageView!
    @IBOutlet weak var lblOrgaizerText_Joined: UILabel!
    
    @IBOutlet weak var vwStackCurrentStepProgresInfo: UIStackView!
    
    @IBOutlet weak var lblOrganize: UILabel!
    @IBOutlet weak var imgSponcerLogo: UIImageView!
    @IBOutlet weak var imgLeagueLogo: UIImageView!
    
    @IBOutlet weak var lblLeagueTitle: UILabel!
    
    @IBOutlet weak var vwProgressBG: UIView!
    @IBOutlet weak var vwLeagueInfo: UIView!
    @IBOutlet weak var btnJoinNow: UIButton!
    @IBOutlet weak var vwJoinNow: UIView!
    
    @IBOutlet weak var lblTotalDonations: UILabel!
    @IBOutlet weak var lblTotalMembers: UILabel!
    @IBOutlet weak var lblTotalGoals: UILabel!
    @IBOutlet weak var lblTotalSteps: UILabel!
    
    @IBOutlet weak var btnLeaderBoard: UIButton!
    @IBOutlet weak var btnImgVw: UIButton!
    
    @IBOutlet weak var btnDonation: UIButton!
    
    @IBOutlet weak var vwDonation: UIStackView!
    
    @IBOutlet weak var vwNewTop: UIView!
    @IBOutlet weak var vwNewBottom: UIView!
    
    @IBOutlet weak var imgVwTopLeft: UIImageView!
    
    @IBOutlet weak var imgVWTopRight: UIImageView!
    
    @IBOutlet weak var imgVwBottom: UIImageView!
    
    @IBOutlet weak var lblEventDate: UILabel!
    
    @IBOutlet weak var imgVw2000Steps: UIImageView!
    
    @IBOutlet weak var imgEventLeague: UIImageView!
    @IBOutlet weak var vwProgress: UIView!
    
    @IBOutlet weak var lbl2000StepsCompleted: UILabel!
    @IBOutlet weak var constVwProgressWidth: NSLayoutConstraint!
    @IBOutlet weak var lblCurrentStepProgress: UILabel!
    
    @IBOutlet weak var btnCharity: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}


extension CollCellHFLLeague{
    func updateCell(bannerLeagueInfo : BannerLeagueInfo?,curentSteps : String? = nil){
        
        vwStepProgress.clipsToBounds = false
        constVwStepProgressHeight.constant = 80
        
        vwDonation.isHidden = !(bannerLeagueInfo?.isShowDonation ?? true)
        btnDonation.isHidden = !(bannerLeagueInfo?.isShowDonation ?? true)
        
        let leagueInfo = bannerLeagueInfo
        var isShowLoginPage = false
        if leagueInfo?.isJoint == true {
            isShowLoginPage = false
            
        } else {
           // if leagueInfo?.isJoinNow == true{
                isShowLoginPage = true
//            } else {
//                isShowLoginPage = false
//            }
        }
        
        vwJoinNow.isHidden = true
        vwLeagueInfo.isHidden = true
        
        lblLeagueTitle.text = bannerLeagueInfo?.leagueName
      
        imgLeagueLogo.set_image(bannerLeagueInfo?.leagueLogo ?? "")
        imgSponcerLogo.set_image(bannerLeagueInfo?.leagueSponsorLogo ?? "")
        lblOrganize.text = bannerLeagueInfo?.organizedText
        
        imgLeagueLogo_Join.set_image(bannerLeagueInfo?.leagueLogo ?? "")
        imgVwSponcerLogo_Joined.set_image(bannerLeagueInfo?.leagueSponsorLogo ?? "")
        lblOrgaizerText_Joined.text = bannerLeagueInfo?.organizedText
        
        vwBefore_Joined.isHidden = true
        vw_After_joined.isHidden = true
        vwNewBottom.isHidden = true
        lbl2000StepsCompleted.isHidden = true
        vwStackCurrentStepProgresInfo.isHidden = true
        
        constEventDateHeight.constant = 50.0
        
        let currentDate = Date().getDateStrFrom(format: DateFormatType.pickerDate)
        
        
        if let stDat = bannerLeagueInfo?.leagueStartDate, let endDat = bannerLeagueInfo?.leagueEndDate {
            if stDat == endDat{
                lblEventDate.text = "Express your kindness on \(stDat.getDateFromString().getDateStrFrom()) \n to help save Bonobos"
                lblEventDate.text = bannerLeagueInfo?.footer_text
            }else{
                lblEventDate.text = "Express your kindness on \(stDat.getDateFromString().getDateStrFrom()) - \(endDat.getDateFromString().getDateStrFrom()) \n to help save Bonobos"
                lblEventDate.text = bannerLeagueInfo?.footer_text
             
            }
        }
        
        btnDonation.isHidden = true
        lblEventDate.bottomCorners(radius: 10)

        if isShowLoginPage{
           //Not join in any team
            vwBefore_Joined.isHidden = false
            vwJoinNow.isHidden = false
                        
            if let imgBefore = bannerLeagueInfo?.bg_images?.before_image?.banner_url{
                if imgBefore.count > 0{
                    vwNewBottom.isHidden = false
                    if imgBefore.fileExtension().count > 0{
                        imgVwBottom.imageFromURL(urlString: imgBefore)
                    }else{
                        imgVwBottom.setAlamofireImage(imgBefore)
                    }
                }
            }
           
            if bannerLeagueInfo?.leagueEndDate.getDateFromString() ?? Date() < currentDate.getDateFromString() {
//                imgVwBottom.set_image("HFL/after_end_of_league")
                if let withoutJoined = bannerLeagueInfo?.bg_images?.after_event_without_joined?.banner_url{
                    if withoutJoined.count > 0{
                        vwNewBottom.isHidden = false
                        if withoutJoined.fileExtension().count > 0{
                            imgVwBottom.imageFromURL(urlString: withoutJoined)
                        }else{
                            imgVwBottom.setAlamofireImage(withoutJoined)
                        }
                    }
                }
                constEventDateHeight.constant = 0.0
            }
            
        }else{

            //After Join the League
            vw_After_joined.isHidden = false
            vwLeagueInfo.isHidden = false
            updateLeagueInfo(bannerLeagueInfo: bannerLeagueInfo)
            
         
              if bannerLeagueInfo?.leagueStartDate.getDateFromString() ?? Date() > currentDate.getDateFromString(){
                // League Not start let
                vwNewBottom.isHidden = false
//                imgVwBottom.image = UIImage(named: "dummy_LeagueImg.jpeg")
//                if let imgAfter = bannerLeagueInfo?.leagueAfterJoiningBgImage {
//                    if imgAfter.count > 0{
//                        if imgAfter.fileExtension().count > 0{
//                            imgVwBottom.imageFromURL(urlString: imgAfter)
//                        }else{
//                            imgVwBottom.set_image(imgAfter)
//                        }
//                    }
//                }
                
                if let beforeEventStart = bannerLeagueInfo?.bg_images?.after_join_before_event_start?.banner_url{
                    if beforeEventStart.count > 0{
                        vwNewBottom.isHidden = false
                        if beforeEventStart.fileExtension().count > 0{
                            imgVwBottom.imageFromURL(urlString: beforeEventStart)
                        }else{
                            imgVwBottom.setAlamofireImage(beforeEventStart)
                        }
                    }
                }
            }else if ((bannerLeagueInfo?.leagueStartDate.getDateFromString() ?? Date() <= currentDate.getDateFromString()) && (bannerLeagueInfo?.leagueEndDate.getDateFromString() ?? Date() >= currentDate.getDateFromString())){
                // League started
                constEventDateHeight.constant = 10.0
//                imgEventLeague.set_image("HFL/Group-before complete 2000 steps")
                vwNewBottom.isHidden = true
                
                if bannerLeagueInfo?.add2KStepsConfirmation == true{
                    if let duringEventAchieved = bannerLeagueInfo?.bg_images?.during_event_achieved?.banner_url{
                        if duringEventAchieved.count > 0{
                            if duringEventAchieved.fileExtension().count > 0{
                                imgEventLeague.imageFromURL(urlString: duringEventAchieved)
                            }else{
                                imgEventLeague.setAlamofireImage(duringEventAchieved)
                            }
                        }
                    }
                    UserDefaultConstants.shared.isUserReached2000Steps = true
                    self.show2KSucessView()
                }else{
                    if let duringEventUnachieved = bannerLeagueInfo?.bg_images?.during_event_unachieved?.banner_url{
                        if duringEventUnachieved.count > 0{
                            if duringEventUnachieved.fileExtension().count > 0{
                                imgEventLeague.imageFromURL(urlString: duringEventUnachieved)
                            }else{
                                imgEventLeague.setAlamofireImage(duringEventUnachieved)
                            }
                        }
                    }

                    if let stepCount = Int(curentSteps ?? "0"){
                        progressStepsLeague(stepsCount: stepCount)
                    }
                }
               
               // lbl2000StepsCompleted.text = "Thank you Kindness Champ!"
                lblEventDate.text = leagueInfo?.footer_text
                
            }else if bannerLeagueInfo?.leagueEndDate.getDateFromString() ?? Date() < currentDate.getDateFromString(){
                constEventDateHeight.constant = 0.0
                vwNewBottom.isHidden = false
                // League Expired
                if let afterEventJoined = bannerLeagueInfo?.bg_images?.after_event_joined?.banner_url{
                    if afterEventJoined.count > 0{
                        vwNewBottom.isHidden = false
                        if afterEventJoined.fileExtension().count > 0{
                            imgVwBottom.imageFromURL(urlString: afterEventJoined)
                        }else{
                            imgVwBottom.setAlamofireImage(afterEventJoined)
                        }
                    }
                }

//                imgVwBottom.set_image("HFL/after_end_of_league")
            }
            
        }
        imgVwTopLeft.setAlamofireImage(bannerLeagueInfo?.leagueLogo ?? "")
        imgVWTopRight.setAlamofireImage(bannerLeagueInfo?.leagueSponsorLogo ?? "")
    }
    
    
    func updateLeagueInfo(bannerLeagueInfo : BannerLeagueInfo?){
        lblTotalDonations.text = "$\(bannerLeagueInfo?.intTotalDonation?.formatedValue() ?? "0")"
        lblTotalMembers.text = "\(bannerLeagueInfo?.intTotalMember ?? 0)"
        lblTotalGoals.text = bannerLeagueInfo?.intTotalGoals?.formatedValue()
        lblTotalSteps.text = bannerLeagueInfo?.intTotalSteps?.formatedValue()
        
        imgClubLogo_Joined.set_image(bannerLeagueInfo?.strTeamFlag ?? "")
        lblClubName_Joined.text = bannerLeagueInfo?.strTeamName
    }
    
    func show2KSucessView(){
        
        vwStepProgress.clipsToBounds = true
        constVwStepProgressHeight.constant = 0
        
        //imgEventLeague.set_image("HFL/top_after_complete_2k_steps")
        lblCurrentStepProgress.text = "\(Int(maxReachCount).formatedValue())"
        constVwProgressWidth.constant = vwProgressBG.frame.size.width
        if UserDefaultConstants.shared.isUserReached2000Steps != true{
            delegate?.show2000StepsSuccessPopup()
        }
        
    }
    
    func progressStepsLeague(stepsCount : Int){
        if CGFloat(stepsCount) >= maxReachCount{
            show2KSucessView()
        }else{
            vwStackCurrentStepProgresInfo.isHidden = false
            lblCurrentStepProgress.text = "\(stepsCount.formatedValue())"
            let fullWidth = vwProgressBG.frame.size.width
            let singlePixel = fullWidth / maxReachCount
            let currentWidthSize = CGFloat(stepsCount) * singlePixel
            constVwProgressWidth.constant = currentWidthSize
        }
    }
}
extension String {
    
    func fileName() -> String {
        return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
    }
    
    func fileExtension() -> String {
        return URL(fileURLWithPath: self).pathExtension
    }
}
