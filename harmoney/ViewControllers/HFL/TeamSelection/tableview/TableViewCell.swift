//
//  TableViewCell.swift
//  Harmoney
//
//  Created by Thirukumaran on 30/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    static let identifier = "TableViewCell"
    static func nib()->UINib{
       return UINib(nibName: TableViewCell.identifier, bundle: nil)
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwImgBgView: UIView!
    @IBOutlet weak var lblAll: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnTick: UIButton!
    @IBOutlet weak var vwBackground: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
