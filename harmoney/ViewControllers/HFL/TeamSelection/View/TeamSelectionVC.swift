//
//  TeamSelectionVC.swift
//  Harmoney
//
//  Created by Saravanan on 30/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit
protocol TeamSelectionDelegate {
    func selectIndex(index: Int, isClub : Bool)
}

class TeamSelectionVC: UIViewController {
    @IBOutlet weak var vwCenter: UIView!
    @IBOutlet weak var tblVW: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    //1E3F66
    var delegate : TeamSelectionDelegate? = nil
    var selectedIndex = 0
    var isClub = true
    var clubListResponse : TeamListResponse?
    var subTeamListResponse : SubTeamListResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
initalSetup()
        // Do any additional setup after loading the view.
    }

    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onOk(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        self.delegate?.selectIndex(index: selectedIndex, isClub: isClub)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TeamSelectionVC{
    func initalSetup(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        if isClub{
            lblHeader.text = "Select Club"
        }else{
            lblHeader.text = "Select Team"
        }
        registerCells()
    }
    
    func registerCells(){
        tblVW.separatorStyle = .none
        tblVW.register(TableViewCell.nib(), forCellReuseIdentifier: TableViewCell.identifier)
        tblVW.delegate = self
        tblVW.dataSource = self
        tblVW.reloadData()
    }
}

extension TeamSelectionVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tblVW.reloadData()
    }
}

extension TeamSelectionVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isClub{
            if let arrClubList = self.clubListResponse?.result{
                return arrClubList.count + 1
            }
            return 1
        }else{
            if let arrSubTeam = self.subTeamListResponse?.arrSubTeam{
                return arrSubTeam.count + 1
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier) as?TableViewCell else {
            return UITableViewCell()
        }
        var text = ""
        if isClub{
         
            if indexPath.row == 0{
                text = "All Clubs"
            }else{
                text =  self.clubListResponse?.result?[indexPath.row - 1].strDisplayName ?? ""
                //cell.imgLogo.set_image(self.clubListResponse?.result?[indexPath.row - 1].strProfilePicLink ?? "")
                cell.imgLogo.imageFromURL(urlString: self.clubListResponse?.result?[indexPath.row - 1].strProfilePicLink ?? "")
            }
            
        }else{
         
            if indexPath.row == 0{
                text = "All"
            }else{
                text =  self.subTeamListResponse?.arrSubTeam?[indexPath.row - 1].name ?? ""
                //cell.imgLogo.set_image( self.subTeamListResponse?.arrSubTeam?[indexPath.row - 1].banner_img_url ?? "")
                cell.imgLogo.imageFromURL(urlString: self.subTeamListResponse?.arrSubTeam?[indexPath.row - 1].banner_img_url ?? "")
            }
    }
        cell.lblTitle.text = text
        cell.lblAll.isHidden = true
        cell.imgLogo.isHidden = false
        if indexPath.row == 0{
            cell.lblAll.isHidden = false
            cell.imgLogo.isHidden = true
        }
        cell.btnTick.isSelected = false
        cell.lblTitle.textColor = .black
        cell.vwBackground.backgroundColor = .white
        if indexPath.row == selectedIndex{
            cell.btnTick.isSelected = true
            cell.lblTitle.textColor = .white
            cell.vwBackground.backgroundColor = UIColor.hflBlue
        }
        
        cell.selectionStyle = .none
    return cell
}

}
