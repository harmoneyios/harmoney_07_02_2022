//
//  HFLEarnPopup.swift
//  Harmoney
//
//  Created by Saravanan on 18/10/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class HFLEarnPopup: UIViewController {

    @IBOutlet weak var btnDontShow: UIButton!
    var imgLogoUrl : String?
    
    @IBOutlet weak var imgLogo: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
        // Do any additional setup after loading the view.
    }
    @IBAction func onDontShowThis(_ sender: Any) {
        btnDontShow.isSelected = !btnDontShow.isSelected
    }
    
    @IBAction func onClose(_ sender: Any) {
        UserDefaultConstants.shared.isDontShowPopup = btnDontShow.isSelected
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension HFLEarnPopup{
    func initalSetup(){
        imgLogo.set_image(imgLogoUrl ?? "")
    }
}
