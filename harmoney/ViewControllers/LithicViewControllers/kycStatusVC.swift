//
//  kycStatusVC.swift
//  Harmoney
//
//  Created by Thirukumaran on 30/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class kycStatusVC: UIViewController {

    @IBOutlet weak var btnContinue: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onClickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onclickRefreshBtn(_ sender: Any) {
        let addLithicBankaccountVc = GlobalStoryBoard().addLithicBankVC
        addLithicBankaccountVc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(addLithicBankaccountVc, animated: true)
    }
    
    
    @IBAction func onClickContinuebtn(_ sender: Any) {
        let addLithicBankaccountVc = GlobalStoryBoard().addLithicBankVC
        addLithicBankaccountVc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(addLithicBankaccountVc, animated: true)
    }
    
}
