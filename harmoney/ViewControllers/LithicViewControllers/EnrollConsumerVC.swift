//
//  EnrollConsumerVC.swift
//  Harmoney
//
//  Created by Thirukumaran on 30/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class EnrollConsumerVC: UIViewController {


    @IBOutlet weak var lblLithicTitle: UINavigationItem!
    override func viewDidLoad() {
      
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    
    

    @IBAction func onClickCloseBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickRequestKycbtn(_ sender: Any) {
        let kycStatusVc = GlobalStoryBoard().KycStatusVC
        kycStatusVc.hidesBottomBarWhenPushed = true
        lblLithicTitle.titleView?.isHidden = true
        self.navigationController?.pushViewController(kycStatusVc, animated: true)
    }
    

}
