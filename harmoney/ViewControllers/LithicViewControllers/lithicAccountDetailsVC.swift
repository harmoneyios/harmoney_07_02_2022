//
//  lithicAccountDetailsVC.swift
//  Harmoney
//
//  Created by Thirukumaran on 30/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class lithicAccountDetailsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        colVwCardDetails.delegate = self
        colVwCardDetails.dataSource = self
        colVwCardDetails.register(UINib(nibName: liticCardColVC.identifier, bundle: .main), forCellWithReuseIdentifier: liticCardColVC.identifier)
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var colVwCardDetails: UICollectionView!
    
    @IBAction func onClickNewCardbtn(_ sender: Any) {
        let LithicCreateCardVc = GlobalStoryBoard().LithicCreatecardVC
        LithicCreateCardVc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(LithicCreateCardVc, animated: true)
    }
    
    @IBAction func onClickBckbtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension lithicAccountDetailsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = colVwCardDetails.dequeueReusableCell(withReuseIdentifier: liticCardColVC.identifier, for: indexPath) as! liticCardColVC
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 384, height: 201)
    }
    
    
    
}
