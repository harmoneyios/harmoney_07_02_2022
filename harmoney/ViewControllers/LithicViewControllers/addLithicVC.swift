//
//  addLithicVC.swift
//  Harmoney
//
//  Created by Thirukumaran on 30/11/21.
//  Copyright © 2021 harmoney. All rights reserved.
//

import UIKit

class addLithicVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBackbtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddBankbtn(_ sender: Any) {
        let BankaccountdetailsVc = GlobalStoryBoard().LithicAcctDetailsVC
        BankaccountdetailsVc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(BankaccountdetailsVc, animated: true)
    }
    

}
