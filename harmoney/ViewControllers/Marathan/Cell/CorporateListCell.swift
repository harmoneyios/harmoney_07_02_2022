//
//  CorporateListCell.swift
//  FitKet
//
//  Created by Mac on 27/10/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

class CorporateListCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var borderView: UIView!
    @IBOutlet var iconImageview: UIImageView!

    static let defaultid = "CorporateListCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
