//
//  homeDefaultCollectionViewCell.swift
//  FitKet
//
//  Created by vaish navi on 06/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

class homeDefaultCollectionViewCell: UICollectionViewCell {
    
        @IBOutlet  var baseView: UIView!
        @IBOutlet weak var join_now_btn: UIButton!
        @IBOutlet weak var banner_btn: UIButton!
        @IBOutlet weak var banner_background_image_view: UIImageView!
        @IBOutlet weak var banner_statistics_label: UILabel!
        @IBOutlet weak var karma_coins_title_label: UILabel!
        @IBOutlet weak var banner_karma_view: UIView!
        @IBOutlet weak var banner_runs_title_lbl: UILabel!
        @IBOutlet weak var banner_runs_view: UIView!
        @IBOutlet weak var banner_my_performance_label: HPLabel!
        @IBOutlet weak var banner_team_logo_imageView: UIImageView!
        @IBOutlet weak var banner_sponcer_imageView: UIImageView!
        @IBOutlet weak var banner_league_logo_imageView: UIImageView!
        @IBOutlet weak var banner_karma_coins_label: UILabel!
        @IBOutlet weak var banner_total_runs_label: UILabel!
        @IBOutlet weak var banner_top_image_view: UIImageView!
        @IBOutlet weak var viewExtension: UIView!
        @IBOutlet weak var joinImageView: UIImageView!

    @IBOutlet  var baseView_WidthConstraint: NSLayoutConstraint!
    @IBOutlet  var baseView_heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomStatusview: UIView!
    @IBOutlet weak var bottomStatusLabel: UILabel!

    @IBOutlet var layerImg: UIImageView!
    @IBOutlet var joinNowBtn: UIButton!
    @IBOutlet var organizedLbl: UILabel!
    static let defaultid = "homeDefaultCollectionViewCell"
      
    @IBOutlet weak var backgroundUrlImageview: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        bottomStatusview.layer.masksToBounds = true
        bottomStatusview.clipsToBounds = true
        bottomStatusview.layer.cornerRadius = 7
        bottomStatusview.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            self.baseView_heightConstraint.constant = 140
                   baseView.layer.cornerRadius = 7
                   baseView.addShadow(color: UIColor.black.withAlphaComponent(0.7))
                
      
        // Initialization code
    }
    
    
    func manage_banner_viewsSetting(_ is_team_available:Bool,leagueInfos:BannerLeagueInfo){

        if is_team_available {
            self.joinNowBtn.isHidden = false
            self.banner_runs_title_lbl.isHidden = true
            self.banner_total_runs_label.isHidden = true
            self.banner_karma_coins_label.isHidden = true
            self.banner_karma_view.isHidden = true
            self.karma_coins_title_label.isHidden = true
            self.banner_statistics_label.isHidden = true
            self.banner_my_performance_label.isHidden = true
            self.banner_runs_view.isHidden = true
            self.banner_statistics_label.text = ""
            self.viewExtension.isHidden = true

        }else{
            self.joinNowBtn.isHidden = true
            self.banner_runs_title_lbl.isHidden = false
            self.banner_total_runs_label.isHidden = false
            self.banner_karma_coins_label.isHidden = false
            self.banner_karma_view.isHidden = false
            self.karma_coins_title_label.isHidden = false
            self.banner_statistics_label.isHidden = false
            self.banner_my_performance_label.isHidden = false
            self.banner_runs_view.isHidden = false
            self.banner_total_runs_label.text = "\(Int(leagueInfos.intTotalRun ))"
            
            self.banner_karma_coins_label.text = String(format: "%.2f", leagueInfos.intTotalKarmaPoints )
            self.banner_statistics_label.text = "Statistics"
            self.viewExtension.isHidden = false

        }
        self.bottomStatusLabel.text = "\(leagueInfos.leagueStartInfo)"

    }
    
    
        func manage_banner_views(_ is_team_available:Bool){
            

            if is_team_available {
                self.banner_my_performance_label.isHidden = false
                self.banner_runs_view.isHidden = false
                self.banner_runs_title_lbl.isHidden = false
                self.banner_total_runs_label.isHidden = false
                self.banner_karma_coins_label.isHidden = false
                self.banner_karma_view.isHidden = false
                self.karma_coins_title_label.isHidden = false
                self.banner_team_logo_imageView.isHidden = false
                self.banner_statistics_label.text = "Statistics"
                self.viewExtension.isHidden = false
                self.joinImageView.isHidden = true
                self.joinNowBtn.isHidden = true
                // Nelson

//                FitketSingleton.sharedInstance.isjoinBtnHidden = false
                // Nelson

    //            self.banner_background_image_view.image = UIImage(named:"home_banner")
            }else{
                self.resetAllDefaults()
                // Nelson

//                FitketSingleton.sharedInstance.teamId = nil
                // Nelson

                self.banner_my_performance_label.isHidden = true
                self.banner_runs_view.isHidden = true
                self.banner_runs_title_lbl.isHidden = true
                self.banner_total_runs_label.isHidden = true
                self.banner_karma_coins_label.isHidden = true
                self.banner_karma_view.isHidden = true
                self.karma_coins_title_label.isHidden = true
                self.banner_team_logo_imageView.isHidden = true
                self.banner_statistics_label.text = ""
                self.viewExtension.isHidden = true
                self.joinImageView.isHidden = false

    //            self.banner_background_image_view.image = UIImage(named:"home_banner_when_no_team")
            }
        }
    
        func manage_banner_views_forCorporates(_ is_team_available:Bool){
            
                self.resetAllDefaults()
                self.banner_my_performance_label.isHidden = true
                self.banner_runs_view.isHidden = true
                self.banner_runs_title_lbl.isHidden = true
                self.banner_total_runs_label.isHidden = true
                self.banner_karma_coins_label.isHidden = true
                self.banner_karma_view.isHidden = true
                self.karma_coins_title_label.isHidden = true
                self.banner_team_logo_imageView.isHidden = true
                self.banner_statistics_label.text = ""
                self.viewExtension.isHidden = true
                self.joinImageView.isHidden = false
                self.joinNowBtn.isHidden = false

    //            self.banner_background_image_view.image = UIImage(named:"home_banner_when_no_team")
            
        }
    // Nelson
//    func load_banner(_ user_info:userInfo?){
//        self.banner_total_runs_label.text = "\(Int(user_info?.intTotalRun ?? 0).withCommas())"
//        self.banner_karma_coins_label.text = "\(Int(user_info?.intTotalKarmaPoints ?? 0).withCommas())"
//        self.banner_team_logo_imageView.set_image(user_info?.strTeamFlag ?? "")
//        self.banner_top_image_view.isHidden = true
//
//    }
    // Nelson


    func resetAllDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if key != "ShowFirstTime" {
                if key != "currentTime" {
                    if key != "NumberOfSteps"{
                        if key != "savedDate"{
                           if key != "isTeamSelected"{
                            if key != "userDefaultsLeagueId"{
                                 if key != "userdefaultsTeamId"{
                                   if key != "userDefaultsGuid"{
                                    defaults.removeObject(forKey: key)}
                                }
                            }
                            }
                        }
                    }
                }
            }
        }
        defaults.synchronize()
    }


}
