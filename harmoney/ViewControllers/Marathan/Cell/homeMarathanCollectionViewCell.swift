//
//  homeMarathanCollectionViewCell.swift
//  FitKet
//
//  Created by vaish navi on 06/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit
import UICircularProgressRing

class homeMarathanCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet  var TimeLabel: UILabel!
    @IBOutlet  var DayLabel: UILabel!
    @IBOutlet  var YearLabel: UILabel!
    @IBOutlet  var monthLabel: UILabel!
    
    @IBOutlet  var distanceLabel: UILabel!
    
    @IBOutlet  var eventLogoImageview: UIImageView!
    @IBOutlet  var eventSponsorLogoImageview: UIImageView!
    
    @IBOutlet  var rankLabel: UILabel!
    @IBOutlet  var rankView: UIView!
    @IBOutlet  var baseView: UIView!
    @IBOutlet  var distanceView: UIView!
    @IBOutlet  var distanceParentView: UIView!
    @IBOutlet weak var join_now_btn: UIButton!
    @IBOutlet var ditanceProgressView: UICircularProgressRing!
    static let marathanid = "homeMarathanCollectionViewCell"
    @IBOutlet  var viewOneWidthConstraint: NSLayoutConstraint!
    @IBOutlet  var join_btn_WidthConstraint: NSLayoutConstraint!
    
    @IBOutlet  var baseView_WidthConstraint: NSLayoutConstraint!
    @IBOutlet  var baseView_heightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var downloadandDetailV: UIView!
    
    @IBOutlet var downloadPdf_btn: UIButton!
    @IBOutlet var viewDetail_btn: UIButton!
    @IBOutlet var cornerProgressView: UIView!

    @IBOutlet var walkLabel: UILabel!

//       var viewModel: coroporatePDFDownloadModel!
       var homeViewController: HomeViewController!
//       var GUID_Model: FPL_GUID_Model!
    
    var selectedIndex = 0
       var selectedIndexpath = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cornerProgressView.layer.cornerRadius = 15
        
        join_btn_WidthConstraint.constant = self.frame.size.width * 0.35
        baseView_heightConstraint.constant = 140
        
        viewOneWidthConstraint.constant = self.frame.size.width * 0.4
        join_now_btn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        baseView.layer.cornerRadius = 7
        baseView.addShadow(color: UIColor.black.withAlphaComponent(0.7))
        
        //        baseView.layer.shadowColor = UIColor(red: 35/255, green: 136/255, blue: 243/255, alpha: 0.25).cgColor
        //        baseView.layer.shadowOpacity = 1.0
        //        baseView.layer.shadowOffset = .init(width: 5 , height: 10)
        //        baseView.layer.shadowRadius = 5
        
        distanceView.layer.cornerRadius = 30
        ditanceProgressView.isClockwise =  true
        ditanceProgressView.innerRingColor = UIColor(hex: 0x2482E2)
        ditanceProgressView.innerRingWidth = 4
        ditanceProgressView.innerCapStyle = .round
        ditanceProgressView.startAngle = -90
        ditanceProgressView.endAngle = -90
        ditanceProgressView.style = .ontop
        ditanceProgressView.outerRingColor = UIColor(hex: 0xECEEF2)
        ditanceProgressView.outerRingWidth = 2
        ditanceProgressView.minValue = 0
        ditanceProgressView.maxValue = 100 // Based on max runs
        
        
    }
    
    
}
    



