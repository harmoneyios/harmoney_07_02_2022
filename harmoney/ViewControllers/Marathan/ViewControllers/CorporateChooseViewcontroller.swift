//
//  CorporateChooseViewcontroller.swift
//  FitKet
//
//  Created by Mac on 27/10/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

class  CorporateChooseViewcontroller: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var CorporateChooseTableview: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var continueBtnBottomConstraint: NSLayoutConstraint!

    var corporateModel: corporateListModel!
    var corporateList: corporateListData!
    var filterCorporateList: corporateListData!

    var accountManager: AccountManager!
    var homeViewController: HomeViewController!
    var GUID_Model: FPL_GUID_Model!
    
    var selectedIndex = 0
    var selectedIndexItem = -1

    var searchActive : Bool = false

    
    lazy var MarathanBoard:UIStoryboard = {
        let board = UIStoryboard(name: Storyboard_Names.Marathan, bundle: Bundle.main)
        return board
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        searchBar.delegate = self
        CorporateChooseTableview.register(UINib(nibName: CorporateListCell.defaultid, bundle: nil), forCellReuseIdentifier: CorporateListCell.defaultid)

        
        corporateModel.GetCorporateList() { [weak self](success, data, message, error) in
            guard success, error == nil else {
                self?.hideLoader()
                self?.handleError(error: error, message: message)
                return
            }
            
            self?.hideLoader()
            self?.corporateList = data
            self?.filterCorporateList = data

           
            self?.CorporateChooseTableview.reloadData()
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
           searchActive = true;
       }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
           searchActive = false;
       }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
           searchActive = false;
       }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
           searchActive = false;
       }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        selectedIndexItem = -1
        
        if filterCorporateList.result?.count ?? 0 > 0{
            filterCorporateList.result?.removeAll()
        }
        if(corporateList.result?.count ?? 0 > 0){
            
            for Index in corporateList.result! {
                if (Index.businessName?.lowercased().contains(searchText.lowercased())) == true {
                    filterCorporateList.result?.append(Index)
                }
            }
        }

        
       
        
//        filterCorporateList = corporateList.filter({($0.itemCategory.localizedCaseInsensitiveContains(searchText))!})

//        filterCorporateList = corporateList.filter({ (text) -> Bool in
//               let tmp: NSString = text
//               let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
//               return range.location != NSNotFound
//           })
        if((filterCorporateList.result?.count == 0) && (searchText == "")){
               searchActive = false
           } else {
               searchActive = true
           }
           self.CorporateChooseTableview.reloadData()
       }
    
    @IBAction func backAct(_ sender: Any) {
        
         self.dismiss(animated: true, completion: nil)
    }
    @IBAction func continueBtnAction(_ sender: Any) {
        
        if selectedIndexItem == -1 {
            toast(message: "Please select corporate company")
            return
        }
        let marathanVC = self.MarathanBoard.instantiateViewController(withIdentifier: "CorporateLoginViewController") as! CorporateLoginViewController
        marathanVC.viewModel = corporateLoginAndUpdateModel( accountManager: accountManager)
        marathanVC.loginTypeModel = corporateLoginTypeModel( accountManager: accountManager)
        marathanVC.GUID_Model = GUID_Model
        marathanVC.selectedIndex = 0
        if(searchActive) {
            marathanVC.corporateList = filterCorporateList
        }else {
            marathanVC.corporateList = corporateList
        }
        marathanVC.selectedObject =  selectedIndexItem
        marathanVC.homeViewController = homeViewController
        marathanVC.accountManager = accountManager
        self.present(marathanVC, animated: true)
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            continueBtnBottomConstraint.constant = keyboardSize.size.height + 40
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            continueBtnBottomConstraint.constant = 40
            
        }
        
    }

}


extension CorporateChooseViewcontroller: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return self.filterCorporateList?.result?.count ?? 0
        }
        return self.corporateList?.result?.count ?? 0
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: CorporateListCell.defaultid, for: indexPath) as! CorporateListCell

        if selectedIndexItem == indexPath.item {
            cell.borderView.layer.borderColor = UIColor.blue.cgColor
            cell.borderView.layer.borderWidth = 1
            cell.borderView.layer.cornerRadius = 4

        }else {
            cell.borderView.layer.borderColor = UIColor.lightGray.cgColor
            cell.borderView.layer.borderWidth = 1
            cell.borderView.layer.cornerRadius = 4
        }
        if(searchActive) {
            if let indexdata = self.filterCorporateList?.result?[indexPath.row] {
                
                cell.titleLabel.text = indexdata.businessName
                if let imgPath = indexdata.consoleLogo {
                    cell.iconImageview.setAlamofireImage(imgPath)
                }
            }

        }else {
            if let indexdata = self.corporateList?.result?[indexPath.row] {
                
                cell.titleLabel.text = indexdata.businessName
                
                if let imgPath = indexdata.consoleLogo {
                    cell.iconImageview.setAlamofireImage(imgPath)
                }
            }
        }
        return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexItem = indexPath.item
        CorporateChooseTableview.reloadData()
    }
    
}

