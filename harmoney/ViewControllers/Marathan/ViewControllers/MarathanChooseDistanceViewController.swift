//
//  MarathanChooseDistanceViewController.swift
//  FitKet
//
//  Created by Mac on 02/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

class  MarathanChooseDistanceViewController: UIViewController {
    
//    @IBOutlet var detailView: UIView!
    
    @IBOutlet var detailScroll: UIScrollView!

    @IBOutlet var optionSelection: UICollectionView!
    @IBOutlet var DescTextview: UITextView!
    @IBOutlet var eventLogoImageview: UIImageView!
    @IBOutlet var eventSponserLogoImageview: UIImageView!

    @IBOutlet var DescTextviewHeightConstraint: NSLayoutConstraint!

    var viewModel: coroporateEventRegisterModel!
    
    var homeViewController: HomeViewController!
//    var GUID_Model: FPL_GUID_Model!
    var selectedIndex = 0
    var selectedIndexpath = 0
    var guidDetailMArray = [Any]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        self.getSettingsdata()
        
        let object = self.guidDetailMArray[selectedIndex]
        if object is Corporate_Event_Array{
        let eventInfo = object as? Corporate_Event_Array

        let imgPathSec = eventInfo?.eventLogo ?? ""
         eventLogoImageview.set_image(imgPathSec)
        
        let imgPath = eventInfo?.eventSponsorLogo ?? ""
         eventSponserLogoImageview.set_image(imgPath)

        DescTextview.text = eventInfo?.eventDesc ?? ""
        
        }
//        DescTextview.sizeToFit()
            detailScroll.layer.cornerRadius = 10
            detailScroll.layer.shadowColor = UIColor(red: 35/255, green: 136/255, blue: 243/255, alpha: 0.16).cgColor
            detailScroll.layer.shadowOpacity = 1.0
            detailScroll.layer.shadowOffset = .init(width: 1 , height: 10)
            detailScroll.layer.shadowRadius = 5
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 100, height:100)
         
        layout.scrollDirection = .horizontal
        optionSelection.collectionViewLayout = layout


        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.DescTextviewHeightConstraint.constant = self.DescTextview.contentSize.height
          self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func CoroporateEventRegister(_ sender: Any)
    {
           
               showLoader()
        
        let object = self.guidDetailMArray[selectedIndex]
        if object is Corporate_Event_Array{
        let eventInfo = object as? Corporate_Event_Array

        let eventId = eventInfo?.eventId ?? ""
                   
        let eventdate = eventInfo?.eventStateDate ?? ""
        
            let eventType = (eventInfo?.eventTypes?[selectedIndexpath].value ?? "2")
      

               viewModel.CoroporateEventRegister(eventId: eventId,eventType: eventType,eventdate: eventdate) { [weak self](success, data, message, error) in
                   guard success, error == nil else {
                    

                       self?.hideLoader()
                       self?.handleError(error: error, message: message)
                       return
                   }
                UserDefaults.standard.set(0, forKey: "NumberOfSteps")  //Integer

                self?.homeViewController.apiCallToGetGUID()

                self?.hideLoader()
                
               self?.dismiss(animated: true, completion: nil)

               }
            }
       }

       }
    

extension MarathanChooseDistanceViewController : UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
          let fixedWidth = textView.frame.size.width
          textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
          let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
          var newFrame = textView.frame
          newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        DescTextviewHeightConstraint.constant = newFrame.height
        self.view.layoutIfNeeded()
    }


}
extension MarathanChooseDistanceViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let object = self.guidDetailMArray[selectedIndex]
        if object is Corporate_Event_Array{
        let eventInfo = object as? Corporate_Event_Array
            return eventInfo?.eventTypes?.count ?? 0
        }

        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DummyCollectionCell", for: indexPath) as! DummyCollectionCell

        cell.viewOne.backgroundColor = UIColor.white

        cell.viewOne.backgroundColor = UIColor.lightGray

        if selectedIndexpath == indexPath.item {
            cell.viewOne.backgroundColor = UIColor(red: 35/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        let object = self.guidDetailMArray[selectedIndex]
        if object is Corporate_Event_Array{
        let eventInfo = object as? Corporate_Event_Array

        var raceType = "Walk or Run"
        if (eventInfo?.raceType ?? "Steps") == "Steps"{
            raceType = "Steps"
        }

        cell.titleLabel.text = (eventInfo?.eventTypes?[indexPath.item].text ?? "2") + "\n" + raceType
        }
        
        cell.viewOne.layer.cornerRadius = 10
        cell.viewOne.layer.shadowColor = UIColor(red: 35/255, green: 136/255, blue: 243/255, alpha: 0.75).cgColor
        cell.viewOne.layer.shadowOpacity = 1.0
        cell.viewOne.layer.shadowOffset = .init(width: 1 , height: 10)
        cell.viewOne.layer.shadowRadius = 5

        
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexpath = indexPath.item
        optionSelection.reloadData()

    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = ((self.view.frame.size.width - 20)/5)
//        return CGSize(width: width, height: 110)
//    }

    
}


class DummyCollectionCell: UICollectionViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var viewOne: UIView!
//    override func awakeFromNib() {
//                super.awakeFromNib()
//                // Initialization code
////            viewOne.layer.cornerRadius = 10
////            viewOne.layer.shadowColor = UIColor.lightGray.cgColor
////            viewOne.layer.shadowOpacity = 1.0
////            viewOne.layer.shadowOffset = .init(width: 3 , height: 10)
////            viewOne.layer.shadowRadius = 10
//            }
    
}
