//
//  CorporateLoginViewController.swift
//  marathan
//
//  Created by vaish navi on 01/08/20.
//  Copyright © 2020 vaish navi. All rights reserved.
//

import UIKit

class CorporateLoginViewController: UIViewController {

    
    @IBOutlet  var loginID: UITextField!
    @IBOutlet  var uniqueID: UITextField!
    @IBOutlet var scrollV: UIScrollView!
    
    @IBOutlet var passwordPlaceholderLabel: UILabel!
    @IBOutlet var logoImageview: UIImageView!

    @IBOutlet var placeholderLabel: UILabel!

//    @IBOutlet var passwordplaceHolderHeightConstraint: NSLayoutConstraint!
//    @IBOutlet var passwordTextFieldHeightConstraint: NSLayoutConstraint!

    var GUID_Model: FPL_GUID_Model!
    var selectedIndex = 0
    var selectedObject = 0
    var verificationType = 0
    var is_comes_from_through_tabbar = false

    var viewModel: corporateLoginAndUpdateModel!
    var loginTypeModel: corporateLoginTypeModel!

    var accountManager: AccountManager!
    
     var homeViewController: HomeViewController!
    
    var corporateList: corporateListData!

//     var loginType = false
    
    lazy var MarathanBoard:UIStoryboard = {
          let board = UIStoryboard(name: "Marathan", bundle: Bundle.main)
          return board
      }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.frame = UIScreen.main.bounds

//        if #available(iOS 13.0, *) {
//            self.isModalInPresentation = true
//        } else {
//            // Fallback on earlier versions
//        }
        
        if let indexdata = self.corporateList?.result?[selectedObject] {
            if let imgPath = indexdata.consoleLogo {
                logoImageview.setAlamofireImage(imgPath)
            }
            if indexdata.logintype == 1{
                passwordPlaceholderLabel.text = "Enter your Login ID and Password to continue"
                self.placeholderLabel.text = "Password"
            }else {
                passwordPlaceholderLabel.text = "Enter your Login ID and OTP to continue"
                self.placeholderLabel.text = "OTP"
            }
        }
        
        
       

        
        // Do any additional setup after loading the view.
        loginID.delegate = self
        uniqueID.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
          
           self.tabBarController?.tabBar.isHidden = true
       }
    
    @IBAction func backAction(_ sender: Any) {
          
          if is_comes_from_through_tabbar == false {
            self.dismiss(animated: true, completion: nil)
          }else{
              // navigate to home screen
              self.tabBarController?.selectedIndex = 2
          }
          
      }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            var contentInset:UIEdgeInsets = self.scrollV.contentInset
            contentInset.bottom = keyboardSize.size.height + 50
            scrollV.contentInset = contentInset
//            scrollV.setContentOffset(CGPoint(x: self.scrollV.contentOffset.x, y: -100), animated: true)


        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            scrollV.contentInset = contentInset
        }
        
    }
   
   
    
}

extension CorporateLoginViewController {
    
    @IBAction func nxtAct(_ sender: Any) {
        
        self.view.endEditing(true)
        
             guard let firstName = loginID.text, !firstName.isEmpty else {
            
                   toast(message: "Please enter login id!")
                   return
            
               }
        
//        if !loginType {
//            showLoader()
//            loginTypeModel.CoroporateUserLogInType(loginId:loginID.text ?? "")
//                {
//                    [weak self](success, data, message, error) in
//                    guard success, error == nil else {
//                        self?.hideLoader()
//                        self?.handleError(error: error, message: message)
//                        return
//                    }
//
//                    self?.verificationType = data?.type ?? 0
//                    self?.loginType = true
//                    if data?.type == 1{
//                        self?.passwordPlaceholderLabel.text = "Password"
//                    }else {
//                        self?.passwordPlaceholderLabel.text = "OTP"
//                    }
//                    self?.passwordplaceHolderHeightConstraint.constant  = 21
//                    self?.passwordTextFieldHeightConstraint.constant  = 40
//                    self?.hideLoader()
//
//                }
//
//        }
//        else {

    
            guard let pswrd = uniqueID.text, !pswrd.isEmpty else {
                
                       toast(message: "Please enter password")
                       return
                   }
            showLoader()

        let indexdata = self.corporateList?.result?[selectedObject]

        viewModel.corporateLogin(userId: firstName,verificationCode: pswrd,verificationType:indexdata?.logintype ?? 1,corporateId:indexdata?.code ?? "")
            {
                [weak self](success, data, message , error) in
                guard success, error == nil else {
                    self?.hideLoader()
                    self?.handleError(error: error, message: message)
                    return
                }
                self?.hideLoader()
                if data?.isProfileUpdate == false{
                    
                    let marathanVC = self?.MarathanBoard.instantiateViewController(withIdentifier: "CorporateProfileViewController") as! CorporateProfileViewController
                    marathanVC.viewModel = corporateLoginAndUpdateModel( accountManager:self!.accountManager)
                    marathanVC.GUID_Model = self?.GUID_Model
                    marathanVC.profileMandatoryFields = data?.profileMandatoryFields
                    marathanVC.selectedIndex = self?.selectedIndex ?? 0
                    marathanVC.homeViewController = self?.homeViewController
                    self?.present(marathanVC, animated: true)
                }else{
                    
                    self?.homeViewController.isCorporateUser = true
                    self?.homeViewController.apiCallToGetGUID()
                    self?.homeViewController.hideCorporateView()
                    self?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                }
                
                
            }
//        }
        

        
        
//        let marathanVC = self.MarathanBoard.instantiateViewController(withIdentifier: "CorporateProfileViewController") as! CorporateProfileViewController
//        self.present(marathanVC, animated: true)


        }
    
}
    
extension CorporateLoginViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool { // return NO to disallow editing.
        print("textFieldShouldBeginEditing")
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) { // became first responder
        print("textFieldDidBeginEditing")

    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        print("textFieldShouldEndEditing")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)  {// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("textFieldDidEndEditing")

    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {// if implemented, called in place of textFieldDidEndEditing:
        print("textFieldDidEndEditing")

        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {// return NO to not change text
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {// called when clear button pressed. return NO to ignore (no notifications)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {// called when 'return' key pressed. return NO to ignore.
         textField.resignFirstResponder()
        return true
    }
}
