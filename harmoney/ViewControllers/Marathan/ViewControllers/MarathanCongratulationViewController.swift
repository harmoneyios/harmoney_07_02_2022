//
//  MarathanCongratulationViewController.swift
//  FitKet
//
//  Created by Mac on 02/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

class  MarathanCongratulationViewController: UIViewController {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var cupImageview: UIImageView!

    @IBOutlet var distance: UILabel!
    @IBOutlet var rankLabel: UILabel!
    @IBOutlet var eventLogoImageview: UIImageView!
    @IBOutlet var eventSponserLogoImageview: UIImageView!
//    var GUID_Model: FPL_GUID_Model!
    var guidDetailMArray = [Any]()
    var selectedIndex = 0
    var isReached = false
    
    @IBOutlet var detailView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !isReached {
            titleLbl.text = "Not Reached"
            descriptionLbl.text = "Sorry you haven't reached the destination"
            cupImageview.isHidden = true
            rankLabel.isHidden = true
        }
        
        let object = self.guidDetailMArray[selectedIndex]
        if object is Corporate_Event_Array{
        let eventInfo = object as? Corporate_Event_Array

            
        var raceType = " Walk or Run"
               if (eventInfo?.raceType ?? "Steps") == "Steps"{
                   raceType = " Steps"
               }
        
        distance.text = (eventInfo?.selectedEventTypesText ?? "") + raceType
        // Do any additional setup after loading the view.
        detailView.layer.cornerRadius = 10
                   detailView.layer.shadowColor = UIColor(red: 35/255, green: 136/255, blue: 243/255, alpha: 0.16).cgColor
                   detailView.layer.shadowOpacity = 1.0
                   detailView.layer.shadowOffset = .init(width: 1 , height: 10)
                   detailView.layer.shadowRadius = 5
        
        let imgPathSec =  eventInfo?.eventLogo ?? ""
         eventLogoImageview.set_image(imgPathSec)
        
        let imgPath =  eventInfo?.eventSponsorLogo ?? ""

         eventSponserLogoImageview.set_image(imgPath)
            if eventInfo?.rank == nil
            {
                self.rankLabel.text = "NA"
                
            }else{
                
                self.rankLabel.text = "\(eventInfo?.rank ?? 0)"
                
                
            }
            
    }

//        self.distance.text =  String(GUID_Model.response?.profile?.eventArray?[selectedIndex].rank)

       
    }
    @IBAction func doneBtnAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)

    }
    
}


