//
//  MarathanInstructionViewController.swift
//  FitKet
//
//  Created by Mac on 02/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

class  MarathanInstructionViewController: UIViewController {

    @IBOutlet var DescTextview: UITextView!
    @IBOutlet var eventLogoImageview: UIImageView!
    @IBOutlet var eventSponserLogoImageview: UIImageView!
    @IBOutlet var eventStartLabel: UILabel!

//    var GUID_Model: FPL_GUID_Model!
    var selectedIndex = 0
    var guidDetailMArray = [Any]()

    @IBOutlet var detailView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        detailView.layer.cornerRadius = 10
                   detailView.layer.shadowColor = UIColor(red: 35/255, green: 136/255, blue: 243/255, alpha: 0.16).cgColor
                   detailView.layer.shadowOpacity = 1.0
                   detailView.layer.shadowOffset = .init(width: 1 , height: 10)
                   detailView.layer.shadowRadius = 5
        
        let object = self.guidDetailMArray[selectedIndex]
        if object is Corporate_Event_Array{
        let eventInfo = object as? Corporate_Event_Array
            
        let imgPathSec = eventInfo?.eventLogo ?? ""
         eventLogoImageview.set_image(imgPathSec)
        
        let imgPath = eventInfo?.eventSponsorLogo ?? ""
         eventSponserLogoImageview.set_image(imgPath)

        let desc = eventInfo?.eventDesc ?? ""
        
        let role = eventInfo?.eventRules ?? ""
        DescTextview.text = desc + "/n/n" + role
        
        let date = eventInfo?.eventStateDate ?? ""
        
        let time = eventInfo?.eventStateTime ?? ""
        eventStartLabel.text = "Marathon will start on " + date +  " at " + time
            
        }
    }
    
    @IBAction func doneBtnAction(_ sender: Any) {
           
           self.dismiss(animated: true, completion: nil)

       }
}
