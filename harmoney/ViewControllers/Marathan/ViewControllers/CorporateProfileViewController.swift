//
//  CorporateProfileViewController.swift
//  marathan
//
//  Created by vaish navi on 01/08/20.
//  Copyright © 2020 vaish navi. All rights reserved.
//

import UIKit


class CorporateProfileViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate{

    
    @IBOutlet var scrollV: UIScrollView!
    @IBOutlet var mobileNumTxt: UITextField!
    @IBOutlet var DateTxt: UITextField!
    @IBOutlet var firstNameTxt: UITextField!
    @IBOutlet var lastNameTxt: UITextField!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var editImgView: UIImageView!
    @IBOutlet var malebtn: UIButton!
    @IBOutlet var femalebtn: UIButton!
    @IBOutlet var otherbtn: UIButton!
    @IBOutlet var eventLogoImageView: UIImageView!
    @IBOutlet var profileImgDescribtionLbl: UILabel!
    @IBOutlet var firstNameDescribtionLbl: UILabel!
    @IBOutlet var mobileNumberDescribtionLbl: UILabel!
    @IBOutlet var lastNameDescribtionLbl: UILabel!
    @IBOutlet var genderLbl: UILabel!
    @IBOutlet var userTypelbl: UILabel!
    @IBOutlet var categoryLbl: UILabel!
    @IBOutlet var categoryDropDownImg: UIImageView!
    @IBOutlet var userTypeDropDownImg: UIImageView!
    
    
    @IBOutlet var profileImagDescribtionHeightConstrain: NSLayoutConstraint! //50
    
    @IBOutlet var editImgViewHeightConstrain: NSLayoutConstraint! //70
    
    @IBOutlet var cameraBtnHeightConstrain: NSLayoutConstraint! //24
    
    @IBOutlet var firstNmeDescHeightConstrain: NSLayoutConstraint! //21
    
    @IBOutlet var firstNmeTxtheightconstrain: NSLayoutConstraint! // 40
    @IBOutlet var lastNmeDescHeightConstrain: NSLayoutConstraint! //21
    
    @IBOutlet var lastTxtheightconstrain: NSLayoutConstraint!//40
    
    @IBOutlet var mobileNumDescHeightConstrain: NSLayoutConstraint! //21
       
       @IBOutlet var mobilTxtheightconstrain: NSLayoutConstraint!//40
    @IBOutlet var GenderLblDescHeightConstrain: NSLayoutConstraint! //21
          
          @IBOutlet var malebtnheightconstrain: NSLayoutConstraint!//50
            
            @IBOutlet var Femalebtnheightconstrain: NSLayoutConstraint!//50
     @IBOutlet var otherbtnheightconstrain: NSLayoutConstraint!//50
    
    @IBOutlet var categoryLblDescHeightConstrain: NSLayoutConstraint! //21
     @IBOutlet var dateTxtheightconstrain: NSLayoutConstraint!//40
    @IBOutlet var userTypeLblDescHeightConstrain: NSLayoutConstraint! //21
        @IBOutlet var userTxtheightconstrain: NSLayoutConstraint!//40
     @IBOutlet var userTypeDropDownheightconstrain: NSLayoutConstraint!//30
     @IBOutlet var categoryTpyDropDownheightconstrain: NSLayoutConstraint!//30
    
    var profileMandatoryFields:ProfileMandatoryFields? = nil
    var GUID_Model: FPL_GUID_Model!
    var selectedIndex = 0
    var pickedImage : UIImage?
   let datePicker = UIDatePicker()
    @IBOutlet var userTypeTxt: UITextField!
    //    @IBOutlet var maleBtn: UIButton!
//    @IBOutlet var femaleBtn: UIButton!
    
    var is_comes_from_through_tabbar = false

    var genderSelected = ""
     var userTypeCode = ""
     var list = [String]()
    var categoryList = [String]()
    var viewModel: corporateLoginAndUpdateModel!
   var homeViewController: HomeViewController!

    
    lazy var MarathanBoard:UIStoryboard = {
           let board = UIStoryboard(name: "Marathan", bundle: Bundle.main)
           return board
       }()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        firstNameTxt.text = GUID_Model?.response?.profile?.strFirstName ?? ""
        lastNameTxt.text = GUID_Model?.response?.profile?.strLastName ?? ""
        if let profileURL = GUID_Model?.response?.profile?.strProfilePicLink {
            editImgView.sd_setImage(with: URL(string: profileURL))
        }

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(userDidTap(_:)))
        editImgView.addGestureRecognizer(tapGestureRecognizer)

        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }

//        let imgPathSec = GUID_Model.response?.profile?.eventArray?[selectedIndex].eventSponsorLogo ?? ""
//         eventLogoImageView.set_image(imgPathSec)
         list = homeViewController.settingProfileType
        
        let myPicker = UIPickerView()
        myPicker.delegate = self
        userTypeTxt.inputView = myPicker

        mobileNumTxt.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
//        self.DateTxt.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
        categoryList = ["Below 40 Years","Above 40 Years"]
        let myCategoryPicker = UIPickerView()
        myCategoryPicker.delegate = self
        DateTxt.inputView = myCategoryPicker
        myCategoryPicker.tag = 100
//  self.DateTxt.inputView = datePicker
               datePicker.addTarget(self, action: #selector(tapDone), for: .valueChanged)
               datePicker.datePickerMode = .date
        
        if let datePicker = self.DateTxt.inputView as? UIDatePicker { // 2-1
                    
                    let calendar = Calendar(identifier: .gregorian)

                    let currentDate = Date()
                    var components = DateComponents()
                    components.calendar = calendar

                    components.year = -8
                    components.month = 12
                    let maxDate = calendar.date(byAdding: components, to: currentDate)!

                    components.year = -80
                    let minDate = calendar.date(byAdding: components, to: currentDate)!

                    datePicker.minimumDate = minDate
                    datePicker.maximumDate = maxDate
                    print("minDate: \(minDate)")
                      print("maxDate: \(maxDate)")
      
                   }
        
        malebtn.tag = 1
        femalebtn.tag = 2
        otherbtn.tag = 3
        
        malebtn.layer.cornerRadius = 8.0
        malebtn.layer.borderWidth = 1.0
        malebtn.layer.borderColor = UIColor.gray.cgColor
        
        femalebtn.layer.cornerRadius = 8.0
        femalebtn.layer.borderWidth = 1.0
        femalebtn.layer.borderColor = UIColor.gray.cgColor
        
        otherbtn.layer.cornerRadius = 8.0
        otherbtn.layer.borderWidth = 1.0
        otherbtn.layer.borderColor = UIColor.gray.cgColor

        setupProfileMandatoryFields()
    }
    
    func setupProfileMandatoryFields(){
        setCategory()
        setUserType()
        setgender()
        setmobile()
        setprofilePic()
        setlastName()
        setFirstName()
        
    }
    
    func setCategory(){
        
        if self.profileMandatoryFields?.category.isShow == true{
            categoryLbl.text = self.profileMandatoryFields?.category.text
            categoryLbl.isHidden = false
            DateTxt.isHidden = false
            categoryDropDownImg.isHidden = false
            categoryLblDescHeightConstrain.constant = 21
            dateTxtheightconstrain.constant = 40
            categoryTpyDropDownheightconstrain.constant = 30
            
        }else{
            categoryLbl.isHidden = true
            DateTxt.isHidden = true
            categoryDropDownImg.isHidden = true
            categoryLblDescHeightConstrain.constant = 0
            dateTxtheightconstrain.constant = 0
            categoryTpyDropDownheightconstrain.constant = 0
        }
        
    }
    
    func setUserType(){
        
        if self.profileMandatoryFields?.userType.isShow == true{
            userTypelbl.text = self.profileMandatoryFields?.userType.text
            userTypelbl.isHidden = false
            userTypeTxt.isHidden = false
            userTypeDropDownImg.isHidden = false
            userTypeLblDescHeightConstrain.constant = 21
            userTxtheightconstrain.constant = 40
            userTypeDropDownheightconstrain.constant = 30
        }else{
            userTypelbl.isHidden = true
            userTypeTxt.isHidden = true
            userTypeDropDownImg.isHidden = true
            userTypeLblDescHeightConstrain.constant = 0
            userTxtheightconstrain.constant = 0
            userTypeDropDownheightconstrain.constant = 0
        }
        
    }
    func setFirstName(){
        
        if self.profileMandatoryFields?.firstName.isShow == true{
            firstNameDescribtionLbl.text = self.profileMandatoryFields?.firstName.text
            firstNameDescribtionLbl.isHidden = false
            firstNameTxt.isHidden = false
            firstNmeDescHeightConstrain.constant = 21
            firstNmeTxtheightconstrain.constant = 40
        }else{
            firstNameDescribtionLbl.isHidden = true
            firstNameTxt.isHidden = true
            firstNmeDescHeightConstrain.constant = 0
            firstNmeTxtheightconstrain.constant = 0
        }
        
    }
    
    func setlastName(){
        
        if self.profileMandatoryFields?.lastName.isShow == true{
            lastNameDescribtionLbl.text = self.profileMandatoryFields?.lastName.text
            lastNameDescribtionLbl.isHidden = false
            lastNameTxt.isHidden = false
            lastNmeDescHeightConstrain.constant = 21
            lastTxtheightconstrain.constant = 40
        }else{
            lastNameDescribtionLbl.isHidden = true
            lastNameTxt.isHidden = true
            lastNmeDescHeightConstrain.constant = 0
            lastTxtheightconstrain.constant = 0
        }
        
    }
    
    func setgender(){
        
        if self.profileMandatoryFields?.gender.isShow == true{
            genderLbl.text = self.profileMandatoryFields?.gender.text
            genderLbl.isHidden = false
            malebtn.isHidden = false
            femalebtn.isHidden = false
            otherbtn.isHidden = false
            GenderLblDescHeightConstrain.constant = 21
            malebtnheightconstrain.constant = 50
            Femalebtnheightconstrain.constant = 50
            otherbtnheightconstrain.constant = 50
        }else{
            genderLbl.isHidden = true
            malebtn.isHidden = true
            femalebtn.isHidden = true
            otherbtn.isHidden = true
            GenderLblDescHeightConstrain.constant = 0
            malebtnheightconstrain.constant = 0
            Femalebtnheightconstrain.constant = 0
            otherbtnheightconstrain.constant = 0
        }
        
    }
    func setmobile(){
        if self.profileMandatoryFields?.mobile.isShow == true{
            mobileNumberDescribtionLbl.text = self.profileMandatoryFields?.mobile.text
            mobileNumberDescribtionLbl.isHidden = false
            mobileNumTxt.isHidden = false
            mobileNumDescHeightConstrain.constant = 21
            mobilTxtheightconstrain.constant = 40
            
        }else{
            mobileNumberDescribtionLbl.isHidden = true
            mobileNumTxt.isHidden = true
            mobileNumDescHeightConstrain.constant = 0
            mobilTxtheightconstrain.constant = 0
       }
    }
    func setprofilePic(){
        if self.profileMandatoryFields?.profilePic.isShow == true{
            profileImgDescribtionLbl.text = self.profileMandatoryFields?.profilePic.text
            profileImgDescribtionLbl.isHidden = false
            cameraBtn.isHidden = false
            editImgView.isHidden = false
            profileImagDescribtionHeightConstrain.constant = 50
            cameraBtnHeightConstrain.constant = 24
            editImgViewHeightConstrain.constant = 70
            }else{
            profileImgDescribtionLbl.isHidden = true
            cameraBtn.isHidden = true
            editImgView.isHidden = true
            profileImagDescribtionHeightConstrain.constant = 0
            cameraBtnHeightConstrain.constant = 0
            editImgViewHeightConstrain.constant = 0
        }
        
    }
    
    
    @objc func userDidTap(_ recognizer: UITapGestureRecognizer) {
        showUploadImageOptions()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
    }
    @IBAction func getStart(_ sender: Any) {
        
        
        let firstName = firstNameTxt.text ?? ""
        if self.profileMandatoryFields?.firstName.mandatory == true{
            if firstName.isEmpty{
                
                toast(message: "Please enter first name!")
                return
            }
        }
        
        let lastName = lastNameTxt.text ?? ""
        if self.profileMandatoryFields?.lastName.mandatory == true{
            if lastName.isEmpty {
                
                toast(message: "Please enter last name!")
                return
                
            }
        }
        let mobileNum = mobileNumTxt.text ?? ""
        
        if self.profileMandatoryFields?.mobile.mandatory == true{
            if mobileNum.isEmpty{
                toast(message: "Please enter mobile number!")
                return
            }else if mobileNumTxt.text!.numberValidation == false {
                print("mobile number is incorrect format")
                toast(message: "mobile number is incorrect format")
                return
            }
            
        }
        
        
        if self.profileMandatoryFields?.gender.mandatory == true{
            if genderSelected.isEmpty {
                
                toast(message: "Please select gender!")
                return
                
            }
        }
        
        let dob = DateTxt.text ?? ""
        if self.profileMandatoryFields?.category.mandatory == true{
            if dob.isEmpty  {
                
                toast(message: "Please select Category!")
                return
            }
        }
        
        let userType = userTypeTxt.text ?? ""
        if self.profileMandatoryFields?.userType.mandatory == true{
            if userType.isEmpty  {
                
                toast(message: "Please select User Type!")
                return
            }
        }
        if self.profileMandatoryFields?.profilePic.mandatory == true{
            if editImgView == nil  {
                
                toast(message: "Please select profile picture!")
                return
            }
        }
        
        
        
        //        let userType = userTypeTxt.text
        //
        //        if userType == "Student"{
        //            userTypeCode = "STU"
        //        }else if userType == "Teaching Staff"{
        //            userTypeCode = "TS"
        //        }else if userType == "Non-Teaching Staff"{
        //            userTypeCode = "NTS"
        //        }
        
        showLoader()
        viewModel.corporateUpdateProfile(profilePic: pickedImage ?? nil, strFirstName: firstNameTxt.text ?? "", strLastName: lastNameTxt.text ?? "",strGender: genderSelected ,strDOB: DateTxt.text ?? "", strMobile : mobileNumTxt.text ?? "",strUserType : userType) { [weak self](success, data, message, error) in
            guard success, error == nil else {
                self?.hideLoader()
                self?.handleError(error: error, message: message)
                return
            }
            self?.hideLoader()
            self?.homeViewController.isCorporateUser = true
            self?.homeViewController.apiCallToGetGUID()
            self?.homeViewController.hideCorporateView()
            
            self?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
            
            //            self?.corporateLoginViewController.dismiss(animated: false, completion: nil)
            //            self?.dismiss(animated: true, completion: nil)
            
            //                   let marathanVC = self?.MarathanBoard.instantiateViewController(withIdentifier: "MarathanCongratulationViewController") as! MarathanCongratulationViewController
            //                   self?.present(marathanVC, animated: true)
        }
        
        
    }
    @objc func tapDone() {
           if let datePicker = self.DateTxt.inputView as? UIDatePicker { // 2-1
               let dateformatter = DateFormatter() // 2-2
//               dateformatter.dateStyle = .medium // 2-3
            dateformatter.dateFormat = "YYYY-MM-dd"
//               self.DateTxt.text = dateformatter.string(from: datePicker.date) //2-4
           }
//           self.DateTxt.resignFirstResponder() // 2-5
       }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            var contentInset:UIEdgeInsets = self.scrollV.contentInset
            contentInset.bottom = keyboardSize.size.height
            scrollV.contentInset = contentInset
            
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            scrollV.contentInset = contentInset
        }
        
    }
    
    @IBAction func backAct(_ sender: Any) {
           
           self.dismiss(animated: true, completion: nil)

       }
    @IBAction func maleaAndFameAction(_ sender: UIButton)
    {
        if sender.tag == 1{
            genderSelected = "male"
            malebtn.backgroundColor = UIColor(red:  42.0/255.0, green:  65.0/255.0, blue:  110.0/255.0, alpha: 1.0)
            femalebtn.backgroundColor = UIColor.white
            otherbtn.backgroundColor = UIColor.white

        }
        else if sender.tag == 2{
            
            genderSelected = "female"
            malebtn.backgroundColor = UIColor.white
            femalebtn.backgroundColor = UIColor(red:  42.0/255.0, green:  65.0/255.0, blue:  110.0/255.0, alpha: 1.0)
            otherbtn.backgroundColor = UIColor.white
                
        }else{
            
            genderSelected = "other"
            malebtn.backgroundColor = UIColor.white
            femalebtn.backgroundColor = UIColor.white
            otherbtn.backgroundColor = UIColor(red:  42.0/255.0, green:  65.0/255.0, blue:  110.0/255.0, alpha: 1.0)
        }
    }
    
    
    func showUploadImageOptions() {
        let alert = UIAlertController.init(title: "Upload Image", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction.init(title: "Select Picture", style: .default) { (action) in
            self.selectPictureFromLibraryMtd()
        }
        let action2 = UIAlertAction.init(title: "Capture Image", style: .default) { (action) in
            self.captureProfileImageMtd()
        }
        let action3 = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectPictureFromLibraryMtd() {
       let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
       imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
       imagePicker.allowsEditing = false
       self.present(imagePicker, animated:true){
           
       }
    }
    
    func captureProfileImageMtd() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.sourceType = UIImagePickerController.SourceType.camera
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated:true){
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
           if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
              pickedImage = image
               editImgView.image = image
           }
            picker.dismiss(animated: true) {
            
            }
       }

}

extension CorporateProfileViewController:UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
      
         return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 100 {
            return categoryList.count
        }
        
        return list.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 100{
            return categoryList[row]
        }
        return list[row]

    }

}

extension CorporateProfileViewController:UIPickerViewDelegate{
   
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 100
        {
            self.DateTxt.text = self.categoryList[row];
        }else{
            self.userTypeTxt.text = self.list[row]
        }
        
        
    }
    
}

extension CorporateProfileViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool { // return NO to disallow editing.
        print("textFieldShouldBeginEditing")
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) { // became first responder
        print("textFieldDidBeginEditing")
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        print("textFieldShouldEndEditing")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)  {// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("textFieldDidEndEditing")
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {// if implemented, called in place of textFieldDidEndEditing:
        print("textFieldDidEndEditing")
      
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {// return NO to not change text
        
        if textField == mobileNumTxt {
            
            let maxLength = 10
               let currentString: NSString = mobileNumTxt.text! as NSString
               let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
        
        }
        return true
        
      
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {// called when clear button pressed. return NO to ignore (no notifications)
        print("textFieldShouldClear")
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {// called when 'return' key pressed. return NO to ignore.
        print("textFieldShouldReturn")
        textField.resignFirstResponder()
        return true
    }
  

}
extension String {

 var numberValidation: Bool {
    
      let regex = try! NSRegularExpression(pattern: "^[0-9]\\d{9}$", options: .caseInsensitive)
         let valid = regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
         print("Mobile validation \(valid)")
           return valid
  }
}


extension UITextField {
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
}
