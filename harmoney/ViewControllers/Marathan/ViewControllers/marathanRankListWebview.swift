//
//  marathanRankListWebview.swift
//  FitKet
//
//  Created by Mac on 14/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit
import WebKit



class  marathanRankListWebview: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    var urlString : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        webView = WKWebView()
//        webView.navigationDelegate = self
//        view = webView
        let base = ConfigurationManager().configuration.baseURL
       
        let apistring = "/certificateDownload"
        
//        let url = URL(string: base+apistring)!
        let url = URL(string: urlString)!
        
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true

        }
        
    
    override func viewWillAppear(_ animated: Bool) {
              
              self.tabBarController?.tabBar.isHidden = true
          }
    
    @IBAction func CloseButton(_ sender: Any) {
        
         self.dismiss(animated: true, completion: nil)
    }


}


