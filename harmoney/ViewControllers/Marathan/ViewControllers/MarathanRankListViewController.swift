//
//  MarathanRankListViewController.swift
//  FitKet
//
//  Created by Mac on 02/08/20.
//  Copyright © 2020 Fitket. All rights reserved.
//

import UIKit

class  MarathanRankListViewController: UIViewController {
    
    var viewModel: leaderBoardModel!
    var accountManager: AccountManager!
//    var GUID_Model: FPL_GUID_Model!
    var guidDetailMArray = [Any]()

    @IBOutlet weak var rankListTableView: UITableView!
    @IBOutlet weak var ranklabel: UILabel!
    @IBOutlet weak var headerlabel: UILabel!
    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var myPostionLabel: UILabel!

    @IBOutlet var headerTimeLble: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    var selectedIndex = 0
    
    var lead: leaderboardData?
//    var accountManager: AccountManager!
    
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        rankListTableView.dataSource = self
        rankListTableView.delegate = self
        rankListTableView.register(UINib(nibName: "CustomTableViewHeaderCellTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomTableViewHeaderCellTableViewCell")
        
        var eventId = ""

        let object = self.guidDetailMArray[selectedIndex]
        if object is Corporate_Event_Array{
        let eventInfo = object as? Corporate_Event_Array
        eventId = eventInfo?.eventId ?? ""
        }
        viewModel.GetLeaderBoard(eventId: eventId) { [weak self](success, data, message, error) in
            guard success, error == nil else {
                self?.hideLoader()
                self?.handleError(error: error, message: message)
                return
            }
            
            self?.hideLoader()
            self?.lead = data
            
            if (self?.lead?.myPosition !=  nil){
                if self?.lead?.myPosition?.rank ?? 0 >= 1 {
                    self?.bottomViewHeightConstraint.constant = 90
                    self?.view.setNeedsLayout()
                }else if self?.lead?.myPosition?.rankSting != nil
                {
                    self?.bottomViewHeightConstraint.constant = 90
                    self?.view.setNeedsLayout()
                }else {
                    self?.bottomViewHeightConstraint.constant = 0
                    self?.myPostionLabel.text = ""
                }
                
                if self?.lead?.myPosition?.rank == nil
                {
                   self?.ranklabel.text = self?.lead?.myPosition?.rankSting ?? ""
                    
                }else{
                     self?.ranklabel.text = String(self?.lead?.myPosition?.rank ?? 0)
                    
 
                }
                self?.headerlabel.text = self?.lead?.myPosition?.strDisplayName
                if self!.lead!.isShowMinutes == false{
                    self?.headerTimeLble.isHidden = true
                    self?.timelabel.isHidden = true
                    self?.timelabel.text = self?.lead?.myPosition?.minutes
                    
                }else{
                    self?.headerTimeLble.isHidden = false
                    self?.timelabel.isHidden = false
                    self?.timelabel.text = self?.lead?.myPosition?.minutes
                }
                
            }
            else {
                self?.bottomViewHeightConstraint.constant = 0
                self?.myPostionLabel.text = ""
            }
            
            self?.rankListTableView.reloadData()
//            let marathanVC = self?.MarathanBoard.instantiateViewController(withIdentifier: "MarathanCongratulationViewController") as! MarathanCongratulationViewController
//            self?.present(marathanVC, animated: true)
        }
        
        

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
              
              self.tabBarController?.tabBar.isHidden = true
          }
    
    @IBAction func backAct(_ sender: Any) {
        
         self.dismiss(animated: true, completion: nil)
    }


}


extension MarathanRankListViewController : UITableViewDelegate, UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if lead == nil { return 0 }
        return lead?.overall?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewHeaderCellTableViewCell", for: indexPath) as! CustomTableViewHeaderCellTableViewCell
        if self.lead?.overall?[indexPath.row].rank  == nil
        {
             cell.ranklabel.text = self.lead?.overall?[indexPath.row].rankSting ?? ""
           
            
        }else{
            
           cell.ranklabel.text = String(self.lead?.overall?[indexPath.row].rank ?? 1)
            
        }

        
        cell.headerlabel.text = self.lead?.overall?[indexPath.row].strDisplayName ?? ""
        if self.lead!.isShowMinutes == false{
            
            cell.timelabel.isHidden = true
            cell.timelabel.text = self.lead?.overall?[indexPath.row].minutes ?? ""
            
        }else{
            cell.timelabel.isHidden = false
            cell.timelabel.text = self.lead?.overall?[indexPath.row].minutes ?? ""
            
        }
       
        return cell
    }
}
