//
//  CustomTableViewHeaderCellTableViewCell.swift
//  marathan
//
//  Created by vaish navi on 03/08/20.
//  Copyright © 2020 vaish navi. All rights reserved.
//

import UIKit

class CustomTableViewHeaderCellTableViewCell: UITableViewCell {

    @IBOutlet var ranklabel: UILabel!
    @IBOutlet var headerlabel: UILabel!
    
    @IBOutlet var timelabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
