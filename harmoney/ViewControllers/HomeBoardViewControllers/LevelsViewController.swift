//
//  LevelsViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 8/4/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
import UICircularProgressRing

var onlyOnce = 0

class LevelsViewController: UIViewController {
    @IBOutlet weak var circleProgressView: UICircularProgressRing!
    @IBOutlet weak var experianceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var levelLabel: UILabel!
    var isFromOnboarding = false
    
    let cellReuseIdentifier = "tableViewCell"
    var levelsModel: LevelsModel? {
        return LevelsManager.sharedInstance.levelsModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getLevels()
        configureUI()
        
//        scrolltoLevel()
    }
    
    func scrolltoLevel(){
        if let clev = HarmonySingleton.shared.dashBoardData.data?.points?.level, clev > 0{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.tableView.contentInset.bottom = 110 * 3
                self.tableView.scrollToRow(at: IndexPath.init(row: clev-1, section: 0), at: .top, animated: true)
            }
        }
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        if LevelsManager.sharedInstance.levelRewardClaimed {
            HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
            LevelsManager.sharedInstance.levelRewardClaimed = false
        }
    }
}

//MARK: Private Methods
extension LevelsViewController {
    func getLevels() {
        LevelsManager.sharedInstance.getLevels { (result) in
            switch result {
            case .success(_):
                if LevelsManager.sharedInstance.levelRewardClaimed {
                for i in 0..<(HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0){
                  // if((HarmonySingleton.shared.dashBoardData.data?.points?.level) != 1) {
                 self.claimLevelRewardsAPIFunction(levelInfo: self.levelsModel?.data.levelInfo[i])
                  //  }
                    let xp = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
                    HarmonySingleton.shared.dashBoardData.data?.points?.xp = xp
                }
                }
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    func claimLevelRewardsAPIFunction(levelInfo:LevelInfo?)  {
        if HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0 <= levelInfo?.level ?? 0 {
            LevelsManager.sharedInstance.claimLevelReward(levelInfo: levelInfo!) { (result) in
                switch result {
                case .success(_):
                    let xp = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
                    let xpAfterClaimLevelUp = xp   + (levelInfo?.levelXP ?? 0) //
                    let gem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
                    let gemAfterClaimLevelUp = gem + (levelInfo?.jem ?? 0)
                    HarmonySingleton.shared.dashBoardData.data?.points?.jem = gemAfterClaimLevelUp
                    HarmonySingleton.shared.dashBoardData.data?.points?.xp = xpAfterClaimLevelUp
                    self.tableView.reloadData()
                    self.scrolltoLevel()
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
            }
        }
    }
}

//MARK: UI Methods
extension LevelsViewController {
    func configureUI() {
        circleProgressView.style = .ontop
        let nextLevelXp = CGFloat(HarmonySingleton.shared.requiredNextLevelXp(userLevel: HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0))
            
        
        circleProgressView.value = CGFloat(nextLevelXp)
        circleProgressView.maxValue = CGFloat(HarmonySingleton.shared.calculateRequiredXP(userLevel: HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0))
        levelLabel.text = String(format: "%d", HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0)
        
        setupExperianceAttributedLabel()
        setupTableView()
    }
    
    func setupTableView() {
        tableView.register(UINib.init(nibName: "LevelsTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func setupExperianceAttributedLabel() {
        let experianceString = String(format: "%d", HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0)
        let requieredExperianceString = String(format: "%d", HarmonySingleton.shared.calculateRequiredXP(userLevel: HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0))
        
        let xpAttachment = NSTextAttachment()
        xpAttachment.image = UIImage(named: "xp")
        let boldFont = UIFont.futuraPTBoldFont(size: 16)
        
        let imageSize = xpAttachment.image!.size
        xpAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - imageSize.height) / 2, width: imageSize.width, height: imageSize.height)
        
        let xpImageString = NSAttributedString(attachment: xpAttachment)
        let attributedText = NSMutableAttributedString(string: experianceString + "/" + requieredExperianceString + "  ")
        attributedText.append(xpImageString)
        attributedText.append(NSAttributedString(string:"  to Next level"))
        
        let lightRange = (attributedText.string as NSString).range(of: attributedText.string)
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBookFont(size: 14)], range:lightRange)
        
        experianceLabel.attributedText = attributedText
    }
    
    func presentClaimLevelRewardViewController(levelInfo: LevelInfo) {
        let claimLevelRewardViewController = GlobalStoryBoard().claimLevelRewardVC
        claimLevelRewardViewController.levelInfo = levelInfo
        present(claimLevelRewardViewController, animated: true, completion: nil)
    }
}

//MARK: Action Methods
extension LevelsViewController {
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - UITableViewDelegate Methods
extension LevelsViewController: UITableViewDelegate, LevelsTableViewCellDelegate {
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let levelInfo = self.levelsModel?.data.levelInfo[indexPath.row]
//
//        if HarmonySingleton.shared.dashBoardData.data?.userType?.rawValue ?? "1" == RegistrationType.child.rawValue && levelInfo?.reward != nil {
//            self.view.toast("Reward not approved by parent.")
//        } else {
//            if levelInfo?.levelClaimStatus == false {
//                if HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0 <= levelInfo?.level ?? 0 {
//                    LevelsManager.sharedInstance.claimLevelReward(levelInfo: levelInfo!) { (result) in
//                        switch result {
//                        case .success(_):
//                            let xp = HarmonySingleton.shared.dashBoardData.data?.points?.xp ?? 0
//                            let xpAfterClaimLevelUp = xp + (levelInfo?.levelXP ?? 0)
//
//                            let gem = HarmonySingleton.shared.dashBoardData.data?.points?.jem ?? 0
//                            let gemAfterClaimLevelUp = gem + (levelInfo?.jem ?? 0)
//
//                            HarmonySingleton.shared.dashBoardData.data?.points?.jem = gemAfterClaimLevelUp
//                            HarmonySingleton.shared.dashBoardData.data?.points?.xp = xpAfterClaimLevelUp
//
//                            tableView.reloadData()
//                            self.scrolltoLevel()
//                            if self.isFromOnboarding == false{
//                                self.presentClaimLevelRewardViewController(levelInfo: levelInfo!)
//                            }
//                        case .failure(let error):
//                            self.view.toast(error.localizedDescription)
//                        }
//                    }
//                } else {
//                    self.view.toast("Level not reached yet.")
//                }
//            } else {
//               self.view.toast("Already claimed reward for this level.")
//            }
//        }
//    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
//            if indexPath == lastVisibleIndexPath {
//                if self.isFromOnboarding && onlyOnce == 0{
//                    tableView.selectRow(at: tableView.indexPathsForVisibleRows?.first, animated: false, scrollPosition: .none)
//                    tableView.delegate?.tableView!(tableView, didSelectRowAt:(tableView.indexPathsForVisibleRows?.first)!)
////                    self.isFromOnboarding = false
////                    self.dismiss(animated: true, completion: nil)
//                    onlyOnce = 1
//                }
//            }
//        }
//    }
    func didTapAddRewardButton(levelsTableViewCell: LevelsTableViewCell) {
        let levelInfo = self.levelsModel?.data.levelInfo[levelsTableViewCell.tag]
//        if levelsTableViewCell.levelReward?.count ?? 0 > 0 {
            if HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0 <= levelInfo?.level ?? 0 {
            LevelsManager.sharedInstance.addRewardForLevel(levelReward: levelsTableViewCell.levelReward ?? "", levelInfo: levelsTableViewCell.levelInfo!) { (result) in
                switch result {case .success(_):
                    self.view.toast("Reward added successfully!")
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
                }
                }
            }else{
              self.view.toast("Level not reached yet.")
            }
//        }
    }
    
    func didTapAddCouponButton(levelsTableViewCell: LevelsTableViewCell) {
        
    }
}

//MARK: - UITableViewDataSource Methods
extension LevelsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return levelsModel?.data.levelInfo.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! LevelsTableViewCell
        cell.tag = indexPath.row
        let levelInfo = self.levelsModel?.data.levelInfo[indexPath.row]
        cell.isUserInteractionEnabled = false
        cell.delegate = self
        cell.levelInfo = levelInfo
        
        return cell
    }
}
