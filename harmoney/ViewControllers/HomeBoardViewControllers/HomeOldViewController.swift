//
//  HomeViewController.swift
//  HarmoneyLiveApp
//
//  Created by sramika mangalapurapu on 5/12/20.
//  Copyright © 2020 sramika mangalapurapu. All rights reserved.
//

import UIKit
import Alamofire

class HomeOldViewController: UIViewController {
    
    @IBOutlet weak var challangesInfoLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var experianceLabel: UILabel!
    @IBOutlet weak var harmoneyBucksLabel: UILabel!
    @IBOutlet weak var gemLabel: UILabel!
    @IBOutlet weak var notificationsLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var spendLabel: UILabel!
    @IBOutlet weak var earnLabel: UILabel!
    @IBOutlet weak var myChallengeCustomView1: HomeChallengeCustomView!
    @IBOutlet weak var myChallengeCustomView2: HomeChallengeCustomView!
    @IBOutlet weak var myChallengeCustomView3: HomeChallengeCustomView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var mychallangesCV: UICollectionView!
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var KmsLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var parentView: UIView!
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    let healthService = HealthService()

    var homeModel: HomeModel? {
        return HomeManager.sharedInstance.homeModel
    }
    var sliderCurrentPosition : Float = 0
    let reuseIdentifier = "cell"
    
    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

       parentView.layer.cornerRadius = 7
       parentView.layer.masksToBounds = false
       parentView.layer.shadowColor =  UIColor.black.withAlphaComponent(0.7).cgColor
       parentView.layer.shadowOpacity = 0.3
       parentView.layer.shadowOffset = .zero
       parentView.layer.shadowRadius = 1.5
        
        UserDefaultConstants().login = true
        
        let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        NotificationsManager.sharedInstance.sendDeviceToken(token: UserDefaultConstants().pushdDeviceToken)
//        self.view.toast(UserDefaultConstants().pushdDeviceToken)
        HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
        self.callConfig()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                healthService.requestAccess { [weak self](success, error) in
                    guard error == nil, success else {
                        print("Some error occurend when fetching permission...")
                        return
                    }
        //            DispatchQueue.main.async {
        //                self?.pushToSeperateView()
        //            }
                }
        HarmonySingleton.shared.mainTabBar = self.tabBarController
        getHomeModel()
        updateActivityValues()
    }
    
    func callConfig()  {
        if Reachability.isConnectedToNetwork() {
        let (url, method, param) = APIHandler().getConfig(params: [:])
            AF.request(url, method: method, parameters: param).validate().responseJSON { response in
                    
                    switch response.result {
                       case .success(let value):
                        if let value = value as? [String:Any] {
                            let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                            let configModel = try? JSONDecoder().decode(ConfigModel.self, from: jsonData!)
                            HarmonySingleton.shared.cofigModel = configModel
                        }
                        break
                        case .failure(let error):
                        break
                       }
            }
        }
    }
    @IBAction func refreshBtnAction(_ sender: UIButton) {
            updateActivityValues()
        }
    func updateActivityValues()  {
            DispatchQueue.main.async {
                self.healthService.getCalories { (receivedCalories, error) in
                    DispatchQueue.main.async {
                let stringValue = String(format: "%.2f", receivedCalories ?? 0.0)
                self.caloriesLabel.text = stringValue
                    }
            }
                self.healthService.getTodaysStepsCollectionCumulitive { (stepCount, error) in
                    DispatchQueue.main.async {
                        let stringValue = String(format: "%.0f", stepCount ?? 0)
                      self.stepsLabel.text = stringValue
                    }
            }
                self.healthService.getDistance { (distance, error) in
                    DispatchQueue.main.async {

                        let stringValue = String(format: "%.2f", distance ?? 0.0)
                self.KmsLabel.text = stringValue
                    }
            }
                self.healthService.getCyclingDistance { (distance, error) in
                    DispatchQueue.main.async {

                        let stringValue = String(format: "%.2f", distance ?? 0.0)
                        print("cycling distance \(stringValue)")
              //  self.KmsLabel.text = stringValue
                    }
            }
         }
        }
}

//MARK: UI Methods
extension HomeOldViewController {
    func configureUI() {
        let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        mychallangesCV?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        challangesInfoLabel.adjustsFontSizeToFitWidth = true
        if(homeModel?.data.challenges.count ?? 0 > 0){
            challangesInfoLabel.isHidden = true
        }else{
            challangesInfoLabel.isHidden = false
        }
        self.collectionView.reloadData()
        self.mychallangesCV.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.setupExperianceAttributedLabel()
        }
        setupSlider()
        
        harmoneyBucksLabel.text = String(format: "$%d", homeModel?.data.points.hBucks ?? 0)
        
        gemLabel.text = String(format: "%d", homeModel?.data.points.gem ?? 0)
        
        if homeModel?.data.notificationCount ?? 0 > 0 {
            notificationsLabel.isHidden = false
            notificationsLabel.text = String(format: "%d", homeModel?.data.notificationCount ?? 0)
        } else {
           notificationsLabel.isHidden = true
        }
        
        userNameLabel.text = homeModel?.data.name
        levelLabel.text = String(format: "Level %d", homeModel?.data.points.level ?? 0)
        spendLabel.text = String(format: "$%d", homeModel?.data.points.spend ?? 0)
        earnLabel.text = String(format: "$%d", homeModel?.data.points.earn ?? 0)
        
        myChallengeCustomView1.clearView()
        myChallengeCustomView2.clearView()
        myChallengeCustomView3.clearView()
        
//        if homeModel?.data.challenges.count ?? 0 >= 1 {
//            myChallengeCustomView1.homeChallenge = homeModel?.data.challenges[0]
//        }
//        
//        if homeModel?.data.challenges.count ?? 0 >= 2 {
//            myChallengeCustomView2.homeChallenge = homeModel?.data.challenges[1]
//        }
//                
//        if homeModel?.data.challenges.count ?? 0 >= 3 {
//            myChallengeCustomView3.homeChallenge = homeModel?.data.challenges[2]
//        }
       
        if homeModel?.data.profilePic == "" {
            profileImageView.image = UIImage(named: "useravatar")
        } else {
            profileImageView.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
        }
    }
    
    func getHomeModel() {
        HomeManager.sharedInstance.getHomeModel { (result) in
            switch result {
            case .success(_):
                self.configureUI()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func setupSlider() {
        let userCurrentXp = homeModel?.data.points.xp ?? 0
        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: homeModel?.data.points.level ?? 0)
        
        sliderCurrentPosition = Float(userCurrentXp) / Float(totalXp)
        slider.isUserInteractionEnabled = false
        slider.setValue(self.sliderCurrentPosition, animated: true)
        slider.layoutIfNeeded()
    }
    
    func setupExperianceAttributedLabel() {
        let experianceString = String(format: "%d", homeModel?.data.points.xp ?? 0)
        
        let currentXP = experianceString
        let filArr = HarmonySingleton.shared.cofigModel?.data.gamification.filter({($0.Runs.min <= Int(currentXP) ?? 0) && ($0.Runs.max > Int(currentXP) ?? 0)})
        
        
        let requieredExperianceString = String(format: "%d", (filArr?.last?.Runs.max ?? 0) + 1)// String(format: "%d", HarmonySingleton.shared.calculateRequiredXP(userLevel: homeModel?.data.points.level ?? 0))
        
        let xpAttachment = NSTextAttachment()
        xpAttachment.image = UIImage(named: "xp")
        let boldFont = UIFont.futuraPTBoldFont(size: 16)
        
        let imageSize = xpAttachment.image!.size
        xpAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - imageSize.height) / 2, width: imageSize.width, height: imageSize.height)
        
        let xpImageString = NSAttributedString(attachment: xpAttachment)
        let attributedText = NSMutableAttributedString(string: experianceString + "/" + requieredExperianceString + "  ")
        attributedText.append(xpImageString)
        attributedText.append(NSAttributedString(string:"  to Next level"))
        
        let lightRange = (attributedText.string as NSString).range(of: attributedText.string)
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBookFont(size: 14)], range:lightRange)
        
        experianceLabel.attributedText = attributedText
    }
}

//MARK: Action Methods
extension HomeOldViewController {
    @IBAction func notificationButtonTapped(_ sender: UIButton) {
       
        let notificationViewController = GlobalStoryBoard().notificationsVC
        present(notificationViewController, animated: true, completion: nil)
    }
    
    @IBAction func levelButtonTapped(_ sender: UIButton) {
        let levelsViewController = GlobalStoryBoard().levelsVC
        present(levelsViewController, animated: true, completion: nil)
    }
    
    @IBAction func diamondButtonTapped(_ sender: UIButton) {
        let gemsViewController = GlobalStoryBoard().gemsVC
        present(gemsViewController, animated: true, completion: nil)
    }
}

//MARK: UICollectionViewDelegate Methods
extension HomeOldViewController: UICollectionViewDelegate {
    
}

//MARK: UICollectionViewDataSource Methods
extension HomeOldViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == mychallangesCV){
            return homeModel?.data.challenges.count ?? 0
        }else{
            return homeModel?.data.trending.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! HomeCollectionViewCell
        var trending : Trending?
        if(collectionView == mychallangesCV){
             trending = homeModel?.data.challenges[indexPath.row]
        }else{
             trending = homeModel?.data.trending[indexPath.row]
        }
        
        cell.trending = trending
        
        return cell
    }
}
