//
//  HomeViewController.swift
//  Harmoney
//
//  Created by Mac on 05/01/21.
//  Copyright © 2021 harmoney. All rights reserved.
//


import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

var isFromMyChallange = false
var myChallengeType = 0
var modeofChallenge = 0 // 0 - isnotChoosedbike , 1- device , 2 -watch , 3- swmming strokes(pool swimming ), 4- swmming distance(water swmming)
class HomeViewController: BaseViewController {
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var pageControlStack: UIStackView!
    @IBOutlet weak var challangesInfoLabel: UILabel!
//    @IBOutlet weak var slider: UISlider!
//    @IBOutlet weak var experianceLabel: UILabel!
//    @IBOutlet weak var harmoneyBucksLabel: UILabel!
//    @IBOutlet weak var gemLabel: UILabel!
//    @IBOutlet weak var notificationsLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
//    @IBOutlet weak var levelLabel: UILabel!
//    @IBOutlet weak var spendLabel: UILabel!
//    @IBOutlet weak var earnLabel: UILabel!
    @IBOutlet weak var myChallengeCustomView1: HomeChallengeCustomView!
    @IBOutlet weak var myChallengeCustomView2: HomeChallengeCustomView!
    @IBOutlet weak var myChallengeCustomView3: HomeChallengeCustomView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var mychallangesCV: UICollectionView!
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var KmsLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var parentView: UIView!
    var getInformationForPaymentObject : getInformationForPaymentObject?
    var  thumbsAlertViewController: ThumbsAlertViewController?
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    @IBOutlet var homeCollectionview: UICollectionView!

    @IBOutlet weak var navBar: UIView!
    
    lazy var spotlightView = AwesomeSpotlightView()

    let healthService = HealthService()

    var GUID_Model: FPL_GUID_Model!
    var bannerModel: BannerModel!

    var guidDetailMArray = [Any]()

    let dateformatter = DateFormatter()
    
    let dateFormat2 = DateFormatter()
    
    let dateFormat3 = DateFormatter()
    
    let dateFormat4 = DateFormatter()
    var isInHomePage = false
    var homeModel: HomeModel? {
        return HomeManager.sharedInstance.homeModel
    }
//    var sliderCurrentPosition : Float = 0
    let reuseIdentifier = "HomeCollectionViewCell"
    
    let slideInHandler = SooninSlideInHandler()

    var tabbarHeight:CGFloat = 0

    var profileObj : ProfileObject?
    var getWalletObj : getwalletObject?
    var addHarmoneyWalletValue : addHarmoneyWalletDataObject?
    @IBOutlet weak var stepCountButton: UIButton!
    
    @IBOutlet weak var milesCountButton: UIButton!
    
    @IBOutlet weak var caloriesCountButton: UIButton!
    
    var isPermisson : Bool = false
    var isPopupSHown : Bool = false
    var configdatas: ConfigModel?
    var walkthrough : Walkthrough?
    let headers: HTTPHeaders = [
        "Authorization": UserDefaultConstants().sessionToken ?? ""
       
    ]
    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageControl .bringSubviewToFront(pageControlStack)
        
        isPermisson = false
        slideInHandler.selectionDelegate = self
        dateformatter.dateFormat = "YYYY-MM-dd"
        dateFormat2.dateFormat = "MMMM"
        dateFormat3.dateFormat = "dd"
        dateFormat4.dateFormat = "YYYY"

       parentView.layer.cornerRadius = 7
       parentView.layer.masksToBounds = false
       parentView.layer.shadowColor =  UIColor.black.withAlphaComponent(0.7).cgColor
       parentView.layer.shadowOpacity = 0.3
       parentView.layer.shadowOffset = .zero
       parentView.layer.shadowRadius = 1.5
        
        UserDefaultConstants().login = true
        
//        let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
//        collectionView?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        
        homeCollectionview.register(UINib(nibName: homeDefaultCollectionViewCell.defaultid, bundle: .main), forCellWithReuseIdentifier: homeDefaultCollectionViewCell.defaultid)
        
        registerCells()
        mychallangesCV.register(UINib(nibName: "HomeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: reuseIdentifier)
        collectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: reuseIdentifier)
        
        
//        homeCollectionview.register(UINib(nibName: homeMarathanCollectionViewCell.marathanid, bundle: .main), forCellWithReuseIdentifier: homeMarathanCollectionViewCell.marathanid)

        NotificationsManager.sharedInstance.sendDeviceToken(token: UserDefaultConstants().pushdDeviceToken)
        HarmonySingleton.shared.navHeaderViewEarnBoard.getDashboardData()
//        self.callConfig()
//        self.getLeagueList()
//        self.getBannerListData()
//        getUserProfile()
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / witdh
        let roundedIndex = round(index)
        self.pageControl.currentPage = Int(roundedIndex)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {

        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    func registerCells(){
        homeCollectionview.register(CollCellHFLLeague.nib(), forCellWithReuseIdentifier: CollCellHFLLeague.identifier)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isInHomePage = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        isInHomePage = true
        HarmonySingleton.previousVC = .home
       // self.appDelegate?.scheduleNotification(notificationType: "Stay Possitive")
        let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        mychallangesCV?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        
        self.getDashBoardData()

        tabbarHeight = self.tabBarController?.view.frame.size.height ?? self.view.frame.size.height

                healthService.requestAccess { [weak self](success, error) in
                    guard error == nil, success else {
                        print("Some error occurend when fetching permission...")
                        return
                    }
                    self?.callConfig()

                    self?.isPermisson = true
        //            DispatchQueue.main.async {
        //                self?.pushToSeperateView()
        //            }
                }
        HarmonySingleton.shared.mainTabBar = self.tabBarController
        getUserProfile()
        getHomeModel()
        updateActivityValues()
        setupNavBar()
        self.updateSideMenu()
//        self.getBannerListData()
        getWallet()
    }
    
    func getWallet(){
        if Reachability.isConnectedToNetwork() {
       
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "walletPrivateKey")
            data.updateValue(walletPrivateKeyValue ?? "", forKey: "walletPrivateKey")
        var (url, method, param) = APIHandler().getWalletdata(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                   
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.getWalletObj = Mapper<getwalletObject>().map(JSON: value)
                            if let walletbalance = self.getWalletObj?.data?.sila_balance{
                                self.addHarmoneyToParent(amount: "\(walletbalance)")
                                
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
  
    }
    
    func addHarmoneyToParent(amount: String)
    {
        if Reachability.isConnectedToNetwork() {
       
        var data = [String : Any]()
        //data.updateValue(Defaults.getSessionKey, forKey: "guid")
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            let dateFormatter : DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = Date()
            let dateString = dateFormatter.string(from: date)
            data.updateValue(dateString , forKey: "txnDateTime")
            data.updateValue(amount , forKey: "amount")
        var (url, method, param) = APIHandler().addharmoneytoparent(params: data)
       
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                   
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.addHarmoneyWalletValue = Mapper<addHarmoneyWalletDataObject>().map(JSON: value)
                            HarmonySingleton.shared.addSilaToHarmoney = self.addHarmoneyWalletValue
                            HarmonySingleton.shared.navHeaderViewEarnBoard.updateHeaderViewWithNewData()
                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.setupSpotlight()

    }
    func setupNavBar(){
        HarmonySingleton.shared.navHeaderViewEarnBoard.frame = navBar.bounds
        navBar.addSubview(HarmonySingleton.shared.navHeaderViewEarnBoard)
    }
    
    @IBAction private func menuPressed(_ sender: Any) {
        
            slideInHandler.menuBackColor = UIColor(red:  236.0/255.0, green:  242.0/255.0, blue:  252.0/255.0, alpha: 1.0)
            slideInHandler.showSlideInMenu(menuSide: .Left)
//        self.showMenuPopup(sender)
        
    }
    @IBAction func profileButtonAction(_ sender: Any) {
        self.tabBarController?.selectedIndex = 3
    }
    
    func updateSideMenu()
    {
        slideInHandler.isReloadData = true
        slideInHandler.isCorporateUser = false
//        slideInHandler.profile = profile
        slideInHandler.collectionView.reloadData()
        
    }
    @IBAction func stepButtonAction(_ sender: Any) {
//        let vc = GlobalStoryBoard().localWebVC
//        vc.titletext = ""
//        vc.websiteUrl = "http://admin.harmoneydemo.com/webevent/#/profile/123"
//        vc.push = true
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func milesButtonAction(_ sender: Any) {
//        let vc = GlobalStoryBoard().localWebVC
//        vc.titletext = ""
//        vc.websiteUrl = "http://admin.harmoneydemo.com/webevent/#/profile/123"
//        vc.push = true
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func kalButtonAction(_ sender: Any) {
//        let vc = GlobalStoryBoard().localWebVC
//        vc.titletext = ""
//        vc.websiteUrl = "http://admin.harmoneydemo.com/webevent/#/profile/123"
//        vc.push = true
//        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    
    private func setupSpotlight() {
        
        if (self.configdatas?.data.walkthrough) != nil {

        var ShowFirstTimeBool = false
        if let ShowFirstTime = UserDefaults.standard.bool(forKey: "ShowFirstTime") as? Bool {
            ShowFirstTimeBool = ShowFirstTime
        }
        
        if isPermisson {
        
        if !ShowFirstTimeBool {
        UserDefaults.standard.set(true, forKey: "ShowFirstTime")
        UserDefaults.standard.synchronize()
            
            let levelFrame = (HarmonySingleton.shared.navHeaderViewEarnBoard.levelButton.superview?.convert(HarmonySingleton.shared.navHeaderViewEarnBoard.levelButton.frame, to: self.view))!
            let levelNewFrame = CGRect(x: levelFrame.minX + 10, y: levelFrame.minY+4, width: levelFrame.width - 10, height: levelFrame.height-7)
            
            let levelCountSpotlight = AwesomeSpotlight(withRect: levelNewFrame, shape: .roundRectangle, attributedText: convertText(headingStr: self.walkthrough?.levelCountSpotlight.headingStr ?? "", contentStr: self.walkthrough?.levelCountSpotlight.contentStr ?? ""))

            
            let xpFrame = (HarmonySingleton.shared.navHeaderViewEarnBoard.hbView.superview?.convert(HarmonySingleton.shared.navHeaderViewEarnBoard.hbView.frame, to: self.view))!
            let xpNewFrame = CGRect(x: xpFrame.minX - 4, y: xpFrame.minY+4, width: xpFrame.width - 20, height: xpFrame.height-7)
            
        let xbCountSpotlight = AwesomeSpotlight(withRect: xpNewFrame, shape: .roundRectangle, attributedText: convertText(headingStr: self.walkthrough?.xbCountSpotlight.headingStr ?? "", contentStr: self.walkthrough?.xbCountSpotlight.contentStr ?? ""))
            
           
        let harmoneyCountSpotlight = AwesomeSpotlight(withRect: (HarmonySingleton.shared.navHeaderViewEarnBoard.harmonyBucksContainerView.superview?.convert(HarmonySingleton.shared.navHeaderViewEarnBoard.harmonyBucksContainerView.frame, to: self.view))!, shape: .roundRectangle, attributedText: convertText(headingStr: self.walkthrough?.harmoneyCountSpotlight.headingStr ?? "", contentStr: self.walkthrough?.harmoneyCountSpotlight.contentStr ?? ""))

            let goalFrame = (HarmonySingleton.shared.navHeaderViewEarnBoard.soccerButton.superview?.convert(HarmonySingleton.shared.navHeaderViewEarnBoard.soccerButton.frame, to: self.view))!
            let newGoalFrame = CGRect(x: goalFrame.minX - 5, y: goalFrame.minY, width: goalFrame.width, height: goalFrame.height)
            
        let soccerCountSpotlight = AwesomeSpotlight(withRect: newGoalFrame, shape: .roundRectangle, attributedText: convertText(headingStr: self.walkthrough?.soccerCountSpotlight.headingStr ?? "", contentStr: self.walkthrough?.soccerCountSpotlight.contentStr ?? ""))

        
        let notificationCountSpotlight = AwesomeSpotlight(withRect: (HarmonySingleton.shared.navHeaderViewEarnBoard.notificationButton.superview?.convert(HarmonySingleton.shared.navHeaderViewEarnBoard.notificationButton.frame, to: self.view))!, shape: .roundRectangle, attributedText: convertText(headingStr: self.walkthrough?.notificationCountSpotlight.headingStr ?? "", contentStr: self.walkthrough?.notificationCountSpotlight.contentStr ?? ""))

        
        
        let stepCountSpotlight = AwesomeSpotlight(withRect: (stepCountButton.superview?.convert(stepCountButton.frame, to: self.view))!, shape: .roundRectangle, attributedText: convertText(headingStr: self.walkthrough?.stepCountSpotlight.headingStr ?? "", contentStr: self.walkthrough?.stepCountSpotlight.contentStr ?? ""))
        
        let milesCountSpotlight = AwesomeSpotlight(withRect: (milesCountButton.superview?.convert(milesCountButton.frame, to: self.view))!, shape: .roundRectangle, attributedText: convertText(headingStr: self.walkthrough?.milesCountSpotlight.headingStr ?? "", contentStr: self.walkthrough?.milesCountSpotlight.contentStr ?? ""))
        
        let caloriesCountSpotlight = AwesomeSpotlight(withRect: (caloriesCountButton.superview?.convert(caloriesCountButton.frame, to: self.view))!, shape: .roundRectangle, attributedText: convertText(headingStr: self.walkthrough?.caloriesCountSpotlight.headingStr ?? "", contentStr: self.walkthrough?.caloriesCountSpotlight.contentStr ?? ""))
        
        
        let bannerSpotlight = AwesomeSpotlight(withRect: (homeCollectionview.superview?.convert(homeCollectionview.frame, to: self.view))!, shape: .roundRectangle, attributedText: convertText(headingStr: self.walkthrough?.bannerSpotlight.headingStr ?? "", contentStr: self.walkthrough?.bannerSpotlight.contentStr ?? ""))
        
        var arySpotlights : [AwesomeSpotlight] = [levelCountSpotlight,xbCountSpotlight,harmoneyCountSpotlight,soccerCountSpotlight,notificationCountSpotlight,stepCountSpotlight,milesCountSpotlight,caloriesCountSpotlight,bannerSpotlight]

        
        if let vcs = tabBarController?.viewControllers {
       if vcs[0].tabBarItem != nil {
           let home = AwesomeSpotlight(withRect: (tabBarController?.view.convert(frameForTabAtIndex(index: 0), to: self.view))!, shape: .circle, attributedText: convertText(headingStr: self.walkthrough?.home.headingStr ?? "", contentStr: self.walkthrough?.home.contentStr ?? ""))
           arySpotlights.append(home)
       }
   }
        
        if let vcs = tabBarController?.viewControllers {
       if vcs[1].tabBarItem != nil {
           let home = AwesomeSpotlight(withRect: (tabBarController?.view.convert(frameForTabAtIndex(index: 1), to: self.view))!, shape: .circle, attributedText: convertText(headingStr: self.walkthrough?.earn.headingStr ?? "", contentStr: self.walkthrough?.earn.contentStr ?? ""))
           arySpotlights.append(home)
       }
   }

        
        if let vcs = tabBarController?.viewControllers {
       if vcs[2].tabBarItem != nil {
           let home = AwesomeSpotlight(withRect: (tabBarController?.view.convert(frameForTabAtIndex(index: 2), to: self.view))!, shape: .circle, attributedText: convertText(headingStr: self.walkthrough?.stuff.headingStr ?? "", contentStr: self.walkthrough?.stuff.contentStr ?? ""))
           arySpotlights.append(home)
       }
            
   }
        
            if let vcs = tabBarController?.viewControllers {
           if vcs[3].tabBarItem != nil {
               let home = AwesomeSpotlight(withRect: (tabBarController?.view.convert(frameForTabAtIndex(index: 3), to: self.view))!, shape: .circle, attributedText: convertText(headingStr: self.walkthrough?.profile.headingStr ?? "", contentStr: self.walkthrough?.profile.contentStr ?? ""))
               arySpotlights.append(home)
           }
                
       }
            
            if let vcs = tabBarController?.viewControllers {
           if vcs[4].tabBarItem != nil {
               let home = AwesomeSpotlight(withRect: (tabBarController?.view.convert(frameForTabAtIndex(index: 4), to: self.view))!, shape: .circle, attributedText: convertText(headingStr: self.walkthrough?.hbot.headingStr ?? "", contentStr: self.walkthrough?.hbot.contentStr ?? ""))
               arySpotlights.append(home)
           }
                
       }
        
//        if let vcs = tabBarController?.viewControllers {
//       if vcs[3].tabBarItem != nil {
//           let home = AwesomeSpotlight(withRect: (tabBarController?.view.convert(frameForTabAtIndex(index: 3), to: self.view))!, shape: .circle, attributedText: convertText(headingStr: self.walkthrough?.stuff.headingStr ?? "", contentStr: self.walkthrough?.stuff.contentStr ?? ""))
//           arySpotlights.append(home)
//       }
//   }

          spotlightView = AwesomeSpotlightView(frame: view.frame, spotlight: arySpotlights)
        
          spotlightView.cutoutRadius = 8
          spotlightView.delegate = self
          UIApplication.shared.keyWindow!.addSubview(spotlightView)
          spotlightView.continueButtonModel.isEnable = true
          spotlightView.skipButtonModel.isEnable = true
          spotlightView.showAllSpotlightsAtOnce = false
          spotlightView.start()
        }
        }
        }
    }
    private func frameForTabAtIndex(index: Int) -> CGRect {
        guard let tabBarSubviews = tabBarController?.tabBar.subviews else {
            return CGRect.zero
        }
        var allItems = [UIView]()
        for tabBarItem in tabBarSubviews {
            
            if tabBarItem.isKind(of: NSClassFromString("UITabBarButton")!) {
                allItems.append(tabBarItem)
            }

        }
        let item = allItems[index]
        let itemRect = CGRect(x: item.frame.origin.x , y: item.frame.origin.y - 10, width: item.frame.size.width, height: item.frame.size.height + 20)

        return item.superview!.convert(itemRect, to: view)
    }
    private func convertText(headingStr: String, contentStr: String) -> NSMutableAttributedString {
        
        let attributedText = NSMutableAttributedString(string: headingStr + "\n\n", attributes: [NSAttributedString.Key.font: UIFont.futuraPTMediumFont(size: 15)])
        
        attributedText.append(NSAttributedString(string: contentStr, attributes: [NSAttributedString.Key.font: UIFont.futuraPTMediumFont(size: 15)]))
        
        attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: attributedText.string.count))
        
        return attributedText
        
        
    }
    
    
    func getDashBoardData() {
        var data = [String : Any]()
        data.updateValue(UserDefaultConstants().sessionKey!, forKey: "session_token")
        if Reachability.isConnectedToNetwork(){
            var (url, method, param) = APIHandler().dashboard(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    if  let value = value as? [String:Any] {
                        HarmonySingleton.shared.dashBoardData = Mapper<DashBoardModel>().map(JSON: value)
                       
                       
                     
                        if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
                            
                        }else{
                            if HarmonySingleton.shared.dashBoardData.data?.points?.hbucks ?? 0 <= 10
                            {
                                if HarmonySingleton.shared.dashBoardData.data?.userType == .child {
                                    print("Am child")
                                }else{
                                   // self.getInformation()
                                    
                                }
                            }

                        }
                        
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }else{
            hideLoader()
            self.view.toast("Internet Connection not Available!")
        }
        
    }
    
    func getInformation(){
        
        let now = Date() 
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let nowString = formatter.string(from: now)

                if let lastTime = UserDefaults.standard.string(forKey: "savedDate"), lastTime == nowString {
                    // Already notified today, skip
                    print("same date - no action")
                    return
                }
                
                UserDefaults.standard.set(nowString, forKey: "savedDate")
            
            if Reachability.isConnectedToNetwork() {
                SVProgressHUD.show()
                var data = [String : Any]()
                //data.updateValue(Defaults.getSessionKey, forKey: "guid")
                data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
                var (url, method, param) = APIHandler().getInformationForPayment(params: data)
                print("get info\((url, method, param))")
                
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    SVProgressHUD.dismiss()
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.getInformationForPaymentObject = Mapper<getInformationForPaymentObject>().map(JSON: value)
                            print(self.getInformationForPaymentObject!)
                            if self.getInformationForPaymentObject?.data?.user == nil
                            {
//                                UserDefaults.standard.set("NoKey", forKey: "keyWallet")
//                                print("No account")
//                                self.presentThumbsAlertViewController(alertTitle: " No Wallet!!!\nCreate a wallet for you?",buttonTitle : "Later")
                            
                            }else{
                                if self.getInformationForPaymentObject?.data?.user?.walletPrivateKey == nil{
                                    UserDefaults.standard.set("NoKey", forKey: "keyWallet")
                                    print("No account")
                              //  self.presentThumbsAlertViewController(alertTitle: " No Wallet!!!\nCreate a wallet for you?",buttonTitle : "Later")
                                    
                                    
                                }else{
                                    UserDefaults.standard.set(self.getInformationForPaymentObject?.data?.user?.walletPrivateKey, forKey: "keyWallet")
                                    print("low balance")
                                   // self.showBalanceAlert()
                                    
                                }
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }

    
    func showBalanceAlert() {
        
       // presentThumbsAlertViewController(alertTitle: "Low Balance!!!\nTransfer funds to wallet?")
    }
    
    func presentThumbsAlertViewController(alertTitle: String, buttonTitle : String = "No") {
        thumbsAlertViewController = GlobalStoryBoard().thumbsAlertVC
        thumbsAlertViewController?.alertTitle = alertTitle
        thumbsAlertViewController?.buttonTitle = buttonTitle
        thumbsAlertViewController?.delegate = self
        self.present(thumbsAlertViewController!, animated: true, completion: nil)
    }

    
    func callConfig()  {
        if Reachability.isConnectedToNetwork() {
        let (url, method, param) = APIHandler().getConfig(params: [:])
            print(url, method, param)
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    
                    switch response.result {
                       case .success(let value):
                        if let value = value as? [String:Any] {
                            let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                            let configModel = try? JSONDecoder().decode(ConfigModel.self, from: jsonData!)
                            HarmonySingleton.shared.cofigModel = configModel
                            self.configdatas = configModel
                            self.walkthrough = self.configdatas?.data.walkthrough
                            var ShowFirstTimeBool = false
                            if let ShowFirstTime = UserDefaults.standard.bool(forKey: "ShowFirstTime") as? Bool {
                                ShowFirstTimeBool = ShowFirstTime
                            }
                            if !ShowFirstTimeBool {
                                self.setupSpotlight()
                            }
                        }
                        break
                        case .failure(let error):
                        print(error)
                        break
                       }
            }
        }
    }
    
    func getBannerListData()  {
        if Reachability.isConnectedToNetwork() {
            var data = [String : Any]()
            data.updateValue(UserDefaultConstants().guid ?? "", forKey: "guid")
            let (url, method, param) = APIHandler().getNewLeagueList(params: data)
            print("bannerdata\((url, method, param))")
            
            print("sessionToken:\(headers)")
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    print("banner:\(value)")
                    if let value = value as? [String:Any] {
                        let jsonData = try? JSONSerialization.data(withJSONObject: value, options: [])
                        print(jsonData)
                        do {
                            self.bannerModel = try JSONDecoder().decode(BannerModel.self, from: jsonData!)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                self.showHFLPopup()
                                //self.showRequestNotificationPage()
                            }
                            
                            
                            // here we go, `temp` is an array of main object of the json
                        } catch {
                            print(error)
                        }
                        
                        
                        //                        self.updateHealKitaDatas(isRefreshButtonClicked: false)
                        
                        self.homeCollectionview.reloadData()
                        
                        
                    }
                    break
                case .failure(let error):
                    print("Error \(error.localizedDescription)")
                    break
                }
            }
        }else{
            print("Not connected to internet")
        }
    }
    
    func showHFLPopup(){
     
        if !isInHomePage{
            return
        }
        
        if !isPermisson {
            return
        }
        if isPopupSHown{
            return
        }
        
        guard let joinedLeagueInfo = bannerModel.response.profile.leagueInfo.first(where: { (bannerInfo) -> Bool in
                return bannerInfo.currentLeague != nil
        }) else {
            return
        }
        
        let today = Date().getFilterDateFormat()
    
        var showPopUp = false
        if let lastShowDate = UserDefaultConstants.shared.lastHFLPopupDate {
            if UserDefaultConstants.shared.isDontShowPopup == false{
                showPopUp = true
            }else{
                if lastShowDate != today{
                    showPopUp = true
                    UserDefaultConstants.shared.isDontShowPopup = false
                    UserDefaultConstants.shared.lastHFLPopupDate = today
                }
            }
        }else{
            showPopUp = true
            UserDefaultConstants.shared.lastHFLPopupDate = today
        }
        
        if showPopUp{
            isPopupSHown = true
            hideLoader()
         //   AlertManager.shared.showHFLEarnPopup(controller: nil, img: joinedLeagueInfo.leagueLogo)
        }
    }
    
    func showRequestNotificationPage(){
            let requestNotificationVC = GlobalStoryBoard().requestNotificationVC
            requestNotificationVC.modalPresentationStyle = .overFullScreen
            requestNotificationVC.hidesBottomBarWhenPushed = true
            self.present(requestNotificationVC, animated: false, completion: nil)
    }
    
    func getLeagueList(){
        if Reachability.isConnectedToNetwork() {
         var data = [String : Any]()
            data.updateValue(UserDefaultConstants().guid ?? "", forKey: "guid")
        let (url, method, param) = APIHandler().getLeagueList(params: data)
            print("leaguelist\((url, method, param))")
        
            AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                    switch response.result {
                    case .success(let value):
                     if let value = value as? [String:Any] {
                         let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                        self.GUID_Model = try? JSONDecoder().decode(FPL_GUID_Model.self, from: jsonData!)

                        if self.GUID_Model.response?.profile?.eventInfo?.eventId != nil{

                            let eventInfo = self.GUID_Model.response?.profile?.eventInfo

                            let corporate_Event_Array = Corporate_Event_Array.init(eventStatus: eventInfo?.eventStatus, eventId: eventInfo?.eventId, eventName: eventInfo?.eventName, eventLogo: eventInfo?.eventLogo, eventSponsorLogo: eventInfo?.eventSponsorLogo, eventDesc: eventInfo?.eventDesc, eventRules: eventInfo?.eventRules, eventStateDate: eventInfo?.eventStateDate, eventStateTime: eventInfo?.eventStateTime, eventEndTime: eventInfo?.eventEndTime, selectedEventTypes: eventInfo?.selectedEventTypes, selectedEventTypesText: eventInfo?.selectedEventTypesText, eventCurrentStatus: eventInfo?.eventCurrentStatus, eventCompletedStatus: eventInfo?.eventCompletedStatus, eventTarget: eventInfo?.eventTarget, eventTargetCheckMeter: eventInfo?.eventTargetCheckMeter, eventTargetReached: eventInfo?.eventTargetReached, rank: eventInfo?.rank, rankInt: eventInfo?.rankInt, eventTypes: eventInfo?.eventTypes, isCorporateEvent: eventInfo?.isCorporateEvent, raceType: eventInfo?.raceType)


                            self.GUID_Model.response?.profile?.eventArray?.insert(corporate_Event_Array, at: 0)


                        }
                        self.guidDetailMArray .removeAll()

                        if self.GUID_Model.response?.profile?.leagueInfo != nil
                        {
//                            if self.modules?.fpl.isActive == true
//                            {
//
        //                        self.guidDetailMArray.append(self.GUID_Model.response?.profile?.leagueInfo! as Any)
                                 self.guidDetailMArray.append(contentsOf: ((self.GUID_Model.response?.profile?.leagueInfo!)!))
//                                FitketSingleton.sharedInstance.leagueCount = self.GUID_Model.response?.profile?.leagueInfo?.count

//                            }else{
//
//                                self.toast(message: self.modules?.fpl.disableText ?? "")
//                            }

                        }
        //                if self.GUID_Model.response?.profile?.leagueArray != nil
        //                {
        //                    self.guidDetailMArray.append(contentsOf: ((self.GUID_Model.response?.profile?.leagueArray!)!))
        //                }
                        if self.GUID_Model.response?.profile?.corporates != nil
                        {
                            self.guidDetailMArray.append(contentsOf: ((self.GUID_Model.response?.profile?.corporates!)!))
                        }

                        if self.GUID_Model.response?.profile?.eventArray != nil
                        {
                            self.guidDetailMArray.append(contentsOf: ((self.GUID_Model.response?.profile?.eventArray!)!))
                        }
                        if self.guidDetailMArray.count<=1{

//                            self.pageControl.numberOfPages = 0

                        }else{

//                            self.pageControl.numberOfPages = self.guidDetailMArray.count

                        }

//                        if self.guidDetailMArray != nil  {
//                            if (self.guidDetailMArray.count > 0) {
//                                self.homecollectionview_height_constraint.constant = 150
//                            }else {
//                                self.homecollectionview_height_constraint.constant = 0
//                            }
//                        } else {
//                            self.homecollectionview_height_constraint.constant = 0
//                        }

                        self.homeCollectionview.reloadData()
                     }
                     break
                     case .failure(let error):
                     break
                    }
            }
        }else{
        }
    }
    @IBAction func refreshBtnAction(_ sender: UIButton) {
        healthService.requestAccess { [weak self](success, error) in
            guard error == nil, success else {
                print("Some error occurend when fetching permission...")
                return
            }
            self?.callConfig()

            self?.isPermisson = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self?.showHFLPopup()
            }
            
//            DispatchQueue.main.async {
//                self?.pushToSeperateView()
//            }
        }

        self.restarthealthkit()
        //updateHealKitaDatas(isRefreshButtonClicked: true)
        }
    func updateActivityValues()  {
            DispatchQueue.main.async {
                self.healthService.getCalories { (receivedCalories, error) in
                    DispatchQueue.main.async {
                        // let stringValue = String(format: "%.2f", 1320.0)
                       let stringValue = String(format: "%.2f", receivedCalories ?? 0.0)
                        self.caloriesLabel.text = stringValue.formatedValue()
                    }
            }
                self.healthService.getTodaysStepsCollectionCumulitive { (stepCount, error) in
                    DispatchQueue.main.async {
                       // let stringValue = String(format: "%.0f", 1865.0)
                       let stringValue = String(format: "%.0f", stepCount ?? 0)
                      self.stepsLabel.text = stringValue.formatedValue()
                    }
                }
                self.healthService.getDistance { (distance, error) in
                    DispatchQueue.main.async {

                        let stringValue = String(format: "%.2f", distance ?? 0.0)
                self.KmsLabel.text = stringValue.formatedValue()
                       
                    }
            }
                self.healthService.getCyclingDistance { (distance, error) in
                    DispatchQueue.main.async {

                        let stringValue = String(format: "%.2f", distance ?? 0.0)
                        print("cycling distance \(stringValue)")
              //  self.KmsLabel.text = stringValue
                    }
            }
         }
        }
    
    func updateHealKitaDatas(isRefreshButtonClicked:Bool)  {
        
        let date = Date()
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd hh:mm:ss"
        formatter1.calendar = Calendar.current
        formatter1.timeZone = TimeZone.current
        let todayTime = formatter1 .string(from: date as Date)
        
            if Reachability.isConnectedToNetwork() {
            var data = [String : Any]()
                data.updateValue(UserDefaultConstants().sessionKey, forKey: "guid")
                data.updateValue(todayTime, forKey: "lastUpdatedDateTime")
                data.updateValue(self.stepsLabel.text?.numberFormat ?? "", forKey: "step")
               //data.updateValue("540", forKey: "step")
                data.updateValue(self.caloriesLabel.text?.numberFormat ?? "", forKey: "calories")
                data.updateValue(self.KmsLabel.text?.numberFormat ?? "", forKey: "kms")

            let (url, method, param) = APIHandler().updateHealthkitData(params: data)
                AF.request(url, method: method, parameters: param,headers: headers).validate().responseJSON { response in
                        switch response.result {
                        case .success(let value):
                            if  let value = value as? [String:Any] {
                                print(value)
                                if isRefreshButtonClicked {
                                    self.view.toast("Health kit data updated successfully")

                                }

                                self.getBannerListData()

                            }
                        case .failure(let error):
                            print(error)
                        }
                }
            }
    }
}
extension HomeViewController {
    func restarthealthkit() {
        SVProgressHUD.show()
        updateActivityValues()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.updateHealKitaDatas(isRefreshButtonClicked: false)
            SVProgressHUD.dismiss()

        }
    }
}
//MARK: UI Methods
extension HomeViewController {
    func configureUI() {
        self.updateHealKitaDatas(isRefreshButtonClicked: false)
        self.getBannerListData()
        challangesInfoLabel.adjustsFontSizeToFitWidth = true
        if(homeModel?.data.challenges.count ?? 0 > 0){
            challangesInfoLabel.isHidden = true
        }else{
            challangesInfoLabel.isHidden = true
        }
        self.collectionView.reloadData()
        self.mychallangesCV.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.setupExperianceAttributedLabel()
        }
        
        //Nelson

//        setupSlider()
        
//        harmoneyBucksLabel.text = String(format: "$%d", homeModel?.data.points.hBucks ?? 0)
//
//        gemLabel.text = String(format: "%d", homeModel?.data.points.gem ?? 0)
//
//        if homeModel?.data.notificationCount ?? 0 > 0 {
//            notificationsLabel.isHidden = false
//            notificationsLabel.text = String(format: "%d", homeModel?.data.notificationCount ?? 0)
//        } else {
//           notificationsLabel.isHidden = true
//        }
        
        //Nelson
        userNameLabel.text = homeModel?.data.name.capitalized
//        levelLabel.text = String(format: "Level %d", homeModel?.data.points.level ?? 0)
//        spendLabel.text = String(format: "$%d", homeModel?.data.points.spend ?? 0)
//        earnLabel.text = String(format: "$%d", homeModel?.data.points.earn ?? 0)
        
        myChallengeCustomView1.clearView()
        myChallengeCustomView2.clearView()
        myChallengeCustomView3.clearView()
        
//        if homeModel?.data.challenges.count ?? 0 >= 1 {
//            myChallengeCustomView1.homeChallenge = homeModel?.data.challenges[0]
//        }
//
//        if homeModel?.data.challenges.count ?? 0 >= 2 {
//            myChallengeCustomView2.homeChallenge = homeModel?.data.challenges[1]
//        }
//
//        if homeModel?.data.challenges.count ?? 0 >= 3 {
//            myChallengeCustomView3.homeChallenge = homeModel?.data.challenges[2]
//        }
       
        if homeModel?.data.profilePic == "" {
            self.setProfilePic(frstName: homeModel?.data.name, lstName: nil, profileImageView: profileImageView)
        } else {
            profileImageView.imageFromURL(urlString: homeModel?.data.profilePic ?? "")
        }
    }
    
    func setProfilePic(frstName: String?, lstName: String?, profileImageView: UIImageView){
    
        if let firstName = frstName, let lastName = lstName {
            profileImageView.setImageWith(firstName + " " + lastName, color: ConstantString.menuBackSystemProfilepic)
        } else {
            if let dispName = frstName {
                profileImageView.setImageWith(dispName, color: ConstantString.menuBackSystemProfilepic)
            }
        }
    }

    
    func getHomeModel() {
        HomeManager.sharedInstance.getHomeModel { (result) in
            switch result {
            case .success(_):
                self.configureUI()
            case .failure(let error):
                self.view.toast(error.localizedDescription)
            }
        }
    }
    
    func getUserProfile(){

        if Reachability.isConnectedToNetwork() {
            let header: HTTPHeaders = [
                "sessionToken": UserDefaultConstants().sessionToken ?? ""
            ]

            var data = [String : Any]()
            data.updateValue(UserDefaultConstants().sessionKey!, forKey: "guid")
            var (url, method, param) = APIHandler().getProfile(params: data)
            url = url + "/" + UserDefaultConstants().sessionKey!
            AF.request(url, method: method, parameters: param,headers: header).validate().responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        if  let value = value as? [String:Any] {
                            self.profileObj = Mapper<ProfileObject>().map(JSON: value)
                            self.slideInHandler.mobileNumber  = self.profileObj?.data?.last?.contact ?? ""
                            self.slideInHandler.isReloadData = true
                            self.slideInHandler.collectionView.reloadData()

                        }
                    case .failure(let error):
                        print(error)
                    }
            }
        }
    }
    
//    func setupSlider() {
//        let userCurrentXp = homeModel?.data.points.xp ?? 0
//        let totalXp = HarmonySingleton.shared.calculateRequiredXP(userLevel: homeModel?.data.points.level ?? 0)
//
//        sliderCurrentPosition = Float(userCurrentXp) / Float(totalXp)
//        slider.isUserInteractionEnabled = false
//        slider.setValue(self.sliderCurrentPosition, animated: true)
//        slider.layoutIfNeeded()
//    }
    
    func setupExperianceAttributedLabel() {
        let experianceString = String(format: "%d", homeModel?.data.points.xp ?? 0)
        
        let currentXP = experianceString
        let filArr = HarmonySingleton.shared.cofigModel?.data.gamification.filter({($0.Runs.min <= Int(currentXP) ?? 0) && ($0.Runs.max > Int(currentXP) ?? 0)})
        
        
        let requieredExperianceString = String(format: "%d", (filArr?.last?.Runs.max ?? 0) + 1)// String(format: "%d", HarmonySingleton.shared.calculateRequiredXP(userLevel: homeModel?.data.points.level ?? 0))
        
        let xpAttachment = NSTextAttachment()
        xpAttachment.image = UIImage(named: "xp")
        let boldFont = UIFont.futuraPTBoldFont(size: 16)
        
        let imageSize = xpAttachment.image!.size
        xpAttachment.bounds = CGRect(x: CGFloat(0), y: (boldFont.capHeight - imageSize.height) / 2, width: imageSize.width, height: imageSize.height)
        
        let xpImageString = NSAttributedString(attachment: xpAttachment)
        let attributedText = NSMutableAttributedString(string: experianceString + "/" + requieredExperianceString + "  ")
        attributedText.append(xpImageString)
        attributedText.append(NSAttributedString(string:"  to Next level"))
        
        let lightRange = (attributedText.string as NSString).range(of: attributedText.string)
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.futuraPTBookFont(size: 14)], range:lightRange)
        
        // Nelson

//        experianceLabel.attributedText = attributedText
        
        // Nelson
    }
    
    func timeConversion12(time24: String) -> String {
        let dateAsString = time24
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        
        let date = df.date(from: dateAsString)
        df.dateFormat = "hh:mma"
        
        let time12 = df.string(from: date!)
        print(time12)
        return time12
    }
}

//MARK: Action Methods
extension HomeViewController {
    @IBAction func notificationButtonTapped(_ sender: UIButton) {

        
        let notificationViewController = GlobalStoryBoard().notificationsVC
        present(notificationViewController, animated: true, completion: nil)
    }
    
    @IBAction func levelButtonTapped(_ sender: UIButton) {
        let levelsViewController = GlobalStoryBoard().levelsVC
        present(levelsViewController, animated: true, completion: nil)
    }
    
    @IBAction func diamondButtonTapped(_ sender: UIButton) {
        let gemsViewController = GlobalStoryBoard().gemsVC
        present(gemsViewController, animated: true, completion: nil)
    }
}

//MARK: UICollectionViewDelegate Methods
extension HomeViewController: UICollectionViewDelegate {
    
}

//MARK: UICollectionViewDataSource Methods
extension HomeViewController: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if  collectionView.tag == 100
        {
            if UserDefaultConstants.shared.isUserReached2000Steps == true{
                return CGSize(width:UIScreen.main.bounds.width , height: 520)
            }else{
                return CGSize(width:UIScreen.main.bounds.width , height: 580)
            }
        }else if (collectionView == mychallangesCV){
            return CGSize(width: 100, height: 100)
        }else{
            return CGSize(width: 100, height: 96)
        }
    
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if  collectionView.tag == 100
        {
            if bannerModel != nil {
                pageControl.numberOfPages = bannerModel.response.profile.leagueInfo.count
                    pageControl.isHidden = !(bannerModel.response.profile.leagueInfo.count > 1)
                return bannerModel.response.profile.leagueInfo.count
                
//               if(isCorporateUser == false && GUID_Model.response?.profile?.leagueInfo != nil)
//                {
//                    return (GUID_Model.response?.profile?.eventArray?.count ?? 0) + 1
//                }
//                return GUID_Model.response?.profile?.eventArray?.count ?? 0
            }
            
            return 0
            
        }else {
            if(collectionView == mychallangesCV){
                if homeModel?.data.challenges.count ?? 0 < 3  && homeModel?.data.challenges.count ?? 0 >= 0{
                    let count = homeModel?.data.challenges.count ?? 0
                    
                    if homeModel?.data.challenges != nil{
                        return count + 1

                    } else {
                        return homeModel?.data.challenges.count ?? 0
                    }

                } else {
                    return homeModel?.data.challenges.count ?? 0
                }
                
            }else{
                return homeModel?.data.trending.count ?? 0
            }
        }
       
    }
    
    @objc func onLeaderBoardClick (sender : UIButton){
        let hflDonationVC =  GlobalStoryBoard().hfl_LeaderBoardViewContoller
        hflDonationVC.leagueInfo = bannerModel.response.profile.leagueInfo[sender.tag]
        hflDonationVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(hflDonationVC, animated: true)
        
    }
    @objc func onJoinLeagueClick (sender : UIButton){
        
        showJoinPageBaesdOnLeague(index: sender.tag)
      
    }
    
    func showDashBoardPage(index : Int){
        let currentDate = Date().getDateStrFrom(format: DateFormatType.pickerDate)
        let todayDate = currentDate.getDateFromString()
        
        let leagueInfo = bannerModel.response.profile.leagueInfo[index]
        let eventDate = leagueInfo.leagueStartDate.getDateFromString()
        if eventDate > currentDate.getDateFromString(){
                //No action eventDate > Date()
                // pre event date
        }else if((leagueInfo.leagueStartDate <= Date().getFilterDateFormat()) && (leagueInfo.leagueEndDate >= Date().getFilterDateFormat())){
                //evenent date
                showHflDashboard(leagueInfo: leagueInfo)
             }else{
                //Post league date
            }
    }
    
    func showHflDashboard(leagueInfo : BannerLeagueInfo?){
        let hflDashBoard = GlobalStoryBoard().hflDashBoardVC
        hflDashBoard.leagueInfo = leagueInfo
        hflDashBoard.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(hflDashBoard, animated: true)
    }
    func showJoinPageBaesdOnLeague(index : Int){
        let leagueInfo = bannerModel.response.profile.leagueInfo[index]
        if leagueInfo.isLeagueVerify == 0{
            self.showClubListVC(leagueInfo: leagueInfo, arrAPI: [])
                    
        }else{
            let passVC =  GlobalStoryBoard().enterPassCodeVC
            passVC.deleagte = self
            passVC.bannerLeagueInfo = leagueInfo
            passVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(passVC, animated: true)
        }
    }
    
    func getJoinedLeagueInfo() -> BannerLeagueInfo?{
        return self.bannerModel.response.profile.leagueInfo.first { return $0.isJoint == true}
    }
    
    func showSuccess2000StepsPopup(){
        guard let leagueInfo = getJoinedLeagueInfo(), let teamName = leagueInfo.strSubTeamName?.lowercased() else{
            return
        }
        if UserDefaultConstants.shared.isUserReached2000Steps == true{
            return
        }
        let imageName = "HFL/congrats_2000_\(teamName)"
        AlertManager.shared.show2000SuccesAlertView(controller: self, imgName: imageName) {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        update2kStepCompletion(success: nil)
        UserDefaultConstants.shared.isUserReached2000Steps = true
    }
    func showSubTeamDirectly(leagueInfo : BannerLeagueInfo){
        let subTeamVC = GlobalStoryBoard().subTeamListViewController
        var arrClub = [TeamResult]()
        let teamResult = TeamResult(clubName: leagueInfo.strTeamName, clubLogo: leagueInfo.strTeamFlag, clubId: leagueInfo.strTeamID)
        arrClub.append(teamResult)
        subTeamVC.arrClubList = arrClub
        subTeamVC.leagueInfo = leagueInfo
        subTeamVC.currentIndex = 0
        subTeamVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(subTeamVC, animated: true)
    }
    
    @objc func onDonationClick (sender : UIButton){
        let hflDonationVC =  GlobalStoryBoard().hfl_DonationViewContoller
        hflDonationVC.leagueInfo = bannerModel.response.profile.leagueInfo[sender.tag]
        hflDonationVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(hflDonationVC, animated: true)
        
       // HFLDonationViewContoller
    }
    @objc func onCharityClick (sender : UIButton){
       showCharityScreen()
//        let privacypolicy = GlobalStoryBoard().rewardWebVC
//        privacypolicy.websiteUrl = "https://harmoney.ai/app-pages/bonobos/charity1.html"
//        privacypolicy.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(privacypolicy, animated: true)
    }
    @objc func onImgBtnClick (sender : UIButton){
       // showCharityScreen()
        
        // image click Webview
     /*
 let leagueInfo = bannerModel.response.profile.leagueInfo[sender.tag]
        if leagueInfo.isJoint == false {
            showJoinPageBaesdOnLeague(index: sender.tag)
            return
        }
        
        let currentDate = Date().getDateStrFrom(format: DateFormatType.pickerDate)
        let todayDate = currentDate.getDateFromString()
        
        let eventDate = leagueInfo.leagueStartDate.getDateFromString()
            if eventDate > todayDate{
                showCharityScreen()
            }else if((leagueInfo.leagueStartDate <= Date().getFilterDateFormat()) && (leagueInfo.leagueEndDate >= Date().getFilterDateFormat())){
                let hflDashBoard = GlobalStoryBoard().hflDashBoardVC
                hflDashBoard.leagueInfo = leagueInfo
                hflDashBoard.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(hflDashBoard, animated: true)
            }else{
               
            }
      */
      // HFLDonationViewContoller
    }
    
 
    
    @objc func imageTapped(sender: UITapGestureRecognizer) {
                if sender.state == .ended {
                    showNewBonobosPage()
                }
        }
    func showNewBonobosPage(){
                let privacypolicy = GlobalStoryBoard().rewardWebVC
                privacypolicy.websiteUrl = "https://www.bonobos.org/"
                privacypolicy.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(privacypolicy, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 100{
            let leagueInfo = bannerModel.response.profile.leagueInfo[indexPath.row]
          
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollCellHFLLeague.identifier, for: indexPath) as? CollCellHFLLeague else {
                return UICollectionViewCell()
            }
            cell.btnLeaderBoard.tag = indexPath.row
            cell.btnJoinNow.tag = indexPath.row
            cell.btnDonation.tag = indexPath.row
            cell.btnCharity.tag = indexPath.row
            cell.btnDonation.isHidden = true
            cell.btnLeaderBoard.addTarget(self, action: #selector(onLeaderBoardClick(sender:)), for: .touchUpInside)
            cell.btnJoinNow.addTarget(self, action: #selector(onJoinLeagueClick(sender:)), for: .touchUpInside)
            cell.btnDonation.addTarget(self, action: #selector(onDonationClick(sender:)), for: .touchUpInside)
            cell.btnCharity.addTarget(self, action: #selector(onCharityClick(sender:)), for: .touchUpInside)
            
            
           cell.btnImgVw.addTarget(self, action: #selector(onImgBtnClick(sender:)), for: .touchUpInside)
            cell.delegate = self
            cell.btnImgVw.tag = indexPath.row
            let tapGR = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(sender:)))
            cell.imgVWTopRight.addGestureRecognizer(tapGR)
            cell.imgVWTopRight.isUserInteractionEnabled = true
            
         // cell.updateCell(bannerLeagueInfo: leagueInfo, curentSteps: "2010")
            
            cell.updateCell(bannerLeagueInfo: leagueInfo, curentSteps: stepsLabel.text?.numberFormat)

            return cell
            /*
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeDefaultCollectionViewCell", for: indexPath) as! homeDefaultCollectionViewCell
            
            cell.baseView_WidthConstraint.constant = self.view.frame.size.width - 30
            cell.backgroundUrlImageview.set_image(leagueInfo.leagueBackgroundImage)//cldSetImage(imgString, cloudinary: cloud)
           // cell.backgroundUrlImageview.set_image(leagueInfo.leagueBackgroundImage )
//            cell.banner_league_logo_imageView.set_image(leagueInfo.leagueLogo )
//            cell.banner_sponcer_imageView.set_image(leagueInfo.leagueSponsorLogo )
//            cell.layerImg.set_image(leagueInfo.leagueRunningImageURL )
//            cell.organizedLbl.text = leagueInfo.organizedText
            
            if leagueInfo.isJoint == true {
                cell.manage_banner_viewsSetting(false, leagueInfos: leagueInfo)
            } else {
                if leagueInfo.isJoinNow == true{
                    cell.manage_banner_viewsSetting(true, leagueInfos: leagueInfo)


                } else {
                    cell.manage_banner_viewsSetting(false, leagueInfos: leagueInfo)

                }
            }
           
          
            return cell
 */
            
            
////                cell.banner_total_runs_label.text = "\(Int(leagueInfo?.intTotalRun ?? 0).withCommas())"
////                cell.banner_karma_coins_label.text = "\(Int(leagueInfo?.intTotalKarmaPoints ?? 0).withCommas())"
//
//                cell.banner_total_runs_label.text = leagueInfo?.intTotalRun
//                cell.banner_karma_coins_label.text = leagueInfo?.intTotalKarmaPoints
//
//                //Nelson
////                cell.banner_team_logo_imageView.set_image(leagueInfo?.strTeamFlag ?? "")
//                cell.bottomStatusLabel.text  = leagueInfo?.leagueStartInfo ?? ""
//                cell.organizedLbl.text = leagueInfo?.organizedText ?? ""
//
//                cell.baseView_WidthConstraint.constant = (collectionView.frame.size.width * 0.9) - 10
//                cell.baseView_heightConstraint.constant = 140
//                OperationQueue.main.addOperation {
//                    //Nelson
//
////                    cell.banner_sponcer_imageView.set_image(leagueInfo?.leagueSponsorLogo ?? "")
////                    cell.banner_league_logo_imageView.set_image(leagueInfo?.leagueLogo ?? "")
////                    cell.layerImg.set_image(leagueInfo?.leagueRunningImageUrl ?? "")
//                    //Nelson
//
//                }
//
//                if leagueInfo?.isJoint == true{
//                    cell.manage_banner_views(true)
//                }else{
//                    cell.manage_banner_views(false)
//                    if leagueInfo?.isJoinNow == false{
//                        cell.joinNowBtn.isHidden = true
//                        //Nelson
////                        FitketSingleton.sharedInstance.isjoinBtnHidden = true
//                    }else{
//                        cell.joinNowBtn.isHidden = false
//                        //Nelson
////                        FitketSingleton.sharedInstance.isjoinBtnHidden = false
//
//                    }
//                }
//
//
//            }else if object is Corporates_list{
//                var corporatesInfo = object as? Corporates_list
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeDefaultCollectionViewCell", for: indexPath) as! homeDefaultCollectionViewCell
//                cell.baseView_WidthConstraint.constant = (collectionView.frame.size.width * 0.9) - 10
//                cell.baseView_heightConstraint.constant = 140
//                cell.bottomStatusLabel.text  = corporatesInfo?.leagueStartInfo ?? ""
//                cell.organizedLbl.text = corporatesInfo?.organizedText ?? ""
//                OperationQueue.main.addOperation {
//
//                    //Nelson
//
////                    cell.banner_sponcer_imageView.set_image(corporatesInfo?.leagueSponsorLogo ?? "")
////                    cell.banner_league_logo_imageView.set_image(corporatesInfo?.leagueLogo ?? "")
//
////                    cell.layerImg.set_image(corporatesInfo?.leagueRunningImageUrl ?? "")
//                    //Nelson
//
//                }
//                cell.manage_banner_views_forCorporates(false)
//                if corporatesInfo?.isJoinNow == false{
//                    cell.joinNowBtn.isHidden = true
//                    //Nelson
//
////                    FitketSingleton.sharedInstance.isjoinBtnHidden = true
//                }else{
//                    cell.joinNowBtn.isHidden = false
//                    //Nelson
//
////                    FitketSingleton.sharedInstance.isjoinBtnHidden = false
//
//                }
//                return cell
//
//
//            }else if object is Corporate_Event_Array{
//                var eventInfo = object as? Corporate_Event_Array
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeMarathanCollectionViewCell", for: indexPath) as! homeMarathanCollectionViewCell
//                cell.distanceView.isHidden = true
//                cell.distanceParentView.isHidden = true
//                cell.baseView_WidthConstraint.constant = (collectionView.frame.size.width * 0.9) - 10
//                cell.baseView_heightConstraint.constant = 140
//                cell.join_now_btn.tag = indexPath.item
//                cell.downloadPdf_btn.tag = indexPath.item
//
//                //Nelson
//
////                cell.join_now_btn.addTarget(self, action: #selector(self.joinNowBtnAction(_:)), for: .touchUpInside)
//
////                cell.viewDetail_btn.tag = indexPath.item
////                cell.viewDetail_btn.addTarget(self, action: #selector(self.viewDetailBtnAction(_:)), for: .touchUpInside)
//
//                //Nelson
//
//                var futureEvent = false
//                let dateString = eventInfo?.eventStateDate ?? ""
//                let order = Calendar.current.compare(Date(), to: dateformatter.date(from: dateString) ?? Date(), toGranularity: .day)
//                let starTime = eventInfo?.eventStateTime ?? ""
//                let endTime = eventInfo?.eventEndTime ?? ""
//
//                let formatter = DateFormatter()
//                formatter.dateFormat = "HH:mm:ss"
//
//                let currentTimeStr = formatter.string(from: Date())
//                let eventStartTime = formatter.date(from: starTime)
//                let eventEndTime = formatter.date(from: endTime)
//                let currentTime = formatter.date(from: currentTimeStr)
//
//                var futureTime  = false
//                var expired  = false
//
//                if order == .orderedAscending{
//                    futureEvent = true
//                }
//                else if order == .orderedSame{
//                    if eventStartTime!.compare(currentTime!) == .orderedDescending {
//                        futureTime  = true
//                    }else if eventEndTime!.compare(currentTime!) == .orderedAscending{
//                        expired = true
//                    }
//                }
//                else if order == .orderedDescending{
//                        expired = true
//                }
//
//                cell.join_now_btn.setBackgroundImage(UIImage.init(named: "btn-bgOff"), for: .normal)
//
//                if expired{
//                    cell.join_now_btn.backgroundColor = UIColor(red: 203/255, green: 206/255, blue: 210/255, alpha: 1.0)
//                    cell.join_now_btn.setTitle("Expired", for: .normal)
//                    cell.join_now_btn.setBackgroundImage(UIImage.init(named: ""), for: .normal)
//
//                }else {
//                    cell.join_now_btn.setTitle("Join Now", for: .normal)
//                }
//
//                let stringMonth = dateFormat2.string(from:  dateformatter.date(from: dateString) ?? Date())
//                cell.monthLabel.text = String(stringMonth.prefix(3)).uppercased()
//
//                let stringYear = dateFormat4.string(from:  dateformatter.date(from: dateString) ?? Date())
//                cell.YearLabel.text = stringYear
//
//                let stringDay = dateFormat3.string(from:  dateformatter.date(from: dateString) ?? Date())
//                cell.DayLabel.text = stringDay
//
//                cell.TimeLabel.text = self.timeConversion12(time24: eventInfo?.eventStateTime ?? "")
//
//                if GUID_Model.response?.profile?.eventInfo?.raceType == "Steps"{
//                    cell.walkLabel.text = "Steps"
//                }else {
//                    cell.walkLabel.text = "Walk or Run"
//                }
//                cell.distanceLabel.text =  "0.0" + " / " + (GUID_Model.response?.profile?.eventInfo?.selectedEventTypesText ?? "1000")
//                var index = 0
//                for object in self.guidDetailMArray{
//                    if object is Corporate_Event_Array{
//                        break
//                    }
//                    index += 1
//                }
//
//                if(GUID_Model.response?.profile?.eventInfo?.eventId != nil && indexPath.item == index){
//                    cell.join_now_btn.backgroundColor = .clear
//                    cell.join_now_btn.setBackgroundImage(UIImage.init(named: "btn-bgOff"), for: .normal)
//
//                    var joinButtonStatus = 1
//                    if GUID_Model.response?.profile?.eventInfo?.eventCurrentStatus == 1{
//                        joinButtonStatus = 2
//                    }
//                    if GUID_Model.response?.profile?.eventInfo?.eventCurrentStatus == 2{
//                        joinButtonStatus = 3
//                    }
//                    if joinButtonStatus == 1{
//                        if futureEvent  {
//                            cell.rankView.isHidden = true
//                            cell.join_now_btn.setTitle("Get Ready", for: .normal)
//
//                        }else {
//                            if futureTime == false{
//                                cell.join_now_btn.setTitle("Start", for: .normal)
//                                cell.distanceView.isHidden = false
//                                cell.distanceParentView.isHidden = false
//                            }else{
//
//                                cell.rankView.isHidden = true
//                                cell.join_now_btn.setTitle("Get Ready", for: .normal)
//                            }
//                        }
//                    }
//                    else if joinButtonStatus == 2{
//                        cell.join_now_btn.setTitle("Finish", for: .normal)
//                        cell.distanceView.isHidden = false
//                        cell.distanceParentView.isHidden = false
//
//                        //Nelson
//
////                        if self.globalDashboardResponse != nil {
////                            func meterFrom(steps count: CGFloat) -> CGFloat {
////                                return count*0.76
////                            }
////                            func percentageFor(current: CGFloat, total: CGFloat) -> CGFloat {
////                                return (current/total)*100
////                            }
////                            func kmFrom(meter: CGFloat) -> CGFloat {
////                                return (meter/1000)
////                            }
////                            let meterFromSteps = meterFrom(steps: CGFloat( UserDefaults.standard.integer(forKey: "NumberOfSteps")))
////
////                            cell.ditanceProgressView.value = percentageFor(current: meterFromSteps, total: CGFloat(GUID_Model.response?.profile?.eventInfo?.eventTargetCheckMeter ?? 1000))
////
////                            if (Double(UserDefaults.standard.integer(forKey: "NumberOfSteps")) * Double(0.76)) >= Double(GUID_Model.response?.profile?.eventInfo?.eventTargetCheckMeter ?? 1000) {
////                                cell.join_now_btn.setTitle(" Done ", for: .normal)
////                                cell.distanceLabel.text =  (GUID_Model.response?.profile?.eventInfo?.selectedEventTypesText ?? "1000") + " / " + (GUID_Model.response?.profile?.eventInfo?.selectedEventTypesText ?? "1000")
////
////                            }else {
////                                cell.distanceLabel.text =  String(format: "%.2f", kmFrom(meter: meterFromSteps)) + " / " + (GUID_Model.response?.profile?.eventInfo?.selectedEventTypesText ?? "1000")
////                            }
////                        }
//
//                        //Nelson
//                    }
//                    else if joinButtonStatus == 3{
//
//                        cell.rankLabel.isHidden = false
//                        if eventInfo?.rank == nil
//                        {
//                          cell.rankLabel.text = "NA"
//
//                        }else{
//
//                           cell.rankLabel.text = "\(eventInfo?.rank ?? 0)"
//
//
//                        }
//
//                        cell.rankView.isHidden = false
//                        cell.join_now_btn.setTitle("View Details", for: .normal)
//
//                        cell.downloadandDetailV.isHidden = false
//                        cell.join_now_btn.isHidden = true
//                        //Nelson
////                        cell.downloadPdf_btn.addTarget(self, action: #selector(self.downloadCertificationAction(_:)), for: .touchUpInside)
//
//                    }
//                }
//                let imgPath = eventInfo?.eventLogo ?? ""
////                Nelson
////                cell.eventLogoImageview.set_image(imgPath)
////                let imgPathSec = eventInfo?.eventSponsorLogo ?? ""
////                cell.eventSponsorLogoImageview.set_image(imgPathSec)
//
//                return cell
//
//            }else{
//
//                return UICollectionViewCell()
//            }
//
////            let pages = floor(homeCollectionview.contentSize.width/homeCollectionview.frame.size.width);
//
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! HomeCollectionViewCell
        var trending : Trending?
        if(collectionView == mychallangesCV){
            
            let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)

                if homeModel?.data.challenges.count ?? 0 < 3  && homeModel?.data.challenges.count ?? 0 >= 0{
                    if (indexPath.row == lastRowIndex - 1) {
                        if homeModel?.data.challenges != nil {
                            trending = nil

                        } else {
                            trending = homeModel?.data.challenges[indexPath.row]

                        }
                    } else {
                    trending = homeModel?.data.challenges[indexPath.row]
                    }
                } else {
                    trending = homeModel?.data.challenges[indexPath.row]
                    
                }
        }else{
             trending = homeModel?.data.trending[indexPath.row]
        }
        
        cell.trending = trending
        
        return cell
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 100{
            let leagueInfo = bannerModel.response.profile.leagueInfo[indexPath.row]
            var isShowLoginPage = false
            if leagueInfo.isJoint == true {
                isShowLoginPage = false
            } else {
                isShowLoginPage = true
               if leagueInfo.isLeagueVerify == 1{
                    isShowLoginPage = true
                } else {
                    isShowLoginPage = false
                }
            }
    
            if isShowLoginPage{
                self.showJoinPageBaesdOnLeague(index: indexPath.row)
            }else{
              self.showDashBoardPage(index: indexPath.row)
                
                /*
                let teamVc = GlobalStoryBoard().clubTeamListVC
                teamVc.leagueInfo = leagueInfo
                teamVc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(teamVc, animated: true)
                */
            }
      
           
            
            /*
        let leagueInfo = bannerModel.response.profile.leagueInfo[indexPath.row]

//        let vc = GlobalStoryBoard().localWebVC
//        vc.titletext = "HFL"
//        vc.websiteUrl = leagueInfo.weburl
//        vc.push = true
//        self.navigationController?.pushViewController(vc, animated: true)
//
            
            let vc = GlobalStoryBoard().localWebVC
            vc.titletext = "HFL"
            vc.websiteUrl = leagueInfo.weburl
//            vc.websiteUrl = "http://inqdemo.com/harmoneyadmin/webevent/#/league/5fef52850091ca279acc3e7a/bel8ri4l3f"
            vc.push = false
            self.present(vc, animated: true, completion: nil)
            
            */
            
        } else if (collectionView == mychallangesCV){
            
            let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)
           
                if homeModel?.data.challenges.count ?? 0 < 3  && homeModel?.data.challenges.count ?? 0 >= 0{
                    if (indexPath.row == lastRowIndex - 1) {
                        
                        myChallengeType = 1

                    } else {
                        let trending = homeModel?.data.challenges[indexPath.row]
                        myChallengeType = trending?.type ?? 0
                    }
                } else {
                    let trending = homeModel?.data.challenges[indexPath.row]
                    myChallengeType = trending?.type ?? 0
                }
            self.tabBarController?.selectedIndex = 1
            isFromMyChallange = true
        } else {
            self.tabBarController?.selectedIndex = 1
            isFromMyChallange = true
        }
    }


}
extension HomeViewController: SelectionDelegate {
    
    
    func didTapSelect(menuSelection: String,value: CGFloat) {
        if value == 0{
            
            UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .transitionFlipFromLeft, animations: {
                
                self.tabBarController?.view.frame = CGRect(x: value, y: 0, width: self.tabBarController?.view.frame.size.width ?? self.view.frame.size.width, height: self.tabbarHeight)
                
            }, completion: {finished in
                
            })
        }
        else  {
            
            UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .transitionFlipFromRight, animations: {
                
                self.tabBarController?.view.frame = CGRect(x: value, y: 50, width: self.tabBarController?.view.frame.size.width ?? self.view.frame.size.width, height: self.tabbarHeight - 100)
                
            }, completion: {finished in
                
            })
        }
        //       navigationDelegate.openSettingsLink(linkString: settingDetails?.profilemenu.link ?? "")
        
        
        let vc = GlobalStoryBoard().localWebVC
        vc.push = true

        if menuSelection == "Profile"{
            self.tabBarController?.selectedIndex = 3
        }else if menuSelection == HarmonySingleton.shared.dashBoardData.data?.profileMenu?.about?.menuName{
            vc.titletext = HarmonySingleton.shared.dashBoardData.data?.profileMenu?.about?.menuName
            vc.websiteUrl = HarmonySingleton.shared.dashBoardData.data?.profileMenu?.about?.link
            self.navigationController?.pushViewController(vc, animated: true)
        }else if menuSelection == "Fitket for Corporate"{
        }else if menuSelection == "FAQ"{
        }else if menuSelection == "Support"{
        }else if menuSelection == "Tour"{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.setupSpotlight()
            }
        }else if menuSelection == HarmonySingleton.shared.dashBoardData.data?.profileMenu?.charity?.menuName{
            
            /* Image View
             self.showCharityScreen()
            Image View*/
        // WebView
            vc.titletext =  HarmonySingleton.shared.dashBoardData.data?.profileMenu?.charity?.menuName
            vc.websiteUrl =  "https://harmoney.ai/charity/lagoon.html"
           // HarmonySingleton.shared.dashBoardData.data?.profileMenu?.charity?.link
            self.navigationController?.pushViewController(vc, animated: true)
          //  WebView

        }else if menuSelection == HarmonySingleton.shared.dashBoardData.data?.profileMenu?.privacyPolicy?.menuName{
            vc.titletext =  HarmonySingleton.shared.dashBoardData.data?.profileMenu?.privacyPolicy?.menuName
            vc.websiteUrl =  HarmonySingleton.shared.dashBoardData.data?.profileMenu?.privacyPolicy?.link
            self.navigationController?.pushViewController(vc, animated: true)

        }else if menuSelection == HarmonySingleton.shared.dashBoardData.data?.profileMenu?.termsOfUse?.menuName{
            vc.titletext = HarmonySingleton.shared.dashBoardData.data?.profileMenu?.termsOfUse?.menuName
            vc.websiteUrl = HarmonySingleton.shared.dashBoardData.data?.profileMenu?.termsOfUse?.link
            self.navigationController?.pushViewController(vc, animated: true)

        }else if menuSelection == "corporate Login"{
        }else if menuSelection == "LogOutBtnAction"{
            DispatchQueue.main.async {
                
                
                let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to logout?", preferredStyle: .alert)

                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in

                      UserDefaultConstants().clearDefaults()
                    PersonalFitnessManager.sharedInstance.personalChallengeList.removeAll()
                      UserDefaults.standard.set(false, forKey: "login")
                    UserDefaults.standard.removeObject(forKey: ConstantString.GUID)
                    DailyChallengeManager.sharedInstance.onboardingChallenges.removeAll()
                    DailyChallengeManager.sharedInstance.dailyChallenges.removeAll()
                    HarmonySingleton.shared.dashBoardData = nil
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    appdelegate.initialView()
                    }))
               // refreshAlert.addAction(OKAction)
                self.present(refreshAlert, animated: true, completion: nil)

                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                //          print("Handle Cancel Logic here")
                }))
            }
            
        }else if menuSelection == "corporateLogOutBtnAction"{
            
//            DispatchQueue.main.async {
//
//
//                let refreshAlert = UIAlertController(title: "", message: "Are you sure want to logout", preferredStyle: .alert)
//
//                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
//
//                      UserDefaultConstants().clearDefaults()
//                    PersonalFitnessManager.sharedInstance.personalChallengeList.removeAll()
//                      UserDefaults.standard.set(false, forKey: "login")
//                    DailyChallengeManager.sharedInstance.onboardingChallenges.removeAll()
//                    DailyChallengeManager.sharedInstance.dailyChallenges.removeAll()
//                    HarmonySingleton.shared.dashBoardData = nil
//                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                    appdelegate.initialView()
//                    }))
//               // refreshAlert.addAction(OKAction)
//                self.present(refreshAlert, animated: true, completion: nil)
//
//                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
//                //          print("Handle Cancel Logic here")
//                }))
//            }
        }
    }
    
}
extension HomeViewController : AwesomeSpotlightViewDelegate {
    
    func spotlightView(_ spotlightView: AwesomeSpotlightView, willNavigateToIndex index: Int) {
        //print("spotlightView willNavigateToIndex index = \(index)")
    }
    
    func spotlightView(_ spotlightView: AwesomeSpotlightView, didNavigateToIndex index: Int) {
        //print("spotlightView didNavigateToIndex index = \(index)")
    }
    
    func spotlightViewWillCleanup(_ spotlightView: AwesomeSpotlightView, atIndex index: Int) {
        //print("spotlightViewWillCleanup atIndex = \(index)")
    }
    
    func spotlightViewDidCleanup(_ spotlightView: AwesomeSpotlightView) {
        //print("spotlightViewDidCleanup")
        if UserDefaults.standard.object(forKey: "isWalkthroughShown") as? Bool == nil {
//            self.showPullDownToSync(onCompletion: {
//                self.showDashboardView()
//            })
            UserDefaults.standard.set(true, forKey: "isWalkthroughShown")
        }
    }
}
extension HomeViewController: ThumbsAlertViewControllerDelegate {
    func didTapThumbsUpButton(thumbsAlertViewController: ThumbsAlertViewController) {
        thumbsAlertViewController.dismiss(animated: true, completion: nil)
        let walletPrivateKeyValue = UserDefaults.standard.value(forKey: "keyWallet")
        if walletPrivateKeyValue as! String == "NoKey"
       {        self.tabBarController?.selectedIndex = 3
        }else{
            if ((self.getInformationForPaymentObject?.data?.user?.processStage == 1)  ||   (self.getInformationForPaymentObject?.data?.user == nil) || (self.getInformationForPaymentObject?.data?.user?.walletPrivateKey == nil)) {
              
                    let couponDetails = GlobalStoryBoard().myFriendsVC
                    couponDetails.showView = "INFO"
                    self.navigationController?.pushViewController(couponDetails, animated: false)
                
            }else if (self.getInformationForPaymentObject?.data?.user?.processStage == 2 || self.getInformationForPaymentObject?.data?.user?.processStage == 3){
              
                    let couponDetails = GlobalStoryBoard().myFriendsVC
                    couponDetails.showView = "KYC"
                    self.navigationController?.pushViewController(couponDetails, animated: false)
               
            }else{

                let couponDetails = GlobalStoryBoard().myFriendsVC
                couponDetails.showView = "WALLET"
                self.navigationController?.pushViewController(couponDetails, animated: false)

            }
        }
    }
    
    func didTapThumbsDownButton(thumbsAlertViewController: ThumbsAlertViewController) {
        thumbsAlertViewController.dismiss(animated: true, completion: nil)
    }
        
}


extension HomeViewController: EnterCodeDelegate{
    func showClubListVC(leagueInfo : BannerLeagueInfo?, arrAPI : [Any]?){
        
        let currentDate = Date().getDateStrFrom(format: DateFormatType.pickerDate)
        if leagueInfo?.leagueEndDate.getDateFromString() ?? Date() < currentDate.getDateFromString(){
            return
        }
        if leagueInfo?.isSingleTeam == true{
                let teamVc = GlobalStoryBoard().hfl_SingleClubTeamList
                teamVc.arrAPI = arrAPI ?? []
                teamVc.leagueInfo = leagueInfo
                teamVc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(teamVc, animated: true)
        }else{
            
            let teamVc = GlobalStoryBoard().clubTeamListVC
            teamVc.arrAPI = arrAPI ?? []
            teamVc.leagueInfo = leagueInfo
            teamVc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(teamVc, animated: true)
        }
       
    }
    
}


extension HomeViewController: CollCellHFLLeagueDelegate{
    func show2000StepsSuccessPopup() {
        self.showSuccess2000StepsPopup()
    }
}

extension HomeViewController{
    func showCharityScreen(){
        var imageName = ""
        let currentDate = Date().getDateStrFrom(format: DateFormatType.pickerDate)
        let todayDate = currentDate.getDateFromString()
        /*
        if let joinedLeague = self.getJoinedLeagueInfo() {
            imageName = getJoinedImageName(teamName: joinedLeague.strSubTeamName ?? "")
            if joinedLeague.leagueStartDate.getDateFromString() > todayDate{
                // League Not start let
            }else if joinedLeague.leagueStartDate == Date().getFilterDateFormat(){
                if UserDefaultConstants.shared.isUserReached2000Steps == true{
                    imageName = CharityImage.afterJoin2K
                }
            }
          else if joinedLeague.leagueEndDate < Date().getFilterDateFormat(){
            if UserDefaultConstants.shared.isUserReached2000Steps == true{
                imageName = CharityImage.afterJoin2K
            }
          }
        }else{
            imageName = CharityImage.beforeJoin
        }
         */
        let charityImageVC = GlobalStoryBoard().charityImageVC
        charityImageVC.imageName = CharityImage.beforeJoin
        charityImageVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(charityImageVC, animated: true)
    }

}
