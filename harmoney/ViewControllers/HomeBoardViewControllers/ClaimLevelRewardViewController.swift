//
//  ClaimLevelRewardViewController.swift
//  Harmoney
//
//  Created by Csongor Korosi on 8/7/20.
//  Copyright © 2020 harmoney. All rights reserved.
//

import UIKit
protocol CollectLevelRewardsDelegate {
    func leveleRegardsCollected()
}
class ClaimLevelRewardViewController: UIViewController {
    var levelInfo: LevelInfo?
    var isFromOnboarding = false
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var xpLabel: UILabel!
    @IBOutlet weak var diamondLabel: UILabel!
    
    @IBOutlet weak var diamondImageview: UIImageView!
    
    var delegate : CollectLevelRewardsDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        self.getAllNotifications()
    }
    private func getAllNotifications() {
        NotificationsManager.sharedInstance.getAllNotifications { (result) in
            switch result {
            case .success(_): break
//                    self.setupNotificationsUI()
                case .failure(let error):
                    self.view.toast(error.localizedDescription)
            }
        }
    }
}

//MARK: UI Methods
extension ClaimLevelRewardViewController {
    func configureUI() {
        let creditType = HarmonySingleton.shared.cofigModel?.data.creditType
        
        if creditType == 1 {
            self.diamondImageview.image = UIImage(named: "soccer")

        } else if creditType == 2 {
            self.diamondImageview.image = UIImage(named: "diamond")

        } else {
            self.diamondImageview.image = UIImage(named: "touchdown")

        }
//        diamondImageview.image = UIImage(named: "diamond")
        let userLevel = HarmonySingleton.shared.dashBoardData.data?.points?.level ?? 0
        levelLabel.text = String(format: "%d", userLevel )
        
        if userLevel >= 5{
            diamondLabel.text = String(format: "%d", 10)
            xpLabel.text = String(format: "%d", 200)
        }else {
            diamondLabel.text = String(format: "%d", levelInfo?.jem ?? 0)
            xpLabel.text = String(format: "%d", levelInfo?.levelXP ?? 0)
        }

//        levelLabel.text = String(format: "%d", levelInfo?.level ?? 0)
//        diamondLabel.text = String(format: "%d", levelInfo?.jem ?? 0)
    }
}

//MARK: Action Methods
extension ClaimLevelRewardViewController {
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        if isFromOnboarding{
            self.dismiss(animated: true, completion: nil)
            delegate?.leveleRegardsCollected()
            isFromOnboarding = false
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        if isFromOnboarding{
            self.dismiss(animated: true, completion: nil)
            delegate?.leveleRegardsCollected()
            isFromOnboarding = false
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}


